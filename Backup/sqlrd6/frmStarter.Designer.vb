<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStarter
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStarter))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.picDemoEvent = New System.Windows.Forms.PictureBox
        Me.picDemoAuto = New System.Windows.Forms.PictureBox
        Me.picDemoPackage = New System.Windows.Forms.PictureBox
        Me.picDemoSingle = New System.Windows.Forms.PictureBox
        Me.LinkLabel5 = New System.Windows.Forms.LinkLabel
        Me.DividerLabel2 = New sqlrd.DividerLabel
        Me.chkDonotshow = New System.Windows.Forms.CheckBox
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel
        Me.lblEvent = New System.Windows.Forms.LinkLabel
        Me.Label5 = New System.Windows.Forms.Label
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel
        Me.lblAuto = New System.Windows.Forms.LinkLabel
        Me.Label4 = New System.Windows.Forms.Label
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel
        Me.lblPackage = New System.Windows.Forms.LinkLabel
        Me.Label3 = New System.Windows.Forms.Label
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.lblSingle = New System.Windows.Forms.LinkLabel
        Me.Label2 = New System.Windows.Forms.Label
        Me.picEvent = New System.Windows.Forms.PictureBox
        Me.picAuto = New System.Windows.Forms.PictureBox
        Me.picPackage = New System.Windows.Forms.PictureBox
        Me.picSingle = New System.Windows.Forms.PictureBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.picMore = New System.Windows.Forms.PictureBox
        Me.lblMore = New System.Windows.Forms.Label
        Me.DividerLabel1 = New sqlrd.DividerLabel
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel
        Me.lblVersion = New System.Windows.Forms.Label
        Me.lblCompany = New System.Windows.Forms.Label
        Me.lblUser = New System.Windows.Forms.Label
        Me.lblLicense = New System.Windows.Forms.Label
        Me.lblCustID = New System.Windows.Forms.Label
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.mnuType = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ConditionTypeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.DatabaseRecordExistsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DatabaseRecordHasBeenModifiedToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator
        Me.FileExistsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.FileHasBeenModifiedToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator
        Me.UnreadMailIsPresentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.Panel1.SuspendLayout()
        CType(Me.picDemoEvent, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picDemoAuto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picDemoPackage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picDemoSingle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.picEvent, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picAuto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPackage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSingle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.picMore, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel5.SuspendLayout()
        Me.mnuType.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.picDemoEvent)
        Me.Panel1.Controls.Add(Me.picDemoAuto)
        Me.Panel1.Controls.Add(Me.picDemoPackage)
        Me.Panel1.Controls.Add(Me.picDemoSingle)
        Me.Panel1.Controls.Add(Me.LinkLabel5)
        Me.Panel1.Controls.Add(Me.DividerLabel2)
        Me.Panel1.Controls.Add(Me.chkDonotshow)
        Me.Panel1.Controls.Add(Me.TableLayoutPanel4)
        Me.Panel1.Controls.Add(Me.TableLayoutPanel3)
        Me.Panel1.Controls.Add(Me.TableLayoutPanel2)
        Me.Panel1.Controls.Add(Me.TableLayoutPanel1)
        Me.Panel1.Controls.Add(Me.picEvent)
        Me.Panel1.Controls.Add(Me.picAuto)
        Me.Panel1.Controls.Add(Me.picPackage)
        Me.Panel1.Controls.Add(Me.picSingle)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 154)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(818, 433)
        Me.Panel1.TabIndex = 0
        '
        'picDemoEvent
        '
        Me.picDemoEvent.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.picDemoEvent.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picDemoEvent.Image = CType(resources.GetObject("picDemoEvent.Image"), System.Drawing.Image)
        Me.picDemoEvent.Location = New System.Drawing.Point(782, 370)
        Me.picDemoEvent.Name = "picDemoEvent"
        Me.picDemoEvent.Size = New System.Drawing.Size(24, 24)
        Me.picDemoEvent.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picDemoEvent.TabIndex = 11
        Me.picDemoEvent.TabStop = False
        Me.ToolTip1.SetToolTip(Me.picDemoEvent, "Click here to watch a demo")
        '
        'picDemoAuto
        '
        Me.picDemoAuto.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.picDemoAuto.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picDemoAuto.Image = CType(resources.GetObject("picDemoAuto.Image"), System.Drawing.Image)
        Me.picDemoAuto.Location = New System.Drawing.Point(782, 262)
        Me.picDemoAuto.Name = "picDemoAuto"
        Me.picDemoAuto.Size = New System.Drawing.Size(24, 24)
        Me.picDemoAuto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picDemoAuto.TabIndex = 10
        Me.picDemoAuto.TabStop = False
        Me.ToolTip1.SetToolTip(Me.picDemoAuto, "Click here to watch a demo")
        '
        'picDemoPackage
        '
        Me.picDemoPackage.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.picDemoPackage.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picDemoPackage.Image = CType(resources.GetObject("picDemoPackage.Image"), System.Drawing.Image)
        Me.picDemoPackage.Location = New System.Drawing.Point(782, 166)
        Me.picDemoPackage.Name = "picDemoPackage"
        Me.picDemoPackage.Size = New System.Drawing.Size(24, 24)
        Me.picDemoPackage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picDemoPackage.TabIndex = 9
        Me.picDemoPackage.TabStop = False
        Me.ToolTip1.SetToolTip(Me.picDemoPackage, "Click here to watch a demo")
        '
        'picDemoSingle
        '
        Me.picDemoSingle.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.picDemoSingle.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picDemoSingle.Image = CType(resources.GetObject("picDemoSingle.Image"), System.Drawing.Image)
        Me.picDemoSingle.Location = New System.Drawing.Point(782, 53)
        Me.picDemoSingle.Name = "picDemoSingle"
        Me.picDemoSingle.Size = New System.Drawing.Size(24, 24)
        Me.picDemoSingle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picDemoSingle.TabIndex = 8
        Me.picDemoSingle.TabStop = False
        Me.ToolTip1.SetToolTip(Me.picDemoSingle, "Click here to watch a demo")
        '
        'LinkLabel5
        '
        Me.LinkLabel5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LinkLabel5.AutoSize = True
        Me.LinkLabel5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel5.Location = New System.Drawing.Point(764, 410)
        Me.LinkLabel5.Name = "LinkLabel5"
        Me.LinkLabel5.Size = New System.Drawing.Size(43, 13)
        Me.LinkLabel5.TabIndex = 7
        Me.LinkLabel5.TabStop = True
        Me.LinkLabel5.Text = "More..."
        '
        'DividerLabel2
        '
        Me.DividerLabel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DividerLabel2.BackColor = System.Drawing.Color.Transparent
        Me.DividerLabel2.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel2.Location = New System.Drawing.Point(-3, 397)
        Me.DividerLabel2.Name = "DividerLabel2"
        Me.DividerLabel2.Size = New System.Drawing.Size(821, 10)
        Me.DividerLabel2.Spacing = 0
        Me.DividerLabel2.TabIndex = 6
        '
        'chkDonotshow
        '
        Me.chkDonotshow.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDonotshow.Location = New System.Drawing.Point(13, 410)
        Me.chkDonotshow.Name = "chkDonotshow"
        Me.chkDonotshow.Size = New System.Drawing.Size(402, 24)
        Me.chkDonotshow.TabIndex = 5
        Me.chkDonotshow.Text = "Do not show at start up"
        Me.chkDonotshow.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel4.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel4.ColumnCount = 1
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.lblEvent, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label5, 0, 1)
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(66, 293)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 2
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(741, 78)
        Me.TableLayoutPanel4.TabIndex = 4
        '
        'lblEvent
        '
        Me.lblEvent.BackColor = System.Drawing.Color.Transparent
        Me.lblEvent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEvent.Location = New System.Drawing.Point(3, 0)
        Me.lblEvent.Name = "lblEvent"
        Me.lblEvent.Size = New System.Drawing.Size(446, 23)
        Me.lblEvent.TabIndex = 0
        Me.lblEvent.TabStop = True
        Me.lblEvent.Text = "Create  an  Event-Based  Schedule"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(3, 23)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(732, 52)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = resources.GetString("Label5.Text")
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel3.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel3.ColumnCount = 1
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.lblAuto, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Label4, 0, 1)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(66, 198)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 2
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(741, 67)
        Me.TableLayoutPanel3.TabIndex = 4
        '
        'lblAuto
        '
        Me.lblAuto.BackColor = System.Drawing.Color.Transparent
        Me.lblAuto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAuto.Location = New System.Drawing.Point(3, 0)
        Me.lblAuto.Name = "lblAuto"
        Me.lblAuto.Size = New System.Drawing.Size(446, 23)
        Me.lblAuto.TabIndex = 0
        Me.lblAuto.TabStop = True
        Me.lblAuto.Text = "Create  an  Automation  Schedule"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(3, 23)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(732, 26)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = resources.GetString("Label4.Text")
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel2.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.lblPackage, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label3, 0, 1)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(66, 89)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(741, 78)
        Me.TableLayoutPanel2.TabIndex = 4
        '
        'lblPackage
        '
        Me.lblPackage.BackColor = System.Drawing.Color.Transparent
        Me.lblPackage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPackage.Location = New System.Drawing.Point(3, 0)
        Me.lblPackage.Name = "lblPackage"
        Me.lblPackage.Size = New System.Drawing.Size(446, 23)
        Me.lblPackage.TabIndex = 0
        Me.lblPackage.TabStop = True
        Me.lblPackage.Text = "Create  a  Package  Schedule"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(3, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(734, 52)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = resources.GetString("Label3.Text")
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.lblSingle, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 1)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(66, 7)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(740, 48)
        Me.TableLayoutPanel1.TabIndex = 4
        '
        'lblSingle
        '
        Me.lblSingle.BackColor = System.Drawing.Color.Transparent
        Me.lblSingle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSingle.Location = New System.Drawing.Point(3, 0)
        Me.lblSingle.Name = "lblSingle"
        Me.lblSingle.Size = New System.Drawing.Size(446, 18)
        Me.lblSingle.TabIndex = 0
        Me.lblSingle.TabStop = True
        Me.lblSingle.Text = "Create  a  Single  Report  Schedule"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(727, 26)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = resources.GetString("Label2.Text")
        '
        'picEvent
        '
        Me.picEvent.BackColor = System.Drawing.Color.Transparent
        Me.picEvent.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picEvent.Image = CType(resources.GetObject("picEvent.Image"), System.Drawing.Image)
        Me.picEvent.Location = New System.Drawing.Point(12, 293)
        Me.picEvent.Name = "picEvent"
        Me.picEvent.Size = New System.Drawing.Size(48, 48)
        Me.picEvent.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picEvent.TabIndex = 2
        Me.picEvent.TabStop = False
        '
        'picAuto
        '
        Me.picAuto.BackColor = System.Drawing.Color.Transparent
        Me.picAuto.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picAuto.Image = CType(resources.GetObject("picAuto.Image"), System.Drawing.Image)
        Me.picAuto.Location = New System.Drawing.Point(12, 198)
        Me.picAuto.Name = "picAuto"
        Me.picAuto.Size = New System.Drawing.Size(48, 48)
        Me.picAuto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picAuto.TabIndex = 2
        Me.picAuto.TabStop = False
        '
        'picPackage
        '
        Me.picPackage.BackColor = System.Drawing.Color.Transparent
        Me.picPackage.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picPackage.Image = CType(resources.GetObject("picPackage.Image"), System.Drawing.Image)
        Me.picPackage.Location = New System.Drawing.Point(12, 89)
        Me.picPackage.Name = "picPackage"
        Me.picPackage.Size = New System.Drawing.Size(48, 48)
        Me.picPackage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picPackage.TabIndex = 2
        Me.picPackage.TabStop = False
        '
        'picSingle
        '
        Me.picSingle.BackColor = System.Drawing.Color.Transparent
        Me.picSingle.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picSingle.Image = CType(resources.GetObject("picSingle.Image"), System.Drawing.Image)
        Me.picSingle.Location = New System.Drawing.Point(12, 7)
        Me.picSingle.Name = "picSingle"
        Me.picSingle.Size = New System.Drawing.Size(48, 48)
        Me.picSingle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picSingle.TabIndex = 2
        Me.picSingle.TabStop = False
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Navy
        Me.Label1.Location = New System.Drawing.Point(8, 4)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(331, 28)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Getting started with SQL-RD"
        '
        'Panel2
        '
        Me.Panel2.BackgroundImage = CType(resources.GetObject("Panel2.BackgroundImage"), System.Drawing.Image)
        Me.Panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel2.Controls.Add(Me.picMore)
        Me.Panel2.Controls.Add(Me.lblMore)
        Me.Panel2.Controls.Add(Me.DividerLabel1)
        Me.Panel2.Controls.Add(Me.TableLayoutPanel5)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(818, 154)
        Me.Panel2.TabIndex = 1
        '
        'picMore
        '
        Me.picMore.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picMore.BackColor = System.Drawing.Color.Transparent
        Me.picMore.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picMore.Image = CType(resources.GetObject("picMore.Image"), System.Drawing.Image)
        Me.picMore.Location = New System.Drawing.Point(675, 39)
        Me.picMore.Name = "picMore"
        Me.picMore.Size = New System.Drawing.Size(24, 24)
        Me.picMore.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picMore.TabIndex = 4
        Me.picMore.TabStop = False
        '
        'lblMore
        '
        Me.lblMore.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMore.AutoSize = True
        Me.lblMore.BackColor = System.Drawing.Color.Transparent
        Me.lblMore.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lblMore.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMore.ForeColor = System.Drawing.Color.White
        Me.lblMore.Location = New System.Drawing.Point(699, 43)
        Me.lblMore.Name = "lblMore"
        Me.lblMore.Size = New System.Drawing.Size(108, 15)
        Me.lblMore.TabIndex = 7
        Me.lblMore.Text = "Configure settings"
        '
        'DividerLabel1
        '
        Me.DividerLabel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DividerLabel1.BackColor = System.Drawing.Color.Transparent
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(345, 17)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(473, 10)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 3
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel5.ColumnCount = 1
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel5.Controls.Add(Me.lblVersion, 0, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.lblCompany, 0, 1)
        Me.TableLayoutPanel5.Controls.Add(Me.lblUser, 0, 2)
        Me.TableLayoutPanel5.Controls.Add(Me.lblLicense, 0, 3)
        Me.TableLayoutPanel5.Controls.Add(Me.lblCustID, 0, 4)
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(66, 35)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 5
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(549, 116)
        Me.TableLayoutPanel5.TabIndex = 2
        '
        'lblVersion
        '
        Me.lblVersion.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVersion.Location = New System.Drawing.Point(3, 0)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(536, 23)
        Me.lblVersion.TabIndex = 0
        Me.lblVersion.Text = "SQL-RD 6.1 Build 20070222"
        Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCompany
        '
        Me.lblCompany.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompany.Location = New System.Drawing.Point(3, 23)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(536, 23)
        Me.lblCompany.TabIndex = 0
        Me.lblCompany.Text = "ChristianSteven Software Ltd"
        Me.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblUser
        '
        Me.lblUser.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUser.Location = New System.Drawing.Point(3, 46)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(536, 23)
        Me.lblUser.TabIndex = 0
        Me.lblUser.Text = "Steven Amani"
        Me.lblUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLicense
        '
        Me.lblLicense.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLicense.Location = New System.Drawing.Point(3, 69)
        Me.lblLicense.Name = "lblLicense"
        Me.lblLicense.Size = New System.Drawing.Size(536, 23)
        Me.lblLicense.TabIndex = 0
        Me.lblLicense.Text = "ABCD-EFGH-IJKL"
        Me.lblLicense.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCustID
        '
        Me.lblCustID.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCustID.Location = New System.Drawing.Point(3, 92)
        Me.lblCustID.Name = "lblCustID"
        Me.lblCustID.Size = New System.Drawing.Size(536, 23)
        Me.lblCustID.TabIndex = 0
        Me.lblCustID.Text = "1001350"
        Me.lblCustID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'mnuType
        '
        Me.mnuType.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConditionTypeToolStripMenuItem, Me.ToolStripSeparator1, Me.DatabaseRecordExistsToolStripMenuItem, Me.DatabaseRecordHasBeenModifiedToolStripMenuItem, Me.ToolStripMenuItem1, Me.FileExistsToolStripMenuItem, Me.FileHasBeenModifiedToolStripMenuItem, Me.ToolStripMenuItem3, Me.UnreadMailIsPresentToolStripMenuItem})
        Me.mnuType.Name = "mnuType"
        Me.mnuType.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuType.Size = New System.Drawing.Size(264, 154)
        '
        'ConditionTypeToolStripMenuItem
        '
        Me.ConditionTypeToolStripMenuItem.Enabled = False
        Me.ConditionTypeToolStripMenuItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ConditionTypeToolStripMenuItem.Name = "ConditionTypeToolStripMenuItem"
        Me.ConditionTypeToolStripMenuItem.Size = New System.Drawing.Size(263, 22)
        Me.ConditionTypeToolStripMenuItem.Text = "View Demo for Condition Type"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(260, 6)
        '
        'DatabaseRecordExistsToolStripMenuItem
        '
        Me.DatabaseRecordExistsToolStripMenuItem.Image = CType(resources.GetObject("DatabaseRecordExistsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.DatabaseRecordExistsToolStripMenuItem.Name = "DatabaseRecordExistsToolStripMenuItem"
        Me.DatabaseRecordExistsToolStripMenuItem.Size = New System.Drawing.Size(263, 22)
        Me.DatabaseRecordExistsToolStripMenuItem.Text = "Database Record Exists"
        '
        'DatabaseRecordHasBeenModifiedToolStripMenuItem
        '
        Me.DatabaseRecordHasBeenModifiedToolStripMenuItem.Image = CType(resources.GetObject("DatabaseRecordHasBeenModifiedToolStripMenuItem.Image"), System.Drawing.Image)
        Me.DatabaseRecordHasBeenModifiedToolStripMenuItem.Name = "DatabaseRecordHasBeenModifiedToolStripMenuItem"
        Me.DatabaseRecordHasBeenModifiedToolStripMenuItem.Size = New System.Drawing.Size(263, 22)
        Me.DatabaseRecordHasBeenModifiedToolStripMenuItem.Text = "Database Record has been Modified"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(260, 6)
        '
        'FileExistsToolStripMenuItem
        '
        Me.FileExistsToolStripMenuItem.Image = CType(resources.GetObject("FileExistsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.FileExistsToolStripMenuItem.Name = "FileExistsToolStripMenuItem"
        Me.FileExistsToolStripMenuItem.Size = New System.Drawing.Size(263, 22)
        Me.FileExistsToolStripMenuItem.Text = "File Exists"
        '
        'FileHasBeenModifiedToolStripMenuItem
        '
        Me.FileHasBeenModifiedToolStripMenuItem.Image = CType(resources.GetObject("FileHasBeenModifiedToolStripMenuItem.Image"), System.Drawing.Image)
        Me.FileHasBeenModifiedToolStripMenuItem.Name = "FileHasBeenModifiedToolStripMenuItem"
        Me.FileHasBeenModifiedToolStripMenuItem.Size = New System.Drawing.Size(263, 22)
        Me.FileHasBeenModifiedToolStripMenuItem.Text = "File has been Modified"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(260, 6)
        '
        'UnreadMailIsPresentToolStripMenuItem
        '
        Me.UnreadMailIsPresentToolStripMenuItem.Image = CType(resources.GetObject("UnreadMailIsPresentToolStripMenuItem.Image"), System.Drawing.Image)
        Me.UnreadMailIsPresentToolStripMenuItem.Name = "UnreadMailIsPresentToolStripMenuItem"
        Me.UnreadMailIsPresentToolStripMenuItem.Size = New System.Drawing.Size(263, 22)
        Me.UnreadMailIsPresentToolStripMenuItem.Text = "Unread mail is present"
        '
        'frmStarter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(818, 587)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmStarter"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Welcome to SQL-RD"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.picDemoEvent, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picDemoAuto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picDemoPackage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picDemoSingle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.TableLayoutPanel4.PerformLayout()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        CType(Me.picEvent, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picAuto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPackage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSingle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.picMore, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.mnuType.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents picSingle As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents picEvent As System.Windows.Forms.PictureBox
    Friend WithEvents picAuto As System.Windows.Forms.PictureBox
    Friend WithEvents picPackage As System.Windows.Forms.PictureBox
    Friend WithEvents lblSingle As System.Windows.Forms.LinkLabel
    Friend WithEvents lblEvent As System.Windows.Forms.LinkLabel
    Friend WithEvents lblAuto As System.Windows.Forms.LinkLabel
    Friend WithEvents lblPackage As System.Windows.Forms.LinkLabel
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Friend WithEvents TableLayoutPanel5 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblVersion As System.Windows.Forms.Label
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents lblUser As System.Windows.Forms.Label
    Friend WithEvents lblLicense As System.Windows.Forms.Label
    Friend WithEvents lblCustID As System.Windows.Forms.Label
    Friend WithEvents LinkLabel5 As System.Windows.Forms.LinkLabel
    Friend WithEvents DividerLabel2 As sqlrd.DividerLabel
    Friend WithEvents chkDonotshow As System.Windows.Forms.CheckBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents picDemoSingle As System.Windows.Forms.PictureBox
    Friend WithEvents picDemoEvent As System.Windows.Forms.PictureBox
    Friend WithEvents picDemoAuto As System.Windows.Forms.PictureBox
    Friend WithEvents picDemoPackage As System.Windows.Forms.PictureBox
    Friend WithEvents picMore As System.Windows.Forms.PictureBox
    Friend WithEvents lblMore As System.Windows.Forms.Label
    Friend WithEvents mnuType As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ConditionTypeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents DatabaseRecordExistsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DatabaseRecordHasBeenModifiedToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents FileExistsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FileHasBeenModifiedToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents UnreadMailIsPresentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
