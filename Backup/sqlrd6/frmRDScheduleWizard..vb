Imports System.Drawing.Printing
Imports Microsoft.Win32
Imports Microsoft.Reporting.WinForms
#If CRYSTAL_VER = 8 Then
imports My.Crystal85
#ElseIf CRYSTAL_VER = 9 Then
imports My.Crystal9 
#ElseIf CRYSTAL_VER = 10 Then
imports My.Crystal10 
#ElseIf CRYSTAL_VER = 11 Then
Imports My.Crystal11
#End If
Public Class frmRDScheduleWizard
    Inherits System.Windows.Forms.Form
    '#Const CRYSTAL_VER = 9
    Dim nStep As Integer
    Dim sFrequency As String
    Dim oErr As clsMarsUI = New clsMarsUI
    Dim oMsg As clsMarsMessaging = New clsMarsMessaging
    Dim oRpt As Object 'ReportServer.ReportingService
    Dim ReportRestore() As Boolean
    Dim oUI As New clsMarsUI
    Dim ServerUser As String = ""
    Dim ServerPassword As String = ""
    Dim ParDefaults As Hashtable
    Dim dtParameters As DataTable
    Dim showTooltip As Boolean = True
    Dim m_serverParametersTable As DataTable

    Const S1 As String = "Step 1: Data Connection Setup"
    Const S2 As String = "Step 2: Report Setup"
    Const S3 As String = "Step 3: Schedule Setup"
    Const S4 As String = "Step 4: Destination Setup"
    Const S5 As String = "Step 5: Report Parameters"
    Const S6 As String = "Step 6: Report Options"
    Const S7 As String = "Step 7: Exception Handling"
    Const S8 As String = "Step 8: Custom Tasks"

    Dim HasCancelled As Boolean = False
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Friend WithEvents txtUrl As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lsvDatasources As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtSnapshots As System.Windows.Forms.NumericUpDown
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents mnuDatasources As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ClearToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UcSet As sqlrd.ucScheduleSet
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Step1 As System.Windows.Forms.Panel
    Friend WithEvents chkGroupReports As System.Windows.Forms.CheckBox
    Friend WithEvents btnConnect As System.Windows.Forms.Button
    Friend WithEvents grpQuery As System.Windows.Forms.GroupBox
    Friend WithEvents btnBuild As System.Windows.Forms.Button
    Friend WithEvents txtQuery As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents chkSnapshots As System.Windows.Forms.CheckBox
    Friend WithEvents UcError As sqlrd.ucErrorHandler
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents optOnce As System.Windows.Forms.RadioButton
    Friend WithEvents optAll As System.Windows.Forms.RadioButton
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents cmbDDKey As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Public Shared m_DataFields As Hashtable

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Step2 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtFolder As System.Windows.Forms.TextBox
    Friend WithEvents cmdLoc As System.Windows.Forms.Button
    Friend WithEvents txtDBLoc As System.Windows.Forms.TextBox
    Friend WithEvents cmdDbLoc As System.Windows.Forms.Button
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents cmdNext As System.Windows.Forms.Button
    Friend WithEvents cmdBack As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents Step3 As System.Windows.Forms.Panel
    Friend WithEvents fbg As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents Step4 As System.Windows.Forms.Panel
    Friend WithEvents Step5 As System.Windows.Forms.Panel
    Friend WithEvents ErrProv As System.Windows.Forms.ErrorProvider
    Friend WithEvents cmdFinish As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lsvParameters As System.Windows.Forms.ListView
    Friend WithEvents txtFormula As System.Windows.Forms.TextBox
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents oTask As sqlrd.ucTasks
    Friend WithEvents lblStep As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdParameters As System.Windows.Forms.Button
    Friend WithEvents mnuContacts As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuMAPI As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMARS As System.Windows.Forms.MenuItem
    Friend WithEvents txtDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtKeyWord As System.Windows.Forms.TextBox
    Friend WithEvents UcDest As sqlrd.ucDestination
    Friend WithEvents UcBlank As sqlrd.ucBlankAlert
    Friend WithEvents UcTasks As sqlrd.ucTasks
    Friend WithEvents Step7 As System.Windows.Forms.Panel
    Friend WithEvents Step8 As System.Windows.Forms.Panel
    Friend WithEvents Step6 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdLoginTest As System.Windows.Forms.Button
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents Footer1 As WizardFooter.Footer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRDScheduleWizard))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Step2 = New System.Windows.Forms.Panel
        Me.txtUrl = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtDesc = New System.Windows.Forms.TextBox
        Me.cmdDbLoc = New System.Windows.Forms.Button
        Me.txtDBLoc = New System.Windows.Forms.TextBox
        Me.cmdLoc = New System.Windows.Forms.Button
        Me.txtFolder = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtKeyWord = New System.Windows.Forms.TextBox
        Me.lblStep = New System.Windows.Forms.Label
        Me.ofd = New System.Windows.Forms.OpenFileDialog
        Me.cmdNext = New System.Windows.Forms.Button
        Me.cmdBack = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.Step3 = New System.Windows.Forms.Panel
        Me.UcSet = New sqlrd.ucScheduleSet
        Me.Step4 = New System.Windows.Forms.Panel
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.UcDest = New sqlrd.ucDestination
        Me.fbg = New System.Windows.Forms.FolderBrowserDialog
        Me.Step5 = New System.Windows.Forms.Panel
        Me.txtFormula = New System.Windows.Forms.TextBox
        Me.lsvParameters = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader5 = New System.Windows.Forms.ColumnHeader
        Me.cmdParameters = New System.Windows.Forms.Button
        Me.Step7 = New System.Windows.Forms.Panel
        Me.UcError = New sqlrd.ucErrorHandler
        Me.UcBlank = New sqlrd.ucBlankAlert
        Me.ErrProv = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cmdFinish = New System.Windows.Forms.Button
        Me.Step8 = New System.Windows.Forms.Panel
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.optOnce = New System.Windows.Forms.RadioButton
        Me.optAll = New System.Windows.Forms.RadioButton
        Me.UcTasks = New sqlrd.ucTasks
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Step6 = New System.Windows.Forms.Panel
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtSnapshots = New System.Windows.Forms.NumericUpDown
        Me.chkSnapshots = New System.Windows.Forms.CheckBox
        Me.lsvDatasources = New System.Windows.Forms.ListView
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader4 = New System.Windows.Forms.ColumnHeader
        Me.mnuDatasources = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ClearToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.cmdLoginTest = New System.Windows.Forms.Button
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.mnuContacts = New System.Windows.Forms.ContextMenu
        Me.mnuMAPI = New System.Windows.Forms.MenuItem
        Me.mnuMARS = New System.Windows.Forms.MenuItem
        Me.Footer1 = New WizardFooter.Footer
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider
        Me.DividerLabel1 = New sqlrd.DividerLabel
        Me.Step1 = New System.Windows.Forms.Panel
        Me.UcDSN = New sqlrd.ucDSN
        Me.chkGroupReports = New System.Windows.Forms.CheckBox
        Me.btnConnect = New System.Windows.Forms.Button
        Me.grpQuery = New System.Windows.Forms.GroupBox
        Me.btnBuild = New System.Windows.Forms.Button
        Me.txtQuery = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.mnuInserter = New System.Windows.Forms.ContextMenu
        Me.mnuUndo = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.mnuCut = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuPaste = New System.Windows.Forms.MenuItem
        Me.mnuDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.cmbDDKey = New System.Windows.Forms.ComboBox
        Me.Label8 = New System.Windows.Forms.Label
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step2.SuspendLayout()
        Me.Step3.SuspendLayout()
        Me.Step4.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.Step5.SuspendLayout()
        Me.Step7.SuspendLayout()
        CType(Me.ErrProv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step8.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Step6.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.txtSnapshots, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mnuDatasources.SuspendLayout()
        Me.Step1.SuspendLayout()
        Me.grpQuery.SuspendLayout()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.ErrProv.SetIconAlignment(Me.PictureBox1, System.Windows.Forms.ErrorIconAlignment.MiddleLeft)
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.PictureBox1.Location = New System.Drawing.Point(376, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(48, 48)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        Me.PictureBox1.WaitOnLoad = True
        '
        'Step2
        '
        Me.Step2.BackColor = System.Drawing.SystemColors.Control
        Me.Step2.Controls.Add(Me.txtUrl)
        Me.Step2.Controls.Add(Me.Label6)
        Me.Step2.Controls.Add(Me.txtDesc)
        Me.Step2.Controls.Add(Me.cmdDbLoc)
        Me.Step2.Controls.Add(Me.txtDBLoc)
        Me.Step2.Controls.Add(Me.cmdLoc)
        Me.Step2.Controls.Add(Me.txtFolder)
        Me.Step2.Controls.Add(Me.Label4)
        Me.Step2.Controls.Add(Me.Label3)
        Me.Step2.Controls.Add(Me.txtName)
        Me.Step2.Controls.Add(Me.Label2)
        Me.Step2.Controls.Add(Me.Label1)
        Me.Step2.Controls.Add(Me.Label7)
        Me.Step2.Controls.Add(Me.txtKeyWord)
        Me.HelpProvider1.SetHelpKeyword(Me.Step2, "Single_Report_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.Step2, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step2.Location = New System.Drawing.Point(0, 72)
        Me.Step2.Name = "Step2"
        Me.HelpProvider1.SetShowHelp(Me.Step2, True)
        Me.Step2.Size = New System.Drawing.Size(440, 377)
        Me.Step2.TabIndex = 0
        '
        'txtUrl
        '
        Me.txtUrl.BackColor = System.Drawing.Color.White
        Me.txtUrl.ForeColor = System.Drawing.Color.Blue
        Me.txtUrl.Location = New System.Drawing.Point(8, 67)
        Me.txtUrl.Name = "txtUrl"
        Me.txtUrl.Size = New System.Drawing.Size(358, 21)
        Me.SuperTooltip1.SetSuperTooltip(Me.txtUrl, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Enter the address for the report server's web service e.g. http://myreportserver/" & _
                    "reportserver/reportservice.asmx", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, True, True, New System.Drawing.Size(320, 85)))
        Me.txtUrl.TabIndex = 2
        Me.txtUrl.Tag = "memo"
        Me.txtUrl.Text = "http://myReportServer/ReportServer/"
        '
        'Label6
        '
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(8, 50)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(224, 16)
        Me.Label6.TabIndex = 24
        Me.Label6.Text = "Report  Service URL"
        '
        'txtDesc
        '
        Me.txtDesc.Location = New System.Drawing.Point(8, 198)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDesc.Size = New System.Drawing.Size(360, 98)
        Me.txtDesc.TabIndex = 6
        Me.txtDesc.Tag = "memo"
        '
        'cmdDbLoc
        '
        Me.cmdDbLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDbLoc.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdDbLoc.Image = CType(resources.GetObject("cmdDbLoc.Image"), System.Drawing.Image)
        Me.cmdDbLoc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDbLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDbLoc.Location = New System.Drawing.Point(310, 115)
        Me.cmdDbLoc.Name = "cmdDbLoc"
        Me.cmdDbLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdDbLoc.TabIndex = 4
        Me.cmdDbLoc.Text = "..."
        Me.cmdDbLoc.UseVisualStyleBackColor = False
        '
        'txtDBLoc
        '
        Me.txtDBLoc.BackColor = System.Drawing.Color.White
        Me.txtDBLoc.ForeColor = System.Drawing.Color.Blue
        Me.txtDBLoc.Location = New System.Drawing.Point(8, 115)
        Me.txtDBLoc.Name = "txtDBLoc"
        Me.txtDBLoc.Size = New System.Drawing.Size(296, 21)
        Me.txtDBLoc.TabIndex = 3
        '
        'cmdLoc
        '
        Me.cmdLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdLoc.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdLoc.Image = CType(resources.GetObject("cmdLoc.Image"), System.Drawing.Image)
        Me.cmdLoc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoc.Location = New System.Drawing.Point(310, 24)
        Me.cmdLoc.Name = "cmdLoc"
        Me.cmdLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdLoc.TabIndex = 1
        Me.cmdLoc.Text = "...."
        Me.ToolTip.SetToolTip(Me.cmdLoc, "Browse")
        Me.cmdLoc.UseVisualStyleBackColor = False
        '
        'txtFolder
        '
        Me.txtFolder.BackColor = System.Drawing.Color.White
        Me.txtFolder.ForeColor = System.Drawing.Color.Blue
        Me.txtFolder.Location = New System.Drawing.Point(8, 24)
        Me.txtFolder.Name = "txtFolder"
        Me.txtFolder.ReadOnly = True
        Me.txtFolder.Size = New System.Drawing.Size(296, 21)
        Me.txtFolder.TabIndex = 0
        Me.txtFolder.Tag = "1"
        '
        'Label4
        '
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 8)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(208, 16)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Create In"
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 96)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(208, 16)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Report  Location"
        '
        'txtName
        '
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(6, 155)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(360, 21)
        Me.txtName.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 141)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(208, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Schedule Name"
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 179)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(208, 16)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Description (optional)"
        '
        'Label7
        '
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(8, 304)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(208, 16)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Keyword (optional)"
        '
        'txtKeyWord
        '
        Me.txtKeyWord.BackColor = System.Drawing.Color.White
        Me.txtKeyWord.ForeColor = System.Drawing.Color.Blue
        Me.txtKeyWord.Location = New System.Drawing.Point(8, 320)
        Me.txtKeyWord.Name = "txtKeyWord"
        Me.txtKeyWord.Size = New System.Drawing.Size(360, 21)
        Me.txtKeyWord.TabIndex = 7
        Me.txtKeyWord.Tag = "memo"
        '
        'lblStep
        '
        Me.lblStep.BackColor = System.Drawing.Color.Transparent
        Me.lblStep.Font = New System.Drawing.Font("Tahoma", 15.75!)
        Me.lblStep.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblStep.Location = New System.Drawing.Point(8, 11)
        Me.lblStep.Name = "lblStep"
        Me.lblStep.Size = New System.Drawing.Size(360, 32)
        Me.lblStep.TabIndex = 2
        Me.lblStep.Text = "Step 1: Report Setup"
        '
        'ofd
        '
        Me.ofd.DefaultExt = "*.mdb"
        Me.ofd.Filter = "Report Definition File|*.rdl|All Files|*.*"
        '
        'cmdNext
        '
        Me.cmdNext.Enabled = False
        Me.cmdNext.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
        Me.cmdNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(357, 477)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(75, 25)
        Me.cmdNext.TabIndex = 49
        Me.cmdNext.Text = "&Next"
        '
        'cmdBack
        '
        Me.cmdBack.Enabled = False
        Me.cmdBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBack.Image = CType(resources.GetObject("cmdBack.Image"), System.Drawing.Image)
        Me.cmdBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBack.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBack.Location = New System.Drawing.Point(197, 477)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(75, 25)
        Me.cmdBack.TabIndex = 47
        Me.cmdBack.Text = "&Back"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(277, 477)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 25)
        Me.cmdCancel.TabIndex = 48
        Me.cmdCancel.Text = "&Cancel"
        '
        'Step3
        '
        Me.Step3.BackColor = System.Drawing.SystemColors.Control
        Me.Step3.Controls.Add(Me.UcSet)
        Me.HelpProvider1.SetHelpKeyword(Me.Step3, "Single_Report_Schedule.htm#Step2")
        Me.HelpProvider1.SetHelpNavigator(Me.Step3, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step3.Location = New System.Drawing.Point(0, 72)
        Me.Step3.Name = "Step3"
        Me.HelpProvider1.SetShowHelp(Me.Step3, True)
        Me.Step3.Size = New System.Drawing.Size(440, 377)
        Me.Step3.TabIndex = 0
        Me.Step3.Visible = False
        '
        'UcSet
        '
        Me.UcSet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UcSet.Location = New System.Drawing.Point(5, 8)
        Me.UcSet.m_RepeatUnit = ""
        Me.UcSet.Name = "UcSet"
        Me.UcSet.Size = New System.Drawing.Size(421, 366)
        Me.UcSet.TabIndex = 0
        '
        'Step4
        '
        Me.Step4.BackColor = System.Drawing.SystemColors.Control
        Me.Step4.Controls.Add(Me.GroupBox7)
        Me.Step4.Location = New System.Drawing.Point(0, 72)
        Me.Step4.Name = "Step4"
        Me.Step4.Size = New System.Drawing.Size(440, 377)
        Me.Step4.TabIndex = 8
        Me.Step4.Visible = False
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.UcDest)
        Me.GroupBox7.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(424, 336)
        Me.GroupBox7.TabIndex = 2
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Destinations"
        '
        'UcDest
        '
        Me.UcDest.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.HelpProvider1.SetHelpKeyword(Me.UcDest, "Single_Report_Schedule.htm#Step3")
        Me.HelpProvider1.SetHelpNavigator(Me.UcDest, System.Windows.Forms.HelpNavigator.Topic)
        Me.UcDest.Location = New System.Drawing.Point(8, 16)
        Me.UcDest.m_CanDisable = True
        Me.UcDest.m_DelayDelete = False
        Me.UcDest.m_eventBased = False
        Me.UcDest.m_ExcelBurst = False
        Me.UcDest.m_IsDynamic = False
        Me.UcDest.m_isPackage = False
        Me.UcDest.m_StaticDest = False
        Me.UcDest.Name = "UcDest"
        Me.HelpProvider1.SetShowHelp(Me.UcDest, True)
        Me.UcDest.Size = New System.Drawing.Size(408, 312)
        Me.UcDest.TabIndex = 0
        '
        'Step5
        '
        Me.Step5.BackColor = System.Drawing.SystemColors.Control
        Me.Step5.Controls.Add(Me.txtFormula)
        Me.Step5.Controls.Add(Me.lsvParameters)
        Me.Step5.Controls.Add(Me.cmdParameters)
        Me.Step5.Location = New System.Drawing.Point(0, 72)
        Me.Step5.Name = "Step5"
        Me.Step5.Size = New System.Drawing.Size(440, 377)
        Me.Step5.TabIndex = 9
        Me.Step5.Visible = False
        '
        'txtFormula
        '
        Me.txtFormula.Location = New System.Drawing.Point(336, 168)
        Me.txtFormula.Name = "txtFormula"
        Me.txtFormula.Size = New System.Drawing.Size(100, 21)
        Me.txtFormula.TabIndex = 11
        Me.txtFormula.Visible = False
        '
        'lsvParameters
        '
        Me.lsvParameters.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader5})
        Me.lsvParameters.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.lsvParameters.ForeColor = System.Drawing.Color.Blue
        Me.lsvParameters.FullRowSelect = True
        Me.lsvParameters.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.HelpProvider1.SetHelpKeyword(Me.lsvParameters, "Single_Report_Schedule.htm#Step4")
        Me.HelpProvider1.SetHelpNavigator(Me.lsvParameters, System.Windows.Forms.HelpNavigator.Topic)
        Me.lsvParameters.Location = New System.Drawing.Point(8, 16)
        Me.lsvParameters.Name = "lsvParameters"
        Me.HelpProvider1.SetShowHelp(Me.lsvParameters, True)
        Me.lsvParameters.Size = New System.Drawing.Size(424, 298)
        Me.lsvParameters.TabIndex = 0
        Me.ToolTip.SetToolTip(Me.lsvParameters, "Double-click a parameter to set its value")
        Me.lsvParameters.UseCompatibleStateImageBehavior = False
        Me.lsvParameters.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Parameter Name"
        Me.ColumnHeader1.Width = 180
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Parameter Value"
        Me.ColumnHeader2.Width = 164
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Multi-Value"
        Me.ColumnHeader5.Width = 68
        '
        'cmdParameters
        '
        Me.cmdParameters.BackColor = System.Drawing.Color.Transparent
        Me.cmdParameters.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.cmdParameters.ForeColor = System.Drawing.Color.Navy
        Me.HelpProvider1.SetHelpKeyword(Me.cmdParameters, "Single_Report_Schedule.htm#Step4")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdParameters, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdParameters.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdParameters.Location = New System.Drawing.Point(8, 320)
        Me.cmdParameters.Name = "cmdParameters"
        Me.HelpProvider1.SetShowHelp(Me.cmdParameters, True)
        Me.cmdParameters.Size = New System.Drawing.Size(75, 23)
        Me.cmdParameters.TabIndex = 1
        Me.cmdParameters.Text = "Parameters"
        Me.ToolTip.SetToolTip(Me.cmdParameters, "Click me to edit the report's selection formula")
        Me.cmdParameters.UseVisualStyleBackColor = False
        '
        'Step7
        '
        Me.Step7.BackColor = System.Drawing.SystemColors.Control
        Me.Step7.Controls.Add(Me.UcError)
        Me.Step7.Controls.Add(Me.UcBlank)
        Me.Step7.Location = New System.Drawing.Point(0, 72)
        Me.Step7.Name = "Step7"
        Me.Step7.Size = New System.Drawing.Size(440, 377)
        Me.Step7.TabIndex = 10
        Me.Step7.Visible = False
        '
        'UcError
        '
        Me.UcError.BackColor = System.Drawing.Color.Transparent
        Me.UcError.Location = New System.Drawing.Point(5, 8)
        Me.UcError.m_showFailOnOne = True
        Me.UcError.Name = "UcError"
        Me.UcError.Size = New System.Drawing.Size(430, 86)
        Me.UcError.TabIndex = 4
        '
        'UcBlank
        '
        Me.UcBlank.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.HelpProvider1.SetHelpKeyword(Me.UcBlank, "Single_Report_Schedule.htm#Step6")
        Me.HelpProvider1.SetHelpNavigator(Me.UcBlank, System.Windows.Forms.HelpNavigator.Topic)
        Me.UcBlank.Location = New System.Drawing.Point(8, 96)
        Me.UcBlank.m_customDSN = ""
        Me.UcBlank.m_customPassword = ""
        Me.UcBlank.m_customUserID = ""
        Me.UcBlank.m_ParametersList = Nothing
        Me.UcBlank.Name = "UcBlank"
        Me.HelpProvider1.SetShowHelp(Me.UcBlank, True)
        Me.UcBlank.Size = New System.Drawing.Size(424, 278)
        Me.UcBlank.TabIndex = 3
        '
        'ErrProv
        '
        Me.ErrProv.ContainerControl = Me
        Me.ErrProv.Icon = CType(resources.GetObject("ErrProv.Icon"), System.Drawing.Icon)
        '
        'cmdFinish
        '
        Me.cmdFinish.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdFinish.Image = CType(resources.GetObject("cmdFinish.Image"), System.Drawing.Image)
        Me.cmdFinish.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdFinish.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdFinish.Location = New System.Drawing.Point(357, 477)
        Me.cmdFinish.Name = "cmdFinish"
        Me.cmdFinish.Size = New System.Drawing.Size(75, 25)
        Me.cmdFinish.TabIndex = 50
        Me.cmdFinish.Text = "&Finish"
        Me.cmdFinish.Visible = False
        '
        'Step8
        '
        Me.Step8.BackColor = System.Drawing.SystemColors.Control
        Me.Step8.Controls.Add(Me.GroupBox1)
        Me.Step8.Controls.Add(Me.UcTasks)
        Me.Step8.Location = New System.Drawing.Point(0, 72)
        Me.Step8.Name = "Step8"
        Me.Step8.Size = New System.Drawing.Size(440, 377)
        Me.Step8.TabIndex = 0
        Me.Step8.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.optOnce)
        Me.GroupBox1.Controls.Add(Me.optAll)
        Me.GroupBox1.Location = New System.Drawing.Point(14, 300)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(414, 74)
        Me.GroupBox1.TabIndex = 18
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Run custom tasks..."
        '
        'optOnce
        '
        Me.optOnce.AutoSize = True
        Me.optOnce.Location = New System.Drawing.Point(6, 46)
        Me.optOnce.Name = "optOnce"
        Me.optOnce.Size = New System.Drawing.Size(162, 17)
        Me.optOnce.TabIndex = 1
        Me.optOnce.Tag = "1"
        Me.optOnce.Text = "Once for the entire schedule"
        Me.optOnce.UseVisualStyleBackColor = True
        '
        'optAll
        '
        Me.optAll.AutoSize = True
        Me.optAll.Checked = True
        Me.optAll.Location = New System.Drawing.Point(6, 20)
        Me.optAll.Name = "optAll"
        Me.optAll.Size = New System.Drawing.Size(179, 17)
        Me.optAll.TabIndex = 0
        Me.optAll.TabStop = True
        Me.optAll.Tag = "0"
        Me.optAll.Text = "Once for each generated report"
        Me.optAll.UseVisualStyleBackColor = True
        '
        'UcTasks
        '
        Me.UcTasks.BackColor = System.Drawing.SystemColors.Control
        Me.UcTasks.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcTasks.ForeColor = System.Drawing.Color.Navy
        Me.HelpProvider1.SetHelpKeyword(Me.UcTasks, "Single_Report_Schedule.htm#Step7")
        Me.HelpProvider1.SetHelpNavigator(Me.UcTasks, System.Windows.Forms.HelpNavigator.Topic)
        Me.UcTasks.Location = New System.Drawing.Point(8, 8)
        Me.UcTasks.m_defaultTaks = False
        Me.UcTasks.m_eventBased = False
        Me.UcTasks.m_eventID = 99999
        Me.UcTasks.Name = "UcTasks"
        Me.HelpProvider1.SetShowHelp(Me.UcTasks, True)
        Me.UcTasks.Size = New System.Drawing.Size(424, 286)
        Me.UcTasks.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.LightCyan
        Me.Panel1.BackgroundImage = Global.sqlrd.My.Resources.Resources.strip
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.lblStep)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(447, 61)
        Me.Panel1.TabIndex = 13
        '
        'Step6
        '
        Me.Step6.Controls.Add(Me.GroupBox5)
        Me.Step6.Location = New System.Drawing.Point(0, 72)
        Me.Step6.Name = "Step6"
        Me.Step6.Size = New System.Drawing.Size(440, 377)
        Me.Step6.TabIndex = 16
        Me.Step6.Visible = False
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label9)
        Me.GroupBox5.Controls.Add(Me.txtSnapshots)
        Me.GroupBox5.Controls.Add(Me.chkSnapshots)
        Me.GroupBox5.Controls.Add(Me.lsvDatasources)
        Me.GroupBox5.Controls.Add(Me.cmdLoginTest)
        Me.GroupBox5.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(424, 336)
        Me.GroupBox5.TabIndex = 0
        Me.GroupBox5.TabStop = False
        '
        'Label9
        '
        Me.HelpProvider1.SetHelpKeyword(Me.Label9, "Single_Report_Schedule.htm#Step5")
        Me.HelpProvider1.SetHelpNavigator(Me.Label9, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(320, 284)
        Me.Label9.Name = "Label9"
        Me.HelpProvider1.SetShowHelp(Me.Label9, True)
        Me.Label9.Size = New System.Drawing.Size(80, 16)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "Days"
        '
        'txtSnapshots
        '
        Me.txtSnapshots.Enabled = False
        Me.HelpProvider1.SetHelpKeyword(Me.txtSnapshots, "Single_Report_Schedule.htm#Step5")
        Me.HelpProvider1.SetHelpNavigator(Me.txtSnapshots, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtSnapshots.Location = New System.Drawing.Point(256, 282)
        Me.txtSnapshots.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.txtSnapshots.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtSnapshots.Name = "txtSnapshots"
        Me.HelpProvider1.SetShowHelp(Me.txtSnapshots, True)
        Me.txtSnapshots.Size = New System.Drawing.Size(56, 21)
        Me.txtSnapshots.TabIndex = 1
        Me.txtSnapshots.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSnapshots.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'chkSnapshots
        '
        Me.HelpProvider1.SetHelpKeyword(Me.chkSnapshots, "Single_Report_Schedule.htm#Step5")
        Me.HelpProvider1.SetHelpNavigator(Me.chkSnapshots, System.Windows.Forms.HelpNavigator.Topic)
        Me.chkSnapshots.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkSnapshots.Location = New System.Drawing.Point(8, 282)
        Me.chkSnapshots.Name = "chkSnapshots"
        Me.HelpProvider1.SetShowHelp(Me.chkSnapshots, True)
        Me.chkSnapshots.Size = New System.Drawing.Size(240, 24)
        Me.chkSnapshots.TabIndex = 0
        Me.chkSnapshots.Text = "Enable snapshots and keep snapshots for"
        '
        'lsvDatasources
        '
        Me.lsvDatasources.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3, Me.ColumnHeader4})
        Me.lsvDatasources.ContextMenuStrip = Me.mnuDatasources
        Me.lsvDatasources.FullRowSelect = True
        Me.HelpProvider1.SetHelpKeyword(Me.lsvDatasources, "Single_Report_Schedule.htm#Step5")
        Me.HelpProvider1.SetHelpNavigator(Me.lsvDatasources, System.Windows.Forms.HelpNavigator.Topic)
        Me.lsvDatasources.HideSelection = False
        Me.lsvDatasources.LargeImageList = Me.ImageList1
        Me.lsvDatasources.Location = New System.Drawing.Point(6, 16)
        Me.lsvDatasources.Name = "lsvDatasources"
        Me.HelpProvider1.SetShowHelp(Me.lsvDatasources, True)
        Me.lsvDatasources.Size = New System.Drawing.Size(405, 263)
        Me.lsvDatasources.SmallImageList = Me.ImageList1
        Me.SuperTooltip1.SetSuperTooltip(Me.lsvDatasources, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Double-click on a datasource to set its login credentials. Leaving a datasource's" & _
                    " credentials not set will result in errors if they are required.", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon))
        Me.lsvDatasources.TabIndex = 3
        Me.lsvDatasources.UseCompatibleStateImageBehavior = False
        Me.lsvDatasources.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Datasource Name"
        Me.ColumnHeader3.Width = 274
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Name"
        Me.ColumnHeader4.Width = 116
        '
        'mnuDatasources
        '
        Me.mnuDatasources.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClearToolStripMenuItem})
        Me.mnuDatasources.Name = "mnuDatasources"
        Me.mnuDatasources.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuDatasources.Size = New System.Drawing.Size(102, 26)
        '
        'ClearToolStripMenuItem
        '
        Me.ClearToolStripMenuItem.Name = "ClearToolStripMenuItem"
        Me.ClearToolStripMenuItem.Size = New System.Drawing.Size(101, 22)
        Me.ClearToolStripMenuItem.Text = "&Clear"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "data_table.png")
        Me.ImageList1.Images.SetKeyName(1, "key1.ico")
        Me.ImageList1.Images.SetKeyName(2, "view.ico")
        '
        'cmdLoginTest
        '
        Me.cmdLoginTest.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdLoginTest, "Single_Report_Schedule.htm#Step5")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdLoginTest, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdLoginTest.Image = CType(resources.GetObject("cmdLoginTest.Image"), System.Drawing.Image)
        Me.cmdLoginTest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdLoginTest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoginTest.Location = New System.Drawing.Point(336, 304)
        Me.cmdLoginTest.Name = "cmdLoginTest"
        Me.HelpProvider1.SetShowHelp(Me.cmdLoginTest, True)
        Me.cmdLoginTest.Size = New System.Drawing.Size(75, 24)
        Me.cmdLoginTest.TabIndex = 2
        Me.cmdLoginTest.Text = "Preview"
        Me.cmdLoginTest.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'mnuContacts
        '
        Me.mnuContacts.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuMAPI, Me.mnuMARS})
        '
        'mnuMAPI
        '
        Me.mnuMAPI.Index = 0
        Me.mnuMAPI.Text = "MAPI Address Book"
        '
        'mnuMARS
        '
        Me.mnuMARS.Index = 1
        Me.mnuMARS.Text = "CRD Address Book"
        '
        'Footer1
        '
        Me.Footer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Footer1.Location = New System.Drawing.Point(0, 455)
        Me.Footer1.Name = "Footer1"
        Me.Footer1.Size = New System.Drawing.Size(464, 16)
        Me.Footer1.TabIndex = 12
        Me.Footer1.TabStop = False
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.DefaultTooltipSettings = New DevComponents.DotNetBar.SuperTooltipInfo("", "", "", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon)
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'DividerLabel1
        '
        Me.DividerLabel1.BackColor = System.Drawing.Color.Transparent
        Me.DividerLabel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(0, 61)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(447, 10)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 17
        '
        'Step1
        '
        Me.Step1.Controls.Add(Me.UcDSN)
        Me.Step1.Controls.Add(Me.chkGroupReports)
        Me.Step1.Controls.Add(Me.btnConnect)
        Me.Step1.Controls.Add(Me.grpQuery)
        Me.Step1.Controls.Add(Me.Label5)
        Me.Step1.Location = New System.Drawing.Point(0, 72)
        Me.Step1.Name = "Step1"
        Me.Step1.Size = New System.Drawing.Size(440, 377)
        Me.Step1.TabIndex = 51
        Me.Step1.Visible = False
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.SystemColors.Control
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN.Location = New System.Drawing.Point(8, 29)
        Me.UcDSN.Name = "UcDSN"
        Me.UcDSN.Size = New System.Drawing.Size(424, 112)
        Me.UcDSN.TabIndex = 5
        '
        'chkGroupReports
        '
        Me.chkGroupReports.AutoSize = True
        Me.chkGroupReports.Checked = True
        Me.chkGroupReports.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkGroupReports.Location = New System.Drawing.Point(20, 356)
        Me.chkGroupReports.Name = "chkGroupReports"
        Me.chkGroupReports.Size = New System.Drawing.Size(335, 17)
        Me.chkGroupReports.TabIndex = 4
        Me.chkGroupReports.Text = "Group reports together by email address (email destination only)"
        Me.chkGroupReports.UseVisualStyleBackColor = True
        '
        'btnConnect
        '
        Me.btnConnect.Location = New System.Drawing.Point(185, 147)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(75, 23)
        Me.btnConnect.TabIndex = 3
        Me.btnConnect.Text = "&Connect"
        Me.btnConnect.UseVisualStyleBackColor = True
        '
        'grpQuery
        '
        Me.grpQuery.Controls.Add(Me.cmbDDKey)
        Me.grpQuery.Controls.Add(Me.Label8)
        Me.grpQuery.Controls.Add(Me.btnBuild)
        Me.grpQuery.Controls.Add(Me.txtQuery)
        Me.grpQuery.Enabled = False
        Me.grpQuery.Location = New System.Drawing.Point(8, 176)
        Me.grpQuery.Name = "grpQuery"
        Me.grpQuery.Size = New System.Drawing.Size(425, 174)
        Me.grpQuery.TabIndex = 2
        Me.grpQuery.TabStop = False
        Me.grpQuery.Text = "Data selection criteria"
        '
        'btnBuild
        '
        Me.btnBuild.Location = New System.Drawing.Point(177, 115)
        Me.btnBuild.Name = "btnBuild"
        Me.btnBuild.Size = New System.Drawing.Size(75, 23)
        Me.btnBuild.TabIndex = 1
        Me.btnBuild.Text = "&Build"
        Me.btnBuild.UseVisualStyleBackColor = True
        '
        'txtQuery
        '
        Me.txtQuery.BackColor = System.Drawing.Color.White
        Me.txtQuery.Location = New System.Drawing.Point(9, 19)
        Me.txtQuery.Multiline = True
        Me.txtQuery.Name = "txtQuery"
        Me.txtQuery.ReadOnly = True
        Me.txtQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtQuery.Size = New System.Drawing.Size(410, 93)
        Me.txtQuery.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(5, 11)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(141, 13)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Database Connection Setup"
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'cmbDDKey
        '
        Me.cmbDDKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDDKey.FormattingEnabled = True
        Me.cmbDDKey.Location = New System.Drawing.Point(81, 145)
        Me.cmbDDKey.Name = "cmbDDKey"
        Me.cmbDDKey.Size = New System.Drawing.Size(171, 21)
        Me.SuperTooltip1.SetSuperTooltip(Me.cmbDDKey, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "This is the column that hold unique values in the recordset. Also known as the PR" & _
                    "IMARY KEY.", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.cmbDDKey.TabIndex = 5
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(9, 149)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(63, 13)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "Key Column"
        '
        'frmRDScheduleWizard
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(447, 507)
        Me.Controls.Add(Me.Step1)
        Me.Controls.Add(Me.Step2)
        Me.Controls.Add(Me.Step5)
        Me.Controls.Add(Me.Step3)
        Me.Controls.Add(Me.Step4)
        Me.Controls.Add(Me.Step7)
        Me.Controls.Add(Me.Step8)
        Me.Controls.Add(Me.Step6)
        Me.Controls.Add(Me.cmdNext)
        Me.Controls.Add(Me.DividerLabel1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.cmdFinish)
        Me.Controls.Add(Me.Footer1)
        Me.Controls.Add(Me.cmdBack)
        Me.Controls.Add(Me.cmdCancel)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.HelpButton = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmRDScheduleWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Data-Driven Schedule Wizard"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step2.ResumeLayout(False)
        Me.Step2.PerformLayout()
        Me.Step3.ResumeLayout(False)
        Me.Step4.ResumeLayout(False)
        Me.GroupBox7.ResumeLayout(False)
        Me.Step5.ResumeLayout(False)
        Me.Step5.PerformLayout()
        Me.Step7.ResumeLayout(False)
        CType(Me.ErrProv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step8.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Step6.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        CType(Me.txtSnapshots, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mnuDatasources.ResumeLayout(False)
        Me.Step1.ResumeLayout(False)
        Me.Step1.PerformLayout()
        Me.grpQuery.ResumeLayout(False)
        Me.grpQuery.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW As Object = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property

    Private ReadOnly Property m_ParametersList() As ArrayList
        Get
            Dim list As ArrayList = New ArrayList

            For Each item As ListViewItem In lsvParameters.Items
                list.Add(item.Text)
            Next

            Return list
        End Get
    End Property

    Private Sub cmdDbLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDbLoc.Click
        Try
            Dim srsVersion As String = clsMarsReport.m_serverVersion(txtUrl.Text, ServerUser, ServerPassword)

            If srsVersion = "2007" Then
                oRpt = New ReportServer_2005.ReportingService2005
            Else
                oRpt = New ReportServer.ReportingService
            End If
            'Dim oServerLogin As frmRemoteLogin = New frmRemoteLogin
            'Dim sReturn As String() = oServerLogin.ServerLogin(ServerUser, ServerPassword)
            'Dim cred As System.Net.NetworkCredential

            'we check to see if we should put reportserver2005.asmx in the URL
            If srsVersion = "2007" Then txtUrl.Text = clsMarsParser.Parser.fixASMXfor2008(txtUrl.Text)

            oRpt.Url = txtUrl.Text

            'If sReturn Is Nothing Then
            'oRpt.Credentials = System.Net.CredentialCache.DefaultCredentials
            'Else
            'ServerUser = sReturn(0)
            'ServerPassword = sReturn(1)

            'cred = New System.Net.NetworkCredential(ServerUser, ServerPassword)

            'oRpt.Credentials = cred
            'End If

            Dim sPath As String

            Dim oServer As frmReportServer = New frmReportServer

            sPath = oServer.newGetReports(oRpt, txtUrl.Text, ServerUser, ServerPassword, txtDBLoc.Text)

            'If Me.txtDBLoc.Text.Length = 0 Then

            '    showTooltip = False

            '    Dim oServerBrowse As frmReportServer = New frmReportServer

            '    AppStatus(True)

            '    sPath = oServerBrowse._GetReport(txtUrl.Text, cred)
            'Else
            '    sPath = txtDBLoc.Text
            'End If

            If sPath IsNot Nothing Then
                txtDBLoc.Text = sPath
                txtName.Text = sPath.Split("/")(sPath.Split("/").GetUpperBound(0))
                cmdNext.Enabled = True

                If txtDBLoc.Text.StartsWith("/") = False Then
                    txtDBLoc.Text = "/" & txtDBLoc.Text
                End If
            Else
                Return
            End If

            Dim sRDLPath As String = clsMarsReport.m_rdltempPath & txtName.Text & ".rdl"
            Dim repDef() As Byte

            If IO.File.Exists(sRDLPath) = True Then
                IO.File.Delete(sRDLPath)
            End If

            Dim fsReport As New System.IO.FileStream(sRDLPath, IO.FileMode.Create)

            repDef = oRpt.GetReportDefinition(txtDBLoc.Text)

            fsReport.Write(repDef, 0, repDef.Length)

            fsReport.Close()

            'get the parameters
            Dim sPars() As String
            Dim sParDefaults As String()
            Dim I As Integer = 0

            m_serverParametersTable = clsMarsReport.oReport.getserverReportParameters(Me.txtUrl.Text, Me.txtDBLoc.Text, ServerUser, _
                        ServerPassword, sRDLPath)

            sPars = clsMarsReport._GetParameterFields(sRDLPath)
            sParDefaults = clsMarsReport._GetParameterValues(sRDLPath)


            'm_serverParametersTable.WriteXml(sAppPath & "m_serverParametersTable.xml")

            lsvParameters.Items.Clear()

            ParDefaults = New Hashtable

            If sPars IsNot Nothing Then
                For Each s As String In sPars
                    Dim oItem As ListViewItem = lsvParameters.Items.Add(s.Split("|")(0))

                    oItem.Tag = s.Split("|")(1)

                    Try
                        oItem.SubItems(1).Text = ""
                    Catch ex As Exception
                        oItem.SubItems.Add("")
                    End Try

                    Try
                        oItem.SubItems(2).Text = s.Split("|")(2)
                    Catch
                        oItem.SubItems.Add(s.Split("|")(2))
                    End Try

                    ParDefaults.Add(oItem.Text, sParDefaults(I))

                    I += 1
                Next
            End If

            UcDest.m_ParameterList = Me.m_ParametersList
            UcBlank.m_ParametersList = Me.m_ParametersList

            lsvDatasources.Items.Clear()

            Dim sData As ArrayList = New ArrayList

            sData = clsMarsReport._GetDatasources(sRDLPath)

            If sData IsNot Nothing Then
                lsvDatasources.Items.Clear()

                For Each s As Object In sData
                    Dim oItem As ListViewItem = lsvDatasources.Items.Add(s)

                    oItem.SubItems.Add("default")

                    oItem.ImageIndex = 0
                    oItem.Tag = 0
                Next
            End If

            txtDesc.Text = clsMarsReport._GetReportDescription(sRDLPath)

            IO.File.Delete(sRDLPath)

            txtName.Focus()
            txtName.SelectAll()

            cmdNext.Enabled = True
            'save the Url as it has been validated
            logUrlHistory(Me.txtUrl.Text)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), _
            "Please check your report server's web service URL and try again")

            If ex.Message.ToLower.Contains("cannot be found") Then
                txtDBLoc.Text = ""
                txtName.Text = ""
                txtDesc.Text = ""
            End If
        End Try
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click

        Dim oCleaner As clsMarsData = New clsMarsData
        HasCancelled = True
        oCleaner.CleanDB()
        Me.Close()
    End Sub


    Private Sub lsvParameters_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvParameters.DoubleClick
        Dim I As Integer = 1

        If lsvParameters.Items.Count = 0 Then
            MessageBox.Show("This report does not have any parameters", Application.ProductName, _
            MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return
        End If

        Dim oPar As New frmParameters

        Dim Row As DataRow

        dtParameters = New DataTable("Parameters")

        With dtParameters
            .Columns.Add("ParName")
            .Columns.Add("ParValue")
            .Columns.Add("ParType")
            .Columns.Add("MultiValue")
        End With

        For Each item As ListViewItem In lsvParameters.Items
            Row = dtParameters.NewRow

            With Row
                .Item("ParName") = item.Text
                Try
                    .Item("ParValue") = item.SubItems(1).Text
                Catch
                    .Item("ParValue") = String.Empty
                End Try

                .Item("ParType") = item.Tag

                Try
                    .Item("MultiValue") = item.SubItems(2).Text
                Catch ex As Exception
                    .Item("MultiValue") = "false"
                End Try
            End With

            dtParameters.Rows.Add(Row)
        Next

        dtParameters = oPar.SetParameters(dtParameters, ParDefaults, , lsvParameters.SelectedItems(0).Text, m_serverParametersTable, txtUrl.Text, Me.txtDBLoc.Text, ServerUser, ServerPassword)

        If dtParameters Is Nothing Then Return

        lsvParameters.Items.Clear()

        For Each dataRow As DataRow In dtParameters.Rows
            Dim oItem As ListViewItem = New ListViewItem

            oItem.Text = dataRow.Item("ParName")

            Try
                oItem.SubItems(1).Text = dataRow.Item("ParValue")
            Catch
                oItem.SubItems.Add(dataRow.Item("ParValue"))
            End Try

            Try
                oItem.SubItems(2).Text = dataRow.Item("MultiValue")
            Catch ex As Exception
                oItem.SubItems.Add(dataRow.Item("MultiValue"))
            End Try

            oItem.Tag = dataRow.Item("ParType")

            lsvParameters.Items.Add(oItem)

        Next
    End Sub

    Private Sub frmSingleScheduleWizard_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If HasCancelled = False Then
            cmdCancel_Click(Nothing, Nothing)
        End If
    End Sub



    Private Sub frmSingleScheduleWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        HelpProvider1.HelpNamespace = gHelpPath
        Dim oData As New clsMarsData

        oData.CleanDB()

        Dim I As Int32 = 0

        If _CheckScheduleCount() = False Then
            Close()
            Return
        End If


        FormatForWinXP(Me)

        nStep = 1
        Step1.Visible = True
        Step2.Visible = False
        Step3.Visible = False
        Step4.Visible = False
        Step5.Visible = False
        Step6.Visible = False
        Step7.Visible = False
        Step8.Visible = False
        Step1.BringToFront()
        txtFolder.Focus()

        lblStep.Text = S1

        UcSet.EndDate.Value = Now.AddYears(100)

        If gParentID > 0 And gParent.Length > 0 Then
            txtFolder.Text = gParent
            txtFolder.Tag = gParentID
        End If

        txtUrl.Text = clsMarsUI.MainUI.ReadRegistry("ReportsLoc", "")

        If txtUrl.Text.Length = 0 Then
            txtUrl.Text = "http://myReportServer/ReportServer/Reportservice.asmx"
            txtUrl.ForeColor = Color.Gray
        End If

        'set up auto-complete data
        With Me.txtUrl
            .AutoCompleteMode = AutoCompleteMode.SuggestAppend
            .AutoCompleteSource = AutoCompleteSource.CustomSource
            .AutoCompleteCustomSource = getUrlHistory()
        End With

        txtUrl.ContextMenu = Me.mnuInserter
        txtDBLoc.ContextMenu = Me.mnuInserter
    End Sub

    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click

        Dim oData As New clsMarsData

        Select Case nStep
            Case 1
                If txtQuery.Text.Length = 0 Then
                    ErrProv.SetError(txtQuery, "Please create your data selection query")
                    txtQuery.Focus()
                    Return
                ElseIf Me.cmbDDKey.Text = "" Then
                    ErrProv.SetError(Me.cmbDDKey, "Please select the key column")
                    Me.cmbDDKey.Focus()
                    Return
                End If

                clsMarsReport.populateDataDrivenCache(txtQuery.Text, Me.UcDSN.m_conString, True)

                'check if the selected key column contains unique values only. Going to do this by reading from the data cache

                For Each row As DataRow In clsMarsReport.m_dataDrivenCache.Rows
                    Dim dupKey As String = IsNull(row(cmbDDKey.Text), "")
                    Dim rows() As DataRow

                    'use select to get all the rows where the key column matches the dupKey
                    rows = clsMarsReport.m_dataDrivenCache.Select(Me.cmbDDKey.Text & " = '" & dupKey & "'")

                    If rows IsNot Nothing Then
                        If rows.Length > 1 Then
                            ErrProv.SetError(cmbDDKey, "The selected key column contains duplicate values of '" & dupKey & "'. Please select a column that contains UNIQUE values only.")
                            cmbDDKey.Focus()
                            Return
                        End If
                    End If
                Next

                Step1.Visible = False
                Step2.Visible = True
                Step2.BringToFront()
                lblStep.Text = S2
                cmdBack.Enabled = True
            Case 2
                If txtName.Text = "" Then
                    ErrProv.SetError(txtName, "Please provide a name for this schedule")
                    txtName.Focus()
                    Exit Sub
                ElseIf txtFolder.Text = "" Then
                    ErrProv.SetError(cmdLoc, "Please select the destination folder")
                    cmdLoc.Focus()
                    Exit Sub
                ElseIf txtDBLoc.Text = "" Then
                    ErrProv.SetError(cmdDbLoc, "Please select the access database for the report")
                    cmdDbLoc.Focus()
                    Exit Sub
                ElseIf clsMarsUI.candoRename(txtName.Text, Me.txtFolder.Tag, clsMarsScheduler.enScheduleType.REPORT, , , True) = False Then
                    ErrProv.SetError(txtName, "A Data-Driven schedule " & _
                    "already exist with this name")
                    txtName.Focus()
                    Return
                End If

                Step2.Visible = False
                Step3.Visible = True
                UcSet.Focus()
                lblStep.Text = S3
            Case 3

                If UcSet.ValidateSchedule = False Then
                    Return
                End If

                Step3.Visible = False
                Step4.Visible = True
                lblStep.Text = S4
            Case 4
                If UcDest.lsvDestination.Items.Count = 0 Or UcDest.lsvDestination.CheckedItems.Count = 0 Then
                    ErrProv.SetError(UcDest.lsvDestination, "Please set up a destination")
                    Return
                End If

                Step4.Visible = False
                Step5.Visible = True
                cmdParameters.Focus()
                lblStep.Text = S5
            Case 5

                For Each oItem As ListViewItem In lsvParameters.Items
                    Try
                        If oItem.SubItems(1).Text.Length = 0 Then
                            Dim oRes As DialogResult = _
                            MessageBox.Show("One or more parameters has been " & _
                            "left blank. Edit the parameters?", _
                            Application.ProductName, MessageBoxButtons.YesNo, _
                            MessageBoxIcon.Question)

                            If oRes = DialogResult.Yes Then
                                Button1_Click(sender, e)
                                Return
                            End If
                        End If
                    Catch
                        Dim oRes As DialogResult = _
                        MessageBox.Show("One or more parameters has been " & _
                        "left blank. Edit the parameters?", _
                        Application.ProductName, MessageBoxButtons.YesNo, _
                        MessageBoxIcon.Question)

                        If oRes = DialogResult.Yes Then
                            Button1_Click(sender, e)
                            Return
                        End If
                    End Try
                Next

                Step5.Visible = False
                Step6.Visible = True
                lblStep.Text = S6
            Case 6

                Step6.Visible = False
                Step7.Visible = True
                Step7.BringToFront()
                UcError.cmbRetry.Focus()
                lblStep.Text = S7
            Case 7


                If UcBlank.ValidateEntries = False Then Return

                Step7.Visible = False
                Step8.Visible = True
                lblStep.Text = S8
                cmdNext.Visible = False
                cmdFinish.Visible = True
                Me.AcceptButton = cmdFinish
            Case Else
                Exit Sub
        End Select
        nStep += 1
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        oErr.ResetError(sender, ErrProv)
        UcDest.sReportTitle = txtName.Text
    End Sub

    Private Sub cmdLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoc.Click
        Dim oFolders As frmFolders = New frmFolders
        Dim sFolder(1) As String

        sFolder = oFolders.GetFolder

        If sFolder(0) <> "" And sFolder(0) <> "Desktop" Then
            txtFolder.Text = sFolder(0)
            txtFolder.Tag = sFolder(1)
        End If
    End Sub



    Private Sub txtFolder_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFolder.TextChanged
        oErr.ResetError(sender, ErrProv)
    End Sub



    Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBack.Click
        Select Case nStep
            Case 8
                Step8.Visible = False
                Step7.Visible = True
                Step7.BringToFront()
                lblStep.Text = S7
                cmdNext.Visible = True
                cmdFinish.Visible = False
                Me.AcceptButton = cmdNext
            Case 7
                Step7.Visible = False
                Step6.Visible = True
                lblStep.Text = S6
                Step6.BringToFront()
            Case 6
                Step6.Visible = False
                Step5.Visible = True
                Step5.BringToFront()
                lblStep.Text = S5

            Case 5
                Step5.Visible = False
                Step4.Visible = True
                lblStep.Text = S4
                Step4.BringToFront()
            Case 4
                Step4.Visible = False
                Step3.Visible = True
                Step3.BringToFront()
                lblStep.Text = S3
            Case 3
                Step3.Visible = False
                Step2.Visible = True
                Step2.BringToFront()
                txtFolder.Focus()
                lblStep.Text = S2
            Case 2
                Step2.Visible = False
                Step1.Visible = True
                Step1.BringToFront()
                lblStep.Text = S1
                cmdBack.Enabled = False
            Case Else
                Exit Sub
        End Select

        nStep -= 1
    End Sub

    Private Sub cmdFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFinish.Click
        On Error GoTo Trap
        Dim nReportID As Int32
        Dim sPrinters As String = ""
        Dim lsv As ListViewItem
        Dim WriteSuccess As Boolean
        Dim nInclude As Integer
        Dim sCols As String
        Dim sVals As String
        Dim oReport As New clsMarsReport
        Dim dynamicTasks As Integer

        cmdFinish.Enabled = False

        If optAll.Checked = True Then
            dynamicTasks = 0
        Else
            dynamicTasks = 1
        End If

        oErr.BusyProgress(10, "Validating user data...")

        'if everything is in order then lets save the report

        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData

        If UcSet.chkNoEnd.Checked = True Then
            UcSet.EndDate.Value = Now.AddYears(1000)
        End If

        oErr.BusyProgress(30, "Saving report data...")

        nReportID = clsMarsData.CreateDataID("ReportAttr", "ReportID")

        sCols = "ReportID,PackID,DatabasePath,ReportName," & _
            "Parent,ReportTitle," & _
            "Retry,AssumeFail,CheckBlank," & _
            "SelectionFormula,Owner,RptUserID,RptPassword,RptServer,RptDatabase,UseLogin," & _
            "UseSavedData,CachePath,LastRefreshed,AutoRefresh,Bursting,Dynamic,RptDatabaseType," & _
            "IsDataDriven,DynamicTasks,AutoCalc,RetryInterval"

        sVals = nReportID & ",0," & _
            "'" & SQLPrepare(txtUrl.Text) & "'," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            txtFolder.Tag & "," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            UcError.cmbRetry.Value & "," & _
            UcError.m_autoFailAfter(True) & "," & _
            Convert.ToInt32(UcBlank.chkBlankReport.Checked) & "," & _
            "'" & SQLPrepare(txtFormula.Text) & "'," & _
            "'" & gUser & "'," & _
            "'" & SQLPrepare(ServerUser) & "'," & _
            "'" & SQLPrepare(_EncryptDBValue(ServerPassword)) & "'," & _
            "''," & _
            "''," & _
            0 & "," & _
            0 & "," & _
            "'" & SQLPrepare(txtDBLoc.Text) & "', '" & ConDateTime(Date.Now) & "'," & _
            0 & "," & _
            "0,0," & _
            "'', " & _
            1 & "," & _
            dynamicTasks & "," & _
            Convert.ToInt32(UcError.chkAutoCalc.Checked) & "," & _
            UcError.txtRetryInterval.Value

        SQL = "INSERT INTO ReportAttr(" & sCols & ") VALUES (" & sVals & ")"

        WriteSuccess = clsMarsData.WriteData(SQL)

        If WriteSuccess = True Then
            Dim ScheduleID As Integer = clsMarsData.CreateDataID("ScheduleAttr", "ScheduleID")

            oErr.BusyProgress(50, "Saving schedule data...")

            'save the schedule
            With UcSet
                SQL = "INSERT INTO ScheduleAttr(ScheduleID,Frequency,StartDate,EndDate,NextRun," & _
                    "StartTime,Repeat,RepeatInterval," & _
                    "RepeatUntil,Status,ReportID,Description,KeyWord,CalendarName,UseException,ExceptionCalendar,RepeatUnit) " & _
                    "VALUES(" & _
                    ScheduleID & "," & _
                    "'" & .sFrequency & "'," & _
                    "'" & ConDateTime(CTimeZ(.StartDate.Value, dateConvertType.WRITE)) & "'," & _
                    "'" & ConDateTime(CTimeZ(.EndDate.Value, dateConvertType.WRITE)) & "'," & _
                    "'" & ConDate(CTimeZ(.StartDate.Value.Date, dateConvertType.WRITE)) & " " & ConTime(CTimeZ(.RunAt.Value, dateConvertType.WRITE)) & "'," & _
                    "'" & CTimeZ(.RunAt.Value.ToShortTimeString, dateConvertType.WRITE) & "'," & _
                    Convert.ToInt32(.chkRepeat.Checked) & "," & _
                    "'" & Convert.ToString(.cmbRpt.Value).Replace(",", ".") & "'," & _
                    "'" & CTimeZ(.RepeatUntil.Value.ToShortTimeString, dateConvertType.WRITE) & "'," & _
                    Convert.ToInt32(.chkStatus.Checked) & "," & _
                    nReportID & "," & _
                    "'" & SQLPrepare(txtDesc.Text) & "'," & _
                    "'" & SQLPrepare(txtKeyWord.Text) & "'," & _
                    "'" & SQLPrepare(.cmbCustom.Text) & "'," & _
                    Convert.ToInt32(.chkException.Checked) & "," & _
                    "'" & SQLPrepare(.cmbException.Text) & "'," & _
                    "'" & UcSet.m_RepeatUnit & "')"
            End With

            WriteSuccess = clsMarsData.WriteData(SQL)

            clsMarsData.WriteData("UPDATE ScheduleOptions SET " & _
            "ScheduleID = " & ScheduleID & " WHERE ScheduleID = 99999")

            If WriteSuccess = False Then
                clsMarsData.WriteData("DELETE FROM ReportAttr WHERE ReportID = " & nReportID & " AND PackID = 0")
                Exit Sub
            End If

            'save the destination data for the report
            UcDest.CommitDeletions()

            oErr.BusyProgress(75, "Saving destination data...")

            SQL = "UPDATE DestinationAttr SET ReportID =" & nReportID & "," & _
            "PackID = 0, SmartID = 0 WHERE ReportID = 99999"

            WriteSuccess = clsMarsData.WriteData(SQL)

            If WriteSuccess = False Then
                clsMarsData.WriteData("DELETE FROM ScheduleAttr WHERE ReportID = " & nReportID & " AND PackID = 0")
                clsMarsData.WriteData("DELETE FROM ReportAttr WHERE ReportID = " & nReportID & " AND PackID = 0")
                Exit Sub
            End If

            'save the data driven information
            Dim conString As String = UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & _EncryptDBValue(UcDSN.txtPassword.Text)

            SQL = "INSERT INTO DataDrivenAttr (DDID,ReportID,ConString,SQLQuery,FailOnOne,GroupReports,KeyColumn) " & _
            "VALUES (" & _
            clsMarsData.CreateDataID("datadrivenattr", "ddid") & "," & _
            nReportID & "," & _
            "'" & SQLPrepare(conString) & "'," & _
            "'" & SQLPrepare(txtQuery.Text) & "'," & _
            Convert.ToInt32(UcError.chkFailonOne.Checked) & "," & _
            Convert.ToInt32(Me.chkGroupReports.Checked) & "," & _
            "'" & SQLPrepare(Me.cmbDDKey.Text) & "')"

            clsMarsData.WriteData(SQL)

            oErr.BusyProgress(90, "Saving report parameters...")

            'save the parameters
            SQL = "UPDATE ReportParameter SET ReportID =" & nReportID & " WHERE " & _
            "ReportID = 99999"
            clsMarsData.WriteData(SQL)

            UcTasks.CommitDeletions()

            SQL = "UPDATE Tasks SET ScheduleID = " & ScheduleID & " WHERE ScheduleID = 99999"
            clsMarsData.WriteData(SQL)

            'update subreport parameters
            SQL = "UPDATE SubReportParameters SET ReportID =" & nReportID & " WHERE ReportID = 99999"

            clsMarsData.WriteData(SQL)

            SQL = "UPDATE SubReportLogin SET ReportID = " & nReportID & " WHERE ReportID = 99999"

            clsMarsData.WriteData(SQL)

            'save report tables
            SQL = "UPDATE ReportTable SET ReportID = " & nReportID & " WHERE ReportID = 99999"

            clsMarsData.WriteData(SQL)

            SQL = "UPDATE SubReportTable SET ReportID = " & nReportID & " WHERE ReportID = 99999"

            clsMarsData.WriteData(SQL)

            SQL = "UPDATE ReportDatasource SET ReportID = " & nReportID & " WHERE ReportID = 99999"

            clsMarsData.WriteData(SQL)

            For Each item As ListViewItem In lsvDatasources.Items
                SQL = "SELECT * FROM ReportDatasource WHERE ReportID = " & nReportID & " AND " & _
                "DatasourceName = '" & SQLPrepare(item.Text) & "'"

                Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

                If oRs IsNot Nothing Then
                    If oRs.EOF = True Then
                        sCols = "DatasourceID,ReportID,DatasourceName,RptUserID,RptPassword"
                        sVals = clsMarsData.CreateDataID("ReportDatasource", "DatasourceID") & "," & _
                        nReportID & "," & _
                        "'" & SQLPrepare(item.Text) & "'," & _
                        "'default'," & _
                        "'<default>'"

                        SQL = "INSERT INTO ReportDatasource (" & sCols & ") VALUES (" & sVals & ")"

                        clsMarsData.WriteData(SQL)
                    End If

                    oRs.Close()
                End If
            Next

            'save blank report alert information if available
            If UcBlank.chkBlankReport.Checked = True Then
                Dim sBlankType As String = ""

                If UcBlank.optAlert.Checked = True Then
                    sBlankType = "Alert"
                ElseIf UcBlank.optAlertandReport.Checked = True Then
                    sBlankType = "AlertandReport"
                ElseIf UcBlank.optIgnore.Checked = True Then
                    sBlankType = "Ignore"
                End If

                oErr.BusyProgress(95, "Cleaning up...")

                SQL = "INSERT INTO BlankReportAlert(BlankID,[Type],[To],Msg,ReportID,[Subject],CustomQueryDetails) VALUES (" & _
                clsMarsData.CreateDataID("blankreportalert", "blankid") & "," & _
                "'" & sBlankType & "'," & _
                "'" & UcBlank.txtAlertTo.Text.Replace("'", "''") & "'," & _
                "'" & UcBlank.txtBlankMsg.Text.Replace("'", "''") & "'," & _
                nReportID & "," & _
                "'" & SQLPrepare(UcBlank.txtSubject.Text) & "'," & _
                "'" & SQLPrepare(UcBlank.m_CustomBlankQuery) & "')"

                clsMarsData.WriteData(SQL)
            End If

            'save any snapshots details if any
            If chkSnapshots.Checked = True Then
                sCols = "SnapID,ReportID,KeepSnap"

                sVals = clsMarsData.CreateDataID("reportsnapshots", "snapid") & "," & _
                nReportID & "," & _
                txtSnapshots.Value

                SQL = "INSERT INTO ReportSnapshots (" & sCols & ") VALUES " & _
                "(" & sVals & ")"

                clsMarsData.WriteData(SQL)
            End If
        End If

        clsMarsData.WriteData("UPDATE ReportOptions SET ReportID =0 WHERE ReportID =99999")

        If gRole.ToLower <> "administrator" Then
            Dim oUser As New clsMarsUsers

            oUser.AssignView(nReportID, gUser, clsMarsUsers.enViewType.ViewSingle)
        End If


        _Delay(2)

        On Error Resume Next

        oErr.RefreshView(oWindow(nWindowCurrent))

        Me.Close()
        oErr.BusyProgress(, , True)

        On Error Resume Next

        Dim nCount As Integer
        nCount = txtFolder.Text.Split("\").GetUpperBound(0)

        Dim oTree As TreeView = oWindow(nWindowCurrent).tvFolders

        oErr.FindNode("Folder:" & txtFolder.Tag, oTree, oTree.Nodes(0))

        oWindow(nWindowCurrent).lsvWindow.Focus()

        For Each item As ListViewItem In oWindow(nWindowCurrent).lsvWindow.Items
            If item.Text = txtName.Text Then
                item.Selected = True
                item.Focused = True
            Else
                item.Selected = False
                item.Focused = False
            End If
        Next

        clsMarsReport.m_dataDrivenCache = Nothing

        clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.SINGLES, clsMarsAudit.AuditAction.CREATE)

        Exit Sub
Trap:
        cmdFinish.Enabled = True

    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdParameters.Click
        Dim I As Integer = 1

        If lsvParameters.Items.Count = 0 Then
            MessageBox.Show("This report does not have any parameters", Application.ProductName, _
            MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return
        End If

        Dim oPar As New frmParameters

        Dim Row As DataRow

        dtParameters = New DataTable("Parameters")

        With dtParameters
            .Columns.Add("ParName")
            .Columns.Add("ParValue")
            .Columns.Add("ParType")
            .Columns.Add("MultiValue")
        End With

        For Each item As ListViewItem In lsvParameters.Items
            Row = dtParameters.NewRow

            With Row
                .Item("ParName") = item.Text
                Try
                    .Item("ParValue") = item.SubItems(1).Text
                Catch
                    .Item("ParValue") = String.Empty
                End Try

                .Item("ParType") = item.Tag.ToString.Split("|")(0)

                Try
                    .Item("MultiValue") = item.SubItems(2).Text
                Catch
                    .Item("MultiValue") = "false"
                End Try
            End With

            dtParameters.Rows.Add(Row)
        Next

        dtParameters = oPar.SetParameters(dtParameters, ParDefaults, , , m_serverParametersTable, txtUrl.Text, Me.txtDBLoc.Text, ServerUser, ServerPassword)

        If dtParameters Is Nothing Then Return

        lsvParameters.Items.Clear()

        For Each dataRow As DataRow In dtParameters.Rows
            Dim oItem As ListViewItem = New ListViewItem

            oItem.Text = dataRow.Item("ParName")

            Try
                oItem.SubItems(1).Text = dataRow.Item("ParValue")
            Catch
                oItem.SubItems.Add(dataRow.Item("ParValue"))
            End Try

            Try
                oItem.SubItems(2).Text = dataRow.Item("MultiValue")
            Catch
                oItem.SubItems.Add(dataRow.Item("MultiValue"))
            End Try

            oItem.Tag = dataRow.Item("ParType")

            lsvParameters.Items.Add(oItem)
        Next

    End Sub


    Private Sub chkSnapshots_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSnapshots.CheckedChanged
        If chkSnapshots.Checked = True Then
            If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.sa8_Snapshots) Then
                _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
                chkSnapshots.Checked = False
                Return
            End If
        End If

        txtSnapshots.Enabled = chkSnapshots.Checked
    End Sub


    Private Sub cmdLoginTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoginTest.Click
        Dim rv As Microsoft.Reporting.WinForms.ReportViewer
        Dim actualUrl As String = ""

        Try
            AppStatus(True)

            oRpt = New ReportServer.ReportingService
            rv = New Microsoft.Reporting.WinForms.ReportViewer

            With oRpt
                Dim sUrl As String = clsMarsParser.Parser.ParseString(txtUrl.Text)
                .Url = sUrl

                For x As Integer = 0 To sUrl.Split("/").GetUpperBound(0) - 1
                    actualUrl &= sUrl.Split("/")(x) & "/"
                Next

                rv.ProcessingMode = ProcessingMode.Remote
                rv.ServerReport.ReportServerUrl = New Uri(actualUrl)

                If ServerPassword.Length = 0 And ServerUser.Length = 0 Then
                    .Credentials = System.Net.CredentialCache.DefaultCredentials
                    rv.ServerReport.ReportServerCredentials.NetworkCredentials = System.Net.CredentialCache.DefaultCredentials
                Else
                    Dim oCred As System.Net.NetworkCredential = New System.Net.NetworkCredential(ServerUser, ServerPassword)

                    .Credentials = oCred

                    rv.ServerReport.ReportServerCredentials.NetworkCredentials = oCred
                End If
            End With

            Dim serverVersion As String = "2000"

            Try
                serverVersion = rv.ServerReport.GetServerVersion
                rv.ServerReport.ReportPath = Me.txtDBLoc.Text
            Catch ex As Exception
                serverVersion = "2000"
                rv = Nothing
            End Try

            Dim oPar() As ReportServer.ParameterValue = Nothing
            Dim reportPars() As Microsoft.Reporting.WinForms.ReportParameter = Nothing
            Dim oDataLogins() As ReportServer.DataSourceCredentials = Nothing
            Dim reportDS() As Microsoft.Reporting.WinForms.DataSourceCredentials
            Dim I As Integer = 0
            Dim oData As New clsMarsData

            'set the parameters
            Dim oParse As New clsMarsParser

            For Each oItem As ListViewItem In lsvParameters.Items
                If oItem.SubItems(1).Text.ToLower = "[sql-rddefault]" Then Continue For

                ReDim Preserve oPar(I)
                ReDim Preserve reportPars(I)

                oPar(I) = New ReportServer.ParameterValue
                oPar(I).Name = oItem.Text

                reportPars(I) = New Microsoft.Reporting.WinForms.ReportParameter
                reportPars(I).Name = oItem.Text

                If oItem.SubItems(1).Text.ToLower = "[sql-rdnull]" Then
                    oPar(I).Value = Nothing
                Else
                    oPar(I).Value = oParse.ParseString(oItem.SubItems(1).Text)

                    For Each s As String In oItem.SubItems(1).Text.Split("|")
                        If s.Length > 0 Then reportPars(I).Values.Add(oParse.ParseString(s))
                    Next
                End If


                I += 1
            Next

            If rv IsNot Nothing And reportPars IsNot Nothing Then rv.ServerReport.SetParameters(reportPars)

            I = 0

            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM ReportDatasource WHERE ReportID = " & 99999)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    ReDim Preserve oDataLogins(I)
                    ReDim Preserve reportDS(I)

                    oDataLogins(I) = New ReportServer.DataSourceCredentials
                    reportDS(I) = New Microsoft.Reporting.WinForms.DataSourceCredentials

                    With oDataLogins(I)
                        .DataSourceName = oRs("datasourcename").Value
                        .UserName = oRs("rptuserid").Value
                        .Password = _DecryptDBValue(IsNull(oRs("rptpassword").Value))
                    End With

                    With reportDS(I)
                        .Name = oRs("datasourcename").Value
                        .UserId = oRs("rptuserid").Value
                        .Password = _DecryptDBValue(IsNull(oRs("rptpassword").Value))
                    End With

                    I += 1
                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            If rv IsNot Nothing And reportDS IsNot Nothing Then rv.ServerReport.SetDataSourceCredentials(reportDS)

            If serverVersion = "2000" Then
                Dim warnings As ReportServer.Warning() = Nothing
                Dim streamIDs As String() = Nothing

                Dim oResult As Byte()

                oResult = oRpt.Render(Me.txtDBLoc.Text, "PDF", Nothing, "", _
                    oPar, oDataLogins, Nothing, "", "", Nothing, _
                    warnings, streamIDs)

                Dim sFileName As String = sAppPath & clsMarsData.CreateDataID & ".pdf"

                'write the bytes to disk
                With System.IO.File.Create(sFileName, oResult.Length)
                    .Write(oResult, 0, oResult.Length)
                    .Close()
                End With

                'view the exported file
                Dim oView As New frmPreview

                AppStatus(False)

                oView.PreviewReport(sFileName, txtDBLoc.Text, True)

                Try : IO.File.Delete(sFileName) : Catch : End Try
            Else
                Dim oView As New frmPreview

                oView.PreviewReport(rv, txtDBLoc.Text, True)
            End If
        Catch ex As Exception
            AppStatus(False)
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace), "Please check your datasource credentials and try again")
        End Try
    End Sub

    Private Sub cmbGroupColumns_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ErrProv.SetError(sender, "")
    End Sub

    Private Sub lsvDatasources_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvDatasources.DoubleClick
        Dim oSet As New frmSetTableLogin
        Dim oItem As ListViewItem
        Dim sValues() As String

        If lsvDatasources.SelectedItems.Count = 0 Then Return

        oItem = lsvDatasources.SelectedItems(0)

        sValues = oSet.SaveLoginInfo(99999, oItem.Text, oItem.Tag)

        If Not sValues Is Nothing Then
            oItem.SubItems(1).Text = sValues(0)
            oItem.Tag = sValues(1)
        End If
    End Sub



    Private Sub txtUrl_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUrl.GotFocus
        If txtUrl.ForeColor = Color.Gray Then
            txtUrl.Text = ""
            txtUrl.ForeColor = Color.Blue
        End If

       
    End Sub


    Private Sub ClearToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClearToolStripMenuItem.Click

        If lsvDatasources.SelectedItems.Count = 0 Then Return
        Dim oData As New clsMarsData

        Dim oItem As ListViewItem = lsvDatasources.SelectedItems(0)

        Dim nDataID As Integer = oItem.Tag

        If nDataID > 0 Then
            If clsMarsData.WriteData("DELETE FROM ReportDatasource WHERE DatasourceID =" & nDataID) = True Then
                oItem.SubItems(1).Text = "default"
            End If
        End If


    End Sub

    Private Sub lsvDatasources_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lsvDatasources.KeyUp
        If e.KeyCode = Keys.Enter Then
            Me.lsvDatasources_DoubleClick(sender, e)
        End If
    End Sub


    Private Sub txtDBLoc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If txtDBLoc.Text.Length > 0 And showTooltip = True Then
            Dim info As New DevComponents.DotNetBar.SuperTooltipInfo
            Dim msg As String

            msg = "Please be aware that by entering the report path manually, SQL-RD will not " & _
            "connect to your report server to list your reports. " & vbCrLf & _
            "If you would like SQL-RD to " & _
            "connect to the report server and list your reports, remove all text from this field."

            With info
                .Color = DevComponents.DotNetBar.eTooltipColor.Lemon
                .CustomSize = New System.Drawing.Size(400, 100)
                .BodyText = msg
                .HeaderText = "For your information:"
                .HeaderVisible = True
                .FooterVisible = False
            End With

            Me.SuperTooltip1.SetSuperTooltip(Me.cmdDbLoc, info)

            SuperTooltip1.ShowTooltip(Me.cmdDbLoc)

            Me.cmdDbLoc.Image = Me.ImageList1.Images(1)
            Me.cmdDbLoc.ImageAlign = ContentAlignment.MiddleCenter

            showTooltip = False
        ElseIf Me.txtDBLoc.Text.Length = 0 Then
            SuperTooltip1.SetSuperTooltip(Me.cmdDbLoc, Nothing)

            Me.cmdDbLoc.Image = ImageList1.Images(2)
            Me.cmdDbLoc.ImageAlign = ContentAlignment.MiddleCenter

            showTooltip = True
        End If

        cmdNext.Enabled = False
    End Sub

    Private Sub btnConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnect.Click
        If UcDSN._Validate = True Then
            Me.grpQuery.Enabled = True

            If Me.txtQuery.Text = "" Then
                Me.btnBuild_Click(sender, e)
            End If
        End If
    End Sub

    Private Sub btnBuild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuild.Click
        Dim queryBuilder As frmDynamicParameter = New frmDynamicParameter

        Dim sVals(1) As String
        Dim sCon As String = ""

        If UcDSN.cmbDSN.Text.Length > 0 Then
            sCon = UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & UcDSN.txtPassword.Text & "|"
        End If

        queryBuilder.m_showStar = True
        queryBuilder.Label1.Text = "Please select the table that holds the required data"

        sVals = queryBuilder.ReturnSQLQuery(sCon, txtQuery.Text)

        If sVals Is Nothing Then Return

        sCon = sVals(0)
        txtQuery.Text = sVals(1)

        UcDSN.cmbDSN.Text = sCon.Split("|")(0)
        UcDSN.txtUserID.Text = sCon.Split("|")(1)
        UcDSN.txtPassword.Text = sCon.Split("|")(2)

        Dim oRs As ADODB.Recordset = New ADODB.Recordset
        Dim oCon As ADODB.Connection = New ADODB.Connection

        oCon.Open(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

        oRs.Open(txtQuery.Text, oCon, ADODB.CursorTypeEnum.adOpenStatic)

        Dim I As Integer = 0

        Me.cmbDDKey.Items.Clear()

        Me.m_DataFields = New Hashtable

        For Each fld As ADODB.Field In oRs.Fields
            Me.m_DataFields.Add(I, fld.Name)

            Me.cmbDDKey.Items.Add(fld.Name)

            I += 1
        Next

        oRs.Close()
        oCon.Close()

        oRs = Nothing
        oCon = Nothing

        If Me.txtQuery.Text <> "" Then cmdNext.Enabled = True Else cmdNext.Enabled = False
    End Sub
    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.Undo()
        Catch : End Try
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.Cut()
        Catch : End Try
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.Copy()
        Catch : End Try
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.Paste()
        Catch : End Try
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.SelectedText = ""
        Catch : End Try
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.SelectAll()
        Catch : End Try
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim oInsert As New frmInserter(99999)
        oInsert.m_EventBased = False
        oInsert.GetConstants(Me)
    End Sub

    Private Sub chkGroupReports_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkGroupReports.CheckedChanged
        Me.UcDest.disableEmbed = chkGroupReports.Checked
    End Sub

  
End Class
