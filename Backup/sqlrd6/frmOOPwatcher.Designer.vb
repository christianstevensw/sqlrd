<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOOPwatcher
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim GlvColumn1 As GlacialComponents.Controls.GlacialListView.GLVColumn = New GlacialComponents.Controls.GlacialListView.GLVColumn
        Dim GlvColumn2 As GlacialComponents.Controls.GlacialListView.GLVColumn = New GlacialComponents.Controls.GlacialListView.GLVColumn
        Dim GlvColumn3 As GlacialComponents.Controls.GlacialListView.GLVColumn = New GlacialComponents.Controls.GlacialListView.GLVColumn
        Dim GlvColumn4 As GlacialComponents.Controls.GlacialListView.GLVColumn = New GlacialComponents.Controls.GlacialListView.GLVColumn

        Me.components = New System.ComponentModel.Container
        Me.mnuKiller = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.TerminateProcessToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SelectAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.tmCheck = New System.Windows.Forms.Timer(Me.components)
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip
        Me.tmExecutor = New System.Windows.Forms.Timer(Me.components)
        Me.bgExecutor = New System.ComponentModel.BackgroundWorker
        Me.bgCleaner = New System.ComponentModel.BackgroundWorker
        Me.gList = New GlacialComponents.Controls.GlacialListView.GlacialListView
        Me.mnuKiller.SuspendLayout()
        Me.SuspendLayout()
        '
        'mnuKiller
        '
        Me.mnuKiller.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TerminateProcessToolStripMenuItem, Me.SelectAllToolStripMenuItem})
        Me.mnuKiller.Name = "mnuKiller"
        Me.mnuKiller.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuKiller.Size = New System.Drawing.Size(157, 48)
        '
        'TerminateProcessToolStripMenuItem
        '
        Me.TerminateProcessToolStripMenuItem.Name = "TerminateProcessToolStripMenuItem"
        Me.TerminateProcessToolStripMenuItem.Size = New System.Drawing.Size(156, 22)
        Me.TerminateProcessToolStripMenuItem.Text = "Cancel Execution"
        '
        'SelectAllToolStripMenuItem
        '
        Me.SelectAllToolStripMenuItem.Name = "SelectAllToolStripMenuItem"
        Me.SelectAllToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.SelectAllToolStripMenuItem.Size = New System.Drawing.Size(156, 22)
        Me.SelectAllToolStripMenuItem.Text = "Select All"
        Me.SelectAllToolStripMenuItem.Visible = False
        '
        'tmCheck
        '
        Me.tmCheck.Interval = 500
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.DefaultTooltipSettings = New DevComponents.DotNetBar.SuperTooltipInfo("Tip", "", "", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon)
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        Me.SuperTooltip1.TooltipDuration = 30
        '
        'tmExecutor
        '
        Me.tmExecutor.Interval = 1000
        '
        'bgExecutor
        '
        '
        'bgCleaner
        '
        '
        'gList
        '
        GlvColumn1.ImageIndex = -1
        GlvColumn1.Name = "Column1"
        GlvColumn1.Text = "Name"
        GlvColumn2.ImageIndex = -1
        GlvColumn2.Name = "Column2"
        GlvColumn2.Text = "Type"
        GlvColumn3.ImageIndex = -1
        GlvColumn3.Name = "Column3"
        GlvColumn3.Text = "Status"
        GlvColumn3.Width = 150
        GlvColumn4.EmbeddedControlType = GlacialComponents.Controls.GLVCommon.GEmbeddedControlTypes.ColumnTemplate
        GlvColumn4.EmbeddedDisplayConditions = GlacialComponents.Controls.GLVCommon.GEmbeddedDisplayConditions.AlwaysVisible
        GlvColumn4.ImageIndex = -1
        GlvColumn4.Name = "Column4"
        GlvColumn4.Text = ""
        GlvColumn4.Width = 170
        Me.gList.Columns.AddRange(New GlacialComponents.Controls.GlacialListView.GLVColumn() {GlvColumn1, GlvColumn2, GlvColumn3, GlvColumn4})
        Me.gList.ContextMenuStrip = Me.mnuKiller
        Me.gList.ControlStyle = GlacialComponents.Controls.GlacialListView.GLVControlStyles.XP
        Me.gList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gList.HideSelection = True
        Me.gList.HorizontalGridLineSize = 0
        Me.gList.HotTrackingColor = System.Drawing.SystemColors.HotTrack
        Me.gList.Location = New System.Drawing.Point(0, 0)
        Me.gList.MultiSelect = True
        Me.gList.Name = "gList"
        Me.gList.SelectedTextColor = System.Drawing.SystemColors.HighlightText
        Me.gList.SelectionColor = System.Drawing.SystemColors.Highlight
        Me.gList.Size = New System.Drawing.Size(524, 211)
        Me.gList.SnapLastColumn = True
        Me.gList.TabIndex = 1
        Me.gList.Text = "GlacialListView1"
        Me.gList.UnfocusedSelectionColor = System.Drawing.SystemColors.Control
        Me.gList.VerticalGridLineSize = 0
        '
        'frmOOPwatcher
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(524, 211)
        Me.ControlBox = False
        Me.Controls.Add(Me.gList)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmOOPwatcher"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.SuperTooltip1.SetSuperTooltip(Me, New DevComponents.DotNetBar.SuperTooltipInfo("Tip", "", "Right-click a schedule to cancel it.", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, True, False, New System.Drawing.Size(0, 0)))
        Me.Text = "Process Watcher                                                                  " & _
            "        Right-click a schedule to cancel it."
        Me.mnuKiller.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tmCheck As System.Windows.Forms.Timer
    Friend WithEvents mnuKiller As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents TerminateProcessToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents SelectAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tmExecutor As System.Windows.Forms.Timer
    Friend WithEvents bgExecutor As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgCleaner As System.ComponentModel.BackgroundWorker
    Friend WithEvents gList As GlacialComponents.Controls.GlacialListView.GlacialListView
End Class
