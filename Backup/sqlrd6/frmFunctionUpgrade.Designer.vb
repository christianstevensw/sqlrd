<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFunctionUpgrade
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFunctionUpgrade))
        Me.Page1 = New System.Windows.Forms.Panel
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtUpgradeKey = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Page2 = New System.Windows.Forms.Panel
        Me.lsvFeatures = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.btnBack = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnFinish = New System.Windows.Forms.Button
        Me.btnNext = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblStep = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Super = New DevComponents.DotNetBar.SuperTooltip
        Me.DividerLabel2 = New sqlrd.DividerLabel
        Me.DividerLabel1 = New sqlrd.DividerLabel
        Me.Page1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Page2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Page1
        '
        Me.Page1.Controls.Add(Me.TableLayoutPanel1)
        Me.Page1.Location = New System.Drawing.Point(3, 83)
        Me.Page1.Name = "Page1"
        Me.Page1.Size = New System.Drawing.Size(475, 251)
        Me.Page1.TabIndex = 1
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.65625!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 77.34375!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtUpgradeKey, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(14, 14)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 27.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 73.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(453, 100)
        Me.TableLayoutPanel1.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 73)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Upgrade Key"
        '
        'txtUpgradeKey
        '
        Me.txtUpgradeKey.Location = New System.Drawing.Point(105, 30)
        Me.txtUpgradeKey.Multiline = True
        Me.txtUpgradeKey.Name = "txtUpgradeKey"
        Me.txtUpgradeKey.Size = New System.Drawing.Size(343, 67)
        Me.txtUpgradeKey.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(105, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(321, 26)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Please enter the Upgrade Key that has been provided to you. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "TIP: Use Copy and P" & _
            "aste to avoid errors in entering the long key."
        '
        'Page2
        '
        Me.Page2.Controls.Add(Me.lsvFeatures)
        Me.Page2.Location = New System.Drawing.Point(3, 83)
        Me.Page2.Name = "Page2"
        Me.Page2.Size = New System.Drawing.Size(475, 251)
        Me.Page2.TabIndex = 3
        '
        'lsvFeatures
        '
        Me.lsvFeatures.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.lsvFeatures.BackColor = System.Drawing.Color.White
        Me.lsvFeatures.CheckBoxes = True
        Me.lsvFeatures.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvFeatures.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvFeatures.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lsvFeatures.ForeColor = System.Drawing.Color.Navy
        Me.lsvFeatures.FullRowSelect = True
        Me.lsvFeatures.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lsvFeatures.HotTracking = True
        Me.lsvFeatures.HoverSelection = True
        Me.lsvFeatures.Location = New System.Drawing.Point(0, 0)
        Me.lsvFeatures.Name = "lsvFeatures"
        Me.lsvFeatures.Size = New System.Drawing.Size(475, 251)
        Me.lsvFeatures.TabIndex = 1
        Me.lsvFeatures.UseCompatibleStateImageBehavior = False
        Me.lsvFeatures.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Width = 300
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "check.png")
        Me.ImageList1.Images.SetKeyName(1, "delete.png")
        '
        'btnBack
        '
        Me.btnBack.Enabled = False
        Me.btnBack.Image = Global.sqlrd.My.Resources.Resources.arrow_left_green
        Me.btnBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBack.Location = New System.Drawing.Point(239, 350)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(75, 28)
        Me.btnBack.TabIndex = 7
        Me.btnBack.Text = "&Back"
        Me.btnBack.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(320, 350)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 28)
        Me.btnCancel.TabIndex = 6
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnFinish
        '
        Me.btnFinish.Image = Global.sqlrd.My.Resources.Resources.check2
        Me.btnFinish.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnFinish.Location = New System.Drawing.Point(401, 350)
        Me.btnFinish.Name = "btnFinish"
        Me.btnFinish.Size = New System.Drawing.Size(75, 28)
        Me.btnFinish.TabIndex = 5
        Me.btnFinish.Text = "&Finish"
        Me.btnFinish.UseVisualStyleBackColor = True
        Me.btnFinish.Visible = False
        '
        'btnNext
        '
        Me.btnNext.Image = Global.sqlrd.My.Resources.Resources.arrow_right_green
        Me.btnNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNext.Location = New System.Drawing.Point(401, 350)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(75, 28)
        Me.btnNext.TabIndex = 5
        Me.btnNext.Text = "&Next"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BackgroundImage = Global.sqlrd.My.Resources.Resources.strip
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.lblStep)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(479, 70)
        Me.Panel1.TabIndex = 0
        '
        'lblStep
        '
        Me.lblStep.BackColor = System.Drawing.Color.Transparent
        Me.lblStep.Font = New System.Drawing.Font("Tahoma", 15.75!)
        Me.lblStep.ForeColor = System.Drawing.Color.Navy
        Me.lblStep.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblStep.Location = New System.Drawing.Point(12, 21)
        Me.lblStep.Name = "lblStep"
        Me.lblStep.Size = New System.Drawing.Size(360, 32)
        Me.lblStep.TabIndex = 4
        Me.lblStep.Text = "Step 1: Report Setup"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = Global.sqlrd.My.Resources.Resources.transform2
        Me.PictureBox1.Location = New System.Drawing.Point(422, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(54, 50)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Super
        '
        Me.Super.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Super.DefaultTooltipSettings = New DevComponents.DotNetBar.SuperTooltipInfo("", "", "", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0))
        Me.Super.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.Super.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        Me.Super.PositionBelowControl = False
        '
        'DividerLabel2
        '
        Me.DividerLabel2.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel2.Location = New System.Drawing.Point(0, 337)
        Me.DividerLabel2.Name = "DividerLabel2"
        Me.DividerLabel2.Size = New System.Drawing.Size(501, 18)
        Me.DividerLabel2.Spacing = 0
        Me.DividerLabel2.TabIndex = 4
        Me.DividerLabel2.Text = "ChristianSteven Software"
        '
        'DividerLabel1
        '
        Me.DividerLabel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(0, 70)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(479, 10)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 2
        '
        'frmFunctionUpgrade
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(479, 381)
        Me.ControlBox = False
        Me.Controls.Add(Me.Page2)
        Me.Controls.Add(Me.Page1)
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.btnBack)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnFinish)
        Me.Controls.Add(Me.DividerLabel2)
        Me.Controls.Add(Me.DividerLabel1)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmFunctionUpgrade"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SQL-RD Feature Upgrade Wizard"
        Me.Page1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.Page2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblStep As System.Windows.Forms.Label
    Friend WithEvents Page1 As System.Windows.Forms.Panel
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtUpgradeKey As System.Windows.Forms.TextBox
    Friend WithEvents Page2 As System.Windows.Forms.Panel
    Friend WithEvents lsvFeatures As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents DividerLabel2 As sqlrd.DividerLabel
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnBack As System.Windows.Forms.Button
    Friend WithEvents btnFinish As System.Windows.Forms.Button
    Friend WithEvents Super As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
