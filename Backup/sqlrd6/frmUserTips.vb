Public Class frmUserTips
    Dim currentVal As Integer = Nothing
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Close()
    End Sub

    Private Function GetTipNumber(ByVal maxValue As Integer, Optional ByVal currentValue As Integer = Nothing) As Integer
        Dim randomValue As Integer
        Dim randomClass As Random = New Random

        randomValue = randomClass.Next(0, maxValue)

        Try
            Int32.Parse(currentValue)

            If currentValue = randomvalue Then
                Do
                    randomValue = randomClass.Next(0, maxValue)
                Loop Until currentValue <> randomvalue
            End If
        Catch : End Try

        currentVal = randomValue

        Return randomValue
    End Function

    Public Sub ShowTips()

        If IO.File.Exists(sAppPath & "\tips.xml") = False Then Return

        Me.txtTip.Text = GetTip()
        Me.txtTip.SelectionStart = 0
        Me.txtTip.SelectionLength = 0
        Me.Height = 306
        Me.Show()
    End Sub

    Private Function GetTip() As String
        Dim tipsFile As String
        Dim tips() As String
        Dim tip As String

        tipsFile = sqlrd.MarsCommon.ReadTextFromFile(sAppPath & "\tips.xml")

        tips = tipsFile.Split("|")

        tip = tips(GetTipNumber(tips.GetUpperBound(0), currentVal))

        Return tip
    End Function

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        txtTip.Text = GetTip()
    End Sub

    Private Sub frmUserTips_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub chkTips_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTips.CheckedChanged
        clsMarsUI.MainUI.SaveRegistry("ShowTips", Convert.ToInt32(chkTips.Checked))
    End Sub
End Class