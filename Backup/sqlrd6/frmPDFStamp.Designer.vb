<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPDFStamp
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPDFStamp))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.grpGraphic = New System.Windows.Forms.GroupBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtImageLoc = New System.Windows.Forms.TextBox
        Me.btnImage = New System.Windows.Forms.Button
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.grpText = New System.Windows.Forms.GroupBox
        Me.lblSample = New System.Windows.Forms.Label
        Me.btnFont = New System.Windows.Forms.Button
        Me.txtFont = New System.Windows.Forms.TextBox
        Me.txtCaption = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.cmbHorizontalPos = New System.Windows.Forms.ComboBox
        Me.cmbVerticalPos = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtAngle = New System.Windows.Forms.NumericUpDown
        Me.cmbType = New System.Windows.Forms.ComboBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.optText = New System.Windows.Forms.RadioButton
        Me.optGraphic = New System.Windows.Forms.RadioButton
        Me.fontDlg = New System.Windows.Forms.FontDialog
        Me.colorDlg = New System.Windows.Forms.ColorDialog
        Me.openDlg = New System.Windows.Forms.OpenFileDialog
        Me.mnuInserter = New System.Windows.Forms.ContextMenu
        Me.mnuUndo = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.mnuCut = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuPaste = New System.Windows.Forms.MenuItem
        Me.mnuDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.mnuDatabase = New System.Windows.Forms.MenuItem
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip
        Me.GroupBox1.SuspendLayout()
        Me.grpGraphic.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpText.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.txtAngle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.grpGraphic)
        Me.GroupBox1.Controls.Add(Me.grpText)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.optText)
        Me.GroupBox1.Controls.Add(Me.optGraphic)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 5)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(415, 398)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'grpGraphic
        '
        Me.grpGraphic.Controls.Add(Me.Label5)
        Me.grpGraphic.Controls.Add(Me.txtImageLoc)
        Me.grpGraphic.Controls.Add(Me.btnImage)
        Me.grpGraphic.Controls.Add(Me.PictureBox1)
        Me.grpGraphic.Location = New System.Drawing.Point(6, 43)
        Me.grpGraphic.Name = "grpGraphic"
        Me.grpGraphic.Size = New System.Drawing.Size(399, 245)
        Me.grpGraphic.TabIndex = 1
        Me.grpGraphic.TabStop = False
        Me.grpGraphic.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 20)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(56, 13)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Image File"
        '
        'txtImageLoc
        '
        Me.txtImageLoc.Location = New System.Drawing.Point(71, 17)
        Me.txtImageLoc.Name = "txtImageLoc"
        Me.txtImageLoc.Size = New System.Drawing.Size(255, 21)
        Me.SuperTooltip1.SetSuperTooltip(Me.txtImageLoc, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "The path to the image file to be used as the stamp", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.txtImageLoc.TabIndex = 2
        '
        'btnImage
        '
        Me.btnImage.Image = Global.sqlrd.My.Resources.Resources.view
        Me.btnImage.Location = New System.Drawing.Point(339, 17)
        Me.btnImage.Name = "btnImage"
        Me.btnImage.Size = New System.Drawing.Size(50, 21)
        Me.btnImage.TabIndex = 1
        Me.btnImage.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PictureBox1.Location = New System.Drawing.Point(71, 45)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(255, 194)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'grpText
        '
        Me.grpText.Controls.Add(Me.lblSample)
        Me.grpText.Controls.Add(Me.btnFont)
        Me.grpText.Controls.Add(Me.txtFont)
        Me.grpText.Controls.Add(Me.txtCaption)
        Me.grpText.Controls.Add(Me.Label3)
        Me.grpText.Controls.Add(Me.Label1)
        Me.grpText.Location = New System.Drawing.Point(6, 43)
        Me.grpText.Name = "grpText"
        Me.grpText.Size = New System.Drawing.Size(399, 245)
        Me.grpText.TabIndex = 2
        Me.grpText.TabStop = False
        '
        'lblSample
        '
        Me.lblSample.Location = New System.Drawing.Point(56, 65)
        Me.lblSample.Name = "lblSample"
        Me.lblSample.Size = New System.Drawing.Size(251, 174)
        Me.lblSample.TabIndex = 4
        Me.lblSample.Text = "The quick brown fox jumps over the lazy dog. 1234567890"
        Me.lblSample.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnFont
        '
        Me.btnFont.Location = New System.Drawing.Point(316, 41)
        Me.btnFont.Name = "btnFont"
        Me.btnFont.Size = New System.Drawing.Size(51, 21)
        Me.btnFont.TabIndex = 3
        Me.btnFont.Text = "..."
        Me.btnFont.UseVisualStyleBackColor = True
        '
        'txtFont
        '
        Me.txtFont.Location = New System.Drawing.Point(56, 41)
        Me.txtFont.Name = "txtFont"
        Me.txtFont.ReadOnly = True
        Me.txtFont.Size = New System.Drawing.Size(251, 21)
        Me.SuperTooltip1.SetSuperTooltip(Me.txtFont, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Configure the font for the stamp", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.txtFont.TabIndex = 1
        '
        'txtCaption
        '
        Me.txtCaption.Location = New System.Drawing.Point(56, 14)
        Me.txtCaption.Name = "txtCaption"
        Me.txtCaption.Size = New System.Drawing.Size(311, 21)
        Me.SuperTooltip1.SetSuperTooltip(Me.txtCaption, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Enter the text to be used for the stamp", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.txtCaption.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 45)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Font"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Caption"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmbHorizontalPos)
        Me.GroupBox2.Controls.Add(Me.cmbVerticalPos)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.txtAngle)
        Me.GroupBox2.Controls.Add(Me.cmbType)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 294)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(399, 97)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        '
        'cmbHorizontalPos
        '
        Me.cmbHorizontalPos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbHorizontalPos.FormattingEnabled = True
        Me.cmbHorizontalPos.Items.AddRange(New Object() {"Left", "Center", "Right"})
        Me.cmbHorizontalPos.Location = New System.Drawing.Point(107, 67)
        Me.cmbHorizontalPos.Name = "cmbHorizontalPos"
        Me.cmbHorizontalPos.Size = New System.Drawing.Size(129, 21)
        Me.SuperTooltip1.SetSuperTooltip(Me.cmbHorizontalPos, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Specify the horizontal position of the stamp", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.cmbHorizontalPos.TabIndex = 4
        Me.cmbHorizontalPos.Tag = "donotsort"
        '
        'cmbVerticalPos
        '
        Me.cmbVerticalPos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbVerticalPos.FormattingEnabled = True
        Me.cmbVerticalPos.Items.AddRange(New Object() {"Top", "Middle", "Bottom"})
        Me.cmbVerticalPos.Location = New System.Drawing.Point(107, 40)
        Me.cmbVerticalPos.Name = "cmbVerticalPos"
        Me.cmbVerticalPos.Size = New System.Drawing.Size(131, 21)
        Me.SuperTooltip1.SetSuperTooltip(Me.cmbVerticalPos, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Specify where the vertical position of the stamp", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.cmbVerticalPos.TabIndex = 4
        Me.cmbVerticalPos.Tag = "donotsort"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 17)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(31, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Type"
        '
        'txtAngle
        '
        Me.txtAngle.Location = New System.Drawing.Point(284, 13)
        Me.txtAngle.Maximum = New Decimal(New Integer() {360, 0, 0, 0})
        Me.txtAngle.Name = "txtAngle"
        Me.txtAngle.Size = New System.Drawing.Size(51, 21)
        Me.SuperTooltip1.SetSuperTooltip(Me.txtAngle, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "The angle of the stamp (0-360 degrees)", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.txtAngle.TabIndex = 2
        Me.txtAngle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cmbType
        '
        Me.cmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbType.FormattingEnabled = True
        Me.cmbType.Items.AddRange(New Object() {"Overlay", "Underlay"})
        Me.cmbType.Location = New System.Drawing.Point(71, 13)
        Me.cmbType.Name = "cmbType"
        Me.cmbType.Size = New System.Drawing.Size(167, 21)
        Me.SuperTooltip1.SetSuperTooltip(Me.cmbType, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Specify whether the stamp appears over or under the document objects", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.cmbType.TabIndex = 4
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(9, 71)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(95, 13)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Horizontal Position"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(9, 44)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(82, 13)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Vertical Position"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(244, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(34, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Angle"
        '
        'optText
        '
        Me.optText.AutoSize = True
        Me.optText.Checked = True
        Me.optText.Location = New System.Drawing.Point(6, 20)
        Me.optText.Name = "optText"
        Me.optText.Size = New System.Drawing.Size(47, 17)
        Me.optText.TabIndex = 1
        Me.optText.TabStop = True
        Me.optText.Text = "Text"
        Me.optText.UseVisualStyleBackColor = True
        '
        'optGraphic
        '
        Me.optGraphic.AutoSize = True
        Me.optGraphic.Location = New System.Drawing.Point(101, 20)
        Me.optGraphic.Name = "optGraphic"
        Me.optGraphic.Size = New System.Drawing.Size(61, 17)
        Me.optGraphic.TabIndex = 0
        Me.optGraphic.Text = "Graphic"
        Me.optGraphic.UseVisualStyleBackColor = True
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(429, 12)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 2
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(429, 41)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 3
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.DefaultTooltipSettings = New DevComponents.DotNetBar.SuperTooltipInfo("", "", "", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0))
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        '
        'frmPDFStamp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(508, 411)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmPDFStamp"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "PDF Stamp Details"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.grpGraphic.ResumeLayout(False)
        Me.grpGraphic.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpText.ResumeLayout(False)
        Me.grpText.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.txtAngle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents grpText As System.Windows.Forms.GroupBox
    Friend WithEvents txtAngle As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtCaption As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents optText As System.Windows.Forms.RadioButton
    Friend WithEvents optGraphic As System.Windows.Forms.RadioButton
    Friend WithEvents cmbVerticalPos As System.Windows.Forms.ComboBox
    Friend WithEvents cmbType As System.Windows.Forms.ComboBox
    Friend WithEvents btnFont As System.Windows.Forms.Button
    Friend WithEvents txtFont As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents fontDlg As System.Windows.Forms.FontDialog
    Friend WithEvents colorDlg As System.Windows.Forms.ColorDialog
    Friend WithEvents grpGraphic As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtImageLoc As System.Windows.Forms.TextBox
    Friend WithEvents btnImage As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents openDlg As System.Windows.Forms.OpenFileDialog
    Friend WithEvents lblSample As System.Windows.Forms.Label
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmbHorizontalPos As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
End Class
