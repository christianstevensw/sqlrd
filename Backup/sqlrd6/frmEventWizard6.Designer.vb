<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEventWizard6
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEventWizard6))
        Me.pnlTop = New System.Windows.Forms.Panel
        Me.lblStep = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Step1 = New System.Windows.Forms.Panel
        Me.btnBrowse = New System.Windows.Forms.Button
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtDescription = New System.Windows.Forms.TextBox
        Me.txtKeyword = New System.Windows.Forms.TextBox
        Me.txtName = New System.Windows.Forms.TextBox
        Me.txtLocation = New System.Windows.Forms.TextBox
        Me.Step2 = New System.Windows.Forms.Panel
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel
        Me.chkStatus = New System.Windows.Forms.CheckBox
        Me.UcConditions1 = New sqlrd.ucConditions
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.Label1 = New System.Windows.Forms.Label
        Me.cmbAnyAll = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Step3 = New System.Windows.Forms.Panel
        Me.RadioButton2 = New System.Windows.Forms.RadioButton
        Me.optNone = New System.Windows.Forms.RadioButton
        Me.optExisting = New System.Windows.Forms.RadioButton
        Me.Step4 = New System.Windows.Forms.Panel
        Me.lsvSchedules = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.imgFolders = New System.Windows.Forms.ImageList(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.btnRemove = New System.Windows.Forms.Button
        Me.btnAdd = New System.Windows.Forms.Button
        Me.ExpandableSplitter1 = New DevComponents.DotNetBar.ExpandableSplitter
        Me.tvSchedules = New System.Windows.Forms.TreeView
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.Step5 = New System.Windows.Forms.Panel
        Me.UcReports1 = New sqlrd.ucReports
        Me.Step7 = New System.Windows.Forms.Panel
        Me.UcTasks1 = New sqlrd.ucTasks
        Me.cmdNext = New System.Windows.Forms.Button
        Me.cmdFinish = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdBack = New System.Windows.Forms.Button
        Me.Step8 = New System.Windows.Forms.Panel
        Me.Label3 = New System.Windows.Forms.Label
        Me.ucFlow = New sqlrd.ucExecutionPath
        Me.Step6 = New System.Windows.Forms.Panel
        Me.cmbPriority = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.DividerLabel5 = New sqlrd.DividerLabel
        Me.cmbOpHrs = New System.Windows.Forms.ComboBox
        Me.chkOpHrs = New System.Windows.Forms.CheckBox
        Me.DividerLabel4 = New sqlrd.DividerLabel
        Me.DividerLabel3 = New sqlrd.DividerLabel
        Me.UcError = New sqlrd.ucErrorHandler
        Me.mnuParameters = New System.Windows.Forms.ContextMenu
        Me.mnuParameter = New System.Windows.Forms.MenuItem
        Me.DividerLabel2 = New sqlrd.DividerLabel
        Me.DividerLabel1 = New sqlrd.DividerLabel
        Me.pnlTop.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step1.SuspendLayout()
        Me.Step2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Step3.SuspendLayout()
        Me.Step4.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step5.SuspendLayout()
        Me.Step7.SuspendLayout()
        Me.Step8.SuspendLayout()
        Me.Step6.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlTop
        '
        Me.pnlTop.BackColor = System.Drawing.Color.White
        Me.pnlTop.BackgroundImage = Global.sqlrd.My.Resources.Resources.strip
        Me.pnlTop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pnlTop.Controls.Add(Me.lblStep)
        Me.pnlTop.Controls.Add(Me.PictureBox1)
        Me.pnlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTop.Location = New System.Drawing.Point(0, 0)
        Me.pnlTop.Name = "pnlTop"
        Me.pnlTop.Size = New System.Drawing.Size(436, 69)
        Me.pnlTop.TabIndex = 0
        '
        'lblStep
        '
        Me.lblStep.BackColor = System.Drawing.Color.Transparent
        Me.lblStep.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep.ForeColor = System.Drawing.Color.Navy
        Me.lblStep.Location = New System.Drawing.Point(12, 28)
        Me.lblStep.Name = "lblStep"
        Me.lblStep.Size = New System.Drawing.Size(360, 23)
        Me.lblStep.TabIndex = 1
        Me.lblStep.Text = "Label1"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(378, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(46, 50)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Step1
        '
        Me.Step1.Controls.Add(Me.btnBrowse)
        Me.Step1.Controls.Add(Me.Label8)
        Me.Step1.Controls.Add(Me.Label7)
        Me.Step1.Controls.Add(Me.Label6)
        Me.Step1.Controls.Add(Me.Label5)
        Me.Step1.Controls.Add(Me.txtDescription)
        Me.Step1.Controls.Add(Me.txtKeyword)
        Me.Step1.Controls.Add(Me.txtName)
        Me.Step1.Controls.Add(Me.txtLocation)
        Me.Step1.Location = New System.Drawing.Point(0, 84)
        Me.Step1.Name = "Step1"
        Me.Step1.Size = New System.Drawing.Size(436, 273)
        Me.Step1.TabIndex = 0
        '
        'btnBrowse
        '
        Me.btnBrowse.Image = CType(resources.GetObject("btnBrowse.Image"), System.Drawing.Image)
        Me.btnBrowse.Location = New System.Drawing.Point(329, 68)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(43, 21)
        Me.btnBrowse.TabIndex = 1
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(9, 227)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(54, 13)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "Keywords"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(9, 92)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(60, 13)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Description"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(9, 52)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(93, 13)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Schedule Location"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 12)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Schedule Name"
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(12, 108)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(360, 116)
        Me.txtDescription.TabIndex = 3
        '
        'txtKeyword
        '
        Me.txtKeyword.Location = New System.Drawing.Point(12, 243)
        Me.txtKeyword.Name = "txtKeyword"
        Me.txtKeyword.Size = New System.Drawing.Size(360, 21)
        Me.txtKeyword.TabIndex = 4
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(12, 28)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(360, 21)
        Me.txtName.TabIndex = 0
        '
        'txtLocation
        '
        Me.txtLocation.BackColor = System.Drawing.Color.White
        Me.txtLocation.Location = New System.Drawing.Point(12, 68)
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.ReadOnly = True
        Me.txtLocation.Size = New System.Drawing.Size(301, 21)
        Me.txtLocation.TabIndex = 2
        '
        'Step2
        '
        Me.Step2.Controls.Add(Me.FlowLayoutPanel1)
        Me.Step2.Controls.Add(Me.UcConditions1)
        Me.Step2.Controls.Add(Me.TableLayoutPanel1)
        Me.Step2.Location = New System.Drawing.Point(0, 84)
        Me.Step2.Name = "Step2"
        Me.Step2.Size = New System.Drawing.Size(436, 273)
        Me.Step2.TabIndex = 4
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.chkStatus)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 246)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(436, 27)
        Me.FlowLayoutPanel1.TabIndex = 3
        '
        'chkStatus
        '
        Me.chkStatus.AutoSize = True
        Me.chkStatus.Checked = True
        Me.chkStatus.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkStatus.Location = New System.Drawing.Point(3, 3)
        Me.chkStatus.Name = "chkStatus"
        Me.chkStatus.Size = New System.Drawing.Size(64, 17)
        Me.chkStatus.TabIndex = 0
        Me.chkStatus.Text = "Enabled"
        Me.chkStatus.UseVisualStyleBackColor = True
        '
        'UcConditions1
        '
        Me.UcConditions1.Dock = System.Windows.Forms.DockStyle.Top
        Me.UcConditions1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UcConditions1.Location = New System.Drawing.Point(0, 26)
        Me.UcConditions1.m_EventID = 99999
        Me.UcConditions1.m_OrderID = 0
        Me.UcConditions1.Name = "UcConditions1"
        Me.UcConditions1.Size = New System.Drawing.Size(436, 217)
        Me.UcConditions1.TabIndex = 1
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.23622!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.11927!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.78899!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.cmbAnyAll, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 2, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(436, 26)
        Me.TableLayoutPanel1.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(37, 26)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Fulfill"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbAnyAll
        '
        Me.cmbAnyAll.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmbAnyAll.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbAnyAll.FormattingEnabled = True
        Me.cmbAnyAll.Items.AddRange(New Object() {"ANY", "ALL"})
        Me.cmbAnyAll.Location = New System.Drawing.Point(47, 3)
        Me.cmbAnyAll.Name = "cmbAnyAll"
        Me.cmbAnyAll.Size = New System.Drawing.Size(64, 21)
        Me.cmbAnyAll.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.Location = New System.Drawing.Point(125, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(195, 26)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "of the following conditions"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Step3
        '
        Me.Step3.Controls.Add(Me.RadioButton2)
        Me.Step3.Controls.Add(Me.optNone)
        Me.Step3.Controls.Add(Me.optExisting)
        Me.Step3.Location = New System.Drawing.Point(0, 84)
        Me.Step3.Name = "Step3"
        Me.Step3.Size = New System.Drawing.Size(436, 273)
        Me.Step3.TabIndex = 5
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Checked = True
        Me.RadioButton2.Location = New System.Drawing.Point(13, 9)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(87, 17)
        Me.RadioButton2.TabIndex = 1
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "New Reports"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'optNone
        '
        Me.optNone.AutoSize = True
        Me.optNone.Location = New System.Drawing.Point(13, 55)
        Me.optNone.Name = "optNone"
        Me.optNone.Size = New System.Drawing.Size(50, 17)
        Me.optNone.TabIndex = 0
        Me.optNone.Text = "None"
        Me.optNone.UseVisualStyleBackColor = True
        '
        'optExisting
        '
        Me.optExisting.AutoSize = True
        Me.optExisting.Location = New System.Drawing.Point(13, 32)
        Me.optExisting.Name = "optExisting"
        Me.optExisting.Size = New System.Drawing.Size(112, 17)
        Me.optExisting.TabIndex = 0
        Me.optExisting.Text = "Existing schedules"
        Me.optExisting.UseVisualStyleBackColor = True
        '
        'Step4
        '
        Me.Step4.Controls.Add(Me.lsvSchedules)
        Me.Step4.Controls.Add(Me.Panel1)
        Me.Step4.Controls.Add(Me.ExpandableSplitter1)
        Me.Step4.Controls.Add(Me.tvSchedules)
        Me.Step4.Location = New System.Drawing.Point(0, 84)
        Me.Step4.Name = "Step4"
        Me.Step4.Size = New System.Drawing.Size(436, 273)
        Me.Step4.TabIndex = 6
        '
        'lsvSchedules
        '
        Me.lsvSchedules.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvSchedules.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvSchedules.LargeImageList = Me.imgFolders
        Me.lsvSchedules.Location = New System.Drawing.Point(227, 0)
        Me.lsvSchedules.Name = "lsvSchedules"
        Me.lsvSchedules.Size = New System.Drawing.Size(209, 273)
        Me.lsvSchedules.SmallImageList = Me.imgFolders
        Me.lsvSchedules.TabIndex = 0
        Me.lsvSchedules.UseCompatibleStateImageBehavior = False
        Me.lsvSchedules.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Schedules"
        Me.ColumnHeader1.Width = 200
        '
        'imgFolders
        '
        Me.imgFolders.ImageStream = CType(resources.GetObject("imgFolders.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgFolders.TransparentColor = System.Drawing.Color.Transparent
        Me.imgFolders.Images.SetKeyName(0, "")
        Me.imgFolders.Images.SetKeyName(1, "")
        Me.imgFolders.Images.SetKeyName(2, "")
        Me.imgFolders.Images.SetKeyName(3, "")
        Me.imgFolders.Images.SetKeyName(4, "")
        Me.imgFolders.Images.SetKeyName(5, "")
        Me.imgFolders.Images.SetKeyName(6, "")
        Me.imgFolders.Images.SetKeyName(7, "document_gear.png")
        Me.imgFolders.Images.SetKeyName(8, "")
        Me.imgFolders.Images.SetKeyName(9, "")
        Me.imgFolders.Images.SetKeyName(10, "")
        Me.imgFolders.Images.SetKeyName(11, "")
        Me.imgFolders.Images.SetKeyName(12, "")
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnRemove)
        Me.Panel1.Controls.Add(Me.btnAdd)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel1.Location = New System.Drawing.Point(179, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(48, 273)
        Me.Panel1.TabIndex = 3
        '
        'btnRemove
        '
        Me.btnRemove.Image = CType(resources.GetObject("btnRemove.Image"), System.Drawing.Image)
        Me.btnRemove.Location = New System.Drawing.Point(6, 140)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(35, 23)
        Me.btnRemove.TabIndex = 0
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.Location = New System.Drawing.Point(6, 99)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(35, 23)
        Me.btnAdd.TabIndex = 0
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'ExpandableSplitter1
        '
        Me.ExpandableSplitter1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ExpandableSplitter1.BackColor2 = System.Drawing.Color.Empty
        Me.ExpandableSplitter1.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.ExpandableSplitter1.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.ExpandableSplitter1.ExpandableControl = Me.tvSchedules
        Me.ExpandableSplitter1.ExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(163, Byte), Integer), CType(CType(209, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ExpandableSplitter1.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground
        Me.ExpandableSplitter1.ExpandLineColor = System.Drawing.SystemColors.Highlight
        Me.ExpandableSplitter1.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.GripDarkColor = System.Drawing.SystemColors.Highlight
        Me.ExpandableSplitter1.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.GripLightColor = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.ExpandableSplitter1.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.MenuBackground
        Me.ExpandableSplitter1.HotBackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ExpandableSplitter1.HotBackColor2 = System.Drawing.Color.Empty
        Me.ExpandableSplitter1.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.ExpandableSplitter1.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemCheckedBackground
        Me.ExpandableSplitter1.HotExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(163, Byte), Integer), CType(CType(209, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ExpandableSplitter1.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground
        Me.ExpandableSplitter1.HotExpandLineColor = System.Drawing.SystemColors.Highlight
        Me.ExpandableSplitter1.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.HotGripDarkColor = System.Drawing.SystemColors.Highlight
        Me.ExpandableSplitter1.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.HotGripLightColor = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.ExpandableSplitter1.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.MenuBackground
        Me.ExpandableSplitter1.Location = New System.Drawing.Point(176, 0)
        Me.ExpandableSplitter1.Name = "ExpandableSplitter1"
        Me.ExpandableSplitter1.Size = New System.Drawing.Size(3, 273)
        Me.ExpandableSplitter1.Style = DevComponents.DotNetBar.eSplitterStyle.Mozilla
        Me.ExpandableSplitter1.TabIndex = 2
        Me.ExpandableSplitter1.TabStop = False
        '
        'tvSchedules
        '
        Me.tvSchedules.Dock = System.Windows.Forms.DockStyle.Left
        Me.tvSchedules.ImageIndex = 0
        Me.tvSchedules.ImageList = Me.imgFolders
        Me.tvSchedules.Location = New System.Drawing.Point(0, 0)
        Me.tvSchedules.Name = "tvSchedules"
        Me.tvSchedules.SelectedImageIndex = 0
        Me.tvSchedules.Size = New System.Drawing.Size(176, 273)
        Me.tvSchedules.TabIndex = 1
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        '
        'Step5
        '
        Me.Step5.Controls.Add(Me.UcReports1)
        Me.Step5.Location = New System.Drawing.Point(0, 84)
        Me.Step5.Name = "Step5"
        Me.Step5.Size = New System.Drawing.Size(436, 273)
        Me.Step5.TabIndex = 7
        '
        'UcReports1
        '
        Me.UcReports1.Location = New System.Drawing.Point(3, 3)
        Me.UcReports1.m_MasterID = 99999
        Me.UcReports1.Name = "UcReports1"
        Me.UcReports1.Size = New System.Drawing.Size(418, 255)
        Me.UcReports1.TabIndex = 0
        '
        'Step7
        '
        Me.Step7.Controls.Add(Me.UcTasks1)
        Me.Step7.Location = New System.Drawing.Point(0, 84)
        Me.Step7.Name = "Step7"
        Me.Step7.Size = New System.Drawing.Size(436, 273)
        Me.Step7.TabIndex = 8
        '
        'UcTasks1
        '
        Me.UcTasks1.BackColor = System.Drawing.SystemColors.Control
        Me.UcTasks1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcTasks1.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcTasks1.ForeColor = System.Drawing.Color.Navy
        Me.UcTasks1.Location = New System.Drawing.Point(0, 0)
        Me.UcTasks1.m_defaultTaks = False
        Me.UcTasks1.m_eventBased = False
        Me.UcTasks1.m_eventID = 99999
        Me.UcTasks1.Name = "UcTasks1"
        Me.UcTasks1.Size = New System.Drawing.Size(436, 273)
        Me.UcTasks1.TabIndex = 0
        '
        'cmdNext
        '
        Me.cmdNext.Enabled = False
        Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
        Me.cmdNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(355, 387)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(75, 25)
        Me.cmdNext.TabIndex = 23
        Me.cmdNext.Text = "&Next"
        '
        'cmdFinish
        '
        Me.cmdFinish.Image = CType(resources.GetObject("cmdFinish.Image"), System.Drawing.Image)
        Me.cmdFinish.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdFinish.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdFinish.Location = New System.Drawing.Point(355, 387)
        Me.cmdFinish.Name = "cmdFinish"
        Me.cmdFinish.Size = New System.Drawing.Size(75, 25)
        Me.cmdFinish.TabIndex = 26
        Me.cmdFinish.Text = "&Finish"
        Me.cmdFinish.Visible = False
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(275, 387)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 25)
        Me.cmdCancel.TabIndex = 25
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdBack
        '
        Me.cmdBack.Enabled = False
        Me.cmdBack.Image = CType(resources.GetObject("cmdBack.Image"), System.Drawing.Image)
        Me.cmdBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBack.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBack.Location = New System.Drawing.Point(195, 387)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(75, 25)
        Me.cmdBack.TabIndex = 24
        Me.cmdBack.Text = "&Back"
        '
        'Step8
        '
        Me.Step8.Controls.Add(Me.Label3)
        Me.Step8.Controls.Add(Me.ucFlow)
        Me.Step8.Location = New System.Drawing.Point(0, 84)
        Me.Step8.Name = "Step8"
        Me.Step8.Size = New System.Drawing.Size(436, 273)
        Me.Step8.TabIndex = 29
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(238, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Please select the schedule's execution flow type"
        '
        'ucFlow
        '
        Me.ucFlow.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ucFlow.Location = New System.Drawing.Point(3, 52)
        Me.ucFlow.m_ExecutionFlow = "AFTER"
        Me.ucFlow.Name = "ucFlow"
        Me.ucFlow.Size = New System.Drawing.Size(427, 171)
        Me.ucFlow.TabIndex = 0
        '
        'Step6
        '
        Me.Step6.Controls.Add(Me.cmbPriority)
        Me.Step6.Controls.Add(Me.Label4)
        Me.Step6.Controls.Add(Me.DividerLabel5)
        Me.Step6.Controls.Add(Me.cmbOpHrs)
        Me.Step6.Controls.Add(Me.chkOpHrs)
        Me.Step6.Controls.Add(Me.DividerLabel4)
        Me.Step6.Controls.Add(Me.DividerLabel3)
        Me.Step6.Controls.Add(Me.UcError)
        Me.Step6.Location = New System.Drawing.Point(0, 84)
        Me.Step6.Name = "Step6"
        Me.Step6.Size = New System.Drawing.Size(436, 273)
        Me.Step6.TabIndex = 30
        '
        'cmbPriority
        '
        Me.cmbPriority.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPriority.FormattingEnabled = True
        Me.cmbPriority.Items.AddRange(New Object() {"1 - High", "2 - Above Nornal", "3 - Normal", "4 - Below Normal", "5 - Low "})
        Me.cmbPriority.Location = New System.Drawing.Point(209, 170)
        Me.cmbPriority.Name = "cmbPriority"
        Me.cmbPriority.Size = New System.Drawing.Size(218, 21)
        Me.cmbPriority.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(2, 173)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(175, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Set the priority of this schedules to"
        '
        'DividerLabel5
        '
        Me.DividerLabel5.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel5.Location = New System.Drawing.Point(3, 154)
        Me.DividerLabel5.Name = "DividerLabel5"
        Me.DividerLabel5.Size = New System.Drawing.Size(429, 19)
        Me.DividerLabel5.Spacing = 0
        Me.DividerLabel5.TabIndex = 5
        Me.DividerLabel5.Text = "Schedule Priority"
        '
        'cmbOpHrs
        '
        Me.cmbOpHrs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOpHrs.Enabled = False
        Me.cmbOpHrs.FormattingEnabled = True
        Me.cmbOpHrs.Location = New System.Drawing.Point(209, 119)
        Me.cmbOpHrs.Name = "cmbOpHrs"
        Me.cmbOpHrs.Size = New System.Drawing.Size(218, 21)
        Me.cmbOpHrs.TabIndex = 4
        '
        'chkOpHrs
        '
        Me.chkOpHrs.AutoSize = True
        Me.chkOpHrs.Location = New System.Drawing.Point(6, 119)
        Me.chkOpHrs.Name = "chkOpHrs"
        Me.chkOpHrs.Size = New System.Drawing.Size(173, 17)
        Me.chkOpHrs.TabIndex = 3
        Me.chkOpHrs.Text = "Use custom hours of operation"
        Me.chkOpHrs.UseVisualStyleBackColor = True
        '
        'DividerLabel4
        '
        Me.DividerLabel4.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel4.Location = New System.Drawing.Point(3, 92)
        Me.DividerLabel4.Name = "DividerLabel4"
        Me.DividerLabel4.Size = New System.Drawing.Size(429, 19)
        Me.DividerLabel4.Spacing = 0
        Me.DividerLabel4.TabIndex = 2
        Me.DividerLabel4.Text = "Hours of Operation"
        '
        'DividerLabel3
        '
        Me.DividerLabel3.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel3.Location = New System.Drawing.Point(3, 9)
        Me.DividerLabel3.Name = "DividerLabel3"
        Me.DividerLabel3.Size = New System.Drawing.Size(429, 13)
        Me.DividerLabel3.Spacing = 0
        Me.DividerLabel3.TabIndex = 1
        Me.DividerLabel3.Text = "Exception Handling"
        '
        'UcError
        '
        Me.UcError.BackColor = System.Drawing.Color.Transparent
        Me.UcError.Location = New System.Drawing.Point(3, 23)
        Me.UcError.m_showFailOnOne = False
        Me.UcError.Name = "UcError"
        Me.UcError.Size = New System.Drawing.Size(430, 62)
        Me.UcError.TabIndex = 0
        '
        'mnuParameters
        '
        Me.mnuParameters.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuParameter})
        '
        'mnuParameter
        '
        Me.mnuParameter.Index = 0
        Me.mnuParameter.Text = "Parameter Substitution"
        '
        'DividerLabel2
        '
        Me.DividerLabel2.BackColor = System.Drawing.SystemColors.Control
        Me.DividerLabel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.DividerLabel2.Enabled = False
        Me.DividerLabel2.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel2.Location = New System.Drawing.Point(0, 69)
        Me.DividerLabel2.Name = "DividerLabel2"
        Me.DividerLabel2.Size = New System.Drawing.Size(436, 10)
        Me.DividerLabel2.Spacing = 0
        Me.DividerLabel2.TabIndex = 28
        '
        'DividerLabel1
        '
        Me.DividerLabel1.Enabled = False
        Me.DividerLabel1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.DividerLabel1.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(0, 369)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(436, 15)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 28
        Me.DividerLabel1.Text = "ChristianSteven Software"
        '
        'frmEventWizard6
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(436, 415)
        Me.ControlBox = False
        Me.Controls.Add(Me.Step6)
        Me.Controls.Add(Me.Step4)
        Me.Controls.Add(Me.Step7)
        Me.Controls.Add(Me.Step1)
        Me.Controls.Add(Me.Step2)
        Me.Controls.Add(Me.Step3)
        Me.Controls.Add(Me.Step5)
        Me.Controls.Add(Me.Step8)
        Me.Controls.Add(Me.DividerLabel2)
        Me.Controls.Add(Me.DividerLabel1)
        Me.Controls.Add(Me.cmdNext)
        Me.Controls.Add(Me.cmdFinish)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdBack)
        Me.Controls.Add(Me.pnlTop)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmEventWizard6"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Event-Based Schedule"
        Me.pnlTop.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step1.ResumeLayout(False)
        Me.Step1.PerformLayout()
        Me.Step2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Step3.ResumeLayout(False)
        Me.Step3.PerformLayout()
        Me.Step4.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step5.ResumeLayout(False)
        Me.Step7.ResumeLayout(False)
        Me.Step8.ResumeLayout(False)
        Me.Step8.PerformLayout()
        Me.Step6.ResumeLayout(False)
        Me.Step6.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlTop As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblStep As System.Windows.Forms.Label
    Friend WithEvents Step1 As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents txtKeyword As System.Windows.Forms.TextBox
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents txtLocation As System.Windows.Forms.TextBox
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents Step2 As System.Windows.Forms.Panel
    Friend WithEvents Step3 As System.Windows.Forms.Panel
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents optExisting As System.Windows.Forms.RadioButton
    Friend WithEvents Step4 As System.Windows.Forms.Panel
    Friend WithEvents ExpandableSplitter1 As DevComponents.DotNetBar.ExpandableSplitter
    Friend WithEvents tvSchedules As System.Windows.Forms.TreeView
    Friend WithEvents lsvSchedules As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnRemove As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents Step5 As System.Windows.Forms.Panel
    Friend WithEvents UcReports1 As sqlrd.ucReports
    Friend WithEvents Step7 As System.Windows.Forms.Panel
    Friend WithEvents UcTasks1 As sqlrd.ucTasks
    Friend WithEvents cmdNext As System.Windows.Forms.Button
    Friend WithEvents cmdFinish As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdBack As System.Windows.Forms.Button
    Friend WithEvents UcConditions1 As sqlrd.ucConditions
    Friend WithEvents imgFolders As System.Windows.Forms.ImageList
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Friend WithEvents DividerLabel2 As sqlrd.DividerLabel
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbAnyAll As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents chkStatus As System.Windows.Forms.CheckBox
    Friend WithEvents Step8 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ucFlow As sqlrd.ucExecutionPath
    Friend WithEvents optNone As System.Windows.Forms.RadioButton
    Friend WithEvents Step6 As System.Windows.Forms.Panel
    Friend WithEvents UcError As sqlrd.ucErrorHandler
    Friend WithEvents DividerLabel3 As sqlrd.DividerLabel
    Friend WithEvents DividerLabel4 As sqlrd.DividerLabel
    Friend WithEvents chkOpHrs As System.Windows.Forms.CheckBox
    Friend WithEvents cmbOpHrs As System.Windows.Forms.ComboBox
    Friend WithEvents mnuParameters As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuParameter As System.Windows.Forms.MenuItem
    Friend WithEvents DividerLabel5 As sqlrd.DividerLabel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmbPriority As System.Windows.Forms.ComboBox
End Class
