Public Class frmEventWizard6

    Dim CurrentStep As String = ""

    Const m_Step1 As String = "Step 1: Schedule Details"
    Const m_Step2 As String = "Step 2: Schedule Conditions"
    Const m_Step3 As String = "Step 3: Report Type"
    Const m_Step4 As String = "Step 4: Select schedules"
    Const m_Step5 As String = "Step 4: Add Reports"
    Const m_Step6 As String = "Step 5: Schedule Options"
    Const m_step7 As String = "Step 5: Custom Tasks"
    Const m_Step8 As String = "Step 6: Execution Flow"

    Dim nStep As Integer = 1
    Dim UserCancel As Boolean = True
    Dim m_EventDtls As New Hashtable
    Dim m_PackOrderID As Integer = 0

    Private Sub frmEventWizard6_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.s3_EventBasedScheds) = False Then
            _NeedUpgrade(gEdition.ENTERPRISEPROPLUS)
            Close()
            Return
        End If

        FormatForWinXP(Me)
        Step1.BringToFront()
        CurrentStep = Step1.Name

        Dim oUI As clsMarsUI = New clsMarsUI

        oUI.BuildTree(tvSchedules, True, False)

        UcTasks1.ShowAfterType = False
        UcTasks1.m_eventBased = True
        UcTasks1.m_eventID = 99999
        UcTasks1.lsvTasks.HeaderStyle = ColumnHeaderStyle.None

        tvSchedules.Nodes(0).Expand()

        lblStep.Text = m_Step1

        If gParentID > 0 And gParent.Length > 0 And txtLocation.Tag = "" Then
            txtLocation.Text = gParent
            txtLocation.Tag = gParentID
        End If

        clsMarsData.DataItem.CleanDB()

        lsvSchedules.SmallImageList = gImages
        lsvSchedules.LargeImageList = gImages

        clsMarsEvent.currentEventID = 99999

        cmbPriority.Text = "3 - Normal"
    End Sub

    Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBack.Click
        Select Case nStep
            Case 7
                Step7.BringToFront()

                cmdNext.Visible = True
                cmdFinish.Visible = False

                lblStep.Text = m_step7
            Case 6
                Step6.BringToFront()

                lblStep.Text = m_Step6
            Case 5
                If Me.optExisting.Checked Then
                    Step4.BringToFront()

                    lblStep.Text = m_Step4
                ElseIf optNone.Checked Then
                    Step3.BringToFront()

                    lblStep.Text = m_Step3
                    nStep -= 1
                Else
                    Step5.BringToFront()

                    lblStep.Text = m_Step5
                End If


            Case 4
                Step3.BringToFront()

                lblStep.Text = m_Step3
            Case 3
                Step2.BringToFront()

                lblStep.Text = m_Step2
            Case 2
                Step1.BringToFront()

                'CurrentStep = "step1"
                lblStep.Text = m_Step1

                cmdBack.Enabled = False
        End Select

        nStep -= 1
    End Sub
    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click
        Select Case nStep
            Case 1
                If txtName.Text.Length = 0 Then
                    ep.SetError(txtName, "Please enter the schedule name")
                    txtName.Focus()
                    Return
                ElseIf txtLocation.Text.Length = 0 Then
                    ep.SetError(txtLocation, "Please select the schedule location")
                    txtLocation.Focus()
                    Return
                ElseIf clsMarsData.IsDuplicate("EventAttr", "EventName", txtName.Text, True) Then
                    ep.SetError(txtName, "An event-based schedule with this name already exists")
                    txtName.Focus()
                    Return
                ElseIf clsMarsUI.candoRename(txtName.Text, Me.txtLocation.Tag, clsMarsScheduler.enScheduleType.EVENTBASED) = False Then
                    ep.SetError(txtName, "An event-based schedule with this name already exists")
                    txtName.Focus()
                    Return
                End If

                Step2.BringToFront()

                cmdBack.Enabled = True

                lblStep.Text = m_Step2
            Case 2
                If Me.UcConditions1.lsvConditions.Items.Count = 0 Then
                    ep.SetError(Me.UcConditions1.lsvConditions, "Please add at least one event.")
                    Return
                ElseIf cmbAnyAll.Text.Length = 0 Then
                    ep.SetError(cmbAnyAll, "Please specify a value for this field")
                    cmbAnyAll.Focus()
                    Return
                End If

                Step3.BringToFront()

                lblStep.Text = m_Step3
            Case 3
                If Me.optExisting.Checked Then
                    Step4.BringToFront()
                    lsvSchedules.ContextMenu = Me.mnuParameters
                    lblStep.Text = m_Step4
                ElseIf Me.optNone.Checked = True Then
                    Step6.BringToFront()

                    lblStep.Text = m_step6

                    nStep += 1
                Else
                    Step5.BringToFront()

                    'CurrentStep = Step5.Name
                    lblStep.Text = m_Step5
                End If

            Case 4
                If optExisting.Checked = True Then
                    If Me.lsvSchedules.Items.Count = 0 Then
                        ep.SetError(Me.btnAdd, "Please select an existing schedule to execute when conditions are met")
                        btnAdd.Focus()
                        Return
                    End If
                ElseIf Me.RadioButton2.Checked Then
                    If Me.UcReports1.lsvReports.Items.Count = 0 Then
                        ep.SetError(Me.UcReports1.cmdAddReport, "Please add a report to be produced when conditions are met")
                        Me.UcReports1.cmdAddReport.Focus()
                        Return
                    End If
                End If


                Step6.BringToFront()

                lblStep.Text = m_Step6

                If Me.m_PackOrderID > 0 Then
                    Me.DividerLabel4.Visible = False
                    Me.chkOpHrs.Visible = False
                    Me.cmbOpHrs.Visible = False
                End If
            Case 5
                If Me.chkOpHrs.Checked Then
                    If Me.cmbOpHrs.Text = "" Or Me.cmbOpHrs.Text = "<New...>" Then
                        ep.SetError(Me.cmbOpHrs, "Please select a valid entry...")
                        Me.cmbOpHrs.Focus()
                        Return

                    End If
                End If
                Step7.BringToFront()

                lblStep.Text = m_step7
            Case 6
                If UcTasks1.lsvTasks.Items.Count = 0 Or optNone.Checked Then
                    ucFlow.Enabled = False
                Else
                    ucFlow.Enabled = True
                End If

                Step8.BringToFront()

                lblStep.Text = Me.m_Step8
                cmdNext.Visible = False
                cmdFinish.Visible = True
        End Select

        nStep += 1
    End Sub

    
    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Dim oFolders As frmFolders = New frmFolders
        Dim folderDetails() As String

        folderDetails = oFolders.GetFolder

        txtLocation.Text = folderDetails(0)
        txtLocation.Tag = folderDetails(1)

    End Sub

    Private Sub txtLocation_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles txtLocation.TextChanged, txtName.TextChanged
        Dim oText As TextBox = CType(sender, TextBox)

        If oText.Text.Length = 0 Then
            cmdNext.Enabled = False
        Else
            cmdNext.Enabled = True
        End If
    End Sub

    
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If tvSchedules.SelectedNode Is Nothing Then Return

        Dim oNode As TreeNode
        Dim oItem As ListViewItem
        Dim sType As String

        oNode = tvSchedules.SelectedNode

        sType = oNode.Tag

        sType = sType.Split(":")(0).ToLower

        If sType = "folder" Or sType = "desktop" Or sType = "smartfolder" Then Return

        For Each oItem In lsvSchedules.Items
            If oItem.Text = oNode.Text And oItem.Tag = oNode.Tag Then
                Return
            End If
        Next

        oItem = New ListViewItem

        oItem.Text = oNode.Text
        oItem.Tag = oNode.Tag
        oItem.ImageIndex = oNode.ImageIndex

        lsvSchedules.Items.Add(oItem)
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        If lsvSchedules.SelectedItems.Count = 0 Then Exit Sub

        For Each oItem As ListViewItem In lsvSchedules.SelectedItems
            clsMarsData.WriteData("DELETE FROM EventSubAttr WHERE ReportID=" & oItem.Tag.split(":")(1), False)
            oItem.Remove()
        Next
    End Sub

    Private Sub cmbAnyAll_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAnyAll.SelectedIndexChanged
        ep.SetError(cmbAnyAll, "")
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        clsMarsData.DataItem.CleanDB()
        Close()
    End Sub

    Public Function AddSchedule(ByVal PackOrderID As Integer) As Hashtable
        Me.m_PackOrderID = PackOrderID

        Me.cmbPriority.Enabled = False
        Me.chkOpHrs.Checked = False
        Me.chkOpHrs.Enabled = False

        Me.ShowDialog()

        If UserCancel = True Then
            Return Nothing
        Else
            Return Me.m_EventDtls
        End If

    End Function
    Private Sub cmdFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdFinish.Click
        Dim cols As String
        Dim vals As String
        Dim eventID As Integer = clsMarsData.CreateDataID("eventattr6", "eventid")



        Dim reportType As String

        If Me.optNone.Checked Then
            reportType = "None"
        ElseIf Me.optExisting.Checked Then
            reportType = "Existing"
        Else
            reportType = "New"
        End If

        Dim Status As String = ""

        If chkStatus.Enabled = True Then
            Status = "Enabled"
        Else
            Status = "Disabled"
        End If

        Me.m_EventDtls.Add("Name", txtName.Text)
        Me.m_EventDtls.Add("ID", eventID)
        Me.m_EventDtls.Add("Status", Status)

        cols = "EventID,EventName,Description,Keyword,Parent,Status,Owner,DisabledDate," & _
                "ScheduleType,AnyAll,ExecutionFlow,RetryCount,AutoFailAfter,PackOrderID,AutoCalc," & _
                "RetryInterval,UseOperationHours,OperationName,SchedulePriority"

        vals = eventID & "," & _
        "'" & SQLPrepare(txtName.Text) & "'," & _
        "'" & SQLPrepare(txtDescription.Text) & "'," & _
        "'" & SQLPrepare(txtKeyword.Text) & "'," & _
        Me.txtLocation.Tag & "," & _
        Convert.ToInt32(chkStatus.Checked) & "," & _
        "'" & SQLPrepare(gUser) & "'," & _
        "'" & Now & "'," & _
        "'" & reportType & "'," & _
        "'" & cmbAnyAll.Text & "'," & _
        "'" & ucFlow.m_ExecutionFlow & "'," & _
        UcError.cmbRetry.Value & "," & _
        UcError.m_autoFailAfter(True) & "," & _
        Me.m_PackOrderID & "," & _
        Convert.ToInt32(UcError.chkAutoCalc.Checked) & "," & _
        UcError.txtRetryInterval.Value & "," & _
        Convert.ToInt32(Me.chkOpHrs.Checked) & "," & _
        "'" & Me.cmbOpHrs.Text & "'," & _
        "'" & Me.cmbPriority.Text & "'"

        If clsMarsData.DataItem.InsertData("EventAttr6", cols, vals, True) = True Then
            Dim SQL As String = "UPDATE EventConditions SET EventID = " & eventID & " WHERE EventID = 99999"

            clsMarsData.WriteData(SQL)

            SQL = "UPDATE ReportAttr SET PackID = " & eventID & " WHERE PackID = 99999"

            clsMarsData.WriteData(SQL)

            UcTasks1.CommitDeletions()

            SQL = "UPDATE Tasks SET ScheduleID = " & eventID & " WHERE ScheduleID = 99999"

            clsMarsData.WriteData(SQL)

            If Me.optExisting.Checked Then
                Dim nID As Integer
                Dim sType As String

                cols = "ID,EventID,ScheduleID,ScheduleType"

                clsMarsUI.MainUI.BusyProgress(90, "Saving schedules for event...")

                For Each oItem As ListViewItem In lsvSchedules.Items
                    nID = oItem.Tag.Split(":")(1)
                    sType = oItem.Tag.Split(":")(0)


                    vals = clsMarsData.CreateDataID("eventscheduleid", "id") & "," & _
                    eventID & "," & _
                    nID & "," & _
                    "'" & sType & "'"

                    If clsMarsData.DataItem.InsertData("EventSchedule", cols, vals, True) = False Then
                        Exit For
                    End If
                Next

                clsMarsData.WriteData("UPDATE EventSubAttr SET EventID =" & eventID & " WHERE EventID = 99999")

            End If
        End If

        UserCancel = False

        Close()

        Try
            Dim nCount As Integer
            nCount = txtLocation.Text.Split("\").GetUpperBound(0)

            Dim oTree As TreeView = oWindow(nWindowCurrent).tvFolders

            oTree.SelectedNode = Nothing

            clsMarsUI.MainUI.FindNode("Folder:" & txtLocation.Tag, oTree, oTree.Nodes(0))
        Catch

        End Try

        clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.EVENTS, clsMarsAudit.AuditAction.CREATE)

    End Sub

    Private Sub cmbOpHrs_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbOpHrs.DropDown
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        Me.cmbOpHrs.Items.Clear()

        SQL = "SELECT * FROM OperationAttr"

        oRs = clsMarsData.GetData(SQL)

        Me.cmbOpHrs.Items.Add("<New...>")

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                cmbOpHrs.Items.Add(oRs("operationname").Value)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub

    Private Sub cmbOpHrs_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbOpHrs.SelectedIndexChanged
        ep.SetError(sender, "")

        If cmbOpHrs.Text = "<New...>" Then
            Dim opAdd As frmOperationalHours = New frmOperationalHours

            Dim sNew As String = opAdd.AddOperationalHours()

            If sNew <> "" Then
                Me.cmbOpHrs.Items.Add(sNew)

                Me.cmbOpHrs.Text = sNew
            End If
        End If
    End Sub

    Private Sub chkOpHrs_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkOpHrs.CheckedChanged
        Me.cmbOpHrs.Enabled = Me.chkOpHrs.Checked
    End Sub

    Private Sub mnuParameter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuParameter.Click
        Dim sType As String
        Dim nID As Integer

        If lsvSchedules.SelectedItems.Count = 0 Then Return

        Dim oItem As ListViewItem

        oItem = lsvSchedules.SelectedItems(0)

        sType = oItem.Tag.Split(":")(0)
        nID = oItem.Tag.Split(":")(1)

        If sType.ToLower <> "report" Then Return

        Dim oSub As frmEventSub = New frmEventSub

        oSub.m_eventID = 99999

        oSub.SetSubstitution(nID, 99999)

    End Sub
End Class