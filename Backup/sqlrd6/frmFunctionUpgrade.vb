Public Class frmFunctionUpgrade
    Const Step1 As String = "Step 1: Enter Key"
    Const Step2 As String = "Step 2: Available Features"
    Dim nStep As Integer = 1
    Dim ep As ErrorProvider = New ErrorProvider
    Dim featureDesc As ArrayList
    Dim tip As DevComponents.DotNetBar.SuperTooltipInfo
    Dim gCustNo As String

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Select Case nStep
            Case 1
                If Me.checkKeyValidity = False Then
                    Me.ep.SetError(Me.txtUpgradeKey, "The provided customer number is not valid for the provided Upgrade Key")
                    Me.txtUpgradeKey.Focus()
                    Return
                End If

                Page1.Visible = False
                Page2.Visible = True
                btnBack.Enabled = True
                btnNext.Visible = False
                btnFinish.Visible = True
                btnCancel.Enabled = False

                Me.listFunctions()

                nStep += 1

                lblStep.Text = Step2
        End Select
    End Sub

    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Select Case nStep
            Case 2
                Page1.Visible = True
                Page2.Visible = False
                btnBack.Enabled = False
                btnNext.Enabled = True
                btnNext.Visible = True
                btnFinish.Visible = False
                btnCancel.Enabled = True

                nStep -= 1

                lblStep.Text = Step1
        End Select
    End Sub

    Private Function checkKeyValidity() As Boolean
        Dim key As String = clsSettingsManager.Appconfig.DecryptString(Me.txtUpgradeKey.Text)
        Dim custNo As String = key.Split("|")(0)
        Dim custID As String = clsMarsUI.MainUI.ReadRegistry("custNo", "")

        If custNo <> custID Then
            Return False
        Else
            Return True
        End If
    End Function
    Private Sub listFunctions()
        Dim key As String = clsSettingsManager.Appconfig.DecryptString(Me.txtUpgradeKey.Text)
        Dim custNo As String = key.Split("|")(0)

        For Each s As String In key.Split("|")
            For Each item As ListViewItem In Me.lsvFeatures.Items
                Dim featureID As String = item.Tag.ToString.Split("|")(0).ToLower

                If featureID = s.ToLower Then
                    item.Checked = True
                    Exit For
                End If
            Next
        Next
    End Sub

    Private Sub frmFunctionUpgrade_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        gCustNo = clsMarsUI.MainUI.ReadRegistry("CustNo", "")
        Me.txtUpgradeKey.Text = clsMarsUI.MainUI.ReadRegistry("UpgradeKey", "")

        getAllFeatures()

        Me.Page1.Visible = True
        Me.Page2.Visible = False
        lblStep.Text = Step1
    End Sub

    Private Sub getAllFeatures()
10:     Try
            Dim oRs As ADODB.Recordset
            Dim oCon As ADODB.Connection
            Dim sCon As String
            Dim sPath As String = Application.StartupPath & "\cstmbld.dat"
20:         featureDesc = New ArrayList

30:         sCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sPath & ";Persist Security Info=False"

40:         oCon = New ADODB.Connection
50:         oRs = New ADODB.Recordset

60:         oCon.Open(sCon)

            Dim SQL As String = "SELECT * FROM Features ORDER BY FeatureName"

70:         oRs.Open(SQL, oCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

80:         Do While oRs.EOF = False
90:             Dim item As ListViewItem = New ListViewItem(CType(oRs("featurename").Value, String))
100:            item.Tag = oRs("featureid").Value & "|" & oRs("featureurl").Value
110:            'item.ImageIndex = 1

120:            Me.lsvFeatures.Items.Add(item)

130:            featureDesc.Add(oRs("featuredesc").Value)

140:            oRs.MoveNext()
150:        Loop

160:        oRs.Close()
170:        oCon.Close()

180:        oRs = Nothing
190:        oCon = Nothing
200:    Catch ex As Exception
210:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try
    End Sub


    Private Sub txtCustNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) _
    Handles txtUpgradeKey.TextChanged
        ep.SetError(Me.txtUpgradeKey, "")
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Close()
    End Sub

    Private Sub lsvFeatures_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvFeatures.DoubleClick
        If lsvFeatures.SelectedItems.Count = 0 Then Return

        Dim item As ListViewItem = Me.lsvFeatures.SelectedItems(0)
        Dim url As String = item.Tag.ToString.Split("|")(1)

        Try
            Process.Start(url)
        Catch : End Try
    End Sub

    Private Sub lsvFeatures_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvFeatures.SelectedIndexChanged
        Try
            If lsvFeatures.SelectedItems.Count = 0 Then Return

            Dim nIndex As Integer = lsvFeatures.SelectedItems(0).Index
            Dim feature As String = featureDesc(nIndex)

            If feature <> "" Then
                tip = New DevComponents.DotNetBar.SuperTooltipInfo("", "", feature, My.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, Nothing)

                Super.SetSuperTooltip(Me.lsvFeatures, tip)

                Super.ShowTooltip(Me.lsvFeatures)
            Else
                Super.HideTooltip()
            End If
        Catch : End Try
    End Sub

    Private Sub btnFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        MessageBox.Show("Your Feature Upgrade has been applied successfully!", Application.ProductName, _
         MessageBoxButtons.OK, MessageBoxIcon.Information)

        clsMarsUI.MainUI.SaveRegistry("UpgradeKey", Me.txtUpgradeKey.Text, False, , True)

        Close()
    End Sub
End Class