<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEventPackage
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEventPackage))
        Me.pnlTop = New System.Windows.Forms.Panel
        Me.lblStep = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.cmdNext = New System.Windows.Forms.Button
        Me.cmdFinish = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdBack = New System.Windows.Forms.Button
        Me.Step1 = New System.Windows.Forms.Panel
        Me.btnBrowse = New System.Windows.Forms.Button
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtDescription = New System.Windows.Forms.TextBox
        Me.txtKeyword = New System.Windows.Forms.TextBox
        Me.txtName = New System.Windows.Forms.TextBox
        Me.txtLocation = New System.Windows.Forms.TextBox
        Me.Step2 = New System.Windows.Forms.Panel
        Me.ucSet = New sqlrd.ucScheduleSet
        Me.Step3 = New System.Windows.Forms.Panel
        Me.lsvSchedules = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.imgEvent = New System.Windows.Forms.ImageList(Me.components)
        Me.btnDown = New System.Windows.Forms.Button
        Me.btnUp = New System.Windows.Forms.Button
        Me.btnDelete = New System.Windows.Forms.Button
        Me.btnEdit = New System.Windows.Forms.Button
        Me.btnAdd = New System.Windows.Forms.Button
        Me.Step4 = New System.Windows.Forms.Panel
        Me.UcTasks = New sqlrd.ucTasks
        Me.mnuAdd = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuNew = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExisting = New System.Windows.Forms.ToolStripMenuItem
        Me.Step5 = New System.Windows.Forms.Panel
        Me.ucPath = New sqlrd.ucExecutionPath
        Me.DividerLabel2 = New sqlrd.DividerLabel
        Me.DividerLabel1 = New sqlrd.DividerLabel
        Me.pnlTop.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step1.SuspendLayout()
        Me.Step2.SuspendLayout()
        Me.Step3.SuspendLayout()
        Me.Step4.SuspendLayout()
        Me.mnuAdd.SuspendLayout()
        Me.Step5.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlTop
        '
        Me.pnlTop.BackColor = System.Drawing.Color.White
        Me.pnlTop.BackgroundImage = Global.sqlrd.My.Resources.Resources.strip
        Me.pnlTop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pnlTop.Controls.Add(Me.lblStep)
        Me.pnlTop.Controls.Add(Me.PictureBox1)
        Me.pnlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTop.Location = New System.Drawing.Point(0, 0)
        Me.pnlTop.Name = "pnlTop"
        Me.pnlTop.Size = New System.Drawing.Size(432, 69)
        Me.pnlTop.TabIndex = 1
        '
        'lblStep
        '
        Me.lblStep.BackColor = System.Drawing.Color.Transparent
        Me.lblStep.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep.ForeColor = System.Drawing.Color.Navy
        Me.lblStep.Location = New System.Drawing.Point(12, 28)
        Me.lblStep.Name = "lblStep"
        Me.lblStep.Size = New System.Drawing.Size(360, 23)
        Me.lblStep.TabIndex = 1
        Me.lblStep.Text = "Label1"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(378, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(46, 50)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'cmdNext
        '
        Me.cmdNext.Enabled = False
        Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
        Me.cmdNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(350, 442)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(75, 25)
        Me.cmdNext.TabIndex = 30
        Me.cmdNext.Text = "&Next"
        '
        'cmdFinish
        '
        Me.cmdFinish.Image = CType(resources.GetObject("cmdFinish.Image"), System.Drawing.Image)
        Me.cmdFinish.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdFinish.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdFinish.Location = New System.Drawing.Point(349, 442)
        Me.cmdFinish.Name = "cmdFinish"
        Me.cmdFinish.Size = New System.Drawing.Size(75, 25)
        Me.cmdFinish.TabIndex = 33
        Me.cmdFinish.Text = "&Finish"
        Me.cmdFinish.Visible = False
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(269, 442)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 25)
        Me.cmdCancel.TabIndex = 32
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdBack
        '
        Me.cmdBack.Enabled = False
        Me.cmdBack.Image = CType(resources.GetObject("cmdBack.Image"), System.Drawing.Image)
        Me.cmdBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBack.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBack.Location = New System.Drawing.Point(189, 442)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(75, 25)
        Me.cmdBack.TabIndex = 31
        Me.cmdBack.Text = "&Back"
        '
        'Step1
        '
        Me.Step1.Controls.Add(Me.btnBrowse)
        Me.Step1.Controls.Add(Me.Label8)
        Me.Step1.Controls.Add(Me.Label7)
        Me.Step1.Controls.Add(Me.Label6)
        Me.Step1.Controls.Add(Me.Label5)
        Me.Step1.Controls.Add(Me.txtDescription)
        Me.Step1.Controls.Add(Me.txtKeyword)
        Me.Step1.Controls.Add(Me.txtName)
        Me.Step1.Controls.Add(Me.txtLocation)
        Me.Step1.Location = New System.Drawing.Point(0, 80)
        Me.Step1.Name = "Step1"
        Me.Step1.Size = New System.Drawing.Size(436, 341)
        Me.Step1.TabIndex = 34
        '
        'btnBrowse
        '
        Me.btnBrowse.Image = CType(resources.GetObject("btnBrowse.Image"), System.Drawing.Image)
        Me.btnBrowse.Location = New System.Drawing.Point(329, 68)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(43, 21)
        Me.btnBrowse.TabIndex = 1
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(9, 227)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(54, 13)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "Keywords"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(9, 92)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(60, 13)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Description"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(9, 52)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(51, 13)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Create in"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 12)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Schedule Name"
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(12, 108)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(360, 116)
        Me.txtDescription.TabIndex = 3
        '
        'txtKeyword
        '
        Me.txtKeyword.Location = New System.Drawing.Point(12, 243)
        Me.txtKeyword.Name = "txtKeyword"
        Me.txtKeyword.Size = New System.Drawing.Size(360, 21)
        Me.txtKeyword.TabIndex = 4
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(12, 28)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(360, 21)
        Me.txtName.TabIndex = 0
        '
        'txtLocation
        '
        Me.txtLocation.BackColor = System.Drawing.Color.White
        Me.txtLocation.Location = New System.Drawing.Point(12, 68)
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.ReadOnly = True
        Me.txtLocation.Size = New System.Drawing.Size(301, 21)
        Me.txtLocation.TabIndex = 2
        '
        'Step2
        '
        Me.Step2.Controls.Add(Me.ucSet)
        Me.Step2.Location = New System.Drawing.Point(0, 80)
        Me.Step2.Name = "Step2"
        Me.Step2.Size = New System.Drawing.Size(436, 341)
        Me.Step2.TabIndex = 35
        '
        'ucSet
        '
        Me.ucSet.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ucSet.Location = New System.Drawing.Point(3, 3)
        Me.ucSet.m_RepeatUnit = "hours"
        Me.ucSet.Name = "ucSet"
        Me.ucSet.Size = New System.Drawing.Size(421, 336)
        Me.ucSet.TabIndex = 0
        '
        'Step3
        '
        Me.Step3.Controls.Add(Me.lsvSchedules)
        Me.Step3.Controls.Add(Me.btnDown)
        Me.Step3.Controls.Add(Me.btnUp)
        Me.Step3.Controls.Add(Me.btnDelete)
        Me.Step3.Controls.Add(Me.btnEdit)
        Me.Step3.Controls.Add(Me.btnAdd)
        Me.Step3.Location = New System.Drawing.Point(0, 80)
        Me.Step3.Name = "Step3"
        Me.Step3.Size = New System.Drawing.Size(436, 341)
        Me.Step3.TabIndex = 36
        '
        'lsvSchedules
        '
        Me.lsvSchedules.CheckBoxes = True
        Me.lsvSchedules.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvSchedules.FullRowSelect = True
        Me.lsvSchedules.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvSchedules.HideSelection = False
        Me.lsvSchedules.LargeImageList = Me.imgEvent
        Me.lsvSchedules.Location = New System.Drawing.Point(3, 8)
        Me.lsvSchedules.Name = "lsvSchedules"
        Me.lsvSchedules.Size = New System.Drawing.Size(341, 320)
        Me.lsvSchedules.SmallImageList = Me.imgEvent
        Me.lsvSchedules.TabIndex = 0
        Me.lsvSchedules.UseCompatibleStateImageBehavior = False
        Me.lsvSchedules.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Schedule Name"
        Me.ColumnHeader1.Width = 320
        '
        'imgEvent
        '
        Me.imgEvent.ImageStream = CType(resources.GetObject("imgEvent.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgEvent.TransparentColor = System.Drawing.Color.Transparent
        Me.imgEvent.Images.SetKeyName(0, "document_atoms.png")
        '
        'btnDown
        '
        Me.btnDown.Image = CType(resources.GetObject("btnDown.Image"), System.Drawing.Image)
        Me.btnDown.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDown.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnDown.Location = New System.Drawing.Point(350, 303)
        Me.btnDown.Name = "btnDown"
        Me.btnDown.Size = New System.Drawing.Size(75, 25)
        Me.btnDown.TabIndex = 32
        Me.btnDown.Text = "D&own"
        '
        'btnUp
        '
        Me.btnUp.Image = CType(resources.GetObject("btnUp.Image"), System.Drawing.Image)
        Me.btnUp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUp.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnUp.Location = New System.Drawing.Point(350, 272)
        Me.btnUp.Name = "btnUp"
        Me.btnUp.Size = New System.Drawing.Size(75, 25)
        Me.btnUp.TabIndex = 32
        Me.btnUp.Text = "&Up"
        '
        'btnDelete
        '
        Me.btnDelete.Image = CType(resources.GetObject("btnDelete.Image"), System.Drawing.Image)
        Me.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnDelete.Location = New System.Drawing.Point(349, 68)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(75, 25)
        Me.btnDelete.TabIndex = 32
        Me.btnDelete.Text = "&Delete"
        '
        'btnEdit
        '
        Me.btnEdit.Image = CType(resources.GetObject("btnEdit.Image"), System.Drawing.Image)
        Me.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnEdit.Location = New System.Drawing.Point(349, 40)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 25)
        Me.btnEdit.TabIndex = 32
        Me.btnEdit.Text = "&Edit"
        '
        'btnAdd
        '
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAdd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnAdd.Location = New System.Drawing.Point(350, 12)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(75, 25)
        Me.btnAdd.TabIndex = 32
        Me.btnAdd.Text = "&Add"
        '
        'Step4
        '
        Me.Step4.Controls.Add(Me.UcTasks)
        Me.Step4.Location = New System.Drawing.Point(0, 80)
        Me.Step4.Name = "Step4"
        Me.Step4.Size = New System.Drawing.Size(436, 341)
        Me.Step4.TabIndex = 37
        '
        'UcTasks
        '
        Me.UcTasks.BackColor = System.Drawing.SystemColors.Control
        Me.UcTasks.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcTasks.ForeColor = System.Drawing.Color.Navy
        Me.UcTasks.Location = New System.Drawing.Point(3, 3)
        Me.UcTasks.m_defaultTaks = False
        Me.UcTasks.m_eventBased = False
        Me.UcTasks.m_eventID = 99999
        Me.UcTasks.Name = "UcTasks"
        Me.UcTasks.Size = New System.Drawing.Size(424, 325)
        Me.UcTasks.TabIndex = 0
        '
        'mnuAdd
        '
        Me.mnuAdd.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuNew, Me.mnuExisting})
        Me.mnuAdd.Name = "ContextMenuStrip1"
        Me.mnuAdd.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuAdd.Size = New System.Drawing.Size(153, 70)
        '
        'mnuNew
        '
        Me.mnuNew.Name = "mnuNew"
        Me.mnuNew.Size = New System.Drawing.Size(152, 22)
        Me.mnuNew.Text = "&New"
        '
        'mnuExisting
        '
        Me.mnuExisting.Name = "mnuExisting"
        Me.mnuExisting.Size = New System.Drawing.Size(152, 22)
        Me.mnuExisting.Text = "Existing"
        '
        'Step5
        '
        Me.Step5.Controls.Add(Me.ucPath)
        Me.Step5.Location = New System.Drawing.Point(0, 80)
        Me.Step5.Name = "Step5"
        Me.Step5.Size = New System.Drawing.Size(436, 341)
        Me.Step5.TabIndex = 38
        '
        'ucPath
        '
        Me.ucPath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ucPath.Location = New System.Drawing.Point(8, 12)
        Me.ucPath.m_ExecutionFlow = "AFTER"
        Me.ucPath.Name = "ucPath"
        Me.ucPath.Size = New System.Drawing.Size(412, 171)
        Me.ucPath.TabIndex = 0
        '
        'DividerLabel2
        '
        Me.DividerLabel2.Enabled = False
        Me.DividerLabel2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.DividerLabel2.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.DividerLabel2.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel2.Location = New System.Drawing.Point(0, 424)
        Me.DividerLabel2.Name = "DividerLabel2"
        Me.DividerLabel2.Size = New System.Drawing.Size(436, 15)
        Me.DividerLabel2.Spacing = 0
        Me.DividerLabel2.TabIndex = 29
        Me.DividerLabel2.Text = "ChristianSteven Software"
        '
        'DividerLabel1
        '
        Me.DividerLabel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(0, 69)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(432, 10)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 2
        '
        'frmEventPackage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(432, 470)
        Me.ControlBox = False
        Me.Controls.Add(Me.Step3)
        Me.Controls.Add(Me.Step1)
        Me.Controls.Add(Me.Step4)
        Me.Controls.Add(Me.Step2)
        Me.Controls.Add(Me.Step5)
        Me.Controls.Add(Me.cmdNext)
        Me.Controls.Add(Me.cmdFinish)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdBack)
        Me.Controls.Add(Me.DividerLabel2)
        Me.Controls.Add(Me.DividerLabel1)
        Me.Controls.Add(Me.pnlTop)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmEventPackage"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Event-Based Package"
        Me.pnlTop.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step1.ResumeLayout(False)
        Me.Step1.PerformLayout()
        Me.Step2.ResumeLayout(False)
        Me.Step3.ResumeLayout(False)
        Me.Step4.ResumeLayout(False)
        Me.mnuAdd.ResumeLayout(False)
        Me.Step5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlTop As System.Windows.Forms.Panel
    Friend WithEvents lblStep As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Friend WithEvents DividerLabel2 As sqlrd.DividerLabel
    Friend WithEvents cmdNext As System.Windows.Forms.Button
    Friend WithEvents cmdFinish As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdBack As System.Windows.Forms.Button
    Friend WithEvents Step1 As System.Windows.Forms.Panel
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents txtKeyword As System.Windows.Forms.TextBox
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents txtLocation As System.Windows.Forms.TextBox
    Friend WithEvents Step2 As System.Windows.Forms.Panel
    Friend WithEvents ucSet As sqlrd.ucScheduleSet
    Friend WithEvents Step3 As System.Windows.Forms.Panel
    Friend WithEvents lsvSchedules As System.Windows.Forms.ListView
    Friend WithEvents btnDown As System.Windows.Forms.Button
    Friend WithEvents btnUp As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Step4 As System.Windows.Forms.Panel
    Friend WithEvents UcTasks As sqlrd.ucTasks
    Friend WithEvents mnuAdd As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExisting As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents imgEvent As System.Windows.Forms.ImageList
    Friend WithEvents Step5 As System.Windows.Forms.Panel
    Friend WithEvents ucPath As sqlrd.ucExecutionPath
End Class
