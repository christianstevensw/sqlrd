<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStarterWizard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStarterWizard))
        Dim TreeNode1 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Create Admin user", 1, 1)
        Dim TreeNode2 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Configure messaging (optional)", 2, 2)
        Dim TreeNode3 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Register for free support (optional)", 3, 3)
        Dim TreeNode4 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Finish", 4, 4)
        Dim TreeNode5 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Getting Started with SQL-RD", New System.Windows.Forms.TreeNode() {TreeNode1, TreeNode2, TreeNode3, TreeNode4})
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.cmdFinish = New System.Windows.Forms.Button
        Me.cmdNext = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdBack = New System.Windows.Forms.Button
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.tvSteps = New System.Windows.Forms.TreeView
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Step1 = New System.Windows.Forms.Panel
        Me.cmdSaveUser = New System.Windows.Forms.Button
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtConfirm = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtUserID = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtLastName = New System.Windows.Forms.TextBox
        Me.txtFirstName = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Step2 = New System.Windows.Forms.Panel
        Me.grpSMTP = New System.Windows.Forms.GroupBox
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel
        Me.Label8 = New System.Windows.Forms.Label
        Me.cmbSMTPTimeout = New System.Windows.Forms.NumericUpDown
        Me.txtSMTPUserID = New System.Windows.Forms.TextBox
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtSMTPSenderName = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.txtSMTPSenderAddress = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtSMTPServer = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtSMTPPassword = New System.Windows.Forms.TextBox
        Me.grpGroupwise = New System.Windows.Forms.GroupBox
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel
        Me.Label28 = New System.Windows.Forms.Label
        Me.txtgwUser = New System.Windows.Forms.TextBox
        Me.Label29 = New System.Windows.Forms.Label
        Me.Label41 = New System.Windows.Forms.Label
        Me.Label45 = New System.Windows.Forms.Label
        Me.Label46 = New System.Windows.Forms.Label
        Me.txtgwPassword = New System.Windows.Forms.TextBox
        Me.txtgwProxy = New System.Windows.Forms.TextBox
        Me.txtgwPOIP = New System.Windows.Forms.TextBox
        Me.txtgwPOPort = New System.Windows.Forms.TextBox
        Me.grpMAPI = New System.Windows.Forms.GroupBox
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel
        Me.Label13 = New System.Windows.Forms.Label
        Me.txtMAPIPassword = New System.Windows.Forms.TextBox
        Me.cmbMAPIProfile = New System.Windows.Forms.ComboBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.cmdMailTest = New System.Windows.Forms.Button
        Me.cmbMailType = New System.Windows.Forms.ComboBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Step3 = New System.Windows.Forms.Panel
        Me.lnkProcess = New System.Windows.Forms.LinkLabel
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel
        Me.Label15 = New System.Windows.Forms.Label
        Me.txtCustomerNo = New System.Windows.Forms.TextBox
        Me.Step4 = New System.Windows.Forms.Panel
        Me.chkAdvanced = New System.Windows.Forms.CheckBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.toolTip = New DevComponents.DotNetBar.SuperTooltip
        Me.DividerLabel1 = New sqlrd.DividerLabel
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Step1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Step2.SuspendLayout()
        Me.grpSMTP.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        CType(Me.cmbSMTPTimeout, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpGroupwise.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.grpMAPI.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.Step3.SuspendLayout()
        Me.TableLayoutPanel5.SuspendLayout()
        Me.Step4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.cmdFinish)
        Me.Panel2.Controls.Add(Me.cmdNext)
        Me.Panel2.Controls.Add(Me.cmdCancel)
        Me.Panel2.Controls.Add(Me.cmdBack)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 483)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(628, 48)
        Me.Panel2.TabIndex = 3
        '
        'cmdFinish
        '
        Me.cmdFinish.BackColor = System.Drawing.Color.Transparent
        Me.cmdFinish.Image = CType(resources.GetObject("cmdFinish.Image"), System.Drawing.Image)
        Me.cmdFinish.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdFinish.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdFinish.Location = New System.Drawing.Point(549, 10)
        Me.cmdFinish.Name = "cmdFinish"
        Me.cmdFinish.Size = New System.Drawing.Size(73, 25)
        Me.cmdFinish.TabIndex = 50
        Me.cmdFinish.Text = "&Finish"
        Me.cmdFinish.UseVisualStyleBackColor = False
        Me.cmdFinish.Visible = False
        '
        'cmdNext
        '
        Me.cmdNext.BackColor = System.Drawing.Color.Transparent
        Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
        Me.cmdNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(549, 11)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(73, 25)
        Me.cmdNext.TabIndex = 49
        Me.cmdNext.Text = "&Next"
        Me.cmdNext.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.Color.Transparent
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(461, 11)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(73, 25)
        Me.cmdCancel.TabIndex = 48
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'cmdBack
        '
        Me.cmdBack.BackColor = System.Drawing.Color.Transparent
        Me.cmdBack.Enabled = False
        Me.cmdBack.Image = CType(resources.GetObject("cmdBack.Image"), System.Drawing.Image)
        Me.cmdBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBack.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBack.Location = New System.Drawing.Point(373, 11)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(73, 25)
        Me.cmdBack.TabIndex = 47
        Me.cmdBack.Text = "&Back"
        Me.cmdBack.UseVisualStyleBackColor = False
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.tvSteps)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel3.Location = New System.Drawing.Point(0, 162)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(267, 321)
        Me.Panel3.TabIndex = 4
        '
        'tvSteps
        '
        Me.tvSteps.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tvSteps.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvSteps.ImageIndex = 0
        Me.tvSteps.ImageList = Me.ImageList1
        Me.tvSteps.Location = New System.Drawing.Point(0, 0)
        Me.tvSteps.Name = "tvSteps"
        TreeNode1.ForeColor = System.Drawing.Color.Black
        TreeNode1.ImageIndex = 1
        TreeNode1.Name = "Node1"
        TreeNode1.NodeFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TreeNode1.SelectedImageIndex = 1
        TreeNode1.Text = "Create Admin user"
        TreeNode2.ForeColor = System.Drawing.Color.Silver
        TreeNode2.ImageIndex = 2
        TreeNode2.Name = "Node2"
        TreeNode2.NodeFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TreeNode2.SelectedImageIndex = 2
        TreeNode2.Text = "Configure messaging (optional)"
        TreeNode3.ForeColor = System.Drawing.Color.Silver
        TreeNode3.ImageIndex = 3
        TreeNode3.Name = "Node3"
        TreeNode3.SelectedImageIndex = 3
        TreeNode3.Text = "Register for free support (optional)"
        TreeNode4.ForeColor = System.Drawing.Color.Silver
        TreeNode4.ImageIndex = 4
        TreeNode4.Name = "Node4"
        TreeNode4.SelectedImageIndex = 4
        TreeNode4.Text = "Finish"
        TreeNode5.Name = "Node0"
        TreeNode5.NodeFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TreeNode5.Text = "Getting Started with SQL-RD"
        Me.tvSteps.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode5})
        Me.tvSteps.SelectedImageIndex = 0
        Me.tvSteps.ShowLines = False
        Me.tvSteps.ShowPlusMinus = False
        Me.tvSteps.ShowRootLines = False
        Me.tvSteps.Size = New System.Drawing.Size(267, 321)
        Me.tvSteps.TabIndex = 0
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "node.png")
        Me.ImageList1.Images.SetKeyName(1, "user1_add.ico")
        Me.ImageList1.Images.SetKeyName(2, "mail_earth.ico")
        Me.ImageList1.Images.SetKeyName(3, "doctor.ico")
        Me.ImageList1.Images.SetKeyName(4, "check2.ico")
        '
        'Step1
        '
        Me.Step1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Step1.Controls.Add(Me.cmdSaveUser)
        Me.Step1.Controls.Add(Me.TableLayoutPanel1)
        Me.Step1.Controls.Add(Me.Label1)
        Me.Step1.Location = New System.Drawing.Point(273, 162)
        Me.Step1.Name = "Step1"
        Me.Step1.Size = New System.Drawing.Size(350, 317)
        Me.Step1.TabIndex = 5
        Me.Step1.Visible = False
        '
        'cmdSaveUser
        '
        Me.cmdSaveUser.Image = CType(resources.GetObject("cmdSaveUser.Image"), System.Drawing.Image)
        Me.cmdSaveUser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSaveUser.Location = New System.Drawing.Point(243, 188)
        Me.cmdSaveUser.Name = "cmdSaveUser"
        Me.cmdSaveUser.Size = New System.Drawing.Size(75, 23)
        Me.cmdSaveUser.TabIndex = 1
        Me.cmdSaveUser.Text = "Save"
        Me.cmdSaveUser.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtConfirm, 1, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.Label5, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtPassword, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.txtUserID, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label6, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.txtLastName, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtFirstName, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(8, 46)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.Padding = New System.Windows.Forms.Padding(5)
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(337, 135)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 5)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(113, 25)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "First Name"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtConfirm
        '
        Me.txtConfirm.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtConfirm.Location = New System.Drawing.Point(127, 108)
        Me.txtConfirm.Name = "txtConfirm"
        Me.txtConfirm.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtConfirm.Size = New System.Drawing.Size(183, 21)
        Me.txtConfirm.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(8, 30)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(113, 25)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Last Name"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPassword
        '
        Me.txtPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtPassword.Location = New System.Drawing.Point(127, 83)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtPassword.Size = New System.Drawing.Size(183, 21)
        Me.txtPassword.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 55)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(113, 25)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "UserID"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtUserID
        '
        Me.txtUserID.Location = New System.Drawing.Point(127, 58)
        Me.txtUserID.Name = "txtUserID"
        Me.txtUserID.Size = New System.Drawing.Size(183, 21)
        Me.txtUserID.TabIndex = 2
        '
        'Label6
        '
        Me.Label6.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(8, 105)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(113, 25)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Confirm Password"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 80)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(113, 25)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Password"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLastName
        '
        Me.txtLastName.Location = New System.Drawing.Point(127, 33)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(183, 21)
        Me.txtLastName.TabIndex = 1
        '
        'txtFirstName
        '
        Me.txtFirstName.Location = New System.Drawing.Point(127, 8)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(183, 21)
        Me.txtFirstName.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(318, 36)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Please set up an admnistrator user so that you may log into the system"
        '
        'Step2
        '
        Me.Step2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Step2.Controls.Add(Me.grpSMTP)
        Me.Step2.Controls.Add(Me.grpGroupwise)
        Me.Step2.Controls.Add(Me.grpMAPI)
        Me.Step2.Controls.Add(Me.cmdMailTest)
        Me.Step2.Controls.Add(Me.cmbMailType)
        Me.Step2.Controls.Add(Me.Label7)
        Me.Step2.Location = New System.Drawing.Point(273, 162)
        Me.Step2.Name = "Step2"
        Me.Step2.Size = New System.Drawing.Size(350, 317)
        Me.Step2.TabIndex = 6
        Me.Step2.Visible = False
        '
        'grpSMTP
        '
        Me.grpSMTP.Controls.Add(Me.TableLayoutPanel3)
        Me.grpSMTP.Location = New System.Drawing.Point(6, 48)
        Me.grpSMTP.Name = "grpSMTP"
        Me.grpSMTP.Size = New System.Drawing.Size(297, 216)
        Me.grpSMTP.TabIndex = 1
        Me.grpSMTP.TabStop = False
        Me.grpSMTP.Text = "SMTP Mail"
        Me.grpSMTP.Visible = False
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel3.Controls.Add(Me.Label8, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.cmbSMTPTimeout, 1, 5)
        Me.TableLayoutPanel3.Controls.Add(Me.txtSMTPUserID, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Label19, 0, 5)
        Me.TableLayoutPanel3.Controls.Add(Me.Label9, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.txtSMTPSenderName, 1, 4)
        Me.TableLayoutPanel3.Controls.Add(Me.Label12, 0, 4)
        Me.TableLayoutPanel3.Controls.Add(Me.txtSMTPSenderAddress, 1, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.Label11, 0, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.txtSMTPServer, 1, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Label10, 0, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.txtSMTPPassword, 1, 1)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(10, 28)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.Padding = New System.Windows.Forms.Padding(5)
        Me.TableLayoutPanel3.RowCount = 6
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(276, 171)
        Me.TableLayoutPanel3.TabIndex = 8
        '
        'Label8
        '
        Me.Label8.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(8, 5)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(88, 26)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "User ID"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbSMTPTimeout
        '
        Me.cmbSMTPTimeout.Location = New System.Drawing.Point(102, 138)
        Me.cmbSMTPTimeout.Maximum = New Decimal(New Integer() {360, 0, 0, 0})
        Me.cmbSMTPTimeout.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.cmbSMTPTimeout.Name = "cmbSMTPTimeout"
        Me.cmbSMTPTimeout.Size = New System.Drawing.Size(57, 21)
        Me.cmbSMTPTimeout.TabIndex = 5
        Me.cmbSMTPTimeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.cmbSMTPTimeout.Value = New Decimal(New Integer() {30, 0, 0, 0})
        '
        'txtSMTPUserID
        '
        Me.txtSMTPUserID.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPUserID.Location = New System.Drawing.Point(102, 8)
        Me.txtSMTPUserID.Name = "txtSMTPUserID"
        Me.txtSMTPUserID.Size = New System.Drawing.Size(152, 21)
        Me.txtSMTPUserID.TabIndex = 0
        '
        'Label19
        '
        Me.Label19.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label19.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label19.Location = New System.Drawing.Point(8, 135)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(88, 31)
        Me.Label19.TabIndex = 2
        Me.Label19.Text = "SMTP Timeout"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(8, 31)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(88, 26)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Password"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSMTPSenderName
        '
        Me.txtSMTPSenderName.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPSenderName.Location = New System.Drawing.Point(102, 112)
        Me.txtSMTPSenderName.Name = "txtSMTPSenderName"
        Me.txtSMTPSenderName.Size = New System.Drawing.Size(152, 21)
        Me.txtSMTPSenderName.TabIndex = 4
        '
        'Label12
        '
        Me.Label12.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(8, 109)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(88, 26)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Sender Name"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSMTPSenderAddress
        '
        Me.txtSMTPSenderAddress.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPSenderAddress.Location = New System.Drawing.Point(102, 86)
        Me.txtSMTPSenderAddress.Name = "txtSMTPSenderAddress"
        Me.txtSMTPSenderAddress.Size = New System.Drawing.Size(152, 21)
        Me.txtSMTPSenderAddress.TabIndex = 3
        '
        'Label11
        '
        Me.Label11.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(8, 83)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(88, 26)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Sender Address"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSMTPServer
        '
        Me.txtSMTPServer.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPServer.Location = New System.Drawing.Point(102, 60)
        Me.txtSMTPServer.Name = "txtSMTPServer"
        Me.txtSMTPServer.Size = New System.Drawing.Size(152, 21)
        Me.txtSMTPServer.TabIndex = 2
        '
        'Label10
        '
        Me.Label10.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(8, 57)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(88, 26)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Server Name"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSMTPPassword
        '
        Me.txtSMTPPassword.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtSMTPPassword.Location = New System.Drawing.Point(102, 34)
        Me.txtSMTPPassword.Name = "txtSMTPPassword"
        Me.txtSMTPPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtSMTPPassword.Size = New System.Drawing.Size(152, 21)
        Me.txtSMTPPassword.TabIndex = 1
        '
        'grpGroupwise
        '
        Me.grpGroupwise.Controls.Add(Me.TableLayoutPanel2)
        Me.grpGroupwise.Location = New System.Drawing.Point(6, 48)
        Me.grpGroupwise.Name = "grpGroupwise"
        Me.grpGroupwise.Size = New System.Drawing.Size(297, 216)
        Me.grpGroupwise.TabIndex = 12
        Me.grpGroupwise.TabStop = False
        Me.grpGroupwise.Text = "Groupwise"
        Me.grpGroupwise.Visible = False
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel2.Controls.Add(Me.Label28, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txtgwUser, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label29, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Label41, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.Label45, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.Label46, 0, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.txtgwPassword, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.txtgwProxy, 1, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.txtgwPOIP, 1, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.txtgwPOPort, 1, 4)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(8, 28)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 5
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(278, 135)
        Me.TableLayoutPanel2.TabIndex = 2
        '
        'Label28
        '
        Me.Label28.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(3, 0)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(43, 27)
        Me.Label28.TabIndex = 0
        Me.Label28.Text = "User ID"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtgwUser
        '
        Me.txtgwUser.Location = New System.Drawing.Point(90, 3)
        Me.txtgwUser.Name = "txtgwUser"
        Me.txtgwUser.Size = New System.Drawing.Size(164, 21)
        Me.txtgwUser.TabIndex = 1
        '
        'Label29
        '
        Me.Label29.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(3, 27)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(53, 27)
        Me.Label29.TabIndex = 0
        Me.Label29.Text = "Password"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label41
        '
        Me.Label41.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(3, 54)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(49, 27)
        Me.Label41.TabIndex = 0
        Me.Label41.Text = "Proxy ID"
        Me.Label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label45
        '
        Me.Label45.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(3, 81)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(81, 27)
        Me.Label45.TabIndex = 0
        Me.Label45.Text = "Server Address"
        Me.Label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label46
        '
        Me.Label46.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(3, 108)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(62, 27)
        Me.Label46.TabIndex = 0
        Me.Label46.Text = "Server Port"
        Me.Label46.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtgwPassword
        '
        Me.txtgwPassword.Location = New System.Drawing.Point(90, 30)
        Me.txtgwPassword.Name = "txtgwPassword"
        Me.txtgwPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtgwPassword.Size = New System.Drawing.Size(164, 21)
        Me.txtgwPassword.TabIndex = 1
        '
        'txtgwProxy
        '
        Me.txtgwProxy.Location = New System.Drawing.Point(90, 57)
        Me.txtgwProxy.Name = "txtgwProxy"
        Me.txtgwProxy.Size = New System.Drawing.Size(164, 21)
        Me.txtgwProxy.TabIndex = 1
        '
        'txtgwPOIP
        '
        Me.txtgwPOIP.Location = New System.Drawing.Point(90, 84)
        Me.txtgwPOIP.Name = "txtgwPOIP"
        Me.txtgwPOIP.Size = New System.Drawing.Size(164, 21)
        Me.txtgwPOIP.TabIndex = 1
        '
        'txtgwPOPort
        '
        Me.txtgwPOPort.Location = New System.Drawing.Point(90, 111)
        Me.txtgwPOPort.Name = "txtgwPOPort"
        Me.txtgwPOPort.Size = New System.Drawing.Size(164, 21)
        Me.txtgwPOPort.TabIndex = 1
        '
        'grpMAPI
        '
        Me.grpMAPI.Controls.Add(Me.TableLayoutPanel4)
        Me.grpMAPI.Location = New System.Drawing.Point(6, 48)
        Me.grpMAPI.Name = "grpMAPI"
        Me.grpMAPI.Size = New System.Drawing.Size(297, 216)
        Me.grpMAPI.TabIndex = 14
        Me.grpMAPI.TabStop = False
        Me.grpMAPI.Text = "MAPI Mail"
        Me.grpMAPI.Visible = False
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 2
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel4.Controls.Add(Me.Label13, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.txtMAPIPassword, 1, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.cmbMAPIProfile, 1, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label14, 0, 1)
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(11, 24)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.Padding = New System.Windows.Forms.Padding(5)
        Me.TableLayoutPanel4.RowCount = 2
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(258, 59)
        Me.TableLayoutPanel4.TabIndex = 2
        '
        'Label13
        '
        Me.Label13.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(8, 5)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(88, 24)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "MAPI Profile"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMAPIPassword
        '
        Me.txtMAPIPassword.ForeColor = System.Drawing.Color.Blue
        Me.txtMAPIPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtMAPIPassword.Location = New System.Drawing.Point(102, 32)
        Me.txtMAPIPassword.Name = "txtMAPIPassword"
        Me.txtMAPIPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtMAPIPassword.Size = New System.Drawing.Size(149, 21)
        Me.txtMAPIPassword.TabIndex = 1
        '
        'cmbMAPIProfile
        '
        Me.cmbMAPIProfile.ForeColor = System.Drawing.Color.Blue
        Me.cmbMAPIProfile.ItemHeight = 13
        Me.cmbMAPIProfile.Location = New System.Drawing.Point(102, 8)
        Me.cmbMAPIProfile.Name = "cmbMAPIProfile"
        Me.cmbMAPIProfile.Size = New System.Drawing.Size(149, 21)
        Me.cmbMAPIProfile.TabIndex = 0
        '
        'Label14
        '
        Me.Label14.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(8, 29)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(88, 25)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Password"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmdMailTest
        '
        Me.cmdMailTest.BackColor = System.Drawing.Color.Transparent
        Me.cmdMailTest.Image = CType(resources.GetObject("cmdMailTest.Image"), System.Drawing.Image)
        Me.cmdMailTest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdMailTest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdMailTest.Location = New System.Drawing.Point(199, 268)
        Me.cmdMailTest.Name = "cmdMailTest"
        Me.cmdMailTest.Size = New System.Drawing.Size(104, 24)
        Me.cmdMailTest.TabIndex = 2
        Me.cmdMailTest.Text = "Test Settings"
        Me.cmdMailTest.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdMailTest.UseVisualStyleBackColor = False
        Me.cmdMailTest.Visible = False
        '
        'cmbMailType
        '
        Me.cmbMailType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMailType.ForeColor = System.Drawing.Color.Blue
        Me.cmbMailType.ItemHeight = 13
        Me.cmbMailType.Items.AddRange(New Object() {"MAPI", "SMTP", "GROUPWISE", "NONE"})
        Me.cmbMailType.Location = New System.Drawing.Point(71, 8)
        Me.cmbMailType.Name = "cmbMailType"
        Me.cmbMailType.Size = New System.Drawing.Size(199, 21)
        Me.cmbMailType.TabIndex = 0
        Me.cmbMailType.Tag = "unsorted"
        '
        'Label7
        '
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(3, 11)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(100, 16)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Mail Type"
        '
        'Step3
        '
        Me.Step3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Step3.Controls.Add(Me.lnkProcess)
        Me.Step3.Controls.Add(Me.TableLayoutPanel5)
        Me.Step3.Location = New System.Drawing.Point(273, 162)
        Me.Step3.Name = "Step3"
        Me.Step3.Size = New System.Drawing.Size(350, 317)
        Me.Step3.TabIndex = 0
        Me.Step3.Visible = False
        '
        'lnkProcess
        '
        Me.lnkProcess.Location = New System.Drawing.Point(9, 51)
        Me.lnkProcess.Name = "lnkProcess"
        Me.lnkProcess.Size = New System.Drawing.Size(336, 30)
        Me.lnkProcess.TabIndex = 1
        Me.lnkProcess.TabStop = True
        Me.lnkProcess.Text = "Click here to register for support and get your Customer number for free"
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.ColumnCount = 2
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel5.Controls.Add(Me.Label15, 0, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.txtCustomerNo, 1, 0)
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(12, 11)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 1
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(285, 29)
        Me.TableLayoutPanel5.TabIndex = 4
        '
        'Label15
        '
        Me.Label15.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(3, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(83, 29)
        Me.Label15.TabIndex = 2
        Me.Label15.Text = "Customer #"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCustomerNo
        '
        Me.txtCustomerNo.ForeColor = System.Drawing.Color.Blue
        Me.txtCustomerNo.Location = New System.Drawing.Point(92, 3)
        Me.txtCustomerNo.Name = "txtCustomerNo"
        Me.txtCustomerNo.Size = New System.Drawing.Size(182, 21)
        Me.txtCustomerNo.TabIndex = 0
        '
        'Step4
        '
        Me.Step4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Step4.Controls.Add(Me.chkAdvanced)
        Me.Step4.Controls.Add(Me.Label16)
        Me.Step4.Location = New System.Drawing.Point(273, 162)
        Me.Step4.Name = "Step4"
        Me.Step4.Size = New System.Drawing.Size(350, 317)
        Me.Step4.TabIndex = 0
        Me.Step4.Visible = False
        '
        'chkAdvanced
        '
        Me.chkAdvanced.AutoSize = True
        Me.chkAdvanced.Location = New System.Drawing.Point(12, 163)
        Me.chkAdvanced.Name = "chkAdvanced"
        Me.chkAdvanced.Size = New System.Drawing.Size(323, 17)
        Me.chkAdvanced.TabIndex = 1
        Me.chkAdvanced.Text = "Go to configure advanced  system settings when I click 'Finish'"
        Me.chkAdvanced.UseVisualStyleBackColor = True
        '
        'Label16
        '
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(9, 22)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(336, 111)
        Me.Label16.TabIndex = 0
        Me.Label16.Text = "Congratulations! You have successfully finished setting up your system. Click 'Fi" & _
            "nish' to exit the Wizard."
        '
        'toolTip
        '
        Me.toolTip.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.toolTip.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.toolTip.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'DividerLabel1
        '
        Me.DividerLabel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(0, 152)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(628, 10)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 2
        '
        'Panel1
        '
        Me.Panel1.BackgroundImage = CType(resources.GetObject("Panel1.BackgroundImage"), System.Drawing.Image)
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(628, 152)
        Me.Panel1.TabIndex = 7
        '
        'frmStarterWizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(628, 531)
        Me.Controls.Add(Me.Step1)
        Me.Controls.Add(Me.Step2)
        Me.Controls.Add(Me.Step4)
        Me.Controls.Add(Me.Step3)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.DividerLabel1)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmStarterWizard"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Welcome to SQL-RD"
        Me.Panel2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Step1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.Step2.ResumeLayout(False)
        Me.grpSMTP.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        CType(Me.cmbSMTPTimeout, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpGroupwise.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.grpMAPI.ResumeLayout(False)
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.TableLayoutPanel4.PerformLayout()
        Me.Step3.ResumeLayout(False)
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.TableLayoutPanel5.PerformLayout()
        Me.Step4.ResumeLayout(False)
        Me.Step4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents tvSteps As System.Windows.Forms.TreeView
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Step1 As System.Windows.Forms.Panel
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtConfirm As System.Windows.Forms.TextBox
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents txtUserID As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtFirstName As System.Windows.Forms.TextBox
    Friend WithEvents txtLastName As System.Windows.Forms.TextBox
    Friend WithEvents Step2 As System.Windows.Forms.Panel
    Friend WithEvents cmbMailType As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents grpGroupwise As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txtgwUser As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents txtgwPassword As System.Windows.Forms.TextBox
    Friend WithEvents txtgwProxy As System.Windows.Forms.TextBox
    Friend WithEvents txtgwPOIP As System.Windows.Forms.TextBox
    Friend WithEvents txtgwPOPort As System.Windows.Forms.TextBox
    Friend WithEvents grpSMTP As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtSMTPUserID As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtSMTPPassword As System.Windows.Forms.TextBox
    Friend WithEvents cmbSMTPTimeout As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtSMTPServer As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtSMTPSenderAddress As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtSMTPSenderName As System.Windows.Forms.TextBox
    Friend WithEvents grpMAPI As System.Windows.Forms.GroupBox
    Friend WithEvents txtMAPIPassword As System.Windows.Forms.TextBox
    Friend WithEvents cmbMAPIProfile As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents cmdMailTest As System.Windows.Forms.Button
    Friend WithEvents Step3 As System.Windows.Forms.Panel
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtCustomerNo As System.Windows.Forms.TextBox
    Friend WithEvents lnkProcess As System.Windows.Forms.LinkLabel
    Friend WithEvents TableLayoutPanel5 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Step4 As System.Windows.Forms.Panel
    Friend WithEvents chkAdvanced As System.Windows.Forms.CheckBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents cmdFinish As System.Windows.Forms.Button
    Friend WithEvents cmdNext As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdBack As System.Windows.Forms.Button
    Friend WithEvents toolTip As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents cmdSaveUser As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
End Class
