Public Class frmItemSelector

    Dim userCancel As Boolean = False
    Dim ep As New ErrorProvider

    Public Function chooseTemplate(ByVal scheduleList As Hashtable) As Integer
        For Each item As DictionaryEntry In scheduleList
            Me.cmbSchedules.Items.Add(item.Key)
        Next

        Me.ShowDialog()

        If userCancel = True Then
            Return 0
        Else
            Return scheduleList.Item(Me.cmbSchedules.Text)
        End If
    End Function

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If Me.cmbSchedules.Text.Length = 0 Then
            ep.SetError(cmbSchedules, "Please select the schedule to use as a template")
            Me.cmbSchedules.Focus()
            Return
        End If

        Close()
    End Sub

    Private Sub cmbSchedules_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSchedules.SelectedIndexChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Close()
    End Sub

    Private Sub frmItemSelector_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub
End Class