<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReadReciepts
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReadReciepts))
        Me.chkRepeat = New System.Windows.Forms.CheckBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtRepeat = New System.Windows.Forms.NumericUpDown
        Me.txtCheck = New System.Windows.Forms.NumericUpDown
        Me.tbSetup = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.UcTasks1 = New sqlrd.ucTasks
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel
        Me.DividerLabel1 = New sqlrd.DividerLabel
        CType(Me.txtRepeat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCheck, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'chkRepeat
        '
        Me.chkRepeat.BackColor = System.Drawing.Color.Transparent
        Me.chkRepeat.Location = New System.Drawing.Point(3, 3)
        Me.chkRepeat.Name = "chkRepeat"
        Me.chkRepeat.Size = New System.Drawing.Size(201, 21)
        Me.chkRepeat.TabIndex = 4
        Me.chkRepeat.Text = "Repeat above tasks every (minutes)"
        Me.chkRepeat.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Location = New System.Drawing.Point(278, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(151, 24)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "until a read reciept is recieved"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(383, 24)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Carry out the following tasks if a read reciept has not been recieved in (mins)"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRepeat
        '
        Me.txtRepeat.DecimalPlaces = 1
        Me.txtRepeat.Enabled = False
        Me.txtRepeat.Increment = New Decimal(New Integer() {5, 0, 0, 65536})
        Me.txtRepeat.Location = New System.Drawing.Point(210, 3)
        Me.txtRepeat.Maximum = New Decimal(New Integer() {1440, 0, 0, 0})
        Me.txtRepeat.Minimum = New Decimal(New Integer() {5, 0, 0, 65536})
        Me.txtRepeat.Name = "txtRepeat"
        Me.txtRepeat.Size = New System.Drawing.Size(62, 21)
        Me.txtRepeat.TabIndex = 0
        Me.txtRepeat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtRepeat.Value = New Decimal(New Integer() {5, 0, 0, 65536})
        '
        'txtCheck
        '
        Me.txtCheck.DecimalPlaces = 1
        Me.txtCheck.Increment = New Decimal(New Integer() {5, 0, 0, 65536})
        Me.txtCheck.Location = New System.Drawing.Point(392, 3)
        Me.txtCheck.Maximum = New Decimal(New Integer() {1440, 0, 0, 0})
        Me.txtCheck.Minimum = New Decimal(New Integer() {5, 0, 0, 65536})
        Me.txtCheck.Name = "txtCheck"
        Me.txtCheck.Size = New System.Drawing.Size(62, 21)
        Me.txtCheck.TabIndex = 0
        Me.txtCheck.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCheck.Value = New Decimal(New Integer() {5, 0, 0, 65536})
        '
        'tbSetup
        '
        Me.tbSetup.CloseButtonBounds = New System.Drawing.Rectangle(0, 0, 0, 0)
        Me.tbSetup.Name = "tbSetup"
        Me.tbSetup.Text = "Read Reciepts"
        '
        'cmdOK
        '
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(301, 389)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 5
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(383, 389)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 4
        Me.cmdCancel.Text = "&Cancel"
        '
        'UcTasks1
        '
        Me.UcTasks1.BackColor = System.Drawing.Color.Transparent
        Me.UcTasks1.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcTasks1.ForeColor = System.Drawing.Color.Navy
        Me.UcTasks1.Location = New System.Drawing.Point(7, 46)
        Me.UcTasks1.Name = "UcTasks1"
        Me.UcTasks1.Size = New System.Drawing.Size(456, 294)
        Me.UcTasks1.TabIndex = 3
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtCheck)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(12, 12)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(465, 28)
        Me.FlowLayoutPanel1.TabIndex = 6
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.chkRepeat)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtRepeat)
        Me.FlowLayoutPanel2.Controls.Add(Me.Label2)
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(18, 346)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(443, 27)
        Me.FlowLayoutPanel2.TabIndex = 7
        '
        'DividerLabel1
        '
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(-8, 376)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(558, 10)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 8
        '
        'frmReadReciepts
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(470, 416)
        Me.Controls.Add(Me.DividerLabel1)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.UcTasks1)
        Me.Controls.Add(Me.cmdCancel)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmReadReciepts"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Read Reciepts"
        CType(Me.txtRepeat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCheck, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tbSetup As DevComponents.DotNetBar.TabItem
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtCheck As System.Windows.Forms.NumericUpDown
    Friend WithEvents UcTasks1 As sqlrd.ucTasks
    Friend WithEvents chkRepeat As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtRepeat As System.Windows.Forms.NumericUpDown
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
End Class
