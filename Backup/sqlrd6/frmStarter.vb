Imports sqlrd.clsMarsUI
Public Class frmStarter
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW As Object = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property

    Private Sub frmStarter_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oFile As FileVersionInfo = FileVersionInfo.GetVersionInfo(sAppPath & "sqlrd.exe")

        lblVersion.Text = "SQL-RD " & oFile.FileMajorPart & "." & oFile.FileMinorPart & " " & oFile.Comments

        lblVersion.Text &= " " & gsEdition & " Edition"

        Me.lblCompany.Text = MainUI.ReadRegistry("RegCo", "")
        Me.lblUser.Text = MainUI.ReadRegistry("RegUser", "")
        Me.lblLicense.Text = "License #: " & MainUI.ReadRegistry("RegNo", "")
        Me.lblCustID.Text = "Customer #: " & MainUI.ReadRegistry("CustNo", "")

        Me.chkDonotshow.Checked = Convert.ToBoolean(Convert.ToInt16(MainUI.ReadRegistry("DontShowWelcomeScreen", 0)))
    End Sub

    Private Sub lblMore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblMore.Click, _
    picMore.Click
        Dim opt As frmOptions = New frmOptions

        opt.ShowDialog(Me)

    End Sub

    Private Sub lblSingle_LinkClicked(ByVal sender As System.Object, _
    ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lblSingle.LinkClicked
        Dim frmSingle As New frmSingleScheduleWizard

        frmSingle.ShowDialog(Me)
    End Sub


    Private Sub lblPackage_LinkClicked(ByVal sender As System.Object, _
    ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lblPackage.LinkClicked
        Dim frmPackage As New frmPackWizard

        frmPackage.ShowDialog(Me)
    End Sub

    Private Sub lblAuto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAuto.Click
        Dim frmAuto As New frmAutoScheduleWizard

        frmAuto.ShowDialog(Me)
    End Sub


    Private Sub lblEvent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblEvent.Click
        Dim frmEvent As New frmEventWizard6

        frmEvent.ShowDialog(Me)
    End Sub

    Private Sub picSingle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles picSingle.Click
        Me.lblSingle_LinkClicked(Nothing, Nothing)
    End Sub

    
    Private Sub picPackage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles picPackage.Click
        Me.lblPackage_LinkClicked(Nothing, Nothing)
    End Sub

    Private Sub picAuto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles picAuto.Click
        Me.lblAuto_Click(Nothing, Nothing)
    End Sub

    Private Sub picEvent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles picEvent.Click
        Me.lblEvent_Click(Nothing, Nothing)
    End Sub

    Private Sub LinkLabel5_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel5.LinkClicked
        clsWebProcess.Start("http://www.christiansteven.com/sql-rd/demos/sql-rd_demos.htm") ''
    End Sub

    Private Sub chkDonotshow_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDonotshow.CheckedChanged
        MainUI.SaveRegistry("DontShowWelcomeScreen", Convert.ToInt16(Me.chkDonotshow.Checked))
    End Sub

    Private Sub picDemoSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles picDemoSingle.Click
        Dim viewer As frmWebBrowser = New frmWebBrowser

        viewer.viewPage("http://www.christiansteven.com/sql-rd/demos/single%20schedule/Single%20Schedule%20Wizard.htm", _
        False, "Single Schedule Wizard Demo") ''
    End Sub

    Private Sub picDemoPackage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picDemoPackage.Click
        Dim viewer As frmWebBrowser = New frmWebBrowser

        viewer.viewPage("http://www.christiansteven.com/sql-rd/demos/package%20schedule/Package%20Schedule%20Wizard.htm", _
        False, "Package Schedule Wizard Demo") ''
    End Sub

    Private Sub picDemoAuto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picDemoAuto.Click
        Dim viewer As frmWebBrowser = New frmWebBrowser

        viewer.viewPage("http://www.christiansteven.com/sql-rd/demos/automation/Automation%20Schedule%20Wizard.htm", _
        False, "Automation Schedule Wizard Demo") ''
    End Sub

    Private Sub picDemoEvent_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles picDemoEvent.MouseClick
        Me.mnuType.Show(picDemoEvent, New Point(e.X, e.Y))
    End Sub

    Private Sub DatabaseRecordExistsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DatabaseRecordExistsToolStripMenuItem.Click
        Dim viewer As frmWebBrowser = New frmWebBrowser

        viewer.viewPage("http://www.christiansteven.com/sql-rd/demos/event%20based%20schedule/db%20record/Event%20Based%20Schedule%20-%20Database%20Record.htm", _
        False, "Event-Based Schedule Wizard Demo") ''
    End Sub

    Private Sub DatabaseRecordHasBeenModifiedToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DatabaseRecordHasBeenModifiedToolStripMenuItem.Click
        Dim viewer As frmWebBrowser = New frmWebBrowser

        viewer.viewPage("http://www.christiansteven.com/sql-rd/demos/event%20based%20schedule/db%20record/Event%20Based%20Schedule%20-%20If%20Database%20Record%20Changes.htm", _
        False, "Event-Based Schedule Wizard Demo") ''
    End Sub

    Private Sub FileExistsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FileExistsToolStripMenuItem.Click
        Dim viewer As frmWebBrowser = New frmWebBrowser

        viewer.viewPage("http://www.christiansteven.com/sql-rd/demos/event%20based%20schedule/file/Event%20Based%20Schedule%20-%20File.htm", _
        False, "Event-Based Schedule Wizard Demo") ''
    End Sub

    Private Sub FileHasBeenModifiedToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FileHasBeenModifiedToolStripMenuItem.Click
        Dim viewer As frmWebBrowser = New frmWebBrowser

        viewer.viewPage("http://www.christiansteven.com/sql-rd/demos/event%20based%20schedule/file/Event%20Based%20Schedule%20-%20Modified%20File.htm", _
        False, "Event-Based Schedule Wizard Demo") ''
    End Sub


    Private Sub UnreadMailIsPresentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UnreadMailIsPresentToolStripMenuItem.Click
        Dim viewer As frmWebBrowser = New frmWebBrowser

        viewer.viewPage("http://www.christiansteven.com/sql-rd/demos/event%20based%20schedule/email/Event%20Based%20Schedule%20-%20If%20an%20Unread%20Email%20Exists.htm", _
        False, "Event-Based Schedule Wizard Demo") ''
    End Sub
End Class