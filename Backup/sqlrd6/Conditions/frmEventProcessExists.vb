Imports System.Windows.Forms
Public Class frmEventProcessExists
    Dim conditionType As TYPES
    Dim conditionValue As Boolean
    Dim ep As New ErrorProvider
    Dim eventType As String
    Dim userCancel As Boolean = True
    Dim eventID As Integer
    Dim edit As Boolean = False

    Public Property m_Boolean() As Boolean
        Get
            If cmbValue.Text = "FALSE" Then
                Return False
            Else
                Return True
            End If
        End Get
        Set(ByVal value As Boolean)
            conditionValue = value

            cmbValue.Text = value.ToString.ToUpper
        End Set
    End Property

    Public Enum TYPES
        PROCESS_EXISTS = 0
        WINDOW_EXISTS = 1
    End Enum
    Public Property m_Type() As TYPES
        Get
            Return conditionType
        End Get
        Set(ByVal value As TYPES)
            conditionType = value

            Select Case value

                Case TYPES.PROCESS_EXISTS
                    Me.Text = "Condition - Process Exists"

                    eventType = "process exists"

                    GroupBox1.Text = "Process Name"
                Case TYPES.WINDOW_EXISTS
                    Me.Text = "Condition - Window Exists"

                    eventType = "window is present"

                    GroupBox1.Text = "Window Caption"
            End Select

            Label4.Text = "If " & eventType & " ="
        End Set
    End Property
    Private Sub frmEventProcessExists_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        Dim saveCursor As Cursor = Windows.Forms.Cursor.Current
        Try
            Windows.Forms.Cursor.Current = Cursors.WaitCursor
            Select Case Me.m_Type
                Case TYPES.PROCESS_EXISTS
                    For Each proc As Process In Process.GetProcesses
                        cmbName.Items.Add(proc.ProcessName)
                    Next
                Case TYPES.WINDOW_EXISTS
                    For Each proc As Process In Process.GetProcesses
                        If proc.MainWindowTitle IsNot Nothing And proc.MainWindowTitle.Length > 0 Then
                            cmbName.Items.Add(proc.MainWindowTitle)
                        End If
                    Next
            End Select
        Finally
            Windows.Forms.Cursor.Current = saveCursor
        End Try
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If cmbName.Text.Length = 0 Then
            If Me.m_Type = TYPES.PROCESS_EXISTS Then
                ep.SetError(cmbName, "Please select the process name")
            Else
                ep.SetError(cmbName, "Please select the window caption")
            End If

            cmbName.Focus()
            Return
        ElseIf clsMarsEvent.IsConditionDuplicate(Me.eventID, txtName.Text) = True And edit = False Then
            ep.SetError(txtName, "A condition with this name already exists")
            txtName.Focus()
            Return
        End If

        userCancel = False

        Close()
    End Sub

    Private Sub cmbName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbName.SelectedIndexChanged
        ep.SetError(cmbName, "")
    End Sub

    Public Sub EditCondition(ByVal nConditionID As Integer)
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        edit = True

        SQL = "SELECT * FROM EventConditions WHERE ConditionID =" & nConditionID

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then

                txtName.Text = oRs("conditionname").Value

                Me.cmbName.Text = oRs("searchcriteria").Value

                Me.m_Boolean = Convert.ToBoolean(oRs("returnvalue").Value)

                eventID = oRs("eventid").Value
            End If

            oRs.Close()
        End If

        Me.ShowDialog()

        If userCancel = True Then Return

        Dim vals As String

        vals = "ConditionName = '" & SQLPrepare(txtName.Text) & "'," & _
        "ReturnValue = " & Convert.ToInt32(Me.m_Boolean) & "," & _
        "SearchCriteria = '" & SQLPrepare(cmbName.Text) & "' "

        SQL = "UPDATE EventConditions SET " & vals & " WHERE ConditionID = " & nConditionID

        clsMarsData.WriteData(SQL)
    End Sub
    Public Sub AddCondition(ByVal nEventID As Integer, ByVal nOrderID As Integer)
        eventID = nEventID

        Me.ShowDialog()

        If userCancel = True Then Return

        clsMarsUI.MainUI.BusyProgress(50, "Saving condition...")

        Dim cols As String
        Dim vals As String

        cols = "ConditionID,EventID,ConditionName,ConditionType,ReturnValue,SearchCriteria,OrderID"

        vals = clsMarsData.CreateDataID("eventconditions", "conditionid") & "," & _
        nEventID & "," & _
        "'" & SQLPrepare(txtName.Text) & "'," & _
        "'" & eventType & "'," & _
        Convert.ToInt32(Me.m_Boolean) & "," & _
        "'" & SQLPrepare(cmbName.Text) & "'," & _
        nOrderID

        clsMarsData.DataItem.InsertData("EventConditions", cols, vals, True)

        clsMarsUI.MainUI.BusyProgress(100, , True)
    End Sub


End Class