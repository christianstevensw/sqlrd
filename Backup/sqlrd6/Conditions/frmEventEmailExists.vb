Public Class frmEventEmailExists
    Const eventType As String = "unread email is present"
    Dim conditionValue As Boolean
    Dim ep As New ErrorProvider
    Dim nStep As Integer = 1
    Dim userCancel As Boolean = True
    Dim eventID As Integer
    Dim edit As Boolean = False
    Dim oField As TextBox
    Dim sField As String = "to"
    Dim m_ConditionID As Integer = 99999
    Dim m_Loaded As Boolean = False
    Dim img As ImageList = MarsCommon.InitImages
    Dim m_serverType As String = ""

    Public Property m_Boolean() As Boolean
        Get
            If cmbValue1.Text = "FALSE" Then
                Return False
            Else
                Return True
            End If
        End Get
        Set(ByVal value As Boolean)
            conditionValue = value

            cmbValue1.Text = value.ToString.ToUpper
        End Set
    End Property
    Private Sub frmEventEmailExists_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
        Page1.BringToFront()

        cmbServerType.SelectedIndex = 1

        Me.tvIMAP.ImageList = img

        txtTo.ContextMenu = Me.mnuInserter
        txtAttach.ContextMenu = Me.mnuInserter

        If Me.m_serverType <> "" Then Me.cmbServerType.Text = Me.m_serverType

        Me.m_Loaded = True
    End Sub

    Private Sub cmdTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTest.Click
        Dim oMsg As clsMarsMessaging = New clsMarsMessaging
        Dim port As Integer

        Try
            port = txtPort.Text
        Catch ex As Exception
            port = 0
        End Try

        Select Case Me.cmbServerType.Text
            Case "POP3"
                If oMsg.Pop3Login(txtUserID.Text, txtPassword.Text, txtServer.Text, port, Me.chkSSL.Checked) = True Then
                    cmdNext.Enabled = True
                    ep.SetError(txtServer, "")
                Else
                    cmdNext.Enabled = False
                    ep.SetError(txtServer, "Could not log into POP3 server. Please check the provided credentials")
                    txtServer.Focus()
                End If
            Case "IMAP4"
                Dim imap As Quiksoft.EasyMail.IMAP4.IMAP4

                If oMsg.IMAP4Login(txtUserID.Text, txtPassword.Text, txtServer.Text, port, chkSSL.Checked, imap) = True Then
                    cmdNext.Enabled = True
                    ep.SetError(txtServer, "")

                    Dim mbs As Quiksoft.EasyMail.IMAP4.MailboxCollection

                    mbs = imap.GetMailboxes()

                    drawIMAPServer(mbs)

                    imap.Logout()
                Else
                    cmdNext.Enabled = False
                    ep.SetError(txtServer, "Could not log into IMAP4 server. Please check the provided server and credentials")
                    txtServer.Focus()
                End If
        End Select

    End Sub
    Private Sub drawIMAPServer(ByVal mbs As Quiksoft.EasyMail.IMAP4.MailboxCollection)
        Try
            Dim tv As TreeView = Me.tvIMAP

            tv.Nodes.Clear()

            Dim oNode As TreeNode
            Dim oMaster As TreeNode
            Dim sPath As String
            Dim NodeFound As Boolean
            Dim I As Integer = 1
            Dim boxCount As Integer = mbs.Count


            oMaster = tv.Nodes.Add(Me.txtServer.Text)
            oMaster.ImageIndex = 0
            oMaster.SelectedImageIndex = 0
            oMaster.Tag = "master"

            For Each o As Quiksoft.EasyMail.IMAP4.Mailbox In mbs

                clsMarsUI.MainUI.BusyProgress((I / boxCount) * 100, "Loading IMAP4 server...")

                sPath = o.Name

                If sPath.Contains("/") = False Then
                    NodeFound = False

                    For Each x As TreeNode In oMaster.Nodes
                        If x.Text = o.Name Then
                            NodeFound = True
                            Exit For
                        End If
                    Next

                    If NodeFound = False Then
                        oNode = oMaster.Nodes.Add(o.Name)
                        oNode.ImageIndex = 1
                        oNode.SelectedImageIndex = 2
                        oNode.Tag = "folder"
                    End If
                Else
                    Dim StartNode As TreeNode = tv.Nodes(0)
                    Dim x As TreeNode

                    For Each s As String In sPath.Split("/")
                        If s.Length > 0 Then

                            NodeFound = False

                            For Each x In StartNode.Nodes
                                If x.Text = s And x.Tag = "folder" Then
                                    NodeFound = True
                                    Exit For
                                End If
                            Next

                            If NodeFound = False Then
                                oNode = StartNode.Nodes.Add(s)
                                oNode.Tag = "folder"
                                oNode.ImageIndex = 1
                                oNode.SelectedImageIndex = 2
                                StartNode = oNode
                            Else
                                StartNode = x
                            End If
                        End If
                    Next

                End If

                I += 1
            Next

            oMaster.Expand()

            tv.Sort()
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace))
        Finally
            clsMarsUI.BusyProgress(, , True)
        End Try
    End Sub
    Private Sub txtServer_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtServer.TextChanged, txtPort.TextChanged
        ep.SetError(txtServer, "")
    End Sub

    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click

        If Page1.Visible = True Then
            If clsMarsEvent.IsConditionDuplicate(Me.eventID, txtName.Text) = True And edit = False Then
                ep.SetError(txtName, "A condition with this name already exists")
                txtName.Focus()
                Return
            ElseIf txtName.Text.Length = 0 Then
                ep.SetError(txtName, "Please provide a name for this condition")
                txtName.Focus()
                Return
            ElseIf txtPort.Text.Length > 0 Then
                Try
                    Int32.Parse(Me.txtPort.Text)
                Catch ex As Exception
                    ep.SetError(txtPort, "Please enter a valid port")
                    txtPort.Focus()
                    Return
                End Try
            End If

            If cmbServerType.Text = "POP3" Then
                Page1.Visible = False
                Page2.Visible = True
                cmdBack.Enabled = True
            Else
                Page1a.Visible = True
                Page1.Visible = False
                cmdBack.Enabled = True
            End If
        ElseIf Page1a.Visible = True Then
            If txtIMAPPath.Text.Length = 0 Then
                MessageBox.Show("Please select the Mailbox to monitor", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                tvIMAP.Focus()
                Return
            End If

            Page2.Visible = True
            Page1a.Visible = False
        ElseIf Page2.Visible = True Then
            If cmbAnyAll.Text.Length = 0 Then
                ep.SetError(cmbAnyAll, "Please select the search operator...")
                cmbAnyAll.Focus()
                Return
            ElseIf chkSaveAttach.Checked = True And Me.txtAttach.Text.Length = 0 Then
                ep.SetError(Me.txtAttach, "Please input the path to save attachments")
                txtAttach.Focus()
                Return
            End If

            Page2.Visible = False
            Page3.Visible = True

            cmdNext.Enabled = False
            btnOK.Enabled = True
        End If

        
    End Sub
    Private Sub cmdBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBack.Click
        If Page3.Visible = True Then
            cmdNext.Enabled = True
            btnOK.Enabled = False

            Page3.Visible = False
            Page2.Visible = True
        ElseIf Page2.Visible = True Then
            If cmbServerType.Text = "IMAP4" Then
                Page1a.Visible = True
                Page2.Visible = False
            Else
                Page1.Visible = True
                Page2.Visible = False
                cmdBack.Enabled = False
            End If
        ElseIf Page1a.Visible = True Then
            Page1.Visible = True
            Page1a.Visible = False
            cmdBack.Enabled = False
        End If
    End Sub
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If cmbField.Text.Length = 0 Then
            ep.SetError(cmbField, "Please select the field to search")
            cmbField.Focus()
            Return
        ElseIf cmbOperator.Text.Length = 0 Then
            ep.SetError(cmbOperator, "Please select the search operator")
            cmbOperator.Focus()
            Return
        ElseIf cmbValue.Text.Length = 0 Then
            ep.SetError(cmbValue, "Please provide the search value")
            cmbValue.Focus()
            Return
        End If

        lsvSearch.Items.Add(cmbField.Text & " _" & cmbOperator.Text & "_ " & cmbValue.Text)

    End Sub

    Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        If lsvSearch.SelectedItems.Count = 0 Then Return

        For Each item As ListViewItem In lsvSearch.SelectedItems
            item.Remove()
        Next

    End Sub



    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click

        userCancel = False
        Close()
    End Sub

    Public Sub EditCondition(ByVal nConditionID As Integer)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim searchString As String = ""

        edit = True

        m_ConditionID = nConditionID

        SQL = "SELECT * FROM EventConditions WHERE ConditionID =" & nConditionID

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then

                searchString = oRs("searchcriteria").Value

                For Each s As String In searchString.Split("|")
                    If s.Length > 0 Then lsvSearch.Items.Add(s)
                Next

                txtName.Text = oRs("conditionname").Value
                cmbServerType.Text = IsNull(oRs("mailservertype").Value, "POP3")

                Me.m_serverType = cmbServerType.Text

                txtServer.Text = oRs("popserver").Value
                txtUserID.Text = oRs("popuser").Value
                txtPassword.Text = _DecryptDBValue(oRs("poppassword").Value)
                cmbAnyAll.Text = oRs("anyall").Value
                txtPort.Text = IsNull(oRs("serverPort").Value)
                txtIMAPPath.Text = IsNull(oRs("imapbox").Value)
                Me.chkSaveAttach.Checked = IsNull(oRs("saveattachments").Value, 0)
                Me.txtAttach.Text = IsNull(oRs("attachmentspath").Value, "")

                Me.chkSSL.Checked = Convert.ToBoolean(Convert.ToInt16(IsNull(oRs("useSSL").Value, 0)))

                Me.chkRemove.Checked = Convert.ToBoolean(oRs("popremove").Value)

                Me.m_Boolean = Convert.ToBoolean(oRs("returnvalue").Value)

                eventID = oRs("eventid").Value

                Me.txtPoll.Value = IsNull(oRs("pollinginterval").Value, 5)

                Me.txtTo.Text = IsNull(oRs("redirectto").Value, "")

                Try
                    Me.chkRedirect.Checked = Convert.ToBoolean(Convert.ToInt32(oRs("redirectmail").Value))
                Catch
                    Me.chkRedirect.Checked = False
                End Try

                Try
                    Me.chkForward.Checked = Convert.ToBoolean(Convert.ToInt32(oRs("forwardmail").Value))
                Catch ex As Exception
                    Me.chkForward.Checked = False
                End Try
            End If

            oRs.Close()
        End If

        
        Me.ShowDialog()

        searchString = ""

        If userCancel = True Then Return

        Dim vals As String

        For Each item As ListViewItem In lsvSearch.Items
            If item.Text <> "" Then searchString &= item.Text & "|"
        Next

        Dim port As Integer = 0

        If txtPort.Text.Length > 0 Then
            port = txtPort.Text
        Else
            port = 0
        End If

        vals = "ConditionName = '" & SQLPrepare(txtName.Text) & "'," & _
        "ReturnValue = " & Convert.ToInt32(Me.m_Boolean) & "," & _
        "SearchCriteria = '" & SQLPrepare(searchString) & "'," & _
        "PopServer = '" & SQLPrepare(txtServer.Text) & "'," & _
        "PopUser = '" & SQLPrepare(txtUserID.Text) & "'," & _
        "PopPassword = '" & SQLPrepare(_EncryptDBValue(txtPassword.Text)) & "'," & _
        "PopRemove = " & Convert.ToInt32(Me.chkRemove.Checked) & "," & _
        "PollingInterval = " & txtPoll.Value & "," & _
        "AnyAll = '" & cmbAnyAll.Text & "', " & _
        "RedirectMail =" & Convert.ToInt32(Me.chkRedirect.Checked) & "," & _
        "RedirectTo ='" & SQLPrepare(Me.txtTo.Text) & "', " & _
        "ForwardMail =" & Convert.ToInt32(Me.chkForward.Checked) & "," & _
        "ServerPort =" & port & "," & _
        "UseSSL =" & Convert.ToInt32(Me.chkSSL.Checked) & "," & _
        "MailServerType ='" & cmbServerType.Text & "'," & _
        "IMAPBox = '" & SQLPrepare(txtIMAPPath.Text) & "'," & _
        "SaveAttachments = " & Convert.ToInt32(Me.chkSaveAttach.Checked) & "," & _
        "AttachmentsPath = '" & SQLPrepare(Me.txtAttach.Text) & "'"


        SQL = "UPDATE EventConditions SET " & vals & " WHERE ConditionID = " & nConditionID

        clsMarsData.WriteData(SQL)

        If Me.chkForward.Checked = False Then
            clsMarsData.WriteData("DELETE FROM ForwardMailAttr WHERE ConditionID =" & nConditionID)
        End If
    End Sub

    Public Sub AddCondition(ByVal nEventID As Integer, ByVal nOrderID As Integer)
        eventID = nEventID

        Me.m_Loaded = True

        Me.ShowDialog()

        If userCancel = True Then Return

        clsMarsUI.MainUI.BusyProgress(50, "Saving condition...")

        Dim cols As String
        Dim vals As String
        Dim searchString As String
        Dim port As Integer

        For Each item As ListViewItem In lsvSearch.Items
            searchString &= item.Text & "|"
        Next

        If txtPort.Text.Length > 0 Then
            port = txtPort.Text
        Else
            port = 0
        End If

        cols = "ConditionID,EventID,ConditionName,ConditionType,ReturnValue,SearchCriteria," & _
        "PopServer,PopUser,PopPassword,PopRemove,AnyAll,PollingInterval,LastPolled,OrderID," & _
        "RedirectMail,RedirectTo,ForwardMail,serverPort,UseSSL,MailServerType,IMAPBox, SaveAttachments," & _
        "AttachmentsPath"

        m_ConditionID = clsMarsData.CreateDataID("eventconditions", "conditionid")

        vals = m_ConditionID & "," & _
        nEventID & "," & _
        "'" & SQLPrepare(txtName.Text) & "'," & _
        "'" & eventType & "'," & _
        Convert.ToInt32(Me.m_Boolean) & "," & _
        "'" & SQLPrepare(searchString) & "'," & _
        "'" & SQLPrepare(txtServer.Text) & "'," & _
        "'" & SQLPrepare(txtUserID.Text) & "'," & _
        "'" & SQLPrepare(_EncryptDBValue(txtPassword.Text)) & "'," & _
        Convert.ToInt32(Me.chkRemove.Checked) & "," & _
        "'" & cmbAnyAll.Text & "'," & _
        txtPoll.Value & "," & _
        "'" & ConDateTime(Date.Now) & "'," & _
        nOrderID & "," & _
        Convert.ToInt32(chkRedirect.Checked) & "," & _
        "'" & SQLPrepare(Me.txtTo.Text) & "'," & _
        Convert.ToInt32(Me.chkForward.Checked) & "," & _
        port & "," & _
        Convert.ToInt32(Me.chkSSL.Checked) & "," & _
        "'" & cmbServerType.Text & "'," & _
        "'" & SQLPrepare(txtIMAPPath.Text) & "'," & _
        Convert.ToInt32(Me.chkSaveAttach.Checked) & "," & _
        "'" & SQLPrepare(Me.txtAttach.Text) & "'"

        clsMarsData.DataItem.InsertData("EventConditions", cols, vals, True)

        If Me.chkForward.Checked = True Then
            Dim Sql As String = "UPDATE ForwardMailAttr SET ConditionID =" & m_ConditionID & " WHERE ConditionID = 99999"

            clsMarsData.WriteData(Sql)
        Else
            clsMarsData.WriteData("DELETE FROM ForwardMailAttr WHERE ConditionID =" & m_ConditionID)
        End If

        clsMarsUI.MainUI.BusyProgress(100, , True)
    End Sub

    
    Private Sub cmbAnyAll_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAnyAll.SelectedIndexChanged
        ep.SetError(cmbAnyAll, "")
    End Sub

    Private Sub chkForward_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkForward.CheckedChanged, btnForward.Click
        If chkForward.Checked = True And IsFeatEnabled(gEdition.CORPORATE, modFeatCodes.e2_AdvancedEventsPack) = False Then
            If Me.m_Loaded = True Then _NeedUpgrade(gEdition.CORPORATE)
            chkForward.Checked = False
            Return
        End If

        btnForward.Enabled = chkForward.Checked

        If Me.chkForward.Checked = True And Me.m_Loaded = True Then
            Dim oForward As frmTaskEmail = New frmTaskEmail

            oForward.AddForwardMail(m_ConditionID)
        End If
    End Sub

    Private Sub chkRedirect_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRedirect.CheckedChanged
        If chkRedirect.Checked = True And IsFeatEnabled(gEdition.CORPORATE, modFeatCodes.e2_AdvancedEventsPack) = False Then
            If Me.m_Loaded = True Then _NeedUpgrade(gEdition.CORPORATE)
            chkRedirect.Checked = False
            Return
        ElseIf chkRedirect.Checked And MailType <> gMailType.SQLRDMAIL And MailType <> gMailType.SMTP Then
            If Me.m_Loaded = True Then
                MessageBox.Show("Mail redirection is only available if you use SMTP or SQL-RD Mail", _
                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

                chkRedirect.Checked = False
            End If
        End If

        pnRedirect.Enabled = chkRedirect.Checked
    End Sub

    

    Private Sub cmdTo_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdTo.MouseUp
        If e.Button <> MouseButtons.Left Then Return

        oField = txtTo
        sField = "to"

        mnuContacts.Show(cmdTo, New Point(e.X, e.Y))
    End Sub

    Private Sub mnuMAPI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMAPI.Click
        Dim oMsg As clsMarsMessaging = New clsMarsMessaging

        oMsg.OutlookAddresses(oField)
    End Sub

    Private Sub mnuMARS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMARS.Click


        Dim oPicker As New frmPickContact
        Dim sValues(2) As String

        sValues = oPicker.PickContact(sField)

        If sValues Is Nothing Then Return

        If txtTo.Text.EndsWith(";") = False And txtTo.Text.Length > 0 _
        Then txtTo.Text &= ";"

        Try
            txtTo.Text &= sValues(0)
        Catch
        End Try

        
    End Sub

    Private Sub mnuDB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDB.Click
        If Not IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.m2_MailingLists) Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
            Return
        End If

        Dim oItem As New frmDataItems
        Dim sTemp As String

        sTemp = oItem._GetDataItem(0)

        If sTemp.Length > 0 Then
            If oField.Text.Length > 0 And oField.Text.EndsWith(";") = False Then oField.Text &= ";"
            oField.Text &= sTemp & ";"
        End If
    End Sub

    Private Sub mnuFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFile.Click

        If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.m2_MailingLists) Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
            Return
        End If

        ofd.Title = "Please select the file to read email addresses from"

        ofd.ShowDialog()

        If ofd.FileName.Length > 0 Then
            If oField.Text.EndsWith(";") = False And oField.Text.Length > 0 Then _
            oField.Text &= ";"

            oField.Text &= "<[f]" & ofd.FileName & ">;"
        End If
    End Sub

    Private Sub mnuConstants_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuConstants.Click
        Dim oInsert As New frmInserter(0)

        oInsert.m_EventBased = False
        oInsert.m_EventID = 0

        oInsert.GetConstants(Me)
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Dim SQL As String

        SQL = "DELETE FROM ForwardMailAttr WHERE ConditionID = 99999"

        clsMarsData.WriteData(SQL, False)
    End Sub

    Private Sub tvIMAP_AfterSelect(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvIMAP.AfterSelect
        If tvIMAP.SelectedNode Is Nothing Then
            txtIMAPPath.Text = ""
            Return
        ElseIf tvIMAP.SelectedNode Is tvIMAP.Nodes(0) Then
            txtIMAPPath.Text = ""
        Else
            Dim mailBox As String = tvIMAP.SelectedNode.FullPath
            Dim path As String = ""

            For I As Integer = 1 To mailBox.Split("/").GetLength(0) - 1
                path &= mailBox.Split("/")(I) & "/"
            Next

            If path.EndsWith("/") Then
                path = path.Substring(0, path.Length - 1)
            End If

            txtIMAPPath.Text = path
        End If

    End Sub

    Private Sub cmbServerType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbServerType.SelectedIndexChanged
        'If cmbServerType.Text = "IMAP4" Then
        '    If gnEdition < gEdition.ENTERPRISEPROPLUS Then
        '        _NeedUpgrade(gEdition.ENTERPRISEPROPLUS)
        '        cmbServerType.Text = "POP3"
        '        Return
        '    End If
        'End If

        If cmbServerType.Text = "POP3" Then
            chkRemove.Text = "Remove matched mail from server (Recommended)"
        Else
            chkRemove.Text = "Remove matched mail from server"
            If Me.m_Loaded = True Then chkRemove.Checked = False
        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSaveAttach.CheckedChanged
        If chkSaveAttach.Checked = True And IsFeatEnabled(gEdition.CORPORATE, modFeatCodes.e2_AdvancedEventsPack) = False Then
            _NeedUpgrade(gEdition.CORPORATE)
            chkSaveAttach.Checked = False
            Return
        End If

        pnAttach.Enabled = Me.chkSaveAttach.Checked
    End Sub

   
    Private Sub btnSaveAttach_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveAttach.Click
        Dim fbd As FolderBrowserDialog = New FolderBrowserDialog

        fbd.Description = "Please select the folder to save attachments to"

        fbd.ShowNewFolderButton = True

        If fbd.ShowDialog = Windows.Forms.DialogResult.OK Then
            txtAttach.Text = fbd.SelectedPath

            If txtAttach.Text.EndsWith("\") = False Then txtAttach.Text &= "\"
        End If
    End Sub

    Private Sub txtAttach_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAttach.TextChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        Try
            oField.Undo()
        Catch :End Try
    End Sub

    Private Sub txtTo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTo.GotFocus, txtAttach.GotFocus
        oField = CType(sender, TextBox)
    End Sub

    
    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        Try
            oField.Cut()
        Catch : End Try
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        Try
            oField.SelectedText = ""
        Catch : End Try
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        Try
            oField.Copy()
        Catch : End Try
    End Sub


    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        Try
            oField.Paste()
        Catch : End Try
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        Try
            oField.SelectAll()
        Catch : End Try
    End Sub

    Private Sub mnuInserter_Popup(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuInserter.Popup

    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Try
            Dim oInsert As New frmInserter(0)

            oInsert.m_EventBased = False
            oInsert.m_EventID = 0

            oInsert.GetConstants(Me)
        Catch : End Try
    End Sub

    Private Sub mnuContacts_Popup(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuContacts.Popup

    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        Try
            If Not IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.i1_Inserts) Then
                _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
                Return
            End If

            Dim oItem As New frmDataItems
            Dim sTemp As String

            sTemp = oItem._GetDataItem(0)

            If sTemp.Length > 0 Then
                If oField.Text.Length > 0 And oField.Text.EndsWith(";") = False Then oField.Text &= ";"
                oField.Text &= sTemp & ";"
            End If
        Catch : End Try
    End Sub
End Class