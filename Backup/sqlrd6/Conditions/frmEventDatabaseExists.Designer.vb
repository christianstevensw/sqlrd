<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEventDatabaseExists
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEventDatabaseExists))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Page1 = New System.Windows.Forms.GroupBox
        Me.cmbValue = New System.Windows.Forms.ComboBox
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.btnConnect = New System.Windows.Forms.Button
        Me.Page2 = New System.Windows.Forms.GroupBox
        Me.btnBuild = New System.Windows.Forms.Button
        Me.txtQuery = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Page3 = New System.Windows.Forms.GroupBox
        Me.grpDetect = New System.Windows.Forms.GroupBox
        Me.optNew = New System.Windows.Forms.RadioButton
        Me.optAny = New System.Windows.Forms.RadioButton
        Me.chkDetectInserts = New System.Windows.Forms.CheckBox
        Me.cmbKeyColumn = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Page4 = New System.Windows.Forms.GroupBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.cmdBack = New System.Windows.Forms.Button
        Me.cmdNext = New System.Windows.Forms.Button
        Me.btnOK = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.chkTimeconstraint = New System.Windows.Forms.CheckBox
        Me.txtConstraint = New System.Windows.Forms.NumericUpDown
        Me.lblConstraint = New System.Windows.Forms.Label
        Me.UcDSN1 = New sqlrd.ucDSN
        Me.pnMod = New System.Windows.Forms.Panel
        Me.grpConstraint = New System.Windows.Forms.GroupBox
        Me.chkRunOnce = New System.Windows.Forms.CheckBox
        Me.GroupBox1.SuspendLayout()
        Me.Page1.SuspendLayout()
        Me.Page2.SuspendLayout()
        Me.Page3.SuspendLayout()
        Me.grpDetect.SuspendLayout()
        Me.Page4.SuspendLayout()
        CType(Me.txtConstraint, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnMod.SuspendLayout()
        Me.grpConstraint.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Page3)
        Me.GroupBox1.Controls.Add(Me.Page1)
        Me.GroupBox1.Controls.Add(Me.Page2)
        Me.GroupBox1.Controls.Add(Me.Page4)
        Me.GroupBox1.Controls.Add(Me.cmdBack)
        Me.GroupBox1.Controls.Add(Me.cmdNext)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(422, 294)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Condition Details"
        '
        'Page1
        '
        Me.Page1.Controls.Add(Me.cmbValue)
        Me.Page1.Controls.Add(Me.txtName)
        Me.Page1.Controls.Add(Me.Label4)
        Me.Page1.Controls.Add(Me.Label3)
        Me.Page1.Controls.Add(Me.btnConnect)
        Me.Page1.Controls.Add(Me.UcDSN1)
        Me.Page1.Location = New System.Drawing.Point(6, 20)
        Me.Page1.Name = "Page1"
        Me.Page1.Size = New System.Drawing.Size(406, 235)
        Me.Page1.TabIndex = 0
        Me.Page1.TabStop = False
        '
        'cmbValue
        '
        Me.cmbValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbValue.FormattingEnabled = True
        Me.cmbValue.Items.AddRange(New Object() {"TRUE", "FALSE"})
        Me.cmbValue.Location = New System.Drawing.Point(260, 49)
        Me.cmbValue.Name = "cmbValue"
        Me.cmbValue.Size = New System.Drawing.Size(102, 21)
        Me.cmbValue.TabIndex = 1
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(145, 15)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(217, 21)
        Me.txtName.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(3, 53)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(128, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "If database record exists"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 19)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(34, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Name"
        '
        'btnConnect
        '
        Me.btnConnect.Location = New System.Drawing.Point(145, 197)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(75, 23)
        Me.btnConnect.TabIndex = 2
        Me.btnConnect.Text = "Connect"
        Me.btnConnect.UseVisualStyleBackColor = True
        '
        'Page2
        '
        Me.Page2.Controls.Add(Me.btnBuild)
        Me.Page2.Controls.Add(Me.txtQuery)
        Me.Page2.Controls.Add(Me.Label1)
        Me.Page2.Location = New System.Drawing.Point(6, 20)
        Me.Page2.Name = "Page2"
        Me.Page2.Size = New System.Drawing.Size(406, 235)
        Me.Page2.TabIndex = 35
        Me.Page2.TabStop = False
        '
        'btnBuild
        '
        Me.btnBuild.Image = CType(resources.GetObject("btnBuild.Image"), System.Drawing.Image)
        Me.btnBuild.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBuild.Location = New System.Drawing.Point(303, 202)
        Me.btnBuild.Name = "btnBuild"
        Me.btnBuild.Size = New System.Drawing.Size(75, 23)
        Me.btnBuild.TabIndex = 1
        Me.btnBuild.Text = "Build..."
        Me.btnBuild.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnBuild.UseVisualStyleBackColor = True
        '
        'txtQuery
        '
        Me.txtQuery.BackColor = System.Drawing.SystemColors.Control
        Me.txtQuery.Location = New System.Drawing.Point(9, 33)
        Me.txtQuery.Multiline = True
        Me.txtQuery.Name = "txtQuery"
        Me.txtQuery.ReadOnly = True
        Me.txtQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtQuery.Size = New System.Drawing.Size(369, 163)
        Me.txtQuery.TabIndex = 0
        Me.txtQuery.Tag = "memo"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(117, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Record selection query"
        '
        'Page3
        '
        Me.Page3.Controls.Add(Me.pnMod)
        Me.Page3.Controls.Add(Me.grpDetect)
        Me.Page3.Controls.Add(Me.cmbKeyColumn)
        Me.Page3.Controls.Add(Me.Label2)
        Me.Page3.Location = New System.Drawing.Point(6, 20)
        Me.Page3.Name = "Page3"
        Me.Page3.Size = New System.Drawing.Size(406, 235)
        Me.Page3.TabIndex = 35
        Me.Page3.TabStop = False
        '
        'grpDetect
        '
        Me.grpDetect.Controls.Add(Me.optNew)
        Me.grpDetect.Controls.Add(Me.optAny)
        Me.grpDetect.Location = New System.Drawing.Point(9, 60)
        Me.grpDetect.Name = "grpDetect"
        Me.grpDetect.Size = New System.Drawing.Size(176, 80)
        Me.grpDetect.TabIndex = 4
        Me.grpDetect.TabStop = False
        Me.grpDetect.Text = "Detect"
        '
        'optNew
        '
        Me.optNew.AutoSize = True
        Me.optNew.Checked = True
        Me.optNew.Location = New System.Drawing.Point(6, 20)
        Me.optNew.Name = "optNew"
        Me.optNew.Size = New System.Drawing.Size(108, 17)
        Me.optNew.TabIndex = 2
        Me.optNew.TabStop = True
        Me.optNew.Text = "New records only"
        Me.optNew.UseVisualStyleBackColor = True
        '
        'optAny
        '
        Me.optAny.AutoSize = True
        Me.optAny.Location = New System.Drawing.Point(6, 53)
        Me.optAny.Name = "optAny"
        Me.optAny.Size = New System.Drawing.Size(129, 17)
        Me.optAny.TabIndex = 3
        Me.optAny.Text = "Any matching records"
        Me.optAny.UseVisualStyleBackColor = True
        '
        'chkDetectInserts
        '
        Me.chkDetectInserts.AutoSize = True
        Me.chkDetectInserts.Location = New System.Drawing.Point(9, 3)
        Me.chkDetectInserts.Name = "chkDetectInserts"
        Me.chkDetectInserts.Size = New System.Drawing.Size(199, 17)
        Me.chkDetectInserts.TabIndex = 1
        Me.chkDetectInserts.Text = "Detect inserted and deleted records"
        Me.chkDetectInserts.UseVisualStyleBackColor = True
        '
        'cmbKeyColumn
        '
        Me.cmbKeyColumn.FormattingEnabled = True
        Me.cmbKeyColumn.Location = New System.Drawing.Point(9, 33)
        Me.cmbKeyColumn.Name = "cmbKeyColumn"
        Me.cmbKeyColumn.Size = New System.Drawing.Size(195, 21)
        Me.cmbKeyColumn.TabIndex = 0
        Me.cmbKeyColumn.Tag = "unsorted"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(372, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Select the column that uniquely identifies the data in each row (primary key)"
        '
        'Page4
        '
        Me.Page4.Controls.Add(Me.Label5)
        Me.Page4.Location = New System.Drawing.Point(6, 20)
        Me.Page4.Name = "Page4"
        Me.Page4.Size = New System.Drawing.Size(406, 235)
        Me.Page4.TabIndex = 1
        Me.Page4.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Navy
        Me.Label5.Location = New System.Drawing.Point(9, 23)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(295, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Setting up of condition completed. Click OK to finish"
        '
        'cmdBack
        '
        Me.cmdBack.BackColor = System.Drawing.Color.Transparent
        Me.cmdBack.Enabled = False
        Me.cmdBack.Image = CType(resources.GetObject("cmdBack.Image"), System.Drawing.Image)
        Me.cmdBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBack.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBack.Location = New System.Drawing.Point(12, 261)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(56, 22)
        Me.cmdBack.TabIndex = 2
        Me.cmdBack.Text = "&Back"
        Me.cmdBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdBack.UseVisualStyleBackColor = False
        '
        'cmdNext
        '
        Me.cmdNext.BackColor = System.Drawing.Color.Transparent
        Me.cmdNext.Enabled = False
        Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
        Me.cmdNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(356, 261)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(56, 22)
        Me.cmdNext.TabIndex = 1
        Me.cmdNext.Text = "&Next"
        Me.cmdNext.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdNext.UseVisualStyleBackColor = False
        '
        'btnOK
        '
        Me.btnOK.BackColor = System.Drawing.Color.Transparent
        Me.btnOK.Enabled = False
        Me.btnOK.Image = CType(resources.GetObject("btnOK.Image"), System.Drawing.Image)
        Me.btnOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnOK.Location = New System.Drawing.Point(436, 12)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 0
        Me.btnOK.Text = "&OK"
        Me.btnOK.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.Transparent
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnCancel.Location = New System.Drawing.Point(436, 43)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 0
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'chkTimeconstraint
        '
        Me.chkTimeconstraint.AutoSize = True
        Me.chkTimeconstraint.Location = New System.Drawing.Point(9, 26)
        Me.chkTimeconstraint.Name = "chkTimeconstraint"
        Me.chkTimeconstraint.Size = New System.Drawing.Size(337, 17)
        Me.chkTimeconstraint.TabIndex = 5
        Me.chkTimeconstraint.Text = "Only match the condition if record has been/not been modifed in "
        Me.chkTimeconstraint.UseVisualStyleBackColor = True
        '
        'txtConstraint
        '
        Me.txtConstraint.Location = New System.Drawing.Point(6, 13)
        Me.txtConstraint.Maximum = New Decimal(New Integer() {20160, 0, 0, 0})
        Me.txtConstraint.Name = "txtConstraint"
        Me.txtConstraint.Size = New System.Drawing.Size(61, 21)
        Me.txtConstraint.TabIndex = 6
        Me.txtConstraint.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblConstraint
        '
        Me.lblConstraint.AutoSize = True
        Me.lblConstraint.Location = New System.Drawing.Point(74, 17)
        Me.lblConstraint.Name = "lblConstraint"
        Me.lblConstraint.Size = New System.Drawing.Size(44, 13)
        Me.lblConstraint.TabIndex = 7
        Me.lblConstraint.Text = "minutes"
        '
        'UcDSN1
        '
        Me.UcDSN1.BackColor = System.Drawing.Color.Transparent
        Me.UcDSN1.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN1.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN1.Location = New System.Drawing.Point(6, 79)
        Me.UcDSN1.Name = "UcDSN1"
        Me.UcDSN1.Size = New System.Drawing.Size(363, 112)
        Me.UcDSN1.TabIndex = 0
        '
        'pnMod
        '
        Me.pnMod.Controls.Add(Me.grpConstraint)
        Me.pnMod.Controls.Add(Me.chkDetectInserts)
        Me.pnMod.Controls.Add(Me.chkTimeconstraint)
        Me.pnMod.Location = New System.Drawing.Point(9, 60)
        Me.pnMod.Name = "pnMod"
        Me.pnMod.Size = New System.Drawing.Size(365, 97)
        Me.pnMod.TabIndex = 1
        '
        'grpConstraint
        '
        Me.grpConstraint.Controls.Add(Me.chkRunOnce)
        Me.grpConstraint.Controls.Add(Me.lblConstraint)
        Me.grpConstraint.Controls.Add(Me.txtConstraint)
        Me.grpConstraint.Enabled = False
        Me.grpConstraint.Location = New System.Drawing.Point(3, 46)
        Me.grpConstraint.Name = "grpConstraint"
        Me.grpConstraint.Size = New System.Drawing.Size(325, 43)
        Me.grpConstraint.TabIndex = 6
        Me.grpConstraint.TabStop = False
        '
        'chkRunOnce
        '
        Me.chkRunOnce.AutoSize = True
        Me.chkRunOnce.Location = New System.Drawing.Point(158, 16)
        Me.chkRunOnce.Name = "chkRunOnce"
        Me.chkRunOnce.Size = New System.Drawing.Size(94, 17)
        Me.chkRunOnce.TabIndex = 8
        Me.chkRunOnce.Text = "Run once only"
        Me.chkRunOnce.UseVisualStyleBackColor = True
        '
        'frmEventDatabaseExists
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(516, 305)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmEventDatabaseExists"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Condition - Database Record Exists"
        Me.GroupBox1.ResumeLayout(False)
        Me.Page1.ResumeLayout(False)
        Me.Page1.PerformLayout()
        Me.Page2.ResumeLayout(False)
        Me.Page2.PerformLayout()
        Me.Page3.ResumeLayout(False)
        Me.Page3.PerformLayout()
        Me.grpDetect.ResumeLayout(False)
        Me.grpDetect.PerformLayout()
        Me.Page4.ResumeLayout(False)
        Me.Page4.PerformLayout()
        CType(Me.txtConstraint, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnMod.ResumeLayout(False)
        Me.pnMod.PerformLayout()
        Me.grpConstraint.ResumeLayout(False)
        Me.grpConstraint.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents UcDSN1 As sqlrd.ucDSN
    Friend WithEvents Page1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnConnect As System.Windows.Forms.Button
    Friend WithEvents cmdBack As System.Windows.Forms.Button
    Friend WithEvents cmdNext As System.Windows.Forms.Button
    Friend WithEvents Page2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtQuery As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnBuild As System.Windows.Forms.Button
    Friend WithEvents Page3 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbKeyColumn As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkDetectInserts As System.Windows.Forms.CheckBox
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmbValue As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents grpDetect As System.Windows.Forms.GroupBox
    Friend WithEvents optAny As System.Windows.Forms.RadioButton
    Friend WithEvents optNew As System.Windows.Forms.RadioButton
    Friend WithEvents Page4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents chkTimeconstraint As System.Windows.Forms.CheckBox
    Friend WithEvents txtConstraint As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblConstraint As System.Windows.Forms.Label
    Friend WithEvents pnMod As System.Windows.Forms.Panel
    Friend WithEvents grpConstraint As System.Windows.Forms.GroupBox
    Friend WithEvents chkRunOnce As System.Windows.Forms.CheckBox
End Class
