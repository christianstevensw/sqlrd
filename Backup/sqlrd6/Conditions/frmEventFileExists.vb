Public Class frmEventFileExists
    Private conditionType As String

    Dim conditionValue As Boolean
    Dim ep As New ErrorProvider
    Dim userCancel As Boolean = True
    Dim eventType As String
    Dim eventID As Integer
    Dim edit As Boolean = False

    Public Property m_Boolean() As Boolean
        Get
            If cmbValue.Text = "FALSE" Then
                Return False
            Else
                Return True
            End If
        End Get
        Set(ByVal value As Boolean)
            conditionValue = value

            cmbValue.Text = value.ToString.ToUpper
        End Set
    End Property

    Public Enum TYPES
        FILE_EXISTS = 0
        FILE_MODIFIED = 1
    End Enum
    Public Property m_Type() As TYPES
        Get
            Return conditionType
        End Get
        Set(ByVal value As TYPES)
            conditionType = value

            Select Case value

                Case TYPES.FILE_EXISTS
                    Me.Text = "Condition - File Exists"

                    eventType = "file exists"
                Case TYPES.FILE_MODIFIED
                    Me.Text = "Condition - File has been modified"

                    eventType = "file has been modified"
            End Select

            Label4.Text = "If " & eventType & " ="
        End Set
    End Property
    Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
        Dim sfd As SaveFileDialog = New SaveFileDialog

        sfd.Title = "Please select the file..."
        sfd.CheckFileExists = False
        sfd.OverwritePrompt = False

        If sfd.ShowDialog = Windows.Forms.DialogResult.OK Then
            txtFilePath.Text = sfd.FileName
        End If
    End Sub

    Private Sub frmEventFileExists_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If txtFilePath.Text.Length = 0 Then
            ep.SetError(txtFilePath, "Please select the file")
            txtFilePath.Focus()
            Return
        ElseIf txtName.Text.Length = 0 Then
            ep.SetError(txtName, "Please enter a name for this condition")
            txtName.Focus()
            Return
        ElseIf clsMarsEvent.IsConditionDuplicate(Me.eventID, txtName.Text) = True And edit = False Then
            ep.SetError(txtName, "A condition with this name already exists")
            txtName.Focus()
            Return
        End If

        userCancel = False

        Close()
    End Sub

    Private Sub txtFilePath_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFilePath.TextChanged
        ep.SetError(txtFilePath, "")
    End Sub

    Public Sub EditCondition(ByVal nConditionID As Integer)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim dateMod As String

        edit = True

        SQL = "SELECT * FROM EventConditions WHERE ConditionID =" & nConditionID

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then

                txtName.Text = oRs("conditionname").Value

                txtFilePath.Text = oRs("filepath").Value

                Me.m_Boolean = Convert.ToBoolean(oRs("returnvalue").Value)

                eventID = oRs("eventid").Value
            End If

            oRs.Close()
        End If

        Me.ShowDialog()

        If userCancel = True Then Return

        Dim vals As String

        vals = "ConditionName = '" & SQLPrepare(txtName.Text) & "'," & _
        "ReturnValue = " & Convert.ToInt32(Me.m_Boolean) & "," & _
        "FilePath = '" & SQLPrepare(txtFilePath.Text) & "' "
        
        SQL = "UPDATE EventConditions SET " & vals & " WHERE ConditionID = " & nConditionID

        clsMarsData.WriteData(SQL)
    End Sub
    Public Sub AddCondition(ByVal nEventID As Integer, ByVal nOrderID As Integer)
        eventID = nEventID
        Me.ShowDialog()

        If userCancel = True Then Return

        clsMarsUI.MainUI.BusyProgress(50, "Saving condition...")

        Dim cols As String
        Dim vals As String
        Dim dateMod As String

        If Me.m_Type = TYPES.FILE_MODIFIED Then
            Dim fileInfo As IO.FileInfo = New IO.FileInfo(txtFilePath.Text)

            dateMod = ConDateTime(fileInfo.LastWriteTime)
        Else
            dateMod = ConDateTime(Now)
        End If

        cols = "ConditionID,EventID,ConditionName,ConditionType,ReturnValue,KeyColumn,FilePath,OrderID"

        vals = clsMarsData.CreateDataID("eventconditions", "conditionid") & "," & _
        nEventID & "," & _
        "'" & SQLPrepare(txtName.Text) & "'," & _
        "'" & eventType & "'," & _
        Convert.ToInt32(Me.m_Boolean) & "," & _
        "'" & dateMod & "'," & _
        "'" & SQLPrepare(txtFilePath.Text) & "'," & _
        nOrderID

        clsMarsData.DataItem.InsertData("EventConditions", cols, vals, True)

        clsMarsUI.MainUI.BusyProgress(100, , True)
    End Sub

    
End Class