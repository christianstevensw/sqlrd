Public Class frmEventDatabaseExists
    Dim conditionType As TYPES
    Dim nStep As Integer = 1
    Dim ep As ErrorProvider = New ErrorProvider
    Dim conditionValue As Boolean
    Dim eventType As String
    Dim userCancel As Boolean = True
    Dim eventID As Integer = 99999
    Dim edit As Boolean = False

    Public Property m_Boolean() As Boolean
        Get
            If cmbValue.Text = "FALSE" Then
                Return False
            Else
                Return True
            End If
        End Get
        Set(ByVal value As Boolean)
            conditionValue = value

            cmbValue.Text = value.ToString.ToUpper
        End Set
    End Property

    Public Enum TYPES
        DB_EXISTS = 0
        DB_MODIFIED = 1
    End Enum
    Public Property m_Type() As TYPES
        Get
            Return conditionType
        End Get
        Set(ByVal value As TYPES)
            conditionType = value

            Select Case value

                Case TYPES.DB_EXISTS
                    Me.Text = "Condition - Database Record Exists"
                    
                    Me.pnMod.Visible = False

                    Me.grpDetect.Visible = True

                    eventType = "database record exists"

                Case TYPES.DB_MODIFIED
                    Me.Text = "Condition - Database Record Modified"
                    
                    Me.pnMod.Visible = True

                    Me.grpDetect.Visible = False

                    eventType = "database record has changed"
            End Select

            Label4.Text = "If " & eventType & " ="
        End Set
    End Property
    Private Sub btnConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnect.Click
        If UcDSN1._Validate() = True Then
            cmdNext.Enabled = True
        Else
            cmdNext.Enabled = False
        End If
    End Sub

    Private Sub frmEventDatabaseExists_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        Page1.BringToFront()
    End Sub

    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click
        Select Case nStep
            Case 1
                If txtName.Text.Length = 0 Then
                    ep.SetError(txtName, "Please provide a name for this condition.")
                    txtName.Focus()
                    Return
                ElseIf clsMarsEvent.IsConditionDuplicate(Me.eventID, txtName.Text) = True And edit = False Then
                    ep.SetError(txtName, "A condition with this name already exists")
                    txtName.Focus()
                    Return
                End If

                Page2.BringToFront()
                cmdBack.Enabled = True

                nStep += 1
            Case 2
                If txtQuery.Text.Length = 0 Then
                    ep.SetError(txtQuery, "Please provide a record selection query")
                    btnBuild.Focus()
                    Return
                End If

                Page3.BringToFront()

                nStep += 1
            Case 3
                If cmbKeyColumn.Text.Length = 0 Then
                    ep.SetError(cmbKeyColumn, "Please select the key column")
                    cmbKeyColumn.Focus()
                    Return
                End If

                Page4.BringToFront()
                nStep += 1
                cmdNext.Enabled = False
                btnOK.Enabled = True
        End Select
    End Sub

    Private Sub txtQuery_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtQuery.TextChanged
        ep.SetError(txtQuery, "")
    End Sub

    Private Sub cmdBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBack.Click
        Select Case nStep
            Case 4
                cmdNext.Enabled = True
                btnOK.Enabled = False
                Page3.BringToFront()
                nStep -= 1
            Case 3

                Page2.BringToFront()
                nStep -= 1
            Case 2
                Page1.BringToFront()
                cmdBack.Enabled = False
                nStep -= 1
        End Select
    End Sub

    Private Sub btnBuild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuild.Click
        Dim queryBuilder As frmDynamicParameter = New frmDynamicParameter

        Dim sVals(1) As String
        Dim sCon As String = ""

        If UcDSN1.cmbDSN.Text.Length > 0 Then
            sCon = UcDSN1.cmbDSN.Text & "|" & UcDSN1.txtUserID.Text & "|" & UcDSN1.txtPassword.Text & "|"
        End If

        queryBuilder.m_showStar = True
        queryBuilder.Label1.Text = "Please select the table that holds the required data"

        sVals = queryBuilder.ReturnSQLQuery(sCon, txtQuery.Text)

        If sVals Is Nothing Then Return

        sCon = sVals(0)
        txtQuery.Text = sVals(1)

        UcDSN1.cmbDSN.Text = sCon.Split("|")(0)
        UcDSN1.txtUserID.Text = sCon.Split("|")(1)
        UcDSN1.txtPassword.Text = sCon.Split("|")(2)

        Dim oRs As ADODB.Recordset = New ADODB.Recordset
        Dim oCon As ADODB.Connection = New ADODB.Connection

        oCon.Open(UcDSN1.cmbDSN.Text, UcDSN1.txtUserID.Text, UcDSN1.txtPassword.Text)

        oRs.Open(txtQuery.Text, oCon, ADODB.CursorTypeEnum.adOpenStatic)

        Dim temp As String = cmbKeyColumn.Text

        cmbKeyColumn.Items.Clear()
        cmbKeyColumn.Sorted = True

        For Each field As ADODB.Field In oRs.Fields
            cmbKeyColumn.Items.Add(field.Name)
        Next

        If temp.Length = 0 Then
            cmbKeyColumn.SelectedIndex = 0
        Else
            cmbKeyColumn.Text = temp
        End If

        oRs.Close()
        oCon.Close()

        oRs = Nothing
        oCon = Nothing

    End Sub

    Public Sub EditCondition(ByVal nConditionID As Integer)

        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim connectionString As String

        edit = True

        SQL = "SELECT * FROM EventConditions WHERE ConditionID =" & nConditionID

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then

                connectionString = oRs("connectionstring").Value

                txtName.Text = oRs("conditionname").Value

                With UcDSN1
                    .cmbDSN.Text = connectionString.Split("|")(0)
                    .txtUserID.Text = connectionString.Split("|")(1)
                    .txtPassword.Text = _DecryptDBValue(connectionString.Split("|")(2))
                End With

                txtQuery.Text = oRs("searchcriteria").Value

                Me.chkDetectInserts.Checked = Convert.ToBoolean(oRs("detectinserts").Value)

                cmbKeyColumn.Text = oRs("keycolumn").Value

                Me.m_Boolean = Convert.ToBoolean(oRs("returnvalue").Value)

                Me.eventID = oRs("eventid").Value

                Try
                    If IsNull(oRs("anyall").Value, "NEW") = "NEW" Then
                        optNew.Checked = True
                    Else
                        optAny.Checked = True
                    End If
                Catch ex As Exception
                    optNew.Checked = True
                End Try
            End If

            Me.chkTimeconstraint.Checked = IsNull(oRs("useconstraint").Value, 0)
            Me.txtConstraint.Value = IsNull(oRs("constraintvalue").Value, 0)
            Me.chkRunOnce.Checked = IsNull(oRs("runonce").Value, 0)

            oRs.Close()
        End If

        Me.ShowDialog()

        If userCancel = True Then Return

        Dim vals As String

        With UcDSN1
            connectionString = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & _EncryptDBValue(.txtPassword.Text)
        End With

        vals = "ConditionName = '" & SQLPrepare(txtName.Text) & "'," & _
        "ReturnValue = " & Convert.ToInt32(Me.m_Boolean) & "," & _
        "ConnectionString = '" & SQLPrepare(connectionString) & "'," & _
        "Keycolumn = '" & SQLPrepare(cmbKeyColumn.Text) & "'," & _
        "DetectInserts = " & Convert.ToInt32(Me.chkDetectInserts.Checked) & "," & _
        "SearchCriteria = '" & SQLPrepare(txtQuery.Text) & "', " & _
        "AnyAll = '" & IIf(optNew.Checked = True, "NEW", "ALL") & "'," & _
        "UseConstraint =" & Convert.ToInt32(Me.chkTimeconstraint.Checked) & "," & _
        "ConstraintValue =" & Me.txtConstraint.Value & "," & _
        "RunOnce =" & Convert.ToInt32(Me.chkRunOnce.Checked)

        SQL = "UPDATE EventConditions SET " & vals & " WHERE ConditionID = " & nConditionID

        clsMarsData.WriteData(SQL)
    End Sub
    Public Sub AddCondition(ByVal nEventID As Integer, ByVal nOrderID As Integer)

        eventID = nEventID

        Me.ShowDialog()

        If userCancel = True Then Return

        clsMarsUI.MainUI.BusyProgress(50, "Saving condition...")

        Dim cols As String
        Dim vals As String
        Dim connectionString As String
        Dim conditionID As Integer = clsMarsData.CreateDataID("eventconditions", "conditionid")
        Dim cacheFolderName As String = txtName.Text & "." & conditionID 'IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName())

        With UcDSN1
            connectionString = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & _EncryptDBValue(.txtPassword.Text)
        End With

        cols = "ConditionID,EventID,ConditionName,ConditionType,ReturnValue," & _
        "ConnectionString,SearchCriteria,KeyColumn,DetectInserts,AnyAll,OrderID,UseConstraint," & _
        "ConstraintValue,RunOnce,DataLastModified,cacheFolderName"

        vals = conditionID & "," & _
        nEventID & "," & _
        "'" & SQLPrepare(txtName.Text) & "'," & _
        "'" & eventType & "'," & _
        Convert.ToInt32(Me.m_Boolean) & "," & _
        "'" & SQLPrepare(connectionString) & "'," & _
        "'" & SQLPrepare(txtQuery.Text) & "'," & _
        "'" & SQLPrepare(cmbKeyColumn.Text) & "'," & _
        Convert.ToInt32(Me.chkDetectInserts.Checked) & "," & _
        "'" & IIf(Me.optNew.Checked = True, "NEW", "ALL") & "'," & _
        nOrderID & "," & _
        Convert.ToInt32(Me.chkTimeconstraint.Checked) & "," & _
        Me.txtConstraint.Value & "," & _
        Convert.ToInt32(Me.chkRunOnce.Checked) & "," & _
        "'" & ConDateTime(Date.Now) & "'," & _
        "'" & SQLPrepare(cacheFolderName) & "'"

        If clsMarsData.DataItem.InsertData("EventConditions", cols, vals, True) = True Then

            clsMarsUI.MainUI.BusyProgress(80, "Caching data...")

            Dim eventItem As clsMarsEvent = New clsMarsEvent

            If Me.m_Type = TYPES.DB_EXISTS Then
                If optNew.Checked Then
                    eventItem.CacheData(txtName.Text, txtQuery.Text, UcDSN1.cmbDSN.Text & "|" & UcDSN1.txtUserID.Text & "|" & _EncryptDBValue(UcDSN1.txtPassword.Text), cacheFolderName)
                End If
            ElseIf Me.m_Type = TYPES.DB_MODIFIED Then
                eventItem.CacheData(txtName.Text, txtQuery.Text, _
                UcDSN1.cmbDSN.Text & "|" & UcDSN1.txtUserID.Text & "|" & _EncryptDBValue(UcDSN1.txtPassword.Text), cmbKeyColumn.Text, cacheFolderName)
            End If
        End If

        clsMarsUI.MainUI.BusyProgress(100, "", True)

    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click

        userCancel = False
        Close()
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        ep.SetError(txtName, "")
    End Sub

    Private Sub cmbKeyColumn_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbKeyColumn.DropDown
        If cmbKeyColumn.Items.Count = 0 Then
            Dim oRs As ADODB.Recordset = New ADODB.Recordset
            Dim oCon As ADODB.Connection = New ADODB.Connection

            oCon.Open(UcDSN1.cmbDSN.Text, UcDSN1.txtUserID.Text, UcDSN1.txtPassword.Text)

            oRs.Open(txtQuery.Text, oCon, ADODB.CursorTypeEnum.adOpenStatic)

            cmbKeyColumn.Items.Clear()
            cmbKeyColumn.Sorted = True

            For Each field As ADODB.Field In oRs.Fields
                cmbKeyColumn.Items.Add(field.Name)
            Next

            cmbKeyColumn.SelectedIndex = 0

            oRs.Close()
            oCon.Close()

            oRs = Nothing
            oCon = Nothing
        End If

    End Sub

    Private Sub chkTimeconstraint_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTimeconstraint.CheckedChanged
        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.e2_AdvancedEventsPack) = False And Me.chkTimeconstraint.Checked = True Then
            _NeedUpgrade(gEdition.ENTERPRISEPROPLUS)
            Me.chkTimeconstraint.Checked = False
            Return
        End If

        Me.grpConstraint.Enabled = Me.chkTimeconstraint.Checked

    End Sub
End Class