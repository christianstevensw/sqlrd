Public Class frmPDFStamp

    Dim Dynamic As Boolean = False
    Dim eventBased As Boolean = False
    Dim userCancel As Boolean = True
    Dim ep As New ErrorProvider
    Public Property m_Dynamic() As Boolean
        Get
            Return Dynamic
        End Get
        Set(ByVal value As Boolean)
            Dynamic = value
        End Set
    End Property

    Public Property m_eventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = value
        End Set
    End Property
    Private Sub frmPDFStamp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        txtImageLoc.ContextMenu = Me.mnuInserter
    End Sub

    
    Private Sub btnFont_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFont.Click
        With fontDlg
            .MaxSize = 96
            .MinSize = 8
            .ShowColor = True
            .ShowEffects = True

            If .ShowDialog() = Windows.Forms.DialogResult.OK Then
                txtFont.Text = .Font.ToString & ";" & .Font.Style.ToString & ";" & Convert.ToUInt32(.Color.ToKnownColor)
                lblSample.ForeColor = .Color
                lblSample.Font = .Font
            End If
        End With
    End Sub

    Private Sub btnImage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImage.Click, PictureBox1.Click
        With openDlg
            .Multiselect = False
            .Title = "Please select the image file to be used a  stamp..."
            '            .Filter = "JPEG Image (*.jpg)|*.jpg|JPEG Image (*.jpeg)|*.jpeg|Bitmaps (*.bmp)|*.bmp|Icons (*.ico)|*.ico|Enhanced Metafiles (*.emf)|*.emf|Metafiles (*.wmf)|*.wmf|All Files|*.*"
            .Filter = "JPEG Image (*.jpg)|*.jpg|JPEG Image (*.jpeg)|*.jpeg|Bitmaps (*.bmp)|*.bmp|All Files|*.*"

            If .ShowDialog() = Windows.Forms.DialogResult.OK Then
                txtImageLoc.Text = .FileName

                Try
                    Dim image As Image = image.FromFile(txtImageLoc.Text)
                    Me.PictureBox1.Image = image
                Catch
                    Me.PictureBox1.Image = Nothing
                End Try
            End If

        End With
    End Sub

    Private Sub optText_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optText.CheckedChanged
        grpText.Visible = optText.Checked
    End Sub

    Private Sub optGraphic_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optGraphic.CheckedChanged
        grpGraphic.Visible = optGraphic.Checked
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        txtImageLoc.Undo()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        txtImageLoc.Copy()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        txtImageLoc.Cut()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        txtImageLoc.SelectedText = ""
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        txtImageLoc.Paste()
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        txtImageLoc.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim intruder As New frmInserter(sqlrd.gEventID)

        intruder.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        If Dynamic = False Then
            Dim oItem As New frmDataItems

            txtImageLoc.SelectedText = oItem._GetDataItem(gEventID)
        Else
            Dim oIntruder As New frmInserter(0)

            oIntruder.GetDatabaseField(gTables, gsCon, Me)
        End If
    End Sub

    Public Function AddStampDtls(ByVal nID As Integer, Optional ByVal Caption As String = "") As String

        If Caption.StartsWith("<img") = True Then
            Dim oparse As New clsMarsParser

            Try
                txtImageLoc.Text = Caption.Replace("<img src=", "").Replace(">", "")

                Dim img As Image

                img = Image.FromFile(oparse.ParseString(txtImageLoc.Text))

                Me.PictureBox1.Image = img
            Catch : End Try
        Else
            txtCaption.Text = Caption
        End If

        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim stampID As Integer = 0


        SQL = "SELECT * FROM StampAttr WHERE ForeignID =" & nID

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then
                stampID = oRs("stampid").Value

                If oRs("stamptype").Value = "TEXT" Then
                    optText.Checked = True
                Else
                    optGraphic.Checked = True
                End If

                txtFont.Text = oRs("fontdetails").Value
                cmbType.SelectedIndex = cmbType.Items.IndexOf(oRs("overlayunderlay").Value)
                cmbVerticalPos.SelectedIndex = cmbVerticalPos.Items.IndexOf(oRs("verticalpos").Value)
                cmbHorizontalPos.SelectedIndex = cmbHorizontalPos.Items.IndexOf(oRs("horizontalpos").Value)
                txtImageLoc.Text = oRs("imagefile").Value
                txtAngle.Value = oRs("stampangle").Value

                Try
                    Dim oFont As Font
                    Dim font As String = oRs("fontdetails").Value
                    Dim fontName As String
                    Dim fontSize As Integer
                    Dim Bold, Italic, underLine, Strikeout As Boolean
                    Dim nBold, nItalic, nUnderline, nStrikeout As Integer


                    Dim fontDtls As String = font.Split(";")(0)
                    Dim fontStyle As String = font.Split(";")(1)
                    Dim fontColor As Double = font.Split(";")(2)

                    fontDtls = fontDtls.Replace("[Font:", "").Replace("]", "").Trim

                    For Each s As String In fontDtls.Split(",")
                        If s.Split("=")(0).Trim.ToLower = "name" Then
                            fontName = s.Split("=")(1)
                        ElseIf s.Split("=")(0).Trim.ToLower = "size" Then
                            fontSize = s.Split("=")(1)
                        End If
                    Next

                    Dim fs As New FontStyle

                    nBold = 1
                    nItalic = 2
                    nUnderline = 4
                    nStrikeout = 8

                    Dim fontEnum As Integer = 0

                    For Each s As String In fontStyle.Split(",")
                        If s.ToLower.Trim = "bold" Then
                            fontEnum += nBold
                        ElseIf s.ToLower.Trim = "underline" Then
                            fontEnum += nUnderline
                        ElseIf s.ToLower.Trim = "strikeout" Then
                            fontEnum += nStrikeout
                        ElseIf s.ToLower.Trim = "italic" Then
                            fontEnum += nItalic
                        End If
                    Next

                    oFont = New Font(fontName, fontSize, fontEnum, GraphicsUnit.Point, 0)

                    fontDlg.Font = oFont
                    fontDlg.Color = Color.FromKnownColor(fontColor)
                    lblSample.Font = oFont
                    lblSample.ForeColor = Color.FromKnownColor(fontColor)
                Catch : End Try
            End If

            oRs.Close()
        End If

        Me.ShowDialog()

        If userCancel = True Then Return Caption

        If stampID > 0 Then
            SQL = "UPDATE StampAttr SET " & _
            "ForeignID =" & nID & "," & _
            "StampType='" & IIf(optText.Checked = True, "TEXT", "GRAPHIC") & "'," & _
            "FontDetails='" & SQLPrepare(txtFont.Text) & "'," & _
            "OverlayUnderlay ='" & cmbType.Text & "'," & _
            "VerticalPos ='" & cmbVerticalPos.Text & "'," & _
            "HorizontalPos ='" & cmbHorizontalPos.Text & "'," & _
            "ImageFile ='" & txtImageLoc.Text & "'," & _
            "StampAngle ='" & txtAngle.Value & "' " & _
            "WHERE StampID =" & stampID

            clsMarsData.WriteData(SQL)
        Else
            Dim columns As String
            Dim values As String

            columns = "StampID,ForeignID,StampType,FontDetails,OverlayUnderlay,VerticalPos,HorizontalPos,ImageFile,StampAngle"

            values = clsMarsData.CreateDataID("stampattr", "stampid") & "," & _
            nID & "," & _
            "'" & IIf(optText.Checked = True, "TEXT", "GRAPHIC") & "'," & _
            "'" & SQLPrepare(txtFont.Text) & "'," & _
            "'" & Me.cmbType.Text & "'," & _
            "'" & Me.cmbVerticalPos.Text & "'," & _
            "'" & Me.cmbHorizontalPos.Text & "'," & _
            "'" & SQLPrepare(Me.txtImageLoc.Text) & "'," & _
            Me.txtAngle.Value

            clsMarsData.DataItem.InsertData("StampAttr", columns, values)
        End If

        If optText.Checked = True Then
            Return txtCaption.Text
        Else
            Return "<img src=" & txtImageLoc.Text & ">"
        End If
    End Function

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If optText.Checked = True Then
            If txtCaption.Text.Length = 0 Then
                ep.SetError(txtCaption, "Please enter the caption for the pdf stamp")
                txtCaption.Focus()
                Return
            End If
        Else
            If txtImageLoc.Text.Length = 0 Then
                ep.SetError(txtImageLoc, "Please select the image file to use as the stamp")
                txtImageLoc.Focus()
                Return
            End If
        End If

        If cmbType.Text.Length = 0 Then
            ep.SetError(cmbType, "Please select whether to underlay or overlay the stamp")
            cmbType.Focus()
            Return
        ElseIf cmbVerticalPos.Text.Length = 0 Then
            ep.SetError(cmbVerticalPos, "Please select the vertical stamp position")
            cmbVerticalPos.Focus()
            Return
        ElseIf cmbHorizontalPos.Text.Length = 0 Then
            ep.SetError(cmbHorizontalPos, "Please select the horizontal stamp position")
            cmbHorizontalPos.Focus()
            Return
        End If

        userCancel = False

        Close()
    End Sub
End Class