﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("SQL-RD")> 
<Assembly: AssemblyDescription("Build 20150212")> 
<Assembly: AssemblyCompany("ChristianSteven Software Ltd")> 
<Assembly: AssemblyProduct("SQL-RD")> 
<Assembly: AssemblyCopyright("Copyright © ChristianSteven Software  2015")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("dcb78705-b311-4fa1-a66c-d200a2b1e38d")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("7.2.*")> 
