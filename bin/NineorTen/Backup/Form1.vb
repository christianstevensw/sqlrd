Imports Microsoft.Win32
Imports System.ServiceProcess
Public Class Form1
    Inherits System.Windows.Forms.Form
    Dim oMain As New clsMain
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents opt8 As System.Windows.Forms.RadioButton
    Friend WithEvents opt9 As System.Windows.Forms.RadioButton
    Friend WithEvents opt10 As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents opt11 As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Form1))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.opt10 = New System.Windows.Forms.RadioButton
        Me.opt9 = New System.Windows.Forms.RadioButton
        Me.opt8 = New System.Windows.Forms.RadioButton
        Me.opt11 = New System.Windows.Forms.RadioButton
        Me.Button1 = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.opt10)
        Me.GroupBox1.Controls.Add(Me.opt9)
        Me.GroupBox1.Controls.Add(Me.opt8)
        Me.GroupBox1.Controls.Add(Me.opt11)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(448, 304)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Navy
        Me.Label1.Location = New System.Drawing.Point(8, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(432, 23)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Please select the version of Crystal Reports that you are using"
        '
        'opt10
        '
        Me.opt10.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.opt10.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.opt10.ForeColor = System.Drawing.Color.Navy
        Me.opt10.Location = New System.Drawing.Point(8, 168)
        Me.opt10.Name = "opt10"
        Me.opt10.Size = New System.Drawing.Size(432, 16)
        Me.opt10.TabIndex = 2
        Me.opt10.Text = "Crystal Reports 10.xx:"
        '
        'opt9
        '
        Me.opt9.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.opt9.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.opt9.ForeColor = System.Drawing.Color.Navy
        Me.opt9.Location = New System.Drawing.Point(8, 104)
        Me.opt9.Name = "opt9"
        Me.opt9.Size = New System.Drawing.Size(432, 16)
        Me.opt9.TabIndex = 1
        Me.opt9.Text = "Crystal Reports 9.xx:"
        '
        'opt8
        '
        Me.opt8.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.opt8.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.opt8.ForeColor = System.Drawing.Color.Navy
        Me.opt8.Location = New System.Drawing.Point(8, 40)
        Me.opt8.Name = "opt8"
        Me.opt8.Size = New System.Drawing.Size(432, 16)
        Me.opt8.TabIndex = 0
        Me.opt8.Text = "Crystal Reports 8.5.x:"
        '
        'opt11
        '
        Me.opt11.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.opt11.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.opt11.ForeColor = System.Drawing.Color.Navy
        Me.opt11.Location = New System.Drawing.Point(8, 240)
        Me.opt11.Name = "opt11"
        Me.opt11.Size = New System.Drawing.Size(432, 16)
        Me.opt11.TabIndex = 2
        Me.opt11.Text = "Crystal Reports 11.xx: "
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(381, 320)
        Me.Button1.Name = "Button1"
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "OK"
        '
        'Label2
        '
        Me.Label2.ForeColor = System.Drawing.Color.Navy
        Me.Label2.Location = New System.Drawing.Point(8, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(432, 32)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "If you have CR 8.5 Developer or Advanced above installed select 8.5.  CRD will us" & _
        "e the dlls on your PC to export Crystal Reports written in Crystal Versions up t" & _
        "o 8.5."
        '
        'Label3
        '
        Me.Label3.ForeColor = System.Drawing.Color.Navy
        Me.Label3.Location = New System.Drawing.Point(8, 120)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(432, 32)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = " If you have CR 9 Developer or Advanced installed, select 9.xx.  CRD will use the" & _
        " dlls on your PC to export Crystal Reports written in Crystal Versions 8.5 and 9" & _
        ".xx"
        '
        'Label4
        '
        Me.Label4.ForeColor = System.Drawing.Color.Navy
        Me.Label4.Location = New System.Drawing.Point(8, 184)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(432, 40)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "If you have CR 10 Developer or Advanced installed, select 10.xx.  CRD will use th" & _
        "e dlls on your PC to export Crystal Reports written in Crystal Versions 8.5, 9.x" & _
        "x and 10.xx"
        '
        'Label5
        '
        Me.Label5.ForeColor = System.Drawing.Color.Navy
        Me.Label5.Location = New System.Drawing.Point(8, 256)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(432, 40)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "If you have CR 11 Developer or Advanced installed, select 11.xx.  CRD will use th" & _
        "e dlls on your PC to export Crystal Reports written in Crystal Versions 8.5, 9.x" & _
        "x, 10.xx and 11.xx"
        '
        'Form1
        '
        Me.AcceptButton = Me.Button1
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(466, 352)
        Me.ControlBox = False
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Crystal Reports Versions"
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Dim sPath As String
            Dim dllName As String
            Dim Interop As String
            Dim Ver As String
            Dim sArgs() As String

            sPath = Application.StartupPath

            If sPath.EndsWith("\") = False Then sPath &= "\"

            Try
                IO.File.Delete(sPath & "Interop.CRAXDRT.dll")
            Catch :End Try

            If opt8.Checked Then
                dllName = "interop85.dll"
                Interop = "Interop.CRAXDRT85.dll"
                Ver = 85
            ElseIf opt9.Checked Then
                dllName = "interop90.dll"
                Interop = "Interop.CRAXDRT90.dll"
                Ver = 90
            ElseIf opt10.Checked Then
                dllName = "interop10.dll"
                Interop = "Interop.CRAXDRT10.dll"
                Ver = 10
            ElseIf opt11.Checked Then
                dllName = "interop11.dll"
                Interop = "Interop.CRAXDRT11.dll"
                Ver = 11
            Else
                MsgBox("Please select your version of Crystal Reports before proceeding", vbExclamation, "CRD Installer")
                Exit Sub
            End If

            IO.File.Copy(sPath & dllName, sPath & "crd.exe", True)

            'IO.File.Copy(sPath & Interop, sPath & Replace(Interop, Ver, ""), True)

            Close()
        Catch ex As Exception
            MsgBox("CRD Installer encountered an error." & vbCrLf & _
            "Error Message: " & ex.Message & vbCrLf & _
            "Error Number: " & Err.Number & vbCrLf & _
            "Error Line: " & 0)

            Close()
        End Try
    End Sub

End Class
