Imports System.ServiceProcess
Imports Microsoft.Win32
Public Class frmDataStorage
    Inherits System.Windows.Forms.Form
    Dim sKey As String = "Software\ChristianSteven\CRD"
    Dim oReg As RegistryKey = Registry.LocalMachine
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents optSQL As System.Windows.Forms.RadioButton
    Friend WithEvents optFlat As System.Windows.Forms.RadioButton
    Friend WithEvents cmdGo As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.optFlat = New System.Windows.Forms.RadioButton
        Me.optSQL = New System.Windows.Forms.RadioButton
        Me.cmdGo = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.optFlat)
        Me.GroupBox1.Controls.Add(Me.optSQL)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(360, 128)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'optFlat
        '
        Me.optFlat.ForeColor = System.Drawing.Color.Navy
        Me.optFlat.Location = New System.Drawing.Point(8, 56)
        Me.optFlat.Name = "optFlat"
        Me.optFlat.Size = New System.Drawing.Size(312, 24)
        Me.optFlat.TabIndex = 1
        Me.optFlat.Text = "Do not store in local SQL Server Database"
        '
        'optSQL
        '
        Me.optSQL.Checked = True
        Me.optSQL.ForeColor = System.Drawing.Color.Navy
        Me.optSQL.Location = New System.Drawing.Point(8, 16)
        Me.optSQL.Name = "optSQL"
        Me.optSQL.Size = New System.Drawing.Size(320, 24)
        Me.optSQL.TabIndex = 0
        Me.optSQL.TabStop = True
        Me.optSQL.Text = "Store in Local SQL Server Database (Recommended)"
        '
        'cmdGo
        '
        Me.cmdGo.Location = New System.Drawing.Point(144, 144)
        Me.cmdGo.Name = "cmdGo"
        Me.cmdGo.TabIndex = 1
        Me.cmdGo.Text = "Continue"
        '
        'Label1
        '
        Me.Label1.ForeColor = System.Drawing.Color.Blue
        Me.Label1.Location = New System.Drawing.Point(8, 80)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(344, 40)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "WARNING: The installer will attempt to create a database called CRD in the local " & _
        "SQL Server. Any existing database called CRD will be deleted. "
        '
        'frmDataStorage
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(378, 176)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmdGo)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmDataStorage"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CRD Data Storage"
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub cmdGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGo.Click
        Cursor.Current = Cursors.WaitCursor
        _Action()
    End Sub

    Public Sub _Action()
        Dim sPath As String = Application.StartupPath & "\"
        Dim Migrate As Boolean = False
        Dim SQLPath As String

        If optSQL.Checked = False Then
            Close()
            Return
        End If

        Dim srv As New ServiceController("MSSQLSERVER")

        Dim SQLExists As Boolean

        Try
            Console.WriteLine(srv.ServiceName)
            SQLExists = True
        Catch ex As Exception
            SQLExists = False
        End Try

        Try
            If SQLExists = False Then

                MessageBox.Show("Setup will now install the SQL Server Desktop Engine.", _
                    "CRD Installer", MessageBoxButtons.OK)

                SQLPath = sPath.Replace("CRD", "Common")

                SQLPath &= "MSDE\Helper.exe"

                Dim oProc As New Process

                'install msde

                With oProc
                    .StartInfo.FileName = SQLPath
                    .Start()

                    'save the connection type
                    _SaveRegistry(oReg, sKey, "ConType", "LOCAL")
                    _SaveRegistry(oReg, sKey, "ConString", "Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=CRD;Data Source=" & Environment.MachineName)

                End With
            Else
                _SaveRegistry(oReg, sKey, "ConType", "LOCAL")
                _SaveRegistry(oReg, sKey, "ConString", "Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=CRD;Data Source=" & Environment.MachineName)
            End If

            Close()
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "CRD Installer", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Close()
        End Try
    End Sub
    Public Sub _CreateDatabase()
        Dim oStatus As New frmStatus
        Dim SQLPath As String
        Dim Migrate As Boolean = False

        oStatus.Label1.Text = "Starting SQL Server service..."

        oStatus.Show()
        Application.DoEvents()

        Try

            If IO.File.Exists(Application.StartupPath & "\crdlive.dat") = True Then
                If MessageBox.Show("Would you like to migrate your current system to the local SQL Server?", "CRD", _
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    Migrate = True
                End If
            Else
                Migrate = False
                IO.File.Copy(Application.StartupPath & "\crd.dat", Application.StartupPath & "\crdlive.dat")
            End If

            Dim srv As New ServiceController("MSSQLSERVER")

            'start the sql server service
            If srv.Status <> ServiceControllerStatus.Running Then
                srv.Start()
            End If


            Dim Retry As Integer = 1
retry:

            System.Threading.Thread.Sleep(3000)

            Dim oCon As New ADODB.Connection
            Dim sCon As String

            sCon = "Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=master;Data Source=" & Environment.MachineName

            Try
                oStatus.Label1.Text = "Connecting to SQL Server..."
                Application.DoEvents()
                'create the database
                oCon.Open(sCon)
            Catch ex As Exception
                oStatus.Label1.Text = "Failed to connect to SQL Server. Retry Number: " & Retry
                Application.DoEvents()
                Retry += 1

                If Retry < 11 Then
                    GoTo retry
                Else
                    Throw New Exception(ex.Message)
                    Return
                End If
            End Try

            oStatus.Label1.Text = "Connected to SQL Server service..."
            Application.DoEvents()

            'get the SQL Path
            SQLPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)

            SQLPath &= "\Microsoft SQL Server\MSSQL\Data\"

            Try
                oCon.Execute("DROP DATABASE CRD")
            Catch :End Try

            Dim DataFile As String = SQLPath & "CRD_Data.MDF"
            Dim LogFile As String = SQLPath & "CRD_Log.LDF"
            Dim SQL As String

            Try
                If IO.File.Exists(DataFile) Then IO.File.Delete(DataFile)
                If IO.File.Exists(LogFile) Then IO.File.Delete(LogFile)
            Catch : End Try

            oStatus.Label1.Text = "Creating CRD Database..."
            Application.DoEvents()

            SQL = "CREATE DATABASE CRD " & vbCrLf & _
            "ON " & vbCrLf & _
            "(NAME = CRD_Data," & vbCrLf & _
            "FILENAME = '" & DataFile & "'," & vbCrLf & _
            "SIZE = 10," & vbCrLf & _
            "MAXSIZE = 100," & vbCrLf & _
            "FILEGROWTH = 20 ) " & vbCrLf & _
            "LOG ON " & vbCrLf & _
            "( NAME = 'CRD_Log'," & vbCrLf & _
            "FILENAME = '" & LogFile & "'," & vbCrLf & _
            "SIZE = 5MB, " & vbCrLf & _
            "MAXSIZE = 25MB, " & vbCrLf & _
            "FILEGROWTH = 5MB )"

            oCon.Execute(SQL)

            oCon.Close()

            'migrate the data
            sCon = "Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=CRD;Data Source=" & Environment.MachineName

            oStatus.Label1.Text = "Creating tables..."
            Application.DoEvents()
            Dim ok As Boolean = _CreateDataTables(sCon)

            If Migrate = True Then
                If ok = True Then
                    oStatus.Label1.Text = "Migrating data..."
                    Application.DoEvents()
                    _CopyData("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Application.StartupPath & "\crdlive.dat;Persist Security Info=False", sCon)
                End If
            Else
                Try
                    oCon.Open(sCon)
                    Dim sVals As String
                    Dim sCols As String
                    Dim sFolders(4) As String
                    Dim I As Integer = 1

                    sFolders(0) = "Daily"
                    sFolders(1) = "Weekly"
                    sFolders(2) = "Monthly"
                    sFolders(3) = "Yearly"
                    sFolders(4) = "Custom"

                    oStatus.Label1.Text = "Creating default tables..."
                    Application.DoEvents()
                    sCols = "folderid,foldername,parent"

                    For Each s As String In sFolders
                        sVals = I & ",'" & s & "',0"

                        SQL = "INSERT INTO Folders (" & sCols & ") VALUES (" & sVals & ")"

                        oCon.Execute(SQL)

                        I += 1
                    Next

                    oCon.Close()
                Catch : End Try
            End If

            oStatus.Label1.Text = "Saving settings and cleaning up..."
            Application.DoEvents()

            _SaveRegistry(oReg, sKey, "ConType", "LOCAL")
            _SaveRegistry(oReg, sKey, "ConString", sCon)
            oStatus.Close()
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
    Public Function _CreateDataTables(ByVal sCon As String) As Boolean
        Dim SQL As String
        Dim sAr() As String
        Dim I As Integer = 1
        Dim oCombo As New ComboBox
        Dim sTable As String
        Dim oItems As ComboBox.ObjectCollection

        _GetTables(oCombo, sCon)

        Dim oC As New ADODB.Connection

        oC.Open(sCon)

        oItems = oCombo.Items

        For I = 0 To oItems.Count - 1
            sTable = oItems.Item(I)

            Try
                oC.Execute("DROP TABLE " & sTable)
            Catch : End Try
        Next

        SQL = ReadTextFromFile(Application.StartupPath & "\crd.sql")

        If SQL.Length = 0 Then Return False

        sAr = Split(SQL, "GO")

        I = 1

        For Each s As String In sAr

            If s.Length > 0 Then
                Try
                    oC.Execute(s)
                Catch ex As Exception
                    If ex.Message.IndexOf("There is already an object named") = -1 Then
                        MessageBox.Show(ex.ToString, "CRDInstaller", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Return False
                    End If
                End Try
            End If

            I += 1
        Next

        oC.Close()

        Return True
    End Function

    Public Function ReadTextFromFile(ByVal sPath As String) As String
        On Error Resume Next

        Dim sr As IO.StreamReader = IO.File.OpenText(sPath)
        Dim sInput As String

        sInput = sr.ReadToEnd

        sr.Close()

        Return sInput

    End Function

    Public Function _CopyData(ByVal sFromCon As String, ByVal sToCon As String) As Boolean

        Dim oCombo As New ComboBox
        Dim oC As New ADODB.Connection
        Dim oItems As ComboBox.ObjectCollection
        Dim oRs As ADODB.Recordset
        Dim sTable As String
        Dim sValue As String

        _GetTables(oCombo, sFromCon)

        oC.Open(sToCon)

        Dim SQL As String

        oItems = oCombo.Items

        For I As Integer = 0 To oItems.Count - 1

            SQL = ""

            sTable = oItems.Item(I)

            If sTable.ToLower = "tmpfolders" Then GoTo NextRec

            oRs = _GetData("SELECT * FROM " & sTable, sFromCon)

            If oRs Is Nothing Then GoTo NextRec

            Do While oRs.EOF = False
                SQL = ""
                For x As Integer = 0 To oRs.Fields.Count - 1
                    sValue = SQLPrepare(IsNull(oRs.Fields(x).Value))

                    If oRs.Fields(x).Type = ADODB.DataTypeEnum.adDate Or _
                    oRs.Fields(x).Type = ADODB.DataTypeEnum.adDBDate Or _
                    oRs.Fields(x).Type = ADODB.DataTypeEnum.adDBTime Then
                        Try
                            Date.Parse(sValue)

                            If oRs.Fields(x).Name <> "RepeatUntil" Then
                                sValue = ConDate(sValue) & " " & ConTime(sValue)
                            End If

                        Catch
                            sValue = ConDateTime(Now)
                        End Try
                    End If

                    SQL &= "'" & sValue & "',"
                Next

                SQL = SQL.Substring(0, SQL.Length - 1)

                SQL = "INSERT INTO " & sTable & " VALUES (" & SQL & ")"

                Try : oC.Execute(SQL) : Catch : End Try

                oRs.MoveNext()
            Loop

            oRs.Close()
NextRec:
        Next

        oC.Close()

        oC = Nothing
        oRs = Nothing
    End Function

    Public Function ConDateTime(ByVal iDate As Date) As String
        Return ConDate(iDate) & " " & ConTime(iDate)
    End Function

    Public Function ConTime(ByVal iTime As Date) As String

        ConTime = iTime.ToString("HH:mm:ss")
    End Function
    Public Function ConDate(ByVal iDate As Date) As String
        On Error Resume Next
        Dim nYear1 As Integer
        Dim nYear2 As Integer

        ConDate = iDate.ToString("yyyy-MM-dd")

    End Function
    Public Function _GetData(ByVal sQuery As String, ByVal sCon As String, _
        Optional ByVal oLock As ADODB.CursorTypeEnum = ADODB.CursorTypeEnum.adOpenStatic) As ADODB.Recordset

        Dim oRs As ADODB.Recordset = New ADODB.Recordset

        Try

            oRs.Open(sQuery, sCon, oLock, ADODB.LockTypeEnum.adLockReadOnly)

            Return oRs

        Catch ex As System.Exception
            Console.WriteLine(ex.Message & " " & Err.Number)
        End Try
    End Function
    Public Sub _GetTables(ByVal oCombo As ComboBox, ByVal sCon As String)
        Try
            Dim tb As New ADOX.Table
            Dim cl As New ADOX.Column
            Dim db As New ADOX.Catalog
            Dim Conn As New ADODB.Connection
            Dim str As String
            Dim cKey As Integer
            Dim tKey As Integer
            Dim tName As String
            Dim sDatabase As String
            Dim ShowSystem As Boolean = False

            oCombo.Items.Clear()

            With Conn
                .Open(sCon)

                Try
                    sDatabase = .ConnectionString
                Catch ex As Exception
                    sDatabase = String.Empty
                End Try
            End With

            If sDatabase.ToLower.IndexOf("excel") > -1 Then
                ShowSystem = True
            End If

            db.ActiveConnection = Conn

            For Each tb In db.Tables
                If ShowSystem = True Then
                    If tb.Type = "TABLE" Or tb.Type = "SYSTEM TABLE" Then
                        If InStr(1, tb.Name, " ") > 0 Or InStr(1, tb.Name, "$") Then
                            tName = "[" & tb.Name & "]"
                        Else
                            tName = tb.Name
                        End If

                        oCombo.Items.Add(tName)
                    End If
                Else
                    If tb.Type = "TABLE" Then
                        If InStr(1, tb.Name, " ") > 0 Or InStr(1, tb.Name, "$") Then
                            tName = "[" & tb.Name & "]"
                        Else
                            tName = tb.Name
                        End If

                        oCombo.Items.Add(tName)
                    End If
                End If

            Next

            tb = Nothing
            cl = Nothing
            db = Nothing
            Conn = Nothing
            Exit Sub
        Catch : End Try
    End Sub

    Public Function SQLPrepare(ByVal sIn As String) As String
        On Error Resume Next
        Return sIn.Replace("'", "''")
    End Function
    Public Function IsNull(ByVal sIn As Object, Optional ByVal sDefault As Object = "") As Object
        Dim Temp

        Try
            Temp = sIn & ""

            If Temp.length > 0 Then
                Return Temp
            Else
                Return sDefault
            End If
        Catch
            Return sIn & ""
        End Try
    End Function
    Public Function _SaveRegistry(ByVal ParentKey As RegistryKey, ByVal SubKey As String, _
    ByVal ValueName As String, ByRef Value As Object) As Boolean

        Dim Key As RegistryKey
        Dim Temp

        If Value Is Nothing Then Value = ""

        Temp = Value

        Try
            'Open the registry key.
            Key = ParentKey.OpenSubKey(SubKey, True)

            If Key Is Nothing Then 'if the key doesn't exist

                Key = ParentKey

                Key = Key.CreateSubKey(SubKey)
            End If

            Key.SetValue(ValueName, Value)

            Key.Close()

            Return True
        Catch e As Exception

            Return False
        End Try

    End Function

    Private Sub optFlat_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optFlat.CheckedChanged
        If optFlat.Checked = True Then
            Label1.Visible = False
        End If
    End Sub

    Private Sub optSQL_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optSQL.CheckedChanged
        Label1.Visible = optSQL.Checked
    End Sub
End Class
