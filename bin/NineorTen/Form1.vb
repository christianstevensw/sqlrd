Imports Microsoft.Win32
Imports System.ServiceProcess
Public Class Form1
    Inherits System.Windows.Forms.Form
    Dim oMain As New clsMain
    Friend WithEvents cmbVersions As System.Windows.Forms.ComboBox
    Dim sAppPath As String
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblDesc As System.Windows.Forms.Label
    Dim version As ArrayList = New ArrayList
    Dim desc As ArrayList = New ArrayList

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents opt8 As System.Windows.Forms.RadioButton
    Friend WithEvents opt9 As System.Windows.Forms.RadioButton
    Friend WithEvents opt10 As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents opt11 As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents opt115 As System.Windows.Forms.RadioButton
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.opt10 = New System.Windows.Forms.RadioButton
        Me.opt9 = New System.Windows.Forms.RadioButton
        Me.opt8 = New System.Windows.Forms.RadioButton
        Me.opt115 = New System.Windows.Forms.RadioButton
        Me.opt11 = New System.Windows.Forms.RadioButton
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.cmbVersions = New System.Windows.Forms.ComboBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.lblDesc = New System.Windows.Forms.Label
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.opt10)
        Me.GroupBox1.Controls.Add(Me.opt9)
        Me.GroupBox1.Controls.Add(Me.opt8)
        Me.GroupBox1.Controls.Add(Me.opt115)
        Me.GroupBox1.Controls.Add(Me.opt11)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.GroupBox1.Location = New System.Drawing.Point(8, 469)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(448, 386)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Visible = False
        '
        'Label3
        '
        Me.Label3.ForeColor = System.Drawing.Color.Navy
        Me.Label3.Location = New System.Drawing.Point(8, 120)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(432, 32)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = " If you have CR 9 Developer or Advanced installed, select 9.xx.  CRD will use the" & _
            " dlls on your PC to export Crystal Reports written in Crystal Versions 8.5 and 9" & _
            ".xx"
        '
        'Label2
        '
        Me.Label2.ForeColor = System.Drawing.Color.Navy
        Me.Label2.Location = New System.Drawing.Point(8, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(432, 32)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "If you have CR 8.5 Developer or Advanced above installed select 8.5.  CRD will us" & _
            "e the dlls on your PC to export Crystal Reports written in Crystal Versions up t" & _
            "o 8.5."
        '
        'opt10
        '
        Me.opt10.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.opt10.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.opt10.ForeColor = System.Drawing.Color.Navy
        Me.opt10.Location = New System.Drawing.Point(8, 168)
        Me.opt10.Name = "opt10"
        Me.opt10.Size = New System.Drawing.Size(432, 16)
        Me.opt10.TabIndex = 2
        Me.opt10.Text = "Crystal Reports 10.xx:"
        '
        'opt9
        '
        Me.opt9.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.opt9.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.opt9.ForeColor = System.Drawing.Color.Navy
        Me.opt9.Location = New System.Drawing.Point(8, 104)
        Me.opt9.Name = "opt9"
        Me.opt9.Size = New System.Drawing.Size(432, 16)
        Me.opt9.TabIndex = 1
        Me.opt9.Text = "Crystal Reports 9.xx:"
        '
        'opt8
        '
        Me.opt8.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.opt8.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.opt8.ForeColor = System.Drawing.Color.Navy
        Me.opt8.Location = New System.Drawing.Point(8, 40)
        Me.opt8.Name = "opt8"
        Me.opt8.Size = New System.Drawing.Size(432, 16)
        Me.opt8.TabIndex = 0
        Me.opt8.Text = "Crystal Reports 8.5.x:"
        '
        'opt115
        '
        Me.opt115.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.opt115.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.opt115.ForeColor = System.Drawing.Color.Navy
        Me.opt115.Location = New System.Drawing.Point(8, 318)
        Me.opt115.Name = "opt115"
        Me.opt115.Size = New System.Drawing.Size(432, 16)
        Me.opt115.TabIndex = 2
        Me.opt115.Text = "Crystal Reports 11.xx Release 2: "
        '
        'opt11
        '
        Me.opt11.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.opt11.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.opt11.ForeColor = System.Drawing.Color.Navy
        Me.opt11.Location = New System.Drawing.Point(8, 240)
        Me.opt11.Name = "opt11"
        Me.opt11.Size = New System.Drawing.Size(432, 16)
        Me.opt11.TabIndex = 2
        Me.opt11.Text = "Crystal Reports 11.xx: "
        '
        'Label4
        '
        Me.Label4.ForeColor = System.Drawing.Color.Navy
        Me.Label4.Location = New System.Drawing.Point(8, 184)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(432, 40)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "If you have CR 10 Developer or Advanced installed, select 10.xx.  CRD will use th" & _
            "e dlls on your PC to export Crystal Reports written in Crystal Versions 8.5, 9.x" & _
            "x and 10.xx"
        '
        'Label6
        '
        Me.Label6.ForeColor = System.Drawing.Color.Navy
        Me.Label6.Location = New System.Drawing.Point(8, 337)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(432, 40)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "If you have CR 11 Release 2 Developer or Advanced installed, select 11.xx Release" & _
            " 2.  CRD will use the dlls on your PC to export Crystal Reports written in Cryst" & _
            "al Versions 8.5, 9.xx, 10.xx and 11.xx"
        '
        'Label5
        '
        Me.Label5.ForeColor = System.Drawing.Color.Navy
        Me.Label5.Location = New System.Drawing.Point(8, 256)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(432, 40)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "If you have CR 11 Developer or Advanced installed, select 11.xx.  CRD will use th" & _
            "e dlls on your PC to export Crystal Reports written in Crystal Versions 8.5, 9.x" & _
            "x, 10.xx and 11.xx"
        '
        'Label1
        '
        Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Navy
        Me.Label1.Location = New System.Drawing.Point(12, 416)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(432, 23)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Please select the version of Crystal Reports that you are using"
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(381, 117)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "OK"
        '
        'cmbVersions
        '
        Me.cmbVersions.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbVersions.ForeColor = System.Drawing.Color.MidnightBlue
        Me.cmbVersions.FormattingEnabled = True
        Me.cmbVersions.Location = New System.Drawing.Point(8, 20)
        Me.cmbVersions.Name = "cmbVersions"
        Me.cmbVersions.Size = New System.Drawing.Size(306, 21)
        Me.cmbVersions.TabIndex = 2
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblDesc)
        Me.GroupBox2.Controls.Add(Me.cmbVersions)
        Me.GroupBox2.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox2.Location = New System.Drawing.Point(8, 4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(448, 107)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Please select the version of Crystal Reports that you are using"
        '
        'lblDesc
        '
        Me.lblDesc.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.lblDesc.ForeColor = System.Drawing.Color.Blue
        Me.lblDesc.Location = New System.Drawing.Point(8, 48)
        Me.lblDesc.Name = "lblDesc"
        Me.lblDesc.Size = New System.Drawing.Size(434, 48)
        Me.lblDesc.TabIndex = 3
        '
        'Form1
        '
        Me.AcceptButton = Me.Button1
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(466, 143)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Crystal Reports Versions"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW As Object = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Dim sPath As String
            Dim dllName As String
            Dim Interop As String
            Dim Ver As String
            'Dim sArgs() As String

            sPath = Application.StartupPath

            If sPath.EndsWith("\") = False Then sPath &= "\"

            Try
                IO.File.Delete(sPath & "Interop.CRAXDRT.dll")
            Catch :End Try

            Select Case Me.cmbVersions.Text

                Case "Crystal Reports 8.5.x"
                    dllName = "interop85.dll"
                    Interop = "Interop.CRAXDRT85.dll"
                    Ver = 85
                Case "Crystal Reports 9.xx"
                    dllName = "interop90.dll"
                    Interop = "Interop.CRAXDRT90.dll"
                    Ver = 90
                Case "Crystal Reports 10.xx"
                    dllName = "interop10.dll"
                    Interop = "Interop.CRAXDRT10.dll"
                    Ver = 10
                Case "Crystal Reports 11.xx"
                    dllName = "interop11.dll"
                    Interop = "Interop.CRAXDRT11.dll"
                    Ver = 11
                Case "Crystal Reports 11.5x (Release 2)"
                    dllName = "interop115.dll"
                    Interop = "Interop.CRAXDRT11.dll"
                    Ver = 115
                Case "Crystal Reports 2008 (12.xx)"
                    dllName = "interop12.dll"
                    Interop = "Interop.CRAXDRT12.dll"
                    Ver = 12
                Case Else
                    MsgBox("Please select your version of Crystal Reports before proceeding", vbExclamation, "CRD Installer")
                    Exit Sub
            End Select

            IO.File.Copy(sPath & dllName, sPath & "crd.exe", True)

            'IO.File.Copy(sPath & Interop, sPath & Replace(Interop, Ver, ""), True)

            Try
                Dim saver As ChristianSteven.Settings.clsSettingsManager = New ChristianSteven.Settings.clsSettingsManager

                saver.SaveSettingValue("SelectedCrystalVersion", Ver, sAppPath & "\crdlive.config")
            Catch : End Try

            Close()
        Catch ex As Exception
            MsgBox("CRD Installer encountered an error." & vbCrLf & _
            "Error Message: " & ex.Message & vbCrLf & _
            "Error Number: " & Err.Number & vbCrLf & _
            "Error Line: " & 0, MsgBoxStyle.Exclamation, "CRD - Crystal Versions")

            Close()
        End Try
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        sAppPath = Application.StartupPath

        If sAppPath.EndsWith("\") = False Then sAppPath &= "\"

        Dim reader As ChristianSteven.Settings.clsSettingsManager = New ChristianSteven.Settings.clsSettingsManager

        Dim Ver As Integer = reader.GetSettingValue("SelectedCrystalVersion", 0, sAppPath & "\crdlive.config")

        'If Ver > 0 Then
        '    Select Case Ver
        '        Case 85
        '            opt8.Checked = True
        '        Case 90
        '            opt9.Checked = True
        '        Case 10
        '            opt10.Checked = True
        '        Case 11
        '            opt11.Checked = True
        '        Case 115
        '            opt115.Checked = True
        '    End Select
        'End If

        With version
            .Add("Crystal Reports 8.5.x")
            .Add("Crystal Reports 9.xx")
            .Add("Crystal Reports 10.xx")
            .Add("Crystal Reports 11.xx")
            .Add("Crystal Reports 11.5x (Release 2)")
            .Add("Crystal Reports 2008 (12.xx)")
        End With

        With desc
            .Add("If you have CR 8.5 Developer or Advanced above installed select 8.5.  CRD will use the dlls on your PC to export Crystal Reports written in Crystal Versions up to 8.5.")
            .Add("If you have CR 9 Developer or Advanced installed, select 9.xx.  CRD will use the dlls on your PC to export Crystal Reports written in Crystal Versions 8.5 and 9.xx")
            .Add("If you have CR 10 Developer or Advanced installed, select 10.xx.  CRD will use the dlls on your PC to export Crystal Reports written in Crystal Versions 8.5, 9.xx and 10.xx")
            .Add("If you have CR 11 Developer or Advanced installed, select 11.xx.  CRD will use the dlls on your PC to export Crystal Reports written in Crystal Versions 8.5, 9.xx, 10.xx and 11.xx")
            .Add("If you have CR 11 Release 2 Developer or Advanced installed, select 11.5x Release 2.  CRD will use the dlls on your PC to export Crystal Reports written in Crystal Versions 8.5, 9.xx, 10.xx and 11.xx")
            .Add("If you have CR 2008 installed, select 12.xx.  CRD will use the dlls on your PC to export Crystal Reports written in Crystal Versions 8.5, 9.xx, 10.xx, 11.xx and 12.xx")
        End With

        For Each s As String In version
            cmbVersions.Items.Add(s)
        Next

        If Ver > 0 Then
            Select Case Ver
                Case 85
                    cmbVersions.SelectedItem = cmbVersions.Items(0)
                Case 90
                    cmbVersions.SelectedItem = cmbVersions.Items(1)
                Case 10
                    cmbVersions.SelectedItem = cmbVersions.Items(2)
                Case 11
                    cmbVersions.SelectedItem = cmbVersions.Items(3)
                Case 115
                    cmbVersions.SelectedItem = cmbVersions.Items(4)
                Case 12
                    cmbVersions.SelectedItem = cmbVersions.Items(5)
            End Select
        Else
            Dim CRVersion As String = oMain._GetCRVersion()

            Select Case CRVersion
                Case 85
                    cmbVersions.SelectedItem = cmbVersions.Items(0)
                Case 90
                    cmbVersions.SelectedItem = cmbVersions.Items(1)
                Case 10
                    cmbVersions.SelectedItem = cmbVersions.Items(2)
                Case 11
                    cmbVersions.SelectedItem = cmbVersions.Items(3)
                Case 115
                    cmbVersions.SelectedItem = cmbVersions.Items(4)
                Case 12
                    cmbVersions.SelectedItem = cmbVersions.Items(5)
            End Select
        End If
    End Sub

    Private Sub cmbVersions_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbVersions.SelectedIndexChanged
        If cmbVersions.Text <> "" Then
            lblDesc.Text = desc(cmbVersions.SelectedIndex)
        End If
    End Sub
End Class
