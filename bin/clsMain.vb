Imports System.ServiceProcess
Imports Microsoft.Win32
Imports NineOrTen.clsSettingsManager
Public Class clsMain
    Dim sKey As String = "Software\ChristianSteven\CRD"
    Shared gConfigFile As String

    Shared Sub Main()
        Application.EnableVisualStyles()
        Application.DoEvents()
        Dim sArgs() As String
        Dim oSQL As New frmDataStorage

        sArgs = Environment.GetCommandLineArgs

        Dim oMain As New clsMain

        If sArgs.Length > 1 Then
            If sArgs(1) = "cdb" Then

                oSQL._CreateDatabase()

                End
            End If
        End If


        If sArgs.Length > 1 Then

            oSQL.optSQL.Checked = True

            oSQL._Action()

            End
        End If

        oMain._StopServices()

        Dim CRVersion As String
        Dim Response As DialogResult
        Dim sPath As String

        sPath = Application.StartupPath

        If sPath.EndsWith("\") = False Then sPath &= "\"

        gConfigFile = sPath & "crdlive.config"

        CRVersion = oMain._GetCRVersion()

        Response = MessageBox.Show("The installer has determined that you are using Crystal Reports " & CRVersion & "." & vbCrLf & _
            "If this is correct, then please press 'Yes', or to manually select your Crystal Reports version, select 'No'", _
            "Crystal Versions", MessageBoxButtons.YesNo, MessageBoxIcon.Information)

        If Response = DialogResult.Yes Then

            'Try
            '    IO.File.Delete(sPath & "Interop.CRAXDRT.dll")
            'Catch : End Try

            Select Case CRVersion
                Case "8.5"
                    IO.File.Copy(sPath & "interop85.dll", sPath & "crd.exe", True)
                    'IO.File.Copy(sPath & "Interop.CRAXDRT85.dll", sPath & "Interop.CRAXDRT.dll", True)
                Case "9"
                    IO.File.Copy(sPath & "interop90.dll", sPath & "crd.exe", True)
                    'IO.File.Copy(sPath & "Interop.CRAXDRT90.dll", sPath & "Interop.CRAXDRT.dll", True)
                Case "10"
                    IO.File.Copy(sPath & "interop10.dll", sPath & "crd.exe", True)
                    'IO.File.Copy(sPath & "Interop.CRAXDRT10.dll", sPath & "Interop.CRAXDRT.dll", True)
                Case "11"
                    IO.File.Copy(sPath & "interop11.dll", sPath & "crd.exe", True)
                    'IO.File.Copy(sPath & "Interop.CRAXDRT11.dll", sPath & "Interop.CRAXDRT.dll", True)
            End Select
        Else
            Dim oShow As New Form1
            Application.Run(oShow)
        End If

        'If oMain._CRDSQLExists() = False Then
        '    oSQL.ShowDialog()
        'End If

        oMain._CompactSystem()

        oMain._StartServices()

        Try
            IO.File.Move(sPath & "wPDFControl02.dll_xxx", sPath & "wPDFControl02.dll")
        Catch : End Try

        MessageBox.Show("Installation completed successfully. Thank you for using CRD!", Application.ProductName, _
                MessageBoxButtons.OK, MessageBoxIcon.Information)
        End
    End Sub

    Public Function _GetCRVersion() As String
        Dim CRVersion As String

        'try 11
        CRVersion = _ReadRegistry(Registry.LocalMachine, "Software\Business Objects\Suite 11.0\Shared Tools\Keycode.dll", "Path", "NONE")

        If CRVersion <> "NONE" Then
            Return "11"
        End If

        'try 10
        CRVersion = _ReadRegistry(Registry.LocalMachine, "Software\Crystal Decisions\10.0\Shared Tools\Keycode.dll", "Path", "NONE")

        If CRVersion <> "NONE" Then
            Return "10"
        End If

        'try 9
        CRVersion = _ReadRegistry(Registry.LocalMachine, "Software\Crystal Decisions\9.0\Shared Tools\Keycode.dll", "Path", "NONE")

        If CRVersion <> "NONE" Then
            Return "9"
        End If

        Return "8.5"
    End Function
    Public Overloads Function _ReadRegistry(ByVal settingName As String, _
    ByVal defaultValue As Object, Optional ByVal decryptValue As Boolean = False)
        Dim returnValue As Object

        returnValue = Appconfig.GetSettingValue(settingName, defaultValue, gConfigFile, decryptValue)

        Return returnValue
    End Function

    Public Overloads Function _ReadRegistry(ByVal ParentKey As RegistryKey, ByVal SubKey As String, _
    ByVal ValueName As String, ByRef Value As Object) As Object

        Dim Key As RegistryKey
        Dim Temp

        Temp = Value


        Try
            'Open the registry key.
            Key = ParentKey.OpenSubKey(SubKey, True)

            If Key Is Nothing Then 'if the key doesn't exist
                Return Temp
            End If

            'Get the value.
            Value = Key.GetValue(ValueName)

            Key.Close()

            If Value Is Nothing Then
                Return Temp
            Else
                Return Value
            End If
        Catch e As Exception
            Return Temp
        End Try

    End Function
    Public Sub _CompactSystem()

        'get the service type
        Dim sType As String

        Dim sAppPath As String = Application.StartupPath

        If sAppPath.EndsWith("\") = False Then sAppPath &= "\"

        Try
            Dim oDB As DAO.DBEngine = New DAO.DBEngine

            If System.IO.File.Exists(sAppPath & "crdlive.bak") Then
                System.IO.File.Delete(sAppPath & "crdlive.bak")
            End If

            If System.IO.File.Exists(sAppPath & "temp.tmp") Then
                System.IO.File.Delete(sAppPath & "temp.tmp")
            End If

            If System.IO.File.Exists(sAppPath & "temp1.tmp") Then
                System.IO.File.Delete(sAppPath & "temp1.tmp")
            End If

            'first create a copy of the current database
            System.IO.File.Copy(sAppPath & "crdlive.dat", sAppPath & "crdlive.bak")

            'create a copy of marslive.mdb for compacting
            System.IO.File.Copy(sAppPath & "crdlive.dat", sAppPath & "temp.tmp")

            'compact the file to temp1.mdb
            oDB.CompactDatabase(sAppPath & "temp.tmp", sAppPath & "temp1.tmp")

            'delete the older file
            System.IO.File.Delete(sAppPath & "temp.tmp")

            'delete the old crdlive
            System.IO.File.Delete(sAppPath & "crdlive.dat")

            'create a new marslive.mdb
            System.IO.File.Move(sAppPath & "temp1.tmp", sAppPath & "crdlive.dat")

        Catch : End Try
    End Sub
    Public Sub _StartServices()
        On Error Resume Next
        Dim sType As String
        Dim oMsg As New frmMsg

        oMsg.Show()

        Application.DoEvents()

        sType = _ReadRegistry("CRDService", "NONE")

        Select Case sType.ToLower
            Case "windowsapp"
                Me.RemoveCRD5App()

                Me.InstallBAS()

                Process.Start(Application.StartupPath & "\crdapp.exe")
            Case "windowsnt"
                Me.RemoveCRD5Svc()

                Me.InstallNTScheduler()

                Dim osrv As New ServiceController("CRD")

                If osrv.Status <> ServiceControllerStatus.Running Then
                    osrv.Start()
                End If

                osrv = New ServiceController("CRD Monitor")

                If osrv.Status <> ServiceControllerStatus.Running Then
                    osrv.Start()
                End If

                Process.Start(Application.StartupPath & "\ServiceMonitor.exe")
        End Select

        oMsg.Close()
    End Sub
    Private Sub _StopServices()
        On Error Resume Next

        Dim oProc() As Process

        oProc = Process.GetProcessesByName("crd")

        For Each o As Process In oProc
            o.Kill()
        Next

        oProc = Process.GetProcessesByName("crdapp")

        For Each o As Process In oProc
            o.Kill()
        Next

        Dim oSrv As New ServiceController("CRD")

        If oSrv.Status = ServiceControllerStatus.Running Then
            oSrv.Stop()
        End If

        oSrv = New ServiceController("CRD Monitor")

        If oSrv.Status = ServiceControllerStatus.Running Then
            oSrv.Stop()
        End If
    End Sub

    Private Sub RemoveCRD5App()
        On Error Resume Next
        Dim oReg As RegistryKey = Registry.LocalMachine.OpenSubKey("Software\Microsoft\Windows\CurrentVersion\Run")

        oReg.DeleteValue("CRD5")

    End Sub
    Private Sub RemoveCRD5Svc()
        On Error Resume Next

        Dim sParameters As String
        Dim srv As New ServiceController("CRD5")
        Dim srvmon As New ServiceController("CRD5 Monitor")
        Dim procInstaller As Process = New Process
        Dim sAppPath As String = Application.StartupPath
        Dim schar As Char = Chr(34)

        If sAppPath.EndsWith("\") = False Then sAppPath &= "\"

        If srv.Status = ServiceControllerStatus.Running Then
            srv.Stop()
        End If

        If srvmon.Status = ServiceControllerStatus.Running Then
            srvmon.Stop()
        End If

        'remove the services
        With procInstaller
            With .StartInfo
                .FileName = sAppPath & "installUtil.exe"
                .Arguments = schar & sAppPath & "crd5svc.exe" & schar & " /u"
                .WindowStyle = ProcessWindowStyle.Hidden
            End With

            .Start()
            .WaitForExit()
        End With

        With procInstaller
            With .StartInfo
                .FileName = sAppPath & "installUtil.exe"
                .Arguments = schar & sAppPath & "crd5svcmonitor.exe" & schar & " /u"
                .WindowStyle = ProcessWindowStyle.Hidden
            End With

            .Start()
            .WaitForExit()
        End With
    End Sub
    Public Shared Function CreateShortCut(ByVal path As String, ByVal targetFile As String) As Boolean

        Try
            Dim sAppPath As String = Application.StartupPath

            If sAppPath.EndsWith("\") = False Then sAppPath &= "\"

            Dim wshShell As IWshRuntimeLibrary.IWshShell_Class = New IWshRuntimeLibrary.IWshShell_Class

            If path.EndsWith("\") = False Then path &= "\"

            Dim shortcut As IWshRuntimeLibrary.IWshShortcut_Class = wshShell.CreateShortcut(path & "CRD Scheduler.lnk")

            shortcut.TargetPath = targetFile

            shortcut.IconLocation = sAppPath & "crd.exe" & ", 0"

            shortcut.Description = "CRD Background Application Scheduler"

            shortcut.WorkingDirectory = sAppPath

            shortcut.Save()

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Function InstallBAS()
        On Error Resume Next
        Dim sAppPath As String = Application.StartupPath
        Dim sChar As Char = Chr(34)

        If sAppPath.EndsWith("\") = False Then sAppPath &= "\"

        If CreateShortCut(Environment.GetFolderPath(Environment.SpecialFolder.Startup), sAppPath & "crdapp.exe") = False Then
            Dim oReg As RegistryKey = Registry.CurrentUser.OpenSubKey("Software\Microsoft\Windows\CurrentVersion\Run", True)

            oReg.SetValue("CRD", sAppPath & "crdapp.exe")

            oReg.Close()
        End If
    End Function
    Public Function InstallNTScheduler() As Boolean

        Dim srv As New ServiceController("CRD")
        Dim srvmon As New ServiceController("CRD Monitor")
        Dim sParameter As String
        Dim sCommand As String
        Dim procInstaller As Process
        Dim sAppPath As String = Application.StartupPath
        Dim schar As Char = Chr(34)

        If sAppPath.EndsWith("\") = False Then sAppPath &= "\"
        'stop services if applicable
        On Error Resume Next

        gConfigFile = sAppPath & "crdlive.config"

        Dim sDomain As String = clsSettingsManager.Appconfig.GetSettingValue("NTDomain", "", gConfigFile)
        Dim sUser As String = clsSettingsManager.Appconfig.GetSettingValue("NTLoginName", "", gConfigFile)
        Dim sPassword As String = clsSettingsManager.Appconfig.GetSettingValue("NTLoginPassword", "", gConfigFile, True)

        Dim s As String = srv.DisplayName

        If Err.Number = 0 Then Return True

        'install the main service
        sParameter = " /username=" & sDomain & "\" & sUser
        sParameter &= " /password=" & sPassword
        sParameter &= " " & schar & sAppPath & "crdsvc.exe" & schar

        procInstaller = New Process

        With procInstaller
            With .StartInfo
                .FileName = sAppPath & "installUtil.exe"
                .Arguments = sParameter
                .WindowStyle = ProcessWindowStyle.Hidden
            End With

            .Start()
            .WaitForExit()
        End With

        sParameter = schar & sAppPath & "crdsvcmonitor.exe" & schar

        With procInstaller
            With .StartInfo
                .FileName = sAppPath & "installUtil.exe"
                .Arguments = sParameter
                .WindowStyle = ProcessWindowStyle.Hidden
            End With

            .Start()
            .WaitForExit()
        End With

        Kill(sAppPath & "crdsvc.installlog")

        Return True
    End Function
    Private Function _CRDSQLExists() As Boolean
        Dim sCon As String

        Try
            sCon = "Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=CRD;Data Source=" & Environment.MachineName

            Dim oCon As New ADODB.Connection
            Dim oRs As New ADODB.Recordset

            oCon.Open(sCon)

            Dim SQL As String = "SELECT TOP 1 * FROM ReportAttr"

            oRs.Open(SQL, oCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

            oRs.Close()

            oCon.Close()

            Return True
        Catch
            Return False
        End Try
    End Function
End Class
