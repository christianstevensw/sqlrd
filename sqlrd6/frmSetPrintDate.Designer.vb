<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSetPrintDate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSetPrintDate))
        Me.optOther = New System.Windows.Forms.RadioButton
        Me.optToday = New System.Windows.Forms.RadioButton
        Me.dtTime = New System.Windows.Forms.DateTimePicker
        Me.dtDate = New System.Windows.Forms.DateTimePicker
        Me.pnDateTime = New System.Windows.Forms.TableLayoutPanel
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.btnOK = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.pnDateTime.SuspendLayout()
        Me.SuspendLayout()
        '
        'optOther
        '
        Me.optOther.AutoSize = True
        Me.optOther.Location = New System.Drawing.Point(12, 45)
        Me.optOther.Name = "optOther"
        Me.optOther.Size = New System.Drawing.Size(54, 17)
        Me.optOther.TabIndex = 0
        Me.optOther.Text = "Other:"
        Me.optOther.UseVisualStyleBackColor = True
        '
        'optToday
        '
        Me.optToday.AutoSize = True
        Me.optToday.Checked = True
        Me.optToday.Location = New System.Drawing.Point(12, 12)
        Me.optToday.Name = "optToday"
        Me.optToday.Size = New System.Drawing.Size(135, 17)
        Me.optToday.TabIndex = 1
        Me.optToday.TabStop = True
        Me.optToday.Text = "Today's Date and Time"
        Me.optToday.UseVisualStyleBackColor = True
        '
        'dtTime
        '
        Me.dtTime.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtTime.Location = New System.Drawing.Point(3, 55)
        Me.dtTime.Name = "dtTime"
        Me.dtTime.Size = New System.Drawing.Size(194, 20)
        Me.dtTime.TabIndex = 2
        '
        'dtDate
        '
        Me.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtDate.Location = New System.Drawing.Point(3, 16)
        Me.dtDate.Name = "dtDate"
        Me.dtDate.Size = New System.Drawing.Size(194, 20)
        Me.dtDate.TabIndex = 3
        '
        'pnDateTime
        '
        Me.pnDateTime.ColumnCount = 1
        Me.pnDateTime.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.pnDateTime.Controls.Add(Me.dtTime, 0, 3)
        Me.pnDateTime.Controls.Add(Me.dtDate, 0, 1)
        Me.pnDateTime.Controls.Add(Me.Label1, 0, 0)
        Me.pnDateTime.Controls.Add(Me.Label2, 0, 2)
        Me.pnDateTime.Enabled = False
        Me.pnDateTime.Location = New System.Drawing.Point(38, 68)
        Me.pnDateTime.Name = "pnDateTime"
        Me.pnDateTime.RowCount = 4
        Me.pnDateTime.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.pnDateTime.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.pnDateTime.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.pnDateTime.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.pnDateTime.Size = New System.Drawing.Size(200, 119)
        Me.pnDateTime.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(33, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Date:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 39)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(33, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Time:"
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(255, 12)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 5
        Me.btnOK.Text = "&OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(255, 45)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 6
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        Me.btnCancel.Visible = False
        '
        'frmSetPrintDate
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(332, 150)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.pnDateTime)
        Me.Controls.Add(Me.optToday)
        Me.Controls.Add(Me.optOther)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmSetPrintDate"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Set Print Date and Time"
        Me.pnDateTime.ResumeLayout(False)
        Me.pnDateTime.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents optOther As System.Windows.Forms.RadioButton
    Friend WithEvents optToday As System.Windows.Forms.RadioButton
    Friend WithEvents dtTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents pnDateTime As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
End Class
