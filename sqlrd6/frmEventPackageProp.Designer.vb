<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEventPackageProp
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEventPackageProp))
        Me.btnBrowse = New DevComponents.DotNetBar.ButtonX()
        Me.txtDescription = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtKeyword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtLocation = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.Label7 = New DevComponents.DotNetBar.LabelX()
        Me.Label8 = New DevComponents.DotNetBar.LabelX()
        Me.oTasks = New sqlrd.ucTasks()
        Me.lsvSchedules = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnDown = New DevComponents.DotNetBar.ButtonX()
        Me.btnUp = New DevComponents.DotNetBar.ButtonX()
        Me.btnDelete = New DevComponents.DotNetBar.ButtonX()
        Me.btnEdit = New DevComponents.DotNetBar.ButtonX()
        Me.btnAdd = New DevComponents.DotNetBar.ButtonX()
        Me.UcExecutionPath1 = New sqlrd.ucExecutionPath()
        Me.cmdApply = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.mnuAdd = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExisting = New System.Windows.Forms.ToolStripMenuItem()
        Me.txtID = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.tabProperties = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.tabGeneral = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabTasks = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.UcUpdate = New sqlrd.ucSchedule()
        Me.tabSchedule = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel6 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.scheduleHistory = New sqlrd.ucScheduleHistory()
        Me.tabHistory = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel5 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabExecution = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabSchedules = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.mnuAdd.SuspendLayout()
        CType(Me.tabProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabProperties.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuperTabControlPanel4.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuperTabControlPanel6.SuspendLayout()
        Me.SuperTabControlPanel5.SuspendLayout()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnBrowse
        '
        Me.btnBrowse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnBrowse.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnBrowse.Location = New System.Drawing.Point(462, 3)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(43, 21)
        Me.btnBrowse.TabIndex = 1
        Me.btnBrowse.Text = "..."
        '
        'txtDescription
        '
        '
        '
        '
        Me.txtDescription.Border.Class = "TextBoxBorder"
        Me.txtDescription.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDescription.Location = New System.Drawing.Point(116, 57)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(340, 189)
        Me.txtDescription.TabIndex = 3
        '
        'txtKeyword
        '
        '
        '
        '
        Me.txtKeyword.Border.Class = "TextBoxBorder"
        Me.txtKeyword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKeyword.Location = New System.Drawing.Point(116, 252)
        Me.txtKeyword.Name = "txtKeyword"
        Me.txtKeyword.Size = New System.Drawing.Size(340, 21)
        Me.txtKeyword.TabIndex = 4
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.Location = New System.Drawing.Point(116, 30)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(340, 21)
        Me.txtName.TabIndex = 0
        '
        'txtLocation
        '
        Me.txtLocation.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtLocation.Border.Class = "TextBoxBorder"
        Me.txtLocation.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtLocation.Location = New System.Drawing.Point(116, 3)
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.ReadOnly = True
        Me.txtLocation.Size = New System.Drawing.Size(340, 21)
        Me.txtLocation.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        '
        '
        '
        Me.Label5.BackgroundStyle.Class = ""
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.Location = New System.Drawing.Point(3, 30)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(78, 16)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Schedule Name"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        '
        '
        '
        Me.Label6.BackgroundStyle.Class = ""
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.Location = New System.Drawing.Point(3, 3)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(43, 16)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Location"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        '
        '
        '
        Me.Label7.BackgroundStyle.Class = ""
        Me.Label7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label7.Location = New System.Drawing.Point(3, 57)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(107, 16)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Description (optional)"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        '
        '
        '
        Me.Label8.BackgroundStyle.Class = ""
        Me.Label8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label8.Location = New System.Drawing.Point(3, 252)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(100, 16)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "Keywords (optional)"
        '
        'oTasks
        '
        Me.oTasks.BackColor = System.Drawing.Color.Transparent
        Me.oTasks.Dock = System.Windows.Forms.DockStyle.Fill
        Me.oTasks.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.oTasks.ForeColor = System.Drawing.Color.Navy
        Me.oTasks.Location = New System.Drawing.Point(0, 0)
        Me.oTasks.m_defaultTaks = False
        Me.oTasks.m_eventBased = False
        Me.oTasks.m_eventID = 99999
        Me.oTasks.m_forExceptionHandling = False
        Me.oTasks.m_showAfterType = False
        Me.oTasks.m_showExpanded = True
        Me.oTasks.Name = "oTasks"
        Me.oTasks.Size = New System.Drawing.Size(584, 436)
        Me.oTasks.TabIndex = 0
        '
        'lsvSchedules
        '
        '
        '
        '
        Me.lsvSchedules.Border.Class = "ListViewBorder"
        Me.lsvSchedules.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvSchedules.CheckBoxes = True
        Me.lsvSchedules.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvSchedules.FullRowSelect = True
        Me.lsvSchedules.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvSchedules.HideSelection = False
        Me.lsvSchedules.Location = New System.Drawing.Point(3, 3)
        Me.lsvSchedules.Name = "lsvSchedules"
        Me.lsvSchedules.Size = New System.Drawing.Size(396, 346)
        Me.lsvSchedules.TabIndex = 0
        Me.lsvSchedules.UseCompatibleStateImageBehavior = False
        Me.lsvSchedules.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Schedule Name"
        Me.ColumnHeader1.Width = 385
        '
        'btnDown
        '
        Me.btnDown.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnDown.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnDown.Image = CType(resources.GetObject("btnDown.Image"), System.Drawing.Image)
        Me.btnDown.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnDown.Location = New System.Drawing.Point(405, 324)
        Me.btnDown.Name = "btnDown"
        Me.btnDown.Size = New System.Drawing.Size(75, 25)
        Me.btnDown.TabIndex = 32
        '
        'btnUp
        '
        Me.btnUp.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnUp.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnUp.Image = CType(resources.GetObject("btnUp.Image"), System.Drawing.Image)
        Me.btnUp.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnUp.Location = New System.Drawing.Point(405, 297)
        Me.btnUp.Name = "btnUp"
        Me.btnUp.Size = New System.Drawing.Size(75, 25)
        Me.btnUp.TabIndex = 32
        '
        'btnDelete
        '
        Me.btnDelete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnDelete.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnDelete.Location = New System.Drawing.Point(405, 59)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(75, 25)
        Me.btnDelete.TabIndex = 32
        Me.btnDelete.Text = "&Delete"
        '
        'btnEdit
        '
        Me.btnEdit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnEdit.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnEdit.Location = New System.Drawing.Point(405, 31)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 25)
        Me.btnEdit.TabIndex = 32
        Me.btnEdit.Text = "&Edit"
        '
        'btnAdd
        '
        Me.btnAdd.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnAdd.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnAdd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnAdd.Location = New System.Drawing.Point(405, 3)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(75, 25)
        Me.btnAdd.TabIndex = 32
        Me.btnAdd.Text = "&Add"
        '
        'UcExecutionPath1
        '
        Me.UcExecutionPath1.BackColor = System.Drawing.Color.Transparent
        Me.UcExecutionPath1.Dock = System.Windows.Forms.DockStyle.Top
        Me.UcExecutionPath1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UcExecutionPath1.Location = New System.Drawing.Point(0, 0)
        Me.UcExecutionPath1.m_ExecutionFlow = "AFTER"
        Me.UcExecutionPath1.Name = "UcExecutionPath1"
        Me.UcExecutionPath1.Size = New System.Drawing.Size(573, 171)
        Me.UcExecutionPath1.TabIndex = 0
        '
        'cmdApply
        '
        Me.cmdApply.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdApply.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdApply.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdApply.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdApply.Location = New System.Drawing.Point(622, 3)
        Me.cmdApply.Name = "cmdApply"
        Me.cmdApply.Size = New System.Drawing.Size(75, 23)
        Me.cmdApply.TabIndex = 19
        Me.cmdApply.Text = "&Apply"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(541, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 18
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(460, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 17
        Me.cmdOK.Text = "&OK"
        '
        'mnuAdd
        '
        Me.mnuAdd.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuNew, Me.mnuExisting})
        Me.mnuAdd.Name = "ContextMenuStrip1"
        Me.mnuAdd.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuAdd.Size = New System.Drawing.Size(115, 48)
        '
        'mnuNew
        '
        Me.mnuNew.Name = "mnuNew"
        Me.mnuNew.Size = New System.Drawing.Size(114, 22)
        Me.mnuNew.Text = "&New"
        '
        'mnuExisting
        '
        Me.mnuExisting.Name = "mnuExisting"
        Me.mnuExisting.Size = New System.Drawing.Size(114, 22)
        Me.mnuExisting.Text = "Existing"
        '
        'txtID
        '
        '
        '
        '
        Me.txtID.Border.Class = "TextBoxBorder"
        Me.txtID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtID.Location = New System.Drawing.Point(45, 364)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(100, 21)
        Me.txtID.TabIndex = 20
        Me.txtID.Visible = False
        '
        'tabProperties
        '
        '
        '
        '
        '
        '
        '
        Me.tabProperties.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.tabProperties.ControlBox.MenuBox.Name = ""
        Me.tabProperties.ControlBox.Name = ""
        Me.tabProperties.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabProperties.ControlBox.MenuBox, Me.tabProperties.ControlBox.CloseBox})
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel6)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel5)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel4)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel3)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel2)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel1)
        Me.tabProperties.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabProperties.Location = New System.Drawing.Point(0, 0)
        Me.tabProperties.Name = "tabProperties"
        Me.tabProperties.ReorderTabsEnabled = True
        Me.tabProperties.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.tabProperties.SelectedTabIndex = 0
        Me.tabProperties.Size = New System.Drawing.Size(700, 436)
        Me.tabProperties.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.tabProperties.TabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabProperties.TabIndex = 21
        Me.tabProperties.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabGeneral, Me.tabSchedule, Me.tabSchedules, Me.tabTasks, Me.tabExecution, Me.tabHistory})
        Me.tabProperties.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        Me.tabProperties.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.TableLayoutPanel1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(116, 0)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(584, 436)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabGeneral
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.Controls.Add(Me.txtKeyword, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.txtDescription, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label8, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.btnBrowse, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label6, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label7, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.txtName, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtLocation, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label5, 0, 1)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(531, 277)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'tabGeneral
        '
        Me.tabGeneral.AttachedControl = Me.SuperTabControlPanel1
        Me.tabGeneral.GlobalItem = False
        Me.tabGeneral.Image = CType(resources.GetObject("tabGeneral.Image"), System.Drawing.Image)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Text = "General"
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.oTasks)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(116, 0)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(584, 436)
        Me.SuperTabControlPanel4.TabIndex = 0
        Me.SuperTabControlPanel4.TabItem = Me.tabTasks
        '
        'tabTasks
        '
        Me.tabTasks.AttachedControl = Me.SuperTabControlPanel4
        Me.tabTasks.GlobalItem = False
        Me.tabTasks.Image = CType(resources.GetObject("tabTasks.Image"), System.Drawing.Image)
        Me.tabTasks.Name = "tabTasks"
        Me.tabTasks.Text = "Tasks"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.UcUpdate)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(116, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(584, 436)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.tabSchedule
        '
        'UcUpdate
        '
        Me.UcUpdate.BackColor = System.Drawing.Color.Transparent
        Me.UcUpdate.Dock = System.Windows.Forms.DockStyle.Top
        Me.UcUpdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UcUpdate.Location = New System.Drawing.Point(0, 0)
        Me.UcUpdate.m_collaborationServerID = 0
        Me.UcUpdate.m_nextRun = "2012-05-30 15:50:30"
        Me.UcUpdate.m_RepeatUnit = ""
        Me.UcUpdate.mode = sqlrd.ucSchedule.modeEnum.SETUP
        Me.UcUpdate.Name = "UcUpdate"
        Me.UcUpdate.scheduleID = 0
        Me.UcUpdate.scheduleStatus = True
        Me.UcUpdate.sFrequency = "Daily"
        Me.UcUpdate.Size = New System.Drawing.Size(584, 361)
        Me.UcUpdate.TabIndex = 0
        '
        'tabSchedule
        '
        Me.tabSchedule.AttachedControl = Me.SuperTabControlPanel2
        Me.tabSchedule.GlobalItem = False
        Me.tabSchedule.Image = CType(resources.GetObject("tabSchedule.Image"), System.Drawing.Image)
        Me.tabSchedule.Name = "tabSchedule"
        Me.tabSchedule.Text = "Schedule"
        '
        'SuperTabControlPanel6
        '
        Me.SuperTabControlPanel6.Controls.Add(Me.scheduleHistory)
        Me.SuperTabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel6.Location = New System.Drawing.Point(116, 0)
        Me.SuperTabControlPanel6.Name = "SuperTabControlPanel6"
        Me.SuperTabControlPanel6.Size = New System.Drawing.Size(584, 436)
        Me.SuperTabControlPanel6.TabIndex = 0
        Me.SuperTabControlPanel6.TabItem = Me.tabHistory
        '
        'scheduleHistory
        '
        Me.scheduleHistory.BackColor = System.Drawing.Color.Transparent
        Me.scheduleHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.scheduleHistory.Location = New System.Drawing.Point(0, 0)
        Me.scheduleHistory.m_filter = False
        Me.scheduleHistory.m_historyLimit = 0
        Me.scheduleHistory.m_objectID = 0
        Me.scheduleHistory.m_scheduleType = sqlrd.clsMarsScheduler.enScheduleType.NONE
        Me.scheduleHistory.m_showControls = False
        Me.scheduleHistory.m_showSchedulesList = False
        Me.scheduleHistory.m_sortOrder = " ASC "
        Me.scheduleHistory.Name = "scheduleHistory"
        Me.scheduleHistory.Size = New System.Drawing.Size(584, 436)
        Me.scheduleHistory.TabIndex = 0
        '
        'tabHistory
        '
        Me.tabHistory.AttachedControl = Me.SuperTabControlPanel6
        Me.tabHistory.GlobalItem = False
        Me.tabHistory.Image = CType(resources.GetObject("tabHistory.Image"), System.Drawing.Image)
        Me.tabHistory.Name = "tabHistory"
        Me.tabHistory.Text = "History"
        '
        'SuperTabControlPanel5
        '
        Me.SuperTabControlPanel5.Controls.Add(Me.UcExecutionPath1)
        Me.SuperTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel5.Location = New System.Drawing.Point(127, 0)
        Me.SuperTabControlPanel5.Name = "SuperTabControlPanel5"
        Me.SuperTabControlPanel5.Size = New System.Drawing.Size(573, 436)
        Me.SuperTabControlPanel5.TabIndex = 0
        Me.SuperTabControlPanel5.TabItem = Me.tabExecution
        '
        'tabExecution
        '
        Me.tabExecution.AttachedControl = Me.SuperTabControlPanel5
        Me.tabExecution.GlobalItem = False
        Me.tabExecution.Image = CType(resources.GetObject("tabExecution.Image"), System.Drawing.Image)
        Me.tabExecution.Name = "tabExecution"
        Me.tabExecution.Text = "Execution Path"
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.btnDown)
        Me.SuperTabControlPanel3.Controls.Add(Me.lsvSchedules)
        Me.SuperTabControlPanel3.Controls.Add(Me.btnUp)
        Me.SuperTabControlPanel3.Controls.Add(Me.btnAdd)
        Me.SuperTabControlPanel3.Controls.Add(Me.btnDelete)
        Me.SuperTabControlPanel3.Controls.Add(Me.btnEdit)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(116, 0)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(584, 436)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.tabSchedules
        '
        'tabSchedules
        '
        Me.tabSchedules.AttachedControl = Me.SuperTabControlPanel3
        Me.tabSchedules.GlobalItem = False
        Me.tabSchedules.Image = CType(resources.GetObject("tabSchedules.Image"), System.Drawing.Image)
        Me.tabSchedules.Name = "tabSchedules"
        Me.tabSchedules.Text = "Schedules"
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.cmdApply)
        Me.FlowLayoutPanel3.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel3.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(0, 436)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(700, 30)
        Me.FlowLayoutPanel3.TabIndex = 22
        '
        'frmEventPackageProp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(700, 466)
        Me.ControlBox = False
        Me.Controls.Add(Me.tabProperties)
        Me.Controls.Add(Me.FlowLayoutPanel3)
        Me.Controls.Add(Me.txtID)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmEventPackageProp"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Event-Based Package"
        Me.mnuAdd.ResumeLayout(False)
        CType(Me.tabProperties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabProperties.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.SuperTabControlPanel4.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel6.ResumeLayout(False)
        Me.SuperTabControlPanel5.ResumeLayout(False)
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UcExecutionPath1 As sqlrd.ucExecutionPath
    Friend WithEvents oTasks As sqlrd.ucTasks
    Friend WithEvents btnBrowse As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDescription As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtKeyword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtLocation As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents lsvSchedules As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnDown As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnUp As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnDelete As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnEdit As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnAdd As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdApply As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents mnuAdd As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExisting As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents txtID As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents tabProperties As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel6 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabHistory As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel5 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabExecution As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabTasks As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabSchedules As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabSchedule As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabGeneral As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents UcUpdate As sqlrd.ucSchedule
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents scheduleHistory As sqlrd.ucScheduleHistory
End Class
