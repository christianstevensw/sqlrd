Friend Class frmDock
    Private Sub btnOptions_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnOptions.Click

        If clsMarsSecurity._HasGroupAccess("Options") = True Then
            Dim oConfig As frmOptions = New frmOptions

            oConfig.ShowDialog(oMain)
        End If

    End Sub

    Private Sub btnSystemMonitor_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnSystemMonitor.Click

        If clsMarsSecurity._HasGroupAccess("System Monitor") = True Then
            Dim oSys As frmSystemMonitor = New frmSystemMonitor

            oSys.Show()
        End If
    End Sub

    Private Sub btnProperties_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnProperties.Click

        Dim oForm As frmWindow
        Dim olsv As ListView
        Dim sType As String
        Dim nID As Integer

        oForm = oWindow(nWindowCurrent)


        If olsv.SelectedItems.Count = 0 Then Return

        sType = Convert.ToString(olsv.SelectedItems(0).Tag).Split(":")(0)

        nID = Convert.ToString(olsv.SelectedItems(0).Tag).Split(":")(1)

        Select Case sType.ToLower
            Case "package"

                If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules", nID, clsMarsScheduler.enScheduleType.PACKAGE) = False Then
                    Return
                End If

                Dim oPack As New frmPackageProp

                oPack.EditPackage(nID)
            Case "report"
                If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules", nID, clsMarsScheduler.enScheduleType.REPORT) = False Then
                    Return
                End If

                Dim oReport As New frmSingleProp

                oReport.EditSchedule(nID)
            Case "automation"
                If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules", nID, clsMarsScheduler.enScheduleType.AUTOMATION) = False Then
                    Return
                End If

                Dim oAuto As New frmAutoProp

                oAuto.EditSchedule(nID)
            Case "event"
                If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules", nID, clsMarsScheduler.enScheduleType.EVENTBASED) = False Then
                    Return
                End If

                Dim oEvent As New frmEventProp

                oEvent.EditSchedule(nID)
            Case "packed report"
                If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules", nID, clsMarsScheduler.enScheduleType.REPORT) = False Then
                    Return
                End If

                Dim oReport As New frmPackedReport

                oReport.EditReport(nID)
        End Select
    End Sub

    Private Sub btnDesign_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnDesign.Click
        Dim oList As ListView



        If oList.SelectedItems.Count = 0 Then Return

        Dim nID As Integer = oList.SelectedItems(0).Tag.Split(":")(1)

        Dim oReport As New clsMarsReport

        oReport.DesignReport(nID)
    End Sub

    Private Sub btnCalendar_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnCalendar.Click
        If clsMarsSecurity._HasGroupAccess("Custom Calendar") = True Then
            Dim oCalendar As New frmCalendarExplorer
            oCalendar.MdiParent = oMain

            oCalendar.Show()
        End If
    End Sub

    Private Sub btnAddressBook_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnAddressBook.Click



        Dim oAddress As New frmAddressBook

        oAddress.MdiParent = oMain

        oAddress.Show()
    End Sub

    Private Sub btnSingle_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnSingle.Click

        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
            Dim oSingle As frmSingleScheduleWizard = New frmSingleScheduleWizard

            oSingle.ShowDialog(oMain)

            clsMarsUI.MainUI.SaveRegistry("LastScheduleType", "Single")
        End If
    End Sub

    Private Sub btnPackage_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnPackage.Click

        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
            Dim oPack As frmPackWizard = New frmPackWizard

            oPack.ShowDialog(oMain)

            clsMarsUI.MainUI.SaveRegistry("LastScheduleType", "Package")
        End If
    End Sub

    Private Sub btnDynamic_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnDynamic.Click

        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
            Dim oDynamic As New frmDynamicSchedule

            oDynamic.ShowDialog(oMain)
        End If

    End Sub

    Private Sub btnDynamicPackage_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnDynamicPackage.Click
        Dim oDynamic As New frmDynamicPackage

        oDynamic.ShowDialog()

        clsMarsUI.MainUI.SaveRegistry("LastScheduleType", "Dynamic-Package")
    End Sub

    Private Sub btnAutomation_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnAutomation.Click

        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
            Dim oAuto As New frmAutoScheduleWizard

            oAuto.ShowDialog(oMain)

            clsMarsUI.MainUI.SaveRegistry("LastScheduleType", "Automation")
        End If

    End Sub

    Private Sub btnEvent_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnEvent.Click

        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
            Dim oEvent As New frmEventWizard6

            oEvent.ShowDialog(oMain)

            clsMarsUI.MainUI.SaveRegistry("LastScheduleType", "Event-Based")
        End If

    End Sub

    Private Sub btnBursting_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnBursting.Click


    End Sub

    Private Sub btnHelp_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnHelp.Click

        Try
            Process.Start(sAppPath & "sqlrd.chm")
        Catch : End Try

    End Sub

    Private Sub btnForums_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnForums.Click
        Try
            clsWebProcess.Start("http://www.christiansteven.com/userforum_SQL-RD.htm")
        Catch : End Try
    End Sub

    Private Sub btnKbase_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnKbase.Click
        Try
            clsWebProcess.Start("http://www.christiansteven.com/support_sql-rd.htm")
        Catch : End Try
    End Sub

    Private Sub btnHowTo_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnHowTo.Click

        clsWebProcess.Start("http://www.christiansteven.com/sql-rd/demos/sql-rd_demos.htm")
    End Sub

    Private Sub BubbleButton3_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnEventPackage.Click
        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
            Dim oBurst As New frmEventPackage

            oBurst.ShowDialog()

            clsMarsUI.MainUI.SaveRegistry("LastScheduleType", "Event-Based Package")
        End If
    End Sub

    Private Sub btnDDSchedule_Click(ByVal sender As Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnDDSchedule.Click
        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
          

            Dim oD As frmRDScheduleWizard = New frmRDScheduleWizard

            oD.ShowDialog()

            clsMarsUI.MainUI.SaveRegistry("LastScheduleType", "Data-Driven Schedules")
        End If
    End Sub

    Private Sub btnDDPackage_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnDDPackage.Click
        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
          

            Dim oD As frmDataDrivenPackage = New frmDataDrivenPackage

            oD.ShowDialog()

            clsMarsUI.MainUI.SaveRegistry("LastScheduleType", "Data-Driven Package")
        End If
    End Sub
End Class