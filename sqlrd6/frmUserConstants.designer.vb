<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUserConstants
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUserConstants))
        Me.lblsearch = New DevComponents.DotNetBar.LabelX()
        Me.txtSearch = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btnCreate = New DevComponents.DotNetBar.ButtonX()
        Me.btnEdit = New DevComponents.DotNetBar.ButtonX()
        Me.BtnRemove = New DevComponents.DotNetBar.ButtonX()
        Me.btnOK = New DevComponents.DotNetBar.ButtonX()
        Me.lsvConstants = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.Headerformula = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.chkSearchContents = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.ReflectionImage1 = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblsearch
        '
        '
        '
        '
        Me.lblsearch.BackgroundStyle.Class = ""
        Me.lblsearch.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblsearch.ForeColor = System.Drawing.Color.MediumBlue
        Me.lblsearch.Location = New System.Drawing.Point(3, 3)
        Me.lblsearch.Name = "lblsearch"
        Me.lblsearch.Size = New System.Drawing.Size(37, 20)
        Me.lblsearch.TabIndex = 0
        Me.lblsearch.Text = "Search"
        '
        'txtSearch
        '
        '
        '
        '
        Me.txtSearch.Border.Class = "TextBoxBorder"
        Me.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSearch.Location = New System.Drawing.Point(46, 3)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(257, 20)
        Me.txtSearch.TabIndex = 1
        '
        'btnCreate
        '
        Me.btnCreate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnCreate.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnCreate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCreate.ForeColor = System.Drawing.Color.MediumBlue
        Me.btnCreate.Location = New System.Drawing.Point(313, 56)
        Me.btnCreate.Name = "btnCreate"
        Me.btnCreate.Size = New System.Drawing.Size(75, 24)
        Me.btnCreate.TabIndex = 0
        Me.btnCreate.Text = "&Add"
        '
        'btnEdit
        '
        Me.btnEdit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnEdit.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.MediumBlue
        Me.btnEdit.Location = New System.Drawing.Point(313, 85)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 24)
        Me.btnEdit.TabIndex = 4
        Me.btnEdit.Text = "&Edit"
        '
        'BtnRemove
        '
        Me.BtnRemove.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BtnRemove.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.BtnRemove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnRemove.ForeColor = System.Drawing.Color.MediumBlue
        Me.BtnRemove.Location = New System.Drawing.Point(313, 114)
        Me.BtnRemove.Name = "BtnRemove"
        Me.BtnRemove.Size = New System.Drawing.Size(75, 24)
        Me.BtnRemove.TabIndex = 5
        Me.BtnRemove.Text = "&Remove"
        '
        'btnOK
        '
        Me.btnOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnOK.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.ForeColor = System.Drawing.Color.MediumBlue
        Me.btnOK.Location = New System.Drawing.Point(313, 282)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 24)
        Me.btnOK.TabIndex = 6
        Me.btnOK.Text = "&Close"
        '
        'lsvConstants
        '
        '
        '
        '
        Me.lsvConstants.Border.Class = "ListViewBorder"
        Me.lsvConstants.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvConstants.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Headerformula})
        Me.lsvConstants.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lsvConstants.HideSelection = False
        Me.lsvConstants.Location = New System.Drawing.Point(3, 56)
        Me.lsvConstants.Name = "lsvConstants"
        Me.lsvConstants.Size = New System.Drawing.Size(304, 250)
        Me.lsvConstants.TabIndex = 8
        Me.lsvConstants.UseCompatibleStateImageBehavior = False
        Me.lsvConstants.View = System.Windows.Forms.View.Details
        '
        'Headerformula
        '
        Me.Headerformula.Text = "Constant Name"
        Me.Headerformula.Width = 290
        '
        'chkSearchContents
        '
        Me.chkSearchContents.AutoSize = True
        '
        '
        '
        Me.chkSearchContents.BackgroundStyle.Class = ""
        Me.chkSearchContents.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkSearchContents.Location = New System.Drawing.Point(50, 33)
        Me.chkSearchContents.Name = "chkSearchContents"
        Me.chkSearchContents.Size = New System.Drawing.Size(153, 15)
        Me.chkSearchContents.TabIndex = 12
        Me.chkSearchContents.Text = "Include definition in search"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.lblsearch)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtSearch)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(558, 27)
        Me.FlowLayoutPanel1.TabIndex = 13
        '
        'ReflectionImage1
        '
        '
        '
        '
        Me.ReflectionImage1.BackgroundStyle.Class = ""
        Me.ReflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ReflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.ReflectionImage1.Image = CType(resources.GetObject("ReflectionImage1.Image"), System.Drawing.Image)
        Me.ReflectionImage1.Location = New System.Drawing.Point(418, 56)
        Me.ReflectionImage1.Name = "ReflectionImage1"
        Me.ReflectionImage1.Size = New System.Drawing.Size(128, 245)
        Me.ReflectionImage1.TabIndex = 14
        '
        'frmUserConstants
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(558, 313)
        Me.ControlBox = False
        Me.Controls.Add(Me.ReflectionImage1)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.chkSearchContents)
        Me.Controls.Add(Me.lsvConstants)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.BtnRemove)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.btnCreate)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmUserConstants"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "User Constants"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblsearch As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtSearch As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnCreate As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnEdit As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BtnRemove As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lsvConstants As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents Headerformula As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkSearchContents As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents ReflectionImage1 As DevComponents.DotNetBar.Controls.ReflectionImage

End Class
