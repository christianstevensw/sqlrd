Public Class frmSystemError

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        End
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Process.Start(Application.StartupPath & "\" & assemblyName)

        System.Threading.Thread.Sleep(1000)

        End
    End Sub

    Public Shared Sub showError(ByVal ex As Exception, ByVal errNumber As Long, ByVal errLine As Integer, Optional ByVal solution As String = "Please restart SQL-RD and try again")

        Dim oform As frmSystemError = New frmSystemError

        With oform
            .txterrLine.Text = errLine
            .txterrNumber.Text = errNumber
            .txterrSource.Text = "sqlrd.clsMain.Main"
            .txtError.Text = ex.Message & vbCrLf & vbCrLf & ex.ToString
            .txtError.SelectionStart = 0
            .txtError.SelectionLength = 0
            .txtSolution.Text = solution
        End With

        Dim msg As String

        msg = "PC Name: " & Environment.MachineName & vbCrLf & _
        "Date: " & Date.Now & vbCrLf & _
        "Error Description: " & oform.txtError.Text & vbCrLf & _
        "Error Number: " & oform.txterrNumber.Text & vbCrLf & _
        "Error Line: " & oform.txterrLine.Text & vbCrLf & _
        "Error Source: " & oform.txterrSource.Text

        msg = "-------------------------------------------------" & vbCrLf & _
        msg & vbCrLf & _
        "-------------------------------------------------"

        SaveTextToFile(msg, Application.StartupPath & "\crderror.log", , True, True)

        If RunEditor = True Then oform.ShowDialog()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            Dim msg As String

            msg = "Error Description: " & txtError.Text & vbCrLf & _
            "Error Number: " & txterrNumber.Text & vbCrLf & _
            "Error Line: " & txterrLine.Text & vbCrLf & _
            "Error Source: " & txterrSource.Text

            Clipboard.SetText(msg, TextDataFormat.Text)
        Catch : End Try
    End Sub
End Class