Imports sqlrd.clsMarsData
Public Class frmEventPackageProp
    Dim ep As New ErrorProvider
    Dim OkayToClose As Boolean = False

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Dim oFolders As frmFolders = New frmFolders
        Dim folderDetails() As String

        folderDetails = oFolders.GetFolder

        txtLocation.Text = folderDetails(0)
        txtLocation.Tag = folderDetails(1)
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        cmdApply.Enabled = True
    End Sub
    Private Sub btnAdd_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnAdd.MouseClick
        Me.mnuAdd.Show(Me.btnAdd, New Point(e.X, e.Y))
    End Sub


    Private Sub mnuNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNew.Click
        Dim oEvent As New frmEventWizard6
        Dim eventDtls As Hashtable
        Dim eventName As String = ""
        Dim eventID As Integer = 0
        Dim eventStatus As String

        oEvent.txtLocation.Tag = "99999"
        oEvent.txtLocation.Text = "\Package"
        oEvent.txtLocation.Enabled = False
        oEvent.btnBrowse.Enabled = False
        eventDtls = oEvent.AddSchedule(lsvSchedules.Items.Count)

        If eventDtls IsNot Nothing Then
            Dim lsv As ListViewItem = Me.lsvSchedules.Items.Add(lsvSchedules.Items.Count + 1 & ":" & eventDtls.Item("Name"))
            lsv.Tag = eventDtls.Item("ID")
            lsv.ImageIndex = 0

            If eventDtls.Item("Status") = "Disabled" Then
                lsv.Checked = False
            Else
                lsv.Checked = True
            End If
        End If

    End Sub

    Private Sub mnuExisting_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExisting.Click
        Dim values As Hashtable
        Dim getEvents As frmFolders = New frmFolders

        getEvents.m_eventOnly = True
        values = getEvents.GetEventBasedSchedule

        If values IsNot Nothing Then
            Dim lsv As ListViewItem = lsvSchedules.Items.Add(lsvSchedules.Items.Count + 1 & ":" & values.Item("Name"))
            lsv.Tag = values.Item("ID")
            lsv.ImageIndex = 0

            clsMarsData.WriteData("UPDATE EventAttr6 SET Status = 1 WHERE EventID =" & lsv.Tag)

            lsv.Checked = True

        End If

    End Sub
    Private Sub btnDown_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDown.Click
        MoveListViewItem(Me.lsvSchedules, False)
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lsvSchedules.SelectedItems.Count = 0 Then Return

        Dim frmEventProp As New frmEventProp
        frmEventProp.m_Packed = True
        Dim status As Boolean = frmEventProp.EditSchedule(lsvSchedules.SelectedItems(0).Tag)

        lsvSchedules.SelectedItems(0).Checked = status

        LoadSchedules()
    End Sub
    Private Function MoveListViewItem(ByVal lsv As ListView, ByVal moveUp As Boolean) As Integer
        If lsv.SelectedItems.Count = 0 Then Return 0

        Dim value As Integer

        If moveUp = False Then
            If lsv.SelectedItems(0).Index = lsv.Items.Count - 1 Then Return 0

            value = 1
        Else
            If lsv.SelectedItems(0).Index = 0 Then Return 0

            value = -1
        End If

        Dim itemList As ListViewItem()
        Dim I As Integer = 0
        Dim newIndex As Integer

        ReDim itemList(lsv.Items.Count - 1)

        'copy into an array
        For Each item As ListViewItem In lsv.Items
            itemList(I) = item
            I += 1
        Next

        newIndex = lsv.SelectedItems(0).Index + value

        Dim selItem As ListViewItem = lsv.SelectedItems(0)
        Dim nextItem As ListViewItem = lsv.Items(selItem.Index + value)

        Dim selPrefix As String = lsv.SelectedItems(0).Text.Split(":")(0)
        Dim nextPrefix As String = lsv.Items(selItem.Index + value).Text.Split(":")(0)

        selItem.Text = nextPrefix & ":" & selItem.Text.Split(":")(1)
        nextItem.Text = selPrefix & ":" & nextItem.Text.Split(":")(1)

        itemList(selItem.Index + value) = selItem
        itemList(selItem.Index) = nextItem

        lsv.Items.Clear()

        For Each item As ListViewItem In itemList
            lsv.Items.Add(item)

            If item.Index = newIndex Then
                item.Selected = True
            Else
                item.Selected = False
            End If
        Next

        lsv.Refresh()

        cmdApply.Enabled = True

        Return newIndex
    End Function
    Private Sub btnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUp.Click
        MoveListViewItem(Me.lsvSchedules, True)
    End Sub

    Public Sub EditPackage(ByVal nPackID As Integer)
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        If IsFeatEnabled(gEdition.CORPORATE, modFeatCodes.s3_EventBasedScheds) = False Then
            _NeedUpgrade(gEdition.CORPORATE, Me, "Event-Based Packages")
            Return
        End If

        txtID.Text = nPackID

        SQL = "SELECT * FROM EventPackageAttr WHERE EventPackID =" & nPackID

        oRs = GetData(SQL)

        If oRs Is Nothing Then Return

        If oRs.EOF = True Then Return

        txtName.Text = oRs("packagename").Value
        txtLocation.Tag = oRs("parent").Value

        Me.UcExecutionPath1.m_ExecutionFlow = oRs("executionflow").Value

        oRs.Close()

        UcUpdate.mode = ucSchedule.modeEnum.EDIT
        UcUpdate.loadScheduleInfo(txtID.Text, clsMarsScheduler.enScheduleType.EVENTPACKAGE, txtDescription, txtKeyword, oTasks)

        Me.LoadSchedules()

        oRs = GetData("SELECT FolderName FROM Folders WHERE FolderID = " & txtLocation.Tag)

        txtLocation.Text = oRs.Fields(0).Value

        oRs.Close()

        Me.ShowDialog()
    End Sub

    Private Sub LoadSchedules()
        Dim Sql As String = "SELECT * FROM EventAttr6 WHERE PackID =" & txtID.Text & " ORDER BY PackOrderID"

        Dim oRs As ADODB.Recordset = GetData(Sql)

        If oRs Is Nothing Then Return

        lsvSchedules.Items.Clear()

        Dim I As Integer = 1

        Do While oRs.EOF = False
            Dim lsv As ListViewItem = New ListViewItem(I & ":" & CType(oRs("eventname").Value, String))

            lsv.Tag = oRs("eventid").Value
            lsv.ImageIndex = 0

            If oRs.Fields("status").Value = 1 Then
                lsv.Checked = True
            Else
                lsv.Checked = False
            End If

            Me.lsvSchedules.Items.Add(lsv)

            oRs.MoveNext()

            I += 1
        Loop

        oRs.Close()
    End Sub

    Private Sub cmdApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdApply.Click

        Try
            If txtName.Text.Length = 0 Then
                SetError(txtName, "Please enter the schedule name")
                txtName.Focus()
                Me.tabProperties.SelectedTab = Me.tabGeneral
                OkayToClose = False
                Return
            ElseIf txtLocation.Text.Length = 0 Then
                SetError(txtLocation, "Please select the folder where the schedule resides")
                txtLocation.Focus()
                Me.tabProperties.SelectedTab = Me.tabGeneral
                OkayToClose = False
                Return
            ElseIf clsMarsUI.candoRename(txtName.Text, Me.txtLocation.Tag, clsMarsScheduler.enScheduleType.EVENTPACKAGE, txtID.Text) = False Then
                SetError(txtName, "An Event-Based Package with this name already exists in this folder. Please use a different name")
                txtName.Focus()
                Me.tabProperties.SelectedTab = Me.tabGeneral
                OkayToClose = False
                Return
            ElseIf UcUpdate.isAllDataValid() = False Then
                OkayToClose = False
                Return
            End If

            Dim SQL As String

            SQL = "UPDATE EventPackageAttr SET Parent =" & txtLocation.Tag & "," & _
            "PackageName ='" & SQLPrepare(txtName.Text) & "'," & _
            "ExecutionFlow ='" & Me.UcExecutionPath1.m_ExecutionFlow & "' " & _
            "WHERE EventPackID =" & txtID.Text

            WriteData(SQL)

            For Each item As ListViewItem In lsvSchedules.Items
                Dim nID As Integer = item.Tag

                SQL = "UPDATE EventAttr6 SET PackOrderID=" & item.Index & ", Status =" & Convert.ToInt32(item.Checked) & ", PackID =" & txtID.Text & " WHERE EventID=" & nID

                WriteData(SQL)
            Next

            Dim nRepeat As String = "0"

            If ucUpdate.chkRepeat.Checked = False Then
                nRepeat = "0"
            Else
                nRepeat = Convert.ToString(UcUpdate.txtRepeatInterval.Value).Replace(",", ".")
            End If

            With UcUpdate
                .updateSchedule(txtID.Text, clsMarsScheduler.enScheduleType.EVENTPACKAGE, txtDescription.Text, txtKeyword.Text)
            End With


            OkayToClose = True

            setAndShowMessageOnControl(cmdApply, "Saved!", "", "Your schedule has been updated successfully")
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub cmdOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Me.cmdApply_Click(sender, e)

        If OkayToClose = True Then
            Close()
        End If

    End Sub

    Private Sub frmEventPackageProp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        Me.tabProperties.SelectedTab = Me.tabGeneral

        oTasks.tvTasks.ColumnsVisible = False
    End Sub
   

   

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim Res As DialogResult
        Dim oEvent As clsMarsEvent = New clsMarsEvent

        Res = MessageBox.Show("Would you like to delete the selected event-based schedule(s)?" & vbCrLf & _
        "Click YES to permanently delete the schedule, NO to simply remove it from this package or CANCEL to leave everything unchanged", _
        Application.ProductName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)


        Select Case Res
            Case Windows.Forms.DialogResult.Yes
                For Each item As ListViewItem In lsvSchedules.SelectedItems
                    Dim eventID As Integer = item.Tag

                    oEvent.DeleteEvent(eventID)

                    item.Remove()
                Next
            Case Windows.Forms.DialogResult.No
                For Each item As ListViewItem In lsvSchedules.SelectedItems
                    Dim eventID As Integer = item.Tag

                    Dim SQL As String = "UPDATE EventAttr6 SET PackID =0, Parent =" & txtLocation.Tag & " WHERE EventID =" & eventID

                    clsMarsData.WriteData(SQL)

                    item.Remove()
                Next
            Case Windows.Forms.DialogResult.Cancel
                Return
        End Select

        cmdApply.Enabled = True
    End Sub

    Private Sub lsvSchedules_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvSchedules.DoubleClick
        btnEdit_Click(sender, e)
    End Sub


    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged, _
    txtDescription.TextChanged, txtKeyword.TextChanged, txtLocation.TextChanged
        cmdApply.Enabled = True

    End Sub

    Private Sub ucUpdate_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        cmdApply.Enabled = True
    End Sub

    Private Sub lsvSchedules_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles lsvSchedules.ItemCheck
        cmdApply.Enabled = True
    End Sub

    Private Sub lsvSchedules_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lsvSchedules.ItemChecked
        cmdApply.Enabled = True
    End Sub

   
    Private Sub tabProperties_SelectedTabChanged(sender As System.Object, e As DevComponents.DotNetBar.SuperTabStripSelectedTabChangedEventArgs) Handles tabProperties.SelectedTabChanged
        If e.NewValue.Text = "History" Then
            With scheduleHistory
                .m_objectID = txtID.Text
                .m_scheduleType = clsMarsScheduler.enScheduleType.EVENTPACKAGE
                .m_showControls = True
                .DrawScheduleHistory()
            End With
       
        End If
    End Sub
End Class