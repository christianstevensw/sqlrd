<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOperationalHours
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.chkMonday = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkTuesday = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.pnSunday = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label7 = New DevComponents.DotNetBar.LabelX()
        Me.dtopensunday = New System.Windows.Forms.DateTimePicker()
        Me.Label8 = New DevComponents.DotNetBar.LabelX()
        Me.dtclosesunday = New System.Windows.Forms.DateTimePicker()
        Me.pnFriday = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label9 = New DevComponents.DotNetBar.LabelX()
        Me.dtopenfriday = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New DevComponents.DotNetBar.LabelX()
        Me.dtclosefriday = New System.Windows.Forms.DateTimePicker()
        Me.pnSaturday = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.dtopensaturday = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.dtclosesaturday = New System.Windows.Forms.DateTimePicker()
        Me.pnWednesday = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label11 = New DevComponents.DotNetBar.LabelX()
        Me.dtopenwednesday = New System.Windows.Forms.DateTimePicker()
        Me.Label12 = New DevComponents.DotNetBar.LabelX()
        Me.dtclosewednesday = New System.Windows.Forms.DateTimePicker()
        Me.pnMonday = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label13 = New DevComponents.DotNetBar.LabelX()
        Me.dtopenmonday = New System.Windows.Forms.DateTimePicker()
        Me.Label14 = New DevComponents.DotNetBar.LabelX()
        Me.dtclosemonday = New System.Windows.Forms.DateTimePicker()
        Me.pnThursday = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.dtopenthursday = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.dtclosethursday = New System.Windows.Forms.DateTimePicker()
        Me.chkWednesday = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.pnTuesday = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.dtopentuesday = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.dtclosetuesday = New System.Windows.Forms.DateTimePicker()
        Me.chkThursday = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkFriday = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkSaturday = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkSunday = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkAll = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.pnAll = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label15 = New DevComponents.DotNetBar.LabelX()
        Me.dtopenall = New System.Windows.Forms.DateTimePicker()
        Me.Label16 = New DevComponents.DotNetBar.LabelX()
        Me.dtcloseall = New System.Windows.Forms.DateTimePicker()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label17 = New DevComponents.DotNetBar.LabelX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.DividerLabel1 = New sqlrd.DividerLabel()
        Me.DividerLabel2 = New sqlrd.DividerLabel()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.pnSunday.SuspendLayout()
        Me.pnFriday.SuspendLayout()
        Me.pnSaturday.SuspendLayout()
        Me.pnWednesday.SuspendLayout()
        Me.pnMonday.SuspendLayout()
        Me.pnThursday.SuspendLayout()
        Me.pnTuesday.SuspendLayout()
        Me.pnAll.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'chkMonday
        '
        Me.chkMonday.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkMonday.AutoSize = True
        '
        '
        '
        Me.chkMonday.BackgroundStyle.Class = ""
        Me.chkMonday.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkMonday.Location = New System.Drawing.Point(3, 37)
        Me.chkMonday.Name = "chkMonday"
        Me.chkMonday.Size = New System.Drawing.Size(62, 15)
        Me.chkMonday.TabIndex = 2
        Me.chkMonday.Tag = "0"
        Me.chkMonday.Text = "Monday"
        '
        'chkTuesday
        '
        Me.chkTuesday.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkTuesday.AutoSize = True
        '
        '
        '
        Me.chkTuesday.BackgroundStyle.Class = ""
        Me.chkTuesday.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkTuesday.Location = New System.Drawing.Point(3, 71)
        Me.chkTuesday.Name = "chkTuesday"
        Me.chkTuesday.Size = New System.Drawing.Size(65, 15)
        Me.chkTuesday.TabIndex = 4
        Me.chkTuesday.Tag = "1"
        Me.chkTuesday.Text = "Tuesday"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 325.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.pnSunday, 1, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.pnFriday, 1, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.pnSaturday, 1, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.pnWednesday, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.pnMonday, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.chkMonday, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.pnThursday, 1, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.chkTuesday, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.chkWednesday, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.pnTuesday, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.chkThursday, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.chkFriday, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.chkSaturday, 0, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.chkSunday, 0, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.chkAll, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.pnAll, 1, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 39)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 8
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(350, 277)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'pnSunday
        '
        Me.pnSunday.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pnSunday.Controls.Add(Me.Label7)
        Me.pnSunday.Controls.Add(Me.dtopensunday)
        Me.pnSunday.Controls.Add(Me.Label8)
        Me.pnSunday.Controls.Add(Me.dtclosesunday)
        Me.pnSunday.Enabled = False
        Me.pnSunday.Location = New System.Drawing.Point(90, 241)
        Me.pnSunday.Name = "pnSunday"
        Me.pnSunday.Size = New System.Drawing.Size(251, 33)
        Me.pnSunday.TabIndex = 15
        '
        'Label7
        '
        Me.Label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        '
        '
        '
        Me.Label7.BackgroundStyle.Class = ""
        Me.Label7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label7.Location = New System.Drawing.Point(3, 3)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(29, 15)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Open"
        '
        'dtopensunday
        '
        Me.dtopensunday.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtopensunday.Location = New System.Drawing.Point(38, 3)
        Me.dtopensunday.Name = "dtopensunday"
        Me.dtopensunday.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.dtopensunday.ShowUpDown = True
        Me.dtopensunday.Size = New System.Drawing.Size(79, 20)
        Me.dtopensunday.TabIndex = 0
        '
        'Label8
        '
        Me.Label8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        '
        '
        '
        Me.Label8.BackgroundStyle.Class = ""
        Me.Label8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label8.Location = New System.Drawing.Point(123, 3)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(31, 15)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Close"
        '
        'dtclosesunday
        '
        Me.dtclosesunday.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtclosesunday.Location = New System.Drawing.Point(160, 3)
        Me.dtclosesunday.Name = "dtclosesunday"
        Me.dtclosesunday.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.dtclosesunday.ShowUpDown = True
        Me.dtclosesunday.Size = New System.Drawing.Size(79, 20)
        Me.dtclosesunday.TabIndex = 1
        '
        'pnFriday
        '
        Me.pnFriday.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pnFriday.Controls.Add(Me.Label9)
        Me.pnFriday.Controls.Add(Me.dtopenfriday)
        Me.pnFriday.Controls.Add(Me.Label10)
        Me.pnFriday.Controls.Add(Me.dtclosefriday)
        Me.pnFriday.Enabled = False
        Me.pnFriday.Location = New System.Drawing.Point(90, 173)
        Me.pnFriday.Name = "pnFriday"
        Me.pnFriday.Size = New System.Drawing.Size(251, 28)
        Me.pnFriday.TabIndex = 11
        '
        'Label9
        '
        Me.Label9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        '
        '
        '
        Me.Label9.BackgroundStyle.Class = ""
        Me.Label9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label9.Location = New System.Drawing.Point(3, 3)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(29, 15)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Open"
        '
        'dtopenfriday
        '
        Me.dtopenfriday.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtopenfriday.Location = New System.Drawing.Point(38, 3)
        Me.dtopenfriday.Name = "dtopenfriday"
        Me.dtopenfriday.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.dtopenfriday.ShowUpDown = True
        Me.dtopenfriday.Size = New System.Drawing.Size(79, 20)
        Me.dtopenfriday.TabIndex = 0
        '
        'Label10
        '
        Me.Label10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        '
        '
        '
        Me.Label10.BackgroundStyle.Class = ""
        Me.Label10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label10.Location = New System.Drawing.Point(123, 3)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(31, 15)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "Close"
        '
        'dtclosefriday
        '
        Me.dtclosefriday.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtclosefriday.Location = New System.Drawing.Point(160, 3)
        Me.dtclosefriday.Name = "dtclosefriday"
        Me.dtclosefriday.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.dtclosefriday.ShowUpDown = True
        Me.dtclosefriday.Size = New System.Drawing.Size(79, 20)
        Me.dtclosefriday.TabIndex = 1
        '
        'pnSaturday
        '
        Me.pnSaturday.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pnSaturday.Controls.Add(Me.Label5)
        Me.pnSaturday.Controls.Add(Me.dtopensaturday)
        Me.pnSaturday.Controls.Add(Me.Label6)
        Me.pnSaturday.Controls.Add(Me.dtclosesaturday)
        Me.pnSaturday.Enabled = False
        Me.pnSaturday.Location = New System.Drawing.Point(90, 207)
        Me.pnSaturday.Name = "pnSaturday"
        Me.pnSaturday.Size = New System.Drawing.Size(251, 28)
        Me.pnSaturday.TabIndex = 13
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        '
        '
        '
        Me.Label5.BackgroundStyle.Class = ""
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.Location = New System.Drawing.Point(3, 3)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(29, 15)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Open"
        '
        'dtopensaturday
        '
        Me.dtopensaturday.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtopensaturday.Location = New System.Drawing.Point(38, 3)
        Me.dtopensaturday.Name = "dtopensaturday"
        Me.dtopensaturday.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.dtopensaturday.ShowUpDown = True
        Me.dtopensaturday.Size = New System.Drawing.Size(79, 20)
        Me.dtopensaturday.TabIndex = 0
        '
        'Label6
        '
        Me.Label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        '
        '
        '
        Me.Label6.BackgroundStyle.Class = ""
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.Location = New System.Drawing.Point(123, 3)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(31, 15)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Close"
        '
        'dtclosesaturday
        '
        Me.dtclosesaturday.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtclosesaturday.Location = New System.Drawing.Point(160, 3)
        Me.dtclosesaturday.Name = "dtclosesaturday"
        Me.dtclosesaturday.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.dtclosesaturday.ShowUpDown = True
        Me.dtclosesaturday.Size = New System.Drawing.Size(79, 20)
        Me.dtclosesaturday.TabIndex = 1
        '
        'pnWednesday
        '
        Me.pnWednesday.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pnWednesday.Controls.Add(Me.Label11)
        Me.pnWednesday.Controls.Add(Me.dtopenwednesday)
        Me.pnWednesday.Controls.Add(Me.Label12)
        Me.pnWednesday.Controls.Add(Me.dtclosewednesday)
        Me.pnWednesday.Enabled = False
        Me.pnWednesday.Location = New System.Drawing.Point(90, 105)
        Me.pnWednesday.Name = "pnWednesday"
        Me.pnWednesday.Size = New System.Drawing.Size(251, 28)
        Me.pnWednesday.TabIndex = 7
        '
        'Label11
        '
        Me.Label11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        '
        '
        '
        Me.Label11.BackgroundStyle.Class = ""
        Me.Label11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label11.Location = New System.Drawing.Point(3, 3)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(29, 15)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Open"
        '
        'dtopenwednesday
        '
        Me.dtopenwednesday.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtopenwednesday.Location = New System.Drawing.Point(38, 3)
        Me.dtopenwednesday.Name = "dtopenwednesday"
        Me.dtopenwednesday.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.dtopenwednesday.ShowUpDown = True
        Me.dtopenwednesday.Size = New System.Drawing.Size(79, 20)
        Me.dtopenwednesday.TabIndex = 0
        '
        'Label12
        '
        Me.Label12.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        '
        '
        '
        Me.Label12.BackgroundStyle.Class = ""
        Me.Label12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label12.Location = New System.Drawing.Point(123, 3)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(31, 15)
        Me.Label12.TabIndex = 2
        Me.Label12.Text = "Close"
        '
        'dtclosewednesday
        '
        Me.dtclosewednesday.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtclosewednesday.Location = New System.Drawing.Point(160, 3)
        Me.dtclosewednesday.Name = "dtclosewednesday"
        Me.dtclosewednesday.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.dtclosewednesday.ShowUpDown = True
        Me.dtclosewednesday.Size = New System.Drawing.Size(79, 20)
        Me.dtclosewednesday.TabIndex = 1
        '
        'pnMonday
        '
        Me.pnMonday.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pnMonday.Controls.Add(Me.Label13)
        Me.pnMonday.Controls.Add(Me.dtopenmonday)
        Me.pnMonday.Controls.Add(Me.Label14)
        Me.pnMonday.Controls.Add(Me.dtclosemonday)
        Me.pnMonday.Enabled = False
        Me.pnMonday.Location = New System.Drawing.Point(90, 37)
        Me.pnMonday.Name = "pnMonday"
        Me.pnMonday.Size = New System.Drawing.Size(251, 28)
        Me.pnMonday.TabIndex = 3
        '
        'Label13
        '
        Me.Label13.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        '
        '
        '
        Me.Label13.BackgroundStyle.Class = ""
        Me.Label13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label13.Location = New System.Drawing.Point(3, 3)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(29, 15)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "Open"
        '
        'dtopenmonday
        '
        Me.dtopenmonday.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtopenmonday.Location = New System.Drawing.Point(38, 3)
        Me.dtopenmonday.Name = "dtopenmonday"
        Me.dtopenmonday.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.dtopenmonday.ShowUpDown = True
        Me.dtopenmonday.Size = New System.Drawing.Size(79, 20)
        Me.dtopenmonday.TabIndex = 0
        '
        'Label14
        '
        Me.Label14.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label14.AutoSize = True
        '
        '
        '
        Me.Label14.BackgroundStyle.Class = ""
        Me.Label14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label14.Location = New System.Drawing.Point(123, 3)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(31, 15)
        Me.Label14.TabIndex = 2
        Me.Label14.Text = "Close"
        '
        'dtclosemonday
        '
        Me.dtclosemonday.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtclosemonday.Location = New System.Drawing.Point(160, 3)
        Me.dtclosemonday.Name = "dtclosemonday"
        Me.dtclosemonday.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.dtclosemonday.ShowUpDown = True
        Me.dtclosemonday.Size = New System.Drawing.Size(79, 20)
        Me.dtclosemonday.TabIndex = 1
        '
        'pnThursday
        '
        Me.pnThursday.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pnThursday.Controls.Add(Me.Label1)
        Me.pnThursday.Controls.Add(Me.dtopenthursday)
        Me.pnThursday.Controls.Add(Me.Label2)
        Me.pnThursday.Controls.Add(Me.dtclosethursday)
        Me.pnThursday.Enabled = False
        Me.pnThursday.Location = New System.Drawing.Point(90, 139)
        Me.pnThursday.Name = "pnThursday"
        Me.pnThursday.Size = New System.Drawing.Size(251, 28)
        Me.pnThursday.TabIndex = 9
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(29, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Open"
        '
        'dtopenthursday
        '
        Me.dtopenthursday.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtopenthursday.Location = New System.Drawing.Point(38, 3)
        Me.dtopenthursday.Name = "dtopenthursday"
        Me.dtopenthursday.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.dtopenthursday.ShowUpDown = True
        Me.dtopenthursday.Size = New System.Drawing.Size(79, 20)
        Me.dtopenthursday.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.Location = New System.Drawing.Point(123, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(31, 15)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Close"
        '
        'dtclosethursday
        '
        Me.dtclosethursday.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtclosethursday.Location = New System.Drawing.Point(160, 3)
        Me.dtclosethursday.Name = "dtclosethursday"
        Me.dtclosethursday.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.dtclosethursday.ShowUpDown = True
        Me.dtclosethursday.Size = New System.Drawing.Size(79, 20)
        Me.dtclosethursday.TabIndex = 1
        '
        'chkWednesday
        '
        Me.chkWednesday.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkWednesday.AutoSize = True
        '
        '
        '
        Me.chkWednesday.BackgroundStyle.Class = ""
        Me.chkWednesday.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkWednesday.Location = New System.Drawing.Point(3, 105)
        Me.chkWednesday.Name = "chkWednesday"
        Me.chkWednesday.Size = New System.Drawing.Size(81, 15)
        Me.chkWednesday.TabIndex = 6
        Me.chkWednesday.Tag = "2"
        Me.chkWednesday.Text = "Wednesday"
        '
        'pnTuesday
        '
        Me.pnTuesday.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pnTuesday.Controls.Add(Me.Label3)
        Me.pnTuesday.Controls.Add(Me.dtopentuesday)
        Me.pnTuesday.Controls.Add(Me.Label4)
        Me.pnTuesday.Controls.Add(Me.dtclosetuesday)
        Me.pnTuesday.Enabled = False
        Me.pnTuesday.Location = New System.Drawing.Point(90, 71)
        Me.pnTuesday.Name = "pnTuesday"
        Me.pnTuesday.Size = New System.Drawing.Size(251, 28)
        Me.pnTuesday.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        '
        '
        '
        Me.Label3.BackgroundStyle.Class = ""
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.Location = New System.Drawing.Point(3, 3)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 15)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Open"
        '
        'dtopentuesday
        '
        Me.dtopentuesday.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtopentuesday.Location = New System.Drawing.Point(38, 3)
        Me.dtopentuesday.Name = "dtopentuesday"
        Me.dtopentuesday.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.dtopentuesday.ShowUpDown = True
        Me.dtopentuesday.Size = New System.Drawing.Size(79, 20)
        Me.dtopentuesday.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        '
        '
        '
        Me.Label4.BackgroundStyle.Class = ""
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.Location = New System.Drawing.Point(123, 3)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(31, 15)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Close"
        '
        'dtclosetuesday
        '
        Me.dtclosetuesday.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtclosetuesday.Location = New System.Drawing.Point(160, 3)
        Me.dtclosetuesday.Name = "dtclosetuesday"
        Me.dtclosetuesday.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.dtclosetuesday.ShowUpDown = True
        Me.dtclosetuesday.Size = New System.Drawing.Size(79, 20)
        Me.dtclosetuesday.TabIndex = 1
        '
        'chkThursday
        '
        Me.chkThursday.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkThursday.AutoSize = True
        '
        '
        '
        Me.chkThursday.BackgroundStyle.Class = ""
        Me.chkThursday.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkThursday.Location = New System.Drawing.Point(3, 139)
        Me.chkThursday.Name = "chkThursday"
        Me.chkThursday.Size = New System.Drawing.Size(68, 15)
        Me.chkThursday.TabIndex = 8
        Me.chkThursday.Tag = "3"
        Me.chkThursday.Text = "Thursday"
        '
        'chkFriday
        '
        Me.chkFriday.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkFriday.AutoSize = True
        '
        '
        '
        Me.chkFriday.BackgroundStyle.Class = ""
        Me.chkFriday.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkFriday.Location = New System.Drawing.Point(3, 173)
        Me.chkFriday.Name = "chkFriday"
        Me.chkFriday.Size = New System.Drawing.Size(53, 15)
        Me.chkFriday.TabIndex = 10
        Me.chkFriday.Tag = "4"
        Me.chkFriday.Text = "Friday"
        '
        'chkSaturday
        '
        Me.chkSaturday.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkSaturday.AutoSize = True
        '
        '
        '
        Me.chkSaturday.BackgroundStyle.Class = ""
        Me.chkSaturday.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkSaturday.Location = New System.Drawing.Point(3, 207)
        Me.chkSaturday.Name = "chkSaturday"
        Me.chkSaturday.Size = New System.Drawing.Size(67, 15)
        Me.chkSaturday.TabIndex = 12
        Me.chkSaturday.Tag = "5"
        Me.chkSaturday.Text = "Saturday"
        '
        'chkSunday
        '
        Me.chkSunday.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkSunday.AutoSize = True
        '
        '
        '
        Me.chkSunday.BackgroundStyle.Class = ""
        Me.chkSunday.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkSunday.Location = New System.Drawing.Point(3, 241)
        Me.chkSunday.Name = "chkSunday"
        Me.chkSunday.Size = New System.Drawing.Size(60, 15)
        Me.chkSunday.TabIndex = 14
        Me.chkSunday.Tag = "6"
        Me.chkSunday.Text = "Sunday"
        '
        'chkAll
        '
        Me.chkAll.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkAll.AutoSize = True
        '
        '
        '
        Me.chkAll.BackgroundStyle.Class = ""
        Me.chkAll.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAll.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAll.Location = New System.Drawing.Point(3, 3)
        Me.chkAll.Name = "chkAll"
        Me.chkAll.Size = New System.Drawing.Size(35, 15)
        Me.chkAll.TabIndex = 0
        Me.chkAll.Text = "All"
        '
        'pnAll
        '
        Me.pnAll.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pnAll.Controls.Add(Me.Label15)
        Me.pnAll.Controls.Add(Me.dtopenall)
        Me.pnAll.Controls.Add(Me.Label16)
        Me.pnAll.Controls.Add(Me.dtcloseall)
        Me.pnAll.Location = New System.Drawing.Point(90, 3)
        Me.pnAll.Name = "pnAll"
        Me.pnAll.Size = New System.Drawing.Size(251, 28)
        Me.pnAll.TabIndex = 1
        '
        'Label15
        '
        Me.Label15.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label15.AutoSize = True
        '
        '
        '
        Me.Label15.BackgroundStyle.Class = ""
        Me.Label15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(3, 3)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(29, 15)
        Me.Label15.TabIndex = 0
        Me.Label15.Text = "Open"
        '
        'dtopenall
        '
        Me.dtopenall.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtopenall.Location = New System.Drawing.Point(38, 3)
        Me.dtopenall.Name = "dtopenall"
        Me.dtopenall.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.dtopenall.ShowUpDown = True
        Me.dtopenall.Size = New System.Drawing.Size(79, 20)
        Me.dtopenall.TabIndex = 0
        '
        'Label16
        '
        Me.Label16.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label16.AutoSize = True
        '
        '
        '
        Me.Label16.BackgroundStyle.Class = ""
        Me.Label16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(123, 3)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(31, 15)
        Me.Label16.TabIndex = 2
        Me.Label16.Text = "Close"
        '
        'dtcloseall
        '
        Me.dtcloseall.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtcloseall.Location = New System.Drawing.Point(160, 3)
        Me.dtcloseall.Name = "dtcloseall"
        Me.dtcloseall.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.dtcloseall.ShowUpDown = True
        Me.dtcloseall.Size = New System.Drawing.Size(79, 20)
        Me.dtcloseall.TabIndex = 1
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Label17)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtName)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(350, 29)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'Label17
        '
        Me.Label17.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label17.BackgroundStyle.Class = ""
        Me.Label17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label17.Location = New System.Drawing.Point(3, 3)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(85, 23)
        Me.Label17.TabIndex = 2
        Me.Label17.Text = "Name"
        '
        'txtName
        '
        Me.txtName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.Location = New System.Drawing.Point(94, 3)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(235, 20)
        Me.txtName.TabIndex = 0
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(193, 329)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(72, 23)
        Me.cmdOK.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdOK.TabIndex = 2
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(273, 329)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(72, 23)
        Me.cmdCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdCancel.TabIndex = 3
        Me.cmdCancel.Text = "&Cancel"
        '
        'DividerLabel1
        '
        Me.DividerLabel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(0, 29)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(350, 10)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 6
        '
        'DividerLabel2
        '
        Me.DividerLabel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.DividerLabel2.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel2.Location = New System.Drawing.Point(0, 316)
        Me.DividerLabel2.Name = "DividerLabel2"
        Me.DividerLabel2.Size = New System.Drawing.Size(350, 10)
        Me.DividerLabel2.Spacing = 0
        Me.DividerLabel2.TabIndex = 7
        '
        'frmOperationalHours
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(350, 356)
        Me.ControlBox = False
        Me.Controls.Add(Me.DividerLabel2)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.DividerLabel1)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmOperationalHours"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Configure Operational Hours"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.pnSunday.ResumeLayout(False)
        Me.pnSunday.PerformLayout()
        Me.pnFriday.ResumeLayout(False)
        Me.pnFriday.PerformLayout()
        Me.pnSaturday.ResumeLayout(False)
        Me.pnSaturday.PerformLayout()
        Me.pnWednesday.ResumeLayout(False)
        Me.pnWednesday.PerformLayout()
        Me.pnMonday.ResumeLayout(False)
        Me.pnMonday.PerformLayout()
        Me.pnThursday.ResumeLayout(False)
        Me.pnThursday.PerformLayout()
        Me.pnTuesday.ResumeLayout(False)
        Me.pnTuesday.PerformLayout()
        Me.pnAll.ResumeLayout(False)
        Me.pnAll.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents chkMonday As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkTuesday As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents chkWednesday As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkThursday As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkFriday As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkSaturday As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkSunday As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents pnThursday As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtopenthursday As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtclosethursday As System.Windows.Forms.DateTimePicker
    Friend WithEvents pnTuesday As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtopentuesday As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtclosetuesday As System.Windows.Forms.DateTimePicker
    Friend WithEvents pnSaturday As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtopensaturday As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtclosesaturday As System.Windows.Forms.DateTimePicker
    Friend WithEvents pnSunday As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtopensunday As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtclosesunday As System.Windows.Forms.DateTimePicker
    Friend WithEvents pnFriday As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtopenfriday As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtclosefriday As System.Windows.Forms.DateTimePicker
    Friend WithEvents pnWednesday As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtopenwednesday As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtclosewednesday As System.Windows.Forms.DateTimePicker
    Friend WithEvents pnMonday As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label13 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtopenmonday As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label14 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtclosemonday As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkAll As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents pnAll As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label15 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtopenall As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label16 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtcloseall As System.Windows.Forms.DateTimePicker
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label17 As DevComponents.DotNetBar.LabelX
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents DividerLabel2 As sqlrd.DividerLabel
End Class
