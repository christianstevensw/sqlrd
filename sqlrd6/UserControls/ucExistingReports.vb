Friend Class ucExistingReports

    Dim eventID As Integer = 99999
    Public parentID As Integer = 0
    Friend imgList As ImageList

    Public Property m_eventID() As Integer
        Get
            Return eventID
        End Get
        Set(ByVal value As Integer)
            eventID = value
        End Set
    End Property

    'Private Sub InitImages()
    '    imgList = New ImageList()

    '    Dim sz As Size = New Size(16, 16)

    '    imgList.ImageSize = sz
    '    imgList.ColorDepth = ColorDepth.Depth32Bit
    '    imgList.TransparentColor = Color.Transparent

    '    With imgList.Images
    '        .Add(My.Resources.home) '0
    '        .Add(My.Resources.folder_closed) '1
    '        .Add(My.Resources.folder) '2
    '        .Add(My.Resources.box_closed) '3
    '        .Add(My.Resources.box) '4
    '        .Add(My.Resources.folder_cubes) '5
    '        .Add(My.Resources.document_chart) '6
    '        .Add(My.Resources.document_gear) '7
    '        .Add(My.Resources.folder_closed1) '8
    '        .Add(My.Resources.box_closed_bw) '9
    '        .Add(My.Resources.box_bw) '10
    '        .Add(My.Resources.document_pulse) '11
    '        .Add(My.Resources.document_attachment) '12
    '        .Add(My.Resources.document_atoms) '13
    '        .Add(My.Resources.box_new) '14
    '        .Add(My.Resources.box_new_bw) '15
    '    End With

    'End Sub
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If tvSchedules.SelectedNode Is Nothing Then Return

        Dim oNode As DevComponents.AdvTree.Node
        Dim oItem As DevComponents.AdvTree.Node
        Dim sType As String
        Dim ID As Integer

        oNode = tvSchedules.SelectedNode

        sType = crdxCommon.getScheduleTypeString(oNode.Tag)

        ID = oNode.DataKey '   sType.Split(":")(1)
        'sType = sType.Split(":")(0).ToLower

        If sType = "folder" Or sType = "desktop" Or sType = "smartfolder" Then Return

        For Each oItem In lsvSchedules.Nodes
            If oItem.Text = oNode.Text And oItem.Tag = sType Or ID = parentID Then
                Return
            End If
        Next

        oItem = New DevComponents.AdvTree.Node

        oItem.Text = oNode.Text
        oItem.Tag = sType
        oItem.DataKey = oNode.DataKey
        oItem.Image = oNode.Image

        lsvSchedules.Nodes.Add(oItem)
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        If lsvSchedules.SelectedNodes.Count = 0 Then Exit Sub

        Dim items As ArrayList = New ArrayList

        For Each oItem As DevComponents.AdvTree.Node In lsvSchedules.SelectedNodes
            clsMarsData.WriteData("DELETE FROM EventSubAttr WHERE ReportID=" & oItem.DataKey, False)
            items.Add(oItem)
        Next

        For Each oitem As DevComponents.AdvTree.Node In items
            oitem.Remove()
        Next
    End Sub

    Public Sub LoadSelectedReports()
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim nImage As Integer = 6


        SQL = "SELECT * FROM EventSchedule WHERE EventID =" & eventID & " ORDER BY ordernumber"

        oRs = clsMarsData.GetData(SQL)

        Dim ColName As String
        Dim TableName As String
        Dim KeyCol As String

        lsvSchedules.Nodes.Clear()

        Dim sz As Size = New Size(20, 20)

        Do While oRs.EOF = False
            Select Case oRs("scheduletype").Value.ToLower
                Case "report"
                    ColName = "ReportTitle"
                    TableName = "ReportAttr"
                    KeyCol = "ReportID"
                    nImage = 6
                Case "package"
                    ColName = "PackageName"
                    TableName = "PackageAttr"
                    KeyCol = "PackID"
                    nImage = 3
                Case "automation"
                    ColName = "AutoName"
                    TableName = "AutomationAttr"
                    KeyCol = "AutoID"
                    nImage = 7
            End Select

            SQL = "SELECT " & ColName & "," & KeyCol & " FROM " & _
            TableName & " WHERE " & _
            KeyCol & "=" & oRs("scheduleid").Value

            Dim rsP As ADODB.Recordset

            rsP = clsMarsData.GetData(SQL)

            If rsP.EOF = False Then

                Dim oItem As New DevComponents.AdvTree.Node

                oItem.Text = rsP.Fields(0).Value
                oItem.Tag = oRs("scheduletype").Value
                oItem.DataKey = oRs("scheduleid").Value

                Select Case oRs("scheduletype").Value
                    Case "report"
                        Dim rpt As cssreport = New cssreport(oItem.DataKey)
                        oItem.Image = resizeImage(rpt.reportImage, sz)
                    Case "package"
                        Dim pack As Package = New Package(oItem.DataKey)
                        oItem.Image = resizeImage(pack.packageImage, sz)
                    Case "automation"
                        Dim auto As Automation = New Automation(oItem.DataKey)
                        oItem.Image = resizeImage(auto.automationImage, sz)
                    Case "event-based"
                        Dim evt As EventBased = New EventBased(oItem.DataKey)
                        oItem.Image = resizeImage(evt.eventImage, sz)
                    Case "event-based package", "event-package"
                        Dim evp As EventBasedPackage = New EventBasedPackage(oItem.DataKey)
                        oItem.Image = resizeImage(evp.packageImage, sz)
                End Select


                Me.lsvSchedules.Nodes.Add(oItem)

            End If

            rsP.Close()
            oRs.MoveNext()
        Loop
    End Sub

    Private Sub ucExistingReports_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub mnuParameter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuParameter.Click
        Dim sType As String
        Dim nID As Integer

        If lsvSchedules.SelectedNodes.Count = 0 Then Return

        Dim oItem As DevComponents.AdvTree.Node

        oItem = lsvSchedules.SelectedNodes(0)

        sType = crdxCommon.getScheduleTypeString(oItem.Tag)
        nID = oItem.DataKey

        If sType.ToLower <> "report" Then Return

        Dim oSub As frmEventSub = New frmEventSub
        oSub.m_eventID = eventID
        oSub.SetSubstitution(nID, eventID)
    End Sub
    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub btnUp_Click(sender As Object, e As EventArgs) Handles btnUp.Click
        If lsvSchedules.SelectedNodes.Count = 0 Then Return

        Dim eventid As Integer = m_eventID
        Dim scheduleid As Integer = lsvSchedules.SelectedNode.DataKey

        eventid = 99999

        If eventid = 99999 Then
            MoveItem(lsvSchedules.SelectedNode, scheduleDirection.Up)
        Else
            MoveSchedule(scheduleid, scheduleDirection.Up, eventid)
            LoadSelectedReports()
        End If




        For Each item As DevComponents.AdvTree.Node In lsvSchedules.Nodes
            If item.DataKey = scheduleid Then
                lsvSchedules.SelectedNode = item
                item.EnsureVisible()
            End If
        Next
    End Sub

    Private Sub ButtonX1_Click(sender As Object, e As EventArgs) Handles btnDown.Click
        If lsvSchedules.SelectedNodes.Count = 0 Then Return

        Dim eventid As Integer = m_eventID
        Dim scheduleid As Integer = lsvSchedules.SelectedNode.DataKey

        eventid = 99999

        If eventid = 99999 Then
            MoveItem(lsvSchedules.SelectedNode, scheduleDirection.Down)
        Else
            MoveSchedule(scheduleid, scheduleDirection.Down, eventid)
            LoadSelectedReports()
        End If

        For Each item As DevComponents.AdvTree.Node In lsvSchedules.Nodes
            If item.DataKey = scheduleid Then
                lsvSchedules.SelectedNode = item
                item.EnsureVisible()
            End If
        Next
    End Sub

    Sub MoveItem(nn As DevComponents.AdvTree.Node, direction As scheduleDirection)
        If direction = scheduleDirection.Up Then
            If nn.Index = 0 Then Return

            Dim prevNode As DevComponents.AdvTree.Node = nn.PrevNode
            Dim sd As System.Collections.Generic.SortedDictionary(Of Integer, DevComponents.AdvTree.Node) = New System.Collections.Generic.SortedDictionary(Of Integer, DevComponents.AdvTree.Node)
            Dim I As Integer = 0

            If prevNode.PrevNode IsNot Nothing Then
                For I = 0 To prevNode.PrevNode.Index
                    sd.Add(I, lsvSchedules.Nodes(I))
                Next
            End If

            sd.Add(I, nn)

            I += 1

            sd.Add(I, prevNode)

            For I = nn.Index + 1 To lsvSchedules.Nodes.Count - 1
                sd.Add(I, lsvSchedules.Nodes(I))
            Next

            lsvSchedules.BeginUpdate()
            lsvSchedules.Nodes.Clear()

            For Each key As Integer In sd.Keys
                Dim nx As DevComponents.AdvTree.Node = sd.Item(key)

                lsvSchedules.Nodes.Add(nx)
            Next
            lsvSchedules.EndUpdate()
        Else
            If nn.Index = lsvSchedules.Nodes.Count - 1 Then Return

            Dim prevNode As DevComponents.AdvTree.Node = nn.PrevNode
            Dim sd As System.Collections.Generic.SortedDictionary(Of Integer, DevComponents.AdvTree.Node) = New System.Collections.Generic.SortedDictionary(Of Integer, DevComponents.AdvTree.Node)
            Dim I As Integer = 0

            If prevNode IsNot Nothing Then
                For I = 0 To prevNode.Index
                    sd.Add(I, lsvSchedules.Nodes(I))
                Next
            End If

            sd.Add(I, nn.NextNode)

            I += 1

            sd.Add(I, nn)

            For I = nn.NextNode.Index + 1 To lsvSchedules.Nodes.Count - 1
                sd.Add(I, lsvSchedules.Nodes(I))
            Next

            lsvSchedules.BeginUpdate()
            lsvSchedules.Nodes.Clear()

            For Each key As Integer In sd.Keys
                Dim nx As DevComponents.AdvTree.Node = sd.Item(key)

                lsvSchedules.Nodes.Add(nx)
            Next
            lsvSchedules.EndUpdate()
        End If
    End Sub
    Public Enum scheduleDirection
        Up = 1
        Down = 2
    End Enum

    Private Sub MoveSchedule(ByVal ScheduleID As Integer, ByVal oDirection As scheduleDirection, eventID As Integer)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim nLoc As Integer

        If lsvSchedules.Nodes.Count <= 1 Then Return

        SQL = "SELECT ordernumber FROM eventschedule WHERE scheduleid = " & ScheduleID & " AND eventid = " & eventID

        oRs = clsMarsData.GetData(SQL)

        nLoc = IsNull(oRs("ordernumber").Value, 1)

        Select Case oDirection
            Case scheduleDirection.Up
                If nLoc = 1 Then Exit Sub

                clsMarsData.WriteData("UPDATE eventschedule SET ordernumber = 99999 WHERE ordernumber = " & nLoc - 1 & " AND eventid = " & eventID)

                clsMarsData.WriteData("UPDATE eventschedule SET ordernumber = " & nLoc - 1 & " WHERE scheduleid = " & ScheduleID)

                clsMarsData.WriteData("UPDATE eventschedule SET ordernumber = " & nLoc & " WHERE ordernumber = 99999")
            Case scheduleDirection.Down
                If nLoc = GetMaxOrderID(eventID) - 1 Then Exit Sub

                clsMarsData.WriteData("UPDATE eventschedule SET ordernumber = 99999 WHERE ordernumber = " & nLoc + 1 & " AND eventid = " & eventID)

                clsMarsData.WriteData("UPDATE eventschedule SET ordernumber = " & nLoc + 1 & " WHERE scheduleid = " & ScheduleID)

                clsMarsData.WriteData("UPDATE eventschedule SET ordernumber = " & nLoc & " WHERE ordernumber = 99999")
        End Select
    End Sub

    Public Function GetMaxOrderID(ByVal eventid As Integer) As Integer
        Dim SQL As String
        Dim nOrderID As Integer

        SQL = "SELECT MAX(ordernumber) FROM eventschedule WHERE eventid =" & eventid

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                If IsNull(oRs.Fields(0).Value) <> "" Then
                    nOrderID = IsNull(oRs.Fields(0).Value, 1)
                Else
                    nOrderID = 0
                End If
            Else
                nOrderID = 0
            End If
        Else
            nOrderID = 0
        End If

        nOrderID += 1

        Return nOrderID
    End Function
End Class
