Imports sqlrd.clsMarsUI


Friend Class ucReports

    Dim MasterID As Integer = 99999

    Public Property m_MasterID() As Integer
        Get
            Return MasterID
        End Get
        Set(ByVal value As Integer)
            MasterID = value
        End Set
    End Property

    Private m_eventBased As Boolean = False
    Public Property eventBased() As Boolean
        Get
            Return m_eventBased
        End Get
        Set(ByVal value As Boolean)
            m_eventBased = value
        End Set
    End Property

    Public Sub LoadReports()
        Dim SQL As String
        Dim nPackID As Integer = Me.m_MasterID
        Dim oRs As ADODB.Recordset

        SQL = "SELECT * FROM " & _
            "ReportAttr WHERE PackID =" & nPackID

        SQL &= " ORDER BY PackOrderID"

        oRs = clsMarsData.GetData(SQL)


        Dim reportNode As DevComponents.AdvTree.Node

        If Not oRs Is Nothing Then

            tvReports.Nodes.Clear()

            Do While oRs.EOF = False
                reportNode = New DevComponents.AdvTree.Node(CType(oRs("reporttitle").Value, String))
                reportNode.Image = resizeImage(My.Resources.document_chart2, New Size(New Point(24, 24)))
                reportNode.Tag = oRs("reportid").Value
                reportNode.DataKey = oRs("reportid").Value
                reportNode.CheckBoxVisible = True
                reportNode.Checked = New cssreport(oRs("reportid").Value).packedStatus
                tvReports.Nodes.Add(reportNode)

                oRs.MoveNext()
            Loop
        End If

        oRs.Close()
    End Sub
    Private Sub cmdAddReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddReport.Click
        Dim sReturn(2) As String
        Dim oNewReport As frmPackedReport

        oNewReport = New frmPackedReport

        oNewReport.Text = "Report Properties"

        oNewReport.m_EventReport = True
        oNewReport.m_eventID = Me.m_MasterID

        sReturn = oNewReport.AddReport(tvReports.Nodes.Count + 1, Me.m_MasterID)

        If sReturn(0) = "" Then Exit Sub

        Me.LoadReports()

    End Sub

    Private Sub cmdEditReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEditReport.Click
        If tvReports.SelectedNodes.Count = 0 Then Return

        Dim oEdit As New frmPackedReport

        oEdit.Text = "Report Properties"

        Dim sVals() As String

        oEdit.m_EventReport = Me.m_eventBased

        Dim reportID As Integer = tvReports.SelectedNode.Tag

        sVals = oEdit.EditReport(reportID)

        If Not sVals Is Nothing Then
            Dim oItem As DevComponents.AdvTree.Node = tvReports.SelectedNode

            oItem.Text = sVals(0)

            'oItem.SubItems(1).Text = sVals(1)

            tvReports.Refresh()
        End If
    End Sub

    Private Sub cmdRemoveReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveReport.Click
        If tvReports.SelectedNodes.Count = 0 Then Return


        Dim oData As clsMarsData = New clsMarsData

        Dim nReportID As Integer

        Dim lsv As DevComponents.AdvTree.Node = tvReports.SelectedNode

        nReportID = lsv.Tag

        Dim oReport As New clsMarsReport

        clsMarsUI.MainUI.DeleteReport(nReportID)

        lsv.Remove()
    End Sub

    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        If tvReports.SelectedNodes.Count = 0 Then Return

        If tvReports.SelectedNode.Index = 0 Then Return

        Dim olsv As DevComponents.AdvTree.Node = tvReports.SelectedNode

        If olsv.Index = 0 Then Return

        Dim olsv2 As DevComponents.AdvTree.Node = tvReports.Nodes(olsv.Index - 1)

        Dim SQL As String

        Dim MoverID As Integer
        Dim MoverPos As Integer

        Dim MovedID As Integer
        Dim MovedPos As Integer

        MoverID = olsv.Tag
        MoverPos = olsv.Index

        MovedID = olsv2.Tag
        MovedPos = olsv2.Index

        Dim SelTag As String = olsv.Tag

        SQL = "UPDATE ReportAttr SET PackOrderID =" & MovedPos & " WHERE ReportID =" & MoverID

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE ReportAttr SET PackOrderID =" & MoverPos & " WHERE ReportID =" & MovedID

        clsMarsData.WriteData(SQL)

        Me.LoadReports()

        For Each olsv In tvReports.Nodes
            If olsv.Tag = SelTag Then
                tvReports.SelectedNode = olsv
                Exit For
            End If
        Next

    End Sub

    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        If tvReports.SelectedNodes.Count = 0 Then Return

        If tvReports.SelectedNode.Index = tvReports.Nodes.Count - 1 Then Return

        Dim olsv As DevComponents.AdvTree.Node = tvReports.SelectedNode

        Dim olsv2 As DevComponents.AdvTree.Node = tvReports.Nodes(olsv.Index + 1)

        Dim SQL As String

        Dim MoverID As Integer
        Dim MoverPos As Integer

        Dim MovedID As Integer
        Dim MovedPos As Integer

        MoverID = olsv.Tag
        MoverPos = olsv.Index

        MovedID = olsv2.Tag
        MovedPos = olsv2.Index

        Dim SelTag As String = olsv.Tag

        SQL = "UPDATE ReportAttr SET PackOrderID =" & MovedPos & " WHERE ReportID =" & MoverID

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE ReportAttr SET PackOrderID =" & MoverPos & " WHERE ReportID =" & MovedID

        clsMarsData.WriteData(SQL)

        Me.LoadReports()

        For Each olsv In tvReports.Nodes
            If olsv.Tag = SelTag Then
                tvReports.SelectedNode = olsv
                Exit For
            End If
        Next

    End Sub


    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub tvReports_AfterCheck(sender As Object, e As DevComponents.AdvTree.AdvTreeCellEventArgs) Handles tvReports.AfterCheck
        If tvReports.SelectedNode IsNot Nothing Then
            Dim reportid As Integer = tvReports.SelectedNode.DataKey
            Dim rpt As cssreport = New cssreport(reportid)

            rpt.packedStatus = tvReports.SelectedNode.Checked

            If tvReports.SelectedNode.Checked = False Then
                tvReports.SelectedNode.Image = MakeGrayscale3(tvReports.SelectedNode.Image)
            Else
                tvReports.SelectedNode.Image = resizeImage(rpt.reportImage, 24, 24)
            End If
        End If
    End Sub
End Class
