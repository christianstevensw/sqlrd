<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucFTPDetails
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnVerify = New DevComponents.DotNetBar.ButtonX()
        Me.btnBrowseFTP = New DevComponents.DotNetBar.ButtonX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.txtFTPServer = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtUserName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtDirectory = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmbFTPType = New System.Windows.Forms.ComboBox()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.txtPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.txtPort = New System.Windows.Forms.NumericUpDown()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.chkPassive = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkAscii = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.optPVK = New System.Windows.Forms.RadioButton()
        Me.grpCerts = New System.Windows.Forms.TableLayoutPanel()
        Me.btnBrowse2 = New DevComponents.DotNetBar.ButtonX()
        Me.Label7 = New DevComponents.DotNetBar.LabelX()
        Me.txtCertFile = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btnBrowse = New DevComponents.DotNetBar.ButtonX()
        Me.Label9 = New DevComponents.DotNetBar.LabelX()
        Me.txtCertPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label8 = New DevComponents.DotNetBar.LabelX()
        Me.txtCertName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label10 = New DevComponents.DotNetBar.LabelX()
        Me.txtPrivateKeyFile = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.optPFX = New System.Windows.Forms.RadioButton()
        Me.grpProxy = New System.Windows.Forms.TableLayoutPanel()
        Me.txtProxyPort = New System.Windows.Forms.NumericUpDown()
        Me.Label11 = New DevComponents.DotNetBar.LabelX()
        Me.Label12 = New DevComponents.DotNetBar.LabelX()
        Me.Label13 = New DevComponents.DotNetBar.LabelX()
        Me.Label14 = New DevComponents.DotNetBar.LabelX()
        Me.txtProxyServer = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtProxyUserName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtProxyPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.chkProxy = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.stabFTP = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.chkRetryUpload = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.tabGeneral = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.chkClearChannel = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.tabCertificate = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.tabProxy = New DevComponents.DotNetBar.SuperTabItem()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        CType(Me.txtPort, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpCerts.SuspendLayout()
        Me.grpProxy.SuspendLayout()
        CType(Me.txtProxyPort, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.stabFTP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabFTP.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.ColumnCount = 4
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.4026!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.02597!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.88312!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.42857!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel3, 3, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label6, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.txtFTPServer, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtUserName, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtDirectory, 1, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.cmbFTPType, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtPassword, 3, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label5, 2, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.txtPort, 3, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 1)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(7, 3)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 6
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(510, 115)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel3.Controls.Add(Me.btnVerify)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnBrowseFTP)
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(350, 84)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(132, 29)
        Me.FlowLayoutPanel3.TabIndex = 2
        '
        'btnVerify
        '
        Me.btnVerify.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnVerify.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVerify.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnVerify.Location = New System.Drawing.Point(3, 3)
        Me.btnVerify.Name = "btnVerify"
        Me.btnVerify.Size = New System.Drawing.Size(50, 18)
        Me.btnVerify.TabIndex = 0
        Me.btnVerify.Text = "&Verify"
        '
        'btnBrowseFTP
        '
        Me.btnBrowseFTP.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnBrowseFTP.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBrowseFTP.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnBrowseFTP.Enabled = False
        Me.btnBrowseFTP.Location = New System.Drawing.Point(59, 3)
        Me.btnBrowseFTP.Name = "btnBrowseFTP"
        Me.btnBrowseFTP.Size = New System.Drawing.Size(53, 18)
        Me.btnBrowseFTP.TabIndex = 1
        Me.btnBrowseFTP.Text = "&Browse"
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 21)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "FTP Server"
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.Location = New System.Drawing.Point(3, 57)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(82, 21)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "FTP Type"
        '
        'Label6
        '
        Me.Label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.Location = New System.Drawing.Point(3, 84)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(82, 21)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Remote Directory"
        '
        'txtFTPServer
        '
        Me.txtFTPServer.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtFTPServer.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtFTPServer.Border.Class = "TextBoxBorder"
        Me.txtFTPServer.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TableLayoutPanel1.SetColumnSpan(Me.txtFTPServer, 3)
        Me.txtFTPServer.ForeColor = System.Drawing.Color.Black
        Me.txtFTPServer.Location = New System.Drawing.Point(91, 3)
        Me.txtFTPServer.Name = "txtFTPServer"
        Me.txtFTPServer.Size = New System.Drawing.Size(416, 21)
        Me.txtFTPServer.TabIndex = 1
        '
        'txtUserName
        '
        Me.txtUserName.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtUserName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtUserName.Border.Class = "TextBoxBorder"
        Me.txtUserName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUserName.ForeColor = System.Drawing.Color.Black
        Me.txtUserName.Location = New System.Drawing.Point(91, 30)
        Me.txtUserName.Name = "txtUserName"
        Me.txtUserName.Size = New System.Drawing.Size(167, 21)
        Me.txtUserName.TabIndex = 3
        '
        'txtDirectory
        '
        Me.txtDirectory.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDirectory.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDirectory.Border.Class = "TextBoxBorder"
        Me.txtDirectory.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TableLayoutPanel1.SetColumnSpan(Me.txtDirectory, 2)
        Me.txtDirectory.ForeColor = System.Drawing.Color.Black
        Me.txtDirectory.Location = New System.Drawing.Point(91, 84)
        Me.txtDirectory.Name = "txtDirectory"
        Me.txtDirectory.Size = New System.Drawing.Size(253, 21)
        Me.txtDirectory.TabIndex = 5
        '
        'cmbFTPType
        '
        Me.cmbFTPType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFTPType.FormattingEnabled = True
        Me.cmbFTPType.Items.AddRange(New Object() {"FTP", "FTP - SSL 3.1 (TLS)", "FTP - SSL 3.0", "FTP - SSL 3.0 (Implicit)", "FTP - SSH (FTPS)"})
        Me.cmbFTPType.Location = New System.Drawing.Point(91, 57)
        Me.cmbFTPType.Name = "cmbFTPType"
        Me.cmbFTPType.Size = New System.Drawing.Size(125, 21)
        Me.cmbFTPType.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.Location = New System.Drawing.Point(264, 30)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 21)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Password"
        '
        'txtPassword
        '
        Me.txtPassword.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPassword.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtPassword.Border.Class = "TextBoxBorder"
        Me.txtPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPassword.ForeColor = System.Drawing.Color.Black
        Me.txtPassword.Location = New System.Drawing.Point(350, 30)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtPassword.Size = New System.Drawing.Size(157, 21)
        Me.txtPassword.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.Location = New System.Drawing.Point(264, 57)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 21)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Port"
        '
        'txtPort
        '
        Me.txtPort.Location = New System.Drawing.Point(350, 57)
        Me.txtPort.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.txtPort.Name = "txtPort"
        Me.txtPort.Size = New System.Drawing.Size(68, 21)
        Me.txtPort.TabIndex = 6
        Me.txtPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPort.Value = New Decimal(New Integer() {21, 0, 0, 0})
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.Location = New System.Drawing.Point(3, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(82, 21)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "User Name"
        '
        'chkPassive
        '
        Me.chkPassive.AutoSize = True
        Me.chkPassive.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkPassive.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkPassive.Checked = True
        Me.chkPassive.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPassive.CheckValue = "Y"
        Me.chkPassive.Location = New System.Drawing.Point(10, 124)
        Me.chkPassive.Name = "chkPassive"
        Me.chkPassive.Size = New System.Drawing.Size(88, 16)
        Me.chkPassive.TabIndex = 1
        Me.chkPassive.Text = "Passive Mode"
        '
        'chkAscii
        '
        Me.chkAscii.AutoSize = True
        Me.chkAscii.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkAscii.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAscii.Location = New System.Drawing.Point(104, 124)
        Me.chkAscii.Name = "chkAscii"
        Me.chkAscii.Size = New System.Drawing.Size(81, 16)
        Me.chkAscii.TabIndex = 2
        Me.chkAscii.Text = "ASCII mode"
        '
        'optPVK
        '
        Me.optPVK.AutoSize = True
        Me.optPVK.Location = New System.Drawing.Point(135, 3)
        Me.optPVK.Name = "optPVK"
        Me.optPVK.Size = New System.Drawing.Size(133, 17)
        Me.optPVK.TabIndex = 1
        Me.optPVK.TabStop = True
        Me.optPVK.Text = "Use Private Certificate"
        Me.optPVK.UseVisualStyleBackColor = True
        '
        'grpCerts
        '
        Me.grpCerts.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpCerts.BackColor = System.Drawing.Color.Transparent
        Me.grpCerts.ColumnCount = 3
        Me.grpCerts.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.47945!))
        Me.grpCerts.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 59.17809!))
        Me.grpCerts.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.34247!))
        Me.grpCerts.Controls.Add(Me.btnBrowse2, 2, 1)
        Me.grpCerts.Controls.Add(Me.Label7, 0, 0)
        Me.grpCerts.Controls.Add(Me.txtCertFile, 1, 0)
        Me.grpCerts.Controls.Add(Me.btnBrowse, 2, 0)
        Me.grpCerts.Controls.Add(Me.Label9, 0, 3)
        Me.grpCerts.Controls.Add(Me.txtCertPassword, 1, 3)
        Me.grpCerts.Controls.Add(Me.Label8, 0, 2)
        Me.grpCerts.Controls.Add(Me.txtCertName, 1, 2)
        Me.grpCerts.Controls.Add(Me.Label10, 0, 1)
        Me.grpCerts.Controls.Add(Me.txtPrivateKeyFile, 1, 1)
        Me.grpCerts.Location = New System.Drawing.Point(4, 31)
        Me.grpCerts.Name = "grpCerts"
        Me.grpCerts.RowCount = 4
        Me.grpCerts.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.grpCerts.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.grpCerts.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.grpCerts.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.grpCerts.Size = New System.Drawing.Size(513, 100)
        Me.grpCerts.TabIndex = 3
        '
        'btnBrowse2
        '
        Me.btnBrowse2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnBrowse2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBrowse2.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnBrowse2.Location = New System.Drawing.Point(473, 28)
        Me.btnBrowse2.Name = "btnBrowse2"
        Me.btnBrowse2.Size = New System.Drawing.Size(37, 19)
        Me.btnBrowse2.TabIndex = 1
        Me.btnBrowse2.Text = "..."
        '
        'Label7
        '
        Me.Label7.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label7.Location = New System.Drawing.Point(3, 3)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(124, 19)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Certificate File"
        '
        'txtCertFile
        '
        Me.txtCertFile.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCertFile.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtCertFile.Border.Class = "TextBoxBorder"
        Me.txtCertFile.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCertFile.ForeColor = System.Drawing.Color.Black
        Me.txtCertFile.Location = New System.Drawing.Point(133, 3)
        Me.txtCertFile.Name = "txtCertFile"
        Me.txtCertFile.Size = New System.Drawing.Size(297, 21)
        Me.txtCertFile.TabIndex = 0
        '
        'btnBrowse
        '
        Me.btnBrowse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnBrowse.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBrowse.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnBrowse.Location = New System.Drawing.Point(473, 3)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(37, 19)
        Me.btnBrowse.TabIndex = 0
        Me.btnBrowse.Text = "..."
        '
        'Label9
        '
        Me.Label9.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label9.Location = New System.Drawing.Point(3, 78)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(124, 19)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "Password"
        '
        'txtCertPassword
        '
        Me.txtCertPassword.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCertPassword.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtCertPassword.Border.Class = "TextBoxBorder"
        Me.txtCertPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.grpCerts.SetColumnSpan(Me.txtCertPassword, 2)
        Me.txtCertPassword.ForeColor = System.Drawing.Color.Black
        Me.txtCertPassword.Location = New System.Drawing.Point(133, 78)
        Me.txtCertPassword.Name = "txtCertPassword"
        Me.txtCertPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtCertPassword.Size = New System.Drawing.Size(377, 21)
        Me.txtCertPassword.TabIndex = 0
        '
        'Label8
        '
        Me.Label8.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label8.Location = New System.Drawing.Point(3, 53)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(124, 19)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Certificate Name"
        '
        'txtCertName
        '
        Me.txtCertName.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCertName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtCertName.Border.Class = "TextBoxBorder"
        Me.txtCertName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.grpCerts.SetColumnSpan(Me.txtCertName, 2)
        Me.txtCertName.ForeColor = System.Drawing.Color.Black
        Me.txtCertName.Location = New System.Drawing.Point(133, 53)
        Me.txtCertName.Name = "txtCertName"
        Me.txtCertName.Size = New System.Drawing.Size(377, 21)
        Me.txtCertName.TabIndex = 2
        '
        'Label10
        '
        Me.Label10.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label10.Location = New System.Drawing.Point(3, 28)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(124, 19)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Private Key File"
        '
        'txtPrivateKeyFile
        '
        Me.txtPrivateKeyFile.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPrivateKeyFile.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtPrivateKeyFile.Border.Class = "TextBoxBorder"
        Me.txtPrivateKeyFile.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPrivateKeyFile.ForeColor = System.Drawing.Color.Black
        Me.txtPrivateKeyFile.Location = New System.Drawing.Point(133, 28)
        Me.txtPrivateKeyFile.Name = "txtPrivateKeyFile"
        Me.txtPrivateKeyFile.Size = New System.Drawing.Size(297, 21)
        Me.txtPrivateKeyFile.TabIndex = 1
        '
        'optPFX
        '
        Me.optPFX.AutoSize = True
        Me.optPFX.Location = New System.Drawing.Point(3, 3)
        Me.optPFX.Name = "optPFX"
        Me.optPFX.Size = New System.Drawing.Size(126, 17)
        Me.optPFX.TabIndex = 0
        Me.optPFX.TabStop = True
        Me.optPFX.Text = "Use Client Certificate"
        Me.optPFX.UseVisualStyleBackColor = True
        '
        'grpProxy
        '
        Me.grpProxy.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpProxy.BackColor = System.Drawing.Color.Transparent
        Me.grpProxy.ColumnCount = 2
        Me.grpProxy.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.88119!))
        Me.grpProxy.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.11881!))
        Me.grpProxy.Controls.Add(Me.txtProxyPort, 1, 3)
        Me.grpProxy.Controls.Add(Me.Label11, 0, 0)
        Me.grpProxy.Controls.Add(Me.Label12, 0, 1)
        Me.grpProxy.Controls.Add(Me.Label13, 0, 2)
        Me.grpProxy.Controls.Add(Me.Label14, 0, 3)
        Me.grpProxy.Controls.Add(Me.txtProxyServer, 1, 0)
        Me.grpProxy.Controls.Add(Me.txtProxyUserName, 1, 1)
        Me.grpProxy.Controls.Add(Me.txtProxyPassword, 1, 2)
        Me.grpProxy.Enabled = False
        Me.grpProxy.Location = New System.Drawing.Point(4, 29)
        Me.grpProxy.Name = "grpProxy"
        Me.grpProxy.RowCount = 4
        Me.grpProxy.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.grpProxy.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.grpProxy.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.grpProxy.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.grpProxy.Size = New System.Drawing.Size(513, 100)
        Me.grpProxy.TabIndex = 3
        '
        'txtProxyPort
        '
        Me.txtProxyPort.Location = New System.Drawing.Point(192, 78)
        Me.txtProxyPort.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.txtProxyPort.Name = "txtProxyPort"
        Me.txtProxyPort.Size = New System.Drawing.Size(68, 21)
        Me.txtProxyPort.TabIndex = 3
        Me.txtProxyPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtProxyPort.Value = New Decimal(New Integer() {21, 0, 0, 0})
        '
        'Label11
        '
        Me.Label11.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        '
        '
        '
        Me.Label11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label11.Location = New System.Drawing.Point(3, 3)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(64, 16)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Proxy Server"
        '
        'Label12
        '
        Me.Label12.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        '
        '
        '
        Me.Label12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label12.Location = New System.Drawing.Point(3, 28)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(106, 16)
        Me.Label12.TabIndex = 1
        Me.Label12.Text = "User Name (optional)"
        '
        'Label13
        '
        Me.Label13.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        '
        '
        '
        Me.Label13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label13.Location = New System.Drawing.Point(3, 53)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(99, 16)
        Me.Label13.TabIndex = 2
        Me.Label13.Text = "Password (optional)"
        '
        'Label14
        '
        Me.Label14.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label14.AutoSize = True
        '
        '
        '
        Me.Label14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label14.Location = New System.Drawing.Point(3, 78)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(22, 16)
        Me.Label14.TabIndex = 3
        Me.Label14.Text = "Port"
        '
        'txtProxyServer
        '
        Me.txtProxyServer.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtProxyServer.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtProxyServer.Border.Class = "TextBoxBorder"
        Me.txtProxyServer.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtProxyServer.ForeColor = System.Drawing.Color.Black
        Me.txtProxyServer.Location = New System.Drawing.Point(192, 3)
        Me.txtProxyServer.Name = "txtProxyServer"
        Me.txtProxyServer.Size = New System.Drawing.Size(318, 21)
        Me.txtProxyServer.TabIndex = 0
        '
        'txtProxyUserName
        '
        Me.txtProxyUserName.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtProxyUserName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtProxyUserName.Border.Class = "TextBoxBorder"
        Me.txtProxyUserName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtProxyUserName.ForeColor = System.Drawing.Color.Black
        Me.txtProxyUserName.Location = New System.Drawing.Point(192, 28)
        Me.txtProxyUserName.Name = "txtProxyUserName"
        Me.txtProxyUserName.Size = New System.Drawing.Size(318, 21)
        Me.txtProxyUserName.TabIndex = 1
        '
        'txtProxyPassword
        '
        Me.txtProxyPassword.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtProxyPassword.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtProxyPassword.Border.Class = "TextBoxBorder"
        Me.txtProxyPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtProxyPassword.ForeColor = System.Drawing.Color.Black
        Me.txtProxyPassword.Location = New System.Drawing.Point(192, 53)
        Me.txtProxyPassword.Name = "txtProxyPassword"
        Me.txtProxyPassword.Size = New System.Drawing.Size(318, 21)
        Me.txtProxyPassword.TabIndex = 2
        '
        'chkProxy
        '
        Me.chkProxy.AutoSize = True
        '
        '
        '
        Me.chkProxy.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkProxy.Location = New System.Drawing.Point(3, 3)
        Me.chkProxy.Name = "chkProxy"
        Me.chkProxy.Size = New System.Drawing.Size(105, 16)
        Me.chkProxy.TabIndex = 0
        Me.chkProxy.Text = "Use Proxy Server"
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'stabFTP
        '
        Me.stabFTP.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.stabFTP.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabFTP.ControlBox.MenuBox.Name = ""
        Me.stabFTP.ControlBox.Name = ""
        Me.stabFTP.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabFTP.ControlBox.MenuBox, Me.stabFTP.ControlBox.CloseBox})
        Me.stabFTP.Controls.Add(Me.SuperTabControlPanel1)
        Me.stabFTP.Controls.Add(Me.SuperTabControlPanel2)
        Me.stabFTP.Controls.Add(Me.SuperTabControlPanel3)
        Me.stabFTP.Dock = System.Windows.Forms.DockStyle.Fill
        Me.stabFTP.ForeColor = System.Drawing.Color.Black
        Me.stabFTP.Location = New System.Drawing.Point(0, 0)
        Me.stabFTP.Name = "stabFTP"
        Me.stabFTP.ReorderTabsEnabled = True
        Me.stabFTP.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabFTP.SelectedTabIndex = 0
        Me.stabFTP.Size = New System.Drawing.Size(520, 197)
        Me.stabFTP.TabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.stabFTP.TabIndex = 3
        Me.stabFTP.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabGeneral, Me.tabCertificate, Me.tabProxy})
        Me.stabFTP.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.chkRetryUpload)
        Me.SuperTabControlPanel1.Controls.Add(Me.TableLayoutPanel1)
        Me.SuperTabControlPanel1.Controls.Add(Me.chkAscii)
        Me.SuperTabControlPanel1.Controls.Add(Me.chkPassive)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(520, 171)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabGeneral
        '
        'chkRetryUpload
        '
        Me.chkRetryUpload.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkRetryUpload.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkRetryUpload.Location = New System.Drawing.Point(199, 124)
        Me.chkRetryUpload.Name = "chkRetryUpload"
        Me.chkRetryUpload.Size = New System.Drawing.Size(211, 18)
        Me.chkRetryUpload.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.chkRetryUpload.TabIndex = 3
        Me.chkRetryUpload.Text = "Retry uploading if the transaction fails"
        '
        'tabGeneral
        '
        Me.tabGeneral.AttachedControl = Me.SuperTabControlPanel1
        Me.tabGeneral.GlobalItem = False
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Text = "General"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.chkClearChannel)
        Me.SuperTabControlPanel2.Controls.Add(Me.grpCerts)
        Me.SuperTabControlPanel2.Controls.Add(Me.FlowLayoutPanel1)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(520, 197)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.tabCertificate
        '
        'chkClearChannel
        '
        Me.chkClearChannel.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkClearChannel.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkClearChannel.Location = New System.Drawing.Point(7, 135)
        Me.chkClearChannel.Name = "chkClearChannel"
        Me.chkClearChannel.Size = New System.Drawing.Size(167, 16)
        Me.chkClearChannel.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.chkClearChannel.TabIndex = 5
        Me.chkClearChannel.Text = "Clear Command Channel"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel1.Controls.Add(Me.optPFX)
        Me.FlowLayoutPanel1.Controls.Add(Me.optPVK)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(520, 25)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'tabCertificate
        '
        Me.tabCertificate.AttachedControl = Me.SuperTabControlPanel2
        Me.tabCertificate.GlobalItem = False
        Me.tabCertificate.Name = "tabCertificate"
        Me.tabCertificate.Text = "Certificate"
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.grpProxy)
        Me.SuperTabControlPanel3.Controls.Add(Me.FlowLayoutPanel2)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(520, 197)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.tabProxy
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel2.Controls.Add(Me.chkProxy)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(520, 23)
        Me.FlowLayoutPanel2.TabIndex = 0
        '
        'tabProxy
        '
        Me.tabProxy.AttachedControl = Me.SuperTabControlPanel3
        Me.tabProxy.GlobalItem = False
        Me.tabProxy.Name = "tabProxy"
        Me.tabProxy.Text = "Proxy Server"
        '
        'ucFTPDetails
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.Controls.Add(Me.stabFTP)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "ucFTPDetails"
        Me.Size = New System.Drawing.Size(520, 197)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        CType(Me.txtPort, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCerts.ResumeLayout(False)
        Me.grpProxy.ResumeLayout(False)
        Me.grpProxy.PerformLayout()
        CType(Me.txtProxyPort, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.stabFTP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabFTP.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel1.PerformLayout()
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtFTPServer As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtUserName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtDirectory As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmbFTPType As System.Windows.Forms.ComboBox
    Friend WithEvents txtPort As System.Windows.Forms.NumericUpDown
    Friend WithEvents btnVerify As DevComponents.DotNetBar.ButtonX
    Friend WithEvents chkPassive As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents optPVK As System.Windows.Forms.RadioButton
    Friend WithEvents optPFX As System.Windows.Forms.RadioButton
    Friend WithEvents grpCerts As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtCertFile As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtCertName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtCertPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnBrowse As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtPrivateKeyFile As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnBrowse2 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents chkProxy As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents grpProxy As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label13 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label14 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtProxyPort As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtProxyServer As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtProxyUserName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtProxyPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents btnBrowseFTP As DevComponents.DotNetBar.ButtonX
    Friend WithEvents chkAscii As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents stabFTP As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabGeneral As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents tabCertificate As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents tabProxy As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents chkRetryUpload As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkClearChannel As DevComponents.DotNetBar.Controls.CheckBoxX

End Class
