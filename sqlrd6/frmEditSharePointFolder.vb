﻿Friend Class frmEditSharePointFolder
    Public eventID As Integer = 99999
    Public m_eventBased As Boolean
    Dim ep As New ErrorProvider
    Dim userCancel As Boolean
    Dim txt As TextBox
    Dim oInserter As frmInserter

    Private Sub frmEditSharePointFolder_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        setupForDragAndDrop(txtLibrary) '.ContextMenu = mnuInserter)
        setupForDragAndDrop(txtValue) '.ContextMenu = mnuInserter)
        setupForDragAndDrop(txtName)

        oInserter = New frmInserter(eventID)
        oInserter.GetConstants(Me)
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        txt.Undo()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        txt.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        txt.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        txt.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        txt.SelectedText = ""
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        txt.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(eventID)
        oInsert.m_EventBased = Me.m_eventBased
        oInsert.m_EventID = Me.eventID
        'oField.SelectedText = oInsert.GetConstants(Me)

        oInsert.GetConstants(Me, sender.Container)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oItem As New frmDataItems

        txt.SelectedText = oItem._GetDataItem(Me.eventID)

    End Sub

    Public Function editSharepointLib(ByVal library As String, ByRef metaData As String) As String
        txtLibrary.Text = library

        For Each s As String In metaData.Split(PD)
            If s IsNot Nothing AndAlso s.Contains("=") Then
                Dim fldName, fldValue As String
                fldName = s.Split("=")(0)
                fldValue = s.Split("=")(1)

                Dim lsv As ListViewItem = New ListViewItem(fldName)
                lsv.SubItems.Add(fldValue)

                lsvMetadata.Items.Add(lsv)
            End If
        Next

        Me.ShowDialog()

        If userCancel = True Then Return Nothing

        metaData = ""

        For Each it As ListViewItem In lsvMetadata.Items
            Dim name As String = it.Text
            Dim value As String = it.SubItems(1).Text
            Dim entry As String = name & "=" & value

            metaData &= entry & PD
        Next

        Return txtLibrary.Text
    End Function

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        userCancel = True
        Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtLibrary.Text = "" Then
            ep.SetError(txtLibrary, "Please specify the Sharepoint libary")
            txtLibrary.Focus()
            Return
        End If

        Close()
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If txtName.Text = "" Or txtValue.Text = "" Then Return

        Dim lsv As ListViewItem

        '//check if the item already exists in the list
        For Each it As ListViewItem In lsvMetadata.Items
            If it.Text.ToLower = txtName.Text.ToLower Then
                lsv = it

                lsv.SubItems(1).Text = txtValue.Text

                txtName.Focus()
                Return
            End If
        Next

        '//if we got here then it doesn't exist in the list
        lsv = New ListViewItem(txtName.Text)
        lsv.SubItems.Add(txtValue.Text)

        lsvMetadata.Items.Add(lsv)

        txtName.Focus()
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        If lsvMetadata.SelectedItems.Count = 0 Then Return

        For Each it As ListViewItem In lsvMetadata.SelectedItems
            it.Remove()
        Next
    End Sub

    Private Sub lsvMetadata_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvMetadata.SelectedIndexChanged
        If lsvMetadata.SelectedItems.Count = 0 Then Return

        Dim it As ListViewItem = lsvMetadata.SelectedItems(0)

        txtName.Text = it.Text
        txtValue.Text = it.SubItems(1).Text
    End Sub


    Private Sub txtValue_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtValue.GotFocus
        txt = txtValue
    End Sub

    Private Sub txtLibrary_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLibrary.GotFocus
        txt = txtLibrary
    End Sub

    Private Sub txtValue_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtValue.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Return Then
            btnAdd_Click(Nothing, Nothing)
        End If
    End Sub

End Class