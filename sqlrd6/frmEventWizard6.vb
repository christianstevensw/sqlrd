Public Class frmEventWizard6

    Dim CurrentStep As String = ""

    Const m_Step1 As String = "Step 1: Schedule Details"
    Const m_Step2 As String = "Step 2: Schedule Conditions"
    Const m_Step3 As String = "Step 3: Report Type"
    Const m_Step4 As String = "Step 4: Select schedules"
    Const m_Step5 As String = "Step 4: Add Reports"
    Const m_Step6 As String = "Step 5: Schedule Options"
    Const m_step7 As String = "Step 5: Custom Tasks"
    Const m_Step8 As String = "Step 6: Execution Flow"

    Dim nStep As Integer = 1
    Dim UserCancel As Boolean = True
    Dim m_EventDtls As New Hashtable
    Dim m_PackOrderID As Integer = 0

    Private Sub frmEventWizard6_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        clsMarsDebug.writeToDebug("_eventbasedload.debug", "MyBase.Load Event", False)

        clsMarsDebug.writeToDebug("_eventbasedload.debug", "Checking edition and purchased functionality", True)

        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.s3_EventBasedScheds) = False Then
            _NeedUpgrade(gEdition.ENTERPRISEPROPLUS, Me, "Event-Based Schedules")
            Close()
            Return
        End If

        clsMarsDebug.writeToDebug("_eventbasedload.debug", "formatting controls", True)

        FormatForWinXP(Me)

        clsMarsDebug.writeToDebug("_eventbasedload.debug", "building folder structure", True)

       

        clsMarsDebug.writeToDebug("_eventbasedload.debug", "initializing ucTasks", True)

        UcTasks1.ShowAfterType = False
        UcTasks1.m_eventBased = True
        UcTasks1.m_eventID = 99999
        UcTasks1.tvTasks.ColumnsVisible = False

        clsMarsDebug.writeToDebug("_eventbasedload.debug", "Loading folder parent properties", True)

        If gParentID > 0 And gParent.Length > 0 And txtLocation.Tag = "" Then
            Dim fld As folder = New folder(gParentID)

            txtLocation.Text = fld.getFolderPath
            txtLocation.Tag = gParentID
        End If

        clsMarsDebug.writeToDebug("_eventbasedload.debug", "cleaning the database", True)

        clsMarsData.DataItem.CleanDB()

        clsMarsDebug.writeToDebug("_eventbasedload.debug", "Assigning images", True)

        clsMarsEvent.currentEventID = 99999

        cmbPriority.Text = "3 - Normal"

        clsMarsDebug.writeToDebug("_eventbasedload.debug", "Form should now show to screen", True)
    End Sub


    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click
        closeFlag = False

        Select Case stabMain.SelectedTab.Text
            Case "General"
                If txtName.Text.Length = 0 Then
                    SetError(txtName, "Please enter the schedule name")
                    txtName.Focus()
                    Return
                ElseIf txtLocation.Text.Length = 0 Then
                    SetError(txtLocation, "Please select the schedule location")
                    txtLocation.Focus()
                    Return
                ElseIf clsMarsData.IsDuplicate("EventAttr", "EventName", txtName.Text, True) Then
                    SetError(txtName, "An event-based schedule with this name already exists")
                    txtName.Focus()
                    Return
                ElseIf clsMarsUI.candoRename(txtName.Text, Me.txtLocation.Tag, clsMarsScheduler.enScheduleType.EVENTBASED) = False Then
                    SetError(txtName, "An event-based schedule with this name already exists")
                    txtName.Focus()
                    Return
                End If

                tabConditions.Visible = True
                stabMain.SelectedTab = tabConditions
            Case "Conditions"
                If Me.UcConditions1.lsvConditions.Items.Count = 0 Then
                    setError(Me.UcConditions1.lsvConditions, "Please add at least one event.")
                    Return
                ElseIf cmbAnyAll.Text.Length = 0 Then
                    setError(cmbAnyAll, "Please specify a value for this field")
                    cmbAnyAll.Focus()
                    Return
                End If

                tabReports.Visible = True
                stabMain.SelectedTab = tabReports
            Case "Report Type"
                If Me.optExisting.Checked Then
                    UcExisting.lsvSchedules.ContextMenu = Me.mnuParameters

                    tabExistingReports.Visible = True
                    stabMain.SelectedTab = tabExistingReports
                ElseIf Me.optNone.Checked = True Then
                    tabException.Visible = True
                    stabMain.SelectedTab = tabException
                Else
                    tabNewReports.Visible = True
                    stabMain.SelectedTab = tabNewReports
                End If
            Case "Existing Reports"
                If UcExisting.lsvSchedules.Nodes.Count = 0 Then
                    setError(UcExisting.btnAdd, "Please select an existing schedule to execute when conditions are met")
                    UcExisting.btnAdd.Focus()
                    Return
                End If

                tabException.Visible = True
                stabMain.SelectedTab = tabException
            Case "New Reports"
                If UcReports1.tvReports.Nodes.Count = 0 Then
                    setError(Me.UcReports1.cmdAddReport, "Please add a report to be produced when conditions are met")
                    Me.UcReports1.cmdAddReport.Focus()
                    Return
                End If

                tabException.Visible = True
                stabMain.SelectedTab = tabException

                If Me.m_PackOrderID > 0 Then
                    Me.DividerLabel4.Visible = False
                    Me.chkOpHrs.Visible = False
                    Me.cmbOpHrs.Visible = False
                End If
            Case "Exception Handling"
                If Me.chkOpHrs.Checked Then
                    If Me.cmbOpHrs.Text = "" Or Me.cmbOpHrs.Text = "<New...>" Then
                        setError(Me.cmbOpHrs, "Please select a valid entry...")
                        Me.cmbOpHrs.Focus()
                        Return

                    End If
                End If

                tabTasks.Visible = True
                stabMain.SelectedTab = tabTasks
            Case "Custom Tasks"
                If UcTasks1.tvTasks.Nodes.Count = 0 Or optNone.Checked Then
                    ucFlow.Enabled = False
                Else
                    ucFlow.Enabled = True
                End If

                tabExecutionFlow.Visible = True
                stabMain.SelectedTab = tabExecutionFlow

                cmdFinish.Enabled = True
                cmdNext.Enabled = False
        End Select
    End Sub


    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Dim oFolders As frmFolders = New frmFolders
        Dim folderDetails() As String

        folderDetails = oFolders.GetFolder

        If folderDetails(0) <> "" Then
            txtLocation.Text = folderDetails(0)
            txtLocation.Tag = folderDetails(1)
        End If
    End Sub

    Private Sub txtLocation_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLocation.TextChanged

        Dim oText As TextBox = CType(sender, TextBox)

        If oText.Text.Length = 0 Then
            cmdNext.Enabled = False
        Else
            cmdNext.Enabled = True
        End If
    End Sub


    Private Sub cmbAnyAll_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAnyAll.SelectedIndexChanged
        setError(cmbAnyAll, "")
    End Sub
    Dim closeFlag As Boolean = False
    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        If closeFlag = False Then
            Dim sp As DevComponents.DotNetBar.SuperTooltip = New DevComponents.DotNetBar.SuperTooltip
            Dim spi As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo("Really Cancel?", "",
                                                                                                               "Are you sure you would like to close the wizard? All your progress will be lost. Click Cancel again to confirm.",
                                                                                                               Nothing,
                                                                                                               Nothing,
                                                                                                               DevComponents.DotNetBar.eTooltipColor.Office2003)
            sp.SetSuperTooltip(cmdCancel, spi)
            sp.ShowTooltip(cmdCancel)
            closeFlag = True
        Else
            clsMarsData.DataItem.CleanDB()
            Close()
        End If
    End Sub

    Public Function AddSchedule(ByVal PackOrderID As Integer) As Hashtable
        Me.m_PackOrderID = PackOrderID

        Me.cmbPriority.Enabled = False
        Me.chkOpHrs.Checked = False
        Me.chkOpHrs.Enabled = False

        Me.ShowDialog()

        If UserCancel = True Then
            Return Nothing
        Else
            Return Me.m_EventDtls
        End If

    End Function
    Private Sub cmdFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdFinish.Click
        Dim cols As String
        Dim vals As String
        Dim eventID As Integer = clsMarsData.CreateDataID("eventattr6", "eventid")



        Dim reportType As String

        If Me.optNone.Checked Then
            reportType = "None"
        ElseIf Me.optExisting.Checked Then
            reportType = "Existing"
        Else
            reportType = "New"
        End If

        Dim Status As String = ""

        If chkStatus.Enabled = True Then
            Status = "Enabled"
        Else
            Status = "Disabled"
        End If

        Me.m_EventDtls.Add("Name", txtName.Text)
        Me.m_EventDtls.Add("ID", eventID)
        Me.m_EventDtls.Add("Status", Status)

        cols = "EventID,EventName,Description,Keyword,Parent,Status,Owner,DisabledDate," & _
                "ScheduleType,AnyAll,ExecutionFlow,RetryCount,AutoFailAfter,PackOrderID,AutoCalc," & _
                "RetryInterval,UseOperationHours,OperationName,SchedulePriority, Packid, ExistingAsync,maxthreads"

        vals = eventID & "," & _
        "'" & SQLPrepare(txtName.Text) & "'," & _
        "'" & SQLPrepare(txtDescription.Text) & "'," & _
        "'" & SQLPrepare(txtKeyword.Text) & "'," & _
        Me.txtLocation.Tag & "," & _
        Convert.ToInt32(chkStatus.Checked) & "," & _
        "'" & SQLPrepare(gUser) & "'," & _
        "'" & ConDateTime(Now) & "'," & _
        "'" & reportType & "'," & _
        "'" & cmbAnyAll.Text & "'," & _
        "'" & ucFlow.m_ExecutionFlow & "'," & _
        UcError.cmbRetry.Value & "," & _
        UcError.m_autoFailAfter(True) & "," & _
        Me.m_PackOrderID & "," & _
        Convert.ToInt32(UcError.chkAutoCalc.Checked) & "," & _
        UcError.txtRetryInterval.Value & "," & _
        Convert.ToInt32(Me.chkOpHrs.Checked) & "," & _
        "'" & Me.cmbOpHrs.Text & "'," & _
        "'" & Me.cmbPriority.Text & "',0," & _
        Convert.ToInt16(chkExistingAsync.Checked) & "," & _
        txtMaxthreads.Value

        If clsMarsData.DataItem.InsertData("EventAttr6", cols, vals, True) = True Then
            Dim SQL As String = "UPDATE EventConditions SET EventID = " & eventID & " WHERE EventID = 99999"

            clsMarsData.WriteData(SQL)

            SQL = "UPDATE ReportAttr SET PackID = " & eventID & " WHERE PackID = 99999"

            clsMarsData.WriteData(SQL)

            SQL = "UPDATE Tasks SET ScheduleID = " & eventID & " WHERE ScheduleID = 99999"

            clsMarsData.WriteData(SQL)

            If Me.optExisting.Checked Then
                Dim nID As Integer
                Dim sType As String

                cols = "ID,EventID,ScheduleID,ScheduleType,OrderNumber"

                clsMarsUI.MainUI.BusyProgress(90, "Saving schedules for event...")

                Dim ordernumber As Integer = 1

                For Each oItem As DevComponents.AdvTree.Node In UcExisting.lsvSchedules.Nodes
                    nID = oItem.DataKey 'Tag.Split(":")(1)
                    sType = oItem.Tag '.Split(":")(0)


                    vals = clsMarsData.CreateDataID("eventschedule", "id") & "," & _
                    eventID & "," & _
                    nID & "," & _
                    "'" & sType & "'," & _
                    ordernumber

                    If clsMarsData.DataItem.InsertData("EventSchedule", cols, vals, True) = False Then
                        Exit For
                    End If

                    ordernumber += 1
                Next

                clsMarsData.WriteData("UPDATE EventSubAttr SET EventID =" & eventID & " WHERE EventID = 99999")

            End If
        End If

        UserCancel = False

        frmMainWin.itemToSelect = New genericExplorerObject
        frmMainWin.itemToSelect.folderID = txtLocation.Tag
        frmMainWin.itemToSelect.itemName = txtName.Text

        Close()

        clsMarsUI.BusyProgress(100, , True)
       

        clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.EVENTS, clsMarsAudit.AuditAction.CREATE)

    End Sub

    Private Sub cmbOpHrs_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbOpHrs.DropDown
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        Me.cmbOpHrs.Items.Clear()

        SQL = "SELECT * FROM OperationAttr"

        oRs = clsMarsData.GetData(SQL)

        Me.cmbOpHrs.Items.Add("<New...>")

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                cmbOpHrs.Items.Add(oRs("operationname").Value)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub

    Private Sub cmbOpHrs_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbOpHrs.SelectedIndexChanged
        SetError(sender, "")

        If cmbOpHrs.Text = "<New...>" Then
            Dim opAdd As frmOperationalHours = New frmOperationalHours

            Dim sNew As String = opAdd.AddOperationalHours()

            If sNew <> "" Then
                Me.cmbOpHrs.Items.Add(sNew)

                Me.cmbOpHrs.Text = sNew
            End If
        End If
    End Sub

    Private Sub chkOpHrs_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkOpHrs.CheckedChanged
        Me.cmbOpHrs.Enabled = Me.chkOpHrs.Checked
    End Sub

    Private Sub mnuParameter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuParameter.Click
        Dim sType As String
        Dim nID As Integer

        If UcExisting.lsvSchedules.SelectedNodes.Count = 0 Then Return

        Dim oItem As DevComponents.AdvTree.Node

        oItem = UcExisting.lsvSchedules.SelectedNodes(0)

        sType = oItem.Tag '.Split(":")(0)
        nID = oItem.DataKey  'Tag.Split(":")(1)

        If sType.ToLower <> "report" Then Return

        Dim oSub As frmEventSub = New frmEventSub

        oSub.m_eventID = 99999

        oSub.SetSubstitution(nID, 99999)

    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optNewReport.CheckedChanged
        If optNewReport.Checked Then
            tabExistingReports.Visible = False
        End If
    End Sub

    Private Sub optExisting_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optExisting.CheckedChanged
        pnlMaxthreads.Enabled = optExisting.Checked

        If optExisting.Checked Then
            AppStatus(True)
            tabNewReports.Visible = False
            clsMarsUI.MainUI.BuildAdvTree(UcExisting.tvSchedules, True)
            AppStatus(False)
        End If
    End Sub

    Private Sub optNone_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optNone.CheckedChanged
        If optNone.Checked Then
            tabNewReports.Visible = False
            tabExistingReports.Visible = False
        End If
    End Sub

    Private Sub chkExistingAsync_CheckedChanged(sender As Object, e As EventArgs) Handles chkExistingAsync.CheckedChanged
        txtMaxthreads.Enabled = chkExistingAsync.Checked
    End Sub
End Class