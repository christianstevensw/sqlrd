<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFieldChooser
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ListViewItem1 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Calendar Name")
        Dim ListViewItem2 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Description")
        Dim ListViewItem3 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Disabled Date")
        Dim ListViewItem4 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Duration")
        Dim ListViewItem5 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Enabled")
        Dim ListViewItem6 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("End Date")
        Dim ListViewItem7 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Exception Calendar")
        Dim ListViewItem8 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Execution Time")
        Dim ListViewItem9 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Frequency")
        Dim ListViewItem10 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Keywords")
        Dim ListViewItem11 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Last Run")
        Dim ListViewItem12 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Name")
        Dim ListViewItem13 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Recurring")
        Dim ListViewItem14 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Repeat")
        Dim ListViewItem15 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Repeat Interval")
        Dim ListViewItem16 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Repeat Until")
        Dim ListViewItem17 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Result")
        Dim ListViewItem18 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Success")
        Me.lsvFields = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.mnuState = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.SaveColumnsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.LoadColumnSelectionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.chkSelect = New System.Windows.Forms.CheckBox()
        Me.mnuState.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lsvFields
        '
        '
        '
        '
        Me.lsvFields.Border.Class = "ListViewBorder"
        Me.lsvFields.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvFields.CheckBoxes = True
        Me.lsvFields.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvFields.ContextMenuStrip = Me.mnuState
        Me.lsvFields.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvFields.FullRowSelect = True
        Me.lsvFields.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvFields.HideSelection = False
        ListViewItem1.StateImageIndex = 0
        ListViewItem2.StateImageIndex = 0
        ListViewItem3.StateImageIndex = 0
        ListViewItem4.StateImageIndex = 0
        ListViewItem5.StateImageIndex = 0
        ListViewItem6.StateImageIndex = 0
        ListViewItem7.StateImageIndex = 0
        ListViewItem8.StateImageIndex = 0
        ListViewItem9.StateImageIndex = 0
        ListViewItem10.StateImageIndex = 0
        ListViewItem11.StateImageIndex = 0
        ListViewItem12.StateImageIndex = 0
        ListViewItem13.StateImageIndex = 0
        ListViewItem14.StateImageIndex = 0
        ListViewItem15.StateImageIndex = 0
        ListViewItem16.StateImageIndex = 0
        ListViewItem17.StateImageIndex = 0
        ListViewItem18.StateImageIndex = 0
        Me.lsvFields.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem1, ListViewItem2, ListViewItem3, ListViewItem4, ListViewItem5, ListViewItem6, ListViewItem7, ListViewItem8, ListViewItem9, ListViewItem10, ListViewItem11, ListViewItem12, ListViewItem13, ListViewItem14, ListViewItem15, ListViewItem16, ListViewItem17, ListViewItem18})
        Me.lsvFields.Location = New System.Drawing.Point(0, 22)
        Me.lsvFields.Name = "lsvFields"
        Me.lsvFields.Size = New System.Drawing.Size(307, 365)
        Me.lsvFields.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lsvFields.TabIndex = 0
        Me.lsvFields.UseCompatibleStateImageBehavior = False
        Me.lsvFields.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Field"
        Me.ColumnHeader1.Width = 300
        '
        'mnuState
        '
        Me.mnuState.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SaveColumnsToolStripMenuItem, Me.ToolStripMenuItem1, Me.LoadColumnSelectionToolStripMenuItem})
        Me.mnuState.Name = "mnuState"
        Me.mnuState.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuState.Size = New System.Drawing.Size(198, 54)
        '
        'SaveColumnsToolStripMenuItem
        '
        Me.SaveColumnsToolStripMenuItem.Name = "SaveColumnsToolStripMenuItem"
        Me.SaveColumnsToolStripMenuItem.Size = New System.Drawing.Size(197, 22)
        Me.SaveColumnsToolStripMenuItem.Text = "Save Column Selection"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(194, 6)
        '
        'LoadColumnSelectionToolStripMenuItem
        '
        Me.LoadColumnSelectionToolStripMenuItem.Name = "LoadColumnSelectionToolStripMenuItem"
        Me.LoadColumnSelectionToolStripMenuItem.Size = New System.Drawing.Size(197, 22)
        Me.LoadColumnSelectionToolStripMenuItem.Text = "Load Column Selection"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.chkSelect)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(307, 22)
        Me.Panel1.TabIndex = 1
        '
        'chkSelect
        '
        Me.chkSelect.AutoSize = True
        Me.chkSelect.Checked = True
        Me.chkSelect.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkSelect.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.chkSelect.Location = New System.Drawing.Point(0, 5)
        Me.chkSelect.Name = "chkSelect"
        Me.chkSelect.Size = New System.Drawing.Size(307, 17)
        Me.chkSelect.TabIndex = 0
        Me.chkSelect.Text = "Select All"
        Me.chkSelect.UseVisualStyleBackColor = True
        '
        'frmFieldChooser
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(307, 387)
        Me.Controls.Add(Me.lsvFields)
        Me.Controls.Add(Me.Panel1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(323, 426)
        Me.MinimizeBox = False
        Me.Name = "frmFieldChooser"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Field Chooser"
        Me.mnuState.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lsvFields As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents chkSelect As System.Windows.Forms.CheckBox
    Friend WithEvents mnuState As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents SaveColumnsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents LoadColumnSelectionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
