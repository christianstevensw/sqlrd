Imports sqlrd.clsMarsData
Imports DevComponents.DotNetBar
Public Class frmCalendarExplorer
    Dim imgL As ImageList = New ImageList
    Private Sub ListCalendars()
        Dim SQL As String = "SELECT DISTINCT CalendarName FROM CalendarAttr"
        Dim oRs As ADODB.Recordset = GetData(SQL)
        Dim listItem As ListViewItem

        lsvList.Items.Clear()
        Calendar.BoldedDates = Nothing
        lsvDates.Items.Clear()

        If oRs Is Nothing Then
            Return
        End If

        Do While oRs.EOF = False
            listItem = New ListViewItem(CType(oRs(0).Value, String))
            listItem.ImageIndex = 0

            lsvList.Items.Add(listItem)
            oRs.MoveNext()
        Loop

        oRs.Close()

        oRs = Nothing
    End Sub

    Private Sub frmCalendarExplorer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim img As Image = Image.FromFile(IO.Path.Combine(sAssetsFolder, "calendar.png"))
        imgL.ImageSize = New Size(24, 24)
        imgL.ColorDepth = ColorDepth.Depth32Bit

        imgL.Images.Add("calendar", img)

        lsvList.SmallImageList = imgL
        lsvList.LargeImageList = imgL

        ListCalendars()

        lsvList.Columns(0).Width = lsvList.Width - 5
        lsvDates.Columns(0).Width = lsvDates.Width - 5

        If lsvList.Items.Count > 0 Then
            lsvList.Items(0).Selected = True
        End If
    End Sub

    Private Sub dbman_ItemClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dbman.ItemClick
        Dim oItem As BaseItem
        Dim name As String
        Dim oAdd As frmAddCalendar = New frmAddCalendar

        oItem = CType(sender, BaseItem)

        Select Case oItem.Name.ToLower
            Case "btnnew"

                name = oAdd.AddCalendar("")

                Me.ListCalendars()

                Me.FindCalendar(name)
            Case "btnedit"
                If lsvList.SelectedItems.Count = 0 Then Return

                name = oAdd.AddCalendar(lsvList.SelectedItems(0).Text)

                Me.ListCalendars()

                Me.FindCalendar(name)
            Case "btnshare"
                If lsvList.SelectedItems.Count = 0 Then Return

                ShareCalendar(lsvList.SelectedItems(0).Text)
            Case "btndelete"
                If lsvList.SelectedItems.Count = 0 Then Return

                DeleteCalendar(lsvList.SelectedItems(0).Text)
            Case "btnimport"
                ImportCalendar()
            Case "btnexport"
                If lsvList.SelectedItems.Count = 0 Then Return

                ExportCalendar(lsvList.SelectedItems(0).Text)
        End Select
    End Sub

    Private Sub FindCalendar(ByVal calendarName As String)
        For Each item As ListViewItem In lsvList.Items
            If item.Text.ToLower = calendarName.ToLower Then
                item.Selected = True
                Return
            End If
        Next
    End Sub
    Private Sub lsvList_AfterLabelEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.LabelEditEventArgs) Handles lsvList.AfterLabelEdit
        Dim SQL As String

        If e.Label Is Nothing Then Return

        If e.Label.Length = 0 Then e.CancelEdit = True


        SQL = "UPDATE CalendarAttr SET CalendarName = '" & SQLPrepare(e.Label) & "' WHERE " & _
        "CalendarName ='" & SQLPrepare(lsvList.SelectedItems(0).Text) & "'"

        clsMarsData.WriteData(SQL)
    End Sub

    Private Sub lsvList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvList.SelectedIndexChanged
        If lsvList.SelectedItems.Count = 0 Then Return

        Dim calendarName As String = lsvList.SelectedItems(0).Text

        ViewCalendar(calendarName)

    End Sub

    Private Sub ViewCalendar(ByVal calendarName As String)
        Dim oRs As ADODB.Recordset
        Dim isHoliday As Boolean = False
        Dim holidayForm As String = ""
        Dim year As Integer
        Dim dateValue As String
        Dim oHol As Dls.Holidays.HolidayService = New Dls.Holidays.HolidayService

        oRs = clsMarsData.GetData("SELECT * FROM CalendarAttr " & _
        "WHERE CalendarName = '" & SQLPrepare(calendarName) & "'")

        lsvDates.Items.Clear()

        Calendar.RemoveAllBoldedDates()
        Calendar.UpdateBoldedDates()

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                isHoliday = IsNull(oRs("isholiday").Value, 0)
                holidayForm = IsNull(oRs("holidayform").Value, "")

                If isHoliday = False And holidayForm = "" Then
                    dateValue = oRs("calendardate").Value
                Else
                    If oRs("calendardate").Value < Now Then
                        Dim countryCode As String = holidayForm.Split("|")(0)
                        Dim holidayName As String = holidayForm.Split("|")(1)

                        year = Now.Year

                        Do
                            Dim ds As DataSet = oHol.GetHolidaysForYear(countryCode, year)
                            Dim dt As DataTable = ds.Tables(0)

                            Dim rows() As DataRow = dt.Select("Name ='" & SQLPrepare(holidayName) & "'")

                            If rows.Length = 0 Then
                                dateValue = oRs("calendardate").Value
                            Else
                                Dim row As DataRow = rows(0)

                                dateValue = ConDate(row("Date"))
                            End If

                            year += 1
                        Loop Until ConDate(dateValue) > ConDate(Now.Date)
                    Else
                        dateValue = oRs("calendardate").Value
                    End If

                End If

                lsvDates.Items.Add(dateValue)

                Dim d As Date

                d = dateValue

                Calendar.AddBoldedDate(d)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If


        Calendar.UpdateBoldedDates()
    End Sub

    Private Sub ShareCalendar(ByVal calendarName As String)
        Dim oUI As New clsMarsUI

        Try
            Dim FileName As String
            Dim Path As String
            Dim NewPath As String
            Dim oRs As New ADODB.Recordset
            Dim SQL As String

            FileName = calendarName


            If FileName.EndsWith(".cal") = False Then FileName &= ".cal"

            Path = sAppPath & "Output\"

            Path = Path & FileName

            oUI.BusyProgress(25, "Opening calendar...")

            Path = Replace(Path, ".cal", ".xml")

            SQL = "SELECT * FROM CalendarAttr WHERE " & _
            "CalendarName = '" & SQLPrepare(calendarName) & "'"

            oRs = clsMarsData.GetData(SQL)

            oUI.BusyProgress(60, "Configurating calendar for sharing...")

            If Not oRs Is Nothing Then
                If oRs.EOF = False Then
                    CreateXML(oRs, Path)
                End If
            End If

            oUI.BusyProgress(75, "Finalizing export...")

            NewPath = Path.Replace(".xml", ".cal")

            System.IO.File.Copy(Path, NewPath)

            oUI.BusyProgress(85, "Sending calendar...")

            Dim oMsg As New clsMarsMessaging

            If MailType = MarsGlobal.gMailType.SMTP Or _
            MailType = MarsGlobal.gMailType.SQLRDMAIL Then
                oMsg.SendSMTP("support@christiansteven.com", CustID, _
                    "Sharing: " & calendarName, "Single", NewPath)
            ElseIf MailType = gMailType.GROUPWISE Then
                oMsg.SendGROUPWISE("support@christiansteven.com", "", "", CustID, "Sharing: " & calendarName, NewPath, "Single")
            ElseIf MailType = gMailType.MAPI Then
                oMsg.SendMAPI("support@christiansteven.com", CustID, _
                    "Sharing: " & calendarName, "Single", NewPath)
            End If

            Try
                System.IO.File.Delete(Path)
                System.IO.File.Delete(NewPath)
            Catch : End Try

            MessageBox.Show("Calendar shared successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), _
            "Please save the calendar before attempting to share it.")
        Finally
            oUI.BusyProgress(100, "", True)
        End Try
    End Sub

    Private Sub DeleteCalendar(ByVal calendarName As String)
        Dim Res As DialogResult

        Res = MessageBox.Show("Delete the '" & calendarName & "' calendar?", _
        Application.ProductName, _
        MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If Res = Windows.Forms.DialogResult.Yes Then
            Dim SQL As String = "SELECT ReportID,PackID,AutoID,SmartID FROM ScheduleAttr WHERE (Frequency = 'Custom' AND CalendarName ='" & SQLPrepare(calendarName) & "') OR " & _
            "ExceptionCalendar ='" & SQLPrepare(calendarName) & "'"

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
            Dim reportID As Integer = 0
            Dim packID As Integer = 0
            Dim autoID As Integer = 0
            Dim smartID As Integer = 0
            Dim type As String = ""
            Dim list As String = ""

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    reportID = IsNull(oRs("reportid").Value, 0)
                    packID = IsNull(oRs("packid").Value, 0)
                    autoID = IsNull(oRs("autoid").Value, 0)
                    smartID = IsNull(oRs("smartid").Value, 0)

                    Dim oRsName As ADODB.Recordset

                    If reportID > 0 Then
                        SQL = "SELECT ReportTitle, Dynamic,Bursting FROM ReportAttr WHERE ReportID =" & reportID
                        type = "Single Schedule: "
                    ElseIf packID > 0 Then
                        SQL = "SELECT PackageName FROM PackageAttr WHERE PackID =" & packID
                        type = "Package Schedule: "
                    ElseIf autoID > 0 Then
                        SQL = "SELECT AutoName FROM AutomationAttr WHERE AutoID =" & autoID
                        type = "Automation Schedule: "
                    ElseIf smartID > 0 Then
                        SQL = "SELECT SmartName FROM SmartFolders WHERE SmartID =" & smartID
                        type = "Smart Folder: "
                    End If

                    oRsName = clsMarsData.GetData(SQL)

                    If oRsName IsNot Nothing Then
                        If oRsName.EOF = False Then
                            Select Case type
                                Case "Single Schedule"
                                    If IsNull(oRsName("dynamic").Value, "0") = "1" Then
                                        type = "Dynamic Schedule: "
                                    ElseIf IsNull(oRsName("bursting").Value, "0") = "1" Then
                                        type = "Bursting Schedule: "
                                    Else
                                        type = "Single Schedule: "
                                    End If
                            End Select

                            list &= type & oRsName.Fields(0).Value & vbCrLf
                        End If

                        oRsName.Close()
                    End If

                    oRs.MoveNext()
                Loop

                oRs.Close()

                If type.Length > 0 Then
                    MessageBox.Show("You cannot delete the selected custom calendar as it is in use by the following schedules:" & vbCrLf & _
                    list, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                    Return
                End If
            End If

            clsMarsData.WriteData("DELETE FROM CalendarAttr WHERE " & _
            "CalendarName = '" & SQLPrepare(calendarName) & "'")

            Calendar.BoldedDates = Nothing

            lsvDates.Items.Clear()

            lsvList.SelectedItems(0).Remove()
        End If

    End Sub

    Private Sub ImportCalendar()
        Dim oRs As ADODB.Recordset
        Dim rsTest As New ADODB.Recordset
        Dim SQL As String
        Dim FileName As String
        Dim NewFilename As String
        Dim NewName As String
        Dim I As Integer = 1
        Dim oUI As New clsMarsUI
        Dim Append As Boolean = False
        Dim nCount As Integer
        Dim ofd As OpenFileDialog = New OpenFileDialog

        Try
            ofd.Title = "Select the calendar file to import..."
            ofd.Filter = "Calendar File|*.cal"

            If ofd.ShowDialog() <> Windows.Forms.DialogResult.OK Then Return

            FileName = ofd.FileName

            NewFilename = FileName.Replace(".cal", ".xml")

            System.IO.File.Copy(FileName, NewFilename, True)

            oUI.BusyProgress(20, "Reading calendar file")

            oRs = GetXML(NewFilename)

            If oRs Is Nothing Then Return

            If oRs.EOF = False Then

                Do While oRs.EOF = False
                    nCount += 1
                    oRs.MoveNext()
                Loop

                oRs.MoveFirst()

                NewName = ExtractFileName(NewFilename)

                NewName = Replace(NewName, ".xml", "")

                oUI.BusyProgress(50, "Validating file contents...")

                SQL = "SELECT * FROM CalendarAttr WHERE CalendarName = '" & NewName & "'"

                rsTest = clsMarsData.GetData(SQL, ADODB.CursorTypeEnum.adOpenStatic)

                If rsTest IsNot Nothing Then
                    If rsTest.RecordCount > 0 Then
                        Dim Res As DialogResult

                        Res = MessageBox.Show("A Calendar with this name " & _
                        "already exists in the system, add this calendar " & _
                        "to the existing one?", _
                        Application.ProductName, _
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question)

                        If Res = Windows.Forms.DialogResult.No Then
                            NewName = InputBox("Please provide a new name " & _
                            "for the imported calendar", Application.ProductName)
                        Else
                            NewName = NewName
                            Append = True
                        End If
                    End If

                    rsTest.Close()
                End If

                If NewName.Length = 0 Then
                    oUI.BusyProgress(, , True)
                    Exit Sub
                End If

                oUI.BusyProgress(75, "Importing calendar data...")

                Do While oRs.EOF = False
                    SQL = "INSERT INTO CalendarAttr(CalendarID, CalendarName, " & _
                    "CalendarDate) Values(" & _
                    clsMarsData.CreateDataID("calendarattr", "calendarid") & "," & _
                    "'" & SQLPrepare(NewName) & "'," & _
                    "'" & oRs("calendardate").Value & "')"

                    Dim Ok As Boolean = clsMarsData.WriteData(SQL)

                    If Ok = False Then Exit Do

                    oRs.MoveNext()

                    I += 1

                    oUI.BusyProgress((I / nCount) * 100, "Importing calendar data...")
                Loop

                oRs.Close()
            End If

            Me.ListCalendars()

            Me.FindCalendar(NewName)

            oUI.BusyProgress(95, "Cleaning up...")

            System.IO.File.Delete(NewFilename)

            oUI.BusyProgress(, , True)

            MessageBox.Show("Calendar imported successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub ExportCalendar(ByVal calendarName As String)

        Dim Path As String
        Dim SQL As String
        Dim oUI As New clsMarsUI
        Dim oRs As ADODB.Recordset
        Dim sfd As SaveFileDialog = New SaveFileDialog

        sfd.OverwritePrompt = True
        sfd.Title = "Save calendar as..."
        sfd.Filter = "Calendar File|*.cal"

        If sfd.ShowDialog() <> Windows.Forms.DialogResult.OK Then Return

        Path = sfd.FileName.ToLower

        If Path.EndsWith(".cal") = False Then
            Path &= ".cal"
        End If

        oUI.BusyProgress(25, "Opening calendar...")

        Path = Path.Replace(".cal", ".xml")

        SQL = "SELECT * FROM CalendarAttr WHERE CalendarName = '" & SQLPrepare(calendarName) & "'"

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then

            oUI.BusyProgress(60, "Configurating calendar for export...")

            If oRs.EOF = False Then
                CreateXML(oRs, Path)
            Else
                MessageBox.Show("Please save the calendar before attempting to export it", Application.ProductName, _
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                oRs.Close()

                oUI.BusyProgress(, , True)
                Return
            End If

        End If

        oUI.BusyProgress(95, "Finalizing export...")

        System.IO.File.Copy(Path, Path.Replace(".xml", ".cal"), True)

        System.IO.File.Delete(Path)

        oUI.BusyProgress(, , True)

        MessageBox.Show("Calendar Exported Successfully", _
        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
    Private Sub lsvDates_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        On Error Resume Next
        lsvDates.Columns(0).Width = lsvDates.Width - 5
    End Sub

    Private Sub lsvList_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvList.SizeChanged
        On Error Resume Next
        lsvList.Columns(0).Width = lsvList.Width - 5
    End Sub

    Private Sub lsvDates_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvDates.SelectedIndexChanged
        If Me.lsvDates.SelectedItems.Count = 0 Then Return

        Me.Calendar.SetDate(lsvDates.SelectedItems(0).Text)
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click

    End Sub

    Private Sub btnDelete_Click(sender As System.Object, e As System.EventArgs) Handles btnDelete.Click

    End Sub
End Class