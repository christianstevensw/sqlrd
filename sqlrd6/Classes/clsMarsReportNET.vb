Imports System.Xml

#If CRYSTAL_VER = 12 Then
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
#End If


Partial Class clsMarsReport
#If CRYSTAL_VER = 14 Then
    Public Overloads Sub XLPDFMerging(ByVal nReportID As Integer, ByVal oRpt As ReportClass, ByVal UseSavedData As Boolean, _
           ByVal CRFieldType As String, ByVal sBurstColumn As String, ByVal sFormat As String, _
           ByVal sDateStamp As String, ByRef sExport As String, ByVal sFileName As String, ByVal oRs As ADODB.Recordset, _
           ByVal sFormula As String, ByVal nRetry As Integer, ByVal ExcelBurst As Boolean, ByVal sExt As String, _
           ByVal nDestinationID As Integer, ByVal IsDynamic As Boolean, ByVal PDFBookmark As Boolean)
10:     Try
            Dim sOriginalGroupFormula As String
            Dim sGroupValue As String
20:         Dim oCombo As New ComboBox
30:         Dim oGroups As New ucBursting
            Dim oItems As ComboBox.ObjectCollection
            Dim sGroupFormula As String
            'Dim oPrinter As CrystalDecisions.CrystalReports.Engine.pri
            Dim nPages() As Integer
            Dim sGroupNames() As String
40:         Dim oUI As clsMarsUI = New clsMarsUI

50:         oUI.BusyProgress(60, "Bursting report groups...")

60:         If UseSavedData = False Then

70:             oRpt.Refresh()

80:             ClearPar(oRpt)
90:
100:            If SetReportParameters(nReportID) = False Then
110:                Return
120:            End If

130:            Dim oSub As New clsSubReports

140:            If oSub.SetSubParameters(oRpt, nReportID) = False Then
150:                Return
160:            End If

170:            If oSub.SetSubReportLogin(oRpt, nReportID) = False Then
180:                Return
190:            End If
200:        End If

210:        oGroups.oCombo = oCombo
220:        oGroups.oRpt = oRpt
230:        oGroups.GetGroupValues(oRpt)

240:        oGroups = Nothing

250:        oItems = oCombo.Items

260:        ReDim rptFileNames(0)
270:        ReDim nPages(0)
280:        ReDim sGroupNames(0)

290:        If ExcelBurst = True Then
300:            sOriginalGroupFormula = oRpt.DataDefinition.GroupSelectionFormula

310:            For w As Integer = 0 To oItems.Count - 1

320:                oUI.BusyProgress(75, "Bursting group " & (w + 1) & " of " & oItems.Count)

330:                oRpt.DataDefinition.GroupSelectionFormula = sOriginalGroupFormula

340:                sGroupValue = oItems(w)

350:                If CRFieldType.Length > 0 Then
360:                    Select Case CRFieldType.ToLower
                            Case "crstringfield", "stringfield"
370:                            sGroupFormula = "{" & sBurstColumn & "} = '" & SQLPrepare(sGroupValue) & "'"
380:                        Case "sqlrdatefield", "datefield"
                                Dim tempDate As Date = sGroupValue
                                Dim Year, Month, Day, Hours, Minutes As Integer

390:                            Year = tempDate.Year
400:                            Month = tempDate.Month
410:                            Day = tempDate.Day
420:                            Hours = tempDate.Hour
430:                            Minutes = tempDate.Minute

440:                            Dim groupValue As String = "cdatetime(" & Year & "," & Month & "," & Day & ")"

450:                            sGroupFormula = "{" & sBurstColumn & "} = " & groupValue
460:                        Case "sqlrdatetimefield", "sqlrdatefield", "datetimefield"
                                Dim tempDate As Date = sGroupValue
                                Dim Year, Month, Day, Hours, Minutes As Integer

470:                            Year = tempDate.Year
480:                            Month = tempDate.Month
490:                            Day = tempDate.Day
500:                            Hours = tempDate.Hour
510:                            Minutes = tempDate.Minute

                                Dim groupValue As String = "cdatetime(" & Year & "," & Month & "," & Day & "," & Hours & "," & Minutes & ",0)"

520:                            sGroupFormula = "{" & sBurstColumn & "} = " & groupValue
530:                        Case "crnumberfield", "crcurrencyfield", "numberfield", "currencyfield"
540:                            sGroupValue = sGroupValue.Replace(",", "")
550:                            sGroupFormula = "{" & sBurstColumn & "} = ToNumber(" & sGroupValue & ")"
560:                        Case Else
570:                            sGroupFormula = "{" & sBurstColumn & "} = " & sGroupValue
                        End Select
580:                Else
590:                    Try
600:                        Dim n As Double
610:                        n = sGroupValue

620:                        sGroupFormula = "{" & sBurstColumn & "} = ToNumber(" & n & ")"
630:                    Catch
640:                        Try
650:                            Date.Parse(sGroupValue)
660:                            sGroupFormula = "{" & sBurstColumn & "} = cdate('" & sGroupValue & "')"
670:                        Catch
680:                            sGroupFormula = "{" & sBurstColumn & "} = '" & SQLPrepare(sGroupValue) & "'"
                            End Try
690:                    End Try
                    End If

700:                If oRpt.DataDefinition.GroupSelectionFormula.Length > 0 Then
710:                    Dim grpTemp As String

720:                    grpTemp = oRpt.DataDefinition.GroupSelectionFormula

730:                    grpTemp &= " AND " & sGroupFormula

740:                    Try
750:                        oRpt.DataDefinition.GroupSelectionFormula = grpTemp
760:                    Catch : End Try
770:                Else
780:                    Try
790:                        oRpt.DataDefinition.GroupSelectionFormula = sGroupFormula
800:                    Catch : End Try
810:                End If

820:                sGroupValue = sGroupValue.Replace(":", "").Replace("\", "").Replace("/", "").Replace("|", "").Replace("?", ""). _
                          Replace("""", "").Replace("<", "").Replace(">", ""). _
                          Replace("*", "").Replace("-", "")

                    '2420:       oPrinter = oRpt.PrintingStatus 

830:                sExport = ProcessReport(sFormat, oRpt, sDateStamp, nReportID, _
                          m_OutputFolder & sGroupValue, sFileName, , _
                          nDestinationID, "", sFormula, UseSavedData, , True, , nRetry)

840:                If sExport.ToLower = "{error}" Then
850:                    Return
                    End If

860:                If ExcelBurst = True Then
870:                    Try
880:                        Dim nTry As Double

890:                        nTry = sGroupValue

900:                        sGroupValue = "{" & sGroupValue & "}"
                        Catch : End Try

910:                    Dim oXL As ExcelMan.clsExcelMan = New ExcelMan.clsExcelMan(ExcelMan.clsExcelMan.XLFormats.Excel8)

920:                    oXL.SetWorkSheetName(sExport, sGroupValue)

930:                    oXL.Dispose()
                    End If

940:                ReDim Preserve rptFileNames(w)
950:                ReDim Preserve nPages(w)
960:                ReDim Preserve sGroupNames(w)

970:                rptFileNames(w) = sExport

                    'get the total pages
980:                nPages(w) = oRpt.FormatEngine.GetLastPageNumber(New CrystalDecisions.Shared.ReportPageRequestContext)

990:                sGroupNames(w) = oItems(w)
1000:           Next
            End If

1010:       If frmMain.m_UserCancel = True Then
1020:           Me.CancelExecution("RunSingleSchedule")
1030:           Return
            End If

1040:       If ExcelBurst = True Then
1050:           oUI.BusyProgress(60, "Merging Excel files...")

1060:           PrepareXLS(m_OutputFolder & sFileName & sDateStamp)

1070:           sExport = rptFileNames(0)
1080:       Else
1090:           sExport = ProcessReport(sFormat, oRpt, sDateStamp, nReportID, _
                     m_OutputFolder & sGroupValue, sFileName, , _
                     nDestinationID, "", sFormula, UseSavedData, , True, , nRetry, True)

                Dim oRs1 As ADODB.Recordset

1100:           oRs1 = clsMarsData.GetData("SELECT * FROM ReportOptions WHERE DestinationID =" & nDestinationID)

1110:           If Not oRs1 Is Nothing Then
1120:               If oRs1.EOF = False Then
1130:                   Dim PDFSecurity As Boolean = False

1140:                   Try
1150:                       If oRs1("pdfsecurity").Value = 1 Or oRs1("pdfsecurity").Value = -1 Then
1160:                           PDFSecurity = True
1170:                       End If
1180:                   Catch
1190:                       PDFSecurity = False
1200:                   End Try

1210:                   sExport = Me._PostProcessPDF(sExport, oRs1("infotitle").Value, _
                             oRs1("infoauthor").Value, oRs1("infosubject").Value, _
                             oRs1("infokeywords").Value, oRs1("infoproducer").Value, _
                             oRs1("infocreated").Value, IsDynamic, nReportID, oRpt, PDFSecurity, False, _
                             oRs1("canprint").Value, oRs1("cancopy").Value, oRs1("canedit").Value, _
                             oRs1("cannotes").Value, oRs1("canfill").Value, oRs1("canaccess").Value, _
                             oRs1("canassemble").Value, _
                             oRs1("canprintfull").Value, oRs1("pdfpassword").Value, _
                             oRs1("userpassword").Value, oRs1("pdfwatermark").Value, 0)
                    End If
                End If
            End If
1220:   Catch ex As Exception

        End Try
    End Sub




    Public Overloads Function addsectionHandlers(ByVal oRpt As ReportClass) As Boolean
        Dim fileName As String = Me.m_OutputFolder & IO.Path.GetRandomFileName

10:     oRpt.ExportToDisk(ExportFormatType.Xml, fileName)

20:     Dim doc As XmlDocument = New XmlDocument

30:     doc.Load(fileName)

        Dim groupNodeList As XmlNodeList
        Dim groupNode As XmlNode

40:     groupNodeList = doc.GetElementsByTagName("Field")

        'get the name of the first field
        Dim field As String = groupNodeList.Item(0).Attributes("FieldName").Value

50:     For Each groupNode In groupNodeList
            Dim s As String = groupNode.Attributes("FieldName").Value
            Dim n As Integer = 0

60:         If reportFields.ContainsKey(s) = False Then
                Dim nodes As XmlNodeList = groupNode.ChildNodes

70:             If n = 0 Then
80:                 For Each node As XmlNode In nodes
90:                     If node.Name = "Value" Then
100:                        reportFields.Add(s, node.InnerText)
                        End If
110:                Next
                End If
            End If
120:    Next
    End Function







#End If
End Class
