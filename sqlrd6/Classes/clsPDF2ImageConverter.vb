Friend Class clsPDF2ImageConverter

    Private Declare Function PDFToImageConverter Lib _
    "pdf2image.dll" _
    (ByVal szPDFFileName As String, ByVal szOutputName As String, _
    ByVal szUserPassword As String, ByVal szOwnPassword As String, _
    ByVal xresolution As Integer, ByVal yresolution As Integer, ByVal bitcount As Integer, _
    ByVal compression As Integer, ByVal quality As Integer, ByVal grayscale As Integer, _
    ByVal multipage As Integer, ByVal firstPage As Integer, ByVal lastPage As Integer) As Integer

    Public Enum CompressionType
        COMPRESSION_NONE = 1              '/* dump mode */
        COMPRESSION_CCITTRLE = 2          '/* CCITT modified Huffman RLE */
        COMPRESSION_CCITTFAX3 = 3         '/* CCITT Group 3 fax encoding */
        COMPRESSION_CCITTFAX4 = 4         '/* CCITT Group 4 fax encoding */
        COMPRESSION_LZW = 5               '/* Lempel-Ziv  & Welch */
        COMPRESSION_JPEG = 7              '/* JPEG DCT compression */
        COMPRESSION_PACKBITS = 32773      '/* Macintosh RLE */
    End Enum

    Public Sub New()

    End Sub

    Public Function ConvertPDFToImage(ByVal strInputPDF As String, ByVal strOutputImage As String, _
    ByVal strUsername As String, ByVal strPassword As String, _
    ByVal lngXDPI As Long, ByVal lngYDPI As Long, ByVal lngBitCount As Long, _
    ByVal enumCompression As CompressionType, ByVal lngQuality As Long, _
    ByVal blnGreyscale As Boolean, ByVal blnMultipage As Boolean, _
    ByVal lngFirstPage As Long, ByVal lngLastPage As Long) As Long

        ConvertPDFToImage = PDFToImageConverter(strInputPDF, strOutputImage, strUsername, strPassword, _
                            lngXDPI, lngYDPI, lngBitCount, enumCompression, lngQuality, blnGreyscale, _
                            blnMultipage, lngFirstPage, lngLastPage)

    End Function


End Class
