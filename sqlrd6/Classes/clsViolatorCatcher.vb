﻿Imports System.Data.OleDb

Public Class clsViolatorCatcher

    Private m_noAlert As Boolean
    Public Property noAlert() As Boolean
        Get
            Return m_noAlert
        End Get
        Set(ByVal value As Boolean)
            m_noAlert = value
        End Set
    End Property

    Private Function transportTableDef() As DataTable
        Dim transportTable As DataTable = New DataTable("transportTable")

        With transportTable
            .Columns.Add("processtype")
            .Columns.Add("application")
            .Columns.Add("datasetlocation")
            .Columns.Add("outputlocation")
            .Columns.Add("violatedkeys")
        End With

        Return transportTable
    End Function
    Public Sub processData()
10:     Try
            '//get the user's additional functionality
            Dim key As String = clsMarsUI.MainUI.ReadRegistry("UpgradeKey", "", True)

20:         If key = "" Then key = CustID & "|"

30:         If gnEdition = gEdition.CORPORATE Or gnEdition = gEdition.EVALUATION Then Return

            If gIsXModel Then Return

            '//check that the scanner exists
            If IO.File.Exists(Application.StartupPath & "\CSS Scanner.exe") = False Then
                MessageBox.Show("A critical component required to validate your functionality is missing or corrupt. Please re-install the application.", Application.ProductName, _
                                  MessageBoxButtons.OK, MessageBoxIcon.Error)
                End
            End If

            Dim customKey As String = customKeyforEdition()

40:         If key.EndsWith("|") Then
50:             key = key & customKey
60:         Else
70:             key = key & "|" & customKey
            End If

80:         Dim showscanner As frmScanner = New frmScanner

90:         If Not noAlert Then showscanner.Show()

100:        Application.DoEvents()
            '//get what they are using, the bastards!
            'Dim scanner As CSS_Scanner.clsScanner = New CSS_Scanner.clsScanner
            'Dim pg As ProgressBar = New ProgressBar
            Dim conType As String = clsMarsUI.MainUI.ReadRegistry("ConType", "DAT")
            Dim dm As DataMigrator
            Dim srcdsn As String = ""

110:        If conType = "DAT" Then
120:            dm = New DataMigrator(DataMigrator.dbType.DAT)
130:        ElseIf conType = "ODBC" Then
140:            dm = New DataMigrator(DataMigrator.dbType.ODBC)

                Dim conString As String = clsMarsUI.MainUI.ReadRegistry("ConString", "", True)

150:            If conString.Length > 0 Then
                    Dim dsn As String = conString.Split(";")(4).Split("=")(1)
                    Dim user As String = conString.Split(";")(3).Split("=")(1)
                    Dim pwd As String = conString.Split(";")(1).Split("=")(1)

160:                srcdsn = "DSN=" & dsn & ";Uid=" & user & ";Pwd=" & pwd & ";"
                End If
170:        ElseIf conType = "LOCAL" Then
180:            dm = New DataMigrator(DataMigrator.dbType.LOCALSQL)
            End If

            '//create the database snapshot and save it to disk
            Dim xds As DataSet = dm.Export(srcdsn)
190:        xds.DataSetName = "mainExport"
            Dim xdsLoc As String = Environment.GetEnvironmentVariable("TEMP") & "\appscan.xml"
200:        If IO.File.Exists(xdsLoc) Then IO.File.Delete(xdsLoc)
210:        xds.WriteXml(xdsLoc, XmlWriteMode.WriteSchema)

            '//create the transport datatable and save it to disk; contains all the info needed for scan
            Dim usageDataLoc As String = Environment.GetEnvironmentVariable("TEMP") & "\usagedata.xml"
220:        If IO.File.Exists(usageDataLoc) Then IO.File.Delete(usageDataLoc)
            Dim transportTable As DataTable = transportTableDef()
            Dim trow As DataRow = transportTable.Rows.Add
230:        trow("processtype") = "scan"
240:        trow("application") = "sqlrd"
250:        trow("datasetlocation") = xdsLoc
260:        trow("outputlocation") = usageDataLoc
270:        trow("violatedkeys") = ""

            '//save the transport table
            Dim transportTableLoc As String = Environment.GetEnvironmentVariable("TEMP") & "\transportTable.xml"
280:        If IO.File.Exists(transportTableLoc) Then IO.File.Delete(transportTableLoc)
290:        transportTable.WriteXml(transportTableLoc, XmlWriteMode.WriteSchema)

            '//perform the scan out of process
300:        Dim pscanner As Process = New Process
310:        pscanner.StartInfo.FileName = Application.StartupPath & "\CSS Scanner.exe"
320:        pscanner.StartInfo.Arguments = transportTableLoc

330:        pscanner.Start()
340:        If Not noAlert Then showscanner.Label3.Text = "Perfoming scan of schedules..."
350:        pscanner.WaitForExit()

            System.Threading.Thread.Sleep(2000)
            '//read the result
360:        Dim usageData As DataTable = New DataTable

370:        If IO.File.Exists(usageDataLoc) Then
380:            usageData.ReadXml(usageDataLoc)
            End If

            Dim purchasedKeys As String() = key.Split("|")
390:        Dim violatedKeys As ArrayList = New ArrayList

400:        For Each row As DataRow In usageData.Rows
                Dim useKey As String = row("feature").ToString.ToLower

410:            useKey = useKey.Split("_")(0)

420:            If useKey.StartsWith("lk") = False Then '//not a Loki key
430:                If purchasedKeys.IndexOf(purchasedKeys, useKey) = -1 Then
440:                    violatedKeys.Add(useKey)
                    End If
450:            Else
                    Dim lokiPurchased As Boolean = False

460:                For Each k As String In purchasedKeys
470:                    If k.StartsWith("lk") Then
480:                        lokiPurchased = True
490:                        Exit For
                        End If
500:                Next

510:                If lokiPurchased = False Then
520:                    violatedKeys.Add("lk")
                    End If
                End If
530:        Next

540:        If Not noAlert Then showscanner.Label3.Text = "Discovering schedules..."

550:        Application.DoEvents()

560:        If violatedKeys.Count > 0 Then
570:            disableViolatingSchedules(xds, violatedKeys, xdsLoc)
            End If

            'scanner = Nothing

580:        If Not noAlert Then showscanner.Close()
590:    Catch ex As Exception
600:        _ErrorHandle(ex.Message, Erl, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try
    End Sub


    Private Sub disableViolatingSchedules(ByVal ds As DataSet, ByVal violatedKeys As ArrayList, ByVal datasetLocation As String)
        Try
            Dim scheduleid As Integer
            Dim scheduleName As String
            Dim key As String
            Dim scheduletype As Integer
            Dim SQL As String
            Dim dt As DataTable
            Dim violatedKeysString As String = ""
            Dim resultsTableLoc As String = Environment.GetEnvironmentVariable("TEMP") & "\resultsTable.xml"

            If IO.File.Exists(resultsTableLoc) Then IO.File.Delete(resultsTableLoc)

            For Each violatedKey As String In violatedKeys
                violatedKeysString &= violatedKey & ","
            Next

            '//convert the array to a string
            If violatedKeysString.EndsWith(",") Then violatedKeysString = violatedKeysString.Remove(violatedKeysString.Length - 1, 1)

            '//create the transport table
            Dim transportTable As DataTable = transportTableDef()
            Dim trow As DataRow = transportTable.Rows.Add
            trow("processtype") = "getschedulesthatuse"
            trow("application") = "sqlrd"
            trow("datasetlocation") = datasetLocation
            trow("outputlocation") = resultsTableLoc
            trow("violatedkeys") = violatedKeysString

            '//save the transport table
            Dim transportTableLoc As String = Environment.GetEnvironmentVariable("TEMP") & "\transportTable.xml"
            If IO.File.Exists(transportTableLoc) Then IO.File.Delete(transportTableLoc)
            transportTable.WriteXml(transportTableLoc, XmlWriteMode.WriteSchema)

            '//perform the scan out of process
            Dim pscanner As Process = New Process
            pscanner.StartInfo.FileName = Application.StartupPath & "\CSS Scanner.exe"
            pscanner.StartInfo.Arguments = transportTableLoc
            pscanner.Start()
            pscanner.WaitForExit()

            System.Threading.Thread.Sleep(2000)

            If IO.File.Exists(resultsTableLoc) Then
                dt = New DataTable
                dt.ReadXml(resultsTableLoc)
            End If

            If dt.Rows.Count = 0 Then Return

            For Each row As DataRow In dt.Rows
                scheduleid = row("scheduleid")

                Dim tbl As DataTable = ds.Tables("scheduleattr")

                If tbl.Select("scheduleid =" & scheduleid).Length > 0 Then
                    SQL = "UPDATE scheduleattr SET [status] = 0, locked = 1 WHERE scheduleid =" & scheduleid
                Else
                    SQL = "UPDATE eventattr6 SET [status] = 0, locked = 1 WHERE eventid =" & scheduleid
                End If

                clsMarsData.WriteData(SQL, False)
            Next


            Dim res As DialogResult

            If noAlert Then
                res = DialogResult.No
            Else
                res = MessageBox.Show(Application.ProductName & " has detected usage of functionality that has not been purchased. All the schedules that are " & _
                                                       "currently using unpurchased functionality will be disabled and locked. We apologize for the inconvience." & vbCrLf & _
                                                       "Would you like to contact your account manager to review your options?", Application.ProductName, _
                                                        MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
            End If

            If res = DialogResult.Yes Then
                Try
                    Process.Start("mailto:sales@christiansteven.com?subject=Help with locked schedules: " & CustID)
                Catch ex As Exception
                    _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 0, "Please install an e-mail client")
                End Try
            End If

            If noAlert = False Then
                Dim resultForm As frmResults = New frmResults

                resultForm.viewResults(dt)
            End If

            dt.TableName = "violatingSchedules"

            dt.WriteXml(Application.StartupPath & "\vls.xml", XmlWriteMode.WriteSchema)
        Catch ex As Exception
            If noAlert Then Return Else Throw ex
        End Try
    End Sub
    

    Private Function customKeyforEdition() As String
        Dim customKey As String = ""

        Select Case gnEdition
            Case gEdition.PLATINUM
                customKey = "s1|ca1|f1|fr1|fr2|sa5|sa7|g1"
            Case gEdition.ENTERPRISE
                customKey = "s1|ca1|ca4|ca5|i1|m1|m2|p1|f1|x1|fr1|fr2|sa3|sa5|sa6|sa7|g1"
            Case gEdition.ENTERPRISEPRO
                customKey = "s1|s2|ca1|ca2|ca3|ca4|ca5|i1|m1|m2|p1|f1|x1|fr1|fr2|sa3|sa4|sa5|sa6|sa7|sa9|cl1|g1"
            Case gEdition.ENTERPRISEPROPLUS
                customKey = "s1|s2|s3|s4|s5|ca1|ca2|ca3|ca4|ca5|ca6|i1|e1|m1|m2|m3|p1|f1|x1|o1|o2|o3|fr1|fr2|xl1|pd1|sa1|sa3|sa4|sa5|sa6|sa7|sa8|sa9|cl1|g1"
            Case Else

        End Select

        Return customKey
    End Function
End Class
