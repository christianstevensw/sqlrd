﻿Public Class clsDefaultParameter
    Dim oRs As ADODB.Recordset

    Sub New(ByVal parameterName As String)
        oRs = clsMarsData.GetData("SELECT * FROM DefaultParameterValues WHERE parametername ='" & SQLPrepare(parameterName) & "'")
    End Sub

    Public Shared ReadOnly Property AdminCanEditDefaultParameters() As Boolean
        Get
            If gRole = "Administrator" Then
                Return Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("AllowAdminsToSetDefaultParameters", 0))
            Else
                '//get the groupid
                Dim grp As usergroup = New usergroup(gRole)

                Return grp.IsGroupAllowedAccessTo("Over-ride Default Parameters")
            End If
        End Get
    End Property

    ''' <summary>
    ''' creates or updates a default parameter
    ''' </summary>
    ''' <param name="parameterName"></param>
    ''' <param name="parameterValue"></param>
    ''' <remarks></remarks>
    Public Shared Function setDefaultParameter(ByVal parameterName As String, ByVal parameterValue As String) As Boolean
        '//lets see if it exists
        Dim exists As Boolean = False
        Dim testRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM DefaultParameterValues WHERE parametername ='" & SQLPrepare(parameterName) & "'")

        If testRs IsNot Nothing AndAlso testRs.EOF = False Then
            exists = True
            testRs.Close()
        Else
            exists = False
        End If

        If exists = False Then
            Dim cols, vals As String

            cols = "parametername, parametervalue"

            vals = "'" & SQLPrepare(parameterName) & "'," & _
                "'" & SQLPrepare(parameterValue) & "'"

            clsMarsData.DataItem.InsertData("DefaultParameterValues", cols, vals)
        Else
            clsMarsData.WriteData("UPDATE DefaultParameterValues SET parameterValue = '" & SQLPrepare(parameterValue) & "' WHERE parametername ='" & SQLPrepare(parameterName) & "'")
        End If

        Return True
    End Function

    Public ReadOnly Property hasValue() As Boolean
        Get
            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property
    Public ReadOnly Property parameterValue() As String
        Get
            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                Return oRs("parametervalue").Value
            Else
                Return ""
            End If
        End Get
    End Property

    Public Sub dispose()
        oRs.Close()
        oRs = Nothing
    End Sub

End Class
