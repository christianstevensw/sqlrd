<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEventWizard6
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEventWizard6))
        Me.Step2 = New System.Windows.Forms.Panel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.chkStatus = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.UcConditions1 = New sqlrd.ucConditions()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.cmbAnyAll = New System.Windows.Forms.ComboBox()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.Step3 = New System.Windows.Forms.Panel()
        Me.pnlMaxthreads = New System.Windows.Forms.FlowLayoutPanel()
        Me.chkExistingAsync = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtMaxthreads = New DevComponents.Editors.IntegerInput()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.optNewReport = New System.Windows.Forms.RadioButton()
        Me.optNone = New System.Windows.Forms.RadioButton()
        Me.optExisting = New System.Windows.Forms.RadioButton()
        Me.Step5 = New System.Windows.Forms.Panel()
        Me.UcReports1 = New sqlrd.ucReports()
        Me.Step7 = New System.Windows.Forms.Panel()
        Me.UcTasks1 = New sqlrd.ucTasks()
        Me.cmdNext = New DevComponents.DotNetBar.ButtonX()
        Me.cmdFinish = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.Step8 = New System.Windows.Forms.Panel()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.ucFlow = New sqlrd.ucExecutionPath()
        Me.Step6 = New System.Windows.Forms.Panel()
        Me.cmbPriority = New System.Windows.Forms.ComboBox()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.DividerLabel5 = New sqlrd.DividerLabel()
        Me.cmbOpHrs = New System.Windows.Forms.ComboBox()
        Me.chkOpHrs = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.DividerLabel4 = New sqlrd.DividerLabel()
        Me.DividerLabel3 = New sqlrd.DividerLabel()
        Me.UcError = New sqlrd.ucErrorHandler()
        Me.mnuParameters = New System.Windows.Forms.ContextMenu()
        Me.mnuParameter = New System.Windows.Forms.MenuItem()
        Me.stabMain = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabReports = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.UcExisting = New sqlrd.ucExistingReports()
        Me.tabExistingReports = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.ReflectionImage1 = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.txtLocation = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtKeyword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btnBrowse = New DevComponents.DotNetBar.ButtonX()
        Me.txtDescription = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.tabGeneral = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel5 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabNewReports = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabConditions = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel8 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabExecutionFlow = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel7 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabTasks = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel6 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabException = New DevComponents.DotNetBar.SuperTabItem()
        Me.Step2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Step3.SuspendLayout()
        Me.pnlMaxthreads.SuspendLayout()
        CType(Me.txtMaxthreads, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step5.SuspendLayout()
        Me.Step7.SuspendLayout()
        Me.Step8.SuspendLayout()
        Me.Step6.SuspendLayout()
        CType(Me.stabMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabMain.SuspendLayout()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.SuperTabControlPanel4.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.SuperTabControlPanel5.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuperTabControlPanel8.SuspendLayout()
        Me.SuperTabControlPanel7.SuspendLayout()
        Me.SuperTabControlPanel6.SuspendLayout()
        Me.SuspendLayout()
        '
        'Step2
        '
        Me.Step2.BackColor = System.Drawing.Color.Transparent
        Me.Step2.Controls.Add(Me.FlowLayoutPanel1)
        Me.Step2.Controls.Add(Me.UcConditions1)
        Me.Step2.Controls.Add(Me.TableLayoutPanel1)
        Me.Step2.Location = New System.Drawing.Point(3, 3)
        Me.Step2.Name = "Step2"
        Me.Step2.Size = New System.Drawing.Size(538, 373)
        Me.Step2.TabIndex = 4
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.chkStatus)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 346)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(538, 27)
        Me.FlowLayoutPanel1.TabIndex = 3
        '
        'chkStatus
        '
        Me.chkStatus.AutoSize = True
        '
        '
        '
        Me.chkStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkStatus.Checked = True
        Me.chkStatus.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkStatus.CheckValue = "Y"
        Me.chkStatus.Location = New System.Drawing.Point(3, 3)
        Me.chkStatus.Name = "chkStatus"
        Me.chkStatus.Size = New System.Drawing.Size(61, 16)
        Me.chkStatus.TabIndex = 0
        Me.chkStatus.Text = "Enabled"
        '
        'UcConditions1
        '
        Me.UcConditions1.Dock = System.Windows.Forms.DockStyle.Top
        Me.UcConditions1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UcConditions1.Location = New System.Drawing.Point(0, 26)
        Me.UcConditions1.m_EventID = 99999
        Me.UcConditions1.m_OrderID = 0
        Me.UcConditions1.Name = "UcConditions1"
        Me.UcConditions1.Size = New System.Drawing.Size(538, 317)
        Me.UcConditions1.TabIndex = 1
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.23622!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.11927!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.78899!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.cmbAnyAll, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 2, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(538, 26)
        Me.TableLayoutPanel1.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(37, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Fulfill"
        '
        'cmbAnyAll
        '
        Me.cmbAnyAll.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmbAnyAll.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbAnyAll.FormattingEnabled = True
        Me.cmbAnyAll.Items.AddRange(New Object() {"ANY", "ALL"})
        Me.cmbAnyAll.Location = New System.Drawing.Point(57, 3)
        Me.cmbAnyAll.Name = "cmbAnyAll"
        Me.cmbAnyAll.Size = New System.Drawing.Size(64, 21)
        Me.cmbAnyAll.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.Location = New System.Drawing.Point(154, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(195, 20)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "of the following conditions"
        '
        'Step3
        '
        Me.Step3.BackColor = System.Drawing.Color.Transparent
        Me.Step3.Controls.Add(Me.pnlMaxthreads)
        Me.Step3.Controls.Add(Me.optNewReport)
        Me.Step3.Controls.Add(Me.optNone)
        Me.Step3.Controls.Add(Me.optExisting)
        Me.Step3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Step3.Location = New System.Drawing.Point(0, 0)
        Me.Step3.Name = "Step3"
        Me.Step3.Size = New System.Drawing.Size(552, 379)
        Me.Step3.TabIndex = 5
        '
        'pnlMaxthreads
        '
        Me.pnlMaxthreads.Controls.Add(Me.chkExistingAsync)
        Me.pnlMaxthreads.Controls.Add(Me.txtMaxthreads)
        Me.pnlMaxthreads.Controls.Add(Me.Label5)
        Me.pnlMaxthreads.Enabled = False
        Me.pnlMaxthreads.Location = New System.Drawing.Point(131, 25)
        Me.pnlMaxthreads.Name = "pnlMaxthreads"
        Me.pnlMaxthreads.Size = New System.Drawing.Size(413, 25)
        Me.pnlMaxthreads.TabIndex = 3
        '
        'chkExistingAsync
        '
        '
        '
        '
        Me.chkExistingAsync.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkExistingAsync.Location = New System.Drawing.Point(3, 3)
        Me.chkExistingAsync.Name = "chkExistingAsync"
        Me.chkExistingAsync.Size = New System.Drawing.Size(247, 23)
        Me.chkExistingAsync.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkExistingAsync.TabIndex = 2
        Me.chkExistingAsync.Text = "Execute schedules asyncronously. Use up to "
        '
        'txtMaxthreads
        '
        '
        '
        '
        Me.txtMaxthreads.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtMaxthreads.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtMaxthreads.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.txtMaxthreads.Enabled = False
        Me.txtMaxthreads.Location = New System.Drawing.Point(256, 3)
        Me.txtMaxthreads.MaxValue = 8
        Me.txtMaxthreads.MinValue = 2
        Me.txtMaxthreads.Name = "txtMaxthreads"
        Me.txtMaxthreads.ShowUpDown = True
        Me.txtMaxthreads.Size = New System.Drawing.Size(49, 21)
        Me.txtMaxthreads.TabIndex = 3
        Me.txtMaxthreads.Value = 2
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(311, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(56, 26)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "threads"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'optNewReport
        '
        Me.optNewReport.AutoSize = True
        Me.optNewReport.Checked = True
        Me.optNewReport.Location = New System.Drawing.Point(13, 9)
        Me.optNewReport.Name = "optNewReport"
        Me.optNewReport.Size = New System.Drawing.Size(87, 17)
        Me.optNewReport.TabIndex = 1
        Me.optNewReport.TabStop = True
        Me.optNewReport.Text = "New Reports"
        Me.optNewReport.UseVisualStyleBackColor = True
        '
        'optNone
        '
        Me.optNone.AutoSize = True
        Me.optNone.Location = New System.Drawing.Point(13, 55)
        Me.optNone.Name = "optNone"
        Me.optNone.Size = New System.Drawing.Size(50, 17)
        Me.optNone.TabIndex = 0
        Me.optNone.Text = "None"
        Me.optNone.UseVisualStyleBackColor = True
        '
        'optExisting
        '
        Me.optExisting.AutoSize = True
        Me.optExisting.Location = New System.Drawing.Point(13, 32)
        Me.optExisting.Name = "optExisting"
        Me.optExisting.Size = New System.Drawing.Size(112, 17)
        Me.optExisting.TabIndex = 0
        Me.optExisting.Text = "Existing schedules"
        Me.optExisting.UseVisualStyleBackColor = True
        '
        'Step5
        '
        Me.Step5.BackColor = System.Drawing.Color.Transparent
        Me.Step5.Controls.Add(Me.UcReports1)
        Me.Step5.Location = New System.Drawing.Point(3, 3)
        Me.Step5.Name = "Step5"
        Me.Step5.Size = New System.Drawing.Size(546, 373)
        Me.Step5.TabIndex = 7
        '
        'UcReports1
        '
        Me.UcReports1.eventBased = True
        Me.UcReports1.Location = New System.Drawing.Point(3, 3)
        Me.UcReports1.m_MasterID = 99999
        Me.UcReports1.Name = "UcReports1"
        Me.UcReports1.Size = New System.Drawing.Size(540, 367)
        Me.UcReports1.TabIndex = 0
        '
        'Step7
        '
        Me.Step7.BackColor = System.Drawing.Color.Transparent
        Me.Step7.Controls.Add(Me.UcTasks1)
        Me.Step7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Step7.Location = New System.Drawing.Point(0, 0)
        Me.Step7.Name = "Step7"
        Me.Step7.Size = New System.Drawing.Size(537, 379)
        Me.Step7.TabIndex = 8
        '
        'UcTasks1
        '
        Me.UcTasks1.BackColor = System.Drawing.Color.Transparent
        Me.UcTasks1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcTasks1.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcTasks1.ForeColor = System.Drawing.Color.Navy
        Me.UcTasks1.Location = New System.Drawing.Point(0, 0)
        Me.UcTasks1.m_defaultTaks = False
        Me.UcTasks1.m_eventBased = False
        Me.UcTasks1.m_eventID = 99999
        Me.UcTasks1.m_forExceptionHandling = False
        Me.UcTasks1.m_showAfterType = True
        Me.UcTasks1.m_showExpanded = True
        Me.UcTasks1.Name = "UcTasks1"
        Me.UcTasks1.Size = New System.Drawing.Size(537, 379)
        Me.UcTasks1.TabIndex = 0
        '
        'cmdNext
        '
        Me.cmdNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdNext.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdNext.Enabled = False
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(521, 385)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(75, 23)
        Me.cmdNext.TabIndex = 23
        Me.cmdNext.Text = "&Next"
        '
        'cmdFinish
        '
        Me.cmdFinish.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdFinish.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdFinish.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.cmdFinish.Enabled = False
        Me.cmdFinish.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdFinish.Location = New System.Drawing.Point(602, 385)
        Me.cmdFinish.Name = "cmdFinish"
        Me.cmdFinish.Size = New System.Drawing.Size(75, 23)
        Me.cmdFinish.TabIndex = 26
        Me.cmdFinish.Text = "&Finish"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(438, 385)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 25
        Me.cmdCancel.Text = "&Cancel"
        '
        'Step8
        '
        Me.Step8.BackColor = System.Drawing.Color.Transparent
        Me.Step8.Controls.Add(Me.Label3)
        Me.Step8.Controls.Add(Me.ucFlow)
        Me.Step8.Location = New System.Drawing.Point(3, 3)
        Me.Step8.Name = "Step8"
        Me.Step8.Size = New System.Drawing.Size(546, 376)
        Me.Step8.TabIndex = 29
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.Location = New System.Drawing.Point(9, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(235, 16)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Please select the schedule's execution flow type"
        '
        'ucFlow
        '
        Me.ucFlow.BackColor = System.Drawing.Color.Transparent
        Me.ucFlow.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ucFlow.Location = New System.Drawing.Point(3, 52)
        Me.ucFlow.m_ExecutionFlow = "AFTER"
        Me.ucFlow.Name = "ucFlow"
        Me.ucFlow.Size = New System.Drawing.Size(427, 171)
        Me.ucFlow.TabIndex = 0
        '
        'Step6
        '
        Me.Step6.BackColor = System.Drawing.Color.Transparent
        Me.Step6.Controls.Add(Me.cmbPriority)
        Me.Step6.Controls.Add(Me.Label4)
        Me.Step6.Controls.Add(Me.DividerLabel5)
        Me.Step6.Controls.Add(Me.cmbOpHrs)
        Me.Step6.Controls.Add(Me.chkOpHrs)
        Me.Step6.Controls.Add(Me.DividerLabel4)
        Me.Step6.Controls.Add(Me.DividerLabel3)
        Me.Step6.Controls.Add(Me.UcError)
        Me.Step6.Location = New System.Drawing.Point(3, 3)
        Me.Step6.Name = "Step6"
        Me.Step6.Size = New System.Drawing.Size(436, 273)
        Me.Step6.TabIndex = 30
        '
        'cmbPriority
        '
        Me.cmbPriority.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPriority.FormattingEnabled = True
        Me.cmbPriority.Items.AddRange(New Object() {"1 - High", "2 - Above Normal", "3 - Normal", "4 - Below Normal", "5 - Low "})
        Me.cmbPriority.Location = New System.Drawing.Point(209, 170)
        Me.cmbPriority.Name = "cmbPriority"
        Me.cmbPriority.Size = New System.Drawing.Size(218, 21)
        Me.cmbPriority.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        '
        '
        '
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.Location = New System.Drawing.Point(2, 173)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(172, 16)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Set the priority of this schedules to"
        '
        'DividerLabel5
        '
        Me.DividerLabel5.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel5.Location = New System.Drawing.Point(3, 154)
        Me.DividerLabel5.Name = "DividerLabel5"
        Me.DividerLabel5.Size = New System.Drawing.Size(429, 19)
        Me.DividerLabel5.Spacing = 0
        Me.DividerLabel5.TabIndex = 5
        Me.DividerLabel5.Text = "Schedule Priority"
        '
        'cmbOpHrs
        '
        Me.cmbOpHrs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOpHrs.Enabled = False
        Me.cmbOpHrs.FormattingEnabled = True
        Me.cmbOpHrs.Location = New System.Drawing.Point(209, 119)
        Me.cmbOpHrs.Name = "cmbOpHrs"
        Me.cmbOpHrs.Size = New System.Drawing.Size(218, 21)
        Me.cmbOpHrs.TabIndex = 4
        '
        'chkOpHrs
        '
        Me.chkOpHrs.AutoSize = True
        '
        '
        '
        Me.chkOpHrs.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkOpHrs.Location = New System.Drawing.Point(6, 119)
        Me.chkOpHrs.Name = "chkOpHrs"
        Me.chkOpHrs.Size = New System.Drawing.Size(172, 16)
        Me.chkOpHrs.TabIndex = 3
        Me.chkOpHrs.Text = "Use custom hours of operation"
        '
        'DividerLabel4
        '
        Me.DividerLabel4.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel4.Location = New System.Drawing.Point(3, 92)
        Me.DividerLabel4.Name = "DividerLabel4"
        Me.DividerLabel4.Size = New System.Drawing.Size(429, 19)
        Me.DividerLabel4.Spacing = 0
        Me.DividerLabel4.TabIndex = 2
        Me.DividerLabel4.Text = "Hours of Operation"
        '
        'DividerLabel3
        '
        Me.DividerLabel3.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel3.Location = New System.Drawing.Point(3, 9)
        Me.DividerLabel3.Name = "DividerLabel3"
        Me.DividerLabel3.Size = New System.Drawing.Size(429, 13)
        Me.DividerLabel3.Spacing = 0
        Me.DividerLabel3.TabIndex = 1
        Me.DividerLabel3.Text = "Exception Handling"
        '
        'UcError
        '
        Me.UcError.BackColor = System.Drawing.Color.Transparent
        Me.UcError.Location = New System.Drawing.Point(3, 23)
        Me.UcError.m_showFailOnOne = False
        Me.UcError.Name = "UcError"
        Me.UcError.Size = New System.Drawing.Size(430, 62)
        Me.UcError.TabIndex = 0
        '
        'mnuParameters
        '
        Me.mnuParameters.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuParameter})
        '
        'mnuParameter
        '
        Me.mnuParameter.Index = 0
        Me.mnuParameter.Text = "Parameter Substitution"
        '
        'stabMain
        '
        Me.stabMain.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.stabMain.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabMain.ControlBox.MenuBox.Name = ""
        Me.stabMain.ControlBox.Name = ""
        Me.stabMain.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabMain.ControlBox.MenuBox, Me.stabMain.ControlBox.CloseBox})
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel6)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel5)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel4)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel3)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel1)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel2)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel8)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel7)
        Me.stabMain.Dock = System.Windows.Forms.DockStyle.Top
        Me.stabMain.ForeColor = System.Drawing.Color.Black
        Me.stabMain.Location = New System.Drawing.Point(0, 0)
        Me.stabMain.Name = "stabMain"
        Me.stabMain.ReorderTabsEnabled = True
        Me.stabMain.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabMain.SelectedTabIndex = 0
        Me.stabMain.Size = New System.Drawing.Size(688, 379)
        Me.stabMain.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.stabMain.TabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.stabMain.TabIndex = 31
        Me.stabMain.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabGeneral, Me.tabConditions, Me.tabReports, Me.tabExistingReports, Me.tabNewReports, Me.tabException, Me.tabTasks, Me.tabExecutionFlow})
        Me.stabMain.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.Step3)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(552, 379)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.tabReports
        '
        'tabReports
        '
        Me.tabReports.AttachedControl = Me.SuperTabControlPanel3
        Me.tabReports.GlobalItem = False
        Me.tabReports.Image = CType(resources.GetObject("tabReports.Image"), System.Drawing.Image)
        Me.tabReports.Name = "tabReports"
        Me.tabReports.Text = "Report Type"
        Me.tabReports.Visible = False
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.UcExisting)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(552, 379)
        Me.SuperTabControlPanel4.TabIndex = 0
        Me.SuperTabControlPanel4.TabItem = Me.tabExistingReports
        '
        'UcExisting
        '
        Me.UcExisting.BackColor = System.Drawing.Color.Transparent
        Me.UcExisting.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcExisting.Location = New System.Drawing.Point(0, 0)
        Me.UcExisting.m_eventID = 99999
        Me.UcExisting.Name = "UcExisting"
        Me.UcExisting.Size = New System.Drawing.Size(552, 379)
        Me.UcExisting.TabIndex = 0
        '
        'tabExistingReports
        '
        Me.tabExistingReports.AttachedControl = Me.SuperTabControlPanel4
        Me.tabExistingReports.GlobalItem = False
        Me.tabExistingReports.Image = CType(resources.GetObject("tabExistingReports.Image"), System.Drawing.Image)
        Me.tabExistingReports.Name = "tabExistingReports"
        Me.tabExistingReports.Text = "Existing Reports"
        Me.tabExistingReports.Visible = False
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.ReflectionImage1)
        Me.SuperTabControlPanel1.Controls.Add(Me.TableLayoutPanel2)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(552, 379)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabGeneral
        '
        'ReflectionImage1
        '
        Me.ReflectionImage1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.ReflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ReflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.ReflectionImage1.Image = CType(resources.GetObject("ReflectionImage1.Image"), System.Drawing.Image)
        Me.ReflectionImage1.Location = New System.Drawing.Point(413, 76)
        Me.ReflectionImage1.Name = "ReflectionImage1"
        Me.ReflectionImage1.Size = New System.Drawing.Size(128, 218)
        Me.ReflectionImage1.TabIndex = 2
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel2.ColumnCount = 3
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.61693!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 72.38307!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.LabelX1, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.LabelX3, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.LabelX4, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.LabelX5, 0, 5)
        Me.TableLayoutPanel2.Controls.Add(Me.txtLocation, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txtName, 1, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.txtKeyword, 1, 5)
        Me.TableLayoutPanel2.Controls.Add(Me.btnBrowse, 2, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txtDescription, 1, 3)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(4, 8)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 6
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(514, 317)
        Me.TableLayoutPanel2.TabIndex = 1
        '
        'LabelX1
        '
        Me.LabelX1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(3, 3)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(100, 25)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "Parent folder"
        '
        'LabelX3
        '
        Me.LabelX3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Location = New System.Drawing.Point(3, 34)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(100, 25)
        Me.LabelX3.TabIndex = 0
        Me.LabelX3.Text = "Schedule Name"
        '
        'LabelX4
        '
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.Location = New System.Drawing.Point(3, 65)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(100, 19)
        Me.LabelX4.TabIndex = 0
        Me.LabelX4.Text = "Description (optional)"
        '
        'LabelX5
        '
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.Location = New System.Drawing.Point(3, 263)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(100, 20)
        Me.LabelX5.TabIndex = 0
        Me.LabelX5.Text = "Keywords (optional)"
        '
        'txtLocation
        '
        Me.txtLocation.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtLocation.Border.Class = "TextBoxBorder"
        Me.txtLocation.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtLocation.ForeColor = System.Drawing.Color.Black
        Me.txtLocation.Location = New System.Drawing.Point(109, 3)
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.Size = New System.Drawing.Size(271, 21)
        Me.txtLocation.TabIndex = 0
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.ForeColor = System.Drawing.Color.Black
        Me.txtName.Location = New System.Drawing.Point(109, 34)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(271, 21)
        Me.txtName.TabIndex = 4
        '
        'txtKeyword
        '
        Me.txtKeyword.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtKeyword.Border.Class = "TextBoxBorder"
        Me.txtKeyword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKeyword.ForeColor = System.Drawing.Color.Black
        Me.txtKeyword.Location = New System.Drawing.Point(109, 263)
        Me.txtKeyword.Name = "txtKeyword"
        Me.txtKeyword.Size = New System.Drawing.Size(271, 21)
        Me.txtKeyword.TabIndex = 6
        '
        'btnBrowse
        '
        Me.btnBrowse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnBrowse.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnBrowse.Location = New System.Drawing.Point(386, 3)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(59, 21)
        Me.btnBrowse.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnBrowse.TabIndex = 1
        Me.btnBrowse.Text = "..."
        '
        'txtDescription
        '
        Me.txtDescription.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDescription.Border.Class = "TextBoxBorder"
        Me.txtDescription.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TableLayoutPanel2.SetColumnSpan(Me.txtDescription, 2)
        Me.txtDescription.ForeColor = System.Drawing.Color.Black
        Me.txtDescription.Location = New System.Drawing.Point(109, 65)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(274, 192)
        Me.txtDescription.TabIndex = 5
        '
        'tabGeneral
        '
        Me.tabGeneral.AttachedControl = Me.SuperTabControlPanel1
        Me.tabGeneral.GlobalItem = False
        Me.tabGeneral.Image = CType(resources.GetObject("tabGeneral.Image"), System.Drawing.Image)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Text = "General"
        '
        'SuperTabControlPanel5
        '
        Me.SuperTabControlPanel5.Controls.Add(Me.Step5)
        Me.SuperTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel5.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel5.Name = "SuperTabControlPanel5"
        Me.SuperTabControlPanel5.Size = New System.Drawing.Size(552, 379)
        Me.SuperTabControlPanel5.TabIndex = 0
        Me.SuperTabControlPanel5.TabItem = Me.tabNewReports
        '
        'tabNewReports
        '
        Me.tabNewReports.AttachedControl = Me.SuperTabControlPanel5
        Me.tabNewReports.GlobalItem = False
        Me.tabNewReports.Image = CType(resources.GetObject("tabNewReports.Image"), System.Drawing.Image)
        Me.tabNewReports.Name = "tabNewReports"
        Me.tabNewReports.Text = "New Reports"
        Me.tabNewReports.Visible = False
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.Step2)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(151, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(537, 379)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.tabConditions
        '
        'tabConditions
        '
        Me.tabConditions.AttachedControl = Me.SuperTabControlPanel2
        Me.tabConditions.GlobalItem = False
        Me.tabConditions.Image = CType(resources.GetObject("tabConditions.Image"), System.Drawing.Image)
        Me.tabConditions.Name = "tabConditions"
        Me.tabConditions.Text = "Conditions"
        Me.tabConditions.Visible = False
        '
        'SuperTabControlPanel8
        '
        Me.SuperTabControlPanel8.Controls.Add(Me.Step8)
        Me.SuperTabControlPanel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel8.Location = New System.Drawing.Point(151, 0)
        Me.SuperTabControlPanel8.Name = "SuperTabControlPanel8"
        Me.SuperTabControlPanel8.Size = New System.Drawing.Size(537, 379)
        Me.SuperTabControlPanel8.TabIndex = 0
        Me.SuperTabControlPanel8.TabItem = Me.tabExecutionFlow
        '
        'tabExecutionFlow
        '
        Me.tabExecutionFlow.AttachedControl = Me.SuperTabControlPanel8
        Me.tabExecutionFlow.GlobalItem = False
        Me.tabExecutionFlow.Image = CType(resources.GetObject("tabExecutionFlow.Image"), System.Drawing.Image)
        Me.tabExecutionFlow.Name = "tabExecutionFlow"
        Me.tabExecutionFlow.Text = "Execution Flow"
        Me.tabExecutionFlow.Visible = False
        '
        'SuperTabControlPanel7
        '
        Me.SuperTabControlPanel7.Controls.Add(Me.Step7)
        Me.SuperTabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel7.Location = New System.Drawing.Point(151, 0)
        Me.SuperTabControlPanel7.Name = "SuperTabControlPanel7"
        Me.SuperTabControlPanel7.Size = New System.Drawing.Size(537, 379)
        Me.SuperTabControlPanel7.TabIndex = 0
        Me.SuperTabControlPanel7.TabItem = Me.tabTasks
        '
        'tabTasks
        '
        Me.tabTasks.AttachedControl = Me.SuperTabControlPanel7
        Me.tabTasks.GlobalItem = False
        Me.tabTasks.Image = CType(resources.GetObject("tabTasks.Image"), System.Drawing.Image)
        Me.tabTasks.Name = "tabTasks"
        Me.tabTasks.Text = "Custom Tasks"
        Me.tabTasks.Visible = False
        '
        'SuperTabControlPanel6
        '
        Me.SuperTabControlPanel6.Controls.Add(Me.Step6)
        Me.SuperTabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel6.Location = New System.Drawing.Point(151, 0)
        Me.SuperTabControlPanel6.Name = "SuperTabControlPanel6"
        Me.SuperTabControlPanel6.Size = New System.Drawing.Size(537, 379)
        Me.SuperTabControlPanel6.TabIndex = 0
        Me.SuperTabControlPanel6.TabItem = Me.tabException
        '
        'tabException
        '
        Me.tabException.AttachedControl = Me.SuperTabControlPanel6
        Me.tabException.GlobalItem = False
        Me.tabException.Image = CType(resources.GetObject("tabException.Image"), System.Drawing.Image)
        Me.tabException.Name = "tabException"
        Me.tabException.Text = "Exception Handling"
        Me.tabException.Visible = False
        '
        'frmEventWizard6
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(688, 416)
        Me.ControlBox = False
        Me.Controls.Add(Me.stabMain)
        Me.Controls.Add(Me.cmdNext)
        Me.Controls.Add(Me.cmdFinish)
        Me.Controls.Add(Me.cmdCancel)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmEventWizard6"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Event-Based Schedule"
        Me.Step2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Step3.ResumeLayout(False)
        Me.Step3.PerformLayout()
        Me.pnlMaxthreads.ResumeLayout(False)
        CType(Me.txtMaxthreads, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step5.ResumeLayout(False)
        Me.Step7.ResumeLayout(False)
        Me.Step8.ResumeLayout(False)
        Me.Step8.PerformLayout()
        Me.Step6.ResumeLayout(False)
        Me.Step6.PerformLayout()
        CType(Me.stabMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabMain.ResumeLayout(False)
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.SuperTabControlPanel4.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel5.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel8.ResumeLayout(False)
        Me.SuperTabControlPanel7.ResumeLayout(False)
        Me.SuperTabControlPanel6.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Step2 As System.Windows.Forms.Panel
    Friend WithEvents Step3 As System.Windows.Forms.Panel
    Friend WithEvents optNewReport As System.Windows.Forms.RadioButton
    Friend WithEvents optExisting As System.Windows.Forms.RadioButton
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents Step5 As System.Windows.Forms.Panel
    Friend WithEvents UcReports1 As sqlrd.ucReports
    Friend WithEvents Step7 As System.Windows.Forms.Panel
    Friend WithEvents UcTasks1 As sqlrd.ucTasks
    Friend WithEvents cmdNext As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdFinish As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents UcConditions1 As sqlrd.ucConditions
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbAnyAll As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents chkStatus As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Step8 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents ucFlow As sqlrd.ucExecutionPath
    Friend WithEvents optNone As System.Windows.Forms.RadioButton
    Friend WithEvents Step6 As System.Windows.Forms.Panel
    Friend WithEvents UcError As sqlrd.ucErrorHandler
    Friend WithEvents DividerLabel3 As sqlrd.DividerLabel
    Friend WithEvents DividerLabel4 As sqlrd.DividerLabel
    Friend WithEvents chkOpHrs As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents cmbOpHrs As System.Windows.Forms.ComboBox
    Friend WithEvents mnuParameters As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuParameter As System.Windows.Forms.MenuItem
    Friend WithEvents DividerLabel5 As sqlrd.DividerLabel
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbPriority As System.Windows.Forms.ComboBox
    Friend WithEvents stabMain As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabGeneral As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel8 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabExecutionFlow As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel7 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabTasks As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel6 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabException As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel5 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabNewReports As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabExistingReports As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabReports As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabConditions As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtLocation As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtDescription As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtKeyword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnBrowse As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ReflectionImage1 As DevComponents.DotNetBar.Controls.ReflectionImage
    Friend WithEvents chkExistingAsync As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents UcExisting As sqlrd.ucExistingReports
    Friend WithEvents pnlMaxthreads As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtMaxthreads As DevComponents.Editors.IntegerInput
    Friend WithEvents Label5 As System.Windows.Forms.Label
End Class
