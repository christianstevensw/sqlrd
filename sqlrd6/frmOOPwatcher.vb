Imports System.Net
Imports System.Net.Sockets
Imports System.Text
Imports GlacialComponents.Controls.GlacialListView

Public Class frmOOPwatcher
    Dim img As ImageList
    Dim selItem() As ListViewItem
    Dim lastStatusUpdate As Date = Now

    Friend WithEvents dt As DataTable
    Dim listenThread As Threading.Thread


    Public Sub ListenForMessages()
        Try
            Do
                Try
                    Dim messageQ As ChristianStevenMessaging.messageQ

                    Try
                        messageQ = New ChristianStevenMessaging.messageQ("progress", False)
                    Catch
                        Return
                    End Try

                    Dim unreadMessages As ArrayList = messageQ.getUnreadMessages()

                    For Each msg As ChristianStevenMessaging.message In unreadMessages

                        Dim ID As String = msg.messageBody.Split("|")(0)
                        Dim message As String = msg.messageBody.Split("|")(1)
                        Dim value As Integer = msg.messageBody.Split("|")(2)

                        logDataFromClient(message, value, ID)

                        msg.deleteMessage()

                        msg.dispose()

                        System.Threading.Thread.Sleep(250)
                    Next

                    For Each msg As ChristianStevenMessaging.message In messageQ.getReadMessages
                        msg.deleteMessage()
                    Next

                    messageQ.dispose()

                    System.Threading.Thread.Sleep(250)

                    lastStatusUpdate = Now
                Catch : End Try
            Loop
        Catch : End Try
    End Sub
    Public Sub Listen()

        Dim tcpListener As New TcpListener(IPAddress.Any, 2476)

        Try
            tcpListener.Start()
        Catch
            Return
        End Try

        'Accept the pending client connection and return             'a TcpClient initialized for communication. 
        Dim tcpClient As TcpClient = tcpListener.AcceptTcpClient()

        Try

            Dim networkStream As NetworkStream = tcpClient.GetStream()

            ' Read the stream into a byte array
            Dim bytes(tcpClient.ReceiveBufferSize) As Byte

            networkStream.Read(bytes, 0, CInt(tcpClient.ReceiveBufferSize))

            Dim clientdata As String = Encoding.ASCII.GetString(bytes)

            'Close TcpListener and TcpClient.
            logDataFromClient(clientdata)

            tcpClient.Close()
            tcpListener.Stop()

            Dim oT As New Threading.Thread(AddressOf Listen)

            oT.Start()

        Catch : End Try

    End Sub

    Private Sub logDataFromClient(ByVal data As String)
        Dim ID As Integer = data.Split("|")(0)
        Dim msg As String = data.Split("|")(1)
        Dim value As Integer = data.Split("|")(2)

        Dim rows() As DataRow = crdxCommon.dtOOP.Select("ScheduleID =" & ID)

        For Each row As DataRow In rows
            row("Status") = msg
            row("StatusValue") = value
        Next

    End Sub

    Private Sub logDataFromClient(ByVal message As String, value As Integer, id As Integer)

        Dim rows() As DataRow = crdxCommon.dtOOP.Select("ScheduleID =" & id)

        For Each row As DataRow In rows
            row("Status") = message
            row("StatusValue") = value
        Next

    End Sub

    Private Sub tmCheck_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmCheck.Tick
        Try
            tmCheck.Stop()

            'remove exited ones
            cleanThreads()

            'add the new ones
            For Each row As DataRow In crdxCommon.dtOOP.Select("", "EntryDate ASC")
                Dim exists As Boolean = False
                Dim scheduleID As Integer = row("ScheduleID")

                For Each it As GLVItem In Me.gList.Items
                    If it.SubItems(4).Text = scheduleID Then
                        exists = True
                        Exit For
                    End If
                Next

                If exists = False Then

                    Dim oItem As GLVItem = New GLVItem

                    oItem.Text = row("Name") '0
                    oItem.SubItems.Add(row("Type")) '1

                    Dim progress As String = row("status") 'getProgress(row("scheduleid"))

                    Try
                        oItem.SubItems.Add(progress) 'progress(0) & "(" & progress(1) & "%)") '2
                    Catch ex As Exception
                        oItem.SubItems.Add(progress)
                    End Try

                    oItem.SubItems.Add(row("statusvalue")) '3

                    oItem.SubItems.Add(row("ScheduleID")) '4

                    oItem.Tag = row("PID")

                    Dim subit As GLVSubItem = oItem.SubItems(0)

                    ''console.writeline(row("type"))

                    Select Case row("Type")
                        Case "Single Schedule"
                            subit.ImageIndex = 6
                        Case "Package Schedule"
                            subit.ImageIndex = 4
                        Case "Dynamic Schedule"
                            subit.ImageIndex = 11
                        Case "Bursting Schedule"
                            subit.ImageIndex = 12
                        Case "Automation Schedule"
                            subit.ImageIndex = 7
                        Case "Event-Based Schedule"
                            subit.ImageIndex = 13
                        Case "Event-Based Package"
                            subit.ImageIndex = 16
                        Case "Data Driven Schedule"
                            subit.ImageIndex = 18
                    End Select

                    Me.gList.Items.Add(oItem)
                Else
                    Continue For
                End If

            Next

            'mark for uncomment
            ''update the existing ones
            For Each it As GLVItem In Me.gList.Items
                Dim scheduleID As Integer = it.SubItems(4).Text

                Dim rows() As DataRow = crdxCommon.dtOOP.Select("ScheduleID =" & scheduleID)

                If rows.Length > 0 Then
                    Dim row As DataRow = rows(0)
                    Dim nStatus As String = row("status")

                    it.SubItems(2).Text = row("status")

                    'update progress bar
                    Try
                        Dim p As ProgressBar = it.SubItems(3).Control

                        p.Value = row("statusvalue")
                    Catch : End Try

                    it.Tag = row("PID")
                End If

            Next

            'check if we should now exit
            If Me.gList.Items.Count = 0 Then
                If Me.Owner IsNot Nothing Then Me.Owner.Focus()
                Me.Visible = False
                Me.tmCheck.Enabled = False
                tmExecutor.Enabled = False

                Try
                    IO.File.Delete(sAppPath & "oopstatus.log")
                Catch : End Try


                If Me.Owner IsNot Nothing Then
                    Dim mainWin As frmMainWin = Me.Owner

                    If mainWin IsNot Nothing Then
                        mainWin.btnRefresh_Click(Nothing, Nothing)
                    End If
                End If

                If listenThread IsNot Nothing AndAlso listenThread.IsAlive Then
                    listenThread.Abort()
                End If

                Return
            Else
                If listenThread Is Nothing Then
                    listenThread = New Threading.Thread(AddressOf ListenForMessages)
                    listenThread.Start()
                Else
                    If Date.Now.Subtract(lastStatusUpdate).TotalSeconds > 5 Then
                        Try
                            listenThread.Abort()
                            listenThread = Nothing
                        Catch : End Try

                        listenThread = New Threading.Thread(AddressOf ListenForMessages)
                        listenThread.Start()

                        lastStatusUpdate = Now
                    End If
                End If
            End If
        Catch ex As Exception
        End Try

        tmCheck.Start()
    End Sub


    Private Function getProgress(ByVal ID As Integer) As String()
        Dim value As String = clsMarsUI.MainUI.ReadRegistry(ID, 0, , sAppPath & "oopstatus.log")

        Dim values As String() = value.Split("|")

        Return values
    End Function

    

   


    Private Sub frmOOPwatcher_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        sqlrd.MarsGlobal.FormatForWinXP(Me)


        clsMarsUI.MainUI.InitImages(Me.gList)

        Me.gList.Columns(3).EmbeddedControlTemplateType = GetType(ProgressBar)

        'Dim oT As New Threading.Thread(AddressOf Listen)

        'AddHandler crdxCommon.dtOOP.RowChanged, AddressOf dt_RowChanged

        listenThread = New Threading.Thread(AddressOf ListenForMessages)

        listenThread.Start()


    End Sub

    Private Sub TerminateProcessToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TerminateProcessToolStripMenuItem.Click
        If Me.gList.SelectedItems.Count = 0 Then Return

        Dim res As DialogResult = MessageBox.Show("Are you sure you would like to cancel the selected schedule execution(s)?", _
         Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If res = Windows.Forms.DialogResult.Yes Then
            For Each t As GLVItem In Me.gList.SelectedItems

                Dim pID As Integer = t.Tag

                Try
                    Dim p As Process = Process.GetProcessById(pID)
                    ScheduleStart = p.StartTime

                    p.Kill()

                    Dim type As String = t.SubItems(1).Text
                    Dim ID As String = t.SubItems(4).Text

                    Select Case type
                        Case "Single Schedule", "Dynamic Schedule", "Bursting Schedule"
                            clsMarsScheduler.globalItem.SetScheduleHistory(False, "Schedule execution cancelled by user", ID)
                        Case "Package Schedule"
                            clsMarsScheduler.globalItem.SetScheduleHistory(False, "Schedule execution cancelled by user", , ID)
                        Case "Automation Schedule"
                            clsMarsScheduler.globalItem.SetScheduleHistory(False, "Schedule execution cancelled by user", , , ID)
                        Case "Event-Based Package"
                            clsMarsScheduler.globalItem.SetScheduleHistory(False, "Schedule execution cancelled by user", , , , , ID)
                    End Select

                Catch : End Try

            Next
        End If
    End Sub




    Private Sub SelectAllToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SelectAllToolStripMenuItem.Click
        For Each it As GLVItem In Me.gList.Items
            it.Selected = True
        Next
    End Sub

    Private Sub glist_EmbeddedControlShow(ByVal source As Object, ByVal args As GlacialComponents.Controls.GlacialListView.GLVBeforeEditEventArgs)
        If args.Column = 3 Then
            Dim pb As ProgressBar = CType(args.Control, ProgressBar)
            pb.Minimum = 0
            pb.Maximum = 100
            pb.Value = Int32.Parse(args.Item.SubItems(args.Column).Text)
        End If
    End Sub


    Private Sub gList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Delete Then
            Me.TerminateProcessToolStripMenuItem_Click(Nothing, Nothing)
        End If

    End Sub

    Private Sub tmExecutor_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmExecutor.Tick
        Try
            ' If Process.GetProcessesByName("crd").GetLength(0) > 3 Then Return

            If crdxCommon.dtOOP Is Nothing Then Return

            If Me.bgExecutor.IsBusy = True Then Return

            Me.bgExecutor.RunWorkerAsync()
        Catch : End Try
    End Sub

    Private Function getNumberOfExecuting() As Integer
        Dim rows() As DataRow = crdxCommon.dtOOP.Select("PID <> 0")
        Dim count As Integer = 0

        For Each r As DataRow In rows
            Dim procID As Integer = r("pid")

            Try
                Dim p As Process = Process.GetProcessById(procID) '//check the process exists

                count += 1
            Catch : End Try
        Next

        Return count
    End Function

    Private Sub spawnThreads()
        Try
            Dim rows() As DataRow = crdxCommon.dtOOP.Select("PID = 0", "EntryDate ASC")

            If rows.Length = 0 Then Return

            '//select the top n rows
            Dim n As Integer = 1
            Dim multiThread As Boolean = clsMarsUI.MainUI.ReadRegistry("CRDThreading", 0)

            If multiThread = True Then
                If rows.Length < 3 Then
                    n = rows.Length
                Else
                    n = 4
                End If
            Else
                n = 1
            End If

            For Each row As DataRow In rows

                ' If Process.GetProcessesByName("crd").GetLength(0) > 3 Then Exit Sub
                If getNumberOfExecuting() > n Then Return

                Dim argument As String = row("argument")
                Dim p As Process = New Process

                p.StartInfo.FileName = sAppPath & sqlrd.assemblyName
                p.StartInfo.Arguments = argument & " " & sCon

                p.Start()

                Try
                    row("PID") = p.Id
                Catch
                    System.Threading.Thread.Sleep(1000)

                    Try
                        row("PID") = p.Id
                    Catch : End Try
                End Try

                System.Threading.Thread.Sleep(1000)

                If listenThread Is Nothing Then
                    listenThread = New Threading.Thread(AddressOf Listen)
                    listenThread.Start()
                ElseIf listenThread.IsAlive = False Then
                    listenThread = New Threading.Thread(AddressOf Listen)
                    listenThread.Start()
                End If
            Next
        Catch : End Try
    End Sub

    Private Sub bgExecutor_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgExecutor.DoWork

        Me.spawnThreads()
    End Sub

    Private Sub cleanThreads()
        Try
            Dim I As Integer = 0
            Dim clean As ArrayList = New ArrayList

            For Each oitem As GLVItem In Me.gList.Items
                Dim PID As Integer = oitem.Tag

                If PID = 0 Then
                    I += 1
                    Continue For
                End If

                Try
                    Dim p As Process = Process.GetProcessById(oitem.Tag)

                Catch ex As Exception
                    Try
                        crdxCommon.dtOOP.Rows(I).Delete()
                    Catch : End Try
                    'gList.Items.Remove(oitem)
                    clean.Add(I)
                End Try

                I += 1
            Next

            For Each n As Integer In clean
                Try
                    gList.Items.RemoveAt(n)
                Catch : End Try
            Next
        Catch : End Try
    End Sub

    Private Sub bgCleaner_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgCleaner.DoWork
        Me.cleanThreads()
    End Sub


    Private Sub dt_RowChanged(sender As Object, e As System.Data.DataRowChangeEventArgs)
        Dim row As DataRow = e.Row

        For Each it As GLVItem In Me.gList.Items
            If it.SubItems(4).Text = row("scheduleid") Then
                Dim nStatus As String = row("status")
                it.SubItems(2).Text = row("status")

                'update progress bar

                Try
                    Dim p As ProgressBar = it.SubItems(3).Control

                    p.Value = row("statusvalue")
                Catch : End Try

                it.Tag = row("PID")

                Exit For
            End If
        Next
    End Sub
End Class

