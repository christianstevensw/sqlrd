Public Class frmEventPackage

    Dim nStep As Integer = 1
    Dim ep As New ErrorProvider

    Private ReadOnly Property m_Step(ByVal stepValue As Integer) As String
        Get
            Select Case stepValue
                Case 1
                    Return "Step 1: Schedule Details"
                Case 2
                    Return "Step 2: Schedule Setup"
                Case 3
                    Return "Step 3: Add Schedules"
                Case 4
                    Return "Step 4: Custom Tasks"
                Case 5
                    Return "Step 5: Execution Flow"
            End Select
        End Get
    End Property
    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Dim oFolders As frmFolders = New frmFolders
        Dim folderDetails() As String

        folderDetails = oFolders.GetFolder

        txtLocation.Text = folderDetails(0)
        txtLocation.Tag = folderDetails(1)
    End Sub

    Private Sub frmEventPackage_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsFeatEnabled(gEdition.CORPORATE, modFeatCodes.s3_EventBasedScheds) = False Then
            _NeedUpgrade(gEdition.CORPORATE, Me, "Event-Based Schedules")
            Close()
            Return
        End If

        FormatForWinXP(Me)


        UcTasks.ShowAfterType = False
        UcTasks.tvTasks.ColumnsVisible = False

        If gParentID > 0 And gParent.Length > 0 Then
            Dim fld As folder = New folder(gParentID)
            txtLocation.Text = fld.getFolderPath
            txtLocation.Tag = gParentID
        End If

        '//sort out the imageList
        imgL.ImageSize = New Size(16, 16)
        imgL.ColorDepth = Windows.Forms.ColorDepth.Depth32Bit
        imgL.Images.Add(My.Resources.atom1)

        lsvSchedules.LargeImageList = imgL
        lsvSchedules.SmallImageList = imgL
    End Sub

    Private Sub cmdNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdNext.Click

        closeFlag = False

        Select Case stabMain.SelectedTab.Text
            Case "General"
                If txtName.Text.Length = 0 Then
                    setError(txtName, "Please enter the schedule name")
                    txtName.Focus()
                    Return
                ElseIf txtLocation.Text.Length = 0 Then
                    setError(txtLocation, "Please select the schedule location")
                    txtLocation.Focus()
                    Return
                ElseIf clsMarsUI.candoRename(txtName.Text, Me.txtLocation.Tag, clsMarsScheduler.enScheduleType.EVENTPACKAGE) = False Then
                    setError(txtName, "An Event-Based Package with this name already exists in this folder")
                    txtName.Focus()
                    Return
                End If

                tabSchedule.Visible = True
                stabMain.SelectedTab = tabSchedule
            Case "Schedule"
                If ucSet.isAllDataValid = False Then
                    Return
                End If

                tabSchedules.Visible = True
                stabMain.SelectedTab = tabSchedules

            Case "Schedules"
                tabTasks.Visible = True
                stabMain.SelectedTab = tabTasks
            Case "Custom Tasks"


                If UcTasks.tvTasks.Nodes.Count = 0 Then
                    ucPath.Enabled = False
                Else
                    ucPath.Enabled = True
                End If

                tabExecution.Visible = True
                stabMain.SelectedTab = tabExecution

                cmdFinish.Enabled = True
                cmdNext.Enabled = False
        End Select

    End Sub

    
    Private Sub txtLocation_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles txtLocation.TextChanged, txtName.TextChanged
        Dim oText As TextBox = CType(sender, TextBox)

        If oText.Text.Length = 0 Then
            cmdNext.Enabled = False
        Else
            cmdNext.Enabled = True
        End If
    End Sub

   
    Private Sub btnAdd_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnAdd.MouseClick

    End Sub

    Dim imgL As ImageList = New ImageList
    Private Sub mnuNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNew.Click
        Dim oEvent As New frmEventWizard6
        Dim eventDtls As Hashtable
        Dim eventName As String = ""
        Dim eventID As Integer = 0
        Dim eventStatus As String



        oEvent.txtLocation.Tag = "99999"
        oEvent.txtLocation.Text = "\Package"
        oEvent.txtLocation.Enabled = False
        oEvent.btnBrowse.Enabled = False
        eventDtls = oEvent.AddSchedule(lsvSchedules.Items.Count)

        If eventDtls IsNot Nothing Then
            Dim lsv As ListViewItem = Me.lsvSchedules.Items.Add(lsvSchedules.Items.Count + 1 & ":" & eventDtls.Item("Name"))
            lsv.Tag = eventDtls.Item("ID")
            lsv.ImageIndex = 0

            If eventDtls.Item("Status") = "Disabled" Then
                lsv.Checked = False
            Else
                lsv.Checked = True
            End If
        End If
    End Sub

    Private Sub mnuExisting_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExisting.Click
        Dim values As Hashtable
        Dim getEvents As frmFolders = New frmFolders

        getEvents.m_eventOnly = True
        values = getEvents.GetEventBasedSchedule

        If values IsNot Nothing Then
            Dim lsv As ListViewItem = lsvSchedules.Items.Add(lsvSchedules.Items.Count + 1 & ":" & values.Item("Name"))
            lsv.Tag = values.Item("ID")
            lsv.ImageIndex = 0

            clsMarsData.WriteData("UPDATE EventAttr6 SET Status = 1 WHERE EventID =" & lsv.Tag)

            lsv.Checked = True

        End If

    End Sub

    Private Sub cmdFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdFinish.Click
        Dim SQL As String
        Dim PackID As Integer = clsMarsData.CreateDataID("eventpackageattr", "eventpackid")
        Dim cols As String
        Dim vals As String

        cols = "EventPackID,Parent,PackageName,ExecutionFlow,Owner,AutoFailAfter"

        vals = PackID & "," & _
        txtLocation.Tag & "," & _
        "'" & SQLPrepare(txtName.Text) & "'," & _
        "'" & Me.ucPath.m_ExecutionFlow & "'," & _
        "'" & gUser & "'," & _
        txtAutoFail.Value
        
        If clsMarsData.DataItem.InsertData("EventPackageAttr", cols, vals) = True Then
            For Each lsv As ListViewItem In lsvSchedules.Items
                Dim EventID As Integer = lsv.Tag

                SQL = "UPDATE EventAttr6 SET PackID =" & PackID & "," & _
                "Parent = " & PackID & "," & _
                "PackOrderID =" & lsv.Index & " WHERE EventID =" & EventID

                clsMarsData.WriteData(SQL)
            Next

            If ucSet.chkEndDate.Checked = False Then ucSet.dtEndDate.Value = Now.AddYears(1000)

            Dim ScheduleID As Integer = ucSet.saveSchedule(PackID, clsMarsScheduler.enScheduleType.EVENTPACKAGE, txtDescription.Text, txtKeyword.Text)

            SQL = "UPDATE Tasks SET ScheduleID = " & ScheduleID & " WHERE ScheduleID = 99999"

            clsMarsData.WriteData(SQL)

            frmMainWin.itemToSelect = New genericExplorerObject
            frmMainWin.itemToSelect.folderID = txtLocation.Tag
            frmMainWin.itemToSelect.itemName = txtName.Text

            Close()

            frmMainWin.itemToSelect = New genericExplorerObject
            frmMainWin.itemToSelect.folderID = txtLocation.Tag
            frmMainWin.itemToSelect.itemName = txtName.Text

            clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.EVENTPACKAGE, clsMarsAudit.AuditAction.CREATE)

        End If
    End Sub

    
    Private Sub btnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUp.Click
        MoveListViewItem(Me.lsvSchedules, True)
    End Sub

    Private Function MoveListViewItem(ByVal lsv As ListView, ByVal moveUp As Boolean) As Integer
        If lsv.SelectedItems.Count = 0 Then Return 0

        Dim value As Integer

        If moveUp = False Then
            If lsv.SelectedItems(0).Index = lsv.Items.Count - 1 Then Return 0

            value = 1
        Else
            If lsv.SelectedItems(0).Index = 0 Then Return 0

            value = -1
        End If

        Dim itemList As ListViewItem()
        Dim I As Integer = 0
        Dim newIndex As Integer

        ReDim itemList(lsv.Items.Count - 1)

        'copy into an array
        For Each item As ListViewItem In lsv.Items
            itemList(I) = item
            I += 1
        Next

        newIndex = lsv.SelectedItems(0).Index + value

        Dim selItem As ListViewItem = lsv.SelectedItems(0)
        Dim nextItem As ListViewItem = lsv.Items(selItem.Index + value)

        Dim selPrefix As String = lsv.SelectedItems(0).Text.Split(":")(0)
        Dim nextPrefix As String = lsv.Items(selItem.Index + value).Text.Split(":")(0)

        selItem.Text = nextPrefix & ":" & selItem.Text.Split(":")(1)
        nextItem.Text = selPrefix & ":" & nextItem.Text.Split(":")(1)

        itemList(selItem.Index + value) = selItem
        itemList(selItem.Index) = nextItem

        lsv.Items.Clear()

        For Each item As ListViewItem In itemList
            lsv.Items.Add(item)

            If item.Index = newIndex Then
                item.Selected = True
            Else
                item.Selected = False
            End If
        Next

        lsv.Refresh()

        Return newIndex
    End Function

    Private Sub btnDown_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDown.Click
        MoveListViewItem(Me.lsvSchedules, False)
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lsvSchedules.SelectedItems.Count = 0 Then Return

        Dim frmEventProp As New frmEventProp

        frmEventProp.EditSchedule(lsvSchedules.SelectedItems(0).Tag)
    End Sub

    Dim closeFlag As Boolean = False

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim Res As DialogResult
        Dim oEvent As clsMarsEvent = New clsMarsEvent

        Res = MessageBox.Show("Would you like to delete the selected event-based schedule(s)?" & vbCrLf & _
        "Click YES to permanently delete the schedule, NO to simply remove it from this package or CANCEL to leave everything unchanged", _
        Application.ProductName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)


        Select Case Res
            Case Windows.Forms.DialogResult.Yes
                For Each item As ListViewItem In lsvSchedules.SelectedItems
                    Dim eventID As Integer = item.Tag

                    oEvent.DeleteEvent(eventID)

                    item.Remove()
                Next
            Case Windows.Forms.DialogResult.No
                For Each item As ListViewItem In lsvSchedules.SelectedItems
                    Dim eventID As Integer = item.Tag

                    Dim SQL As String = "UPDATE EventAttr6 SET PackID =0, Parent =" & txtLocation.Tag & " WHERE EventID =" & eventID

                    clsMarsData.WriteData(SQL)

                    item.Remove()
                Next
            Case Windows.Forms.DialogResult.Cancel
                Return
        End Select

    End Sub

   
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If closeFlag = False Then
            Dim sp As DevComponents.DotNetBar.SuperTooltip = New DevComponents.DotNetBar.SuperTooltip
            Dim spi As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo("Really Cancel?", "",
                                                                                                               "Are you sure you would like to close the wizard? All your progress will be lost. Click Cancel again to confirm.",
                                                                                                               Nothing,
                                                                                                               Nothing,
                                                                                                               DevComponents.DotNetBar.eTooltipColor.Office2003)
            sp.SetSuperTooltip(btnCancel, spi)
            sp.ShowTooltip(btnCancel)
            closeFlag = True
        Else
            Try
                clsMarsData.DataItem.CleanDB()

                Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT EventID FROM EventAttr6 WHERE Parent =99999")

                If oRs IsNot Nothing Then
                    Do While oRs.EOF = False
                        clsMarsEvent.DeleteEvent(oRs("eventid").Value)

                        oRs.MoveNext()
                    Loop

                    oRs.Close()
                End If
            Catch : End Try

            Close()
        End If
    End Sub

    Private Sub btnNew_Click(sender As System.Object, e As System.EventArgs) Handles btnNew.Click
        mnuNew_Click(sender, e)
    End Sub

    Private Sub btnExisting_Click(sender As Object, e As System.EventArgs) Handles btnExisting.Click
        mnuExisting_Click(sender, e)
    End Sub
End Class