<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFunctionUpgrade
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFunctionUpgrade))
        Me.Page1 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.txtUpgradeKey = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.Page2 = New System.Windows.Forms.Panel()
        Me.lsvFeatures = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnCancel = New DevComponents.DotNetBar.ButtonX()
        Me.btnFinish = New DevComponents.DotNetBar.ButtonX()
        Me.btnNext = New DevComponents.DotNetBar.ButtonX()
        Me.Super = New DevComponents.DotNetBar.SuperTooltip()
        Me.stabFeatures = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabFeatures = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabEnterKey = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Page1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Page2.SuspendLayout()
        CType(Me.stabFeatures, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabFeatures.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Page1
        '
        Me.Page1.BackColor = System.Drawing.Color.Transparent
        Me.Page1.Controls.Add(Me.TableLayoutPanel1)
        Me.Page1.Location = New System.Drawing.Point(3, 3)
        Me.Page1.Name = "Page1"
        Me.Page1.Size = New System.Drawing.Size(475, 251)
        Me.Page1.TabIndex = 1
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.65625!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 77.34375!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtUpgradeKey, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(14, 14)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.23932!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 83.76068!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(453, 234)
        Me.TableLayoutPanel1.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.Location = New System.Drawing.Point(3, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Upgrade Key"
        '
        'txtUpgradeKey
        '
        '
        '
        '
        Me.txtUpgradeKey.Border.Class = "TextBoxBorder"
        Me.txtUpgradeKey.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUpgradeKey.Location = New System.Drawing.Point(105, 41)
        Me.txtUpgradeKey.Multiline = True
        Me.txtUpgradeKey.Name = "txtUpgradeKey"
        Me.txtUpgradeKey.Size = New System.Drawing.Size(320, 190)
        Me.txtUpgradeKey.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.Location = New System.Drawing.Point(105, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(320, 29)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Please enter the Upgrade Key that has been provided to you. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "TIP: Use Copy and P" & _
    "aste to avoid errors in entering the long key."
        '
        'Page2
        '
        Me.Page2.Controls.Add(Me.lsvFeatures)
        Me.Page2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Page2.Location = New System.Drawing.Point(0, 0)
        Me.Page2.Name = "Page2"
        Me.Page2.Size = New System.Drawing.Size(508, 324)
        Me.Page2.TabIndex = 3
        '
        'lsvFeatures
        '
        Me.lsvFeatures.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.lsvFeatures.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvFeatures.Border.Class = "ListViewBorder"
        Me.lsvFeatures.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvFeatures.CheckBoxes = True
        Me.lsvFeatures.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvFeatures.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvFeatures.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lsvFeatures.ForeColor = System.Drawing.Color.Navy
        Me.lsvFeatures.FullRowSelect = True
        Me.lsvFeatures.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lsvFeatures.HotTracking = True
        Me.lsvFeatures.HoverSelection = True
        Me.lsvFeatures.Location = New System.Drawing.Point(0, 0)
        Me.lsvFeatures.Name = "lsvFeatures"
        Me.lsvFeatures.Size = New System.Drawing.Size(508, 324)
        Me.lsvFeatures.TabIndex = 1
        Me.lsvFeatures.UseCompatibleStateImageBehavior = False
        Me.lsvFeatures.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Width = 300
        '
        'btnCancel
        '
        Me.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnCancel.Location = New System.Drawing.Point(344, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnCancel.TabIndex = 6
        Me.btnCancel.Text = "&Cancel"
        '
        'btnFinish
        '
        Me.btnFinish.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnFinish.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnFinish.Enabled = False
        Me.btnFinish.Location = New System.Drawing.Point(506, 3)
        Me.btnFinish.Name = "btnFinish"
        Me.btnFinish.Size = New System.Drawing.Size(75, 23)
        Me.btnFinish.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnFinish.TabIndex = 5
        Me.btnFinish.Text = "&Finish"
        '
        'btnNext
        '
        Me.btnNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnNext.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnNext.Location = New System.Drawing.Point(425, 3)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(75, 23)
        Me.btnNext.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnNext.TabIndex = 5
        Me.btnNext.Text = "&Next"
        '
        'Super
        '
        Me.Super.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Super.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.Super.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        Me.Super.PositionBelowControl = False
        '
        'stabFeatures
        '
        '
        '
        '
        '
        '
        '
        Me.stabFeatures.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabFeatures.ControlBox.MenuBox.Name = ""
        Me.stabFeatures.ControlBox.Name = ""
        Me.stabFeatures.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabFeatures.ControlBox.MenuBox, Me.stabFeatures.ControlBox.CloseBox})
        Me.stabFeatures.Controls.Add(Me.SuperTabControlPanel1)
        Me.stabFeatures.Controls.Add(Me.SuperTabControlPanel2)
        Me.stabFeatures.Dock = System.Windows.Forms.DockStyle.Top
        Me.stabFeatures.Location = New System.Drawing.Point(0, 0)
        Me.stabFeatures.Name = "stabFeatures"
        Me.stabFeatures.ReorderTabsEnabled = True
        Me.stabFeatures.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabFeatures.SelectedTabIndex = 1
        Me.stabFeatures.Size = New System.Drawing.Size(584, 324)
        Me.stabFeatures.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.stabFeatures.TabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.stabFeatures.TabIndex = 8
        Me.stabFeatures.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabEnterKey, Me.tabFeatures})
        Me.stabFeatures.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.Page2)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(76, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(508, 324)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.tabFeatures
        '
        'tabFeatures
        '
        Me.tabFeatures.AttachedControl = Me.SuperTabControlPanel2
        Me.tabFeatures.GlobalItem = False
        Me.tabFeatures.Name = "tabFeatures"
        Me.tabFeatures.Text = "Features"
        Me.tabFeatures.Visible = False
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.Page1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(76, 0)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(508, 324)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabEnterKey
        '
        'tabEnterKey
        '
        Me.tabEnterKey.AttachedControl = Me.SuperTabControlPanel1
        Me.tabEnterKey.GlobalItem = False
        Me.tabEnterKey.Name = "tabEnterKey"
        Me.tabEnterKey.Text = "Enter Key"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnFinish)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnNext)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCancel)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 343)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(584, 29)
        Me.FlowLayoutPanel1.TabIndex = 9
        '
        'frmFunctionUpgrade
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(584, 372)
        Me.ControlBox = False
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.stabFeatures)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmFunctionUpgrade"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SQL-RD Feature Upgrade Wizard"
        Me.Page1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.Page2.ResumeLayout(False)
        CType(Me.stabFeatures, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabFeatures.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Page1 As System.Windows.Forms.Panel
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtUpgradeKey As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Page2 As System.Windows.Forms.Panel
    Friend WithEvents lsvFeatures As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnNext As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnFinish As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Super As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents stabFeatures As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabFeatures As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabEnterKey As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
End Class
