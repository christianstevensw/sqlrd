Imports sqlrd.clsMarsData

Public Class frmReadReciepts
    Public Enum recieptType As Integer
        DELIVERY = 0
        READ = 1
    End Enum
    Dim UserCancel As Boolean = True
    Dim RecptID As Integer = CreateDataID("ReadReceiptsAttr", "ReceiptID")

    Private Sub frmReadReciepts_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
        UcTasks1.ShowAfterType = False
    End Sub

    Public Sub AddReceipt(ByVal type As recieptType, Optional ByVal DestinationID As Integer = 99999)
        Dim oRs As ADODB.Recordset
        Dim SQL As String = "SELECT * FROM ReadReceiptsAttr WHERE DestinationID =" & DestinationID

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then
                Try
                    txtCheck.Value = oRs("waitlength").Value
                Catch
                    txtCheck.Value = 0.5
                End Try

                chkRepeat.Checked = Convert.ToBoolean(oRs("repeat").Value)
                Try
                    txtRepeat.Value = oRs("repeatevery").Value
                Catch
                    txtRepeat.Value = 0.5
                End Try

                RecptID = oRs("receiptid").Value
            Else
                RecptID = CreateDataID("ReadReceiptsAttr", "ReceiptID")
            End If

            oRs.Close()
        End If

        UcTasks1.ScheduleID = RecptID
        UcTasks1.LoadTasks()

        oRs = Nothing

        If type = recieptType.DELIVERY Then
            Me.Text = "Delivery Receipts"
            Label1.Text = "Carry out the following tasks if a delivery receipt has not been recieved in (mins)"
            Label2.Text = "until a delivery receipt is recieved"
        End If

        Me.ShowDialog()

        If UserCancel = True Then
            Return
        End If


        Dim cols As String
        Dim vals As String

        If IsDuplicate("ReadReceiptsAttr", "ReceiptID", RecptID, False) = False Then
            cols = "ReceiptID,DestinationID,waitlength,Repeat,RepeatEvery"

            vals = RecptID & "," & _
            DestinationID & "," & _
            Me.txtCheck.Value & "," & _
            Convert.ToInt32(chkRepeat.Checked) & "," & _
            Me.txtRepeat.Value

            DataItem.InsertData("ReadReceiptsAttr", cols, vals)
        Else
            SQL = "UPDATE ReadReceiptsAttr SET " & _
            "waitlength = " & txtCheck.Value & "," & _
            "Repeat = " & Convert.ToInt32(chkRepeat.Checked) & "," & _
            "RepeatEvery = " & txtRepeat.Value & " WHERE ReceiptID = " & RecptID

            WriteData(SQL)
        End If

    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        UserCancel = False

        Close()
    End Sub

    Private Sub chkRepeat_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRepeat.CheckedChanged
        txtRepeat.Enabled = chkRepeat.Checked
    End Sub
End Class