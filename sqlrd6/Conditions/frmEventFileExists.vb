Public Class frmEventFileExists
    Private conditionType As String

    Dim conditionValue As Boolean
    Dim ep As New ErrorProvider
    Dim userCancel As Boolean = True
    Dim eventType As String
    Dim eventID As Integer
    Dim edit As Boolean = False


    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW As Object = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property

    Public Property m_runForEachFile() As Boolean
        Get
            If optRunForEachFile.Checked Then
                Return True
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            If value = True Then
                optRunForEachFile.Checked = True
            Else
                optRunOnce.Checked = True
            End If
        End Set
    End Property

    Private Property directoryType As String
        Get
            If stabFiles.SelectedTab.Text = "Local File" Then
                Return "LOCAL"
            Else
                Return "REMOTE"
            End If
        End Get
        Set(value As String)
            If value = "LOCAL" Or value = "" Then
                stabFiles.SelectedTabIndex = 0
            Else
                stabFiles.SelectedTabIndex = 1
            End If
        End Set
    End Property
    Private Property selectedFile As String
        Get
            If directoryType = "LOCAL" Then
                Return txtFilePath.Text
            Else
                Return ucFTP.txtDirectory.Text
            End If
        End Get
        Set(value As String)
            If directoryType = "LOCAL" Then
                txtFilePath.Text = value
            Else
                ucFTP.txtDirectory.Text = value
            End If
        End Set
    End Property
    Public Property m_Boolean() As Boolean
        Get
            If cmbValue.Text = "FALSE" Then
                Return False
            Else
                Return True
            End If
        End Get
        Set(ByVal value As Boolean)
            conditionValue = value

            cmbValue.Text = value.ToString.ToUpper
        End Set
    End Property

    Public Enum TYPES
        FILE_EXISTS = 0
        FILE_MODIFIED = 1
    End Enum
    Public Property m_Type() As TYPES
        Get
            Return conditionType
        End Get
        Set(ByVal value As TYPES)
            conditionType = value

            Select Case value

                Case TYPES.FILE_EXISTS
                    Me.Text = "Condition - File Exists"

                    eventType = "file exists"
                Case TYPES.FILE_MODIFIED
                    Me.Text = "Condition - File has been modified"

                    eventType = "file has been modified"
                    SuperTabItem2.Visible = False
            End Select

            Label4.Text = "If " & eventType & " ="
        End Set
    End Property
    Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
        Dim sfd As SaveFileDialog = New SaveFileDialog

        sfd.Title = "Please select the file..."
        sfd.CheckFileExists = False
        sfd.OverwritePrompt = False

        If sfd.ShowDialog = Windows.Forms.DialogResult.OK Then
            txtFilePath.Text = sfd.FileName
        End If
    End Sub

    Private Sub frmEventFileExists_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
        txtFilePath.ContextMenu = mnuInserter
        ucFTP.Label6.Text = "Remote File"
        ucFTP.m_browseFTPForFile = True
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If txtFilePath.Text.Length = 0 And directoryType = "LOCAL" Then
            setError(txtFilePath, "Please select the file")
            txtFilePath.Focus()
            Return
        ElseIf txtName.Text.Length = 0 Then
            setError(txtName, "Please enter a name for this condition")
            txtName.Focus()
            Return
        ElseIf clsMarsEvent.IsConditionDuplicate(Me.eventID, txtName.Text) = True And edit = False Then
            setError(txtName, "A condition with this name already exists")
            txtName.Focus()
            Return
        ElseIf directoryType = "REMOTE" And ucFTP.validateFtpInfo() = False Then
            setError(ucFTP.txtFTPServer, "Please validate your FTP information")
            ucFTP.txtFTPServer.Focus()
            Return
        ElseIf directoryType = "REMOTE" And ucFTP.txtDirectory.Text = "" Then
            setError(ucFTP.txtDirectory, "Please select the file to search for")
            ucFTP.txtDirectory.Focus()
            Return
        End If

        userCancel = False

        Close()
    End Sub

    Private Sub txtFilePath_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFilePath.TextChanged
        SetError(txtFilePath, "")
    End Sub

    Public Sub EditCondition(ByVal nConditionID As Integer)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim dateMod As String

        edit = True

        SQL = "SELECT * FROM EventConditions WHERE ConditionID =" & nConditionID

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then

                txtName.Text = oRs("conditionname").Value

                directoryType = IsNull(oRs("redirectto").Value, "LOCAL")

                Me.m_Boolean = Convert.ToBoolean(oRs("returnvalue").Value)

                eventID = oRs("eventid").Value

                If directoryType = "LOCAL" Or directoryType = "" Then
                    txtFilePath.Text = oRs("filepath").Value

                    Try
                        If IsNull(oRs("runforeachfile").Value, 0) = 0 Then
                            optRunOnce.Checked = True
                        Else
                            optRunForEachFile.Checked = True
                        End If
                    Catch ex As Exception
                        optRunOnce.Checked = True
                    End Try
                Else
                    '//popserver =ftpserver
                    '//popuser = ftp user
                    '//poppassword = ftp password
                    '//usessl = passive
                    '//mailservertype = ftptype
                    '//imapbox = ftp options
                    With ucFTP
                        .cmbFTPType.Text = oRs("mailservertype").Value
                        .txtDirectory.Text = oRs("filepath").Value
                        .txtFTPServer.Text = oRs("popserver").Value
                        .txtUserName.Text = oRs("popuser").Value
                        .txtPassword.Text = _DecryptDBValue(oRs("poppassword").Value)
                        .txtPort.Value = oRs("serverport").Value
                        .chkPassive.Checked = oRs("usessl").Value
                        .m_ftpOptions = oRs("imapbox").Value
                    End With
                End If
            End If

            oRs.Close()
        End If

        Me.ShowDialog()

        If userCancel = True Then Return

        Dim vals As String

        If directoryType = "LOCAL" Then
            vals = "ConditionName = '" & SQLPrepare(txtName.Text) & "'," & _
            "ReturnValue = " & Convert.ToInt32(Me.m_Boolean) & "," & _
            "FilePath = '" & SQLPrepare(txtFilePath.Text) & "', " & _
             "runforeachfile = " & Convert.ToInt32(m_runForEachFile)
        Else
            With ucFTP
                vals = "ConditionName = '" & SQLPrepare(txtName.Text) & "'," & _
                "ReturnValue = " & Convert.ToInt32(Me.m_Boolean) & "," & _
                "FilePath = '" & SQLPrepare(.txtDirectory.Text) & "', " & _
                "popserver = '" & SQLPrepare(.txtFTPServer.Text) & "'," & _
                "popuser = '" & SQLPrepare(.txtUserName.Text) & "'," & _
                "poppassword  = '" & SQLPrepare(_EncryptDBValue(.txtPassword.Text)) & "'," & _
                "serverport = " & .txtPort.Value & "," & _
                "usessl  = " & Convert.ToInt32(.chkPassive.Checked) & "," & _
                "mailservertype = '" & SQLPrepare(.cmbFTPType.Text) & "'," & _
                "imapbox = '" & SQLPrepare(.m_ftpOptions) & "', " & _
                "runforeachfile = " & Convert.ToInt32(m_runForEachFile)
            End With
        End If

        SQL = "UPDATE EventConditions SET " & vals & " WHERE ConditionID = " & nConditionID

        clsMarsData.WriteData(SQL)
    End Sub
    Public Sub AddCondition(ByVal nEventID As Integer, ByVal nOrderID As Integer)
        eventID = nEventID

        directoryType = "LOCAL"

        Me.ShowDialog()

        If userCancel = True Then Return

        clsMarsUI.MainUI.BusyProgress(50, "Saving condition...")

        Dim cols As String
        Dim vals As String
        Dim dateMod As String

        If Me.m_Type = TYPES.FILE_MODIFIED Then
            Dim fileInfo As IO.FileInfo = New IO.FileInfo(txtFilePath.Text)

            dateMod = ConDateTime(fileInfo.LastWriteTime)
        Else
            dateMod = ConDateTime(Now)
        End If

        '//redirectto = directory type

        If directoryType = "LOCAL" Then
            cols = "ConditionID,EventID,ConditionName,ConditionType,ReturnValue,KeyColumn,FilePath,redirectto,OrderID,runforeachfile"

            vals = clsMarsData.CreateDataID("eventconditions", "conditionid") & "," & _
            nEventID & "," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            "'" & eventType & "'," & _
            Convert.ToInt32(Me.m_Boolean) & "," & _
            "'" & dateMod & "'," & _
            "'" & SQLPrepare(txtFilePath.Text) & "'," & _
            "'" & directoryType & "'," & _
            nOrderID & "," & _
            Convert.ToInt32(m_runForEachFile)

            clsMarsData.DataItem.InsertData("EventConditions", cols, vals, True)
        Else
            '//popserver =ftpserver
            '//popuser = ftp user
            '//poppassword = ftp password
            '//usessl = passive
            '//mailservertype = ftptype
            '//imapbox = ftp options

            cols = "ConditionID,EventID,ConditionName,ConditionType,ReturnValue,KeyColumn,FilePath," & _
                "popserver,popuser,poppassword,serverport,usessl,mailservertype,imapbox,redirectto,OrderID"

            With ucFTP
                vals = clsMarsData.CreateDataID("eventconditions", "conditionid") & "," & _
                   nEventID & "," & _
                   "'" & SQLPrepare(txtName.Text) & "'," & _
                   "'" & eventType & "'," & _
                   Convert.ToInt32(Me.m_Boolean) & "," & _
                   "'" & dateMod & "'," & _
                   "'" & SQLPrepare(.txtDirectory.Text) & "'," & _
                   "'" & SQLPrepare(.txtFTPServer.Text) & "'," & _
                   "'" & SQLPrepare(.txtUserName.Text) & "'," & _
                   "'" & SQLPrepare(_EncryptDBValue(.txtPassword.Text)) & "'," & _
                   .txtPort.Value & "," & _
                   Convert.ToInt32(.chkPassive.Checked) & "," & _
                   "'" & .cmbFTPType.Text & "'," & _
                   "'" & SQLPrepare(.m_ftpOptions) & "'," & _
                   "'" & directoryType & "'," & _
                   nOrderID
            End With

            clsMarsData.DataItem.InsertData("eventconditions", cols, vals)
        End If

        clsMarsUI.MainUI.BusyProgress(100, , True)
    End Sub

    
    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        txtFilePath.Undo()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        txtFilePath.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        txtFilePath.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        txtFilePath.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        txtFilePath.SelectedText = ""
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        txtFilePath.SelectAll()
    End Sub


    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim intruder As New frmInserter(0)

        intruder.GetConstants(Me)
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs)

    End Sub
End Class