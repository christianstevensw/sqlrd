﻿Imports System.Management

Public Class frmEventDataRecvd
    Dim eventID As Integer
    Dim userCancel As Boolean
    Dim conditionValue As Boolean
    Dim edit As Boolean
    Dim ep As ErrorProvider = New ErrorProvider
    Dim mode As String

    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW As Object = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property

    Private Sub CheckBoxX1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkSpecificIPAddress.CheckedChanged
        txtIPAddress.Enabled = chkSpecificIPAddress.Checked
    End Sub

    Public Property ipAddress As String
        Get
            If chkSpecificIPAddress.Checked Then
                Return txtIPAddress.Value
            Else
                Return ""
            End If
        End Get
        Set(value As String)
            If value = "" Then
                chkSpecificIPAddress.Checked = False
            Else
                chkSpecificIPAddress.Checked = True
                txtIPAddress.Value = value
            End If
        End Set
    End Property

    Private Sub frmEventDataRecvd_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        FormatForWinXP(Me)
    End Sub



    Public Property m_Boolean() As Boolean
        Get
            If cmbValue.Text = "FALSE" Then
                Return False
            Else
                Return True
            End If
        End Get
        Set(ByVal value As Boolean)
            conditionValue = value

            cmbValue.Text = value.ToString.ToUpper
        End Set
    End Property

    Public Sub AddCondition(ByVal nEventID As Integer, ByVal nOrderID As Integer)
        mode = "add"

        eventID = nEventID

        criteriaBuilder.criteria = "::" & PD

        Me.ShowDialog()

        If userCancel = True Then Return

        clsMarsUI.MainUI.BusyProgress(50, "Saving condition...")

        Dim cols As String
        Dim vals As String
        Dim dateMod As String
        Dim nConditionID As Integer = clsMarsData.CreateDataID("eventconditions", "conditionid")
        Dim configFile = "" 'createConfigFileForListener(nConditionID, nEventID)
        '//using the popserver field for the IP Address
        '//using pollinginterval as the buffer size
        cols = "ConditionID,EventID,ConditionName,ConditionType,ReturnValue,serverPort,popServer,cacheFolderName,OrderID,searchCriteria,MatchExactCriteria"

        vals = nConditionID & "," & _
        nEventID & "," & _
        "'" & SQLPrepare(txtName.Text) & "'," & _
        "'Data Received'," & _
        Convert.ToInt32(Me.m_Boolean) & "," & _
        txtPortNumber.Value & "," & _
        "'" & SQLPrepare(ipAddress) & "'," & _
        "'" & SQLPrepare(configFile) & "'," & _
        nOrderID & "," & _
        "'" & SQLPrepare(criteriaBuilder.criteria) & "'," & _
        criteriaBuilder.criteriaMatch

        clsMarsData.DataItem.InsertData("EventConditions", cols, vals, True)

        clsMarsUI.MainUI.BusyProgress(100, , True)
    End Sub

    Public Sub EditCondition(ByVal nConditionID As Integer)
        mode = "edit"
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim dateMod As String

        edit = True

        SQL = "SELECT * FROM EventConditions WHERE ConditionID =" & nConditionID

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then

                txtName.Text = oRs("conditionname").Value

                txtPortNumber.Value = oRs("serverport").Value

                Me.m_Boolean = Convert.ToBoolean(oRs("returnvalue").Value)

                ipAddress = oRs("popServer").Value

                eventID = oRs("eventid").Value

                criteriaBuilder.criteria = IsNull(oRs("searchcriteria").Value)

                criteriaBuilder.criteriaMatch = IsNull(oRs("MatchExactCriteria").Value, 1)
            End If

            oRs.Close()
        End If

        Me.ShowDialog()

        If userCancel = True Then Return

        Dim vals As String
        Dim configFile = "" 'createConfigFileForListener(nConditionID, eventID)

        vals = "ConditionName = '" & SQLPrepare(txtName.Text) & "'," & _
        "serverPort = " & txtPortNumber.Value & "," & _
        "popServer = '" & SQLPrepare(ipAddress) & "', " & _
        "cacheFolderName ='" & SQLPrepare(configFile) & "'," & _
        "returnvalue =" & Convert.ToInt32(Me.m_Boolean) & "," & _
        "searchcriteria ='" & SQLPrepare(criteriaBuilder.criteria) & "'," & _
        "MatchExactCriteria = " & criteriaBuilder.criteriaMatch

        SQL = "UPDATE EventConditions SET " & vals & " WHERE ConditionID = " & nConditionID

        clsMarsData.WriteData(SQL)

        Dim controller As clsServiceController = New clsServiceController

        If controller.m_serviceStatus = clsServiceController.enum_svcStatus.RUNNING Then
            controller.StopScheduler()

            System.Threading.Thread.Sleep(1500)

            controller.StartScheduler()
        End If

        '//restart the listener if its running
        'Dim dt As DataTable = GetRunningProcesses()

        'If dt IsNot Nothing Then
        '    Dim rows() As DataRow = dt.Select("name ='csslistenerd.exe'")

        '    If rows IsNot Nothing Then
        '        For Each row As DataRow In rows
        '            Dim commandLine As String = row("commandline")
        '            Dim procID As Integer = row("processid")

        '            If commandLine.Contains(nConditionID) Then
        '                '//kill and restart
        '                Try
        '                    Dim p As Process = Process.GetProcessById(procID)

        '                    p.Kill()

        '                    System.Threading.Thread.Sleep(1500)

        '                    p = New Process
        '                    p.StartInfo.FileName = Application.StartupPath & "\csslistenerd.exe"
        '                    p.StartInfo.Arguments = configFile
        '                    p.Start()
        '                    Exit For
        '                Catch : End Try
        '            End If
        '        Next
        '    End If
        'End If
    End Sub
    Public Function GetRunningProcesses() As DataTable
        '//One way of constructing a query

        Dim wmiClass As String = "Win32_Process"

        Dim condition As String = ""

        Dim queryProperties As String() = New String() {"Name", "ProcessId", "Caption", "ExecutablePath", "CommandLine"}

        Dim wmiQuery As SelectQuery = New SelectQuery(wmiClass, condition, queryProperties)

        Dim scope As ManagementScope = New System.Management.ManagementScope("\\.\root\CIMV2")

        Dim searcher As ManagementObjectSearcher = New ManagementObjectSearcher(scope, wmiQuery)

        Dim runningProcesses As ManagementObjectCollection = searcher.Get()

        Dim queryResults As DataTable = New DataTable()

        queryResults.Columns.Add("Name", Type.GetType("System.String"))

        queryResults.Columns.Add("ProcessId", Type.GetType("System.Int32"))

        queryResults.Columns.Add("Caption", Type.GetType("System.String"))

        queryResults.Columns.Add("Path", Type.GetType("System.String"))

        queryResults.Columns.Add("CommandLine", Type.GetType("System.String"))


        For Each obj As ManagementObject In runningProcesses

            Dim row As DataRow = queryResults.NewRow()

            row("Name") = obj("Name").ToString()

            row("ProcessId") = Convert.ToInt32(obj("ProcessId"))

            If (obj("Caption") IsNot Nothing) Then row("Caption") = obj("Caption").ToString()

            If (obj("ExecutablePath") IsNot Nothing) Then row("Path") = obj("ExecutablePath").ToString()

            If (obj("CommandLine") IsNot Nothing) Then row("CommandLine") = obj("CommandLine").ToString()

            queryResults.Rows.Add(row)
        Next

        Return queryResults

    End Function

    Private Sub btnOK_Click(sender As Object, e As System.EventArgs) Handles btnOK.Click
        If txtName.Text = "" Then
            setError(txtName, "Please specify the name for this condition")
            txtName.Focus()
            Return
        ElseIf cmbValue.Text = "" Then
            setError(cmbValue, "Please set whether the condition is met when there is data or not")
            cmbValue.Focus()
            Return
        Else
            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT COUNT(*) FROM eventconditions WHERE conditiontype = 'data received' AND serverport = " & txtPortNumber.Value & " AND popServer ='" & txtIPAddress.Text & "' AND searchCriteria = '" & SQLPrepare("txtSearchString.Text") & "' " & _
                                                              "eventID IN (SELECT eventID FROM eventattr6 WHERE status =1)")

            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                Dim total As Integer = oRs(0).Value

                If mode = "add" And total > 0 Then
                    setError(txtPortNumber, "A condition is already set up to monitor the specified port and search string. Please change the port, IP address to monitor or search value")
                    txtPortNumber.Focus()
                    Return
                ElseIf mode = "edit" And total > 1 Then
                    setError(txtPortNumber, "A condition is already set up to monitor the specified port and search string. Please change the port, IP address to monitor, or search value")
                    txtPortNumber.Focus()
                    Return
                End If
            End If
        End If

        Close()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        userCancel = True
        Close()
    End Sub

    Private Sub txtName_TextChanged(sender As Object, e As System.EventArgs) Handles txtName.TextChanged
        setError(txtName, "")
    End Sub

    Private Sub cmbValue_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cmbValue.SelectedIndexChanged
        setError(cmbValue, "")
    End Sub

    Private Function createConfigFileForListener(conditionID As Integer, eventID As Integer) As String
        Return ""

        Dim dt As DataTable = New DataTable("configuration")

        dt.Columns.Add("conditionid")
        dt.Columns.Add("portnumber")
        dt.Columns.Add("ipaddress")
        dt.Columns.Add("constring")

        Dim r As DataRow = dt.Rows.Add

        r("conditionid") = conditionID
        r("portnumber") = txtPortNumber.Value
        r("ipaddress") = ipAddress
        r("constring") = _EncryptDBValue(gCon.ConnectionString)

        Dim saveToFile As String = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)
        saveToFile = IO.Path.Combine(saveToFile, "ChristianSteven\SQL-RD\portConditionConfig\" & conditionID)

        clsMarsParser.Parser.ParseDirectory(saveToFile)

        saveToFile = IO.Path.Combine(saveToFile, "config.xml")

        Try
            If IO.File.Exists(saveToFile) Then
                IO.File.Delete(saveToFile)
            End If
        Catch : End Try

        dt.WriteXml(saveToFile, XmlWriteMode.WriteSchema)

        Return saveToFile
    End Function


   
End Class