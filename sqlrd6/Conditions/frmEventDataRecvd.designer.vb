﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEventDataRecvd
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkSpecificIPAddress = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtIPAddress = New DevComponents.Editors.IpAddressInput()
        Me.txtPortNumber = New DevComponents.Editors.IntegerInput()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.btnOK = New DevComponents.DotNetBar.ButtonX()
        Me.btnCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmbValue = New System.Windows.Forms.ComboBox()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.criteriaBuilder = New sqlrd.ucCriteriaBuilder()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtIPAddress, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPortNumber, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'LabelX1
        '
        '
        '
        '
        Me.LabelX1.BackgroundStyle.Class = ""
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(15, 19)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(75, 23)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "Name"
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.Location = New System.Drawing.Point(156, 20)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(238, 20)
        Me.txtName.TabIndex = 1
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkSpecificIPAddress)
        Me.GroupBox1.Controls.Add(Me.txtIPAddress)
        Me.GroupBox1.Controls.Add(Me.txtPortNumber)
        Me.GroupBox1.Controls.Add(Me.LabelX2)
        Me.GroupBox1.Location = New System.Drawing.Point(11, 93)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(480, 113)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Data is received on the following"
        '
        'chkSpecificIPAddress
        '
        '
        '
        '
        Me.chkSpecificIPAddress.BackgroundStyle.Class = ""
        Me.chkSpecificIPAddress.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkSpecificIPAddress.Location = New System.Drawing.Point(16, 51)
        Me.chkSpecificIPAddress.Name = "chkSpecificIPAddress"
        Me.chkSpecificIPAddress.Size = New System.Drawing.Size(185, 23)
        Me.chkSpecificIPAddress.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkSpecificIPAddress.TabIndex = 3
        Me.chkSpecificIPAddress.Text = "Monitor on a specific IP address"
        '
        'txtIPAddress
        '
        Me.txtIPAddress.AutoOverwrite = True
        '
        '
        '
        Me.txtIPAddress.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtIPAddress.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtIPAddress.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.txtIPAddress.ButtonFreeText.Visible = True
        Me.txtIPAddress.Enabled = False
        Me.txtIPAddress.Location = New System.Drawing.Point(141, 80)
        Me.txtIPAddress.Name = "txtIPAddress"
        Me.txtIPAddress.Size = New System.Drawing.Size(114, 20)
        Me.txtIPAddress.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.txtIPAddress.TabIndex = 2
        '
        'txtPortNumber
        '
        '
        '
        '
        Me.txtPortNumber.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtPortNumber.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPortNumber.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.txtPortNumber.Location = New System.Drawing.Point(141, 23)
        Me.txtPortNumber.Name = "txtPortNumber"
        Me.txtPortNumber.ShowUpDown = True
        Me.txtPortNumber.Size = New System.Drawing.Size(114, 20)
        Me.txtPortNumber.TabIndex = 1
        Me.txtPortNumber.Value = 8080
        '
        'LabelX2
        '
        '
        '
        '
        Me.LabelX2.BackgroundStyle.Class = ""
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Location = New System.Drawing.Point(16, 22)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(75, 23)
        Me.LabelX2.TabIndex = 0
        Me.LabelX2.Text = "Port Number"
        '
        'btnOK
        '
        Me.btnOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnOK.Location = New System.Drawing.Point(519, 12)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 5
        Me.btnOK.Text = "&OK"
        '
        'btnCancel
        '
        Me.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnCancel.Location = New System.Drawing.Point(519, 43)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 6
        Me.btnCancel.Text = "&Cancel"
        '
        'cmbValue
        '
        Me.cmbValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbValue.FormattingEnabled = True
        Me.cmbValue.Items.AddRange(New Object() {"TRUE", "FALSE"})
        Me.cmbValue.Location = New System.Drawing.Point(156, 55)
        Me.cmbValue.Name = "cmbValue"
        Me.cmbValue.Size = New System.Drawing.Size(238, 21)
        Me.cmbValue.TabIndex = 34
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        '
        '
        '
        Me.Label4.BackgroundStyle.Class = ""
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.Location = New System.Drawing.Point(15, 58)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(126, 15)
        Me.Label4.TabIndex = 35
        Me.Label4.Text = "If data has been received"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.GroupBox3)
        Me.GroupBox2.Controls.Add(Me.LabelX1)
        Me.GroupBox2.Controls.Add(Me.cmbValue)
        Me.GroupBox2.Controls.Add(Me.txtName)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.GroupBox1)
        Me.GroupBox2.Location = New System.Drawing.Point(5, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(501, 478)
        Me.GroupBox2.TabIndex = 36
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Condition Details"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.criteriaBuilder)
        Me.GroupBox3.Location = New System.Drawing.Point(11, 212)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(480, 256)
        Me.GroupBox3.TabIndex = 36
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Recvd data"
        '
        'criteriaBuilder
        '
        Me.criteriaBuilder.criteria = ""
        Me.criteriaBuilder.criteriaMatch = 1
        Me.criteriaBuilder.Location = New System.Drawing.Point(3, 19)
        Me.criteriaBuilder.Name = "criteriaBuilder"
        Me.criteriaBuilder.Size = New System.Drawing.Size(471, 231)
        Me.criteriaBuilder.TabIndex = 0
        '
        'frmEventDataRecvd
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(606, 495)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmEventDataRecvd"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Condition - Data received on a port"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.txtIPAddress, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPortNumber, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtPortNumber As DevComponents.Editors.IntegerInput
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents chkSpecificIPAddress As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtIPAddress As DevComponents.Editors.IpAddressInput
    Friend WithEvents btnOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmbValue As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents criteriaBuilder As sqlrd.ucCriteriaBuilder
End Class
