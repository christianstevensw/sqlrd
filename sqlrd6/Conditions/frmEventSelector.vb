Friend Class frmEventSelector
    'File exists
    'Database record exists
    'File has been modified
    'Window is present
    'Process exists
    'Unread email is present
    'Database record has changed
    Dim ep As New ErrorProvider
    Dim nEventID As Integer = 99999
    Dim nOrderID As Integer = 0

    Private ReadOnly Property m_Boolean() As Boolean
        Get
            If Me.cmbValue.Text = "TRUE" Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    Public WriteOnly Property m_EventID() As Integer
        Set(ByVal value As Integer)
            nEventID = value
        End Set
    End Property

    Public WriteOnly Property m_OrderID() As Integer
        Set(ByVal value As Integer)
            nOrderID = value
        End Set
    End Property

    Private Sub btnProceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProceed.Click

        If cmbType.Text.Length = 0 Then
            ep.SetError(cmbType, "Please select the condition before proceeding")
            cmbType.Focus()
            Return
        ElseIf cmbValue.Text.Length = 0 Then
            ep.SetError(cmbValue, "Please specify the return value")
            cmbValue.Focus()
            Return
        End If

        Select Case cmbType.Text
            Case "Database record exists"

                Dim oForm As frmEventDatabaseExists = New frmEventDatabaseExists

                oForm.m_Type = frmEventDatabaseExists.TYPES.DB_EXISTS
                oForm.m_Boolean = Me.m_Boolean

                Me.Close()

                oForm.AddCondition(nEventID, nOrderID)
            Case "Database record has changed"
                Dim oForm As frmEventDatabaseExists = New frmEventDatabaseExists

                oForm.m_Type = frmEventDatabaseExists.TYPES.DB_MODIFIED
                oForm.m_Boolean = Me.m_Boolean

                Me.Close()

                oForm.AddCondition(nEventID, nOrderID)

            Case "File exists"

                Dim oForm As frmEventFileExists = New frmEventFileExists

                oForm.m_Type = frmEventFileExists.TYPES.FILE_EXISTS
                oForm.m_Boolean = Me.m_Boolean

                Me.Close()

                oForm.AddCondition(nEventID, nOrderID)

            Case "File has been modified"
                Dim oForm As frmEventFileExists = New frmEventFileExists

                oForm.m_Type = frmEventFileExists.TYPES.FILE_MODIFIED
                oForm.m_Boolean = Me.m_Boolean

                Me.Close()

                oForm.AddCondition(nEventID, nOrderID)

            Case "Process exists"
                Dim oForm As frmEventProcessExists = New frmEventProcessExists

                oForm.m_Type = frmEventProcessExists.TYPES.PROCESS_EXISTS
                oForm.m_Boolean = Me.m_Boolean

                Me.Close()

                oForm.AddCondition(nEventID, nOrderID)

            Case "Unread email is present"

                Dim oForm As frmEventEmailExists = New frmEventEmailExists

                oForm.m_Boolean = Me.m_Boolean

                Me.Close()

                oForm.AddCondition(nEventID, nOrderID)

            Case "Window is present"
                Dim oForm As frmEventProcessExists = New frmEventProcessExists

                oForm.m_Type = frmEventProcessExists.TYPES.WINDOW_EXISTS
                oForm.m_Boolean = Me.m_Boolean

                Me.Close()

                oForm.AddCondition(nEventID, nOrderID)

        End Select
    End Sub


    Private Sub frmEventSelector_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub cmbType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbType.SelectedIndexChanged, _
    cmbValue.SelectedIndexChanged
        Dim cmb As ComboBox = CType(sender, ComboBox)

        ep.SetError(cmb, "")
    End Sub
End Class