﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLocalSQLCon
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLocalSQLCon))
        Me.lblServername = New DevComponents.DotNetBar.LabelX()
        Me.txtservername = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.lblDatabasename = New DevComponents.DotNetBar.LabelX()
        Me.txtdatabasename = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.lblinstanceName = New DevComponents.DotNetBar.LabelX()
        Me.txtinstancename = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.lblpassword = New DevComponents.DotNetBar.LabelX()
        Me.txtsystemadmin = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btnOK = New DevComponents.DotNetBar.ButtonX()
        Me.btnCancel = New DevComponents.DotNetBar.ButtonX()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.ReflectionImage1 = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblServername
        '
        Me.lblServername.AutoSize = True
        '
        '
        '
        Me.lblServername.BackgroundStyle.Class = ""
        Me.lblServername.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblServername.Location = New System.Drawing.Point(3, 3)
        Me.lblServername.Name = "lblServername"
        Me.lblServername.Size = New System.Drawing.Size(65, 16)
        Me.lblServername.TabIndex = 0
        Me.lblServername.Text = "Server Name"
        '
        'txtservername
        '
        Me.txtservername.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtservername.Border.Class = "TextBoxBorder"
        Me.txtservername.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtservername.Location = New System.Drawing.Point(3, 25)
        Me.txtservername.Name = "txtservername"
        Me.txtservername.Size = New System.Drawing.Size(284, 21)
        Me.txtservername.TabIndex = 1
        '
        'lblDatabasename
        '
        Me.lblDatabasename.AutoSize = True
        '
        '
        '
        Me.lblDatabasename.BackgroundStyle.Class = ""
        Me.lblDatabasename.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblDatabasename.Location = New System.Drawing.Point(3, 52)
        Me.lblDatabasename.Name = "lblDatabasename"
        Me.lblDatabasename.Size = New System.Drawing.Size(79, 16)
        Me.lblDatabasename.TabIndex = 2
        Me.lblDatabasename.Text = "Database Name"
        '
        'txtdatabasename
        '
        '
        '
        '
        Me.txtdatabasename.Border.Class = "TextBoxBorder"
        Me.txtdatabasename.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtdatabasename.Location = New System.Drawing.Point(3, 74)
        Me.txtdatabasename.Name = "txtdatabasename"
        Me.txtdatabasename.Size = New System.Drawing.Size(284, 21)
        Me.txtdatabasename.TabIndex = 3
        '
        'lblinstanceName
        '
        Me.lblinstanceName.AutoSize = True
        '
        '
        '
        Me.lblinstanceName.BackgroundStyle.Class = ""
        Me.lblinstanceName.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblinstanceName.Location = New System.Drawing.Point(3, 101)
        Me.lblinstanceName.Name = "lblinstanceName"
        Me.lblinstanceName.Size = New System.Drawing.Size(128, 16)
        Me.lblinstanceName.TabIndex = 4
        Me.lblinstanceName.Text = "Instance Name (Optional)"
        '
        'txtinstancename
        '
        '
        '
        '
        Me.txtinstancename.Border.Class = "TextBoxBorder"
        Me.txtinstancename.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtinstancename.Location = New System.Drawing.Point(3, 123)
        Me.txtinstancename.Name = "txtinstancename"
        Me.txtinstancename.Size = New System.Drawing.Size(284, 21)
        Me.txtinstancename.TabIndex = 5
        '
        'lblpassword
        '
        Me.lblpassword.AutoSize = True
        '
        '
        '
        Me.lblpassword.BackgroundStyle.Class = ""
        Me.lblpassword.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblpassword.Location = New System.Drawing.Point(3, 150)
        Me.lblpassword.Name = "lblpassword"
        Me.lblpassword.Size = New System.Drawing.Size(144, 16)
        Me.lblpassword.TabIndex = 6
        Me.lblpassword.Text = "System Admin (sa) Password"
        '
        'txtsystemadmin
        '
        '
        '
        '
        Me.txtsystemadmin.Border.Class = "TextBoxBorder"
        Me.txtsystemadmin.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtsystemadmin.Location = New System.Drawing.Point(293, 3)
        Me.txtsystemadmin.Name = "txtsystemadmin"
        Me.txtsystemadmin.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtsystemadmin.Size = New System.Drawing.Size(284, 21)
        Me.txtsystemadmin.TabIndex = 7
        '
        'btnOK
        '
        Me.btnOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnOK.Location = New System.Drawing.Point(142, 190)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 8
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnCancel.Location = New System.Drawing.Point(223, 190)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 9
        Me.btnCancel.Text = " Cancel"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.lblServername)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtservername)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblDatabasename)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtdatabasename)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblinstanceName)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtinstancename)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblpassword)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtsystemadmin)
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 10)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(292, 174)
        Me.FlowLayoutPanel1.TabIndex = 13
        '
        'ReflectionImage1
        '
        '
        '
        '
        Me.ReflectionImage1.BackgroundStyle.Class = ""
        Me.ReflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ReflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.ReflectionImage1.Image = CType(resources.GetObject("ReflectionImage1.Image"), System.Drawing.Image)
        Me.ReflectionImage1.Location = New System.Drawing.Point(301, 2)
        Me.ReflectionImage1.Name = "ReflectionImage1"
        Me.ReflectionImage1.Size = New System.Drawing.Size(128, 235)
        Me.ReflectionImage1.TabIndex = 14
        '
        'frmLocalSQLCon
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(440, 216)
        Me.ControlBox = False
        Me.Controls.Add(Me.ReflectionImage1)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLocalSQLCon"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SQL Server Configuration"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblServername As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtservername As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents lblDatabasename As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtdatabasename As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents lblinstanceName As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtinstancename As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents lblpassword As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtsystemadmin As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents ReflectionImage1 As DevComponents.DotNetBar.Controls.ReflectionImage

End Class
