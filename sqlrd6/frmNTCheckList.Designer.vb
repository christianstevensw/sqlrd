<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNTCheckList
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNTCheckList))
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.CheckBox4 = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.CheckBox5 = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.CheckBox3 = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.CheckBox2 = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.CheckBox1 = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.btnProceed = New DevComponents.DotNetBar.ButtonX()
        Me.btnCancel = New DevComponents.DotNetBar.ButtonX()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Navy
        Me.Label1.Location = New System.Drawing.Point(1, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(546, 67)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = resources.GetString("Label1.Text")
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.CheckBox4)
        Me.Panel1.Controls.Add(Me.CheckBox5)
        Me.Panel1.Controls.Add(Me.CheckBox3)
        Me.Panel1.Controls.Add(Me.CheckBox2)
        Me.Panel1.Controls.Add(Me.CheckBox1)
        Me.Panel1.Location = New System.Drawing.Point(4, 79)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(552, 156)
        Me.Panel1.TabIndex = 1
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(477, 15)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(53, 50)
        Me.PictureBox1.TabIndex = 3
        Me.PictureBox1.TabStop = False
        '
        'CheckBox4
        '
        Me.CheckBox4.AutoSize = True
        '
        '
        '
        Me.CheckBox4.BackgroundStyle.Class = ""
        Me.CheckBox4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CheckBox4.Location = New System.Drawing.Point(6, 61)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(385, 16)
        Me.CheckBox4.TabIndex = 2
        Me.CheckBox4.Text = "The above user is a member of the LOCAL administrators group on this PC"
        '
        'CheckBox5
        '
        '
        '
        '
        Me.CheckBox5.BackgroundStyle.Class = ""
        Me.CheckBox5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CheckBox5.Location = New System.Drawing.Point(6, 103)
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.Size = New System.Drawing.Size(506, 44)
        Me.CheckBox5.TabIndex = 4
        Me.CheckBox5.Text = "I am not using Terminal Services or XP Remote Control based software during the i" & _
    "nstallation of the Windows service."
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        '
        '
        '
        Me.CheckBox3.BackgroundStyle.Class = ""
        Me.CheckBox3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CheckBox3.Location = New System.Drawing.Point(6, 84)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(334, 16)
        Me.CheckBox3.TabIndex = 3
        Me.CheckBox3.Text = "The above user has Windows security right ""Logon as a service"""
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        '
        '
        '
        Me.CheckBox2.BackgroundStyle.Class = ""
        Me.CheckBox2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CheckBox2.Location = New System.Drawing.Point(6, 38)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(418, 16)
        Me.CheckBox2.TabIndex = 1
        Me.CheckBox2.Text = "The above user has Windows security right ""Act as part of the Operating System"""
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        '
        '
        '
        Me.CheckBox1.BackgroundStyle.Class = ""
        Me.CheckBox1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CheckBox1.Location = New System.Drawing.Point(6, 15)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(358, 16)
        Me.CheckBox1.TabIndex = 0
        Me.CheckBox1.Text = "I am logged into the PC as the user that will run the windows service"
        '
        'btnProceed
        '
        Me.btnProceed.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnProceed.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnProceed.Location = New System.Drawing.Point(391, 241)
        Me.btnProceed.Name = "btnProceed"
        Me.btnProceed.Size = New System.Drawing.Size(75, 23)
        Me.btnProceed.TabIndex = 2
        Me.btnProceed.Text = "&Proceed"
        '
        'btnCancel
        '
        Me.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnCancel.Location = New System.Drawing.Point(472, 241)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "&Cancel"
        '
        'frmNTCheckList
        '
        Me.AcceptButton = Me.btnProceed
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(559, 268)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnProceed)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmNTCheckList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NT Service Checklist"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CheckBox4 As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents CheckBox3 As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents CheckBox2 As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents CheckBox1 As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents CheckBox5 As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents btnProceed As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnCancel As DevComponents.DotNetBar.ButtonX
End Class
