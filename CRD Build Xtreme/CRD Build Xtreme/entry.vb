
Module entry

    Private Const STR_Crystal11 As String = "Crystal11 "
    Private Const STR_Crystal10 As String = "Crystal10 "
    Private Const STR_Crystal9 As String = "Crystal9 "
    Private Const STR_Crystal8 As String = "Crystal8 "
    Private Const STR_Crystal115 As String = "Crystal115 "
    Private Const STR_ProjectName As String = "crd5 "
    Private Const STR_devenvPath As String = Chr(34) & "C:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe" & Chr(34)
    Private Const STR_slnPath As String = Chr(34) & "c:\visual studio projects\crd5\crd5.sln" & Chr(34)
    Private Const STR_pcguardPath As String = Chr(34) & "C:\Program Files\PC Guard for .NET V5\PCGNET.EXE" & Chr(34)
    Private Const STR_assemblyPath As String = "C:\Visual Studio Projects\crd5\bin\crd.exe"
    Private Const STR_pcguardCommand As String = "-process -silent E:\ROOT\CHRISTIANSTEVEN\PCGNET\crd5.prj"
    Private Const STR_Interop8 As String = "interop85.dll"
    Private Const STR_Interop9 As String = "interop90.dll"
    Private Const STR_Interop10 As String = "interop10.dll"
    Private Const STR_Interop11 As String = "interop11.dll"
    Private Const STR_Interop115 As String = "interop115.dll"
    Private Const STR_BuildFolder As String = "C:\Visual Studio Projects\crd5\Builds\"
    Private Const STR_CRDBuilderXtreme As String = "CRD Builder Xtreme"
    Private Const STR_ISPath As String = "C:\Program Files\Macrovision\IS12\System\IsCmdBld.exe"
    Private Const STR_CRDSetup As String = "C:\Visual Studio Projects\crd5\MarsSetup\CRDSetup.ism"
    Private Const STR_MSBuild As String = "C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727\MSBuild.exe"
    Private Const STR_Signwizard As String = Chr(34) & "C:\Program Files\Microsoft Platform SDK for Windows Server 2003 R2\Bin\signtool.exe" & Chr(34)
    Private Const STR_BuildSelector As String = Chr(34) & "C:\Visual Studio Projects\crd5\CRD Build Xtreme\BuildSelector\BuildSelector\bin\BuildSelector.exe" & Chr(34)
    Public procHandler As Process

    Public Enum crystalVersions
        CRYSTAL8 = 8
        CRYSTAL9 = 9
        CRYSTAL10 = 10
        CRYSTAL11 = 11
        CRYSTAL115 = 115
    End Enum
    Private Sub doAction(ByVal crystalVersion As crystalVersions)
        'build crd.exe

10:     ShowMsg("Building assembly...")
20:     BuildAssembly(crystalVersion)


30:     ShowMsg("Protecting assembly...")
40:     ProtectAssembly()

50:     ShowMsg("Signing assembly...")
60:     signAssembly()

70:     ShowMsg("Moving assembly to build folder...")
80:     MoveAssembly(crystalVersion)

    End Sub


    Private Sub signAssembly()
        Dim argument1 As String = " sign /a "
        Dim argument2 As String = " /t http://timestamp.verisign.com/scripts/timstamp.dll "
        'Dim argument3 As String = " /d " & Chr(34) & "ChristianSteven Software Ltd " & Chr(34)
        'Dim argument4 As String = " /du " & Chr(34) & "www.christiansteven.com " & Chr(34)
        Dim argument3 As String = Chr(34) & STR_assemblyPath & Chr(34)

        Dim proc As New Process

        proc.StartInfo.FileName = STR_Signwizard
        proc.StartInfo.Arguments = argument1 & argument2 & argument3
        proc.Start()
        proc.WaitForExit()

    End Sub
    Private Sub BuildAssembly(ByVal crystalVersion As crystalVersions)
        Dim configName As String = ""
        Dim arguments As String

        Select Case crystalVersion
            Case crystalVersions.CRYSTAL8
                configName = STR_Crystal8
            Case crystalVersions.CRYSTAL9
                configName = STR_Crystal9
            Case crystalVersions.CRYSTAL10
                configName = STR_Crystal10
            Case crystalVersions.CRYSTAL11
                configName = STR_Crystal11
            Case crystalVersions.CRYSTAL115
                configName = STR_Crystal115
        End Select


        'Dim enginePath As String = STR_MSBuild
        'arguments = STR_slnPath & " /t:Rebuild /p:" & configName

        arguments = "/rebuild " & configName & "/project " & STR_ProjectName & STR_slnPath

        procHandler = New Process

        Using procHandler
            procHandler.StartInfo.FileName = STR_devenvPath
            procHandler.StartInfo.Arguments = arguments
            procHandler.Start()
            procHandler.WaitForExit()
        End Using

    End Sub

    Private Sub ProtectAssembly()

        procHandler = New Process

        Using procHandler
            procHandler.StartInfo.FileName = STR_pcguardPath
            procHandler.StartInfo.Arguments = STR_pcguardCommand
            procHandler.Start()
            procHandler.WaitForExit()
        End Using

    End Sub
    Private Sub MakeInstaller()
        Dim arguments As String
        Dim c As Char = Chr(34)

        arguments = " -p " & c & STR_CRDSetup & c & " -c comp -r " & c & "Release 1" & c & " -a " & c & "Product Configuration 1" & c

        procHandler = New Process

        Using procHandler
            procHandler.StartInfo.FileName = STR_ISPath
            procHandler.StartInfo.Arguments = arguments
            procHandler.Start()
            procHandler.WaitForExit()
        End Using


    End Sub
    Private Sub MoveAssembly(ByVal crystalVersion As crystalVersions)
        Dim interopName As String = ""


        Select Case crystalVersion
            Case crystalVersions.CRYSTAL8
                interopName = STR_Interop8
            Case crystalVersions.CRYSTAL9
                interopName = STR_Interop9
            Case crystalVersions.CRYSTAL10
                interopName = STR_Interop10
            Case crystalVersions.CRYSTAL11
                interopName = STR_Interop11
            Case crystalVersions.CRYSTAL115
                interopName = STR_Interop115
        End Select

        Dim interopFile As String = STR_BuildFolder & interopName

        If IO.File.Exists(interopFile) = True Then
            Dim result As DialogResult = MessageBox.Show("The file '" & interopFile & "' already exists, replace?", _
            STR_CRDBuilderXtreme, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If result = Windows.Forms.DialogResult.No Then
                Return
            Else
                IO.File.Delete(interopFile)
            End If
        End If

        IO.File.Move(STR_assemblyPath, STR_BuildFolder & interopName)
    End Sub


    Private Sub DoWork()
        Dim startTime As Date = Now

        ShowMsg("Updating build number...")

        Dim p As Process = New Process
        p.StartInfo.FileName = STR_BuildSelector
        p.Start()
        p.WaitForExit()

        System.Threading.Thread.Sleep(1500)

        ShowMsg("Processing assembly for Crystal 8")

        doAction(crystalVersions.CRYSTAL8)


        ShowMsg("Processing assembly for Crystal 9")

        doAction(crystalVersions.CRYSTAL9)


        ShowMsg("Processing assembly for Crystal 10")

        doAction(crystalVersions.CRYSTAL10)


        ShowMsg("Processing assembly for Crystal 11")

        doAction(crystalVersions.CRYSTAL11)

        ShowMsg("Processing assembly for Crystal 11.5")

        doAction(crystalVersions.CRYSTAL115)

        Console.WriteLine("Create installer? y/n")
        Dim create As String = Console.ReadLine

        If create.ToLower = "y" Then
            ShowMsg("Creating assembly for installer")

            BuildAssembly(crystalVersions.CRYSTAL11)

            ShowMsg("Creating installer")
            MakeInstaller()
        End If


        ShowMsg("All assemblies created successfully. Press any key to exit (Build took " & Now.Subtract(startTime).TotalMinutes & " minutes)")

        Console.Read()
        End
    End Sub


    Public Sub ShowMsg(ByVal sMsg1 As String)
        Console.WriteLine(sMsg1 & vbCrLf)
    End Sub

    Sub Main()
        Console.WriteLine("ChristianSteven Builder (CRD) Version 2" & vbCrLf & vbCrLf)

        Console.WriteLine("Delete previous builds? y/n")

        Dim deleteOld As String = Console.ReadLine

        If deleteOld.ToLower = "y" Then
            For Each s As String In My.Computer.FileSystem.GetFiles(STR_BuildFolder)
                If s.EndsWith(".dll") Then
                    IO.File.Delete(s)
                End If
            Next
        End If

        'Console.WriteLine("Have you updated the build number? y/n")

        'Dim buildnum As String = Console.ReadLine

        'If buildnum.ToLower = "n" Then Return

        Console.WriteLine("Begin build process? y/n")

        Dim begin As String = Console.ReadLine

        If begin.ToLower = "y" Then
            DoWork()
        Else
            End
        End If
    End Sub

End Module
