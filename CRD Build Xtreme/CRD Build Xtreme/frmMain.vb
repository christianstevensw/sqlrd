Public Class frmMain
    Private Const STR_Crystal11 As String = "Crystal11 "
    Private Const STR_Crystal10 As String = "Crystal10 "
    Private Const STR_Crystal9 As String = "Crystal9 "
    Private Const STR_Crystal8 As String = "Crystal8 "
    Private Const STR_ProjectName As String = "crd5 "
    Private Const STR_devenvPath As String = Chr(34) & "C:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe" & Chr(34)
    Private Const STR_slnPath As String = Chr(34) & "c:\visual studio projects\crd5\crd5.sln" & Chr(34)
    Private Const STR_pcguardPath As String = Chr(34) & "C:\Program Files\PCGNETC5\PCGNET.EXE" & Chr(34)
    Private Const STR_assemblyPath As String = "C:\Visual Studio Projects\crd5\bin\crd5.exe"
    Private Const STR_pcguardCommand As String = "-process -silent G:\ROOT\CHRISTIANSTEVEN\PCGNET\crd5.prj"
    Private Const STR_Interop8 As String = "interop85.dll"
    Private Const STR_Interop9 As String = "interop90.dll"
    Private Const STR_Interop10 As String = "interop10.dll"
    Private Const STR_Interop11 As String = "interop11.dll"
    Private Const STR_BuildFolder As String = "C:\Visual Studio Projects\crd5\Builds\"
    Private Const STR_CRDBuilderXtreme As String = "CRD Builder Xtreme"
    Private Const STR_ISPath As String = "C:\Program Files\InstallShield 10.5\System\IsCmdBld.exe"
    Private Const STR_CRDSetup As String = "C:\Visual Studio Projects\crd5\MarsSetup\CRDSetup.ism"

    Shared procHandler As Process

    Public Enum crystalVersions
        CRYSTAL8 = 8
        CRYSTAL9 = 9
        CRYSTAL10 = 10
        CRYSTAL11 = 11
    End Enum
    Private Sub doAction(ByVal crystalVersion As crystalVersions)
        'build crd.exe

        ShowMsg("Building assembly...", 33, "")
        BuildAssembly(crystalVersion)


        ShowMsg("Protecting assembly...", 66, "")
        ProtectAssembly()

        ShowMsg("Moving assembly to build folder...", 90, "")
        MoveAssembly(crystalVersion)

    End Sub

    Private Shared Sub BuildAssembly(ByVal crystalVersion As crystalVersions)
        Dim configName As String = ""
        Dim arguments As String

        Select Case crystalVersion
            Case crystalVersions.CRYSTAL8
                configName = STR_Crystal8
            Case crystalVersions.CRYSTAL9
                configName = STR_Crystal9
            Case crystalVersions.CRYSTAL10
                configName = STR_Crystal10
            Case crystalVersions.CRYSTAL11
                configName = STR_Crystal11
        End Select

        arguments = "/rebuild " & configName & "/project " & STR_ProjectName & STR_slnPath

        procHandler = New Process

        Using procHandler
            procHandler.StartInfo.FileName = STR_devenvPath
            procHandler.StartInfo.Arguments = arguments
            procHandler.Start()
            procHandler.WaitForExit()
        End Using

    End Sub

    Private Shared Sub ProtectAssembly()

        procHandler = New Process

        Using procHandler
            procHandler.StartInfo.FileName = STR_pcguardPath
            procHandler.StartInfo.Arguments = STR_pcguardCommand
            procHandler.Start()
            procHandler.WaitForExit()
        End Using

    End Sub
    Private Shared Sub MakeInstaller()
        Dim arguments As String
        Dim c As Char = Chr(34)

        arguments = " -p " & c & STR_CRDSetup & c & " -c comp -r " & c & "Release 1" & c & " -a " & c & "Product Configuration 1" & c

        procHandler = New Process

        Using procHandler
            procHandler.StartInfo.FileName = STR_ISPath
            procHandler.StartInfo.Arguments = arguments
            procHandler.Start()
            procHandler.WaitForExit()
        End Using


    End Sub
    Private Shared Sub MoveAssembly(ByVal crystalVersion As crystalVersions)
        Dim interopName As String = ""


        Select Case crystalVersion
            Case crystalVersions.CRYSTAL8
                interopName = STR_Interop8
            Case crystalVersions.CRYSTAL9
                interopName = STR_Interop9
            Case crystalVersions.CRYSTAL10
                interopName = STR_Interop10
            Case crystalVersions.CRYSTAL11
                interopName = STR_Interop11
        End Select

        Dim interopFile As String = STR_BuildFolder & interopName

        If IO.File.Exists(interopFile) = True Then
            Dim result As DialogResult = MessageBox.Show("The file '" & interopFile & "' already exists, replace?", _
            STR_CRDBuilderXtreme, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If result = Windows.Forms.DialogResult.No Then
                Return
            Else
                IO.File.Delete(interopFile)
            End If
        End If

        IO.File.Move(STR_assemblyPath, STR_BuildFolder & interopName)
    End Sub
    Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LinkLabel1.Click, _
    PictureBox1.Click
        LinkLabel1.Enabled = False
        PictureBox1.Enabled = False

        bg.RunWorkerAsync()
    End Sub

    Private Sub bg_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bg.DoWork
        'PictureBox1.Enabled = False
        'LinkLabel1.Enabled = False

        ShowMsg("", 0, "Processing assembly for Crystal 8")

        doAction(crystalVersions.CRYSTAL8)


        ShowMsg("", 0, "Processing assembly for Crystal 9")

        doAction(crystalVersions.CRYSTAL9)


        ShowMsg("", 0, "Processing assembly for Crystal 10")

        doAction(crystalVersions.CRYSTAL10)


        ShowMsg("", 0, "Processing assembly for Crystal 11")

        doAction(crystalVersions.CRYSTAL11)


        If MessageBox.Show("Create installer?", STR_CRDBuilderXtreme, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            ShowMsg("Creating assembly", 0, "Building installer...")
            BuildAssembly(crystalVersions.CRYSTAL11)

            ShowMsg("Creating installer", 0, "Building installer...")
            MakeInstaller()
        End If

        ShowMsg("", 100, "")

        'PictureBox1.Enabled = True
        'Label1.Enabled = True

        MessageBox.Show("All assemblies created successfully. ", STR_CRDBuilderXtreme, MessageBoxButtons.OK)

        End
    End Sub
    Private Delegate Function SetValuesDelegate(ByVal s As String, ByVal n As Integer, ByVal s1 As String) As Object

    Private Function SetValues(Optional ByVal labelValue As String = "", _
    Optional ByVal progressValue As Integer = 0, Optional ByVal linkValue As String = "") As Object

        If Label1.Text.Length > 0 Then Label1.Text = labelValue

        If progressValue > 0 Then pg.Value = progressValue

        If LinkLabel1.Text.Length > 0 Then LinkLabel1.Text = linkValue

        Application.DoEvents()

        Return Nothing
    End Function

    Public Sub ShowMsg(ByVal sMsg As String, ByVal sValue As Integer, ByVal sMsg1 As String)
        Try
            If InvokeRequired = True Then
                BeginInvoke(New SetValuesDelegate(AddressOf SetValues), New Object() {sMsg, sValue, sMsg1})
                Return
            End If

        Catch ex As Exception
#If DEBUG Then
            MsgBox(ex.Message)
#End If
        End Try
    End Sub


    
    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If MessageBox.Show("Begin build process?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Me.btnGo_Click(sender, e)
        End If
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked

    End Sub
End Class
