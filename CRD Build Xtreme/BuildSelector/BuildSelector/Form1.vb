Imports System.IO

Public Class Form1
    Dim assemblyInfo As String = ""

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.buildDate.SelectionStart = Now.Date
        Me.buildDate.SelectionEnd = Now.Date

        assemblyInfo = My.Settings.assemblyInfo
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Dim input As String = ReadTextFromFile(assemblyInfo)
        Dim startLoc As Integer = 0
        Dim endLoc As Integer = 0
        Dim len As Integer = 0

        
        Do
            Dim buildStr As String = "Build " & Me.buildDate.SelectionStart.ToString("yyyyMMdd")

            startLoc = input.IndexOf("Build 2", startLoc)

            If startLoc = -1 Then Exit Do

            endLoc = input.IndexOf(Chr(34), startLoc)

            len = endLoc - startLoc

            Dim val As String = input.Substring(startLoc, len)
            Dim crVer As String = val.Split("/")(1)

            buildStr &= "/" & crVer

            input = input.Replace(val, buildStr)

            startLoc = startLoc + len
        Loop

        Dim res As Boolean = SaveTextToFile(input, assemblyInfo, , False, False)

        If res = False Then
            MessageBox.Show("Could not overwrite assemblyinfo, make sure it is checked out and try again", "BuildSelector", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            MessageBox.Show("AssemblyInfo updated!", "BuildSelector", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End
        End If
    End Sub

    Public Function SaveTextToFile(ByVal strData As String, _
     ByVal FullPath As String, _
       Optional ByVal ErrInfo As String = "", Optional ByVal Append As Boolean = False, Optional ByVal addCR As Boolean = True) As Boolean

        Dim bAns As Boolean = False
        Dim objReader As StreamWriter

        Try
            If strData.StartsWith(vbCrLf) = False And strData.StartsWith(Environment.NewLine) = False And addCR = True Then
                strData = vbCrLf & strData
            End If

            objReader = New StreamWriter(FullPath, Append)

            objReader.Write(strData)
            'End If

            objReader.Close()
            bAns = True
        Catch Ex As Exception
            ErrInfo = Ex.Message
            bAns = False
        End Try

        Return bAns
    End Function
    Public Function ReadTextFromFile(ByVal sPath As String) As String
        Try

            Dim sr As StreamReader = File.OpenText(sPath)
            Dim sInput As String

            sInput = sr.ReadToEnd

            sr.Close()

            Return sInput
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Function
End Class
