﻿Public Class clsCollaboration
    Dim machineName As String
    Dim _serverID As Integer
    Dim oRs As ADODB.Recordset

    Sub New(ByVal serverName As String)
        m_serverName = serverName

        oRs = clsMarsData.GetData("SELECT * FROM collaboratorsattr WHERE serverName ='" & SQLPrepare(m_serverName) & "'")
    End Sub

    Sub New(ByVal serverID As Integer)
        m_serverID = serverID

        oRs = clsMarsData.GetData("SELECT * FROM collaboratorsattr WHERE collaboID =" & serverID)

    End Sub

    Public Property m_serverName As String
        Get
            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                Return IsNull(oRs("servername").Value, "DEFAULT")
            Else
                Return "DEFAULT"
            End If
        End Get
        Set(ByVal value As String)
            machineName = value
        End Set
    End Property

    Public Property m_serverID As Integer
        Get
            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                Return oRs("collaboID").Value
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            _serverID = value
        End Set
    End Property

    Public ReadOnly Property m_serverPath As String
        Get
            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                Return oRs("serverPath").Value
            Else
                Return ""
            End If
        End Get
    End Property

    Public ReadOnly Property m_username As String
        Get
            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                Return oRs("username").Value
            Else
                Return ""
            End If
        End Get
    End Property

    Public ReadOnly Property m_password As String
        Get
            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                Return _DecryptDBValue(oRs("userpassword").Value)
            Else
                Return ""
            End If
        End Get
    End Property

    Public ReadOnly Property m_domain As String
        Get
            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                Return oRs("domain").Value
            Else
                Return ""
            End If
        End Get
    End Property

    Public ReadOnly Property m_schedulerType As String
        Get
            Try
                Dim type As String = clsMarsUI.MainUI.ReadRegistry("SQL-RDService", "NONE", , m_serverPath, True)

                Return type
            Catch
                Return "NONE"
            End Try
        End Get
    End Property

    Public Sub Dispose()
        oRs.Close()
        oRs = Nothing
    End Sub


    Public Shared Function getCollaborationForSchedule(scheduleID As Integer) As String
        Dim SQL As String = "SELECT collaborationserver FROM scheduleattr WHERE scheduleID = " & scheduleID
        Dim oRsx As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim collaboServer As String = "DEFAULT"

        If oRsx IsNot Nothing AndAlso oRsx.EOF = False Then
            Dim collaboID As Integer = IsNull(oRsx("collaborationserver").Value, 0)
            Dim coll As clsCollaboration = New clsCollaboration(collaboID)
            collaboServer = coll.m_serverName.ToUpper

            oRsx.Close()
        End If

        Return collaboServer
    End Function
End Class
