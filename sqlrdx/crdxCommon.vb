﻿Imports System.Net
Imports System.Runtime.InteropServices
Module crdxCommon
    Dim epx As ErrorProvider = New ErrorProvider
    Public executingForm As frmMainWin
    Public selectedForm As frmMainWin
    Public pWatcher As frmOOPwatcher
    Public gwindows As ArrayList = New ArrayList
    Public dtOOP As DataTable
    Public gIsLoggedIn As Boolean = False
    Public dtFolderSettings As DataTable
    Public dtcolumnFolderSettings As DataTable
    Public dtfolderObjects As DataTable
    Public g_accountInfo As licensex
    Public g_parameterList As ArrayList
    Public g_ProcessID As Integer = Process.GetCurrentProcess.Id
    Public g_noshrinkage As Boolean = False
    Public g_exchangeWDS As exchangeWDS.exchangeMail = Nothing
    <DllImport("user32.dll")> _
    Public Function CreateIconIndirect(ByRef icon As IconInfo) As IntPtr
    End Function

    <DllImport("user32.dll")> _
    Public Function GetIconInfo(hIcon As IntPtr, ByRef pIconInfo As IconInfo) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function
    Public Structure IconInfo
        Public fIcon As Boolean
        Public xHotspot As Integer
        Public yHotspot As Integer
        Public hbmMask As IntPtr
        Public hbmColor As IntPtr
    End Structure

    Public Function newHashTable(label As String, value As String) As Hashtable
        newHashTable = New Hashtable
        newHashTable.Add(label, value)

        Return newHashTable
    End Function

    Public Function showInserter(what As Form, eventID As Integer, eventBased As Boolean) As frmInserter
        Dim inserter As frmInserter = New frmInserter(eventID)

        inserter.m_EventBased = eventBased
        inserter.GetConstants(what)

        Return inserter
    End Function
    Public Function showInserter(what As Form, eventID As Integer) As frmInserter
        Dim inserter As frmInserter = New frmInserter(eventID)

        Dim eventBased As Boolean

        If eventID = 99999 Then
            eventBased = False
        Else
            eventBased = True
        End If

     
        inserter.m_EventBased = eventBased
        inserter.GetConstants(what)

        Return inserter
    End Function

    Private Sub Form_Move(sender As Object, e As EventArgs)
        Dim frm As Form = CType(sender, Form)

        For I As Integer = 0 To frm.OwnedForms.Length - 1
            Dim toolbox As Form = frm.OwnedForms(I)

            Dim left As Integer = (toolbox.Width + 5)
            toolbox.Location = New Point(frm.Left - left, frm.Top)

            toolbox.Left = frm.Left - left
            toolbox.Top = frm.Top - 5
        Next
    End Sub

    Public ReadOnly Property isNewActivationSystem As Boolean
        Get
            Return IO.File.Exists(sAppPath & "regwizx.exe")
        End Get
    End Property

    Public ReadOnly Property getProcessWatcherTable As DataTable
        Get
            If dtOOP IsNot Nothing Then
                Return dtOOP
            Else
                dtOOP = New DataTable

                With dtOOP.Columns
                    .Add("Name")
                    .Add("Type")
                    .Add("PID")
                    .Add("ScheduleID")
                    .Add("Status")
                    .Add("StatusValue")
                    .Add("Argument")
                    .Add("EntryDate")
                End With

                Return dtOOP
            End If
        End Get
    End Property

    Public Sub getDomainAndUserFromString(ByRef userid As String, ByRef domain As String)

        If userid.Contains("\") = False Then
            domain = ""
        Else
            Dim original As String = userid

            '//test value RFK\RJ\Marcia   -- marcia is the userid
            Dim ls As String() = original.Split("\")

            userid = ls(ls.GetUpperBound(0))

            Dim tmp As String

            For I As Integer = 0 To ls.GetUpperBound(0) - 1
                tmp &= ls(I) & "\"
            Next

            If tmp.EndsWith("\") Then tmp = tmp.Remove(tmp.Length - 1, 1)

            domain = tmp
        End If
    End Sub

    Public Sub browseAndPickReportFromServer(ByRef oRpt As Object, ByRef serverUser As String, ByRef serverPassword As String, ByRef txtUrl As TextBox, _
                                             ByRef txtDBLoc As TextBox, ByRef txtName As TextBox, ByRef txtDesc As TextBox, ByRef txtKeywords As Object, _
                                             ByRef cmdNext As DevComponents.DotNetBar.ButtonX, ByRef parameterController As ucParameterController, ByRef lsvDatasources As Object, ByRef formsAuth As Boolean)
        Try
            Dim srsVersion As String = clsMarsReport.m_serverVersion(txtUrl.Text, serverUser, serverPassword, formsAuth)

            ' If srsVersion >= "2009" Then
            ' oRpt = New rsClients.rsClient2010(txtUrl.Text)
            If srsVersion >= "2007" Then
                oRpt = New rsClients.rsClient2008(txtUrl.Text)
            Else
                oRpt = New rsClients.rsClient(txtUrl.Text)
            End If

            'Dim oServerLogin As frmRemoteLogin = New frmRemoteLogin
            'Dim sReturn As String() = oServerLogin.ServerLogin(ServerUser, ServerPassword)
            'Dim cred As System.Net.NetworkCredential

            'we check to see if we should put reportserver2005.asmx in the URL
            If srsVersion >= "2007" Then txtUrl.Text = clsMarsParser.Parser.fixASMXfor2008(txtUrl.Text)

            If txtUrl.Text.Contains("2006.asmx") Then '//its sharepoint mode
                oRpt = New ReportServer_2006.ReportingService2006
            ElseIf txtUrl.Text.Contains("2010.asmx") Then
                oRpt = New rsClients.rsClient2010(txtUrl.Text)
            End If

            oRpt.Url = txtUrl.Text

            Dim sPath As String

            Dim oServer As frmReportServer = New frmReportServer

            sPath = oServer.newGetReports(oRpt, txtUrl.Text, serverUser, serverPassword, txtDBLoc.Text, formsAuth)

            If sPath IsNot Nothing Then
                txtDBLoc.Text = sPath
                txtName.Text = sPath.Split("/")(sPath.Split("/").GetUpperBound(0))
                cmdNext.Enabled = True

                If txtDBLoc.Text.StartsWith("/") = False And (TypeOf oRpt Is ReportServer.ReportingService Or TypeOf oRpt Is ReportServer_2005.ReportingService2005 Or _
                                                              TypeOf oRpt Is ReportServer_2010.ReportingService2010) Then
                    txtDBLoc.Text = "/" & txtDBLoc.Text
                End If
            Else
                Return
            End If

            Dim sRDLPath As String = clsMarsReport.m_rdltempPath & txtName.Text & ".rdl"

            Dim repDef() As Byte

            If IO.File.Exists(sRDLPath) = True Then
                IO.File.Delete(sRDLPath)
            End If

            Dim fsReport As New System.IO.FileStream(sRDLPath, IO.FileMode.Create)

            If TypeOf oRpt Is rsClients.rsClient2010 Then
                If txtDBLoc.Text.ToLower.StartsWith("/http") Then
                    txtDBLoc.Text = txtDBLoc.Text.Remove(0, 1)
                End If

                repDef = oRpt.GetItemDefinition(txtDBLoc.Text)
            Else
                repDef = oRpt.GetReportDefinition(txtDBLoc.Text)
            End If


            fsReport.Write(repDef, 0, repDef.LongLength)

            fsReport.Close()

            'get the parameters
            If parameterController IsNot Nothing Then parameterController.loadParameters(txtUrl.Text, txtDBLoc.Text, Nothing, serverUser, serverPassword, formsAuth)

            'UcDest.m_parameterList = ucparList.m_ParametersCollection
            '    Me.UcBlank.m_ParametersList = Me.m_ParametersList

            lsvDatasources.Items.Clear()

            Dim sData As ArrayList = New ArrayList

            sData = clsMarsReport._GetDatasources(sRDLPath)

            If sData IsNot Nothing Then
                lsvDatasources.Items.Clear()

                For Each s As Object In sData
                    Dim oItem As ListViewItem = lsvDatasources.Items.Add(s)

                    oItem.SubItems.Add("default")

                    oItem.ImageIndex = 0
                    oItem.Tag = 0
                Next
            End If

            txtDesc.Text = clsMarsReport._GetReportDescription(sRDLPath)

            IO.File.Delete(sRDLPath)

            txtName.Focus()
            txtName.SelectAll()

            cmdNext.Enabled = True

            'save the Url as it has been validated
            logUrlHistory(txtUrl.Text)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), _
            "Please check your report server's web service URL and try again")

            If ex.Message.ToLower.Contains("cannot be found") Then
                txtDBLoc.Text = ""
                txtName.Text = ""
                txtDesc.Text = ""
            End If
        End Try
    End Sub

    Public Sub setError(ByVal ctrl As Control, ByVal message As String, Optional ByVal alignment As ErrorIconAlignment = ErrorIconAlignment.MiddleRight)
        epx.SetIconAlignment(ctrl, alignment)
        epx.SetError(ctrl, message)

        Try
            ctrl.Focus()
        Catch :End Try
    End Sub

    Public Sub setError(ByVal ep As ErrorProvider, ByVal ctrl As Control, ByVal message As String, Optional ByVal alignment As ErrorIconAlignment = ErrorIconAlignment.MiddleRight)
        epx.SetIconAlignment(ctrl, alignment)
        epx.SetError(ctrl, message)
    End Sub

    Public Sub removeError(ByVal ep As ErrorProvider, ByVal ctrl As Control)
        epx.SetError(ctrl, "")
    End Sub

    Private Sub TextBox_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.F8 Then
            If e.KeyCode = Keys.F8 Then
                Dim ctrl As Control = CType(sender, Control).Parent

                Do While Not (TypeOf ctrl.Parent Is Form)
                    ctrl = ctrl.Parent
                Loop

                Dim frm As Form = ctrl.Parent

                Dim inserter As frmInserter = New frmInserter(99999)
                inserter.Parent = frm
                inserter.Show()

            End If
        End If
    End Sub
    Public Sub setupForDragAndDrop(ByVal ctrl As Control)
        ctrl.AllowDrop = True

        AddHandler ctrl.DragEnter, AddressOf ctrl_DragEnter
        AddHandler ctrl.DragDrop, AddressOf ctrl_DragDrop


        Try
            If TypeOf ctrl Is DevComponents.DotNetBar.Controls.TextBoxX Then
                CType(ctrl, DevComponents.DotNetBar.Controls.TextBoxX).Border.CornerDiameter = 2
                CType(ctrl, DevComponents.DotNetBar.Controls.TextBoxX).Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
            ElseIf TypeOf ctrl Is UrielGuy.SyntaxHighlightingTextBox.SyntaxHighlightingTextBox Then
                AddHandler ctrl.DragOver, AddressOf rtf_DragOver
            End If
        Catch : End Try
    End Sub

    Private Sub rtf_DragOver(sender As Object, e As DragEventArgs)
        If (e.AllowedEffect And DragDropEffects.Copy) = DragDropEffects.Copy Then

            ' By default, the drop action should be move, if allowed.
            e.Effect = DragDropEffects.Copy
        End If
    End Sub

    Public Sub ctrl_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs)
        e.Effect = DragDropEffects.None

        Dim val As String = CType(e.Data.GetData(DataFormats.StringFormat, True), String)


        If val.StartsWith("<[") Then
            If IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.i1_Inserts) = False And _
                IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.s3_EventBasedScheds) = False And _
                IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.s5_DataDrivenSched) = False Then
                _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE, sender, "Inserts")

                Return
            End If
        End If

        If val = "<[u]...New...>" Then
            Dim formEditor As frmFormulaEditor = New frmFormulaEditor
            Dim name As String = formEditor.AddConstant

            If name IsNot Nothing Then
                val = "<[u]" & name & ">"
            Else
                Return
            End If
        ElseIf val = "<[d]...New...>" Then
            Dim newDataItem As frmEditDataItem = New frmEditDataItem

            val = frmEditDataItem.AddDataItem

            If val IsNot Nothing Then
                val = "<[d]" & val & ">"
            Else
                Return
            End If
        End If

        If TypeOf sender Is TextBox Then
            Dim txt As TextBox = CType(sender, TextBox)

            txt.SelectedText = val
        ElseIf TypeOf sender Is UrielGuy.SyntaxHighlightingTextBox.SyntaxHighlightingTextBox Then
            Dim txt As UrielGuy.SyntaxHighlightingTextBox.SyntaxHighlightingTextBox = CType(sender, UrielGuy.SyntaxHighlightingTextBox.SyntaxHighlightingTextBox)

            txt.SelectedText = val
        ElseIf TypeOf sender Is DevComponents.DotNetBar.Controls.TextBoxX Then
            Dim txt As TextBox = CType(sender, DevComponents.DotNetBar.Controls.TextBoxX)

            txt.SelectedText = val
        ElseIf TypeOf sender Is ComboBox Then
            Dim cmb As ComboBox = CType(sender, ComboBox)

            cmb.Text = val
        ElseIf TypeOf sender Is DevComponents.DotNetBar.Controls.ComboBoxEx Then
            Dim cmb As ComboBox = CType(sender, DevComponents.DotNetBar.Controls.ComboBoxEx)

            cmb.Text = val
        ElseIf TypeOf sender Is SpiceLogic.WinHTMLEditor.HTMLEditor Then
            Dim htmlEd As SpiceLogic.WinHTMLEditor.HTMLEditor = CType(sender, SpiceLogic.WinHTMLEditor.HTMLEditor)

            htmlEd.InsertHtml(val)
        Else
            sender.Text = val
        End If
    End Sub

    Public Function CreateCursor(bmp As Bitmap, xHotSpot As Integer, yHotSpot As Integer) As Cursor
        Dim tmp As New IconInfo()
        GetIconInfo(bmp.GetHicon(), tmp)
        tmp.xHotspot = xHotSpot
        tmp.yHotspot = yHotSpot
        tmp.fIcon = False
        Return New Cursor(CreateIconIndirect(tmp))
    End Function

    Private Sub ctrl_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs)
        If e.Data.GetDataPresent(DataFormats.StringFormat, True) Then
            e.Effect = DragDropEffects.Copy

            Dim size As SizeF
            Dim f As New Font("Tahoma", 8, FontStyle.Regular)
            Dim str As String = CType(e.Data.GetData(DataFormats.StringFormat, True), String)

            Using tmpBmp As New Bitmap(1, 1)
                Using g As Graphics = Graphics.FromImage(tmpBmp)
                    size = g.MeasureString(str, f)
                End Using
            End Using

            Dim bitmap As New Bitmap(CInt(Math.Ceiling(size.Width)), CInt(Math.Ceiling(size.Height)))
            Using g As Graphics = Graphics.FromImage(bitmap)
                g.DrawString(str, f, Brushes.Blue, 0, 0)
            End Using

            Cursor.Current = CreateCursor(bitmap, 0, 0)

            bitmap.Dispose()
            f.Dispose()
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub


    Public Sub setAndShowMessageOnControl(ByVal ctrl As Control, ByVal header As String, ByVal footer As String, ByVal message As String)
        Dim sp As DevComponents.DotNetBar.SuperTooltip = New DevComponents.DotNetBar.SuperTooltip
        Dim spi As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo(header, footer,
                                                                                                          message,
                                                                                                           Nothing,
                                                                                                           Nothing,
                                                                                                           DevComponents.DotNetBar.eTooltipColor.Office2003)
        sp.SetSuperTooltip(ctrl, spi)
        sp.ShowTooltip(ctrl)

        System.Threading.Thread.Sleep(1000)

        sp.SetSuperTooltip(ctrl, Nothing)
    End Sub

    Public Function getAssetLocation(ByVal assetName) As String

        Return IO.Path.Combine(Application.StartupPath, "Assets\" & assetName)
    End Function
    ''' <summary>
    ''' Looks for the provided filenames in the assets folder
    ''' </summary>
    ''' <param name="images"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function createImageList(ByVal images() As String) As ImageList
        Dim imgL As ImageList = New ImageList
        imgL.ImageSize = New Size(16, 16)
        imgL.ColorDepth = ColorDepth.Depth32Bit

        For Each imageFile As String In images
            imgL.Images.Add(Image.FromFile(getAssetLocation(imageFile)))
        Next

        Return imgL
    End Function

    Public Function getScheduleTypeString(ByVal ntype As clsMarsScheduler.enScheduleType) As String
        Select Case ntype
            Case clsMarsScheduler.enScheduleType.REPORT
                Return "report"
            Case clsMarsScheduler.enScheduleType.PACKAGE
                Return "package"
            Case clsMarsScheduler.enScheduleType.AUTOMATION
                Return "automation"
            Case clsMarsScheduler.enScheduleType.EVENTBASED
                Return "event"
            Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                Return "event-package"
            Case clsMarsScheduler.enScheduleType.SMARTFOLDER
                Return "smartfolder"
            Case clsMarsScheduler.enScheduleType.FOLDER
                Return "folder"
            Case Else
                Return "none"
        End Select
    End Function
    Public Function getScheduleTypeValue(ByVal stype As String) As clsMarsScheduler.enScheduleType
        Select Case stype.ToLower
            Case "report" : Return clsMarsScheduler.enScheduleType.REPORT
            Case "package" : Return clsMarsScheduler.enScheduleType.PACKAGE
            Case "automation" : Return clsMarsScheduler.enScheduleType.AUTOMATION
            Case "event" : Return clsMarsScheduler.enScheduleType.EVENTBASED
            Case "event-package" : Return clsMarsScheduler.enScheduleType.EVENTPACKAGE
            Case "smartfolder" : Return clsMarsScheduler.enScheduleType.SMARTFOLDER
            Case "folder" : Return clsMarsScheduler.enScheduleType.FOLDER
            Case Else
                Return clsMarsScheduler.enScheduleType.NONE
        End Select
    End Function

    Public Sub convertBlankAlertToTasks()
        Try
            Dim alreadyConverted As Boolean = Convert.ToInt16(clsMarsUI.MainUI.ReadRegistry("ForExceptionProcessed", 0))

            If alreadyConverted Then Return

            Dim SQL As String = " SELECT * FROM BlankReportAlert WHERE [type] LIKE 'Alert' OR [type] LIKE 'AlertandReport'"
            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                Do While oRs.EOF = False
                    Dim SQL1 As String
                    Dim sCols As String
                    Dim sVals As String
                    Dim oData As clsMarsData = New clsMarsData
                    Dim oTask As clsMarsTask = New clsMarsTask
                    Dim nID As Integer = clsMarsData.CreateDataID("tasks", "taskid")
                    Dim reportID, packID As Integer
                    Dim scheduleID As Integer
                    Dim blankID As Integer = oRs("blankid").Value
                    Dim blankType As String = oRs("type").Value

                    clsMarsData.WriteData("DELETE FROM tasks WHERE tasktype ='sendmail' AND taskname ='Blank Report Email Alert' AND sendto ='' AND cc ='' AND bcc =''", False)

                    sCols = "TaskID,ScheduleID,TaskType,TaskName," & _
                    "SendTo,Cc,Bcc,Msg,FileList,Subject,ProgramPath,PrinterName,SenderName,SenderAddress,OrderID,forexception"

                    reportID = IsNull(oRs("reportid").Value, 0)
                    packID = IsNull(oRs("packid").Value, 0)

                    If packID <> 0 Then
                        scheduleID = packID

                    ElseIf reportID <> 0 Then
                        scheduleID = reportID
                    Else
                        GoTo skip '//a little of me die whenever I use this
                    End If

                    Dim oTest As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM tasks WHERE scheduleid =" & scheduleID)

                    If oTest IsNot Nothing AndAlso oTest.EOF = False Then
                        oTest.Close()
                        oTest = Nothing

                        GoTo skip
                    End If

                    Dim sto, cc, bcc As String

                    sto = IsNull(oRs("to").Value, "")

                    If sto = "" Then GoTo skip

                    'program path used for mail format
                    'printername used for smtpserver

                    sVals = nID & "," & _
                    scheduleID & "," & _
                    "'SendMail'," & _
                    "'Blank Report Email Alert'," & _
                    "'" & SQLPrepare(sto) & "'," & _
                    "''," & _
                    "''," & _
                    "'" & SQLPrepare(IsNull(oRs("msg").Value)) & "'," & _
                    "''," & _
                    "'" & SQLPrepare(IsNull(oRs("subject").Value)) & "'," & _
                    "'TEXT'," & _
                    "'DEFAULT'," & _
                    "''," & _
                    "''," & _
                    oTask.GetOrderID(scheduleID) & ",1"


                    SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

                    clsMarsData.WriteData(SQL)

                    If blankType.ToLower = "alertandreport" Then
                        clsMarsData.WriteData("UPDATE blankreportalert SET [type] = 'AlertandReport' WHERE blankid = " & blankID)
                    Else
                        clsMarsData.WriteData("UPDATE blankreportalert SET [type] = 'Ignore' WHERE blankid = " & blankID)
                    End If
skip:

                    oRs.MoveNext()
                Loop

                oRs.Close()

                oRs = Nothing
            End If

            ' clsMarsData.WriteData("UPDATE tasks SET forexception = 1 WHERE scheduleid IN (SELECT reportid FROM reportattr WHERE checkblank = 1) AND forexception IS NULL", False)
            'clsMarsData.WriteData("UPDATE tasks SET forexception = 1 WHERE scheduleid IN (SELECT packid FROM packageattr WHERE checkblank = 1) AND forexception IS NULL", False)
            clsMarsData.WriteData("UPDATE tasks SET forexception = 0 WHERE forexception IS NULL", False)

            clsMarsUI.MainUI.SaveRegistry("ForExceptionProcessed", 1)
        Catch : End Try
    End Sub

    Public Function SystemCopyFile(source As String, destination As String, replace As Boolean, Optional keepBoth As Boolean = False) As Boolean
        If keepBoth And IO.File.Exists(destination) Then
            Dim dir As String = IO.Path.GetDirectoryName(destination)
            Dim fileName As String = IO.Path.GetFileNameWithoutExtension(destination)
            Dim ext As String = IO.Path.GetExtension(destination)
            Dim count As Integer = 1

            Dim newFile As String = String.Format("{0}\{1}({2}){3}", dir, fileName, count, ext)

            Do While IO.File.Exists(newFile)
                count += 1

                newFile = String.Format("{0}\{1}({2}){3}", dir, fileName, count, ext)
            Loop

            destination = newFile

        ElseIf replace Then
            If IO.File.Exists(destination) Then
                IO.File.Delete(destination)
            End If
        End If

        IO.File.Copy(source, destination)

        System.Threading.Thread.Sleep(250)

        If IO.File.Exists(destination) = False Then
            Throw New Exception("File copy failed with unknown error. Please make sure that you have full permissions to the destination directory.")
        End If
    End Function

End Module
