﻿Public Class clsSharePointHelper
    Dim helper As String = IO.Path.Combine(Application.StartupPath, "spointaccess.exe")
    Dim sharePointAccessFolder As String = IO.Path.Combine(sAppDataPath, "SharePointAccessHelper")

    Sub New()
        If IO.Directory.Exists(sharePointAccessFolder) = False Then
            IO.Directory.CreateDirectory(sharePointAccessFolder)
        End If
    End Sub
    Public Function UploadMultipleDocuments(url As String, library As String, files() As String, username As String, password As String, isoffice365 As Boolean, ByRef errorInfo As Exception) As Boolean
        Dim resultLoc As String
        Dim commandFileLoc As String

        Try
            Dim commandData As DataTable = New DataTable("command")

            With commandData.Columns
                .Add("command")
                .Add("commandparameters")
                .Add("resultlocation")
            End With

            Dim filesList As String

            For Each s As String In files
                If s <> "" Then filesList &= s & ">"
            Next

            If filesList.Length > 0 Then filesList = filesList.Remove(filesList.Length - 1, 1)
            resultLoc = IO.Path.Combine(sharePointAccessFolder, String.Format("{0}.{1}", Guid.NewGuid.ToString, ".xml"))
            commandFileLoc = IO.Path.Combine(sharePointAccessFolder, String.Format("{0}.{1}", Guid.NewGuid.ToString, ".xml"))

            '//build the command
            Dim x As DataRow = commandData.Rows.Add
            x("command") = "uploadmultipledocuments"
            x("commandparameters") = String.Format("serverurl:={0}|username:={1}|password:={2}|library:={3}|metadata:={4}|isoffice365:={5}|localfiles:={6}", url, username, password, library, "", isoffice365, filesList)
            x("resultlocation") = resultLoc

            'save the command to disk
            commandData.WriteXml(commandFileLoc, XmlWriteMode.WriteSchema)

            'send it tot the helper
            Dim proc As Process = New Process
            proc.StartInfo.FileName = helper
            proc.StartInfo.Arguments = String.Format("{0}{1}{0}", Chr(34), commandFileLoc)
            proc.Start()
            proc.WaitForExit()

            'read the result
            If IO.File.Exists(resultLoc) Then
                Dim dt As DataTable = New DataTable

                dt.ReadXml(resultLoc)

                If dt.Rows.Count > 0 Then
                    Dim r As DataRow = dt.Rows(0)
                    Dim ok As Boolean = r("success")
                    Dim resultString As String = r("resultstring")
                    Dim errorMessage As String = r("errormessage")

                    If ok = False Then
                        Throw New Exception(errorMessage)
                    Else
                        Return True
                    End If
                Else
                    Throw New Exception("SharepointHelper is unable to complete the operation")
                End If
            Else
                Throw New Exception("SharepointHelper is unable to complete the operation")
            End If
        Catch ex As Exception
            errorInfo = ex
            Return False
        Finally
            Try
                IO.File.Delete(resultLoc)
                IO.File.Delete(commandFileLoc)
            Catch :End Try
        End Try
    End Function

    Public Function UploadDocument(ByVal serverUrl As String, ByVal Library As String, ByVal localFile As String, ByVal userName As String, _
    ByVal password As String, ByRef errorInfo As Exception, ByVal metaData As String, Optional isOffice365 As Boolean = False) As Boolean
        Dim resultLoc As String
        Dim commandFileLoc As String
        Try
            Dim commandData As DataTable = New DataTable("command")
            resultLoc = IO.Path.Combine(sharePointAccessFolder, String.Format("{0}.{1}", Guid.NewGuid.ToString, ".xml"))
            commandFileLoc = IO.Path.Combine(sharePointAccessFolder, String.Format("{0}.{1}", Guid.NewGuid.ToString, ".xml"))

            With commandData.Columns
                .Add("command")
                .Add("commandparameters")
                .Add("resultlocation")
            End With

            Dim x As DataRow = commandData.Rows.Add
            x("command") = "uploaddocument"
            x("commandparameters") = String.Format("serverurl:={0}|username:={1}|password:={2}|library:={3}|metadata:={4}|isoffice365:={5}|localfile:={6}", serverUrl, userName, password, Library, metaData, isOffice365, localFile)
            x("resultlocation") = resultLoc

            'save the command to disk
            commandData.WriteXml(commandFileLoc, XmlWriteMode.WriteSchema)

            'send it tot the helper
            Dim proc As Process = New Process
            proc.StartInfo.FileName = helper
            proc.StartInfo.Arguments = String.Format("{0}{1}{0}", Chr(34), commandFileLoc)
            proc.Start()
            proc.WaitForExit()

            'read the result
            If IO.File.Exists(resultLoc) Then
                Dim dt As DataTable = New DataTable

                dt.ReadXml(resultLoc)

                If dt.Rows.Count > 0 Then
                    Dim r As DataRow = dt.Rows(0)
                    Dim ok As Boolean = r("success")
                    Dim resultString As String = r("resultstring")
                    Dim errorMessage As String = r("errormessage")

                    If ok = False Then
                        Throw New Exception(errorMessage)
                    Else
                        Return True
                    End If
                Else
                    Throw New Exception("SharepointHelper is unable to complete the operation")
                End If
            Else
                Throw New Exception("SharepointHelper is unable to complete the operation")
            End If
        Catch ex As Exception
            errorInfo = ex
            Return False
        Finally
            Try
                IO.File.Delete(resultLoc)
                IO.File.Delete(commandFileLoc)
            Catch : End Try
        End Try
    End Function

    Public Function GetSharePointLib(ByRef url As String, ByRef username As String, ByRef password As String, ByRef library As String, Optional ByRef metadata As String = "", Optional ByVal showmeta As Boolean = True, Optional ByRef isOffice365 As Boolean = False) As Boolean
        Dim resultLoc As String
        Dim commandFileLoc As String
        Try
            Dim commandData As DataTable = New DataTable("command")
            resultLoc = IO.Path.Combine(sharePointAccessFolder, String.Format("{0}.{1}", Guid.NewGuid.ToString, ".xml"))
            commandFileLoc = IO.Path.Combine(sharePointAccessFolder, String.Format("{0}.{1}", Guid.NewGuid.ToString, ".xml"))

            With commandData.Columns
                .Add("command")
                .Add("commandparameters")
                .Add("resultlocation")
            End With

            Dim x As DataRow = commandData.Rows.Add
            x("command") = "getsharepointlib"
            x("commandparameters") = String.Format("url:={0}|username:={1}|password:={2}|library:={3}|metadata:={4}|isoffice365:={5}|showmeta:={6}", url, username, password, library, metadata, isOffice365, showmeta)
            x("resultlocation") = resultLoc

            commandData.WriteXml(commandFileLoc, XmlWriteMode.WriteSchema)

            'send it tot the helper
            Dim proc As Process = New Process
            proc.StartInfo.FileName = helper
            proc.StartInfo.Arguments = String.Format("{0}{1}{0}", Chr(34), commandFileLoc)
            proc.Start()
            proc.WaitForExit()

            'read the result
            If IO.File.Exists(resultLoc) Then
                Dim dt As DataTable = New DataTable

                dt.ReadXml(resultLoc)

                If dt.Rows.Count > 0 Then
                    Dim r As DataRow = dt.Rows(0)
                    Dim ok As Boolean = r("success")
                    Dim resultString As String = r("resultstring")
                    Dim errorMessage As String = r("errormessage")

                    If ok = False Then
                        Throw New Exception(errorMessage)
                    Else
                        'url:=https://christianstevenus.sharepoint.com|username:=steven@christiansteven.com|password:=Kamuzu64|library:=Document Library/Amani/SubFolder Test|metadata:=|showmeta:=True|isoffice365:=True
                        For Each parameter As String In resultString.Split("|")
                            Dim field = System.Text.RegularExpressions.Regex.Split(parameter, ":=")(0)
                            Dim value = System.Text.RegularExpressions.Regex.Split(parameter, ":=")(1)

                            Select Case field.ToLower
                                Case "url"
                                    url = value
                                Case "username"
                                    username = value
                                Case "password"
                                    password = value
                                Case "library"
                                    library = value
                                Case "metadata"
                                    metadata = value
                                Case "showmeta"
                                    showmeta = value
                                Case "isoffice365"
                                    isOffice365 = value
                            End Select
                        Next

                        Return True
                    End If
                Else
                    Throw New Exception("SharepointHelper is unable to complete the operation")
                End If
            Else
                Throw New Exception("SharepointHelper is unable to complete the operation")
            End If

            Return True
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 0)
            Return False
        Finally
            Try
                IO.File.Delete(resultLoc)
                IO.File.Delete(commandFileLoc)
            Catch : End Try
        End Try
    End Function
End Class
