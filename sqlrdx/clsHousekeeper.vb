﻿Imports System.Xml

Public Class clsHousekeeper
    Property directoryType As String

    Property age As Integer

    Property ageUnit As String

    Property selectedPath As String

    Property includeSubfolders As Boolean



    Public Function houseKeepLocalDirectory()
        Dim oParse As New clsMarsParser

        selectedPath = oParse.ParseString(selectedPath)


        clsSystemTools.deleteFilesInfolder(selectedPath, age, ageUnit, includeSubfolders)
    End Function

    Public Function housekeepFTPDir(ftpServer As String, ftpUser As String, ftpPassword As String, ftpPassive As Boolean, ftpPort As Integer, ftpOptions As String, ftpType As String)
        Dim ftpCtrl As ucFTPDetails = New ucFTPDetails
        Dim oFtp As Object
        Dim ok As Boolean
        Dim sDirectory As String = selectedPath
        Dim ftpException As Exception

        With ftpCtrl
            .txtFTPServer.Text = ftpServer
            .txtUserName.Text = ftpUser
            .txtPassword.Text = ftpPassword
            .cmbFTPType.Text = ftpType
            .txtPort.Value = ftpPort
            .m_ftpOptions = ftpOptions
            oFtp = .ConnectFTP()
        End With

        '//turn on logging
        If TypeOf oFtp Is Chilkat.Ftp2 AndAlso oFtp IsNot Nothing Then
            oFtp.KeepSessionLog = True
        Else
            AddHandler CType(oFtp, Rebex.Net.Sftp).ResponseRead, AddressOf ResponseRead
            AddHandler CType(oFtp, Rebex.Net.Sftp).CommandSent, AddressOf CommandSent
        End If

        If sDirectory.EndsWith("/") = False Then
            sDirectory = sDirectory & "/"
        End If

        '//change to the required directory
        If selectedPath.Length > 0 Then
            If TypeOf oFtp Is Chilkat.Ftp2 Then

                Dim ftp As Chilkat.Ftp2 = oFtp

                ok = ftp.ChangeRemoteDir(sDirectory)

                If ok = False Then
                    For Each s As String In sDirectory.Split("/")
                        If s.Length > 0 Then
                            ok = ftp.ChangeRemoteDir(s)

                            If ok = False Then
                                If ftp.CreateRemoteDir(s) = True Then
                                    _Delay(1)
                                    ftp.ChangeRemoteDir(s)
                                Else
                                    Throw New Exception(ftp.LastErrorText)
                                End If
                            End If
                        End If
                    Next
                End If

                If recurseFTPDelete(ftp, ftpException) = False Then
                    Throw ftpException
                End If
            Else
                Dim ftp As Rebex.Net.Sftp = oFtp

                Try
                    ftp.ChangeDirectory(sDirectory)
                Catch
                    For Each s As String In sDirectory.Split("/")
                        If s.Length > 0 Then
                            Try
                                oFtp.ChangeDirectory(s)
                            Catch ex As Exception
                                oFtp.CreateDirectory(s)
                                _Delay(1)
                                oFtp.ChangeDirectory(s)
                            End Try

                        End If
                    Next
                End Try

                If recurseFTPDelete(ftp, ftpException) = False Then
                    Throw ftpException
                End If
            End If
        End If

        '//get file list

    End Function

    Private Sub ResponseRead(ByVal sender As Object, ByVal e As Rebex.Net.SftpResponseReadEventArgs)
        clsMarsDebug.writeToDebug("ftp_" & gScheduleName, " -> " & e.Response, True)
    End Sub 'ResponseRead

    Private Sub CommandSent(ByVal sender As Object, ByVal e As Rebex.Net.SftpCommandSentEventArgs)
        clsMarsDebug.writeToDebug("ftp_" & gScheduleName, " <- " & e.Command, True)
    End Sub 'ResponseRead
    Private Function recurseFTPDelete(ftp As Rebex.Net.Sftp, ByRef errInfo As Exception) As Boolean
        Try
            '//files 
            For Each item As Rebex.Net.SftpItem In ftp.GetList
                If item.IsFile Then
                    Dim fulDate As Date = item.Modified

                    Dim diff As Integer

                    Select Case ageUnit.ToLower
                        Case "seconds"
                            diff = Date.Now.Subtract(fulDate).TotalSeconds
                        Case "minutes"
                            diff = Date.Now.Subtract(fulDate).TotalMinutes
                        Case "hours"
                            diff = Date.Now.Subtract(fulDate).TotalHours
                        Case "days"
                            diff = Date.Now.Subtract(fulDate).TotalDays
                    End Select

                    If diff > age Then
                        ftp.DeleteFile(item.Name)
                    End If
                End If
            Next

            If includeSubfolders Then
                Dim tmp As String = ftp.GetCurrentDirectory

                For Each item As Rebex.Net.SftpItem In ftp.GetList
                    If item.IsDirectory Then
                        ftp.ChangeDirectory(ftp.GetCurrentDirectory & "/" & item.Name)

                        recurseFTPDelete(ftp, errInfo)

                        ftp.ChangeDirectory(tmp)
                    End If
                Next
            End If

            Return True
        Catch ex As Exception
            errInfo = ex
            Return False
        End Try

    End Function


    Private Function recurseFTPDelete(ftp As Chilkat.Ftp2, ByRef errInfo As Exception) As Boolean
        Try
            Dim doc As XmlDocument
            doc = New XmlDocument
            doc.LoadXml(ftp.GetXmlDirListing("*.*"))

            Dim oNodes As XmlNodeList
            Dim oNode As XmlNode
            Dim folders As ArrayList = New ArrayList

            '//check the files
            oNodes = doc.GetElementsByTagName("file")

            For Each oNode In oNodes
                Dim name As String

                name = oNode.ChildNodes(0).InnerText

                Dim fmonth, fdate, fyear, fhour, fminute As Integer

                fmonth = oNode.ChildNodes(2).Attributes("m").Value
                fdate = oNode.ChildNodes(2).Attributes("d").Value
                fyear = oNode.ChildNodes(2).Attributes("y").Value
                fhour = oNode.ChildNodes(2).Attributes("hh").Value
                fminute = oNode.ChildNodes(2).Attributes("mm").Value

                Dim fulDate As Date = New Date(fyear, fmonth, fdate, fhour, fminute, 0)
                Dim diff As Integer

                Select Case ageUnit.ToLower
                    Case "seconds"
                        diff = Date.Now.Subtract(fulDate).TotalSeconds
                    Case "minutes"
                        diff = Date.Now.Subtract(fulDate).TotalMinutes
                    Case "hours"
                        diff = Date.Now.Subtract(fulDate).TotalHours
                    Case "days"
                        diff = Date.Now.Subtract(fulDate).TotalDays
                End Select

                If diff > age Then
                    If ftp.DeleteRemoteFile(name) = False Then
                        Throw New Exception(ftp.LastErrorText)
                    End If
                End If
            Next

            If includeSubfolders Then
                Dim tmp As String = ftp.GetCurrentRemoteDir

                oNodes = doc.GetElementsByTagName("dir")

                For Each oNode In oNodes
                    ftp.ChangeRemoteDir(oNode.InnerText)

                    recurseFTPDelete(ftp, errInfo)

                    ftp.ChangeRemoteDir(tmp)
                Next
            End If

            Return True
        Catch ex As Exception
            errInfo = ex
            Return False
        End Try
    End Function
End Class
