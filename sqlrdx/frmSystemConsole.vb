﻿Public Class frmSystemConsole

    Private Sub btnExecute_Click(sender As Object, e As EventArgs) Handles btnExecute.Click
        If txtQuery.Text.Trim.ToLower.StartsWith("select") Then
            Dim ex As Exception
            Dim conString = ""

            If gConType = "DAT" Then
                conString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sAppPath & "crdlive.dat" & ";Persist Security Info=False"
            Else
                'don't just decrypt it as it may not be encrypted...
                conString = clsMarsUI.MainUI.ReadRegistry("ConString", "", True)
            End If




            Dim ors As ADODB.Recordset = New ADODB.Recordset
            Dim con As ADODB.Connection = New ADODB.Connection

            con.Open(conString)

            Try
                ors.Open(txtQuery.Text, con, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                Dim oResult As New frmDBResults

                oResult._ShowResults(ors)

                'ors.Close()
                con.Close()

                ors = Nothing
                con = Nothing
            Catch ex1 As Exception
                _ErrorHandle(ex1.Message, 0, Reflection.MethodBase.GetCurrentMethod.Name, 0)
            End Try

            'If txtQuery.Text.Trim.ToLower.StartsWith("update") Or txtQuery.Text.Trim.ToLower.StartsWith("delete") Or txtQuery.Text.Trim.ToLower.StartsWith("insert") Then
        Else
            Dim res As DialogResult = MessageBox.Show("You are about to run a query that modifies the data in the backend database and this cannot be undone. Proceed?", _
                                                         Application.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)

            If res = Windows.Forms.DialogResult.Cancel Then
                Return
            Else
                If clsMarsData.WriteData(txtQuery.Text) = True Then
                    MessageBox.Show("The query was executed successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        End If
    End Sub
End Class