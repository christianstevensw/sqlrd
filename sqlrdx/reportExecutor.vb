﻿Public Class reportExecutor
    Property m_url As String
    Property m_reportid As Integer
    Property m_serverUser As String
    Property m_serverPassword As String
    Property m_asmxUrl As String

    Dim rs As ReportExecution2005.ReportExecutionService
    Dim m_KeyParameter As String = ""
    Dim m_KeyValue As String = ""
    Dim m_reportPath As String
    Dim m_ParametersTable As Hashtable
    Dim m_formsAuth As Boolean

    Sub New(reportid As Integer, serverUrl As String, reportPath As String)
        m_url = serverUrl
        m_reportid = reportid
        m_reportPath = reportPath

        m_asmxUrl = serverUrl

        Dim rpt As cssreport = New cssreport(reportid)
        m_serverUser = rpt.reportuserid
        m_serverPassword = rpt.reportuserpassword
        m_formsAuth = rpt.usesFormsAuth

    End Sub

    Public Function SetReportParameters() As ReportExecution2005.ParameterValue()
10:     Try
            Dim SQL As String
            Dim ParName As String = ""
            Dim ParValue As String = ""
            Dim parNull As Boolean = False
            Dim ParType As Integer
            Dim sQuotes As String = String.Empty
20:         Dim oParse As New clsMarsParser
30:         Dim oDynamic As New clsMarsDynamic
            Dim I As Integer = 0
            Dim oPar As ReportExecution2005.ParameterValue()
            Dim Z As Integer

            m_ParametersTable = New Hashtable

            Dim rptInfo As ReportExecution2005.ExecutionInfo = rs.LoadReport(m_reportPath, Nothing)
            Dim parameterList As ReportExecution2005.ReportParameter() = rptInfo.Parameters

            For Each rptParameter As ReportExecution2005.ReportParameter In parameterList

40:             SQL = "SELECT * FROM ReportParameter WHERE ReportID = " & m_reportid & " AND parname LIKE '" & SQLPrepare(rptParameter.Name) & "' ORDER BY ParID ASC"

                SaveTextToFile(Date.Now & ": Starting set parameters for reportid " & m_reportid, sAppPath & "sqlrd_parameters.debug", , False, False)

                Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

60:             If oRs Is Nothing Then Return Nothing

70:             If oRs.EOF = True Then Return Nothing

80:             Do While oRs.EOF = False
90:                 ParName = oRs("parname").Value
100:                ParValue = oRs("parvalue").Value

                    SaveTextToFile(Date.Now & ": Parameter Name - " & ParName, sAppPath & "sqlrd_parameters.debug", , True, True)

110:                If ParValue.ToLower = "[sql-rddefault]" Then GoTo Skip


                    SaveTextToFile(Date.Now & ": Key Parameter Name (if applicable) - " & m_KeyParameter, sAppPath & "sqlrd_parameters.debug", , True, True)

160:                If String.Compare(ParName.Trim, m_KeyParameter.Trim, True) = 0 Then
                        SaveTextToFile(Date.Now & ": Parameter matched with Key parameter, values will be replace with " & m_KeyValue, sAppPath & "sqlrd_parameters.debug", , True, True)

170:                    ParValue = m_KeyValue
180:                Else
                        SaveTextToFile(Date.Now & ": Parameter not matched with Key parameter." & m_KeyValue, sAppPath & "sqlrd_parameters.debug", , True, True)
                    End If

190:                ParValue = oParse.ParseString(ParValue, , , , , , Nothing)

200:                Try
210:                    If ParValue.IndexOf("<[x]") > -1 Then
220:                        ParValue = oDynamic._CreateDynamicString(ParValue, m_reportid, m_KeyParameter, m_KeyValue)
                        End If
                    Catch : End Try

                    'check provided command lines
230:                Try
240:                    If Not gParNames Is Nothing Then
250:                        For Z = 0 To gParNames.GetUpperBound(0)
260:                            If gParNames(Z).ToLower = ParName.ToLower Then
270:                                ParValue = gParValues(Z)
                                End If
280:                        Next
                        End If
                    Catch : End Try

290:                If gRunByEvent = True Then
300:                    SQL = "SELECT * FROM EventSubAttr WHERE EventID =" & clsMarsEvent.currentEventID & " AND ReportID =" & m_reportid & " AND  ParName = '" & SQLPrepare(ParName) & "'"

                        Dim oRs1 As ADODB.Recordset = clsMarsData.GetData(SQL)

310:                    If Not oRs1 Is Nothing Then
320:                        If oRs1.EOF = False Then
330:                            Try
340:                                ParValue = clsMarsParser.Parser.ParseString(oRs1("parvalue").Value, , , , , , m_ParametersTable)
350:                            Catch ex As Exception
360:                                ParValue = String.Empty
                                End Try
                            End If

370:                        oRs1.Close()
                        End If
                    End If

                    SaveTextToFile(Date.Now & ": Parameter Value - " & ParValue, sAppPath & "sqlrd_parameters.debug", , True, True)

380:                Try
390:                    parNull = Convert.ToBoolean(oRs("parnull").Value)
400:                Catch
410:                    parNull = False
                    End Try

420:                If ParValue IsNot Nothing Then
430:                    If ParValue.ToLower = "[sql-rdnull]" Then
440:                        ParValue = Nothing
450:                    ElseIf ParValue.ToLower.Contains("[selectall]") Then
                            Dim values As ArrayList = getParameterValidValues(ParName)
460:                        ParValue = ""

470:                        For Each s As String In values
490:                            ParValue &= s & "|"
500:                        Next
                        End If
                    End If

                    If ParValue.Contains("|") Then
                        For Each v As String In ParValue.Split("|")
                            If v <> "" Then
                                ReDim Preserve oPar(I)

                                oPar(I) = New ReportExecution2005.ParameterValue()

                                oPar(I).Name = ParName
                                oPar(I).Value = v

                                I += 1
                            End If
                        Next
                    Else
                        ReDim Preserve oPar(I)

                        oPar(I) = New ReportExecution2005.ParameterValue()

510:                    With oPar(I)
520:                        .Name = ParName
530:                        .Value = ParValue
                        End With

                        I += 1
                    End If

                    If m_ParametersTable.ContainsKey(ParName) = False Then
                        m_ParametersTable.Add(ParName, ParValue)
                    End If

                    SaveTextToFile(Date.Now & ": Adding value to report object", sAppPath & "sqlrd_parameters.debug", , True, True)
Skip:
660:                oRs.MoveNext()
670:            Loop

680:            oRs.Close()
            Next

            Return oPar
740:    Catch ex As Exception
            SaveTextToFile(Date.Now & ": Error setting parameters (" & Erl() & ") : " & ex.Message, sAppPath & "sqlrd_parameters.debug", , True, True)
        End Try

    End Function

    Private Function getParameterValidValues(ByVal name As String) As ArrayList

        Dim rptServer
        Dim ls As ArrayList

        If m_asmxUrl.ToLower.Contains("reportservice2010.asmx") Then
            rptServer = New rsClients.rsClient2010(m_asmxUrl)

            ls = obtainValidValuesForEndPoint2010(rptServer, name)
        ElseIf m_asmxUrl.ToLower.Contains("reportservice2006.asmx") Then
            rptServer = New ReportServer_2006.ReportingService2006
            ls = obtainValidValuesForEndPoint2006(rptServer, name)
        ElseIf m_asmxUrl.ToLower.Contains("reportservice2005.asmx") Then
            rptServer = New rsClients.rsClient2008(m_asmxUrl)
            ls = obtainValidValuesForEndPoint2008(rptServer, name)
        Else
            rptServer = New rsClients.rsClient(m_asmxUrl)
            ls = obtainValidValuesForEndPoint(rptServer, name)
        End If

        Return ls
    End Function

#Region "GetValidValuesFordifferentEndPoints"

    Private Overloads Function obtainValidValuesForEndPoint(rptServer As rsClients.rsClient, name As String) As ArrayList
        Dim parIndex As Integer
        Dim pValues As IList
        Dim availableValues As ArrayList = New ArrayList
        Dim I As Integer = 0

        If m_serverUser <> "" Then
            Dim user, domain As String

            user = m_serverUser

            getDomainAndUserFromString(user, domain)

            If m_formsAuth Then
                rptServer.LogonUser(user, m_serverPassword, domain)
            Else
                rptServer.Credentials = New System.Net.NetworkCredential(user, m_serverPassword, domain)
            End If
        Else
            rptServer.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials
        End If


        Dim rpt As cssreport = New cssreport(m_reportid)
        Dim dataSources() As ReportServer.DataSourceCredentials

        For Each tmp As reportExecutor.reportDatasource In rpt.getDatasourceCredentials
            If tmp.username.ToLower <> "default" Then
                ReDim Preserve dataSources(I)

                dataSources(I) = New ReportServer.DataSourceCredentials

                dataSources(I).DataSourceName = tmp.datasourceName
                dataSources(I).UserName = tmp.username
                dataSources(I).Password = tmp.password

                I += 1
            End If
        Next

        Dim parameters() = rptServer.GetReportParameters(m_reportPath, Nothing, True, Nothing, dataSources)

        '   Dim report As ReportExecution2005.ExecutionInfo = rs.LoadReport(m_reportPath, Nothing)
        Dim setparameters() As ReportServer.ParameterValue

        I = 0
        Dim parameterReset As Boolean

        If m_ParametersTable.Count > 0 Then
            For Each p As ReportServer.ReportParameter In parameters
                Try
                    Dim key As String = p.Name

                    If m_ParametersTable.ContainsKey(key) Then
                        parameterReset = True

                        Dim parValue As String = m_ParametersTable.Item(key)

                        If parValue.Contains("|") = False Then
                            ReDim Preserve setparameters(I)

                            setparameters(I) = New ReportServer.ParameterValue

                            setparameters(I).Name = key
                            setparameters(I).Value = parValue
                            I += 1
                        Else
                            For Each v As String In parValue.Split("|")
                                If v <> "" Then
                                    ReDim Preserve setparameters(I)

                                    setparameters(I) = New ReportServer.ParameterValue

                                    setparameters(I).Name = key
                                    setparameters(I).Value = v
                                    I += 1
                                End If
                            Next
                        End If
                    End If


                Catch : End Try
            Next

            If parameterReset Then parameters = rptServer.GetReportParameters(m_reportPath, Nothing, True, setparameters, dataSources)
        End If

        For Each p As ReportServer.ReportParameter In parameters 'loop through the parameters

            If p.Name = name Then
                pValues = p.ValidValues

                If p.ValidValues IsNot Nothing Then
                    For Each val As ReportServer.ValidValue In pValues
                        availableValues.Add(val.Value)
                    Next
                End If

                Exit For
            End If
        Next

        Return availableValues
    End Function

    Private Overloads Function obtainValidValuesForEndPoint2006(rptServer As ReportServer_2006.ReportingService2006, name As String) As ArrayList
        Dim parIndex As Integer
        Dim pValues As IList
        Dim availableValues As ArrayList = New ArrayList
        Dim I As Integer = 0

        If m_serverUser <> "" Then
            Dim user, domain As String

            user = m_serverUser

            getDomainAndUserFromString(user, domain)

            If m_formsAuth Then
                Throw New Exception("Forms authentication is not supported when using the Sharepoint endpoint (2006.asmx). Please use 2010.asmx instead")
            Else
                rptServer.Credentials = New System.Net.NetworkCredential(user, m_serverPassword, domain)
            End If
        Else
            rptServer.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials
        End If

        Dim rpt As cssreport = New cssreport(m_reportid)
        Dim dataSources() As ReportServer_2006.DataSourceCredentials

        For Each tmp As reportExecutor.reportDatasource In rpt.getDatasourceCredentials
            If tmp.username.ToLower <> "default" Then
                ReDim Preserve dataSources(I)

                dataSources(I) = New ReportServer_2006.DataSourceCredentials

                dataSources(I).DataSourceName = tmp.datasourceName
                dataSources(I).UserName = tmp.username
                dataSources(I).Password = tmp.password

                I += 1
            End If
        Next


        Dim parameters() = rptServer.GetReportParameters(m_reportPath, Nothing, Nothing, dataSources)

        '   Dim report As ReportExecution2005.ExecutionInfo = rs.LoadReport(m_reportPath, Nothing)
        Dim setparameters() As ReportServer_2006.ParameterValue

        I = 0

        Dim parameterReset As Boolean

        If m_ParametersTable.Count > 0 Then
            For Each p As ReportServer_2006.ReportParameter In parameters
                Try
                    Dim key As String = p.Name

                    If m_ParametersTable.ContainsKey(key) Then
                        parameterReset = True

                        Dim parValue As String = m_ParametersTable.Item(key)

                        If parValue.Contains("|") = False Then
                            ReDim Preserve setparameters(I)

                            setparameters(I) = New ReportServer_2006.ParameterValue

                            setparameters(I).Name = key
                            setparameters(I).Value = parValue
                            I += 1
                        Else
                            For Each v As String In parValue.Split("|")
                                If v <> "" Then
                                    ReDim Preserve setparameters(I)

                                    setparameters(I) = New ReportServer_2006.ParameterValue

                                    setparameters(I).Name = key
                                    setparameters(I).Value = v
                                    I += 1
                                End If
                            Next
                        End If
                    End If


                Catch : End Try
            Next

            If parameterReset Then parameters = rptServer.GetReportParameters(m_reportPath, Nothing, setparameters, dataSources)
        End If

        For Each p As ReportServer_2006.ReportParameter In parameters 'loop through the parameters

            If p.Name = name Then
                pValues = p.ValidValues

                If p.ValidValues IsNot Nothing Then
                    For Each val As ReportServer_2006.ValidValue In pValues
                        availableValues.Add(val.Value)
                    Next
                End If

                Exit For
            End If
        Next

        Return availableValues
    End Function

    Private Overloads Function obtainValidValuesForEndPoint2008(rptServer As rsClients.rsClient2008, name As String) As ArrayList
        Dim parIndex As Integer
        Dim pValues As IList
        Dim availableValues As ArrayList = New ArrayList
        Dim I As Integer = 0

        If m_serverUser <> "" Then
            Dim user, domain As String

            user = m_serverUser

            getDomainAndUserFromString(user, domain)

            If m_formsAuth Then
                Throw New Exception("Forms authentication is not supported when using the Sharepoint endpoint (2006.asmx). Please use 2010.asmx instead")
            Else
                rptServer.Credentials = New System.Net.NetworkCredential(user, m_serverPassword, domain)
            End If
        Else
            rptServer.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials
        End If


        Dim rpt As cssreport = New cssreport(m_reportid)
        Dim dataSources() As ReportServer_2005.DataSourceCredentials

        For Each tmp As reportExecutor.reportDatasource In rpt.getDatasourceCredentials
            If tmp.username.ToLower <> "default" Then
                ReDim Preserve dataSources(I)

                dataSources(I) = New ReportServer_2005.DataSourceCredentials

                dataSources(I).DataSourceName = tmp.datasourceName
                dataSources(I).UserName = tmp.username
                dataSources(I).Password = tmp.password

                I += 1
            End If
        Next

        Dim parameters() = rptServer.GetReportParameters(m_reportPath, Nothing, True, Nothing, dataSources)

        Dim setparameters() As ReportServer_2005.ParameterValue

        I = 0
        Dim parameterReset As Boolean

        If m_ParametersTable.Count > 0 Then
            For Each p As ReportServer_2005.ReportParameter In parameters
                Try
                    Dim key As String = p.Name

                    If m_ParametersTable.ContainsKey(key) Then
                        parameterReset = True

                        Dim parValue As String = m_ParametersTable.Item(key)

                        If parValue.Contains("|") = False Then
                            ReDim Preserve setparameters(I)

                            setparameters(I) = New ReportServer_2005.ParameterValue

                            setparameters(I).Name = key
                            setparameters(I).Value = parValue
                            I += 1
                        Else
                            For Each v As String In parValue.Split("|")
                                If v <> "" Then
                                    ReDim Preserve setparameters(I)

                                    setparameters(I) = New ReportServer_2005.ParameterValue

                                    setparameters(I).Name = key
                                    setparameters(I).Value = v
                                    I += 1
                                End If
                            Next
                        End If
                    End If


                Catch : End Try
            Next

            If parameterReset Then parameters = rptServer.GetReportParameters(m_reportPath, Nothing, True, setparameters, dataSources)
        End If

        For Each p As ReportServer_2005.ReportParameter In parameters 'loop through the parameters

            If p.Name = name Then
                pValues = p.ValidValues

                If p.ValidValues IsNot Nothing Then
                    For Each val As ReportServer_2005.ValidValue In pValues
                        availableValues.Add(val.Value)
                    Next
                End If

                Exit For
            End If
        Next

        Return availableValues
    End Function

    Private Overloads Function obtainValidValuesForEndPoint2010(rptServer As rsClients.rsClient2010, name As String) As ArrayList
        Dim parIndex As Integer
        Dim pValues As IList
        Dim availableValues As ArrayList = New ArrayList
        Dim i As Integer = 0

        If m_serverUser <> "" Then
            Dim user, domain As String

            user = m_serverUser

            getDomainAndUserFromString(user, domain)

            If m_formsAuth Then
                Throw New Exception("Forms authentication is not supported when using the Sharepoint endpoint (2006.asmx). Please use 2010.asmx instead")
            Else
                rptServer.Credentials = New System.Net.NetworkCredential(user, m_serverPassword, domain)
            End If
        Else
            rptServer.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials
        End If


        Dim rpt As cssreport = New cssreport(m_reportid)
        Dim dataSources() As ReportServer_2010.DataSourceCredentials

        For Each tmp As reportExecutor.reportDatasource In rpt.getDatasourceCredentials
            If tmp.username.ToLower <> "default" Then

                ReDim Preserve dataSources(i)

                dataSources(i) = New ReportServer_2010.DataSourceCredentials

                dataSources(i).DataSourceName = tmp.datasourceName
                dataSources(i).UserName = tmp.username
                dataSources(i).Password = tmp.password

                i += 1
            End If
        Next

        Dim parameters() = rptServer.GetItemParameters(m_reportPath, Nothing, True, Nothing, dataSources)

        Dim setparameters() As ReportServer_2010.ParameterValue

        i = 0
        Dim parameterReset As Boolean

        If m_ParametersTable.Count > 0 Then
            For Each p As ReportServer_2010.ItemParameter In parameters
                Try
                    Dim key As String = p.Name

                    If m_ParametersTable.ContainsKey(key) Then
                        parameterReset = True

                        Dim parValue As String = m_ParametersTable.Item(key)

                        If parValue.Contains("|") = False Then
                            ReDim Preserve setparameters(i)

                            setparameters(i) = New ReportServer_2010.ParameterValue

                            setparameters(i).Name = key
                            setparameters(i).Value = parValue
                            i += 1
                        Else
                            For Each v As String In parValue.Split("|")
                                If v <> "" Then
                                    ReDim Preserve setparameters(i)

                                    setparameters(i) = New ReportServer_2010.ParameterValue

                                    setparameters(i).Name = key
                                    setparameters(i).Value = v
                                    i += 1
                                End If
                            Next
                        End If
                    End If

                Catch : End Try
            Next

            If parameterReset Then parameters = rptServer.GetItemParameters(m_reportPath, Nothing, True, setparameters, dataSources)
        End If

        For Each p As ReportServer_2010.ItemParameter In parameters 'loop through the parameters

            If p.Name = name Then
                pValues = p.ValidValues

                If p.ValidValues IsNot Nothing Then
                    For Each val As ReportServer_2010.ValidValue In pValues
                        availableValues.Add(val.Value)
                    Next
                End If

                Exit For
            End If
        Next

        Return availableValues
    End Function

#End Region

    Public Function setDatasourceCredentials() As ReportExecution2005.DataSourceCredentials()

        Dim SQL As String = "SELECT * FROM reportdatasource WHERE reportid = " & m_reportid
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim reportDS() As ReportExecution2005.DataSourceCredentials
        Dim I As Integer = 0

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim dsName As String = oRs("datasourcename").Value
                Dim userid As String = oRs("rptuserid").Value
                Dim password As String = oRs("rptpassword").Value
                Dim dsAlreadySet As Boolean = False

                password = _DecryptDBValue(password)

                If reportDS IsNot Nothing Then
                    For Each ds As ReportExecution2005.DataSourceCredentials In reportDS
                        If ds.DataSourceName.ToLower = dsName.ToLower Then
                            dsAlreadySet = True
                            Exit For
                        End If
                    Next
                End If

                If userid <> "default" And password <> "default" And dsAlreadySet = False Then
                    ReDim Preserve reportDS(I)

                    reportDS(I) = New ReportExecution2005.DataSourceCredentials

                    With reportDS(I)
                        .DataSourceName = dsName
                        .UserName = userid
                        .Password = password
                    End With

                    I += 1
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
            oRs = Nothing

            Return reportDS
        End If
    End Function

    Public Function ReportServerRenderStream(streamID As String, format As String, deviceInfo As String, encoding As String, mimeType As String) As Byte()
        If rs IsNot Nothing Then
            Return rs.RenderStream(format, streamID, deviceInfo, encoding, mimeType)
        End If
    End Function

    Public Function ReportServerRenderByExecutionEndPoint(ByVal sFormat As String, _
            ByVal sReportPath As String, ByVal sOutput As String, extension As String, _
            Optional ByRef oStream() As String = Nothing, _
            Optional ByVal timeout As Int64 = 3600000, Optional ByVal deviceInfo As String = "", Optional ByVal reportName As String = "", Optional keyParameters As String = "", Optional keyValue As String = "") As Byte()
        'Server did not recognize the value of HTTP Header SOAPAction: 
        m_url = m_url.ToLower.Replace("reportservice.asmx", "").Replace("reportservice2005.asmx", "").Replace("reportservice2006.asmx", "").Replace("reportservice2008.asmx", "").Replace("reportservice2010.asmx", "")

        If m_url.EndsWith("/") Then
            m_url = m_url & "reportexecution2005.asmx"
        Else
            m_url = String.Format("{0}/{1}", m_url, "reportexecution2005.asmx")
        End If

        m_KeyParameter = keyParameters
        m_KeyValue = keyValue

        rs = New ReportExecution2005.ReportExecutionService

        rs.Url = m_url

        If m_serverUser = "" Then
            rs.UseDefaultCredentials = True
        Else
            Dim domain As String

            getDomainAndUserFromString(m_serverUser, domain)
            rs.Credentials = New System.Net.NetworkCredential(m_serverUser, m_serverPassword, domain)
        End If

        Dim result As Byte() = Nothing
        Dim historyID As String = Nothing
        Dim devInfo As String = Nothing
        Dim showHideToggle As String = Nothing
        Dim encoding As String
        Dim mimeType As String
        Dim warnings As ReportExecution2005.Warning() = Nothing
        ' Dim reportHistoryParameters As ReportExecution2005.ParameterValue() = Me.SetReportParameters()
        Dim reportDatasourceCredentials As ReportExecution2005.DataSourceCredentials() = Me.setDatasourceCredentials

        Dim execInfo As ReportExecution2005.ExecutionInfo = New ReportExecution2005.ExecutionInfo()

        Dim execHeader As ReportExecution2005.ExecutionHeader = New ReportExecution2005.ExecutionHeader

        rs.ExecutionHeaderValue = execHeader

        execInfo = rs.LoadReport(sReportPath, historyID)

        Dim execParameters As ReportExecution2005.ParameterValue() = Me.SetReportParameters()

        rs.SetExecutionParameters(execParameters, "en-us")

        If reportDatasourceCredentials IsNot Nothing AndAlso reportDatasourceCredentials.Length > 0 Then rs.SetExecutionCredentials(reportDatasourceCredentials)

        rs.Timeout = -1

        Dim SessionId As String = rs.ExecutionHeaderValue.ExecutionID

        result = rs.Render(sFormat, devInfo, extension, encoding, mimeType, warnings, oStream)

        execInfo = rs.GetExecutionInfo()

        Return result

    End Function



    Public Class reportDatasource
        Public Property datasourceName As String
        Public Property username As String
        Public Property password As String
    End Class
End Class
