﻿Public Class EventBased
    Dim m_eventID As Integer
    Dim m_conceredRow As ADODB.Recordset
    Dim m_schedule As Schedule
    Dim dateFormat As String = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToString

    Sub New(ByVal eventID As Integer)
        m_eventID = eventID

        m_schedule = New Schedule(eventID, Schedule.parentscheduleType.EVENTBASED)
    End Sub

    Private ReadOnly Property concernedRow() As ADODB.Recordset
        Get
            If m_conceredRow IsNot Nothing Then
                Return m_conceredRow
            Else
                m_conceredRow = clsMarsData.GetData("SELECT * FROM eventattr6 WHERE eventid =" & m_eventID)


                If m_conceredRow IsNot Nothing AndAlso m_conceredRow.EOF = False Then
                    Return m_conceredRow
                Else
                    Throw New Exception("Could not find Event-Based schedule referenced by ID: " & m_eventID)
                    Return Nothing
                End If

            End If
        End Get
    End Property

    Public ReadOnly Property schedulePriorityClass As System.Diagnostics.ProcessPriorityClass
        Get
            Select Case IsNull(m_conceredRow("schedulepriority"), "3 - Normal")
                Case "1 - High"
                    Return ProcessPriorityClass.High
                Case "2 - Above Nornal", "2 - Above Normal"
                    Return ProcessPriorityClass.AboveNormal
                Case "3 - Normal"
                    Return ProcessPriorityClass.Normal
                Case "4 - Below Normal"
                    Return ProcessPriorityClass.BelowNormal
                Case "5 - Low"
                    Return ProcessPriorityClass.Idle
                Case Else
                    Return ProcessPriorityClass.Normal
            End Select
        End Get
    End Property

    Public ReadOnly Property eventParent As Integer
        Get
            Return concernedRow.Fields("parent").Value
        End Get
    End Property

    Public ReadOnly Property eventName() As String
        Get
            Return concernedRow.Fields("eventname").Value
        End Get
    End Property

    Public ReadOnly Property schedulePath As String
        Get
            Dim parent As Integer = Me.eventParent
            Dim folder As folder = New folder(parent)

            Return folder.getFolderPath()
        End Get
    End Property
    Public ReadOnly Property lastRunDuration As Double
        Get
            Try
                Dim fin As Date = LastRun
                Dim start As Date = LastRunStart

                Return fin.Subtract(start).TotalSeconds
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Public ReadOnly Property executeExistingAsync As Boolean
        Get
            Try
                Return IsNull(concernedRow.Fields("existingasync").Value, 0)
            Catch ex As Exception
                Return False
            End Try
        End Get
    End Property

    Public ReadOnly Property maxAsyncthreads As Integer
        Get
            Try
                Return IsNull(concernedRow.Fields("maxthreads").Value, 2)
            Catch ex As Exception
                Return 2
            End Try
        End Get
    End Property
    Public ReadOnly Property LastRunStart(Optional format As String = "") As String
        Get
            Try
                If format = "" Then
                    format = dateFormat & " HH:mm:ss"
                End If

                Dim dtHistory As ADODB.Recordset = clsMarsData.GetData("SELECT TOP 1 * FROM eventhistory WHERE eventid =" & m_eventID & " ORDER BY lastfired DESC")

                If dtHistory IsNot Nothing AndAlso dtHistory.EOF = False Then
                    Dim lastRunStartTime As Date = dtHistory("startdate").Value

                    Return lastRunStartTime.ToString("yyyy-MM-dd HH:mm:ss")

                Else
                    Return "Never run"
                End If
            Catch ex As Exception
                Return "#Error#"
            End Try
        End Get
    End Property


    Public ReadOnly Property LastRun(Optional format As String = "") As String
        Get
            Try
                If format = "" Then
                    format = dateFormat & " HH:mm:ss"
                End If

                Dim dtHistory As ADODB.Recordset = clsMarsData.GetData("SELECT TOP 1 * FROM eventhistory WHERE eventid =" & m_eventID & " ORDER BY lastfired DESC")

                If dtHistory IsNot Nothing AndAlso dtHistory.EOF = False Then
                    Dim lastRunDate As Date = dtHistory("lastfired").Value

                    Return lastRunDate.ToString(format)


                Else
                    Return "Never run"
                End If
            Catch ex As Exception
                Return "#Error#"
            End Try
        End Get
    End Property

    Public ReadOnly Property NextRun() As String
        Get
            Return "Event-Based"
        End Get
    End Property

    Public ReadOnly Property LastResult() As String
        Get
            Dim dtHistory As ADODB.Recordset = clsMarsData.GetData("SELECT TOP 1 * FROM eventhistory WHERE eventid =" & m_eventID & " ORDER BY lastfired DESC")

            
            If dtHistory IsNot Nothing AndAlso dtHistory.EOF = False Then
                Dim success As Boolean = CType(dtHistory("status").Value, Boolean)
                Dim msg As String = IsNull(dtHistory("errmsg").Value, "")

                If success = True Then
                    Return "Success"
                Else
                    Return "Failed: " & msg
                End If


            Else
                Return "Never run"
            End If
        End Get
    End Property

    Public ReadOnly Property Owner() As String
        Get
            Return concernedRow.Fields("owner").Value
        End Get
    End Property

    Public ReadOnly Property Status() As Boolean
        Get
            Return concernedRow.Fields("status").Value
        End Get
    End Property

    Public ReadOnly Property ID As Integer
        Get
            Return m_eventID
        End Get
    End Property
    Public ReadOnly Property eventImage() As Image
        Get
            Return My.Resources.document_atom1

            'If Status = True Then
            '    Return My.Resources.document_atom1
            'Else
            '    Return MakeGrayscale3(My.Resources.document_atom1)
            'End If
        End Get
    End Property

    Public ReadOnly Property largeimageFileURL() As String
        Get
            If Status = True Then
                Return "largeimages/document_atoms.png"
            Else
                Return "largeimages/document_atoms_bw.png"
            End If
        End Get
    End Property

    'Public ReadOnly Property conditions() As eventcondition()
    '    Get
    '        Dim dt As DataTable = mainDS.Tables("eventconditions")
    '        Dim rows As DataRow() = dt.Select("eventid =" & m_eventID)
    '        Dim i As Integer = 0
    '        Dim conds() As eventcondition = Nothing

    '        If rows IsNot Nothing Then
    '            For Each row As DataRow In rows
    '                ReDim Preserve conds(i)

    '                conds(i) = New eventcondition(row("conditionid"))

    '                i += 1
    '            Next

    '            Return conds
    '        Else
    '            Return Nothing
    '        End If
    '    End Get
    'End Property
    'Public Sub deleteEvent()
    '    If Me.conditions IsNot Nothing Then
    '        For Each cond As eventcondition In Me.conditions
    '            cond.deleteCondition()
    '        Next
    '    End If

    '    dataAccess.commitData("DELETE FROM eventattr6 WHERE eventid =" & m_eventID, Nothing)
    '    dataAccess.commitData("DELETE FROM eventHistory WHERE eventid =" & m_eventID, Nothing)
    'End Sub
End Class
