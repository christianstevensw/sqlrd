﻿Public Class smartfolder
    Dim m_smartID As Integer
    Dim oRs As ADODB.Recordset
    Public Sub New(ByVal smartID As Integer)
        m_smartID = smartID
        oRs = clsMarsData.GetData("select * from smartfolders where smartid= " & smartID)
    End Sub
    Public ReadOnly Property folderName As String
        Get
            Return IsNull(oRs("smartname").Value, "")
        End Get
    End Property

    Public Shared Function canRename(smartid As Integer, newName As String) As Boolean
        Dim rs As ADODB.Recordset
        rs = clsMarsData.GetData("SELECT COUNT(*) FROM smartfolders WHERE smartname LIKE '" & SQLPrepare(newName) & "' AND smartid <> " & smartid)

        If rs IsNot Nothing AndAlso rs.EOF = False Then
            Dim count As Integer = rs(0).Value
            rs.Close()
            If count > 0 Then
                Return False
            Else
                Return True
            End If
        Else
            Return True
        End If
    End Function
    Public ReadOnly Property folderID As Integer
        Get
            Return m_smartID
        End Get
    End Property

    Private Sub getColumnsAndVals(ByRef sColumn() As String, ByRef sValue() As String)

        Dim SQL As String = "SELECT * FROM SmartFolders WHERE SmartID =" & m_smartID
        Dim I As Integer = 0

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            I = 0

            Dim sType As String = oRs("smarttype").Value

            SQL = "SELECT * FROM SmartFolderAttr WHERE SmartID = " & m_smartID

            Dim oRs1 As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs1 IsNot Nothing Then

                Do While oRs1.EOF = False
                    ReDim Preserve sColumn(I)
                    ReDim Preserve sValue(I)

                    sColumn(I) = oRs1("smartcolumn").Value
                    sValue(I) = oRs1("smartcriteria").Value

                    oRs1.MoveNext()
                    I += 1
                Loop
            End If

            oRs1.Close()
        End If

        oRs.Close()
    End Sub

    Public ReadOnly Property m_smartType() As String
        Get
            Dim SQL As String = "SELECT smartType FROM SmartFolders WHERE SmartID =" & m_smartID
            Dim I As Integer = 0

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                I = 0

                Dim sType As String = oRs("smarttype").Value

                If sType = "All" Then
                    sType = " AND "
                Else
                    sType = " OR "
                End If

                oRs.Close()

                oRs = Nothing

                Return sType
            Else
                Return " AND "
            End If

        End Get
    End Property

    Private Function getPackageQuery() As String
        'for packages
        Dim I As Integer
        Dim sColumn(), sValue(), sPackCriteria() As String
        Dim x As Integer = 0

        getColumnsAndVals(sColumn, sValue)

        For I = 0 To sColumn.GetUpperBound(0)
            Select Case sColumn(I).ToLower
                Case "frequency", "starttime", _
                "destinationtype", _
                "printername", "ftpserver", "ftpusername", "description", "keyword"

                    ReDim Preserve sPackCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sPackCriteria(x) = sColumn(I) & " " & sValue(I)
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sPackCriteria(x) = "(" & sColumn(I) & " IS NULL OR " & sColumn(I) & " LIKE '')"
                        Else
                            sPackCriteria(x) = sColumn(I) & " IS NOT NULL AND " & sColumn(I) & " NOT LIKE ''"
                        End If
                    End If
                    x += 1
                Case "enddate"
                    ReDim Preserve sPackCriteria(x)

                   
                    If sValue(I).ToLower.Contains("'never'") Then

                        If gConType = "DAT" Then
                            sPackCriteria(x) = sColumn(I) & ">= #3004-01-01#"
                        Else
                            sPackCriteria(x) = sColumn(I) & ">= '3004-01-01'"
                        End If
                    Else
                        If gConType = "DAT" Then
                            sPackCriteria(x) = sColumn(I) & " " & sValue(I).Replace("'", "#")
                        Else
                            sPackCriteria(x) = sColumn(I) & " " & sValue(I)
                        End If
                    End If


                    x += 1
                Case "startdate", "enddate", "nextrun"
                    ReDim Preserve sPackCriteria(x)

                    If gConType = "DAT" Then
                        sPackCriteria(x) = sColumn(I) & " " & sValue(I).Replace("'", "#")
                    Else
                        sPackCriteria(x) = sColumn(I) & " " & sValue(I)
                    End If

                    x += 1
                Case "schedulename"

                    ReDim Preserve sPackCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sPackCriteria(x) = "PackageName" & " " & sValue(I)
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sPackCriteria(x) = "(PackageName IS NULL OR PackageName LIKE '')"
                        Else
                            sPackCriteria(x) = "PackageName IS NOT NULL AND PackageName NOT LIKE ''"
                        End If
                    End If
                    x += 1
                Case "status"
                    ReDim Preserve sPackCriteria(x)

                    sPackCriteria(x) = sColumn(I) & " " & sValue(I).Replace("'", String.Empty)
                    x += 1
                Case "foldername"
                    ReDim Preserve sPackCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sPackCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName " & sValue(I) & "))"
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sPackCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName IS NULL OR FolderName LIKE ''))"
                        Else
                            sPackCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName IS NOT NULL OR FolderName NOT LIKE ''))"
                        End If
                    End If
                    x += 1
                Case "sendto", "subject", "message", "cc", "bcc"
                    ReDim Preserve sPackCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sPackCriteria(x) = "(PackageAttr.PackID IN (SELECT PackID FROM DestinationAttr WHERE " & sColumn(I) & " " & sValue(I) & "))"
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sPackCriteria(x) = "(PackageAttr.PackID IN (SELECT PackID FROM DestinationAttr WHERE " & sColumn(I) & " IS NULL OR " & sColumn(I) & " LIKE ''))"
                        Else
                            sPackCriteria(x) = "(PackageAttr.PackID IN (SELECT PackID FROM DestinationAttr WHERE " & sColumn(I) & " IS NOT NULL OR " & sColumn(I) & " NOT LIKE ''))"
                        End If
                    End If
                    x += 1
                Case "errmsg"
                    ReDim Preserve sPackCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sPackCriteria(x) = "(PackageAttr.PackID IN (SELECT PackID FROM ScheduleHistory WHERE ErrMsg " & sValue(I) & "))"
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sPackCriteria(x) = "(PackageAttr.PackID IN (SELECT PackID FROM ScheduleHistory WHERE ErrMsg IS NULL OR ErrMsg LIKE ''))"
                        Else
                            sPackCriteria(x) = "(PackageAttr.PackID IN (SELECT PackID FROM ScheduleHistory WHERE ErrMsg IS NOT NULL OR ErrMsg NOT LIKE ''))"
                        End If
                    End If

                    x += 1
                Case "scheduletype"
                    ReDim Preserve sPackCriteria(x)

                    If sValue(I).ToLower.IndexOf("single schedule") > -1 Then
                        sPackCriteria(x) = "ScheduleAttr.ReportID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("package schedule") > -1 Then
                        sPackCriteria(x) = "ScheduleAttr.PackID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("automation schedule") > -1 Then
                        sPackCriteria(x) = "ScheduleAttr.AutoID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("event-based schedule") > -1 Then
                        sPackCriteria(x) = "ScheduleAttr.ReportID = 1"
                    End If
                    x += 1
            End Select
        Next

        'for packages
        Dim sFinal As String = String.Empty
        Dim stype = m_smartType
        Dim sQuery As String
        Dim sNotAdmin As String = ""

        Try
            For I = 0 To sPackCriteria.GetUpperBound(0)
                If I < sPackCriteria.GetUpperBound(0) Then
                    sFinal &= sPackCriteria(I) & sType
                Else
                    sFinal &= sPackCriteria(I)
                End If
            Next

            sQuery = "SELECT PackageAttr.PackID FROM PackageAttr INNER JOIN ScheduleAttr ON " & _
            "PackageAttr.PackID = ScheduleAttr.PackID WHERE " & sFinal

            If gRole.ToLower <> "administrator" Then
                sNotAdmin = " AND PackageAttr.PackID IN (SELECT PackID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                sQuery &= sNotAdmin
            End If
        Catch
            sQuery = String.Empty
        End Try

        Return sQuery
    End Function
    Private Function getReportQuery() As String
        'create criteria for reports
        Dim I As Integer = 0
        Dim sColumn(), sValue(), sRptCriteria() As String

        getColumnsAndVals(sColumn, sValue)

        For I = 0 To sColumn.GetUpperBound(0)

            Select Case sColumn(I).ToLower
                Case "status"
                    ReDim Preserve sRptCriteria(I)
                    sRptCriteria(I) = sColumn(I) & " " & sValue(I).Replace("'", String.Empty)

                Case "schedulename"
                    ReDim Preserve sRptCriteria(I)
                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sRptCriteria(I) = "ReportTitle" & " " & sValue(I)
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sRptCriteria(I) = "(ReportTitle IS NULL OR ReportTitle LIKE '')"
                        Else
                            sRptCriteria(I) = "ReportTitle IS NOT NULL AND ReportTitle NOT LIKE ''"
                        End If
                    End If
                Case "foldername"
                    ReDim Preserve sRptCriteria(I)
                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sRptCriteria(I) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName " & sValue(I) & "))"
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sRptCriteria(I) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName IS NULL OR FolderName LIKE ''))"
                        Else
                            sRptCriteria(I) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName IS NOT NULL OR FolderName NOT LIKE ''))"
                        End If
                    End If
                Case "sendto", "subject", "message", "cc", "bcc"
                    ReDim Preserve sRptCriteria(I)
                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sRptCriteria(I) = "(ReportAttr.ReportID IN (SELECT ReportID FROM DestinationAttr WHERE " & sColumn(I) & " " & sValue(I) & "))"
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sRptCriteria(I) = "(ReportAttr.ReportID IN (SELECT ReportID FROM DestinationAttr WHERE " & sColumn(I) & " IS NULL OR " & sColumn(I) & " LIKE ''))"
                        Else
                            sRptCriteria(I) = "(ReportAttr.ReportID IN (SELECT ReportID FROM DestinationAttr WHERE " & sColumn(I) & " IS NOT NULL OR " & sColumn(I) & " NOT LIKE ''))"
                        End If
                    End If
                Case "errmsg"
                    ReDim Preserve sRptCriteria(I)
                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sRptCriteria(I) = "(ReportAttr.ReportID IN (SELECT ReportID FROM ScheduleHistory WHERE ErrMsg " & sValue(I) & "))"
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sRptCriteria(I) = "(ReportAttr.ReportID IN (SELECT ReportID FROM ScheduleHistory WHERE ErrMsg IS NULL OR ErrMsg LIKE ''))"
                        Else
                            sRptCriteria(I) = "(ReportAttr.ReportID IN (SELECT ReportID FROM ScheduleHistory WHERE ErrMsg IS NOT NULL OR ErrMsg NOT LIKE ''))"
                        End If
                    End If
                Case "startdate", "enddate", "nextrun"
                    ReDim Preserve sRptCriteria(I)
                    If gConType = "DAT" Then
                        sRptCriteria(I) = sColumn(I) & " " & sValue(I).Replace("'", "#")
                    Else
                        sRptCriteria(I) = sColumn(I) & " " & sValue(I)
                    End If
                Case "scheduletype"
                    ReDim Preserve sRptCriteria(I)

                    If sValue(I).ToLower.IndexOf("single schedule") > -1 Then
                        sRptCriteria(I) = "ScheduleAttr.ReportID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("dynamic schedule") > -1 Then
                        sRptCriteria(I) = "ScheduleAttr.ReportID <> 0 AND reportattr.dynamic =1 "
                    ElseIf sValue(I).ToLower.IndexOf("bursting schedule") > -1 Then
                        sRptCriteria(I) = "ScheduleAttr.ReportID <> 0 AND reportattr.bursting = 1 "
                    ElseIf sValue(I).ToLower.IndexOf("data-driven schedule") > -1 Then
                        sRptCriteria(I) = "ScheduleAttr.ReportID <> 0 AND reportattr.isdatadriven = 1"
                    ElseIf sValue(I).ToLower.IndexOf("dynamic package") > -1 Then
                        sRptCriteria(I) = "ScheduleAttr.PackID <> 0 AND packageattr.Dynamic = 1"
                    ElseIf sValue(I).ToLower.IndexOf("data-driven package") > -1 Then
                        sRptCriteria(I) = "ScheduleAttr.PackID <> 0 AND packageattr.isdatadriven = 1"
                    ElseIf sValue(I).ToLower.IndexOf("package schedule") > -1 Then
                        sRptCriteria(I) = "ScheduleAttr.PackID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("automation schedule") > -1 Then
                        sRptCriteria(I) = "ScheduleAttr.AutoID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("event-based schedule") > -1 Then
                        sRptCriteria(I) = "ScheduleAttr.ReportID = 1"
                    ElseIf sValue(I).ToLower.IndexOf("event-based package") > -1 Then
                        sRptCriteria(I) = "ScheduleAttr.eventpackid <> 0"
                    End If
                Case Else
                    ReDim Preserve sRptCriteria(I)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sRptCriteria(I) = sColumn(I) & " " & sValue(I)
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sRptCriteria(I) = "(" & sColumn(I) & " IS NULL OR " & sColumn(I) & " LIKE '')"
                        Else
                            sRptCriteria(I) = sColumn(I) & " IS NOT NULL AND " & sColumn(I) & " NOT LIKE ''"
                        End If
                    End If
            End Select
        Next

        'create the full criteria for single schedules
        Dim sFinal As String = ""
        Dim sType As String = Me.m_smartType
        Dim sQuery As String
        Dim sNotAdmin As String = ""

        Try
            For I = 0 To sRptCriteria.GetUpperBound(0)
                If I < sRptCriteria.GetUpperBound(0) Then
                    sFinal &= sRptCriteria(I) & sType
                Else
                    sFinal &= sRptCriteria(I)
                End If
            Next

            sQuery = "SELECT reportAttr.ReportID FROM ReportAttr INNER JOIN ScheduleAttr ON " & _
            "ReportAttr.ReportID = ScheduleAttr.ReportID WHERE " & sFinal

            If gRole.ToLower <> "administrator" Then
                sNotAdmin = " AND ReportAttr.ReportID IN (SELECT ReportID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                sQuery &= sNotAdmin
            End If
        Catch
            sQuery = String.Empty
        End Try

        Return sQuery
    End Function

    Private Function getAutomationQuery() As String
        Dim I As Integer
        Dim sColumn(), sValue(), sAutoCriteria() As String
        Dim x As Integer = 0

        getColumnsAndVals(sColumn, sValue)

        x = 0
        For I = 0 To sColumn.GetUpperBound(0)
            Select Case sColumn(I).ToLower
                Case "frequency", "starttime", _
                "description", "keyword"
                    ReDim Preserve sAutoCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sAutoCriteria(x) = sColumn(I) & " " & sValue(I)
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sAutoCriteria(x) = "(" & sColumn(I) & " IS NULL OR " & sColumn(I) & " LIKE '')"
                        Else
                            sAutoCriteria(x) = sColumn(I) & " IS NOT NULL AND " & sColumn(I) & " NOT LIKE ''"
                        End If
                    End If
                    x += 1
                Case "startdate", "enddate", "nextrun"
                    ReDim Preserve sAutoCriteria(x)

                    If gConType = "DAT" Then
                        sAutoCriteria(x) = sColumn(I) & " " & sValue(I).Replace("'", "#")
                    Else
                        sAutoCriteria(x) = sColumn(I) & " " & sValue(I)
                    End If

                    x += 1
                Case "schedulename"
                    ReDim Preserve sAutoCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sAutoCriteria(x) = "AutoName" & " " & sValue(I)
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sAutoCriteria(x) = "(AutoName IS NULL OR AutoName LIKE '')"
                        Else
                            sAutoCriteria(x) = "AutoName IS NOT NULL AND AutoName NOT LIKE ''"
                        End If
                    End If
                    x += 1
                Case "status"

                    ReDim Preserve sAutoCriteria(x)
                    sAutoCriteria(x) = sColumn(x) & " " & sValue(I).Replace("'", String.Empty)
                    x += 1
                Case "foldername"
                    ReDim Preserve sAutoCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sAutoCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName " & sValue(I) & "))"
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sAutoCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName IS NULL OR FolderName LIKE ''))"
                        Else
                            sAutoCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName IS NOT NULL OR FolderName NOT LIKE ''))"
                        End If
                    End If
                    x += 1
                Case "errmsg"
                    ReDim Preserve sAutoCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sAutoCriteria(x) = "(AutomationAttr.AutoID IN (SELECT AutoID FROM ScheduleHistory WHERE ErrMsg " & sValue(I) & "))"
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sAutoCriteria(x) = "(AutomationAttr.AutoID IN (SELECT AutoID FROM ScheduleHistory WHERE ErrMsg IS NULL OR ErrMsg LIKE ''))"
                        Else
                            sAutoCriteria(x) = "(AutomationAttr.AutoID IN (SELECT AutoID FROM ScheduleHistory WHERE ErrMsg IS NOT NULL OR ErrMsg NOT LIKE ''))"
                        End If
                    End If

                    x += 1
                Case "scheduletype"
                    ReDim Preserve sAutoCriteria(x)

                    If sValue(I).ToLower.IndexOf("single schedule") > -1 Then
                        sAutoCriteria(x) = "ScheduleAttr.ReportID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("package schedule") > -1 Then
                        sAutoCriteria(x) = "ScheduleAttr.PackID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("automation schedule") > -1 Then
                        sAutoCriteria(x) = "ScheduleAttr.AutoID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("event-based schedule") > -1 Then
                        sAutoCriteria(x) = "ScheduleAttr.ReportID = 1"
                    End If
                    x += 1
            End Select
        Next

        'for automation
        Dim sFinal As String = String.Empty
        Dim sType As String = m_smartType
        Dim sQuery As String = ""
        Dim sNotAdmin As String = ""

        Try
            For I = 0 To sAutoCriteria.GetUpperBound(0)
                If I < sAutoCriteria.GetUpperBound(0) Then
                    sFinal &= sAutoCriteria(I) & sType
                Else
                    sFinal &= sAutoCriteria(I)
                End If
            Next

            sQuery = "SELECT AutomationAttr.AutoID FROM AutomationAttr INNER JOIN ScheduleAttr ON " & _
            "AutomationAttr.AutoID = ScheduleAttr.AutoID WHERE " & sFinal

            If gRole.ToLower <> "administrator" Then
                sNotAdmin = " AND AutomationAttr.AutoID IN (SELECT AutoID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                sQuery &= sNotAdmin
            End If
        Catch
            sQuery = String.Empty
        End Try

        Return sQuery
    End Function

    Private Function getEventBasedQuery()
        Dim I As Integer
        Dim sColumn(), sValue(), sEventCriteria() As String
        Dim x As Integer = 0

        getColumnsAndVals(sColumn, sValue)

        x = 0

        For I = 0 To sColumn.GetUpperBound(0)
            Select Case sColumn(I).ToLower
                Case "description", "keyword"
                    ReDim Preserve sEventCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sEventCriteria(x) = sColumn(I) & " " & sValue(I)
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sEventCriteria(x) = "(" & sColumn(I) & " IS NULL OR " & sColumn(I) & " LIKE '')"
                        Else
                            sEventCriteria(x) = sColumn(I) & " IS NOT NULL AND " & sColumn(I) & " NOT LIKE ''"
                        End If
                    End If

                    x += 1
                Case "schedulename"

                    ReDim Preserve sEventCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sEventCriteria(x) = "EventName" & " " & sValue(I)
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sEventCriteria(x) = "(EventName IS NULL OR EventName LIKE '')"
                        Else
                            sEventCriteria(x) = "EventName IS NOT NULL AND EventName NOT LIKE ''"
                        End If
                    End If
                    x += 1
                Case "status"
                    ReDim Preserve sEventCriteria(x)
                    sEventCriteria(x) = sColumn(x) & " " & sValue(I).Replace("'", String.Empty)
                    x += 1
                Case "foldername"
                    ReDim Preserve sEventCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sEventCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName " & sValue(I) & "))"
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sEventCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName IS NULL OR FolderName LIKE ''))"
                        Else
                            sEventCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName IS NOT NULL OR FolderName NOT LIKE ''))"
                        End If
                    End If
                    x += 1
                Case "scheduletype"
                    ReDim Preserve sEventCriteria(x)

                    If sValue(I).ToLower.IndexOf("single schedule") > -1 Then
                        sEventCriteria(x) = "EventID = 1"
                    ElseIf sValue(I).ToLower.IndexOf("package schedule") > -1 Then
                        sEventCriteria(x) = "EventID = 1"
                    ElseIf sValue(I).ToLower.IndexOf("automation schedule") > -1 Then
                        sEventCriteria(x) = "EventID = 1"
                    ElseIf sValue(I).ToLower.IndexOf("event-based schedule") > -1 Then
                        sEventCriteria(x) = "EventID <> 0"
                    End If
                    x += 1

            End Select
        Next

        'for events
        Dim sFinal As String = String.Empty
        Dim sType As String = m_smartType
        Dim sNotAdmin As String = ""
        Dim sQuery As String = ""

        Try
            For I = 0 To sEventCriteria.GetUpperBound(0)
                If I < sEventCriteria.GetUpperBound(0) Then
                    sFinal &= sEventCriteria(I) & sType
                Else
                    sFinal &= sEventCriteria(I)
                End If
            Next

            sQuery = "SELECT EventAttr6.EventID FROM EventAttr6 WHERE " & sFinal & " AND (PackID IS NULL OR PackID =0) "

            If gRole.ToLower <> "administrator" Then
                sNotAdmin = " AND EventAttr6.EventID IN (SELECT EventID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                sQuery &= sNotAdmin
            End If
        Catch
            sQuery = String.Empty
        End Try

        Return sQuery
    End Function

    Private Function getEventPackageQuery()
        Dim I As Integer
        Dim sColumn(), sValue(), sEventPackCriteria() As String
        Dim x As Integer = 0

        getColumnsAndVals(sColumn, sValue)

        x = 0

        For I = 0 To sColumn.GetUpperBound(0)
            Select Case sColumn(I).ToLower
                Case "frequency", "starttime", _
                "destinationtype", _
                "printername", "ftpserver", "ftpusername", "description", "keyword"

                    ReDim Preserve sEventPackCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sEventPackCriteria(x) = sColumn(I) & " " & sValue(I)
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sEventPackCriteria(x) = "(" & sColumn(I) & " IS NULL OR " & sColumn(I) & " LIKE '')"
                        Else
                            sEventPackCriteria(x) = sColumn(I) & " IS NOT NULL AND " & sColumn(I) & " NOT LIKE ''"
                        End If
                    End If
                    x += 1
                Case "startdate", "enddate", "nextrun"
                    ReDim Preserve sEventPackCriteria(x)

                    If gConType = "DAT" Then
                        sEventPackCriteria(x) = sColumn(I) & " " & sValue(I).Replace("'", "#")
                    Else
                        sEventPackCriteria(x) = sColumn(I) & " " & sValue(I)
                    End If

                    x += 1
                Case "schedulename"

                    ReDim Preserve sEventPackCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sEventPackCriteria(x) = "PackageName" & " " & sValue(I)
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sEventPackCriteria(x) = "(PackageName IS NULL OR PackageName LIKE '')"
                        Else
                            sEventPackCriteria(x) = "PackageName IS NOT NULL AND PackageName NOT LIKE ''"
                        End If
                    End If
                    x += 1
                Case "status"
                    ReDim Preserve sEventPackCriteria(x)

                    sEventPackCriteria(x) = sColumn(I) & " " & sValue(I).Replace("'", String.Empty)
                    x += 1
                Case "foldername"
                    ReDim Preserve sEventPackCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sEventPackCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName " & sValue(I) & "))"
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sEventPackCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName IS NULL OR FolderName LIKE ''))"
                        Else
                            sEventPackCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName IS NOT NULL OR FolderName NOT LIKE ''))"
                        End If
                    End If
                    x += 1
                Case "errmsg"
                    ReDim Preserve sEventPackCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sEventPackCriteria(x) = "(EventPackageAttr.EventPackID IN (SELECT EventPackID FROM ScheduleHistory WHERE ErrMsg " & sValue(I) & "))"
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sEventPackCriteria(x) = "(EventPackageAttr.EventPackID IN (SELECT EventPackID FROM ScheduleHistory WHERE ErrMsg IS NULL OR ErrMsg LIKE ''))"
                        Else
                            sEventPackCriteria(x) = "(EventPackageAttr.EventPackID IN (SELECT EventPackID FROM ScheduleHistory WHERE ErrMsg IS NOT NULL OR ErrMsg NOT LIKE ''))"
                        End If
                    End If

                    x += 1
                Case "scheduletype"
                    ReDim Preserve sEventPackCriteria(x)

                    If sValue(I).ToLower.IndexOf("single schedule") > -1 Then
                        sEventPackCriteria(x) = "ScheduleAttr.ReportID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("package schedule") > -1 Then
                        sEventPackCriteria(x) = "ScheduleAttr.PackID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("automation schedule") > -1 Then
                        sEventPackCriteria(x) = "ScheduleAttr.AutoID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("event-based schedule") > -1 Then
                        sEventPackCriteria(x) = "ScheduleAttr.ReportID = 1"
                    ElseIf sValue(I).ToLower.IndexOf("event-based package") > -1 Then
                        sEventPackCriteria(x) = "ScheduleAttr.EventPackID <> 0"
                    End If
                    x += 1
            End Select
        Next

        Dim sFinal As String = String.Empty
        Dim sType As String = m_smartType
        Dim sNotAdmin As String = ""
        Dim sQuery As String = ""

        sFinal = String.Empty

        Try
            For I = 0 To sEventPackCriteria.GetUpperBound(0)
                If I < sEventPackCriteria.GetUpperBound(0) Then
                    sFinal &= sEventPackCriteria(I) & sType
                Else
                    sFinal &= sEventPackCriteria(I)
                End If
            Next

            sQuery = "SELECT EventPackageAttr.EventPackID FROM EventPackageAttr INNER JOIN ScheduleAttr ON " & _
            "EventPackageAttr.EventPackID = ScheduleAttr.EventPackID WHERE " & sFinal

            If gRole.ToLower <> "administrator" Then
                sNotAdmin = " AND EventPackageAttr.EventPackID IN (SELECT EventPackID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                sQuery &= sNotAdmin
            End If
        Catch
            sQuery = String.Empty
        End Try


        Return sQuery

    End Function

    Public Function getReports() As ArrayList
        Dim reports As ArrayList = New ArrayList
        Dim SQL As String = getReportQuery()
        Dim oRs As ADODB.Recordset = clsMarsData.DataItem.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim rpt As cssreport = New cssreport(oRs(0).Value)

                reports.Add(rpt)
                oRs.MoveNext()
            Loop

            oRs.Close()

            oRs = Nothing
        End If

        Return reports
    End Function

    Public Function getPackages() As ArrayList
        Dim packs As ArrayList = New ArrayList
        Dim SQL As String = getPackageQuery()
        Dim oRs As ADODB.Recordset = clsMarsData.DataItem.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim pack As Package = New Package(oRs(0).Value)

                packs.Add(pack)
                oRs.MoveNext()
            Loop

            oRs.Close()

            oRs = Nothing
        End If

        Return packs
    End Function

    Public Function getAutomations() As ArrayList
        Dim autos As ArrayList = New ArrayList
        Dim SQL As String = getAutomationQuery()
        Dim oRs As ADODB.Recordset = clsMarsData.DataItem.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim auto As Automation = New Automation(oRs(0).Value)

                autos.Add(auto)
                oRs.MoveNext()
            Loop

            oRs.Close()

            oRs = Nothing
        End If

        Return autos
    End Function

    Public Function getEventBased() As ArrayList
        Dim eventss As ArrayList = New ArrayList
        Dim SQL As String = getEventBasedQuery()
        Dim oRs As ADODB.Recordset = clsMarsData.DataItem.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim evt As EventBased = New EventBased(oRs(0).Value)

                eventss.Add(evt)
                oRs.MoveNext()
            Loop

            oRs.Close()

            oRs = Nothing
        End If

        Return eventss
    End Function

    Public Function getEventBasedPackages() As ArrayList
        Dim eventpacks As ArrayList = New ArrayList
        Dim SQL As String = getEventPackageQuery()
        Dim oRs As ADODB.Recordset = clsMarsData.DataItem.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim pack As EventBasedPackage = New EventBasedPackage(oRs(0).Value)

                eventpacks.Add(pack)
                oRs.MoveNext()
            Loop

            oRs.Close()

            oRs = Nothing
        End If

        Return eventpacks
    End Function
End Class
