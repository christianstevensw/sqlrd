﻿Imports DevComponents.AdvTree
Imports System.Collections.Generic

Public Class frmCloudBrowser
    Dim m_db As clsDropbox
    Dim rootImage, folderImage, fileImage As Image
    Dim cancel As Boolean = False

    Public Property SelectedPath As String
        Get
            Return "dropbox" & txtSelectedPath.Text
        End Get
        Set(value As String)
            txtSelectedPath.Text = value.Remove(0, 7)
        End Set
    End Property
    Public Function browseDropBoxFolders(token As String, secret As String) As DialogResult
        Dim db As clsDropbox = New clsDropbox(token, secret)

        rootImage = resizeImage(My.Resources.dropbox, 16, 16)
        folderImage = resizeImage(My.Resources.Folder_256x2561, 16, 16)
        fileImage = resizeImage(My.Resources.document, 16, 16)

        m_db = db

        tvCloud.Nodes.Clear()

        buildTree("dropbox")

        Me.ShowDialog()

        If cancel Then Return Windows.Forms.DialogResult.Cancel

        Return Windows.Forms.DialogResult.OK
    End Function

    Private Sub buildTree(path As String)
        '//add folders
        Dim folders As List(Of String) = m_db.getDropboxFolders(path)
        Dim root As Node = New Node("Dropbox")

        root.Image = rootImage
        root.DataKey = ""

        For Each s As String In folders
            Dim folderName As String = clsDropbox.getAbsoluteItemName(s)
            Dim folderNode As Node = New Node(folderName)
            folderNode.Image = folderImage
            folderNode.DataKey = s
            folderNode.Tag = "folder"

            root.Nodes.Add(folderNode)
        Next

        tvCloud.Nodes.Add(root)

        root.ExpandAll()
    End Sub

    Private Sub tvCloud_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tvCloud.SelectedIndexChanged
        If tvCloud.SelectedNode Is Nothing Then Return

        If tvCloud.SelectedNode.DataKey = "" Then Return

        Dim parentNode As Node = tvCloud.SelectedNode
        Dim folderPath As String = parentNode.DataKey

        If parentNode.Nodes.Count = 0 Then
            tvCloud.Enabled = False

            Dim folders As List(Of String) = m_db.getDropboxFolders("dropbox" & folderPath)

            For Each s As String In folders
                Dim folderName As String = clsDropbox.getAbsoluteItemName(s)
                Dim folderNode As Node = New Node(folderName)
                folderNode.Image = folderImage
                folderNode.DataKey = s
                folderNode.Tag = "folder"

                parentNode.Nodes.Add(folderNode)
            Next

            tvCloud.Enabled = True
        End If

        txtSelectedPath.Text = parentNode.DataKey
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Close()
    End Sub

    Private Sub btnOk_Click(sender As Object, e As EventArgs) Handles btnOk.Click
        If tvCloud.SelectedNode Is Nothing Then
            cancel = True
        ElseIf tvCloud.SelectedNode.DataKey = "" Then
            cancel = True
        ElseIf txtSelectedPath.Text = "" Then
            cancel = True
        End If

        Close()
    End Sub
End Class