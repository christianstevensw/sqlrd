﻿Public Class frmDefaultDestinationAssigner
    Dim imgList As ImageList
    Private Sub frmDefaultDestinationAssigner_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        AddHandler UcDest.lsvDestination.AfterCheck, AddressOf destinationCheckChanged

        imgList = createImageList(New String() {"group.png"})

        lsvGroups.SmallImageList = imgList
        lsvGroups.LargeImageList = imgList
    End Sub

    Private Sub _LoadGroups()
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oItem As ListViewItem

        SQL = "SELECT * FROM GroupAttr WHERE destinationAccessType = 'DefaultOnly'"

        oRs = clsMarsData.GetData(SQL)

        lsvGroups.Items.Clear()

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                oItem = New ListViewItem

                For Each o As ListViewItem In lsvGroups.Items
                    If o.Text = oRs("groupname").Value Then
                        GoTo skip
                    End If
                Next

                oItem.Text = oRs("groupname").Value
                oItem.SubItems.Add(oRs("groupdesc").Value)
                oItem.Tag = oRs("groupid").Value
                oItem.ImageIndex = 0

                lsvGroups.Items.Add(oItem)

skip:
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub

    Private Sub destinationCheckChanged(ByVal sender As Object, ByVal e As DevComponents.AdvTree.AdvTreeCellEventArgs)
        btnApply.Enabled = True
    End Sub

    Private Sub lsvGroups_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvGroups.SelectedIndexChanged
        If lsvGroups.SelectedItems.Count = 0 Then Return

        '//uncheck them all
        For Each it As DevComponents.AdvTree.Node In UcDest.lsvDestination.Nodes
            it.CheckBoxVisible = True
            it.Checked = False
        Next

        If lsvGroups.SelectedItems.Count = 1 Then
            Dim groupID As Integer = lsvGroups.SelectedItems(0).Tag
            Dim usergrp As usergroup = New usergroup(groupID)

            For Each dest As String In usergrp.assignedDestinations
                '//search the destinations treeview
                For Each it As DevComponents.AdvTree.Node In UcDest.lsvDestination.Nodes
                    If it.Tag = dest Then
                        it.Checked = True
                        Exit For
                    End If
                Next
            Next
        End If

        btnApply.Enabled = False
    End Sub

    Private Sub btnApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApply.Click
        For Each it As ListViewItem In lsvGroups.SelectedItems
            Dim groupID As Integer = it.Tag
            Dim usergrp As usergroup = New usergroup(groupID)
            Dim dests As ArrayList = New ArrayList

            For Each destit As DevComponents.AdvTree.Node In UcDest.lsvDestination.Nodes
                If destit.Checked Then dests.Add(destit.Tag)
            Next

            usergrp.assignedDestinations = dests
        Next

        MessageBox.Show("The Destination assignments have been saved for the selected groups.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        btnApply.Enabled = False
    End Sub

    Public Sub assignDestinationToGroup(Optional ByVal groupID As Integer = 0)
        _LoadGroups()

        UcDest.DefaultDestinations = True
        UcDest.nPackID = 2777
        UcDest.nReportID = 2777
        UcDest.nSmartID = 2777
        UcDest.cmdImport.Visible = False
        UcDest.m_CanDisable = False
        UcDest.destinationsPicker = True
        UcDest.LoadAll()

        If groupID > 0 Then
            For Each it As ListViewItem In lsvGroups.Items
                If it.Tag = groupID Then
                    it.Selected = True
                    it.EnsureVisible()
                    Exit For
                End If
            Next
        End If

        Me.ShowDialog()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
End Class