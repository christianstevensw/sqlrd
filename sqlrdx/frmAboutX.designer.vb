﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAboutX
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAboutX))
        Me.txtWebLics = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtLicense2 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX12 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX9 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX8 = New DevComponents.DotNetBar.LabelX()
        Me.txtOutputlevel = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX7 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.txtEdition = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.txtProduct = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtCustomerName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtCustID2 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.ReflectionImage1 = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.btnOK = New DevComponents.DotNetBar.ButtonX()
        Me.lblEdition = New DevComponents.DotNetBar.LabelX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.lblVersion = New DevComponents.DotNetBar.LabelX()
        Me.ButtonX1 = New DevComponents.DotNetBar.ButtonX()
        Me.btnEdit = New DevComponents.DotNetBar.ButtonX()
        Me.SuspendLayout()
        '
        'txtWebLics
        '
        Me.txtWebLics.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtWebLics.BackColor = System.Drawing.Color.WhiteSmoke
        '
        '
        '
        Me.txtWebLics.Border.Class = "TextBoxBorder"
        Me.txtWebLics.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtWebLics.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtWebLics.ForeColor = System.Drawing.Color.Black
        Me.txtWebLics.Location = New System.Drawing.Point(443, 176)
        Me.txtWebLics.Name = "txtWebLics"
        Me.txtWebLics.ReadOnly = True
        Me.txtWebLics.Size = New System.Drawing.Size(223, 21)
        Me.txtWebLics.TabIndex = 15
        '
        'txtLicense2
        '
        Me.txtLicense2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtLicense2.BackColor = System.Drawing.Color.WhiteSmoke
        '
        '
        '
        Me.txtLicense2.Border.Class = "TextBoxBorder"
        Me.txtLicense2.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtLicense2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLicense2.ForeColor = System.Drawing.Color.Black
        Me.txtLicense2.Location = New System.Drawing.Point(443, 149)
        Me.txtLicense2.Name = "txtLicense2"
        Me.txtLicense2.ReadOnly = True
        Me.txtLicense2.Size = New System.Drawing.Size(179, 21)
        Me.txtLicense2.TabIndex = 13
        '
        'LabelX12
        '
        '
        '
        '
        Me.LabelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX12.Location = New System.Drawing.Point(257, 176)
        Me.LabelX12.Name = "LabelX12"
        Me.LabelX12.Size = New System.Drawing.Size(145, 21)
        Me.LabelX12.TabIndex = 7
        Me.LabelX12.Text = "Web licenses"
        '
        'LabelX9
        '
        '
        '
        '
        Me.LabelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX9.Location = New System.Drawing.Point(257, 149)
        Me.LabelX9.Name = "LabelX9"
        Me.LabelX9.Size = New System.Drawing.Size(145, 21)
        Me.LabelX9.TabIndex = 8
        Me.LabelX9.Text = "License Number"
        '
        'LabelX8
        '
        '
        '
        '
        Me.LabelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX8.Location = New System.Drawing.Point(257, 123)
        Me.LabelX8.Name = "LabelX8"
        Me.LabelX8.Size = New System.Drawing.Size(145, 21)
        Me.LabelX8.TabIndex = 3
        Me.LabelX8.Text = "Outputs"
        '
        'txtOutputlevel
        '
        Me.txtOutputlevel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtOutputlevel.BackColor = System.Drawing.Color.WhiteSmoke
        '
        '
        '
        Me.txtOutputlevel.Border.Class = "TextBoxBorder"
        Me.txtOutputlevel.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtOutputlevel.ForeColor = System.Drawing.Color.Black
        Me.txtOutputlevel.Location = New System.Drawing.Point(443, 123)
        Me.txtOutputlevel.Name = "txtOutputlevel"
        Me.txtOutputlevel.ReadOnly = True
        Me.txtOutputlevel.Size = New System.Drawing.Size(223, 21)
        Me.txtOutputlevel.TabIndex = 12
        '
        'LabelX7
        '
        '
        '
        '
        Me.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX7.Location = New System.Drawing.Point(257, 95)
        Me.LabelX7.Name = "LabelX7"
        Me.LabelX7.Size = New System.Drawing.Size(145, 21)
        Me.LabelX7.TabIndex = 6
        Me.LabelX7.Text = "Edition"
        '
        'LabelX6
        '
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.Location = New System.Drawing.Point(257, 68)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(145, 21)
        Me.LabelX6.TabIndex = 2
        Me.LabelX6.Text = "Product"
        '
        'txtEdition
        '
        Me.txtEdition.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtEdition.BackColor = System.Drawing.Color.WhiteSmoke
        '
        '
        '
        Me.txtEdition.Border.Class = "TextBoxBorder"
        Me.txtEdition.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtEdition.ForeColor = System.Drawing.Color.Black
        Me.txtEdition.Location = New System.Drawing.Point(443, 95)
        Me.txtEdition.Name = "txtEdition"
        Me.txtEdition.ReadOnly = True
        Me.txtEdition.Size = New System.Drawing.Size(223, 21)
        Me.txtEdition.TabIndex = 14
        '
        'LabelX5
        '
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.Location = New System.Drawing.Point(257, 41)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(115, 21)
        Me.LabelX5.TabIndex = 4
        Me.LabelX5.Text = "Company Name"
        '
        'LabelX4
        '
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.Location = New System.Drawing.Point(257, 14)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(145, 21)
        Me.LabelX4.TabIndex = 5
        Me.LabelX4.Text = "Customer Number"
        '
        'txtProduct
        '
        Me.txtProduct.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtProduct.BackColor = System.Drawing.Color.WhiteSmoke
        '
        '
        '
        Me.txtProduct.Border.Class = "TextBoxBorder"
        Me.txtProduct.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtProduct.ForeColor = System.Drawing.Color.Black
        Me.txtProduct.Location = New System.Drawing.Point(443, 68)
        Me.txtProduct.Name = "txtProduct"
        Me.txtProduct.ReadOnly = True
        Me.txtProduct.Size = New System.Drawing.Size(223, 21)
        Me.txtProduct.TabIndex = 9
        '
        'txtCustomerName
        '
        Me.txtCustomerName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtCustomerName.BackColor = System.Drawing.Color.WhiteSmoke
        '
        '
        '
        Me.txtCustomerName.Border.Class = "TextBoxBorder"
        Me.txtCustomerName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCustomerName.ForeColor = System.Drawing.Color.Black
        Me.txtCustomerName.Location = New System.Drawing.Point(443, 41)
        Me.txtCustomerName.Name = "txtCustomerName"
        Me.txtCustomerName.ReadOnly = True
        Me.txtCustomerName.Size = New System.Drawing.Size(223, 21)
        Me.txtCustomerName.TabIndex = 11
        '
        'txtCustID2
        '
        Me.txtCustID2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtCustID2.BackColor = System.Drawing.Color.WhiteSmoke
        '
        '
        '
        Me.txtCustID2.Border.Class = "TextBoxBorder"
        Me.txtCustID2.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCustID2.ForeColor = System.Drawing.Color.Black
        Me.txtCustID2.Location = New System.Drawing.Point(443, 14)
        Me.txtCustID2.Name = "txtCustID2"
        Me.txtCustID2.ReadOnly = True
        Me.txtCustID2.Size = New System.Drawing.Size(223, 21)
        Me.txtCustID2.TabIndex = 10
        '
        'ReflectionImage1
        '
        '
        '
        '
        Me.ReflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ReflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.ReflectionImage1.Image = CType(resources.GetObject("ReflectionImage1.Image"), System.Drawing.Image)
        Me.ReflectionImage1.Location = New System.Drawing.Point(-2, 64)
        Me.ReflectionImage1.Name = "ReflectionImage1"
        Me.ReflectionImage1.ReflectionEnabled = False
        Me.ReflectionImage1.Size = New System.Drawing.Size(679, 232)
        Me.ReflectionImage1.TabIndex = 16
        '
        'btnOK
        '
        Me.btnOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnOK.Location = New System.Drawing.Point(591, 328)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnOK.TabIndex = 17
        Me.btnOK.Text = "Dismiss"
        '
        'lblEdition
        '
        '
        '
        '
        Me.lblEdition.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblEdition.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lblEdition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEdition.Location = New System.Drawing.Point(0, 363)
        Me.lblEdition.Name = "lblEdition"
        Me.lblEdition.Size = New System.Drawing.Size(672, 23)
        Me.lblEdition.TabIndex = 18
        Me.lblEdition.Text = "Copyright ChristianSteven Software 2012"
        '
        'LabelX1
        '
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(373, 292)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(293, 23)
        Me.LabelX1.TabIndex = 18
        Me.LabelX1.Text = "Maximum outputs per 24 hour period: "
        Me.LabelX1.TextAlignment = System.Drawing.StringAlignment.Far
        '
        'lblVersion
        '
        '
        '
        '
        Me.lblVersion.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblVersion.Location = New System.Drawing.Point(0, 292)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(293, 23)
        Me.lblVersion.TabIndex = 18
        Me.lblVersion.Text = "Maximum outputs per 24 hour period: "
        '
        'ButtonX1
        '
        Me.ButtonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.ButtonX1.Image = CType(resources.GetObject("ButtonX1.Image"), System.Drawing.Image)
        Me.ButtonX1.Location = New System.Drawing.Point(628, 203)
        Me.ButtonX1.Name = "ButtonX1"
        Me.ButtonX1.Size = New System.Drawing.Size(38, 21)
        Me.ButtonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ButtonX1.TabIndex = 21
        Me.ButtonX1.Visible = False
        '
        'btnEdit
        '
        Me.btnEdit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnEdit.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnEdit.Image = CType(resources.GetObject("btnEdit.Image"), System.Drawing.Image)
        Me.btnEdit.Location = New System.Drawing.Point(628, 149)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(38, 21)
        Me.btnEdit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnEdit.TabIndex = 22
        '
        'frmAboutX
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(672, 386)
        Me.ControlBox = False
        Me.Controls.Add(Me.ButtonX1)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.lblEdition)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.txtWebLics)
        Me.Controls.Add(Me.txtLicense2)
        Me.Controls.Add(Me.LabelX12)
        Me.Controls.Add(Me.LabelX9)
        Me.Controls.Add(Me.LabelX8)
        Me.Controls.Add(Me.txtOutputlevel)
        Me.Controls.Add(Me.LabelX7)
        Me.Controls.Add(Me.LabelX6)
        Me.Controls.Add(Me.txtEdition)
        Me.Controls.Add(Me.LabelX5)
        Me.Controls.Add(Me.LabelX4)
        Me.Controls.Add(Me.txtProduct)
        Me.Controls.Add(Me.txtCustomerName)
        Me.Controls.Add(Me.txtCustID2)
        Me.Controls.Add(Me.ReflectionImage1)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.LabelX1)
        Me.DoubleBuffered = True
        Me.EnableGlass = False
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmAboutX"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "About SQL-RD"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtWebLics As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtLicense2 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtOutputlevel As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtEdition As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtProduct As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtCustomerName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtCustID2 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents ReflectionImage1 As DevComponents.DotNetBar.Controls.ReflectionImage
    Friend WithEvents btnOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lblEdition As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblVersion As DevComponents.DotNetBar.LabelX
    Friend WithEvents ButtonX1 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnEdit As DevComponents.DotNetBar.ButtonX
End Class
