﻿Imports System.Drawing.Drawing2D

Module globalVars

    '  Public mainDS As DataSet


    Public Function MakeGrayscale3(ByVal original As Bitmap) As Bitmap
        'create a blank bitmap the same size as original
        Dim newBitmap As New Bitmap(original.Width, original.Height)

        'get a graphics object from the new image
        Using g As Graphics = Graphics.FromImage(newBitmap)

            'create the grayscale ColorMatrix
            Dim colorMatrix As New Imaging.ColorMatrix(New Single()() {New Single() {0.3F, 0.3F, 0.3F, 0, 0}, New Single() {0.59F, 0.59F, 0.59F, 0, 0}, New Single() {0.11F, 0.11F, 0.11F, 0, 0}, New Single() {0, 0, 0, 1, 0}, New Single() {0, 0, 0, 0, 1}})

            'create some image attributes
            Using attributes As New Imaging.ImageAttributes()

                'set the color matrix attribute
                attributes.SetColorMatrix(colorMatrix)

                'draw the original image on the new image
                'using the grayscale color matrix
                g.DrawImage(original, New Rectangle(0, 0, original.Width, original.Height), 0, 0, original.Width, original.Height, _
                 GraphicsUnit.Pixel, attributes)

                'dispose the Graphics object
            End Using

            colorMatrix = Nothing
        End Using

        GC.Collect()

        Return newBitmap
    End Function

    Public Function resizeImage(ByVal original As Bitmap, ByVal targetwidth As Integer, ByVal targetheight As Integer) As Bitmap
        'following code resizes picture to fit
        Dim thumb As New Bitmap(targetwidth, targetheight)

        Using bm As Bitmap = original

            Using g As Graphics = Graphics.FromImage(thumb)

                g.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic

                g.DrawImage(bm, New Rectangle(0, 0, targetwidth, targetheight), New Rectangle(0, 0, bm.Width, _
            bm.Height), GraphicsUnit.Pixel)

            End Using

        End Using

        GC.Collect()

        Return thumb
    End Function

    Public Function resizeImage(ByVal original As Bitmap, ByVal newsize As Size) As Bitmap
        'following code resizes picture to fit

        Dim thumb As New Bitmap(newsize.Width, newsize.Height)

        Using bm As Bitmap = original

            Using g As Graphics = Graphics.FromImage(thumb)

                g.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic

                g.DrawImage(bm, New Rectangle(0, 0, newsize.Width, newsize.Width), New Rectangle(0, 0, bm.Width, bm.Height), GraphicsUnit.Pixel)

            End Using

        End Using

        GC.Collect()

        Return thumb
    End Function

    Public Enum ShadowDirections As Integer
        TOP_RIGHT = 1
        BOTTOM_RIGHT = 2
        BOTTOM_LEFT = 3
        TOP_LEFT = 4
    End Enum





End Module
