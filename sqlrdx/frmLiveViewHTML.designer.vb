﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLiveViewHTML
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLiveViewHTML))
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnCancel = New DevComponents.DotNetBar.ButtonX()
        Me.btnSave = New DevComponents.DotNetBar.ButtonX()
        Me.txtHTML = New FastColoredTextBoxNS.FastColoredTextBox()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.txtHTML, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCancel)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnSave)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 616)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(863, 31)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'btnCancel
        '
        Me.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnCancel.Location = New System.Drawing.Point(785, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "C&ancel"
        '
        'btnSave
        '
        Me.btnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnSave.Location = New System.Drawing.Point(704, 3)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        '
        'txtHTML
        '
        Me.txtHTML.AutoScrollMinSize = New System.Drawing.Size(0, 14)
        Me.txtHTML.BackBrush = Nothing
        Me.txtHTML.CharHeight = 14
        Me.txtHTML.CharWidth = 8
        Me.txtHTML.CommentPrefix = Nothing
        Me.txtHTML.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHTML.DisabledColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(180, Byte), Integer))
        Me.txtHTML.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtHTML.Font = New System.Drawing.Font("Courier New", 9.75!)
        Me.txtHTML.IsReplaceMode = False
        Me.txtHTML.Language = FastColoredTextBoxNS.Language.HTML
        Me.txtHTML.LeftBracket = Global.Microsoft.VisualBasic.ChrW(60)
        Me.txtHTML.LeftBracket2 = Global.Microsoft.VisualBasic.ChrW(40)
        Me.txtHTML.Location = New System.Drawing.Point(0, 0)
        Me.txtHTML.Name = "txtHTML"
        Me.txtHTML.Paddings = New System.Windows.Forms.Padding(0)
        Me.txtHTML.RightBracket = Global.Microsoft.VisualBasic.ChrW(62)
        Me.txtHTML.RightBracket2 = Global.Microsoft.VisualBasic.ChrW(41)
        Me.txtHTML.SelectionColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtHTML.Size = New System.Drawing.Size(863, 616)
        Me.txtHTML.TabIndex = 2
        Me.txtHTML.WordWrap = True
        Me.txtHTML.Zoom = 100
        '
        'frmLiveViewHTML
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(863, 647)
        Me.Controls.Add(Me.txtHTML)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.EnableGlass = False
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmLiveViewHTML"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Edit HTML"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        CType(Me.txtHTML, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnSave As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtHTML As FastColoredTextBoxNS.FastColoredTextBox
End Class
