﻿Imports System.ComponentModel
<DefaultPropertyAttribute("Scheduler")> _
Public Class schedulerInfo

    Private lastNormalPoll As Date
    <CategoryAttribute("Normal Schedules"), Description("The date/time the scheduler last polled"), DisplayName("Last Polled")> _
    Public ReadOnly Property LastPolled() As String
        Get
            Dim infoFile As String

            Dim svc As clsServiceController = New clsServiceController

            Select Case svc.m_serviceType
                Case clsServiceController.enum_svcType.BG_APPLICATION
                    infoFile = IO.Path.Combine(sAppPath, "bgservice.info")
                Case clsServiceController.enum_svcType.NT_SERVICE
                    infoFile = IO.Path.Combine(sAppPath, "ntservice.info")
                Case clsServiceController.enum_svcType.NONE
                    Return "No scheduler installed"
            End Select

            If IO.File.Exists(infoFile) Then
                Dim scheduleInfo() As String = IO.File.ReadAllLines(infoFile)

                '//we need the first line
                Try
                    Dim result As String = scheduleInfo(0)

                    result = result.Replace("Last Polled: ", String.Empty)

                    Return result.Trim
                Catch ex As Exception
                    Return "No information found. Wait."
                End Try
            Else
                Return "No information found. Wait"
            End If
        End Get

    End Property


    Private lastNormalResult As String
    <CategoryAttribute("Normal Schedules"), Description("The number of schedules that were found last time the scheduler polled"), DisplayName("Last Result")> _
    Public ReadOnly Property m_lastNormalResult() As String
        Get
            Dim infoFile As String

            Dim svc As clsServiceController = New clsServiceController

            Select Case svc.m_serviceType
                Case clsServiceController.enum_svcType.BG_APPLICATION
                    infoFile = IO.Path.Combine(sAppPath, "bgservice.info")
                Case clsServiceController.enum_svcType.NT_SERVICE
                    infoFile = IO.Path.Combine(sAppPath, "ntservice.info")
                Case clsServiceController.enum_svcType.NONE
                    Return "No scheduler installed"
            End Select

            If IO.File.Exists(infoFile) Then
                Dim scheduleInfo() As String = IO.File.ReadAllLines(infoFile)

                '//we need the second line
                Try
                    Dim result As String = scheduleInfo(1)

                    result = result.Split(":")(1)

                    Return result
                Catch ex As Exception
                    Return "No information found. Wait."
                End Try
            Else
                Return "No information found. Wait"
            End If
        End Get
    End Property
    <CategoryAttribute("Event-Based Schedules"), Description("The date/time the Event-Based scheduler last polled"), DisplayName("Last Poll (EB)")> _
    Public ReadOnly Property m_lastEventBasedPoll As String
        Get
            Dim svc As clsServiceController = New clsServiceController

            If svc.m_serviceType <> clsServiceController.enum_svcType.NONE Then
                If IO.File.Exists(sAppPath & "ebservice.info") Then
                    Dim ebInfo As String = ReadTextFromFile(sAppPath & "ebservice.info").Trim

                    Return ebInfo
                End If
            Else
                Return "Not Installed"
            End If
        End Get
    End Property
    <CategoryAttribute("Scheduler"), Description("The type of scheduler the system has been set up to use"), DisplayName("Scheduler Type")> _
    Public ReadOnly Property m_schedulerType As String
        Get
            Dim svc As clsServiceController = New clsServiceController

            Select Case svc.m_serviceType
                Case clsServiceController.enum_svcType.BG_APPLICATION
                    Return "Background App"
                Case clsServiceController.enum_svcType.NT_SERVICE
                    Return "NT Service"
                Case clsServiceController.enum_svcType.NONE
                    Return "NONE"
            End Select
        End Get
    End Property

    <CategoryAttribute("Normal Schedules"), Description("The set polling interval for the scheduler"), DisplayName("Polling Interval")> _
    Public ReadOnly Property normalPollingInterval As Integer
        Get
            clsMarsUI.MainUI.ReadRegistry("Poll", 30)
        End Get
    End Property
    <CategoryAttribute("Event-Based Schedules"), Description("The set polling interval for the Event-Based scheduler"), DisplayName("Polling Interval (EB)")> _
    Public ReadOnly Property eventPollingInterval As Integer
        Get
            Return clsMarsUI.MainUI.ReadRegistry("PollEB", 60)
        End Get
    End Property

    <CategoryAttribute("System"), Description("The currently logged in SQL-RD user"), DisplayName("User")> _
    Public ReadOnly Property User As String
        Get
            Return gUser
        End Get
    End Property
    <CategoryAttribute("System"), Description("The type of connection to the SQL-RD database"), DisplayName("Connection Type")> _
    Public ReadOnly Property ConnectionType As String
        Get
            Return clsMarsUI.MainUI.ReadRegistry("ConType", "LOCAL")
        End Get
    End Property

    <CategoryAttribute("System"), Description("The name of this workstation"), DisplayName("WorkstationID")> _
    Public ReadOnly Property WorkstationID As String
        Get
            Return Environment.MachineName
        End Get
    End Property
End Class
