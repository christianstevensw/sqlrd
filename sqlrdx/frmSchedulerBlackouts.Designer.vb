﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSchedulerBlackouts
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSchedulerBlackouts))
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel
        Me.btnCancel = New DevComponents.DotNetBar.ButtonX
        Me.btnOK = New DevComponents.DotNetBar.ButtonX
        Me.Label3 = New System.Windows.Forms.Label
        Me.btnRemoveBlackout = New DevComponents.DotNetBar.ButtonX
        Me.btnAddBlackout = New DevComponents.DotNetBar.ButtonX
        Me.cmbBlackOuts = New System.Windows.Forms.ComboBox
        Me.lsvBlackouts = New DevComponents.DotNetBar.Controls.ListvieweX
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCancel)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnOK)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 297)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(473, 31)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(395, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 0
        Me.btnCancel.Text = "&Cancel"
        ' = True
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(314, 3)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 1
        Me.btnOK.Text = "&OK"
        '  Me.btnOK.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(317, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "You may specify when the Scheduler will NOT run any schedules "
        '
        'btnRemoveBlackout
        '
        Me.btnRemoveBlackout.Image = CType(resources.GetObject("btnRemoveBlackout.Image"), System.Drawing.Image)
        Me.btnRemoveBlackout.Location = New System.Drawing.Point(420, 64)
        Me.btnRemoveBlackout.Name = "btnRemoveBlackout"
        Me.btnRemoveBlackout.Size = New System.Drawing.Size(46, 23)
        Me.btnRemoveBlackout.TabIndex = 6
        '' = True
        '
        'btnAddBlackout
        '
        Me.btnAddBlackout.Image = CType(resources.GetObject("btnAddBlackout.Image"), System.Drawing.Image)
        Me.btnAddBlackout.Location = New System.Drawing.Point(420, 22)
        Me.btnAddBlackout.Name = "btnAddBlackout"
        Me.btnAddBlackout.Size = New System.Drawing.Size(46, 23)
        Me.btnAddBlackout.TabIndex = 7
        ' Me.btnAddBlackout.UseVisualStyleBackColor = True
        '
        'cmbBlackOuts
        '
        Me.cmbBlackOuts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBlackOuts.FormattingEnabled = True
        Me.cmbBlackOuts.Location = New System.Drawing.Point(12, 25)
        Me.cmbBlackOuts.Name = "cmbBlackOuts"
        Me.cmbBlackOuts.Size = New System.Drawing.Size(401, 21)
        Me.cmbBlackOuts.TabIndex = 5
        '
        'lsvBlackouts
        '
        Me.lsvBlackouts.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader2})
        Me.lsvBlackouts.FullRowSelect = True
        Me.lsvBlackouts.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lsvBlackouts.HideSelection = False
        Me.lsvBlackouts.Location = New System.Drawing.Point(12, 64)
        Me.lsvBlackouts.Name = "lsvBlackouts"
        Me.lsvBlackouts.Size = New System.Drawing.Size(401, 228)
        Me.lsvBlackouts.TabIndex = 4
        Me.lsvBlackouts.UseCompatibleStateImageBehavior = False
        Me.lsvBlackouts.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Width = 384
        '
        'frmSchedulerBlackouts
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(473, 328)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnRemoveBlackout)
        Me.Controls.Add(Me.btnAddBlackout)
        Me.Controls.Add(Me.cmbBlackOuts)
        Me.Controls.Add(Me.lsvBlackouts)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmSchedulerBlackouts"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Scheduler Blackout Times"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnRemoveBlackout As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnAddBlackout As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmbBlackOuts As System.Windows.Forms.ComboBox
    Friend WithEvents lsvBlackouts As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
End Class
