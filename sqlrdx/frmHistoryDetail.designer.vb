﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHistoryDetail
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmHistoryDetail))
        Me.ReflectionImage1 = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.dtStart = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.dtEnd = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.txtDuration = New DevComponents.Editors.IntegerInput()
        Me.txtDetails = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.dtStart, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtEnd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDuration, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReflectionImage1
        '
        '
        '
        '
        Me.ReflectionImage1.BackgroundStyle.Class = ""
        Me.ReflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ReflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.ReflectionImage1.Image = CType(resources.GetObject("ReflectionImage1.Image"), System.Drawing.Image)
        Me.ReflectionImage1.Location = New System.Drawing.Point(353, 12)
        Me.ReflectionImage1.Name = "ReflectionImage1"
        Me.ReflectionImage1.Size = New System.Drawing.Size(128, 236)
        Me.ReflectionImage1.TabIndex = 0
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.LabelX1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.LabelX2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.LabelX3, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.LabelX4, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.dtStart, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dtEnd, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtDuration, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.txtDetails, 1, 3)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 12)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.910892!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.580858!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 82.50825!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(344, 332)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'LabelX1
        '
        '
        '
        '
        Me.LabelX1.BackgroundStyle.Class = ""
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(3, 3)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(75, 23)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "Started"
        '
        'LabelX2
        '
        '
        '
        '
        Me.LabelX2.BackgroundStyle.Class = ""
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Location = New System.Drawing.Point(3, 32)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(75, 21)
        Me.LabelX2.TabIndex = 0
        Me.LabelX2.Text = "Completed"
        '
        'LabelX3
        '
        '
        '
        '
        Me.LabelX3.BackgroundStyle.Class = ""
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Location = New System.Drawing.Point(3, 59)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(75, 20)
        Me.LabelX3.TabIndex = 0
        Me.LabelX3.Text = "Duration (sec)"
        '
        'LabelX4
        '
        '
        '
        '
        Me.LabelX4.BackgroundStyle.Class = ""
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.Location = New System.Drawing.Point(3, 85)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(75, 19)
        Me.LabelX4.TabIndex = 0
        Me.LabelX4.Text = "Details"
        '
        'dtStart
        '
        '
        '
        '
        Me.dtStart.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.dtStart.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtStart.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.dtStart.ButtonDropDown.Visible = True
        Me.dtStart.Enabled = False
        Me.dtStart.Format = DevComponents.Editors.eDateTimePickerFormat.LongTime
        Me.dtStart.IsPopupCalendarOpen = False
        Me.dtStart.Location = New System.Drawing.Point(84, 3)
        '
        '
        '
        Me.dtStart.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtStart.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window
        Me.dtStart.MonthCalendar.BackgroundStyle.Class = ""
        Me.dtStart.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtStart.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.dtStart.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.dtStart.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.dtStart.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.dtStart.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.dtStart.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.dtStart.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.dtStart.MonthCalendar.CommandsBackgroundStyle.Class = ""
        Me.dtStart.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtStart.MonthCalendar.DisplayMonth = New Date(2011, 12, 1, 0, 0, 0, 0)
        Me.dtStart.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.dtStart.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtStart.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.dtStart.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.dtStart.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.dtStart.MonthCalendar.NavigationBackgroundStyle.Class = ""
        Me.dtStart.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtStart.MonthCalendar.TodayButtonVisible = True
        Me.dtStart.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.dtStart.Name = "dtStart"
        Me.dtStart.Size = New System.Drawing.Size(166, 20)
        Me.dtStart.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.dtStart.TabIndex = 1
        '
        'dtEnd
        '
        '
        '
        '
        Me.dtEnd.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.dtEnd.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtEnd.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.dtEnd.ButtonDropDown.Visible = True
        Me.dtEnd.Enabled = False
        Me.dtEnd.Format = DevComponents.Editors.eDateTimePickerFormat.LongTime
        Me.dtEnd.IsPopupCalendarOpen = False
        Me.dtEnd.Location = New System.Drawing.Point(84, 32)
        '
        '
        '
        Me.dtEnd.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtEnd.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window
        Me.dtEnd.MonthCalendar.BackgroundStyle.Class = ""
        Me.dtEnd.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtEnd.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.dtEnd.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.dtEnd.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.dtEnd.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.dtEnd.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.dtEnd.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.dtEnd.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.dtEnd.MonthCalendar.CommandsBackgroundStyle.Class = ""
        Me.dtEnd.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtEnd.MonthCalendar.DisplayMonth = New Date(2011, 12, 1, 0, 0, 0, 0)
        Me.dtEnd.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.dtEnd.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtEnd.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.dtEnd.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.dtEnd.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.dtEnd.MonthCalendar.NavigationBackgroundStyle.Class = ""
        Me.dtEnd.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtEnd.MonthCalendar.TodayButtonVisible = True
        Me.dtEnd.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.dtEnd.Name = "dtEnd"
        Me.dtEnd.Size = New System.Drawing.Size(166, 20)
        Me.dtEnd.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.dtEnd.TabIndex = 1
        '
        'txtDuration
        '
        '
        '
        '
        Me.txtDuration.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtDuration.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDuration.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.txtDuration.Enabled = False
        Me.txtDuration.Location = New System.Drawing.Point(84, 59)
        Me.txtDuration.Name = "txtDuration"
        Me.txtDuration.ShowUpDown = True
        Me.txtDuration.Size = New System.Drawing.Size(166, 20)
        Me.txtDuration.TabIndex = 2
        '
        'txtDetails
        '
        '
        '
        '
        Me.txtDetails.Border.Class = "TextBoxBorder"
        Me.txtDetails.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDetails.Location = New System.Drawing.Point(84, 85)
        Me.txtDetails.Multiline = True
        Me.txtDetails.Name = "txtDetails"
        Me.txtDetails.ReadOnly = True
        Me.txtDetails.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDetails.Size = New System.Drawing.Size(257, 244)
        Me.txtDetails.TabIndex = 3
        '
        'frmHistoryDetail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(493, 356)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.ReflectionImage1)
        Me.DoubleBuffered = True
        Me.EnableGlass = False
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmHistoryDetail"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "History Details"
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.dtStart, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtEnd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDuration, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ReflectionImage1 As DevComponents.DotNetBar.Controls.ReflectionImage
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtStart As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents dtEnd As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents txtDuration As DevComponents.Editors.IntegerInput
    Friend WithEvents txtDetails As DevComponents.DotNetBar.Controls.TextBoxX
End Class
