﻿Public Class frmSchedulerBlackouts
    Dim operationHoursT As Hashtable = New Hashtable
    Dim cancel As Boolean

    Public Sub setSchedulerBlackouts()
        loadSchedulerBlackoutPeriods()

        Me.ShowDialog()

        If cancel Then Return

        setGroupBlackoutPeriods()

    End Sub

    Private Sub btnAddBlackout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddBlackout.Click
        If cmbBlackOuts.Text = "" Then Return

        cmbBlackOuts_SelectedIndexChanged(sender, e)
    End Sub

    Private Sub cmbBlackOuts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbBlackOuts.SelectedIndexChanged
        If cmbBlackOuts.Text = "" Then Return

        If cmbBlackOuts.Text = "<New...>" Then
            Dim opAdd As frmOperationalHours = New frmOperationalHours
            opAdd.Text = "Configure Blackout Hours"

            Dim newID As Integer = 0
            Dim sNew As String = opAdd.AddOperationalHours(newID)

            If sNew <> "" Then
                operationHoursT.Add(sNew, newID)
                Me.cmbBlackOuts.Items.Add(sNew)
                Me.cmbBlackOuts.Text = sNew
            End If
        Else
            For Each itx As ListViewItem In lsvBlackouts.Items
                If String.Compare(itx.Text, cmbBlackOuts.Text, True) = 0 Then
                    Return
                End If
            Next

            Dim it As ListViewItem = New ListViewItem(cmbBlackOuts.Text)
            it.Tag = operationHoursT.Item(cmbBlackOuts.Text)
            lsvBlackouts.Items.Add(it)
        End If
    End Sub
    '//the scheduler blackout times will use ID of 2911
    Private Sub loadSchedulerBlackoutPeriods()
        Dim SQL As String = "SELECT * FROM groupBlackoutAttr WHERE groupid = 2911"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then

            Do While oRs.EOF = False
                Dim opID As Integer = oRs("operationhrsid").Value
                Dim op As clsOperationalHours = New clsOperationalHours(opID)
                Dim it As ListViewItem

                If op.operationHoursName IsNot Nothing Then
                    it = New ListViewItem(op.operationHoursName)
                    it.Tag = opID

                    lsvBlackouts.Items.Add(it)
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub
    Private Sub setGroupBlackoutPeriods()
        clsMarsData.WriteData("DELETE FROM groupBlackoutAttr WHERE groupid = 2911")

        Dim cols, vals As String

        cols = "blackoutid,groupid,operationhrsid"

        For Each it As ListViewItem In lsvBlackouts.Items
            vals = clsMarsData.CreateDataID("groupBlackoutAttr", "blackoutid") & "," & _
            2911 & "," & _
            it.Tag

            clsMarsData.DataItem.InsertData("groupBlackoutAttr", cols, vals)
        Next

    End Sub

    Private Sub btnRemoveBlackout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveBlackout.Click
        Dim arr As New ArrayList

        For Each it As ListViewItem In lsvBlackouts.SelectedItems
            arr.Add(it)
        Next

        For Each it As ListViewItem In arr
            it.Remove()
        Next
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        cancel = True
        Close()
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Close()
    End Sub

    Private Sub frmSchedulerBlackouts_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormatForWinXP(Me)
        Me.cmbBlackOuts.Items.Clear()

        Dim SQL As String = "SELECT * FROM OperationAttr"

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(Sql)

        Me.cmbBlackOuts.Items.Add("<New...>")

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                cmbBlackOuts.Items.Add(oRs("operationname").Value)
                operationHoursT.Add(oRs("operationname").Value, oRs("operationid").Value)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub
End Class