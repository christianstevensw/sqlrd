﻿Public Class frmSupportRequest
    Dim sCompany As String
    Dim sName As String
    Dim sCustID As String
    Dim ScheduleType As String
    Dim MailType As String
    Dim navigatedUrl As String

    Public Sub requestHelp(ByVal sURL As String)

        Dim Obj As Object

        Dim I As Integer
        Dim Sel As Object
        Dim cur As String
        Dim X As Integer

        Dim Y As Double

        Dim oUI As clsMarsUI = New clsMarsUI
        Dim licenseFile As String = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "ChristianSteven\" & Application.ProductName & "\" & Application.ProductName & ".licx")

        If IO.File.Exists(licenseFile) Then
            Dim lic As licensex = New licensex(licenseFile)
            sCompany = lic.companyName
            sName = lic.companyName
            sCustID = lic.customerNumber
        Else
            sCompany = oUI.ReadRegistry("RegCo", "")
            sName = oUI.ReadRegistry("RegFirstName", "") & " " & oUI.ReadRegistry("RegLastName", "")
            sCustID = oUI.ReadRegistry("CustNo", "")
        End If
        
        ScheduleType = oUI.ReadRegistry("SQL-RDService", "")
        MailType = oUI.ReadRegistry("MailType", "")

        navigatedUrl = sURL
        ' Dim wb = CreateObject("InternetExplorer.Application")

        ' wb.Visible = False

        wb.Navigate(sURL)

        Me.Show()
        
    End Sub

    Private Sub wb_DocumentCompleted(sender As Object, e As WebBrowserDocumentCompletedEventArgs) Handles wb.DocumentCompleted

        If e.Url.Authority = "www.christiansteven.com" Then
            Dim oFile As FileVersionInfo = FileVersionInfo.GetVersionInfo(sAppPath & assemblyName)

            For Each obj As HtmlElement In wb.Document.GetElementsByTagName("input")
                Select Case obj.GetAttribute("name")
                    Case "Customer_Number"
                        obj.SetAttribute("value", sCustID)
                    Case "Company_Name"
                        obj.SetAttribute("value", sCompany)
                    Case "Customer_Name"
                        obj.SetAttribute("value", sName)
                    Case "Product_Version"
                        obj.SetAttribute("value", oFile.FileVersion)
                    Case "Build_Number"
                        obj.SetAttribute("value", oFile.Comments.ToLower.Replace("build", String.Empty).Trim)
                    Case "Operating_System"
                        obj.SetAttribute("value", Environment.OSVersion.ToString)
                End Select
            Next

            For Each obj As HtmlElement In wb.Document.GetElementsByTagName("select")
                Select Case obj.GetAttribute("name")
                    Case "Schduler_Type"
                        Select Case ScheduleType
                            Case "WindowsNT"
                                obj.SetAttribute("value", "Windows Service")
                            Case "WindowsApp"
                                obj.SetAttribute("value", "Background Scheduling Application")
                            Case "NONE"
                                obj.SetAttribute("value", "No Scheduling")
                            Case Else
                                obj.SetAttribute("value", "No Scheduling")
                        End Select
                    Case "Crystal_Version"
                        Dim build As String = oFile.Comments.ToLower.Replace("build", String.Empty).Trim
                        Dim crystalVersion As String = build.Split("/")(1).ToLower
                      
                        Select Case crystalVersion
                            Case "85"
                                obj.SetAttribute("value", "8.5")
                            Case "90"
                                obj.SetAttribute("value", "9")
                            Case "100"
                                obj.SetAttribute("value", "10")
                            Case "110"
                                obj.SetAttribute("value", "XI (11)")
                            Case "115", "116"
                                obj.SetAttribute("value", "XI R2 (11.5)")
                            Case "120"
                                obj.SetAttribute("value", "2008")
                            Case "130"
                                obj.SetAttribute("value", "2011")
                        End Select
                End Select
            Next
        End If
    End Sub

 
End Class