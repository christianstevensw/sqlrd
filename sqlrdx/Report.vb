﻿
Partial Public Class cssreport
    Dim concernedRow As ADODB.Recordset
    Dim m_reportid As Integer
    Dim WithEvents dtReports As DataTable
    Public m_schedule As Schedule
    Dim newReport As Boolean = False
    Dim m_packid As Integer = 0



    Sub New(ByVal reportid As Integer)
        m_reportid = reportid
        m_schedule = New Schedule(m_reportid, Schedule.parentscheduleType.REPORT)

        concernedRow = clsMarsData.GetData("SELECT * FROM reportattr WHERE reportid =" & reportid)
    End Sub

    Sub New(ByVal reportid As Integer, ByVal packid As Integer)
        m_reportid = reportid
        Me.packid = packid

        concernedRow = clsMarsData.GetData("SELECT * FROM reportattr WHERE reportid =" & reportid & " AND packid =" & packid)
    End Sub

    Public Function editReportDesign()
        Try
            MessageBox.Show("Please remember to refresh the schedule (right-click then select 'Refresh') once you have made your changes so that they are " &
                             "reflected in SQL-RD", Application.ProductName, MessageBoxButtons.OK)

            Process.Start(Me.reportLocation)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 0, "Please install Crystal Reports")
        End Try

    End Function

    Public ReadOnly Property schedulePath As String
        Get
            Dim parent As Integer = Me.reportParent
            Dim folder As folder = New folder(parent)

            Return folder.getFolderPath()
        End Get
    End Property

    Public Property packageOrderNumber As Integer
        Get
            Return IsNull(concernedRow("packorderid").Value, 1)
        End Get
        Set(ByVal value As Integer)
            setConcernedRow("packorderid", value)
        End Set
    End Property
    Public Property packid As Integer
        Get
            Return IsNull(concernedRow("packid").Value, 0)
        End Get
        Set(value As Integer)
            m_packid = value
        End Set
    End Property
    Public Property reportName() As String
        Get
            Return concernedRow("reporttitle").Value
        End Get
        Set(ByVal value As String)
            setConcernedRow("reporttitle", value)
            setConcernedRow("reportname", value)
        End Set
    End Property

    Public Property staticDest() As Boolean
        Get
            Return Convert.ToInt32(IsNull(concernedRow("staticdest").Value, 0))
        End Get
        Set(ByVal value As Boolean)
            setConcernedRow("staticdest", Convert.ToInt32(value))
        End Set
    End Property
    Public Property isdynamic() As Boolean
        Get
            Return IsNull(concernedRow("dynamic").Value, False)
        End Get
        Set(ByVal value As Boolean)
            setConcernedRow("dynamic", Convert.ToInt32(value))
        End Set

    End Property

    Public Property isdatadriven() As Boolean
        Get
            Return IsNull(concernedRow("isdatadriven").Value, False)
        End Get
        Set(ByVal value As Boolean)
            setConcernedRow("isdatadriven", Convert.ToInt32(value))
        End Set
    End Property

    Public Property isbursting() As Boolean
        Get
            Return IsNull(concernedRow("bursting").Value, False)
        End Get
        Set(ByVal value As Boolean)
            setConcernedRow("bursting", Convert.ToInt32(value))
        End Set
    End Property

    Public WriteOnly Property recordSelectionFormula() As String
        Set(ByVal value As String)
            setConcernedRow("selectionformula", value)
        End Set
    End Property

    Public ReadOnly Property packagedReportStatus As Boolean
        Get
            Dim ors As ADODB.Recordset = clsMarsData.GetData(String.Format("SELECT status FROM packagedreportattr WHERE reportid = {0}", Me.ID))

            If ors IsNot Nothing AndAlso ors.EOF = False Then
                Dim status As Boolean = IsNull(ors("status").Value, 1)

                ors.Close()

                Return status
            Else
                Return True
            End If
        End Get
    End Property
    Public ReadOnly Property status() As Boolean
        Get
            If Me.isPackagedReport Then
                Return Me.packagedReportStatus
            Else
                Return m_schedule.status
            End If


        End Get
    End Property


    Public ReadOnly Property ID As Integer
        Get
            Return m_reportid
        End Get
    End Property
    Public ReadOnly Property reportType() As String
        Get
            If isbursting = True Then
                Return "Bursting Schedule"
            ElseIf isdatadriven = True Then
                Return "Data-Driven Schedule"
            ElseIf isdynamic = True Then
                Return "Dynamic Schedule"
            Else
                Return "Single Schedule"
            End If
        End Get
    End Property

    Public ReadOnly Property reportImage As Image
        Get

            Dim imageToUse As Image

            If packid = 0 Then

                If isbursting Then
                    imageToUse = My.Resources.document_attachment2
                ElseIf isdatadriven Then
                    imageToUse = My.Resources.document_datadriven_schedule1
                ElseIf isdynamic Then
                    imageToUse = My.Resources.document_pulse2
                Else
                    imageToUse = My.Resources.document_chart2
                End If

                'If status = False Then
                '    imageToUse = MakeGrayscale3(imageToUse)
                'End If
            Else
                imageToUse = My.Resources.document_chart2

                'If packedStatus = False Then
                '    imageToUse = MakeGrayscale3(imageToUse)
                'End If
            End If

            Return imageToUse
        End Get
    End Property
    Public ReadOnly Property lastRun() As String
        Get
            Return m_schedule.lastRun
        End Get
    End Property

    Public ReadOnly Property lastResult() As String
        Get
            Return m_schedule.lastResult
        End Get
    End Property

    Public ReadOnly Property nextRun() As Date
        Get
            Return m_schedule.nextRun
        End Get
    End Property

    Public Property owner() As String
        Get
            Return concernedRow("owner").Value
        End Get
        Set(ByVal value As String)
            setConcernedRow("owner", value)
        End Set
    End Property

    Public ReadOnly Property reportPath() As String
        Get
            Return concernedRow("cachepath").Value
        End Get
    End Property



    Public Property usesavedData() As Boolean
        Get
            Return concernedRow("usesaveddata").Value
        End Get
        Set(ByVal value As Boolean)
            setConcernedRow("usesaveddata", Convert.ToInt32(value))
        End Set
    End Property

    Public Property useLogin() As Boolean
        Get
            Return concernedRow("uselogin").Value
        End Get
        Set(ByVal value As Boolean)
            setConcernedRow("uselogin", Convert.ToInt16(value))
        End Set
    End Property

    Public Property collectReportFields() As Boolean
        Get
            Return IsNull(concernedRow("parsereportfields").Value, False)
        End Get
        Set(ByVal value As Boolean)
            setConcernedRow("parsereportfields", Convert.ToInt16(value))
        End Set
    End Property

    Public Property reportLocation() As String
        Get
            Return concernedRow("databasepath").Value
        End Get
        Set(ByVal value As String)
            setConcernedRow("databasepath", value)
        End Set
    End Property

    Public Property serverUrl() As String
        Get
            Return concernedRow("databasepath").Value
        End Get
        Set(ByVal value As String)
            setConcernedRow("databasepath", value)
        End Set
    End Property

    Public Property serverUser() As String
        Get
            Return concernedRow("rptuserid").Value
        End Get
        Set(ByVal value As String)
            setConcernedRow("rptuserid", value)
        End Set
    End Property
    Public Property serverPassword() As String
        Get
            Return _DecryptDBValue(concernedRow("RptPassword").Value)
        End Get
        Set(ByVal value As String)
            setConcernedRow("RptPassword", _EncryptDBValue(value))
        End Set
    End Property

    Public Property serverFormsAuth() As Boolean
        Get
            Return IsNull(concernedRow("rptFormsAuth").Value, 0)
        End Get
        Set(ByVal value As Boolean)
            setConcernedRow("rptFormsAuth", value)
        End Set
    End Property
    Public Property reportParent() As Integer
        Get
            Return concernedRow("parent").Value
        End Get
        Set(ByVal value As Integer)
            setConcernedRow("parent", value)
        End Set
    End Property

    Public ReadOnly Property destinations() As destination()
        Get
            Dim dests() As destination = Nothing
            Dim I As Integer = 0
            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM destinationattr WHERE reportid =" & m_reportid & " ORDER BY destorderid")

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    ReDim Preserve dests(I)

                    dests(I) = New destination(oRs("destinationid").Value)

                    I += 1

                    oRs.MoveNext()
                Loop

                oRs.Close()
                Return dests
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property TotalDestinationsLive() As Integer
        Get
            Dim dests() As destination = Nothing
            Dim I As Integer = 0
            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT COUNT(*) FROM destinationattr WHERE reportid =" & m_reportid & " AND enabledstatus = 1 ORDER BY destorderid")
            Dim total As Integer = 0

            If oRs IsNot Nothing Then

                total = oRs(0).Value

                oRs.Close()
                Return total
            Else
                Return 0
            End If
        End Get
    End Property
    Public ReadOnly Property lastRefreshed(Optional format As String = "yyyy-MM-dd HH:mm:ss") As String
        Get
            Try
                Dim dt As Date = concernedRow("lastrefreshed").Value

                Return dt.ToString(format)
            Catch ex As Exception
                Return "Never"
            End Try
        End Get
    End Property

    Public Property reportdatabaseType() As String
        Get
            Return IsNull(concernedRow("rptdatabasetype").Value)
        End Get
        Set(ByVal value As String)
            setConcernedRow("rptdatabasetype", value)
        End Set
    End Property
    Public Property reportservername() As String
        Get
            Return concernedRow("rptserver").Value
        End Get
        Set(ByVal value As String)
            setConcernedRow("rptserver", value)
        End Set
    End Property

    Public Property reportdatabasename() As String
        Get
            Return concernedRow("rptdatabase").Value
        End Get
        Set(ByVal value As String)
            setConcernedRow("rptdatabase", value)
        End Set
    End Property

    Public Property reportuserid() As String
        Get
            Return concernedRow("rptuserid").Value
        End Get
        Set(ByVal value As String)
            setConcernedRow("rptuserid", value)
        End Set
    End Property

    Public Property reportuserpassword() As String
        Get
            Return _DecryptDBValue(concernedRow("rptpassword").Value)
        End Get
        Set(ByVal value As String)
            setConcernedRow("rptpassword", _EncryptDBValue(value))
        End Set
    End Property

    Public Property autorefresh() As Boolean
        Get
            Return IsNull(concernedRow("autorefresh").Value, False)
        End Get
        Set(ByVal value As Boolean)
            setConcernedRow("autorefresh", Convert.ToInt32(value))
        End Set
    End Property

    Public Property doretryafterfailure() As Boolean
        Get
            Return IsNull(concernedRow("retry").Value, True)
        End Get
        Set(ByVal value As Boolean)
            setConcernedRow("retry", Convert.ToInt32(value))
        End Set
    End Property

    Public Property autocalculculateduration() As Boolean
        Get
            Return IsNull(concernedRow("autocalc").Value, True)
        End Get
        Set(ByVal value As Boolean)
            setConcernedRow("autocalc", Convert.ToInt32(value))
        End Set
    End Property

    Public Property autofailafter() As Integer
        Get
            Return IsNull(concernedRow("assumefail").Value, 30)
        End Get
        Set(ByVal value As Integer)
            setConcernedRow("assumefail", value)
        End Set
    End Property

    Public Property retryinterval() As Integer
        Get
            Return IsNull(concernedRow("retryinterval").Value, 5)
        End Get
        Set(ByVal value As Integer)
            setConcernedRow("retryinterval", value)
        End Set
    End Property

    Public Property retrytimes() As Integer
        Get
            Return concernedRow("retry").Value
        End Get
        Set(ByVal value As Integer)
            setConcernedRow("retry", value)
        End Set
    End Property

    Public Property checkreporthasdata() As Boolean
        Get
            Return concernedRow("checkblank").Value
        End Get
        Set(ByVal value As Boolean)
            setConcernedRow("checkblank", Convert.ToInt32(value))
        End Set
    End Property

    Public Property reportCacheFile() As String
        Get
            Return concernedRow("cachepath").Value
        End Get
        Set(ByVal value As String)
            setConcernedRow("cachepath", value)
        End Set
    End Property


    Public Property includeAttachment() As Boolean
        Get
            Return IsNull(concernedRow("includeattach").Value, True)
        End Get
        Set(ByVal value As Boolean)
            setConcernedRow("includeattach", Convert.ToInt32(value))
        End Set
    End Property

    Private Sub setConcernedRow(column As String, value As Object)
        clsMarsData.WriteData("UPDATE reportattr SET " & column & " = '" & value & "' WHERE reportid = " & ID)
    End Sub


    Public ReadOnly Property packedReportFormat As String
        Get
            Dim localRs As ADODB.Recordset = clsMarsData.GetData("SELECT outputformat FROM packagedreportattr WHERE reportid = " & ID)

            If localRs IsNot Nothing AndAlso localRs.EOF = False Then
                Dim value As String = localRs(0).Value

                localRs.Close()
                localRs = Nothing

                Return value
            Else
                Return ""
            End If
        End Get
    End Property

    Public Property packedStatus As Boolean
        Get
            Dim localRs As ADODB.Recordset = clsMarsData.GetData("SELECT [status] FROM packagedreportattr WHERE reportid = " & ID)

            If localRs IsNot Nothing AndAlso localRs.EOF = False Then
                Dim value As Integer = IsNull(localRs(0).Value, True)

                localRs.Close()
                localRs = Nothing

                Return value
            Else
                Return True
            End If
        End Get
        Set(value As Boolean)
            clsMarsData.WriteData("UPDATE packagedreportattr SET status = " & Convert.ToInt32(value) & " WHERE reportid =" & ID)
        End Set
    End Property

    Public ReadOnly Property isPackagedReport As Boolean
        Get
            Dim localRs As ADODB.Recordset = clsMarsData.GetData("SELECT packid FROM reportattr WHERE reportid = " & ID)

            If localRs IsNot Nothing AndAlso localRs.EOF = False Then
                Dim value As Integer = IsNull(localRs(0).Value, 0)

                localRs.Close()
                localRs = Nothing

                If value = 0 Then
                    Return False
                Else
                    Return True
                End If
            End If
        End Get
    End Property

    Public ReadOnly Property originalReportFile As String
        Get
            Try
                Return concernedRow("databasepath").Value
            Catch
                Return ""
            End Try
        End Get
    End Property

    Public ReadOnly Property usesFormsAuth As Boolean
        Get
            Try
                Return IsNull(concernedRow("rptformsauth").Value, False)
            Catch ex As Exception
                Return False
            End Try
        End Get
    End Property

    Public ReadOnly Property getDatasourceCredentials As System.Collections.Generic.List(Of reportExecutor.reportDatasource)
        Get
            Dim SQL As String = "SELECT * FROM reportdatasource WHERE reportid = " & m_reportid
            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
            Dim I As Integer = 0
            Dim listOfDatasources As System.Collections.Generic.List(Of reportExecutor.reportDatasource) = New Generic.List(Of reportExecutor.reportDatasource)

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    Dim tmp As reportExecutor.reportDatasource = New reportExecutor.reportDatasource
                    Dim dsName As String = oRs("datasourcename").Value
                    Dim userid As String = oRs("rptuserid").Value
                    Dim password As String = _DecryptDBValue(oRs("rptpassword").Value)

                    tmp.datasourceName = dsName
                    tmp.username = userid
                    tmp.password = password

                    listOfDatasources.Add(tmp)
                    oRs.MoveNext()
                Loop

                oRs.Close()

                oRs = Nothing
            End If

            Return listOfDatasources
        End Get
    End Property

    Public Function configureReportServerConnection()

        Dim rpt

        Dim srsVersion As String = clsMarsReport.m_serverVersion(Me.serverUrl, serverUser, serverPassword, Me.serverFormsAuth)

        If srsVersion >= "2007" Then
            rpt = New rsClients.rsClient2008(serverUrl)
        Else
            rpt = New rsClients.rsClient(serverUrl)
        End If

        If serverUrl.Contains("2006.asmx") Then '//its sharepoint mode
            rpt = New ReportServer_2006.ReportingService2006
        ElseIf serverUrl.Contains("2010.asmx") Then
            rpt = New rsClients.rsClient2010(serverUrl)
        End If

        rpt.Url = serverUrl

        If serverUser = "" Then
            rpt.Credentials = System.Net.CredentialCache.DefaultCredentials

            If serverFormsAuth Then
                Try
                    '//for forms auth
                    Dim netCredentials As System.Net.NetworkCredential = New System.Net.NetworkCredential

                    netCredentials = System.Net.CredentialCache.DefaultCredentials

                    rpt.LogonUser(netCredentials.UserName, netCredentials.Password, Nothing)
                Catch : End Try
            End If
        Else

            Dim domain As String = Nothing
            Dim tmpUser As String = serverUser

            getDomainAndUserFromString(tmpUser, domain)

            Dim cred As System.Net.NetworkCredential = New System.Net.NetworkCredential(tmpUser, serverPassword, domain)
            rpt.Credentials = cred

            If serverFormsAuth Then
                Try
                    rpt.LogonUser(tmpUser, serverPassword, domain)

                    Try

                        If (rpt.CheckAuthorized()) Then
                            Dim type As ReportServer.ItemTypeEnum = rpt.GetItemType("/")

                            ' Console.WriteLine(type)

                        End If

                    Catch ex As Exception
                        ' Console.WriteLine("Exception on call to GetItemType.")
                    End Try

                    rpt.SessionHeaderValue = New ReportServer.SessionHeader()
                Catch : End Try
            End If
        End If


        rpt.Timeout = -1

        Return rpt

    End Function

    Public Shared Function getSSRSVersion(rpt As Object) As Integer
        If TypeOf rpt Is ReportServer.ReportingService Then
            Return 2000
        ElseIf TypeOf rpt Is ReportServer_2005.ReportingService2005 Then
            Return 2005
        ElseIf TypeOf rpt Is ReportServer_2006.ReportingService2006 Then
            Return 2006
        ElseIf TypeOf rpt Is ReportServer_2010.ReportingService2010 Then
            Return 2010
        End If
    End Function

    

    Public Shared Function createDataSourceCredentials(reportid As Integer, version As Integer) ' ReportServer.DataSourceCredentials()
        Dim query As String = String.Format("SELECT * FROM ReportDatasource WHERE reportid = {0} AND rptuserid <> 'default'", reportid)
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(query)
        '   Dim datasources As Object() ' ReportServer.DataSourceCredentials() = Nothing
        Dim tmp As ArrayList = New ArrayList

        Dim count As Integer = 0

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim dsname, username, password As String

                dsname = oRs("DatasourceName").Value
                username = oRs("RptUserID").Value
                password = _DecryptDBValue(oRs("RptPassword").Value)

                '  ReDim Preserve datasources(count)
                Dim ds

                Select Case version
                    Case 2000
                        ds = New ReportServer.DataSourceCredentials
                    Case 2005
                        ds = New ReportServer_2005.DataSourceCredentials
                    Case 2006
                        ds = New ReportServer_2006.DataSourceCredentials
                    Case 2010
                        ds = New ReportServer_2010.DataSourceCredentials
                End Select

                ds.DataSourceName = dsname
                ds.UserName = username
                ds.Password = password

                tmp.Add(ds)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        Select Case version
            Case 2000
                Dim datasources As ReportServer.DataSourceCredentials()
                Dim I As Integer
                Dim ds

                For Each ds In tmp
                    ReDim Preserve datasources(I)

                    datasources(I) = ds

                    I += 1
                Next

                Return datasources
            Case 2005
                Dim datasources As ReportServer_2005.DataSourceCredentials()
                Dim I As Integer
                Dim ds

                For Each ds In tmp
                    ReDim Preserve datasources(I)

                    datasources(I) = ds

                    I += 1
                Next

                Return datasources
            Case 2006
                Dim datasources As ReportServer_2006.DataSourceCredentials()
                Dim I As Integer
                Dim ds

                For Each ds In tmp
                    ReDim Preserve datasources(I)

                    datasources(I) = ds

                    I += 1
                Next

                Return datasources
            Case 2010
                Dim datasources As ReportServer_2010.DataSourceCredentials()
                Dim I As Integer
                Dim ds

                For Each ds In tmp
                    ReDim Preserve datasources(I)

                    datasources(I) = ds

                    I += 1
                Next

                Return datasources
        End Select


        Return Nothing
    End Function

End Class
