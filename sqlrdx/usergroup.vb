﻿Public Class usergroup
    Dim oRs As ADODB.Recordset
    Dim m_groupID As Integer
    Dim m_groupName As String
    Public Enum e_destinationAllowance
        All = 0
        TypesOnly = 1
        DefaultOnly = 2
    End Enum
    Sub New(ByVal groupName As String)
        m_groupName = groupName


        oRs = clsMarsData.GetData("SELECT * FROM groupattr WHERE groupname ='" & SQLPrepare(groupName) & "'")

        If oRs Is Nothing Then
            Throw New Exception("Could not find group " & groupName)
        ElseIf oRs.EOF = True Then
            m_groupID = 0
        Else
            m_groupID = oRs("groupid").Value
        End If
    End Sub

    Public ReadOnly Property GroupID As Integer
        Get
            Return m_groupID
        End Get
    End Property
    Sub New()

    End Sub

    Sub New(ByVal groupID As Integer)

        m_groupID = groupID

        oRs = clsMarsData.GetData("SELECT * FROM groupattr WHERE groupid = " & groupID)

        If oRs Is Nothing Then
            Throw New Exception("Could not find group with ID " & groupID)
        ElseIf oRs.EOF = True Then
            Throw New Exception("Could not find group with ID " & groupID)
        End If
    End Sub

    Sub dispose()
        Try
            oRs.Close()
            oRs = Nothing
        Catch : End Try
    End Sub
    Public Property groupName() As String
        Get
            If oRs.EOF = True Then Return m_groupName Else Return oRs("groupname").Value
        End Get
        Set(ByVal value As String)
            '// oRs("groupname").Value = value
        End Set
    End Property

    Public Property groupDescription() As String
        Get
            If oRs.EOF = True Then Return "" Else Return IsNull(oRs("groupdesc").Value)
        End Get
        Set(ByVal value As String)
            ' oRs("groupdesc").Value = value
        End Set
    End Property

    Public Function assignADestination(ByVal destinationID As Integer) As Boolean
        '//check to see if the destination is already assigned
        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM groupDefaultdestinationsAttr WHERE groupid = " & m_groupID & " AND destinationid = " & destinationID)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then Return True

        Dim cols As String = "attrid, groupid, destinationid"
        Dim vals As String = clsMarsData.CreateDataID("groupDefaultdestinationsAttr", "attrid") & "," & _
            m_groupID & "," & _
            destinationID

        clsMarsData.DataItem.InsertData("groupDefaultdestinationsAttr", cols, vals, False)

        Return True
    End Function

    Public Function revokeADestination(ByVal destinationID As Integer) As Boolean
        clsMarsData.WriteData("DELETE FROM groupDefaultdestinationsAttr WHERE groupid =" & m_groupID & " AND destinationid = " & destinationID)
    End Function
    Public Property assignedDestinations() As ArrayList
        Get
            '//we need to find all the groups that this user is a member of basically

            Dim results As ArrayList = New ArrayList

            Dim lRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM groupDefaultdestinationsAttr WHERE groupID =" & m_groupID)

            If lRs IsNot Nothing Then
                Do While lRs.EOF = False
                    results.Add(lRs("destinationid").Value)
                    lRs.MoveNext()
                Loop
                lRs.Close()
            End If

            Return results
        End Get
        Set(ByVal value As ArrayList)
            clsMarsData.WriteData("DELETE FROM groupDefaultdestinationsAttr WHERE groupID =" & m_groupID)

            Dim cols As String = "attrid, groupid, destinationid"
            Dim vals As String = ""

            For Each dest As String In value
                vals = clsMarsData.CreateDataID("groupDefaultdestinationsAttr", "attrid") & "," & _
                m_groupID & "," & _
                dest

                clsMarsData.DataItem.InsertData("groupDefaultdestinationsAttr", cols, vals, False)
            Next
        End Set
    End Property

    Public ReadOnly Property isFolderAccessRestricted As Boolean
        Get
            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT folderaccesstype FROM groupAttr WHERE groupid =" & m_groupID)
            Dim value As Integer = 0

            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                value = IsNull(oRs("folderaccesstype").Value, 0)   '//0 means no restriction, 1 means restricted
            End If

            oRs.Close()
            oRs = Nothing

            Return Convert.ToInt32(value)
        End Get
    End Property

    Public ReadOnly Property getUserAllowedDestinations() As ArrayList
        Get
            '//we need to find all the groups that this user is a member of basically
            Dim logFile As String = "usegroups.log"
            Dim results As ArrayList = New ArrayList
            Dim allGroups As String = m_groupID & ","
            Dim unifiedLogin As Boolean = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("unifiedlogin", 0))

            If unifiedLogin Then
                For Each i As Integer In getGroupsThatUserIsMemberOf()
                    allGroups = allGroups & i & ","
                Next
            End If

            allGroups = allGroups.Remove(allGroups.Length - 1, 1)

            clsMarsDebug.writeToDebug(logFile, allGroups, False)

            Dim lRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM groupDefaultdestinationsAttr WHERE groupID IN (" & allGroups & ")")

            If lRs IsNot Nothing Then
                Do While lRs.EOF = False
                    results.Add(lRs("destinationid").Value)
                    lRs.MoveNext()
                Loop
                lRs.Close()
            End If

            Return results
        End Get
    End Property
    Private Function getGroupsThatUserIsMemberOf() As System.Collections.Generic.List(Of Integer)
        Dim oRs As ADODB.Recordset = New ADODB.Recordset
        Dim userTest As clsMarsUsers = New clsMarsUsers
        Dim memberGroups As System.Collections.Generic.List(Of Integer) = New System.Collections.Generic.List(Of Integer)

        oRs = clsMarsData.GetData("SELECT * FROM domainattr WHERE domainname LIKE '%.*%'")

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim gname, dname, uname As String

                gname = oRs("domainname").Value
                gname = gname.Remove(gname.Length - 2, 2) '//remove the .*
                gname = gname.Split("\")(1) '//get the name without the domain

                If gUser.Contains("\") Then
                    dname = gUser.Split("\")(0)
                    uname = gUser.Split("\")(1)
                Else
                    dname = ""
                    uname = gUser
                End If


                If userTest.Check_If_Member_Of_AD_Group(Environment.UserName, gname, Environment.UserDomainName, "", "") = True Then
                    Dim ogroup As ADODB.Recordset = clsMarsData.GetData("SELECT groupid FROM groupattr WHERE groupname = '" & SQLPrepare(gname) & "'")

                    If ogroup IsNot Nothing AndAlso ogroup.EOF = False Then
                        memberGroups.Add(ogroup(0).Value)

                        ogroup.Close()
                    End If

                    ogroup = Nothing
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()

            oRs = Nothing
        End If

        Return memberGroups
    End Function

    Public Property userAllowedFolders As System.Collections.Generic.List(Of Integer)
        Get
            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT folderid FROM groupFolderAttr WHERE groupID =" & m_groupID & " AND accessType = '" & SQLPrepare(_EncryptDBValue("allowed")) & "'")

            Dim allowedFolders As System.Collections.Generic.List(Of Integer) = New System.Collections.Generic.List(Of Integer)

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    Dim folderid As Integer = oRs("folderid").Value

                    allowedFolders.Add(folderid)

                    oRs.MoveNext()
                Loop

                oRs.Close()
                oRs = Nothing
            End If


            Return allowedFolders
        End Get
        Set(value As System.Collections.Generic.List(Of Integer))

            clsMarsData.WriteData("DELETE FROM groupfolderattr WHERE groupid =" & m_groupID)

            Dim allowed As String = _EncryptDBValue("allowed")
            Dim cols As String = "accessid,	groupid, folderid, accesstype"
            Dim accessid As String
            Dim vals As String

            For Each folderid As Integer In value
                accessid = Guid.NewGuid.ToString

                vals = "'" & accessid & "'," & _
                    m_groupID & "," & _
                    folderid & "," & _
                    "'" & SQLPrepare(allowed) & "'"

                clsMarsData.DataItem.InsertData("groupfolderattr", cols, vals, False)
            Next
        End Set
    End Property
    Public ReadOnly Property getUserAllowedDestinationTypes() As ArrayList
        Get
            Dim results As ArrayList = New ArrayList
            Dim allGroups As String = m_groupID & ","
            Dim unifiedLogin As Boolean = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("unifiedlogin", 0))

            If unifiedLogin Then
                For Each i As Integer In getGroupsThatUserIsMemberOf()
                    allGroups = allGroups & i & ","
                Next
            End If

            allGroups = allGroups.Remove(allGroups.Length - 1, 1)
            Dim lRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM groupdestinationTypesAttr WHERE groupID IN (" & allGroups & ")")

            If lRs IsNot Nothing Then
                Do While lRs.EOF = False
                    results.Add(lRs("destinationtype").Value)
                    lRs.MoveNext()
                Loop
                lRs.Close()
            End If

            Return results
        End Get
    End Property
    Public Property allowedDestinatoionTypes() As ArrayList
        Get
            Dim results As ArrayList = New ArrayList
            Dim lRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM groupdestinationTypesAttr WHERE groupID =" & m_groupID)

            If lRs IsNot Nothing Then
                Do While lRs.EOF = False
                    results.Add(lRs("destinationtype").Value)
                    lRs.MoveNext()
                Loop
                lRs.Close()
            End If

            Return results
        End Get
        Set(ByVal value As ArrayList)
            clsMarsData.WriteData("DELETE FROM groupdestinationTypesAttr WHERE groupID =" & m_groupID)

            Dim cols As String = "attrid, groupid, destinationType"
            Dim vals As String = ""

            For Each dest As String In value
                vals = clsMarsData.CreateDataID("groupdestinationTypesAttr", "attrid") & "," & _
                m_groupID & "," & _
                "'" & dest & "'"

                clsMarsData.DataItem.InsertData("groupdestinationTypesAttr", cols, vals, False)
            Next
        End Set
    End Property

    Public ReadOnly Property getUserAllowedTaskTypes() As ArrayList
        Get
            Dim results As ArrayList = New ArrayList
            Dim allGroups As String = m_groupID & ","
            Dim unifiedLogin As Boolean = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("unifiedlogin", 0))

            If unifiedLogin Then
                For Each i As Integer In getGroupsThatUserIsMemberOf()
                    allGroups = allGroups & i & ","
                Next
            End If

            allGroups = allGroups.Remove(allGroups.Length - 1, 1)

            Dim lRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM groupTaskTypesAttr WHERE groupID IN (" & allGroups & ")")

            If lRs IsNot Nothing Then
                Do While lRs.EOF = False
                    results.Add(lRs("tasktype").Value)
                    lRs.MoveNext()
                Loop
                lRs.Close()
            End If

            Return results
        End Get
    End Property
    Public Property allowedTaskTypes() As ArrayList
        Get
            Dim results As ArrayList = New ArrayList
            Dim lRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM groupTaskTypesAttr WHERE groupID =" & m_groupID)

            If lRs IsNot Nothing Then
                Do While lRs.EOF = False
                    results.Add(lRs("tasktype").Value)
                    lRs.MoveNext()
                Loop
                lRs.Close()
            End If

            Return results
        End Get
        Set(ByVal value As ArrayList)
            clsMarsData.WriteData("DELETE FROM groupTaskTypesAttr WHERE groupID =" & m_groupID)

            Dim cols As String = "attrid, groupid, tasktype"
            Dim vals As String = ""

            For Each task As String In value
                vals = clsMarsData.CreateDataID("groupTaskTypesAttr", "attrid") & "," & _
                m_groupID & "," & _
                "'" & task & "'"

                clsMarsData.DataItem.InsertData("groupTaskTypesAttr", cols, vals, False)
            Next
        End Set
    End Property

    Public Property destinationAllowance() As e_destinationAllowance
        Get
            If oRs.EOF = True Then Return e_destinationAllowance.All

            Select Case IsNull(oRs("destinationAccessType").Value, "All")
                Case "All"
                    Return e_destinationAllowance.All
                Case "TypesOnly"
                    Return e_destinationAllowance.TypesOnly
                Case "DefaultOnly"
                    Return e_destinationAllowance.DefaultOnly
            End Select
        End Get
        Set(ByVal value As e_destinationAllowance)
            Dim val As String = "All"

            Select Case value
                Case e_destinationAllowance.All
                    val = "All"
                Case e_destinationAllowance.DefaultOnly
                    val = "DefaultOnly"
                Case e_destinationAllowance.TypesOnly
                    val = "TypesOnly"
            End Select

            clsMarsData.WriteData("UPDATE groupattr SET destinationAccessType = '" & val & "' WHERE groupid =" & m_groupID)
        End Set
    End Property

    Public Sub grantAccessToFolder(folderID As Integer)
        Dim allowed As String = _EncryptDBValue("allowed")
        Dim cols As String = "accessid,	groupid, folderid, accesstype"
        Dim vals As String

        vals = "'" & Guid.NewGuid.ToString & "'," & _
            m_groupID & "," & _
            folderID & "," & _
            "'" & SQLPrepare(allowed) & "'"

        clsMarsData.DataItem.InsertData("groupFolderAttr", cols, vals, True)
    End Sub

    Public ReadOnly Property memberCount As Integer
        Get
            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT COUNT(*) AS memberCount FROM crdusers WHERE userrole = '" & SQLPrepare(groupName) & "'")
            Dim count As Integer = 0

            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                count = oRs(0).Value

                oRs.Close()
                oRs = Nothing
            End If

            oRs = clsMarsData.GetData("SELECT COUNT(*) AS memberCount FROM domainattr WHERE userrole = '" & SQLPrepare(groupName) & "'")

            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                count = count + oRs(0).Value

                oRs.Close()
                oRs = Nothing
            End If

            Return count
        End Get
    End Property
    ''' <summary>
    ''' Must be a user-defined group
    ''' </summary>
    ''' <param name="permission"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function IsGroupAllowedAccessTo(permission As String) As Boolean
        Dim query As String = String.Format("SELECT permvalue FROM grouppermissions WHERE permdesc ='{0}' AND groupid = {1}", SQLPrepare(_EncryptDBValue(permission)), Me.m_groupID)
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(query)

        If oRs Is Nothing Then
            Return False
        Else
            If oRs.EOF = True Then
                Return False
            Else
                Dim perValue As String = oRs(0).Value

                oRs.Close()

                If perValue = 1458 Then
                    Return True
                Else
                    Return False
                End If
            End If
        End If
    End Function
End Class
