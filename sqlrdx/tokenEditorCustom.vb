﻿Public Class tokenEditorCustom
    Inherits DevComponents.DotNetBar.Controls.TokenEditor

    Public Property _separator As String

    Sub New(separator As String)
        _separator = separator

        Me.Separators.Clear()

        Me.Separators.Add(_separator)
    End Sub
    Public Sub addTokens(tokenString As String)
        For Each s As String In tokenString.Split(_separator)
            Me.Tokens.Add(New DevComponents.DotNetBar.Controls.EditToken(s.Trim, s.Trim))
        Next
    End Sub

    Public Property SelectedTokensString() As String
        Get
            Dim tags As String = ""

            For Each tk As DevComponents.DotNetBar.Controls.EditToken In Me.SelectedTokens
                tags &= tk.Value.Trim & ","
            Next

            If tags <> "" Then tags = tags.Remove(tags.Length - 1, 1)

            Return tags
        End Get
        Set(value As String)
            For Each s As String In value.Split(_separator)
                Me.SelectedTokens.Add(New DevComponents.DotNetBar.Controls.EditToken(s.Trim, s.Trim))
            Next
        End Set
    End Property
        

End Class
