﻿Public Class ucScheduleCount
    Public Property numberOfReports As Integer
        Get
            Return pgCount.Value
        End Get
        Set(value As Integer)
            pgCount.Value = value
            LabelX1.Text = "Number of Schedules/Reports: " & value

            If value < 25 Then
                pgCount.ColorTable = DevComponents.DotNetBar.eProgressBarItemColor.Normal
            ElseIf value < 40 Then
                pgCount.ColorTable = DevComponents.DotNetBar.eProgressBarItemColor.Paused
            Else
                pgCount.ColorTable = DevComponents.DotNetBar.eProgressBarItemColor.Error
            End If
        End Set
    End Property
End Class
