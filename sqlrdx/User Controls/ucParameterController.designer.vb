﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucParameterController
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucParameterController))
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lsvValues = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lsvParameters = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.fizzle = New System.Windows.Forms.FlowLayoutPanel()
        Me.InfoBox = New DevComponents.DotNetBar.Controls.WarningBox()
        Me.SuspendLayout()
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(260, 161)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(89, 13)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "Confirmed Values"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(257, 10)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 13)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Value Selector"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 13)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Parameters"
        '
        'lsvValues
        '
        Me.lsvValues.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.lsvValues.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvValues.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader4, Me.ColumnHeader5})
        Me.lsvValues.DisabledBackColor = System.Drawing.Color.Empty
        Me.lsvValues.Location = New System.Drawing.Point(260, 177)
        Me.lsvValues.Name = "lsvValues"
        Me.lsvValues.Size = New System.Drawing.Size(369, 174)
        Me.lsvValues.TabIndex = 10
        Me.lsvValues.UseCompatibleStateImageBehavior = False
        Me.lsvValues.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Label"
        Me.ColumnHeader4.Width = 99
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Value"
        Me.ColumnHeader5.Width = 189
        '
        'lsvParameters
        '
        '
        '
        '
        Me.lsvParameters.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvParameters.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lsvParameters.DisabledBackColor = System.Drawing.Color.Empty
        Me.lsvParameters.FullRowSelect = True
        Me.lsvParameters.HideSelection = False
        Me.lsvParameters.Location = New System.Drawing.Point(6, 26)
        Me.lsvParameters.Name = "lsvParameters"
        Me.lsvParameters.Size = New System.Drawing.Size(248, 325)
        Me.lsvParameters.TabIndex = 9
        Me.lsvParameters.UseCompatibleStateImageBehavior = False
        Me.lsvParameters.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Name"
        Me.ColumnHeader1.Width = 86
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Prompt"
        Me.ColumnHeader2.Width = 146
        '
        'fizzle
        '
        Me.fizzle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fizzle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.fizzle.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.fizzle.Location = New System.Drawing.Point(260, 26)
        Me.fizzle.Name = "fizzle"
        Me.fizzle.Size = New System.Drawing.Size(369, 132)
        Me.fizzle.TabIndex = 8
        '
        'InfoBox
        '
        Me.InfoBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.InfoBox.CloseButtonVisible = False
        Me.InfoBox.ColorScheme = DevComponents.DotNetBar.Controls.eWarningBoxColorScheme.Yellow
        Me.InfoBox.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.InfoBox.Image = CType(resources.GetObject("InfoBox.Image"), System.Drawing.Image)
        Me.InfoBox.Location = New System.Drawing.Point(0, 357)
        Me.InfoBox.Name = "InfoBox"
        Me.InfoBox.OptionsButtonVisible = False
        Me.InfoBox.Size = New System.Drawing.Size(638, 33)
        Me.InfoBox.TabIndex = 14
        Me.InfoBox.Text = "<b>This report does not have any parameters</b>"
        Me.InfoBox.Visible = False
        '
        'ucParameterController
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.InfoBox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lsvValues)
        Me.Controls.Add(Me.lsvParameters)
        Me.Controls.Add(Me.fizzle)
        Me.Name = "ucParameterController"
        Me.Size = New System.Drawing.Size(638, 390)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lsvValues As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lsvParameters As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents fizzle As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents InfoBox As DevComponents.DotNetBar.Controls.WarningBox

End Class
