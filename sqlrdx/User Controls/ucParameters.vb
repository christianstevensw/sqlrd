﻿Imports Microsoft.Reporting.WinForms
Imports System.Collections.Generic
Public Class ucParameters

    Dim parameterType As String
    Dim m_rv As Microsoft.Reporting.WinForms.ReportViewer

    Private parameterParent As ucParametersList
    Private maxHeight As Integer = 333 '  285
    Private minHeight As Integer = 148 ' 78

    Private rdlFilePath As String
    Public Property m_rdlFilePath() As String
        Get
            Return rdlFilePath
        End Get
        Set(ByVal value As String)
            rdlFilePath = value
        End Set
    End Property

    Public Shared ReadOnly Property AdminCanEditDefaultParameters() As Boolean
        Get
            If gRole = "Administrator" Then
                Return Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("AllowAdminsToSetDefaultParameters", 0))
            Else
                '//get the groupid
                Dim grp As usergroup = New usergroup(gRole)

                Return grp.IsGroupAllowedAccessTo("Over-ride Default Parameters")
            End If
        End Get
    End Property

    Public Property m_parameterParent() As ucParametersList
        Get
            Return parameterParent
        End Get
        Set(ByVal value As ucParametersList)
            parameterParent = value
        End Set
    End Property


    Private allowMultipleValues As Boolean
    Public Property m_allowMultipleValues() As Boolean
        Get
            Return allowMultipleValues
        End Get
        Set(ByVal value As Boolean)
            allowMultipleValues = value

            If value = True Then
                btnAddValue.Visible = True
            End If
        End Set
    End Property


    Public Property m_parameterType() As String
        Get
            Return parameterType
        End Get
        Set(ByVal value As String)
            parameterType = value
        End Set
    End Property

    Private myparentPanel As ucParametersList
    Public WriteOnly Property m_parentForm() As ucParametersList
        Set(ByVal value As ucParametersList)
            myparentPanel = value
        End Set
    End Property
    ''' <summary>
    ''' a pipe delimited string of available values
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property m_availableValues As String
        Get
            Dim items As String = ""

            For Each s As String In txtValue.Items
                items &= s & "|"
            Next

            Return items
        End Get
    End Property

    Public ReadOnly Property m_parameterValueLabel As String
        Get

            If allowMultipleValues = False Then

                Return txtValue.Text

            Else
                Dim values As String = ""

                If chkIgnore.Checked Then
                    Return "[SQL-RDDefault]"
                ElseIf chkNull.Checked Then
                    Return "[SQL-RDNull]"
                ElseIf chkAllValues.Checked Then
                    Return "[SelectAll]"
                Else
                    For Each it As ListViewItem In lsvValues.Items
                        If it.Checked And it.Text <> "[SelectAll]" Then
                            values &= it.Text & "|"
                        End If
                    Next

                    If values.Length > 1 Then values = values.Remove(values.Length - 1, 1)
                End If

                Return values
            End If
        End Get

    End Property

    Public ReadOnly Property m_rawParameterValue() As String
        Get
            If allowMultipleValues = False Then
                Return txtValue.Text
            Else
                Dim values As String = ""

                If chkIgnore.Checked Then
                    Return "[SQL-RDDefault]"
                ElseIf chkNull.Checked Then
                    Return "[SQL-RDNull]"
                ElseIf chkAllValues.Checked Then
                    Return "[SelectAll]"
                Else
                    For Each it As ListViewItem In lsvValues.Items
                        If it.Checked And it.Text <> "[SelectAll]" Then
                            values &= it.Tag & "|"
                        End If
                    Next

                    If values.Length > 1 Then values = values.Remove(values.Length - 1, 1)

                    Return values
                End If
            End If
        End Get
        'Set(ByVal value As String)
        '    '//reset
        '    myparentPanel.m_controlFiresEvents = False

        '    For Each it As ListViewItem In lsvValues.Items
        '        it.Checked = False
        '    Next

        '    If allowMultipleValues = False Then
        '        txtValue.Text = value
        '    Else
        '        If value.Contains("[SQL-RDNull]") Then
        '            chkNull.Checked = True
        '            lsvValues.Items.Clear()
        '            Dim it As ListViewItem = lsvValues.Items.Add("[SQL-RDNull]")
        '            it.Checked = True
        '            lsvValues.Enabled = False
        '        ElseIf value.Contains("[SQL-RDDefault]") Then
        '            chkIgnore.Checked = True
        '            lsvValues.Items.Clear()
        '            Dim it As ListViewItem = lsvValues.Items.Add("[SQL-RDDefault]")
        '            it.Checked = True
        '            lsvValues.Enabled = False
        '        ElseIf value.Contains("[SelectAll]") Then
        '            chkAllValues.Checked = True
        '            lsvValues.Items.Clear()
        '            Dim it As ListViewItem = lsvValues.Items.Add("[SelectAll]")
        '            it.Checked = True
        '            lsvValues.Enabled = False
        '        Else

        '            For Each de As DictionaryEntry In value
        '                If de.Key <> "" Then
        '                    Dim it As ListViewItem = lsvValues.Items.Add(de.Key) ' myparentPanel.getserverParameterLabel(m_parameterName, m_parameterIndex, s, m_staticValuesList))
        '                    it.Tag = de.Value
        '                    it.Checked = True
        '                End If
        '            Next
        '        End If

        '    End If
        '    myparentPanel.m_controlFiresEvents = True
        'End Set
    End Property

    Public Property m_parameterValue() As Hashtable
        Get
            If allowMultipleValues = False Then
                m_parameterValue = New Hashtable
                m_parameterValue.Add(txtValue.Text, myparentPanel.getserverParameterValue(Me.m_parameterName, txtValue.Text))
            Else
                Dim values As String = ""
                m_parameterValue = New Hashtable

                If chkIgnore.Checked Then
                    m_parameterValue.Add("[SQL-RDDefault]", "[SQL-RDDefault]")
                ElseIf chkNull.Checked Then
                    m_parameterValue.Add("[SQL-RDNull]", "[SQL-RDNull]")
                ElseIf chkAllValues.Checked Then
                    m_parameterValue.Add("[SelectAll]", "[SelectAll]")
                Else
                    For Each it As ListViewItem In lsvValues.Items
                        If it.Checked And it.Text <> "[SelectAll]" Then
                            m_parameterValue.Add(it.Text, it.Tag)
                        End If
                    Next
                End If
            End If
        End Get
        Set(ByVal value As Hashtable)
            '//reset
            myparentPanel.m_controlFiresEvents = False

            For Each it As ListViewItem In lsvValues.Items
                it.Checked = False
            Next

            Dim actualValue As String

            For Each de As DictionaryEntry In value
                actualValue = de.Key
            Next

            If allowMultipleValues = False Then
                txtValue.Text = actualValue
            Else
                If actualValue.Contains("[SQL-RDNull]") Then
                    chkNull.Checked = True
                    lsvValues.Items.Clear()
                    Dim it As ListViewItem = lsvValues.Items.Add("[SQL-RDNull]")
                    it.Checked = True
                    lsvValues.Enabled = False
                ElseIf actualValue.Contains("[SQL-RDDefault]") Then
                    chkIgnore.Checked = True
                    lsvValues.Items.Clear()
                    Dim it As ListViewItem = lsvValues.Items.Add("[SQL-RDDefault]")
                    it.Checked = True
                    lsvValues.Enabled = False
                ElseIf actualValue.Contains("[SelectAll]") Then
                    chkAllValues.Checked = True
                    lsvValues.Items.Clear()
                    Dim it As ListViewItem = lsvValues.Items.Add("[SelectAll]")
                    it.Checked = True
                    lsvValues.Enabled = False
                Else
                    For Each de As DictionaryEntry In value
                        Dim found As Boolean = False

                        For Each it As ListViewItem In lsvValues.Items
                            If it.Text = de.Key And it.Tag = de.Value Then
                                found = True
                                it.Checked = True
                                Exit For
                            End If
                        Next


                        If de.Key <> "" And Not found Then
                            Dim it As ListViewItem = lsvValues.Items.Add(de.Key) ' myparentPanel.getserverParameterLabel(m_parameterName, m_parameterIndex, s, m_staticValuesList))
                            it.Tag = de.Value
                            it.Checked = True
                        End If
                    Next
                End If
            End If
            myparentPanel.m_controlFiresEvents = True
        End Set
    End Property

    Public Property m_staticValuesList As List(Of KeyValuePair(Of String, String))
   

    Private parameterIndex As Integer
    Public Property m_parameterIndex() As Integer
        Get
            Return parameterIndex
        End Get
        Set(ByVal value As Integer)
            parameterIndex = value

            lblIndex.Text = value
        End Set
    End Property

    Private parameterName As String
    Public Property m_parameterName() As String
        Get
            Return parameterName
        End Get
        Set(ByVal value As String)
            parameterName = value
        End Set
    End Property

    Public WriteOnly Property m_parameterDefaults() As String()
        Set(ByVal value As String())
            If m_allowMultipleValues Then
                For Each s As String In value
                    Dim it As ListViewItem = lsvValues.Items.Add(s)
                    it.Tag = s
                Next
            Else
                For Each s As String In value
                    txtValue.Items.Add(s)
                Next

                If txtValue.Items.Count > 0 Then txtValue.DropDownStyle = ComboBoxStyle.DropDown
            End If
        End Set
    End Property


    Private parameterLabel As String
    Public Property m_ParameterPrompt() As String
        Get
            Return grpParameter.TitleText
        End Get
        Set(ByVal value As String)
            grpParameter.TitleText = value
        End Set
    End Property

    Private ReadOnly Property userVisibleParameterValues() As String
        Get
            Dim parameterActualValue As String = ""

            If m_allowMultipleValues Then
                For Each it As ListViewItem In lsvValues.CheckedItems
                    parameterActualValue &= Chr(34) & it.Text & Chr(34) & ", "
                Next

                If parameterActualValue <> "" Then
                    parameterActualValue = parameterActualValue.Remove(parameterActualValue.Length - 2, 2)
                End If
            Else
                parameterActualValue = txtValue.Text
            End If


            Return parameterActualValue
        End Get
       
    End Property

    'Public ReadOnly Property m_parameterValueLabel() As String
    '    Get
    '        Dim parameterActualValue As String = ""

    '        If m_allowMultipleValues Then
    '            For Each it As ListViewItem In lsvValues.CheckedItems
    '                parameterActualValue &= it.Tag & "|"
    '            Next
    '        Else
    '            parameterActualValue = txtValue.Text
    '        End If


    '        Return parameterActualValue
    '    End Get
    '    'Set(ByVal value As String)
    '    '    '//reset
    '    '    For Each it As ListViewItem In lsvValues.Items
    '    '        it.Checked = False
    '    '    Next

    '    '    If m_allowMultipleValues = False Then
    '    '        txtValue.Text = value
    '    '    Else
    '    '        For Each parameterValue As String In value.Split("|")
    '    '            Dim found As Boolean = False

    '    '            For Each it As ListViewItem In lsvValues.Items
    '    '                If it.Text.ToLower = parameterValue.ToLower Then
    '    '                    it.Checked = True
    '    '                    found = True
    '    '                    Exit For
    '    '                End If


    '    '                If found = False And parameterValue <> "" Then
    '    '                    Dim newItem As ListViewItem = New ListViewItem(parameterValue)
    '    '                    newItem.Checked = True
    '    '                    lsvValues.Items.Add(newItem)
    '    '                End If
    '    '            Next
    '    '        Next
    '    '    End If
    '    'End Set
    'End Property

    Public Property m_ParameterNull As Boolean
        Get
            Return chkNull.Checked
        End Get
        Set(value As Boolean)
            chkNull.Checked = value
        End Set
    End Property

    Private hasDefaultValues As Boolean
    Public Property m_hasDefaultValues() As Boolean
        Get
            Return hasDefaultValues
        End Get
        Set(ByVal value As Boolean)
            hasDefaultValues = value

            If m_allowMultipleValues = True Then '//if multiple values are expected
                If value = True Then
                    btnAddValue.Visible = True
                    txtValue.Enabled = True
                    lsvValues.Enabled = True
                    '   txtValue.Width = 421
                Else
                    btnAddValue.Visible = True
                    txtValue.Enabled = True
                    lsvValues.Enabled = True
                    'txtValue.Width = 385
                End If
            Else
                txtValue.Enabled = True

                If value = True Then
                    txtValue.DropDownStyle = ComboBoxStyle.DropDown
                    txtValue.Enabled = True
                Else
                    txtValue.DropDownStyle = ComboBoxStyle.DropDown
                    txtValue.Enabled = True
                End If
            End If
        End Set
    End Property

    Sub New(ByVal parameterName As String, ByVal parameterType As String, ByVal allowmultiValue As Boolean, ByVal parameterValue As String, ByVal parent As ucParametersList, parameterLabel As String)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_parentForm = parent
        m_parameterName = parameterName
        m_parameterType = parameterType
        m_allowMultipleValues = allowmultiValue

        If parameterLabel <> "" Then
            m_parameterValue = newHashTable(parameterLabel, parameterValue)
        End If

        If m_allowMultipleValues = True Then

            'grpParameter.Height = 268
            Me.Height = maxHeight
        Else

            txtValue.Focus()

            txtValue2.Visible = False
            chkLBound.Visible = False
            chkUBound.Visible = False
            chkLBoundInc.Visible = False
            chkUBoundInc.Visible = False
            chkNull.Visible = True
            '   Label1.Text = "Parameter Value"

            Dim val As String = parameterValue

            '   grpParameter.Height = 75
            Me.Height = 150
        End If



    End Sub

    
    Private Sub ucParameters_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        setupForDragAndDrop(txtValue)
        setupForDragAndDrop(txtValue2)
    End Sub

    Private Sub txtValue_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtValue.DropDown '//ok we want to get the current selections for this parameter
        txtValue.Items.Clear()

        doFakeProgress()

        Try
            If m_parameterType = "Boolean" Then '//if its boolean the values are True/False
                txtValue.Items.Clear()

                txtValue.Items.Add("True")
                txtValue.Items.Add("False")
            Else
                If m_allowMultipleValues = False Then

                    Dim f As String = "parameterslisting.debug"

                    clsMarsDebug.writeToDebug(f, "reading values from the database", False)

                    Dim hs As List(Of KeyValuePair(Of String, String)) = myparentPanel.getParameterValidValues(m_parameterIndex, m_parameterName)

                    If hs IsNot Nothing AndAlso hs.Count > 0 Then
                        For Each pair As KeyValuePair(Of String, String) In hs
                            txtValue.Items.Add(pair.Key)
                        Next

                        'For Each de As DictionaryEntry In hs
                        '    txtValue.Items.Add(de.Key)
                        'Next
                    Else

                        clsMarsDebug.writeToDebug(f, "no values found from server - reading rdl file", True)


                        If m_rdlFilePath = "" Or IO.File.Exists(m_rdlFilePath) = False Then m_rdlFilePath = myparentPanel.createRDLFile(myparentPanel.m_formsAuth)

                        clsMarsDebug.writeToDebug(f, "getting default values from the rdl file", True)

                        Try
                            hs = myparentPanel.getParameterValuesFromRDL(m_rdlFilePath, m_parameterName, m_parameterType, m_allowMultipleValues)
                        Catch ex As Exception

                        End Try

                        If hs IsNot Nothing AndAlso hs.Count > 0 Then
                            clsMarsDebug.writeToDebug(f, "got values - populating", True)

                            For Each pair As KeyValuePair(Of String, String) In hs
                                txtValue.Items.Add(pair.Key)
                            Next

                            'For Each de As DictionaryEntry In hs
                            '    txtValue.Items.Add(de.Key)
                            'Next
                        Else
                            clsMarsDebug.writeToDebug(f, "no values found???", True)
                        End If
                    End If
                End If
            End If
        Catch : End Try

        wait.Value = 100
        Application.DoEvents()
        wait.Visible = False
    End Sub

    Private Sub txtValue_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtValue.KeyUp
        If e.KeyCode = Keys.Enter Then
            If m_allowMultipleValues And txtValue.Text <> "" Then
                For Each itx As ListViewItem In lsvValues.Items
                    If itx.Text.ToLower = txtValue.Text Then
                        Return
                    End If
                Next

                Dim it As ListViewItem = New ListViewItem(txtValue.Text)
                lsvValues.Items.Add(it)
                it.Checked = True
                it.Selected = True
                it.EnsureVisible()
                txtValue.Focus()
                txtValue.Text = ""
            End If
        End If
    End Sub

    Private Sub txtValue_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtValue.SelectedIndexChanged
        If m_parameterIndex = myparentPanel.m_ParameterControlsCollection.Count - 1 Then
            Return
        Else
            lsvValues_ItemChecked1(Nothing, Nothing)
        End If

        Dim tmp As String = txtValue.Text
        lsvValues_ItemChecked(Nothing, Nothing)
        txtValue.Text = tmp
    End Sub

    Private Sub txtValue_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtValue.TextChanged, txtValue2.TextChanged
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text.StartsWith("[") And cmb.Text.EndsWith("]") Then
                cmb.Enabled = False

                If cmb.Text.ToLower = "[sql-rdnull]" Then chkNull.Checked = True

                If cmb.Text.ToLower = "[sql-rddefault]" Then chkIgnore.Checked = True

                If cmb.Text.ToLower = "[selectall]" Then chkAllValues.Checked = True
           
            Else
                cmb.Enabled = True
                ToolTip1.SetToolTip(txtValue, String.Empty)
                chkNull.Checked = False
                chkIgnore.Checked = False
            End If
        Catch ex As Exception
            Beep()
        End Try
    End Sub

    Private Sub chkLBoundInc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkLBoundInc.CheckedChanged, chkUBoundInc.CheckedChanged, chkLBound.CheckedChanged, chkUBound.CheckedChanged


    End Sub


    Private Sub chkIgnore_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIgnore.CheckedChanged

        txtValue.Enabled = Not (chkNull.Checked) And Not (chkIgnore.Checked)

        If chkIgnore.Checked = True Then
            txtValue.DropDownStyle = ComboBoxStyle.DropDown
            txtValue.Text = "[SQL-RDDefault]"
            lsvValues.Enabled = False
            For Each it As ListViewItem In lsvValues.Items
                it.Checked = False
            Next
        Else
            txtValue.Text = ""
            txtValue2.Text = ""
            lsvValues.Enabled = True

            If lsvValues.Items.Count > 0 Then
                txtValue.Enabled = False
            End If

        End If
    End Sub

    Private Sub chkNull_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNull.CheckedChanged

        txtValue.Enabled = Not (chkNull.Checked) And Not (chkIgnore.Checked)

        If chkNull.Checked = True Then
            txtValue.DropDownStyle = ComboBoxStyle.DropDown
            txtValue.Text = "[SQL-RDNull]"
            lsvValues.Enabled = False

            For Each it As ListViewItem In lsvValues.Items
                it.Checked = False
            Next
        Else
            txtValue.Text = ""
            lsvValues.Enabled = True

            If lsvValues.Items.Count > 0 Then
                txtValue.Enabled = False
            End If
        End If
    End Sub



    Private Sub chkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAll.CheckedChanged
        'RemoveHandler lsvValues.ItemChecked, AddressOf lsvValues_ItemChecked
        myparentPanel.m_controlFiresEvents = False

        For Each it As ListViewItem In lsvValues.Items
            it.Checked = chkAll.Checked
        Next

        myparentPanel.m_controlFiresEvents = True

        lsvValues_ItemChecked1(Nothing, Nothing)

        ' AddHandler lsvValues.ItemChecked, AddressOf lsvValues_ItemChecked
    End Sub

    Sub doFakeProgress()
        wait.Visible = True

        Application.DoEvents()

        For x As Integer = 1 To 88
            wait.Value = x
            Application.DoEvents()
        Next
    End Sub

    Public Sub lsvValues_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
        Return

        Try


            Dim nextPar As ucParameters = myparentPanel.parameterControls(m_parameterIndex + 1)
            Dim nextParIndex As Integer = m_parameterIndex + 1
            Dim hs As List(Of KeyValuePair(Of String, String)) = myparentPanel.getParameterValidValues(nextParIndex, nextPar.m_parameterName)

            For I As Integer = nextParIndex To myparentPanel.parameterControls.Count - 1
                If CType(myparentPanel.parameterControls(I), ucParameters).m_allowMultipleValues Then
                    CType(myparentPanel.parameterControls(I), ucParameters).lsvValues.Items.Clear()
                Else
                    CType(myparentPanel.parameterControls(I), ucParameters).txtValue.Items.Clear()
                End If
            Next

            If hs IsNot Nothing Then
                For Each pair As KeyValuePair(Of String, String) In hs
                    If nextPar.m_allowMultipleValues Then
                        Dim it As ListViewItem = nextPar.lsvValues.Items.Add(pair.Key)
                        it.Tag = pair.Value
                    Else
                        nextPar.txtValue.Items.Add(pair.Key)
                    End If
                Next
                'For Each de As DictionaryEntry In hs
                '    If nextPar.m_allowMultipleValues Then
                '        Dim it As ListViewItem = nextPar.lsvValues.Items.Add(de.Key)
                '        it.Tag = de.Value
                '    Else
                '        nextPar.txtValue.Items.Add(de.Key)
                '    End If
                'Next

                If hs.Count = 0 Then
                    nextPar.m_hasDefaultValues = False
                Else
                    nextPar.m_hasDefaultValues = True
                End If
            End If

            Application.DoEvents()
            wait.Value = 100
            wait.Visible = False
        Catch
            wait.Value = 100
            wait.Visible = False
            Application.DoEvents()
        End Try
    End Sub


    Private Sub btnAddValue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddValue.Click
        If txtValue.Text <> "" Then
            Dim found As Boolean = False

            For Each it As ListViewItem In lsvValues.Items
                If it.Text = txtValue.Text Then
                    found = True
                    Exit For
                End If
            Next

            If Not found Then
                Dim it As ListViewItem = lsvValues.Items.Add(txtValue.Text)
                it.Tag = txtValue.Text
                it.Checked = True
                it.Selected = True
                it.EnsureVisible()
            End If

            txtValue.Text = ""
            txtValue.Focus()
        End If
    End Sub

    Private Sub grpParameter_Click(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub chkAllValues_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkAllValues.CheckedChanged

        txtValue.Enabled = Not (chkNull.Checked) And Not (chkIgnore.Checked) And Not (chkAllValues.Checked)

        If chkAllValues.Checked = True Then
            txtValue.DropDownStyle = ComboBoxStyle.DropDown
            txtValue.Text = "[SelectAll]"
            lsvValues.Enabled = False

            RemoveHandler Me.lsvValues.ItemChecked, AddressOf lsvValues_ItemChecked

            For Each it As ListViewItem In lsvValues.Items
                it.Remove()
            Next

            Dim itx As ListViewItem = lsvValues.Items.Add("[SelectAll]")
            itx.Checked = True

            lsvValues_ItemChecked(Nothing, Nothing)

            AddHandler Me.lsvValues.ItemChecked, AddressOf lsvValues_ItemChecked
        Else
            txtValue.Text = ""
            txtValue2.Text = ""
            lsvValues.Enabled = True

            If lsvValues.Items.Count > 0 Then
                txtValue.Enabled = False
            End If
        End If
    End Sub

    Dim prevHeight As Integer

    Public ReadOnly Property viewHeight As Integer
        Get
            If m_allowMultipleValues Then
                Return maxHeight
            Else
                Return minHeight
            End If
        End Get
    End Property

    Private Sub grpParameter_ExpandedChanged(sender As Object, e As DevComponents.DotNetBar.ExpandedChangeEventArgs) Handles grpParameter.ExpandedChanged
        If grpParameter.Expanded = False Then
            Me.Height = 44

            Dim sp As DevComponents.DotNetBar.SuperTooltip = New DevComponents.DotNetBar.SuperTooltip
            Dim spi As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo("Values", "",
                                                                                                               Me.userVisibleParameterValues,
                                                                                                           Nothing,
                                                                                                               Nothing,
                                                                                                               DevComponents.DotNetBar.eTooltipColor.Office2003)
            sp.SetSuperTooltip(grpParameter.TitlePanel, spi)
        Else
            Dim sp As DevComponents.DotNetBar.SuperTooltip = New DevComponents.DotNetBar.SuperTooltip
            Dim spi As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo("Values", "",
                                                                                                              Me.userVisibleParameterValues,
                                                                                                          Nothing,
                                                                                                              Nothing,
                                                                                                              DevComponents.DotNetBar.eTooltipColor.Office2003)
            sp.SetSuperTooltip(grpParameter.TitlePanel, spi)
            Me.Height = viewHeight
        End If

        wait.Visible = False

        'If hasDefaultValues = False Then
        '    btnAddValue.Visible = True
        'Else
        '    btnAddValue.Visible = False
        'End If
    End Sub


    Private Sub btnGetValues_Click(sender As System.Object, e As System.EventArgs) Handles btnGetValues.Click
        Try
            If myparentPanel.m_controlFiresEvents = False Then Return

            doFakeProgress()

            myparentPanel.m_controlFiresEvents = False

            'If m_parameterIndex = 0 Then Return

            lsvValues.Items.Clear()
            txtValue.Items.Clear()


            Dim hs As List(Of KeyValuePair(Of String, String)) = myparentPanel.getParameterValidValues(m_parameterIndex, m_parameterName)

            If hs Is Nothing OrElse hs.Count = 0 Then
                hs = myparentPanel.getParameterValuesFromRDL(m_rdlFilePath, m_parameterName, m_parameterType, m_allowMultipleValues)
            ElseIf hs.Count = 0 Then
                hs = myparentPanel.getParameterValuesFromRDL(m_rdlFilePath, m_parameterName, m_parameterType, m_allowMultipleValues)
            End If


            If hs IsNot Nothing Then
                For Each pair As KeyValuePair(Of String, String) In hs
                    If m_allowMultipleValues Then

                        Dim it As ListViewItem = lsvValues.Items.Add(pair.Key)
                        it.Tag = pair.Value
                    Else
                        txtValue.Items.Add(pair.Key)
                    End If
                Next

                'For Each de As DictionaryEntry In hs
                '    If m_allowMultipleValues Then

                '        Dim it As ListViewItem = lsvValues.Items.Add(de.Key)
                '        it.Tag = de.Value
                '    Else
                '        txtValue.Items.Add(de.Key)
                '    End If
                'Next

                If hs.Count = 0 Then
                    m_hasDefaultValues = False
                Else
                    m_hasDefaultValues = True
                End If
            End If

            Application.DoEvents()
            wait.Value = 100
            wait.Visible = False
        Catch
            wait.Value = 100
            wait.Visible = False
            Application.DoEvents()
        End Try

        myparentPanel.m_controlFiresEvents = True
    End Sub

    Private Sub lsvValues_ItemChecked1(sender As Object, e As System.Windows.Forms.ItemCheckedEventArgs) Handles lsvValues.ItemChecked
        If myparentPanel.m_controlFiresEvents = False Then Return

        Dim loadAutomatically As Boolean = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("LoadSSRSParametersAutomatically", 1))

        If loadAutomatically = False Then Return


        If m_parameterIndex = myparentPanel.m_ParameterControlsCollection.Count - 1 Then
            Return '//no need to do shit 
        Else
            Dim nextIndex As Integer = m_parameterIndex + 1
            Dim nextControl As ucParameters

            For Each ctrl As ucParameters In myparentPanel.m_ParameterControlsCollection
                If ctrl.m_parameterIndex = nextIndex Then
                    nextControl = ctrl
                    Exit For
                End If
            Next

            If nextControl.m_allowMultipleValues = True Then
                nextControl.btnGetValues_Click(Nothing, Nothing)
            Else
                nextControl.txtValue_DropDown(Nothing, Nothing)
            End If
        End If

        Me.wait.Visible = False
    End Sub

    Private Sub optSpecificValue_CheckedChanged(sender As Object, e As EventArgs) Handles optSpecificValue.CheckedChanged
        txtValue.Enabled = True

        Try
            Dim arr As ArrayList = New ArrayList

            For Each it As ListViewItem In lsvValues.Items
                If it.Text = "[SelectAll]" Or it.Text = "[SQL-RDDefault]" Or it.Text = "[SQL-RDNull]" Then
                    arr.Add(it)
                End If
            Next

            For Each it As ListViewItem In arr
                lsvValues.Items.Remove(it)
            Next
        Catch : End Try
    End Sub
End Class
