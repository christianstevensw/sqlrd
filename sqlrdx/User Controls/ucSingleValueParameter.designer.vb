﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucSingleValueParameter
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblParameterName = New System.Windows.Forms.Label()
        Me.cmbValue = New System.Windows.Forms.ComboBox()
        Me.optSpecifiedValue = New System.Windows.Forms.RadioButton()
        Me.optNullValue = New System.Windows.Forms.RadioButton()
        Me.optDefaultValue = New System.Windows.Forms.RadioButton()
        Me.btnOK = New DevComponents.DotNetBar.ButtonX()
        Me.SuspendLayout()
        '
        'lblParameterName
        '
        Me.lblParameterName.AutoSize = True
        Me.lblParameterName.Location = New System.Drawing.Point(14, 15)
        Me.lblParameterName.Name = "lblParameterName"
        Me.lblParameterName.Size = New System.Drawing.Size(92, 13)
        Me.lblParameterName.TabIndex = 0
        Me.lblParameterName.Text = "[Parameter Name]"
        '
        'cmbValue
        '
        Me.cmbValue.FormattingEnabled = True
        Me.cmbValue.Location = New System.Drawing.Point(17, 31)
        Me.cmbValue.Name = "cmbValue"
        Me.cmbValue.Size = New System.Drawing.Size(273, 21)
        Me.cmbValue.Sorted = True
        Me.cmbValue.TabIndex = 1
        '
        'optSpecifiedValue
        '
        Me.optSpecifiedValue.AutoSize = True
        Me.optSpecifiedValue.Checked = True
        Me.optSpecifiedValue.Location = New System.Drawing.Point(17, 58)
        Me.optSpecifiedValue.Name = "optSpecifiedValue"
        Me.optSpecifiedValue.Size = New System.Drawing.Size(99, 17)
        Me.optSpecifiedValue.TabIndex = 2
        Me.optSpecifiedValue.TabStop = True
        Me.optSpecifiedValue.Text = "Specified Value"
        Me.optSpecifiedValue.UseVisualStyleBackColor = True
        '
        'optNullValue
        '
        Me.optNullValue.AutoSize = True
        Me.optNullValue.Location = New System.Drawing.Point(122, 58)
        Me.optNullValue.Name = "optNullValue"
        Me.optNullValue.Size = New System.Drawing.Size(73, 17)
        Me.optNullValue.TabIndex = 2
        Me.optNullValue.Text = "Null Value"
        Me.optNullValue.UseVisualStyleBackColor = True
        '
        'optDefaultValue
        '
        Me.optDefaultValue.AutoSize = True
        Me.optDefaultValue.Location = New System.Drawing.Point(201, 58)
        Me.optDefaultValue.Name = "optDefaultValue"
        Me.optDefaultValue.Size = New System.Drawing.Size(89, 17)
        Me.optDefaultValue.TabIndex = 2
        Me.optDefaultValue.Text = "Default Value"
        Me.optDefaultValue.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnOK.Location = New System.Drawing.Point(296, 31)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(48, 21)
        Me.btnOK.TabIndex = 10
        Me.btnOK.Text = "OK"
        '
        'ucSingleValueParameter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.optDefaultValue)
        Me.Controls.Add(Me.optNullValue)
        Me.Controls.Add(Me.optSpecifiedValue)
        Me.Controls.Add(Me.cmbValue)
        Me.Controls.Add(Me.lblParameterName)
        Me.Name = "ucSingleValueParameter"
        Me.Size = New System.Drawing.Size(350, 88)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblParameterName As System.Windows.Forms.Label
    Public WithEvents cmbValue As System.Windows.Forms.ComboBox
    Friend WithEvents optSpecifiedValue As System.Windows.Forms.RadioButton
    Friend WithEvents optNullValue As System.Windows.Forms.RadioButton
    Friend WithEvents optDefaultValue As System.Windows.Forms.RadioButton
    Public WithEvents btnOK As DevComponents.DotNetBar.ButtonX

End Class
