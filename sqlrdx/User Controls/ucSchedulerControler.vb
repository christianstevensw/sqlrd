﻿Public Class ucSchedulerControler
    Public Enum enum_svcStatus As Integer
        RUNNING = 0
        STOPPED = 1
    End Enum
    Private Sub ButtonX2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStartScheduler.Click
        imageSchedulerstatus.Image = Image.FromFile(getAssetLocation("trafficlight_red_yellow.png"))
        lblSchedulerStatus.Text = "Starting"
        imageSchedulerstatus.Refresh()

        clsServiceController.itemGlobal.StartScheduler()
        _Delay(1.5)

        imageSchedulerstatus.Image = Image.FromFile(getAssetLocation("trafficlight_green.png"))
        imageSchedulerstatus.Refresh()
        lblSchedulerStatus.Text = "Running"
        btnStartScheduler.Enabled = False
        btnStopScheduler.Enabled = True
    End Sub

    Private Sub btnStopScheduler_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStopScheduler.Click
        imageSchedulerstatus.Image = Image.FromFile(getAssetLocation("trafficlight_yellow.png"))
        imageSchedulerstatus.Refresh()
        lblSchedulerStatus.Text = "Stopping"

        clsServiceController.itemGlobal.StopScheduler()
        _Delay(1.5)

        imageSchedulerstatus.Image = Image.FromFile(getAssetLocation("trafficlight_red.png"))
        imageSchedulerstatus.Refresh()
        lblSchedulerStatus.Text = "Stopped"

        btnStopScheduler.Enabled = False
        btnStartScheduler.Enabled = True
    End Sub

    Public ReadOnly Property schedulerInstalled As Boolean
        Get
            Dim svc As clsServiceController = New clsServiceController

            If svc.m_serviceType = clsServiceController.enum_svcType.NONE Then
                Return False
            Else
                Return True
            End If
        End Get
    End Property

    Public WriteOnly Property schedulerStatus As enum_svcStatus
        Set(ByVal value As enum_svcStatus)
            If schedulerInstalled = False Then
                tblSchedulerStatus.Enabled = False
                lblSchedulerStatus.Text = "Not Installed"
                imageSchedulerstatus.Image = Image.FromFile(getAssetLocation("trafficlight_off.png"))
                btnStartScheduler.Enabled = False
                btnStopScheduler.Enabled = False
            Else
                If value = clsServiceController.enum_svcStatus.RUNNING Then
                    btnStartScheduler.Enabled = False
                    btnStopScheduler.Enabled = True

                    imageSchedulerstatus.Image = Image.FromFile(getAssetLocation("trafficlight_green.png"))
                    imageSchedulerstatus.Refresh()

                    btnStopScheduler.Enabled = True
                    btnStartScheduler.Enabled = False
                    tblSchedulerStatus.Enabled = True
                    lblSchedulerStatus.Text = "Running"
                Else
                    btnStopScheduler.Enabled = False
                    btnStartScheduler.Enabled = True
                    imageSchedulerstatus.Image = Image.FromFile(getAssetLocation("trafficlight_red.png"))
                    imageSchedulerstatus.Refresh()

                    btnStopScheduler.Enabled = False
                    btnStartScheduler.Enabled = True
                    tblSchedulerStatus.Enabled = True
                    lblSchedulerStatus.Text = "Stopped"
                End If
            End If
        End Set
    End Property



End Class
