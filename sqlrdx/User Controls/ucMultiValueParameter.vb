﻿Imports System.Collections.Generic

Public Class ucMultiValueParameter
    Sub New(avValue As Hashtable, pname As String, prompt As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        availableValuesList = avValue
        parameterName = pname
        parameterPrompt = prompt

        lblParameterName.Text = String.Format("{0} ({1})", parameterPrompt, parameterName)
    End Sub

    Dim m_availableValuesList As Hashtable

    Public Property availableValuesList As Hashtable
        Get
            Return m_availableValuesList
        End Get
        Set(value As Hashtable)
            m_availableValuesList = value

            cmbValue.Items.Clear()

            For Each de As DictionaryEntry In value
                cmbValue.Items.Add(de.Key)
            Next
        End Set
    End Property

    Public WriteOnly Property lockParameter As Boolean
        Set(value As Boolean)

            Me.Enabled = False

        End Set
    End Property


    Dim m_parameterName As String

    Public Property parameterName As String
        Get
            Return m_parameterName
        End Get
        Set(value As String)
            m_parameterName = value
        End Set
    End Property

    Private m_parameterPrompt As String
    Public Property parameterPrompt() As String
        Get
            Return m_parameterPrompt
        End Get
        Set(ByVal value As String)
            m_parameterPrompt = value
        End Set
    End Property

    Public Property selectedValue As List(Of KeyValuePair(Of String, String))
        Get

            Dim ls As List(Of KeyValuePair(Of String, String)) = New List(Of KeyValuePair(Of String, String))

            If optNullValue.Checked Or optAllValues.Checked Or optDefaultValue.Checked Then
                Dim selected As KeyValuePair(Of String, String) = New KeyValuePair(Of String, String)(cmbValue.Text, cmbValue.Text)
                ls.Add(selected)
            Else

                For Each item As Object In cmbValue.CheckBoxItems

                    If item.Checked Then
                        Dim value As String = availableValuesList(item.Text)

                        If value Is Nothing Or value = "" Then
                            value = item.Text
                        End If

                        Dim selected As KeyValuePair(Of String, String) = New KeyValuePair(Of String, String)(item.Text, value)
                        ls.Add(selected)
                    End If
                Next

                Dim numChecked As Integer = 0

                For I As Integer = 0 To cmbValue.CheckBoxItems.Count - 1
                    If cmbValue.CheckBoxItems(I).Checked Then
                        numChecked += 1
                    End If
                Next

                If cmbValue.Text.Contains("|") Or numChecked = 0 Then
                    For Each s As String In cmbValue.Text.Split("|")
                        Dim compareString As String = s.Trim
                        Dim found As Boolean = False

                        For Each kvp As KeyValuePair(Of String, String) In ls
                            If String.Compare(kvp.Key, compareString, True) = 0 Then
                                found = True
                            End If
                        Next

                        If Not found Then
                            ls.Add(New KeyValuePair(Of String, String)(compareString, compareString))
                        End If
                    Next
                End If
            End If

            Return ls
        End Get

        Set(value As List(Of KeyValuePair(Of String, String)))
            For Each kp As KeyValuePair(Of String, String) In value
                If kp.Key = "[SQL-RDNull]" Then
                    optNullValue.Checked = True
                ElseIf kp.Key = "[SQL-RDDefault]" Then
                    optDefaultValue.Checked = True
                ElseIf kp.Key = "[SelectAll]" Then
                    optAllValues.Checked = True
                Else
                    For Each item As Object In cmbValue.CheckBoxItems
                        If String.Compare(item.Text, kp.Key, True) = 0 Then
                            item.Checked = True
                        End If
                    Next

                    If availableValuesList.ContainsKey(kp.Key) Then
                        availableValuesList(kp.Key) = kp.Value
                    Else
                        availableValuesList.Add(kp.Key, kp.Value)
                    End If
                End If
            Next
        End Set
    End Property

    Private Sub optNullValue_CheckedChanged(sender As Object, e As EventArgs) Handles optNullValue.CheckedChanged, optSpecifiedValue.CheckedChanged, optDefaultValue.CheckedChanged, optAllValues.CheckedChanged
        If optNullValue.Checked Then
            cmbValue.Enabled = False
            cmbValue.Text = "[SQL-RDNull]"

            For Each chk As Object In cmbValue.CheckBoxItems
                chk.Checked = False
            Next

        ElseIf optDefaultValue.Checked Then
            cmbValue.Enabled = False
            cmbValue.Text = "[SQL-RDDefault]"

            For Each chk As Object In cmbValue.CheckBoxItems
                chk.Checked = False
            Next
        ElseIf optAllValues.Checked Then
            cmbValue.Enabled = False
            cmbValue.Text = "[SelectAll]"

            For Each chk As Object In cmbValue.CheckBoxItems
                chk.Checked = False
            Next
        Else
            If cmbValue.Text = "[SQL-RDNull]" Or cmbValue.Text = "[SQL-RDDefault]" Or cmbValue.Text = "[SelectAll]" Then cmbValue.Text = ""

            cmbValue.Enabled = True
        End If
    End Sub



    Private Sub btnCancel_Click(sender As Object, e As EventArgs)
        Me.Parent.Controls.Remove(Me)
    End Sub



    Private Sub chkSelectAll_CheckedChanged(sender As Object, e As EventArgs) Handles chkSelectAll.CheckedChanged

        For Each item As Object In cmbValue.CheckBoxItems
            item.Checked = chkSelectAll.Checked
        Next

    End Sub

   
    Private Sub cmbValue_DropDown(sender As Object, e As EventArgs) Handles cmbValue.dropDown
        Stop
    End Sub

    Private Sub cmbValue_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbValue.SelectedIndexChanged

    End Sub

    Private Sub ucMultiValueParameter_Load(sender As Object, e As EventArgs) Handles Me.Load
        setupForDragAndDrop(Me.cmbValue)
    End Sub

   

  
End Class
