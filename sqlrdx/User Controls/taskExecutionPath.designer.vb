﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class taskExecutionPath
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.optProduction = New System.Windows.Forms.RadioButton()
        Me.optDelivery = New System.Windows.Forms.RadioButton()
        Me.cmbRunWhen = New System.Windows.Forms.ComboBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.cmbRunWhen)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(312, 136)
        Me.GroupBox1.TabIndex = 18
        Me.GroupBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(136, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(168, 16)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "THE SCHEDULE"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.optProduction)
        Me.GroupBox2.Controls.Add(Me.optDelivery)
        Me.GroupBox2.Enabled = False
        Me.GroupBox2.Location = New System.Drawing.Point(8, 56)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(296, 72)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'optProduction
        '
        Me.optProduction.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optProduction.Location = New System.Drawing.Point(8, 40)
        Me.optProduction.Name = "optProduction"
        Me.optProduction.Size = New System.Drawing.Size(224, 24)
        Me.optProduction.TabIndex = 0
        Me.optProduction.Tag = "Production"
        Me.optProduction.Text = "After Successful Report Production"
        '
        'optDelivery
        '
        Me.optDelivery.Checked = True
        Me.optDelivery.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDelivery.Location = New System.Drawing.Point(8, 16)
        Me.optDelivery.Name = "optDelivery"
        Me.optDelivery.Size = New System.Drawing.Size(224, 24)
        Me.optDelivery.TabIndex = 0
        Me.optDelivery.TabStop = True
        Me.optDelivery.Tag = "Delivery"
        Me.optDelivery.Text = "After Successful Report Delivery"
        '
        'cmbRunWhen
        '
        Me.cmbRunWhen.ItemHeight = 13
        Me.cmbRunWhen.Items.AddRange(New Object() {"BEFORE", "AFTER", "BOTH"})
        Me.cmbRunWhen.Location = New System.Drawing.Point(8, 24)
        Me.cmbRunWhen.Name = "cmbRunWhen"
        Me.cmbRunWhen.Size = New System.Drawing.Size(120, 21)
        Me.cmbRunWhen.TabIndex = 0
        Me.cmbRunWhen.Text = "AFTER"
        '
        'taskExecutionPath
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "taskExecutionPath"
        Me.Size = New System.Drawing.Size(322, 143)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents optProduction As System.Windows.Forms.RadioButton
    Friend WithEvents optDelivery As System.Windows.Forms.RadioButton
    Friend WithEvents cmbRunWhen As System.Windows.Forms.ComboBox

End Class
