﻿
Imports System.Collections.Generic
Public Class ucParameterController
    Public parameters() ' ReportServer.ReportParameter()
    Dim reportPath As String
    Dim rpt 'As ReportingService
    Dim credentials()
    Public Property reportid As Integer

    Property url As String
    Property serverUser As String
    Property serverPassword As String
    Property formsAuth As Boolean

    Private ReadOnly Property ssrsAPIVersion As Integer
        Get
            If TypeOf rpt Is ReportServer.ReportingService Then
                Return 2000
            ElseIf TypeOf rpt Is ReportServer_2005.ReportingService2005 Then
                Return 2005
            ElseIf TypeOf rpt Is ReportServer_2006.ReportingService2006 Then
                Return 2006
            ElseIf TypeOf rpt Is ReportServer_2010.ReportingService2010 Then
                Return 2010
            End If
        End Get
    End Property

    Public Property ssrsConnected As Boolean

    Public ReadOnly Property numberOfParameters As Integer
        Get
            Return lsvParameters.Items.Count
        End Get
    End Property

    Private Sub configureReportServerConnection()
        AppStatus(True)

        Dim srsVersion As String = clsMarsReport.m_serverVersion(url, serverUser, serverPassword, formsAuth)

        If srsVersion >= "2007" Then
            rpt = New rsClients.rsClient2008(url)
        Else
            rpt = New rsClients.rsClient(url)
        End If

        If url.Contains("2006.asmx") Then '//its sharepoint mode
            rpt = New ReportServer_2006.ReportingService2006
        ElseIf url.Contains("2010.asmx") Then
            rpt = New rsClients.rsClient2010(url)
        End If

        rpt.Url = url

        If serverUser = "" Then
            rpt.Credentials = System.Net.CredentialCache.DefaultCredentials

            If formsAuth Then
                Try
                    '//for forms auth
                    Dim netCredentials As System.Net.NetworkCredential = New System.Net.NetworkCredential

                    netCredentials = System.Net.CredentialCache.DefaultCredentials

                    rpt.LogonUser(netCredentials.UserName, netCredentials.Password, Nothing)
                Catch : End Try
            End If
        Else

            Dim domain As String = Nothing
            Dim tmpUser As String = serverUser

            getDomainAndUserFromString(tmpUser, domain)

            Dim cred As System.Net.NetworkCredential = New System.Net.NetworkCredential(tmpUser, serverPassword, domain)
            rpt.Credentials = cred

            If formsAuth Then
                Try
                    rpt.LogonUser(tmpUser, serverPassword, domain)

                    Try

                        If (rpt.CheckAuthorized()) Then
                            Dim type As ReportServer.ItemTypeEnum = rpt.GetItemType("/")

                            ' Console.WriteLine(type)

                        End If

                    Catch ex As Exception
                        ' Console.WriteLine("Exception on call to GetItemType.")
                    End Try

                    rpt.SessionHeaderValue = New ReportServer.SessionHeader()
                Catch : End Try
            End If
        End If

        ssrsConnected = True

        AppStatus(False)
    End Sub


   

    Public Overloads Sub loadParameters(m_url As String, m_reportPath As String, m_credentials As ReportServer.DataSourceCredentials(), m_serverUser As String, m_serverPassword As String, m_formAuth As Boolean)
10:     AppStatus(True)
20:     Try
            If reportid = 0 Then reportid = 99999

            Dim par As Object ' sqlrd.ReportServer.ReportParameter
            Dim historyID As String = Nothing
            Dim forRendering As Boolean = True
            Dim values() = Nothing ' sqlrd.ReportServer.ParameterValue() = Nothing


30:         reportPath = m_reportPath
40:         credentials = m_credentials
50:         url = m_url
60:         serverUser = m_serverUser
70:         serverPassword = m_serverPassword
80:         formsAuth = m_formAuth

90:         clsMarsUI.BusyProgress(20, "configuring report services")
100:        configureReportServerConnection()

110:        clsMarsUI.BusyProgress(70, "Obtaining parameters from server")


            Dim ssrsVersion As Integer = cssreport.getSSRSVersion(rpt)

            If m_credentials Is Nothing Then
                m_credentials = cssreport.createDataSourceCredentials(99999, ssrsVersion)
            End If

120:        If TypeOf rpt Is rsClients.rsClient2010 Then
130:            parameters = rpt.GetItemParameters(reportPath, historyID, forRendering, values, credentials)
140:        Else
150:            parameters = rpt.GetReportParameters(reportPath, historyID, forRendering, values, credentials)
            End If

            If parameters IsNot Nothing AndAlso parameters.GetLength(0) = 0 Then
                InfoBox.Visible = True
            Else
                InfoBox.Visible = False
            End If

160:        clsMarsUI.BusyProgress(90, "loading parameters")

170:        lsvParameters.Items.Clear()

            g_parameterList = Nothing

            g_parameterList = New ArrayList

180:        For Each par In parameters
190:            Dim item As parameterListViewItem = New parameterListViewItem(Convert.ToString(par.Name))
200:            item.SubItems.Add(par.Prompt)
210:            item.SubItems.Add(par.MultiValue.ToString)
220:            lsvParameters.Items.Add(item)

230:            If g_parameterList.Contains(item.Text) = False Then g_parameterList.Add(item.Text)

240:            Dim defaultParameter As clsDefaultParameter = New clsDefaultParameter(par.Name)

250:            If defaultParameter.hasValue = True Then
260:                item.hasDefaultParameterValue = True

270:                If par.MultiValue = True Then
280:                    Dim ls As List(Of KeyValuePair(Of String, String)) = New List(Of KeyValuePair(Of String, String))
290:                    Dim kvp As KeyValuePair(Of String, String) = New KeyValuePair(Of String, String)(defaultParameter.parameterValue, defaultParameter.parameterValue)

300:                    ls.Add(kvp)

310:                    item.Tag = ls
320:                Else
330:                    Dim kvp As KeyValuePair(Of String, String) = New KeyValuePair(Of String, String)(defaultParameter.parameterValue, defaultParameter.parameterValue)

340:                    item.Tag = kvp
                    End If
                End If
350:        Next
360:    Catch ex As Exception
370:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
380:    Finally
390:        clsMarsUI.BusyProgress(100, "cleaning up", True)
400:        AppStatus(False)
        End Try
    End Sub


    Public Overloads Sub loadParameters(m_reportid As Integer, m_url As String, m_reportPath As String, m_credentials As ReportServer.DataSourceCredentials(), m_serverUser As String, m_serverPassword As String, m_formAuth As Boolean)
10:     Try
20:         AppStatus(True)

            Me.reportid = m_reportid

            Dim SQL As String = "SELECT * FROM reportparameter WHERE reportid = " & m_reportid & " ORDER BY parindex"
            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            Dim par As Object ' sqlrd.ReportServer.ReportParameter
            Dim historyID As String = Nothing
            Dim forRendering As Boolean = True
            Dim values() = Nothing ' sqlrd.ReportServer.ParameterValue() = Nothing


30:         reportPath = m_reportPath
40:         credentials = m_credentials
50:         url = m_url
60:         serverUser = m_serverUser
70:         serverPassword = m_serverPassword
80:         formsAuth = m_formAuth

            ' clsMarsUI.BusyProgress(20, "configuring report services")

            ' configureReportServerConnection()

90:         clsMarsUI.BusyProgress(70, "Obtaining parameters from server")

100:        lsvParameters.Items.Clear()
            g_parameterList = Nothing
110:        g_parameterList = New ArrayList

120:        If oRs IsNot Nothing Then
130:            Do While oRs.EOF = False
140:                Dim it As parameterListViewItem = New parameterListViewItem
150:                it.Text = oRs("parname").Value

160:                If g_parameterList.Contains(it.Text) = False Then g_parameterList.Add(it.Text)

170:                If IsNull(oRs("parameterprompt").Value, "") = "" Then
180:                    it.SubItems.Add(it.Text)
190:                Else
200:                    it.SubItems.Add(oRs("parameterprompt").Value)
                    End If

210:                it.SubItems.Add(oRs("multivalue").Value)

                    Dim parvalues, parlabels As String

220:                parvalues = IsNull(oRs("parvalue").Value, "")
230:                parlabels = IsNull(oRs("parvaluelabel").Value, parvalues)

240:                If oRs("multivalue").Value.ToString.ToLower = "true" Then
250:                    Dim ls As List(Of KeyValuePair(Of String, String)) = New List(Of KeyValuePair(Of String, String))

260:                    For i As Integer = 0 To parvalues.Split("|").GetUpperBound(0)
                            Dim value, key As String

270:                        value = parvalues.Split("|")(i)

280:                        Try
290:                            key = parlabels.Split("|")(i)
300:                        Catch
310:                            key = value
                            End Try

320:                        Dim kvp As KeyValuePair(Of String, String) = New KeyValuePair(Of String, String)(key, value)

330:                        ls.Add(kvp)
340:                    Next

350:                    it.Tag = ls
360:                Else
370:                    Dim kvp As KeyValuePair(Of String, String) = New KeyValuePair(Of String, String)(parlabels, parvalues)

380:                    it.Tag = kvp
                    End If

                    ''//override it with default values if need be
390:                Dim defaultParameter As clsDefaultParameter = New clsDefaultParameter(it.Text)

400:                If defaultParameter.hasValue = True And clsDefaultParameter.AdminCanEditDefaultParameters = False Then
410:                    it.hasDefaultParameterValue = True
                    End If

420:                lsvParameters.Items.Add(it)

430:                oRs.MoveNext()
440:            Loop

450:            oRs.Close()
            End If

            If lsvParameters.Items.Count = 0 Then
                InfoBox.Text = "This report does not have any parameters. You can click 'Requery' to reload parameters from the server"
                InfoBox.Visible = True
            Else
                InfoBox.Visible = False
            End If

460:        oRs = Nothing
470:    Catch ex As Exception
480:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, "")
490:    Finally
500:        clsMarsUI.BusyProgress(100, "Obtaining parameters from server", True)
510:        AppStatus(False)
        End Try
    End Sub




    Private Sub lsvParameters_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lsvParameters.SelectedIndexChanged
        Dim logFile As String = "parameterController.debug"

        clsMarsDebug.writeToDebug(logFile, "Setting previous parameters for cascading...", False)

10:     Try

20:         If lsvParameters.SelectedItems.Count = 0 Then
30:             fizzle.Controls.Clear()
40:             lsvValues.Items.Clear()
50:             Return
60:         Else
70:             fizzle.Controls.Clear()
80:             lsvValues.Items.Clear()
            End If


            clsMarsDebug.writeToDebug(logFile, "Get currently selected parameter")

            Dim item As parameterListViewItem = lsvParameters.SelectedItems(0)

90:         If item.Tag IsNot Nothing Then
                clsMarsDebug.writeToDebug(logFile, "if it has a value already then show it")

100:            If TypeOf item.Tag Is List(Of KeyValuePair(Of String, String)) Then
                    Dim tmp As List(Of KeyValuePair(Of String, String)) = item.Tag

110:                For Each kv As KeyValuePair(Of String, String) In tmp
120:                    Dim it As parameterListViewItem = New parameterListViewItem(kv.Key)
130:                    it.SubItems.Add(kv.Value)

140:                    lsvValues.Items.Add(it)
150:                Next
160:            ElseIf TypeOf item.Tag Is KeyValuePair(Of String, String) Then
                    Dim kv As KeyValuePair(Of String, String) = item.Tag

170:                Dim it As parameterListViewItem = New parameterListViewItem(kv.Key)
180:                it.SubItems.Add(kv.Value)

190:                lsvValues.Items.Add(it)
                End If
            End If

            clsMarsDebug.writeToDebug(logFile, "Check connection to SSRS")

200:        If ssrsConnected = False Then
                clsMarsDebug.writeToDebug(logFile, "Connect to SSRS")
210:            configureReportServerConnection()
            End If

            clsMarsDebug.writeToDebug(logFile, "Set up SSRS with credentials")

            Dim ssrsVersion As Integer = cssreport.getSSRSVersion(rpt)

            If credentials Is Nothing Then
                credentials = cssreport.createDataSourceCredentials(reportid, ssrsVersion)
            End If

            Dim parameterName As String = item.Text
            Dim parameter As Object = Nothing ' ReportServer.ReportParameter = Nothing

220:        If parameters Is Nothing Then
                clsMarsDebug.writeToDebug(logFile, "Get parameter list from the server if its the first trip around")

230:            If TypeOf rpt Is rsClients.rsClient2010 Then
240:                parameters = rpt.GetItemParameters(reportPath, Nothing, True, Nothing, credentials)
250:            Else
260:                parameters = rpt.GetReportParameters(reportPath, Nothing, True, Nothing, credentials)
                End If
            End If
            '  parameters = rpt.GetReportParameters(reportPath, Nothing, True, Nothing, credentials)

            clsMarsDebug.writeToDebug(logFile, "Find the select parameter in the list from the server")

270:        For Each p As Object In parameters
280:            If String.Compare(p.Name, parameterName, True) = 0 Then
                    clsMarsDebug.writeToDebug(logFile, "Found it: " & parameterName)
290:                parameter = p
300:                Exit For
                End If
310:        Next

320:        If item.Index > 0 Then
                clsMarsDebug.writeToDebug(logFile, "Its not the very first parameter, so lets set the previous values")

                'If parameter.State = ParameterStateEnum.HasOutstandingDependencies Or parameter.State = ParameterStateEnum.MissingValidValue Then
                Dim values()

                Dim parameterCount As Integer = 0

330:            For I As Integer = 0 To item.Index - 1
                    Dim previousItem As parameterListViewItem = lsvParameters.Items(I)
                    Dim multiValue As Boolean = previousItem.SubItems(2).Text

340:                If multiValue Then
                        Dim ls As List(Of KeyValuePair(Of String, String)) = previousItem.Tag

350:                    If ls IsNot Nothing Then
360:                        For Each kv As KeyValuePair(Of String, String) In ls
370:                            If kv.Value.ToLower = "[selectall]" Then
                                    clsMarsDebug.writeToDebug(logFile, "For [selectall], select all the available values for this parameter")

380:                                For Each p As Object In parameters
390:                                    If String.Compare(p.Name, previousItem.Text, True) = 0 Then
400:                                        If p.ValidValues IsNot Nothing Then

410:                                            For Each v As Object In p.ValidValues
420:                                                ReDim Preserve values(parameterCount)

430:                                                Select Case ssrsAPIVersion
                                                        Case 2000
440:                                                        values(parameterCount) = New ReportServer.ParameterValue()
450:                                                    Case 2005
460:                                                        values(parameterCount) = New ReportServer_2005.ParameterValue
470:                                                    Case 2006
480:                                                        values(parameterCount) = New ReportServer_2006.ParameterValue
490:                                                    Case 2010
500:                                                        values(parameterCount) = New ReportServer_2010.ParameterValue
510:                                                    Case Else
520:                                                        values(parameterCount) = New ReportServer.ParameterValue()
                                                    End Select

                                                    '  Dim x As ReportServer_2010.ItemParameter = New ReportServer_2010.ItemParameter
                                                    '  x.ParameterTypeName
                                                    clsMarsDebug.writeToDebug(logFile, previousItem.Text & ": " & v.Value)

530:                                                values(parameterCount).Name = previousItem.Text
540:                                                values(parameterCount).Value = v.Value

550:                                                parameterCount += 1
560:                                            Next
                                            End If

570:                                        Exit For
                                        End If
580:                                Next
590:                            Else
                                    If kv.Value.ToLower <> "[sql-rddefault]" Then
                                        clsMarsDebug.writeToDebug(logFile, "If its not [sql-rddefault], then set its value")

600:                                    ReDim Preserve values(parameterCount)

610:                                    Select Case ssrsAPIVersion
                                            Case 2000
620:                                            values(parameterCount) = New ReportServer.ParameterValue()
630:                                        Case 2005
640:                                            values(parameterCount) = New ReportServer_2005.ParameterValue
650:                                        Case 2006
660:                                            values(parameterCount) = New ReportServer_2006.ParameterValue
670:                                        Case 2010
680:                                            values(parameterCount) = New ReportServer_2010.ParameterValue
690:                                        Case Else
700:                                            values(parameterCount) = New ReportServer.ParameterValue()
                                        End Select

                                        clsMarsDebug.writeToDebug(logFile, previousItem.Text & ": " & clsMarsParser.Parser.ParseString(kv.Value))

710:                                    values(parameterCount).Name = previousItem.Text

                                        If kv.Value.ToLower = "[sql-rdnull]" Then
                                            values(parameterCount).Value = Nothing
                                        Else
                                            values(parameterCount).Value = clsMarsParser.Parser.ParseString(kv.Value)
                                        End If


730:                                    parameterCount += 1

                                    End If
                                End If
740:                        Next
                        End If
750:                Else
760:                    If previousItem.Tag IsNot Nothing Then
                            Dim kv As KeyValuePair(Of String, String) = previousItem.Tag

                            If kv.Value.ToLower <> "[sql-rddefault]" Then
770:                            ReDim Preserve values(parameterCount)
780:
                                Select Case ssrsAPIVersion
                                    Case 2000
                                        values(parameterCount) = New ReportServer.ParameterValue()
                                    Case 2005
                                        values(parameterCount) = New ReportServer_2005.ParameterValue
                                    Case 2006
                                        values(parameterCount) = New ReportServer_2006.ParameterValue
                                    Case 2010
                                        values(parameterCount) = New ReportServer_2010.ParameterValue
                                    Case Else
                                        values(parameterCount) = New ReportServer.ParameterValue()
                                End Select

                                clsMarsDebug.writeToDebug(logFile, previousItem.Text & ": " & clsMarsParser.Parser.ParseString(kv.Value))

790:                            values(parameterCount).Name = previousItem.Text

                                If kv.Value.ToLower = "[sql-rdnull]" Then
                                    values(parameterCount).Value = Nothing
                                Else
800:                                values(parameterCount).Value = clsMarsParser.Parser.ParseString(kv.Value)
                                End If

                                parameterCount += 1
                            End If
                            End If
                    End If
820:            Next

                Dim historyID As String = Nothing
                Dim forRendering As Boolean = True

                '//this is some bullshit about unable to cast Object() array to parameterValue() array.
                clsMarsDebug.writeToDebug(logFile, "Updating the server parameters")

830:            Select Case ssrsAPIVersion
                    Case 2000
                        Dim pValues() As ReportServer.ParameterValue

                        If values IsNot Nothing Then
840:                        For x As Integer = 0 To values.GetUpperBound(0)
850:                            ReDim Preserve pValues(x)

860:                            pValues(x) = values(x)
870:                        Next
                        End If
880:                    parameters = rpt.GetReportParameters(reportPath, historyID, forRendering, pValues, credentials)
890:                Case 2005
                        Dim pValues() As ReportServer_2005.ParameterValue

                        If values IsNot Nothing Then
900:                        For x As Integer = 0 To values.GetUpperBound(0)
910:                            ReDim Preserve pValues(x)

920:                            pValues(x) = values(x)
930:                        Next
                        End If

940:                    parameters = rpt.GetReportParameters(reportPath, historyID, forRendering, pValues, credentials)
950:                Case 2006
                        Dim pValues() As ReportServer_2006.ParameterValue

                        If values IsNot Nothing Then
960:                        For x As Integer = 0 To values.GetUpperBound(0)
970:                            ReDim Preserve pValues(x)

980:                            pValues(x) = values(x)
990:                        Next
                        End If

1000:                   parameters = rpt.GetReportParameters(reportPath, historyID, forRendering, pValues, credentials)
1010:               Case 2010
                        Dim pValues() As ReportServer_2010.ParameterValue

                        If values IsNot Nothing Then
1020:                       For x As Integer = 0 To values.GetUpperBound(0)
1030:                           ReDim Preserve pValues(x)

1040:                           pValues(x) = values(x)
1050:                       Next
                        End If
1060:                   parameters = rpt.GetItemParameters(reportPath, historyID, forRendering, pValues, credentials)
1070:               Case Else
                        Dim pValues() As ReportServer.ParameterValue

                        If values IsNot Nothing Then
1080:                       For x As Integer = 0 To values.GetUpperBound(0)
1090:                           ReDim Preserve pValues(x)

1100:                           pValues(x) = values(x)
1110:                       Next
                        End If

1120:                   parameters = rpt.GetReportParameters(reportPath, historyID, forRendering, pValues, credentials)
                End Select
            End If



            '//update the parameter
1130:       For Each p As Object In parameters
                Dim test As String = String.Format("pname={0} parametersname ={1}", p.name, parameterName)
                '  MsgBox(test)
1140:           If p.Name.tolower = parameterName.ToLower Then
                    '  MsgBox("match found")
1150:               parameter = p
1160:               Exit For
                End If
1170:       Next

1180:       Dim hs As Hashtable = New Hashtable

            clsMarsDebug.writeToDebug(logFile, "Get the parameters valid values (if any)")

1190:       If parameter.ValidValues IsNot Nothing Then
1200:           For Each v As Object In parameter.ValidValues
                    Try
1210:                   If hs.ContainsKey(v.Label) = False Then hs.Add(v.Label, v.Value)
                    Catch : End Try
1220:           Next
1230:       ElseIf parameter.DefaultValues IsNot Nothing Then
1240:           For Each v As String In parameter.DefaultValues
                    Try
1250:                   If hs.ContainsKey(v) = False Then hs.Add(v, v)
                    Catch : End Try
1260:           Next
            End If

            clsMarsDebug.writeToDebug(logFile, "Caching the valid values to avoid future trips to the server")

1270:       If TypeOf rpt Is rsClients.rsClient2010 Then
1280:           If CType(parameter, ReportServer_2010.ItemParameter).ParameterTypeName.ToLower = "boolean" Then
1290:               If hs.ContainsKey("True") = False Then hs.Add("True", "True")
1300:               If hs.ContainsKey("False") = False Then hs.Add("False", "False")
                End If
1310:       Else
1320:           If parameter.Type = ReportServer.ParameterTypeEnum.Boolean Then
1330:               If hs.ContainsKey("True") = False Then hs.Add("True", "True")
1340:               If hs.ContainsKey("False") = False Then hs.Add("False", "False")
                End If
            End If

            clsMarsDebug.writeToDebug(logFile, "Drawing the new control")

1350:       If parameter.MultiValue Then
1360:           Dim ctrl As ucMultiValueParameter = New ucMultiValueParameter(hs, parameterName, IIf(parameter.Prompt = "", parameterName, parameter.Prompt))
1370:           fizzle.Controls.Add(ctrl)
1380:           ctrl.Visible = True

1390:           If item.Tag IsNot Nothing Then ctrl.selectedValue = item.Tag

1400:           If item.hasDefaultParameterValue And clsDefaultParameter.AdminCanEditDefaultParameters = False Then
1410:               ctrl.lockParameter = True
                End If

1420:           AddHandler ctrl.btnOK.Click, AddressOf SetParameterEvent
1430:           AddHandler ctrl.cmbValue.KeyDown, AddressOf cmbValue_KeyDown
                AddHandler ctrl.btnAddValue.Click, AddressOf btnAddValue_Click
                AddHandler ctrl.cmbValue.CheckBoxCheckedChanged, AddressOf cmbValue_CheckBoxCheckedChanged
                AddHandler ctrl.optAllValues.CheckedChanged, AddressOf optNullValue_CheckedChanged
                AddHandler ctrl.optDefaultValue.CheckedChanged, AddressOf optNullValue_CheckedChanged
                AddHandler ctrl.optNullValue.CheckedChanged, AddressOf optNullValue_CheckedChanged
                AddHandler ctrl.optSpecifiedValue.CheckedChanged, AddressOf optNullValue_CheckedChanged

1440:       Else
1450:           Dim ctrl As ucSingleValueParameter = New ucSingleValueParameter(hs, parameterName, IIf(parameter.Prompt = "", parameterName, parameter.Prompt))
1460:           fizzle.Controls.Add(ctrl)
1470:           ctrl.Visible = True

1480:           If item.Tag IsNot Nothing Then ctrl.selectedValue = item.Tag

1490:           If item.hasDefaultParameterValue And clsDefaultParameter.AdminCanEditDefaultParameters = False Then
1500:               ctrl.lockParameter = True
                End If

1510:           AddHandler ctrl.btnOK.Click, AddressOf SetParameterEvent
1520:           AddHandler ctrl.cmbValue.KeyDown, AddressOf cmbValue_KeyDown
                AddHandler ctrl.cmbValue.SelectedIndexChanged, AddressOf cmbValue_SelectedIndexChanged
                AddHandler ctrl.optDefaultValue.CheckedChanged, AddressOf optNullValue_CheckedChanged
                AddHandler ctrl.optNullValue.CheckedChanged, AddressOf optNullValue_CheckedChanged
                AddHandler ctrl.optSpecifiedValue.CheckedChanged, AddressOf optNullValue_CheckedChanged
            End If
1530:   Catch ex As Exception
1540:       _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try
    End Sub

    Private Sub optNullValue_CheckedChanged(sender As Object, e As EventArgs)
        SetParameterEvent(sender, e)
    End Sub

    Private Sub cmbValue_SelectedIndexChanged(sender As Object, e As EventArgs)
        SetParameterEvent(Nothing, Nothing)
    End Sub
    Private Sub cmbValue_CheckBoxCheckedChanged(sender As Object, e As EventArgs)
        SetParameterEvent(Nothing, Nothing)
    End Sub


    Private Sub btnAddValue_Click(sender As Object, e As EventArgs)
        Dim ctrl As ucMultiValueParameter = fizzle.Controls(0)

        Dim value As String = InputBox("Enter Value To Add for " & ctrl.parameterName, "Parameter Value", "")

        If value <> "" Then
            If ctrl.availableValuesList.ContainsKey(value) = False Then
                ctrl.availableValuesList.Add(value, value)
            End If

            Dim found As Boolean = False

            For i As Integer = 0 To ctrl.cmbValue.Items.Count - 1
                If ctrl.cmbValue.Items(i) = value Then
                    found = True
                    Exit For
                End If
            Next

            If found = False Then
                Dim int As Integer = ctrl.cmbValue.Items.Add(value)

                Dim presItem As Object = ctrl.cmbValue.CheckBoxItems(int)

                presItem.Checked = True

            End If

            SetParameterEvent(Nothing, Nothing)
        End If
    End Sub

    Private Sub cmbValue_KeyDown(sender As Object, e As KeyEventArgs)
        If e.KeyCode = Keys.Return Or e.KeyCode = Keys.Enter Then
            SetParameterEvent(Nothing, Nothing)
        End If
    End Sub

    Private Sub SetParameterEvent(sender As Object, e As EventArgs)
        Dim ctrl As Control = fizzle.Controls(0)

        If TypeOf ctrl Is ucMultiValueParameter Then
            Dim values As List(Of KeyValuePair(Of String, String)) = CType(ctrl, ucMultiValueParameter).selectedValue
            Dim item As parameterListViewItem = lsvParameters.SelectedItems(0)
            item.Tag = values

            lsvValues.Items.Clear()

            For Each kv As KeyValuePair(Of String, String) In values
                Dim it As parameterListViewItem = New parameterListViewItem(kv.Key)
                it.SubItems.Add(kv.Value)

                lsvValues.Items.Add(it)
            Next
        Else
            lsvValues.Items.Clear()

            Dim kv As KeyValuePair(Of String, String) = CType(ctrl, ucSingleValueParameter).selectedValue

            Dim item As parameterListViewItem = lsvParameters.SelectedItems(0)

            item.Tag = kv

            Dim it As parameterListViewItem = New parameterListViewItem(kv.Key)
            it.SubItems.Add(kv.Value)

            lsvValues.Items.Add(it)

        End If

        '//reset other values 
        For I As Integer = (lsvParameters.SelectedItems(0).Index + 1) To lsvParameters.Items.Count - 1
            lsvParameters.Items(I).Tag = Nothing
        Next


        'If lsvParameters.SelectedItems.Count > 0 Then
        '    Dim index As Integer = lsvParameters.SelectedItems(0).Index

        '    For Each it As parameterListViewItem In lsvParameters.SelectedItems
        '        it.Selected = False
        '    Next

        '    If index < lsvParameters.Items.Count - 1 Then
        '        Dim it As parameterListViewItem = lsvParameters.Items(index + 1)
        '        it.Selected = True
        '        it.EnsureVisible()
        '    Else
        '        Dim it As parameterListViewItem = lsvParameters.Items(0)
        '        it.Selected = True
        '        it.EnsureVisible()
        '    End If
        'End If

    End Sub

    Public ReadOnly Property hasUnsetParameters As Boolean
        Get
            For Each it As parameterListViewItem In lsvParameters.Items
                If it.Tag Is Nothing Then
                    Return True
                End If
            Next

            Return False
        End Get
    End Property
    Public Sub saveParameters(reportID As Integer)
        Try
            Dim sCols As String
            Dim sVals As String
            Dim SQL As String
            Dim parsList As ArrayList = New ArrayList

            clsMarsData.WriteData("DELETE FROM reportparameter WHERE reportid = " & reportID)

            sCols = "ParID,ReportID,ParName,ParValue,ParType,ParNull,MultiValue,parIndex,parvaluelabel,parameterprompt"

            For Each it As parameterListViewItem In lsvParameters.Items
                Dim labels, values As String

                labels = ""
                values = ""

                If it.Tag Is Nothing Then
                    Dim defaultTag As KeyValuePair(Of String, String)

                    defaultTag = New KeyValuePair(Of String, String)("[SQL-RDDefault]", "[SQL-RDDefault]")

                    it.Tag = defaultTag
                End If

                Dim obj = it.Tag

                If TypeOf obj Is KeyValuePair(Of String, String) Then
                    Dim insertvalues(9) As String
                    Dim actualValue = CType(obj, KeyValuePair(Of String, String)).Value
                    Dim isNull As Integer = 0

                    If actualValue.ToString.ToLower = "[sql-rdnull]" Then
                        isNull = 1
                    End If

                    insertvalues(0) = clsMarsData.CreateDataID("ReportParameter", "ParID")
                    insertvalues(1) = reportID
                    insertvalues(2) = it.Text
                    insertvalues(3) = SQLPrepare(CType(obj, KeyValuePair(Of String, String)).Value)
                    insertvalues(4) = ""
                    insertvalues(5) = isNull
                    insertvalues(6) = it.SubItems(2).Text
                    insertvalues(7) = it.Index
                    insertvalues(8) = SQLPrepare(CType(obj, KeyValuePair(Of String, String)).Key)
                    insertvalues(9) = SQLPrepare(it.SubItems(1).Text)

                    sVals = String.Format("'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}'", insertvalues)

                    SQL = "INSERT INTO ReportParameter (" & sCols & ") VALUES(" & sVals & ")"

                    clsMarsData.WriteData(SQL)

                ElseIf TypeOf obj Is List(Of KeyValuePair(Of String, String)) Then
                    Dim listOfValues, listOflabels As String

                    listOfValues = ""
                    listOflabels = ""

                    For Each kvp As KeyValuePair(Of String, String) In obj
                        listOfValues &= kvp.Value & "|"
                        listOflabels &= kvp.Key & "|"
                    Next

                    If listOfValues.Length > 2 Then listOfValues = listOfValues.Remove(listOfValues.Length - 1, 1)
                    If listOflabels.Length > 2 Then listOflabels = listOflabels.Remove(listOflabels.Length - 1, 1)

                    Dim insertvalues(9) As String

                    Dim isNull As Integer = 0

                    If listOfValues.ToString.ToLower.Contains("[sql-rdnull]") Then
                        isNull = 1
                    End If

                    insertvalues(0) = clsMarsData.CreateDataID("ReportParameter", "ParID")
                    insertvalues(1) = reportID
                    insertvalues(2) = it.Text
                    insertvalues(3) = SQLPrepare(listOfValues)
                    insertvalues(4) = ""
                    insertvalues(5) = isNull
                    insertvalues(6) = it.SubItems(2).Text
                    insertvalues(7) = it.Index
                    insertvalues(8) = SQLPrepare(listOflabels)
                    insertvalues(9) = SQLPrepare(it.SubItems(1).Text)

                    sVals = String.Format("'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}'", insertvalues)

                    SQL = "INSERT INTO ReportParameter (" & sCols & ") VALUES(" & sVals & ")"

                    clsMarsData.WriteData(SQL)

                End If
            Next
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, "")
        End Try
    End Sub

    Public Sub generateParametersDataTable(ByRef dt As DataTable)
        If dt Is Nothing Then
            dt = New DataTable

            With dt.Columns
                .Add("Name")
                .Add("Type")
                .Add("MultiValue")
                .Add("Label")
                .Add("Value")
                .Add("AvailableValues")
            End With
        End If

        For Each it As parameterListViewItem In lsvParameters.Items
            Dim rows() As DataRow = dt.Select("name = '" & SQLPrepare(it.Text) & "'")

            If it.Tag Is Nothing Then Continue For

            If rows IsNot Nothing AndAlso rows.Length > 0 Then
                Continue For
            Else
                Dim dr As DataRow = dt.Rows.Add

                dr("name") = it.Text
                dr("type") = ""
                dr("multivalue") = it.SubItems(2).Text
                dr("label") = it.SubItems(1).Text

                Dim hs As Hashtable = New Hashtable

                If TypeOf it.Tag Is KeyValuePair(Of String, String) Then
                    hs.Add(CType(it.Tag, KeyValuePair(Of String, String)).Key, CType(it.Tag, KeyValuePair(Of String, String)).Value)
                Else
                    Dim ls As List(Of KeyValuePair(Of String, String)) = it.Tag


                    For Each kvp As KeyValuePair(Of String, String) In ls
                        If hs.ContainsKey(kvp.Key) = False Then
                            hs.Add(kvp.Key, kvp.Value)
                        End If
                    Next
                End If


                dr("value") = hs
                dr("availablevalues") = Nothing
            End If
        Next
    End Sub

End Class
