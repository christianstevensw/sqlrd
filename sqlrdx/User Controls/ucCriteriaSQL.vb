﻿Public Class ucCriteriaSQL
    Public dataTypes As Hashtable
    Public WriteOnly Property columns As System.Collections.Generic.List(Of String)
        Set(value As System.Collections.Generic.List(Of String))
            cmbColumns.Items.Clear()

            For Each s As String In value
                If s <> "" Then cmbColumns.Items.Add(s)
            Next
        End Set
    End Property
    Public ReadOnly Property criteria As String
        Get
            If cmbOperator.Text = "" Then Return ""
            Dim sChar As String = ""

            If getColumnType(cmbColumns.Text) = "System.String" Or getColumnType(cmbColumns.Text).ToLower.Contains("date") Then
                sChar = "'"
            End If

            Select Case cmbOperator.Text.ToLower
                Case "starts with"
                    Return cmbColumns.Text & " LIKE '" & txtCriteria.Text & "%' " & cmbAndOr.Text
                Case "ends with"
                    Return cmbColumns.Text & " LIKE '%" & txtCriteria.Text & "' " & cmbAndOr.Text
                Case "contains"
                    Return cmbColumns.Text & " LIKE '%" & txtCriteria.Text & "%' " & cmbAndOr.Text
                Case Else
                    Return cmbColumns.Text & " " & cmbOperator.Text & " " & sChar & txtCriteria.Text & sChar & " " & cmbAndOr.Text
            End Select

        End Get
    End Property

    Private Function getColumnType(columnName As String) As String
        If dataTypes Is Nothing Then
            Return "System.String"
        Else
            If dataTypes.ContainsKey(columnName) Then
                Dim type As String = dataTypes.Item(columnName)

                Return type
            Else
                Return "System.String"
            End If
        End If
    End Function


   
End Class
