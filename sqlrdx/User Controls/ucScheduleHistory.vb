﻿Imports sqlrd.clsMarsScheduler.enScheduleType
Imports DevComponents.AdvTree

Public Class ucScheduleHistory

    Private objectID As Integer = 0
    Public Property m_objectID() As Integer
        Get
            Return objectID
        End Get
        Set(ByVal value As Integer)
            objectID = value
        End Set
    End Property


    Private scheduleType As clsMarsScheduler.enScheduleType = clsMarsScheduler.enScheduleType.NONE

    Public Property m_scheduleType() As clsMarsScheduler.enScheduleType
        Get
            Return scheduleType
        End Get
        Set(ByVal value As clsMarsScheduler.enScheduleType)
            scheduleType = value
        End Set
    End Property


    Public Property m_historyLimit As Integer


    Private showControls As Boolean
    Public Property m_showControls() As Boolean
        Get
            Return showControls
        End Get
        Set(ByVal value As Boolean)
            showControls = value

            FlowLayoutPanel1.Visible = value
            ' FlowLayoutPanel2.Visible = value
        End Set
    End Property


    Public Property m_sortOrder() As String
        Get
            If optAsc.Checked Then
                Return " ASC "
            Else
                Return " DESC "
            End If
        End Get
        Set(ByVal value As String)
            If value.ToLower.Contains("asc") Then
                optAsc.Checked = True
            Else
                optDesc.Checked = True
            End If
        End Set
    End Property


    Private myfilter As Boolean = False
    Public Property m_filter() As Boolean
        Get
            Return myfilter
        End Get
        Set(ByVal value As Boolean)
            myfilter = value
        End Set
    End Property

    Private showSchedulesList As Boolean
    Public Property m_showSchedulesList() As Boolean
        Get
            Return showSchedulesList
        End Get
        Set(ByVal value As Boolean)
            showSchedulesList = value
            tvSchedules.Visible = value
            ExpandableSplitter1.Visible = value
            pnlInfo.Visible = value
        End Set
    End Property


    Public Sub DrawScheduleHistory(ByVal sType As clsMarsScheduler.enScheduleType, _
    Optional ByVal nID As Integer = 0, _
    Optional ByVal Filter As Boolean = False)

        Try
            Dim oRs As ADODB.Recordset
            Dim SQL As String
            Dim sWhere As String
            Dim sCol As String
            Dim sTable As String
            Dim sAnd As String
            Dim oRs1 As ADODB.Recordset
            Dim oRs2 As ADODB.Recordset
            Dim oFreq, oReport, oHistory, oRec As DevComponents.AdvTree.Node
            Dim nObjectID As Integer
            Dim nStatus As Integer
            Dim nodeImage As Image

            Select Case sType
                Case clsMarsScheduler.enScheduleType.REPORT
                    sCol = "ReportID"
                    sTable = "ReportAttr"
                    sAnd = " AND R.ReportID = " & nID

                    Dim rpt As cssreport = New cssreport(nID)

                    nodeImage = resizeImage(My.Resources.document_chart2, 16, 16)

                    rpt = Nothing
                Case clsMarsScheduler.enScheduleType.PACKAGE
                    sCol = "PackID"
                    sTable = "PackageAttr"
                    sAnd = " AND R.PackID = " & nID

                    Dim pack As Package = New Package(nID)

                    nodeImage = resizeImage(My.Resources.box2, 16, 16)
                Case clsMarsScheduler.enScheduleType.AUTOMATION
                    sCol = "AutoID"
                    sTable = "AutomationAttr"
                    sAnd = " AND R.AutoID = " & nID

                    nodeImage = resizeImage(My.Resources.document_gear2, 16, 16)
                Case clsMarsScheduler.enScheduleType.SMARTFOLDER
                    sCol = "SmartID"
                    sTable = "SmartFolders"
                    sAnd = "AND R.SmartID = " & nID
                    nodeImage = resizeImage(Image.FromFile(getAssetLocation("folder_cubes.png")), 16, 16)
                Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                    sCol = "EventPackID"
                    sTable = "EventPackageAttr"
                    sAnd = "AND R.EventPackID = " & nID
                    nodeImage = resizeImage(My.Resources.cube_molecule2, 16, 16)
                Case clsMarsScheduler.enScheduleType.NONE
                    Return
            End Select

            If nID = 0 Then
                sWhere = " WHERE " & sCol & " <> 0"

                If Filter = False Then
                    sAnd = String.Empty
                Else
                    sAnd = " AND Status = 1"
                End If

            Else
                sWhere = " WHERE " & sCol & " = " & nID

                If Filter = True Then
                    sAnd &= " AND Status = 1"
                End If
            End If

            SQL = "SELECT DISTINCT(Frequency) FROM ScheduleAttr " & sWhere

            If Filter = True Then
                SQL &= " AND Status =1"
            End If

            oRs = clsMarsData.GetData(SQL)

            tvSchedules.BeginUpdate()
            tvSchedules.Nodes.Clear()

            '//create the images
            Dim imagePath As String = IO.Path.Combine(Application.StartupPath, "Assets")
            Dim frequencyImage As Image = resizeImage(Image.FromFile(IO.Path.Combine(imagePath, "calendar.png")), 16, 16)
            Dim reccurenceImage As Image = resizeImage(Image.FromFile(IO.Path.Combine(imagePath, "about.png")), 16, 16)
            Dim successImage As Image = resizeImage(Image.FromFile(IO.Path.Combine(imagePath, "check.png")), 16, 16)
            Dim failImage As Image = resizeImage(Image.FromFile(IO.Path.Combine(imagePath, "delete.png")), 16, 16)
            Dim warningImage As Image = resizeImage(Image.FromFile(IO.Path.Combine(imagePath, "warning.png")), 16, 16)
            Dim startedImage As Image = resizeImage(Image.FromFile(IO.Path.Combine(imagePath, "date_time.png")), 16, 16)

            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                Do While oRs.EOF = False
                    oFreq = New Node()
                    oFreq.Text = oRs("frequency").Value
                    oFreq.Image = frequencyImage
                    oFreq.Tag = "Frequency"

                    tvSchedules.Nodes.Add(oFreq)

                    Select Case sType
                        Case clsMarsScheduler.enScheduleType.REPORT
                            SQL = "SELECT ReportTitle, R.ReportID, NextRun, Status FROM " & _
                                "ReportAttr R " & _
                                "INNER JOIN " & _
                                "ScheduleAttr S " & _
                                "ON R.ReportID = S.ReportID " & _
                                "WHERE R.PackID = 0 AND Frequency = '" & oFreq.Text & "'" & sAnd
                        Case clsMarsScheduler.enScheduleType.PACKAGE
                            SQL = "SELECT PackageName, R.PackID, NextRun, Status FROM " & _
                                "PackageAttr R " & _
                                "INNER JOIN " & _
                                "ScheduleAttr S " & _
                                "ON R.PackID = S.PackID " & _
                                "WHERE Frequency = '" & oFreq.Text & "'" & sAnd
                        Case clsMarsScheduler.enScheduleType.AUTOMATION
                            SQL = "SELECT AutoName, R.AutoID, NextRun, Status FROM " & _
                                "AutomationAttr R " & _
                                "INNER JOIN " & _
                                "ScheduleAttr S " & _
                                "ON R.AutoID = S.AutoID " & _
                                "WHERE Frequency = '" & oFreq.Text & "'" & sAnd
                        Case clsMarsScheduler.enScheduleType.SMARTFOLDER
                            SQL = "SELECT SmartName, R.SmartID, NextRun, Status FROM " & _
                            "SmartFolders R " & _
                            "INNER JOIN ScheduleAttr S " & _
                            "ON R.SmartID = S.SmartID " & _
                            "WHERE Frequency = '" & oFreq.Text & "'" & sAnd
                        Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                            SQL = "SELECT PackageName, R.EventPackID, NextRun,Status FROM " & _
                            "EventPackageAttr R " & _
                            "INNER JOIN ScheduleAttr S " & _
                            "ON R.EventPackID = S.EventPackID " & _
                            "WHERE Frequency = '" & oFreq.Text & "'" & sAnd
                    End Select

                    oRs1 = clsMarsData.GetData(SQL)

                    Do While oRs1.EOF = False
                        oReport = New Node(oRs1(0).Value.ToString) '//the schedule name
                        nObjectID = oRs1(1).Value '//the id of the schedule

                        Select Case m_scheduleType
                            Case clsMarsScheduler.enScheduleType.REPORT
                                Dim rpt As cssreport = New cssreport(nObjectID)
                                oReport.Image = resizeImage(rpt.reportImage, 16, 16)
                            Case clsMarsScheduler.enScheduleType.PACKAGE
                                Dim pack As Package = New Package(nObjectID)
                                oReport.Image = resizeImage(pack.packageImage, 16, 16)
                            Case Else
                                oReport.Image = nodeImage
                        End Select

                        Try
                            nStatus = oRs1("status").Value '//the status
                        Catch ex As Exception
                            nStatus = 1
                        End Try

                        If nStatus = 0 Then
                            oReport.Image = MakeGrayscale3(oReport.Image)
                        End If

                        oReport.Tag = nObjectID
                        oReport.DataKey = nObjectID

                        oFreq.Nodes.Add(oReport)

                        oRs1.MoveNext()
                    Loop

                    oRs1.Close()

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            Dim oNode As Node

            For Each oNode In tvSchedules.Nodes
                If oNode.Tag <> "History" Then oNode.Expand()
            Next

            tvSchedules.EndUpdate()

            Try
                If tvSchedules.Nodes(0).Nodes.Count > 0 Then
                    tvSchedules.SelectedNode = tvSchedules.Nodes(0).Nodes(0)
                End If
            Catch : End Try
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Public Sub DrawScheduleHistory()
        Try
            Dim oRs As ADODB.Recordset
            Dim SQL As String
            Dim sWhere As String
            Dim sCol As String
            Dim sTable As String
            Dim sAnd As String
            Dim oRs1 As ADODB.Recordset
            Dim oRs2 As ADODB.Recordset
            Dim oFreq, oReport, oHistory, oRec As DevComponents.AdvTree.Node
            Dim nObjectID As Integer
            Dim nStatus As Integer
            Dim nodeImage As Image

            If scheduleType = clsMarsScheduler.enScheduleType.NONE Then Return

            Select Case m_scheduleType
                Case clsMarsScheduler.enScheduleType.REPORT
                    sCol = "ReportID"
                    sTable = "ReportAttr"
                    sAnd = " AND R.ReportID = " & objectID

                    nodeImage = resizeImage(My.Resources.document_attachment2, 16, 16)                  
                Case clsMarsScheduler.enScheduleType.PACKAGE
                    sCol = "PackID"
                    sTable = "PackageAttr"
                    sAnd = " AND R.PackID = " & objectID

                    nodeImage = resizeImage(My.Resources.box2, 16, 16)
                Case clsMarsScheduler.enScheduleType.AUTOMATION
                    sCol = "AutoID"
                    sTable = "AutomationAttr"
                    sAnd = " AND R.AutoID = " & objectID

                    nodeImage = resizeImage(My.Resources.document_gear2, 16, 16)
                Case clsMarsScheduler.enScheduleType.SMARTFOLDER
                    sCol = "SmartID"
                    sTable = "SmartFolders"
                    sAnd = "AND R.SmartID = " & objectID
                    nodeImage = resizeImage(My.Resources.dynamic_package1, 16, 16)
                Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                    sCol = "EventPackID"
                    sTable = "EventPackageAttr"
                    sAnd = "AND R.EventPackID = " & objectID
                    nodeImage = resizeImage(My.Resources.cube_molecule2, 16, 16)
            End Select

            If objectID = 0 Then
                sWhere = " WHERE " & sCol & " <> 0"

                If m_filter = False Then
                    sAnd = String.Empty
                Else
                    sAnd = " AND Status = 1"
                End If

            Else
                sWhere = " WHERE " & sCol & " = " & objectID

                If m_filter = True Then
                    sAnd &= " AND Status = 1"
                End If
            End If

            SQL = "SELECT DISTINCT(Frequency) FROM ScheduleAttr " & sWhere

            If m_filter = True Then
                SQL &= " AND Status =1"
            End If

            oRs = clsMarsData.GetData(SQL)

            tvSchedules.BeginUpdate()

            tvSchedules.Nodes.Clear()

            '//create the images
            Dim imagePath As String = IO.Path.Combine(Application.StartupPath, "Assets")
            Dim frequencyImage As Image = resizeImage(Image.FromFile(IO.Path.Combine(imagePath, "calendar.png")), 16, 16)
            Dim reccurenceImage As Image = resizeImage(Image.FromFile(IO.Path.Combine(imagePath, "about.png")), 16, 16)
            Dim successImage As Image = resizeImage(Image.FromFile(IO.Path.Combine(imagePath, "check.png")), 16, 16)
            Dim failImage As Image = resizeImage(Image.FromFile(IO.Path.Combine(imagePath, "delete.png")), 16, 16)
            Dim warningImage As Image = resizeImage(Image.FromFile(IO.Path.Combine(imagePath, "warning.png")), 16, 16)
            Dim startedImage As Image = resizeImage(Image.FromFile(IO.Path.Combine(imagePath, "date_time.png")), 16, 16)

            If Not oRs Is Nothing And oRs.EOF = False Then
                Do While oRs.EOF = False
                    oFreq = New Node()
                    oFreq.Text = oRs("frequency").Value
                    oFreq.Image = frequencyImage
                    oFreq.Tag = "Frequency"

                    tvSchedules.Nodes.Add(oFreq)

                    Select Case m_scheduleType
                        Case clsMarsScheduler.enScheduleType.REPORT
                            SQL = "SELECT ReportTitle, R.ReportID, NextRun, Status FROM " & _
                                "ReportAttr R " & _
                                "INNER JOIN " & _
                                "ScheduleAttr S " & _
                                "ON R.ReportID = S.ReportID " & _
                                "WHERE R.PackID = 0 AND Frequency = '" & oFreq.Text & "'" & sAnd
                        Case clsMarsScheduler.enScheduleType.PACKAGE
                            SQL = "SELECT PackageName, R.PackID, NextRun, Status FROM " & _
                                "PackageAttr R " & _
                                "INNER JOIN " & _
                                "ScheduleAttr S " & _
                                "ON R.PackID = S.PackID " & _
                                "WHERE Frequency = '" & oFreq.Text & "'" & sAnd
                        Case clsMarsScheduler.enScheduleType.AUTOMATION
                            SQL = "SELECT AutoName, R.AutoID, NextRun, Status FROM " & _
                                "AutomationAttr R " & _
                                "INNER JOIN " & _
                                "ScheduleAttr S " & _
                                "ON R.AutoID = S.AutoID " & _
                                "WHERE Frequency = '" & oFreq.Text & "'" & sAnd
                        Case clsMarsScheduler.enScheduleType.SMARTFOLDER
                            SQL = "SELECT SmartName, R.SmartID, NextRun, Status FROM " & _
                            "SmartFolders R " & _
                            "INNER JOIN ScheduleAttr S " & _
                            "ON R.SmartID = S.SmartID " & _
                            "WHERE Frequency = '" & oFreq.Text & "'" & sAnd
                        Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                            SQL = "SELECT PackageName, R.EventPackID, NextRun,Status FROM " & _
                            "EventPackageAttr R " & _
                            "INNER JOIN ScheduleAttr S " & _
                            "ON R.EventPackID = S.EventPackID " & _
                            "WHERE Frequency = '" & oFreq.Text & "'" & sAnd
                    End Select

                    oRs1 = clsMarsData.GetData(SQL)

                    Do While oRs1.EOF = False
                        oReport = New Node(oRs1(0).Value.ToString) ' oFreq.Nodes.Add(oRs1(0).Value)
                        nObjectID = oRs1(1).Value

                        Select Case m_scheduleType
                            Case clsMarsScheduler.enScheduleType.REPORT
                                Dim rpt As cssreport = New cssreport(nObjectID)
                                oReport.Image = resizeImage(rpt.reportImage, 16, 16)
                            Case clsMarsScheduler.enScheduleType.PACKAGE
                                Dim pack As Package = New Package(nObjectID)
                                oReport.Image = resizeImage(pack.packageImage, 16, 16)
                            Case Else
                                oReport.Image = nodeImage
                        End Select

                        oFreq.Nodes.Add(oReport)

                        Try
                            nStatus = oRs1("status").Value
                        Catch ex As Exception
                            nStatus = 1
                        End Try

                        If nStatus = 0 Then
                            oReport.Image = MakeGrayscale3(oReport.Image)
                        End If

                        oReport.Tag = nObjectID
                        oReport.DataKey = nObjectID


                        oRs1.MoveNext()
                    Loop

                    oRs1.Close()

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            Dim oNode As Node

            For Each oNode In tvSchedules.Nodes
                If oNode.Tag = "Frequency" Then
                    oNode.Expanded = True
                    expandMyKids(oNode)
                End If
            Next

            tvSchedules.EndUpdate()

            Try
                If tvSchedules.Nodes(0).Nodes.Count > 0 Then
                    tvSchedules.SelectedNode = tvSchedules.Nodes(0).Nodes(0)
                End If
            Catch : End Try
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub
    Sub expandMyKids(ByVal tnode As Node)
        For Each kid As Node In tnode.Nodes
            kid.Expanded = True
        Next
    End Sub

    Private Function IsPartialSuccess(ByVal historyID As Integer) As Boolean
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim results As ArrayList = New ArrayList

        SQL = "SELECT * FROM historydetailattr WHERE historyid =" & historyID

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                results.Add(IsNull(oRs("success").Value, 1))
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        'check to see we have mixed results
        If results.Contains("0") And results.Contains("1") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub tvHistory_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.F5 Then
            DrawScheduleHistory()
        End If
    End Sub

    Public Sub reset()
        tvDetails.Nodes.Clear()
        tvSchedules.Nodes.Clear()
        lblInfo.Text = ""
    End Sub
    Public Sub DrawEventHistory()


        Dim SQL As String
        Dim sWhere As String = String.Empty
        Dim oRs As ADODB.Recordset
        Dim oChild As ADODB.Recordset
        Dim oMain As ADODB.Recordset

        tvSchedules.Nodes.Clear()

        If objectID > 0 Then
            sWhere = " WHERE EventID =" & objectID
        End If

        Dim imagePath As String = IO.Path.Combine(Application.StartupPath, "Assets")
        Dim frequencyImage As Image = resizeImage(Image.FromFile(IO.Path.Combine(imagePath, "calendar.png")), 16, 16)

        Dim oFreq As Node = New Node()
        oFreq.Text = "Event-Based"
        oFreq.Image = frequencyImage
        oFreq.Tag = "Frequency"

        tvSchedules.Nodes.Add(oFreq)

        If objectID = 0 Then
            SQL = "SELECT EventID, EventName,Status FROM EventAttr6 WHERE (PackID IS NULL OR PackID = 0)"

            If m_filter = True Then
                SQL &= " AND Status = 1"
            End If
        Else
            SQL = "SELECT EventID, EventName,Status FROM EventAttr6 WHERE EventID =" & objectID

            If m_filter = True Then
                SQL &= " AND Status =1"
            End If
        End If

        '//create the images
        Dim eventImage As Image = resizeImage(My.Resources.document_atom1, 16, 16)
        Dim successImage As Image = resizeImage(Image.FromFile(IO.Path.Combine(imagePath, "check.png")), 16, 16)
        Dim failImage As Image = resizeImage(Image.FromFile(IO.Path.Combine(imagePath, "delete.png")), 16, 16)

        oMain = clsMarsData.GetData(SQL)

        Try
            Do While oMain.EOF = False

                Dim eventNode As Node = New Node
                eventNode.Text = oMain(1).Value
                eventNode.Image = eventImage


                Dim status As Boolean

                status = Convert.ToInt16(IsNull(oMain("status").Value, 1))

                If status = False Then
                    eventNode.Image = MakeGrayscale3(eventNode.Image)
                End If

                objectID = oMain(0).Value

                eventNode.Tag = objectID
                eventNode.DataKey = objectID

                oFreq.Nodes.Add(eventNode)

                oMain.MoveNext()
            Loop

            oMain.Close()

            tvSchedules.Nodes(0).Expand()

            Try
                If tvSchedules.Nodes(0).Nodes.Count > 0 Then
                    tvSchedules.SelectedNode = tvSchedules.Nodes(0).Nodes(0)
                End If
            Catch : End Try
        Catch : End Try
    End Sub

    Private Sub optAsc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optAsc.CheckedChanged
        grSort.Enabled = False

        Dim oSchedule As New clsMarsScheduler

        If optDesc.Checked = True Then
            Me.DrawScheduleHistory(m_scheduleType, m_objectID, False)
        Else
            Me.DrawScheduleHistory(m_scheduleType, m_objectID, False)
        End If

        grSort.Enabled = True
    End Sub

    Private Sub cmdClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClear.Click
        If MessageBox.Show("Delete all history for this schedule?", Application.ProductName, MessageBoxButtons.YesNo, _
    MessageBoxIcon.Question) = DialogResult.Yes Then
            Dim oData As New clsMarsData
            Dim keycolumn As String
            Dim eventBased As Boolean = False

            Select Case m_scheduleType
                Case clsMarsScheduler.enScheduleType.REPORT
                    keycolumn = "reportid"
                Case clsMarsScheduler.enScheduleType.PACKAGE
                    keycolumn = "packid"
                Case clsMarsScheduler.enScheduleType.AUTOMATION
                    keycolumn = "autoid"
                Case clsMarsScheduler.enScheduleType.SMARTFOLDER
                    keycolumn = "smartid"
                Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                    keycolumn = "eventpackid"
                Case clsMarsScheduler.enScheduleType.EVENTBASED
                    keycolumn = "eventid"
                    eventBased = True
            End Select

            If eventBased Then
                clsMarsData.WriteData("DELETE FROM eventhistory WHERE eventid =" & m_objectID, False)
            Else
                clsMarsData.WriteData("DELETE FROM ScheduleHistory WHERE " & keycolumn & " =" & m_objectID, False)
            End If


            _Delay(1)

            DrawScheduleHistory(m_scheduleType, m_objectID, m_filter)
        End If
    End Sub

    Private Sub tvSchedules_AfterNodeSelect(sender As Object, e As DevComponents.AdvTree.AdvTreeNodeEventArgs) Handles tvSchedules.AfterNodeSelect
        pgLoading.Visible = True

        tvDetails.Nodes.Clear()

        If scheduleType = clsMarsScheduler.enScheduleType.NONE Then Return

        If tvSchedules.SelectedNode Is Nothing Then Return

        If tvSchedules.SelectedNode.Parent Is Nothing Then Return

        Dim sCol, sTable, sAnd As String
        Dim nodeImage As Image
        Dim tip As DevComponents.DotNetBar.SuperTooltip = New DevComponents.DotNetBar.SuperTooltip
        Dim info As DevComponents.DotNetBar.SuperTooltipInfo

        objectID = tvSchedules.SelectedNode.DataKey

        Select Case m_scheduleType
            Case clsMarsScheduler.enScheduleType.REPORT
                sCol = "ReportID"
                sTable = "ReportAttr"
                sAnd = " AND R.ReportID = " & objectID

                If clsMarsData.IsScheduleBursting(objectID) Then
                    nodeImage = resizeImage(My.Resources.document_attachment2, 16, 16)
                ElseIf clsMarsData.IsScheduleDataDriven(objectID) Then
                    nodeImage = resizeImage(My.Resources.document_datadriven_schedule1, 16, 16)
                ElseIf clsMarsData.IsScheduleDynamic(objectID, "report") Then
                    nodeImage = resizeImage(My.Resources.document_pulse2, 16, 16)
                Else
                    nodeImage = resizeImage(My.Resources.document_chart2, 16, 16)
                End If

                Dim rpt As cssreport = New cssreport(objectID)
                If rpt.m_schedule.status = True Then
                    lblInfo.Text = "Next to run on " & rpt.m_schedule.nextRun
                Else
                    lblInfo.Text = "This schedule is disabled"
                End If

            Case clsMarsScheduler.enScheduleType.PACKAGE
                sCol = "PackID"
                sTable = "PackageAttr"
                sAnd = " AND R.PackID = " & objectID

                If clsMarsData.IsScheduleDataDriven(objectID, "package") Then
                    nodeImage = resizeImage(My.Resources.data_driven_package1, 16, 16)
                ElseIf clsMarsData.IsScheduleDynamic(objectID, "package") Then
                    nodeImage = resizeImage(My.Resources.dynamic_package1, 16, 16)
                Else
                    nodeImage = resizeImage(My.Resources.box2, 16, 16)
                End If

                Dim pack As Package = New Package(objectID)
                If pack.m_schedule.status = True Then
                    lblInfo.Text = "Next to run on " & pack.m_schedule.nextRun
                Else
                    lblInfo.Text = "This schedule is disabled"
                End If
            Case clsMarsScheduler.enScheduleType.AUTOMATION
                sCol = "AutoID"
                sTable = "AutomationAttr"
                sAnd = " AND R.AutoID = " & objectID

                nodeImage = resizeImage(My.Resources.document_gear2, 16, 16)

                Dim auto As Automation = New Automation(objectID)
                If auto.m_schedule.status = True Then
                    lblInfo.Text = "Next to run on " & auto.m_schedule.nextRun
                Else
                    lblInfo.Text = "This schedule is disabled"
                End If
            Case clsMarsScheduler.enScheduleType.SMARTFOLDER
                sCol = "SmartID"
                sTable = "SmartFolders"
                sAnd = "AND R.SmartID = " & objectID
                nodeImage = resizeImage(My.Resources.dynamic_package1, 16, 16)

                lblInfo.Text = ""
            Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                sCol = "EventPackID"
                sTable = "EventPackageAttr"
                sAnd = "AND R.EventPackID = " & objectID
                nodeImage = resizeImage(My.Resources.cube_molecule2, 16, 16)

                Dim pack As EventBasedPackage = New EventBasedPackage(objectID)
                If pack.m_schedule.status = True Then
                    lblInfo.Text = "Next to run on " & pack.m_schedule.nextRun
                Else
                    lblInfo.Text = "This schedule is disabled"
                End If
            Case clsMarsScheduler.enScheduleType.EVENTBASED
                Dim evt As EventBased = New EventBased(objectID)
                If evt.Status = True Then
                    lblInfo.Text = "Schedule will run when the conditions match."
                Else
                    lblInfo.Text = "This schedule is disabled"
                End If
        End Select

        Dim SQL As String
        Dim nObjectID As Integer = tvSchedules.SelectedNode.DataKey
        Dim imgList As ImageList = createImageList(New String() {"warning.png", "check.png", "delete.png"})
        Dim warningImage As Image = imgList.Images(0)
        Dim checkImage As Image = imgList.Images(1)
        Dim deleteImage As Image = imgList.Images(2)

        If tvSchedules.SelectedNode.Parent.Text <> "Event-Based" Then
            If m_historyLimit = 0 Then
                SQL = "SELECT * FROM ScheduleHistory " & _
                                "WHERE " & sCol & "=" & nObjectID & _
                                " ORDER BY EntryDate " & m_sortOrder
            Else
                SQL = "SELECT TOP " & m_historyLimit & " * FROM ScheduleHistory " & _
                                "WHERE " & sCol & "=" & nObjectID & _
                                " ORDER BY EntryDate " & m_sortOrder
            End If

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL, ADODB.CursorTypeEnum.adOpenStatic)
            Dim i As Integer = 1
            Dim total As Integer = oRs.RecordCount

            Do While oRs.EOF = False
                pgLoading.Value = (i / total) * 100

                Dim historyID As Integer = oRs(0).Value
                Dim oItem As Node
                Dim entryDate As Date = CTimeZ(IsNull(oRs("entrydate").Value), dateConvertType.READ)
                Dim startDate As Date = CTimeZ(IsNull(oRs("startdate").Value), dateConvertType.READ)
                Dim errMsg As String = IsNull(oRs("errmsg").Value)

                If IsPartialSuccess(historyID) = True Then
                    oItem = New Node(startDate)
                    oItem.Image = warningImage
                    oItem.Cells.Add(New Cell(entryDate))
                    oItem.Cells.Add(New Cell("Partial Success: " & errMsg))
                ElseIf oRs("success").Value = 1 Then
                    oItem = New Node(startDate)
                    oItem.Image = checkImage
                    oItem.Cells.Add(New Cell(entryDate))
                    oItem.Cells.Add(New Cell("Success! " & errMsg))
                Else
                    oItem = New Node(startDate)
                    oItem.Image = deleteImage
                    oItem.Cells.Add(New Cell(entryDate))
                    oItem.Cells.Add(New Cell("Failure: " & errMsg))
                End If

                info = New DevComponents.DotNetBar.SuperTooltipInfo()
                info.BodyText = "The schedule executed for " & entryDate.Subtract(startDate).TotalSeconds & " seconds and "


                If oRs("success").Value = 1 Then
                    info.BodyText &= "was successful."
                    info.Color = DevComponents.DotNetBar.eTooltipColor.Yellow
                    info.BodyImage = checkImage
                    info.HeaderText = "Details"
                Else
                    info.BodyText &= "failed. Double-click the entry to view more information."
                    info.Color = DevComponents.DotNetBar.eTooltipColor.Red
                    info.BodyImage = deleteImage
                    info.HeaderText = "Details"
                End If

                oItem.DataKey = historyID

                tip.SetSuperTooltip(oItem, info)

                tvDetails.Nodes.Add(oItem)

                If scheduleType = clsMarsScheduler.enScheduleType.EVENTPACKAGE Then
                    SQL = "SELECT h.LastFired AS EntryDate,h.Status AS Success,h.ErrMsg,h.StartDate, r.EventName AS ReportTitle FROM EventHistory h INNER JOIN EventAttr6 r ON h.EventID = r.EventID WHERE MasterHistoryID = " & historyID
                Else
                    SQL = "SELECT h.EntryDate,h.Success,h.ErrMsg,h.StartDate, r.ReportTitle FROM HistoryDetailAttr h INNER JOIN ReportAttr r ON h.ReportID = r.ReportID WHERE HistoryID = " & historyID
                End If

                Dim oRs3 As ADODB.Recordset = clsMarsData.GetData(SQL)
                Dim bPartialSuccess As Boolean = False

                If oRs3.EOF = False Then
                    oItem.NodesColumns.Add(New ColumnHeader("Report Title"))
                    oItem.NodesColumns.Add(New ColumnHeader("Started"))
                    oItem.NodesColumns.Add(New ColumnHeader("Finished"))
                    oItem.NodesColumns.Add(New ColumnHeader("Details"))


                    For Each cx As ColumnHeader In oItem.NodesColumns
                        cx.Width.AutoSize = True
                    Next

                    Dim reportTitle As String
                    Dim started As String
                    Dim finished As String
                    Dim details As String

                    Do While oRs3.EOF = False

                        Dim detailNode As Node

                        started = ConTime(oRs3("startdate").Value)
                        finished = ConTime(oRs3("entrydate").Value)
                        reportTitle = oRs3("reporttitle").Value
                        details = oRs3("errmsg").Value

                        If oRs3("success").Value = 1 Then
                            bPartialSuccess = True
                            detailNode = New Node(reportTitle) 'ConTime(oRs3("entrydate").Value) & Space(10) & oRs3("reporttitle").Value & " - Success: " & IsNull(oRs3("errmsg").Value))
                            detailNode.Image = checkImage
                            detailNode.Cells.Add(New Cell(started))
                            detailNode.Cells.Add(New Cell(finished))
                            detailNode.Cells.Add(New Cell("Success! " & details))
                            oItem.Nodes.Add(detailNode)
                        Else

                            detailNode = New Node(reportTitle) 'ConTime(oRs3("entrydate").Value) & Space(10) & oRs3("reporttitle").Value & " - Success: " & IsNull(oRs3("errmsg").Value))
                            detailNode.Image = deleteImage
                            detailNode.Cells.Add(New Cell(started))
                            detailNode.Cells.Add(New Cell(finished))
                            detailNode.Cells.Add(New Cell("Failure: " & details))
                            oItem.Nodes.Add(detailNode)
                        End If

                        oRs3.MoveNext()
                    Loop
                End If
                oRs3.Close()

                oItem.Tag = "History"
                i += 1
                oRs.MoveNext()
            Loop

            oRs.Close()
        Else

            objectID = tvSchedules.SelectedNode.DataKey

            If m_historyLimit = 0 Then
                SQL = "SELECT * FROM EventHistory WHERE EventID =" & objectID & " ORDER BY LastFired " & m_sortOrder
            Else
                SQL = "SELECT TOP " & m_historyLimit & " * FROM EventHistory WHERE EventID =" & objectID & " ORDER BY LastFired " & m_sortOrder
            End If

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL, ADODB.CursorTypeEnum.adOpenStatic)

            Try
                Dim i As Integer = 0
                Dim total As Integer = oRs.RecordCount
                Do While oRs.EOF = False
                    pgLoading.Value = (i / total) * 100
                    Dim oItem As Node
                    Dim entryDate As Date = CTimeZ(IsNull(oRs("lastfired").Value), dateConvertType.READ)
                    Dim startDate As Date = CTimeZ(IsNull(oRs("startdate").Value), dateConvertType.READ)
                    Dim errMsg As String = IsNull(oRs("errmsg").Value)

                    If oRs("status").Value = 1 Then
                        oItem = New Node(startDate)
                        oItem.Cells.Add(New Cell(entryDate))
                        oItem.Cells.Add(New Cell("Success: " & errMsg))
                        oItem.Image = imgList.Images(1)
                    Else
                        oItem = New Node(startDate)
                        oItem.Cells.Add(New Cell(entryDate))
                        oItem.Cells.Add(New Cell("Failure: " & errMsg))
                        oItem.Image = imgList.Images(2)
                    End If

                    info = New DevComponents.DotNetBar.SuperTooltipInfo()
                    info.BodyText = "The schedule executed for " & entryDate.Subtract(startDate).TotalSeconds & " seconds and "

                    If errMsg <> "" Then
                        If oRs("status").Value = 1 Then
                            info.BodyText &= "was successful."
                            info.Color = DevComponents.DotNetBar.eTooltipColor.Green
                            info.BodyImage = checkImage
                            info.HeaderText = "Details"
                        Else
                            info.BodyText &= "failed. Double-click me to view more information."
                            info.Color = DevComponents.DotNetBar.eTooltipColor.Red
                            info.BodyImage = deleteImage
                            info.HeaderText = "Details"
                        End If
                    End If

                    tip.SetSuperTooltip(oItem, info)

                    tvDetails.Nodes.Add(oItem)
                    i += 1
                    oRs.MoveNext()
                Loop
                oRs.Close()
            Catch : End Try

        End If
        pgLoading.Visible = False
    End Sub

    Private Sub tvDetails_DoubleClick(sender As Object, e As System.EventArgs) Handles tvDetails.DoubleClick
        If tvDetails.SelectedNode Is Nothing Then Return

        Dim hisView As frmHistoryDetail = New frmHistoryDetail
        Dim nn As Node = tvDetails.SelectedNode

        If nn.Parent Is Nothing Then
            hisView.showDetails(nn.Text, nn.Cells(1).Text, nn.Cells(2).Text, nn.DataKey)
        Else
            hisView.showDetails(nn.Cells(1).Text, nn.Cells(2).Text, nn.Cells(3).Text, nn.DataKey)
        End If
    End Sub

    Private Sub cmdRefresh_Click(sender As System.Object, e As System.EventArgs) Handles cmdRefresh.Click
        Me.reset()

        If m_scheduleType = clsMarsScheduler.enScheduleType.EVENTBASED Then
            DrawEventHistory()
        Else
            DrawScheduleHistory()
        End If

    End Sub

    Private Sub btnLoadAll_Click(sender As System.Object, e As System.EventArgs) Handles btnLoadAll.Click
        m_historyLimit = 0

        tvSchedules_AfterNodeSelect(Nothing, Nothing)
    End Sub
End Class
