﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucMultiValueParameter
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim CheckBoxProperties1 As PresentationControls.CheckBoxProperties = New PresentationControls.CheckBoxProperties()
        Me.optDefaultValue = New System.Windows.Forms.RadioButton()
        Me.optNullValue = New System.Windows.Forms.RadioButton()
        Me.optSpecifiedValue = New System.Windows.Forms.RadioButton()
        Me.cmbValue = New PresentationControls.CheckBoxComboBox()
        Me.lblParameterName = New System.Windows.Forms.Label()
        Me.optAllValues = New System.Windows.Forms.RadioButton()
        Me.btnOK = New DevComponents.DotNetBar.ButtonX()
        Me.chkSelectAll = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.btnAddValue = New DevComponents.DotNetBar.ButtonX()
        Me.SuspendLayout()
        '
        'optDefaultValue
        '
        Me.optDefaultValue.AutoSize = True
        Me.optDefaultValue.Location = New System.Drawing.Point(91, 81)
        Me.optDefaultValue.Name = "optDefaultValue"
        Me.optDefaultValue.Size = New System.Drawing.Size(89, 17)
        Me.optDefaultValue.TabIndex = 3
        Me.optDefaultValue.Text = "Default Value"
        Me.optDefaultValue.UseVisualStyleBackColor = True
        '
        'optNullValue
        '
        Me.optNullValue.AutoSize = True
        Me.optNullValue.Location = New System.Drawing.Point(196, 59)
        Me.optNullValue.Name = "optNullValue"
        Me.optNullValue.Size = New System.Drawing.Size(73, 17)
        Me.optNullValue.TabIndex = 4
        Me.optNullValue.Text = "Null Value"
        Me.optNullValue.UseVisualStyleBackColor = True
        '
        'optSpecifiedValue
        '
        Me.optSpecifiedValue.AutoSize = True
        Me.optSpecifiedValue.Checked = True
        Me.optSpecifiedValue.Location = New System.Drawing.Point(91, 58)
        Me.optSpecifiedValue.Name = "optSpecifiedValue"
        Me.optSpecifiedValue.Size = New System.Drawing.Size(99, 17)
        Me.optSpecifiedValue.TabIndex = 5
        Me.optSpecifiedValue.TabStop = True
        Me.optSpecifiedValue.Text = "Specified Value"
        Me.optSpecifiedValue.UseVisualStyleBackColor = True
        '
        'cmbValue
        '
        CheckBoxProperties1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmbValue.CheckBoxProperties = CheckBoxProperties1
        Me.cmbValue.DisplayMemberSingleItem = ""
        Me.cmbValue.FormattingEnabled = True
        Me.cmbValue.Location = New System.Drawing.Point(15, 32)
        Me.cmbValue.Name = "cmbValue"
        Me.cmbValue.Size = New System.Drawing.Size(229, 21)
        Me.cmbValue.Sorted = True
        Me.cmbValue.TabIndex = 6
        '
        'lblParameterName
        '
        Me.lblParameterName.AutoSize = True
        Me.lblParameterName.Location = New System.Drawing.Point(12, 16)
        Me.lblParameterName.Name = "lblParameterName"
        Me.lblParameterName.Size = New System.Drawing.Size(92, 13)
        Me.lblParameterName.TabIndex = 7
        Me.lblParameterName.Text = "[Parameter Name]"
        '
        'optAllValues
        '
        Me.optAllValues.AutoSize = True
        Me.optAllValues.Location = New System.Drawing.Point(196, 82)
        Me.optAllValues.Name = "optAllValues"
        Me.optAllValues.Size = New System.Drawing.Size(126, 17)
        Me.optAllValues.TabIndex = 3
        Me.optAllValues.Text = "All Values (at runtime)"
        Me.optAllValues.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnOK.Location = New System.Drawing.Point(294, 32)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(39, 21)
        Me.btnOK.TabIndex = 8
        Me.btnOK.Text = "OK"
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        '
        '
        '
        Me.chkSelectAll.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkSelectAll.Location = New System.Drawing.Point(15, 59)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(68, 15)
        Me.chkSelectAll.TabIndex = 9
        Me.chkSelectAll.Text = "Select All"
        '
        'btnAddValue
        '
        Me.btnAddValue.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnAddValue.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnAddValue.Location = New System.Drawing.Point(251, 32)
        Me.btnAddValue.Name = "btnAddValue"
        Me.btnAddValue.Size = New System.Drawing.Size(39, 21)
        Me.btnAddValue.TabIndex = 8
        Me.btnAddValue.Text = "Add"
        '
        'ucMultiValueParameter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.chkSelectAll)
        Me.Controls.Add(Me.btnAddValue)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.lblParameterName)
        Me.Controls.Add(Me.cmbValue)
        Me.Controls.Add(Me.optAllValues)
        Me.Controls.Add(Me.optDefaultValue)
        Me.Controls.Add(Me.optNullValue)
        Me.Controls.Add(Me.optSpecifiedValue)
        Me.Name = "ucMultiValueParameter"
        Me.Size = New System.Drawing.Size(339, 107)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents optDefaultValue As System.Windows.Forms.RadioButton
    Friend WithEvents optNullValue As System.Windows.Forms.RadioButton
    Friend WithEvents optSpecifiedValue As System.Windows.Forms.RadioButton
    Public WithEvents cmbValue As PresentationControls.CheckBoxComboBox
    Friend WithEvents lblParameterName As System.Windows.Forms.Label
    Friend WithEvents optAllValues As System.Windows.Forms.RadioButton
    Public WithEvents btnOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents chkSelectAll As DevComponents.DotNetBar.Controls.CheckBoxX
    Public WithEvents btnAddValue As DevComponents.DotNetBar.ButtonX

End Class
