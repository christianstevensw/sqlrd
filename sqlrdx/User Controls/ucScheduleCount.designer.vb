﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucScheduleCount
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.pgCount = New DevComponents.DotNetBar.Controls.ProgressBarX()
        Me.SuspendLayout()
        '
        'LabelX1
        '
        '
        '
        '
        Me.LabelX1.BackgroundStyle.Class = ""
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Dock = System.Windows.Forms.DockStyle.Top
        Me.LabelX1.Location = New System.Drawing.Point(0, 0)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(497, 23)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "Number of Reports"
        Me.LabelX1.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'pgCount
        '
        '
        '
        '
        Me.pgCount.BackgroundStyle.Class = ""
        Me.pgCount.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.pgCount.ChunkColor = System.Drawing.Color.White
        Me.pgCount.ChunkColor2 = System.Drawing.Color.White
        Me.pgCount.ChunkGradientAngle = 45
        Me.pgCount.Dock = System.Windows.Forms.DockStyle.Top
        Me.pgCount.Location = New System.Drawing.Point(0, 23)
        Me.pgCount.Maximum = 50
        Me.pgCount.Name = "pgCount"
        Me.pgCount.Size = New System.Drawing.Size(497, 16)
        Me.pgCount.TabIndex = 1
        Me.pgCount.Text = "ProgressBarX1"
        Me.pgCount.Value = 25
        '
        'ucScheduleCount
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me.pgCount)
        Me.Controls.Add(Me.LabelX1)
        Me.Name = "ucScheduleCount"
        Me.Size = New System.Drawing.Size(497, 42)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents pgCount As DevComponents.DotNetBar.Controls.ProgressBarX

End Class
