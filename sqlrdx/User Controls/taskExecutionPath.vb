﻿Public Class taskExecutionPath
    Dim ep As ErrorProvider = New ErrorProvider

    Public ReadOnly Property taskRunWhen As String
        Get
            Return cmbRunWhen.Text
        End Get
    End Property

    
    Private Sub cmbRunWhen_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbRunWhen.SelectedIndexChanged
        If cmbRunWhen.Text = "BEFORE" Then
            GroupBox2.Enabled = False
        ElseIf cmbRunWhen.Text = "BOTH" Then
            GroupBox2.Enabled = True
        Else
            GroupBox2.Enabled = True
        End If

        SetError(cmbRunWhen, "")
    End Sub

    Public Function LoadTaskRunWhen(Optional ByVal nTaskID As Integer = 0) As String()
        Dim oData As New clsMarsData
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        If nTaskID > 0 Then
            SQL = "SELECT RunWhen, AfterType FROM Tasks WHERE TaskID = " & nTaskID

            oRs = clsMarsData.GetData(SQL)

            If Not oRs Is Nothing And oRs.EOF = False Then
                cmbRunWhen.Text = IsNull(oRs("runwhen").Value)

                If cmbRunWhen.Text <> "BEFORE" Then
                    Select Case IsNull(oRs("aftertype").Value)
                        Case "Delivery"
                            optDelivery.Checked = True
                        Case "Production"
                            optProduction.Checked = True
                    End Select
                End If
            End If

            oRs.Close()
        Else
            SQL = "SELECT MAX(TaskID) FROM Tasks"

            oRs = clsMarsData.GetData(SQL)

            If Not oRs Is Nothing And oRs.EOF = False Then
                nTaskID = oRs(0).Value
            End If

            oRs.Close()
        End If

        oData.DisposeRecordset(oRs)
    End Function

    Public Function setTaskRunWhen(ByVal taskID As Integer) As String()
        Dim sVals(1) As String
        Dim sVerb As String
        Dim SQL As String

        If cmbRunWhen.Text = "BEFORE" Then
            sVals(0) = "BEFORE"
            sVals(1) = ""
        Else
            If optDelivery.Checked = True Then
                sVerb = optDelivery.Tag
            ElseIf optProduction.Checked = True Then
                sVerb = optProduction.Tag
            End If


            sVals(0) = cmbRunWhen.Text
            sVals(1) = sVerb

        End If

        Sql = "RunWhen = '" & sVals(0) & "'," & _
            "AfterType = '" & sVals(1) & "'"

        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & taskID

        clsMarsData.WriteData(Sql)

        Return sVals
    End Function

    Private Sub taskExecutionPath_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If cmbRunWhen.Text = "" Then cmbRunWhen.Text = "AFTER"
    End Sub
End Class
