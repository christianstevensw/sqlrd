﻿Public Class ucBlankReportX

    Dim customDSN, customUserID, customPassword As String
    Public customBlankQuery As String

    Private m_blankID As Integer
    Public Property blankID() As Integer
        Get
            Return m_blankID
        End Get
        Set(ByVal value As Integer)
            m_blankID = value
        End Set
    End Property

    

    Public Property m_showAllReportsBlankForTasks As Boolean
        Get
            Return chkAllReports.Visible
        End Get
        Set(value As Boolean)
            chkAllReports.Visible = value
        End Set
    End Property
    ''' <summary>
    ''' creates a new ID for the blank alert and sets it for all the tasks that are created
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function createNewBlankAlert() As Integer
        blankID = clsMarsData.CreateDataID("blankreportalert", "blankid")

        ' UcBlankReportTasks.ScheduleID = blankID
    End Function
    Private Sub chkCheckBlankReport_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCheckBlankReport.CheckedChanged
        stabBlankReports.Enabled = chkCheckBlankReport.Checked
        chkIgnoreReport.Enabled = chkCheckBlankReport.Checked
        chkAllReports.Enabled = chkCheckBlankReport.Checked

        If chkCheckBlankReport.Checked = False Then
            chkIgnoreReport.Checked = False
        End If
    End Sub

    Private Sub optSQL_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optSQL.CheckedChanged
        If optSQL.Checked Then
            Panel1.Visible = True
            Panel1.Enabled = True
            Panel2.Visible = False

            ' If txtQuery.Text = "" Then btnBuild_Click(sender, e)
        End If
    End Sub

    Private Sub optFilesize_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optFilesize.CheckedChanged
        If optFilesize.Checked Then
            Panel1.Visible = False
            Panel2.Visible = True
        End If
    End Sub


    Private m_blankType As String
    Public Property blankType() As String
        Get
            If chkIgnoreReport.Checked = True Then
                Return "Ignore"
            Else
                Return "AlertandReport"
            End If
        End Get
        Set(ByVal value As String)
            If String.Compare(value, "Ignore", True) = 0 Then
                chkIgnoreReport.Checked = True
            End If

            m_blankType = value
        End Set
    End Property



    Public Property m_customDSN() As String
        Get
            If customDSN Is Nothing Then customDSN = ""

            Return customDSN
        End Get
        Set(ByVal value As String)
            customDSN = value
        End Set
    End Property

    Public Property m_customUserID() As String
        Get
            If customUserID Is Nothing Then customUserID = ""

            Return customUserID
        End Get
        Set(ByVal value As String)
            customUserID = value
        End Set
    End Property
    ''' <summary>
    ''' The provided must be encrypted as the property decrypts it.
    ''' </summary>
    ''' <value>The provided must be encrypted as the property decrypts it.</value>
    ''' <returns>Returns an already encrypted value</returns>
    ''' <remarks>The provided must be encrypted as the property decrypts it</remarks>
    Public Property m_customPassword() As String
        Get
            If customPassword Is Nothing Then customPassword = ""

            Return customPassword
        End Get
        Set(ByVal value As String)
            customPassword = value
        End Set
    End Property

    Public ReadOnly Property m_CustomBlankQuery() As String
        Get
            If Me.optSQL.Checked Then
                Dim query As String

                query = Me.m_customDSN & "|" & Me.m_customUserID & "|" & _EncryptDBValue(Me.m_customPassword) & "|" & txtQuery.Text

                Return query
            Else
                Return ""
            End If
        End Get
    End Property

    Public Sub saveInfo(ByVal id As Integer, ByVal type As clsMarsScheduler.enScheduleType)
        Dim SQL As String
        Dim keyColumn As String = "reportid"

        If type = clsMarsScheduler.enScheduleType.PACKAGE Then
            keyColumn = "packid"
        Else
            keyColumn = "reportid"
        End If

        clsMarsData.WriteData("DELETE FROM BlankReportAlert WHERE " & keyColumn & " = " & id)

        SQL = "INSERT INTO BlankReportAlert(BlankID,[Type],[To],Msg," & keyColumn & ",[Subject],CustomQueryDetails,UseFileSizeCheck,FileSizeMinimum,blankReportFormat,allReportsBlankOnly) VALUES (" & _
                clsMarsData.CreateDataID("blankreportalert", "blankid") & "," & _
                "'" & blankType & "'," & _
                "''," & _
                "''," & _
                id & "," & _
                "''," & _
                "'" & SQLPrepare(m_CustomBlankQuery) & "'," & _
                Convert.ToInt32(optFilesize.Checked) & "," & _
                txtMinSize.Value & "," & _
                "'" & cmbBlankFormat.Text & "'," & _
                Convert.ToInt32(chkAllReports.Checked) & ")"

        clsMarsData.WriteData(SQL)


    End Sub

    Public Sub getInfo(ByVal nID As Integer, ByVal type As clsMarsScheduler.enScheduleType)
        Dim keyColumn As String = "reportid"

        If type = clsMarsScheduler.enScheduleType.PACKAGE Then
            keyColumn = "packid"
        Else
            keyColumn = "reportid"
        End If

        Dim oRs As ADODB.Recordset

        oRs = clsMarsData.GetData("SELECT * FROM BlankReportAlert WHERE " & keyColumn & " =" & nID)

        Try
            If oRs.EOF = False Then
                With Me
                    .UcBlankReportTasks.ShowAfterType = False
                    .UcBlankReportTasks.ScheduleID = nID
                    .UcBlankReportTasks.LoadTasks()

                    blankType = oRs("type").Value

                    Dim useMin As Boolean

                    Try
                        useMin = Convert.ToInt16(oRs("usefilesizecheck").Value)
                    Catch ex As Exception
                        useMin = False
                    End Try

                    If useMin = False Then
                        Dim query As String = IsNull(oRs("customquerydetails").Value)

                        If query.Length > 0 Then
                            .optSQL.Checked = True
                            .m_customDSN = query.Split("|")(0)
                            .m_customUserID = query.Split("|")(1)
                            .m_customPassword = _DecryptDBValue(query.Split("|")(2))
                            .txtQuery.Text = query.Split("|")(3)
                        End If
                    Else
                        .optFilesize.Checked = True
                        .cmbBlankFormat.Text = IsNull(oRs("blankreportformat").Value, "Text (*.txt)")
                        txtMinSize.Value = IsNull(oRs("filesizeminimum").Value, 1)
                    End If

                    Try
                        chkAllReports.Checked = Convert.ToBoolean(oRs("allReportsBlankOnly").Value)
                    Catch : End Try
                End With

                oRs.Close()
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
                      Erl)
        End Try
    End Sub

    Private Sub ucBlankReportX_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        setupForDragAndDrop(txtQuery)

        UcBlankReportTasks.m_forExceptionHandling = True
    End Sub

    Private Sub btnBuild_Click(sender As System.Object, e As System.EventArgs) Handles btnBuild.Click
        Dim getSQL As frmDynamicParameter = New frmDynamicParameter
        Dim sCon As String

        sCon = Me.m_customDSN & "|" & Me.m_customUserID & "|" & Me.m_customPassword

        Dim query As String() = getSQL.ReturnSQLQuery(sCon, Me.txtQuery.Text)

        If query IsNot Nothing Then
            sCon = query(0)
            txtQuery.Text = query(1)

            Me.m_customDSN = sCon.Split("|")(0)
            Me.m_customUserID = sCon.Split("|")(1)
            Me.m_customPassword = sCon.Split("|")(2)
        End If
    End Sub

  
End Class
