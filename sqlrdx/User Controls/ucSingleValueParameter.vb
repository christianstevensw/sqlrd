﻿Imports System.Collections.Generic

Public Class ucSingleValueParameter

    Sub New(avValue As Hashtable, pname As String, prompt As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        availableValuesList = avValue
        parameterName = pname
        parameterPrompt = prompt

        lblParameterName.Text = String.Format("{0} ({1})", prompt, pname)
    End Sub

    Dim m_parameterName As String

    Public WriteOnly Property lockParameter As Boolean
        Set(value As Boolean)

            Me.Enabled = False

        End Set
    End Property

    Public Property parameterName As String
        Get
            Return m_parameterName
        End Get
        Set(value As String)
            m_parameterName = value
        End Set
    End Property

    Private m_parameterPrompt As String
    Public Property parameterPrompt() As String
        Get
            Return m_parameterPrompt
        End Get
        Set(ByVal value As String)
            m_parameterPrompt = value
        End Set
    End Property


    Dim m_availableValuesList As Hashtable

    Public Property availableValuesList As Hashtable
        Get
            Return m_availableValuesList
        End Get
        Set(value As Hashtable)
            m_availableValuesList = value

            cmbValue.Items.Clear()

            For Each de As DictionaryEntry In value
                cmbValue.Items.Add(de.Key)
            Next
        End Set
    End Property


    Public Property selectedValue As System.Collections.Generic.KeyValuePair(Of String, String)
        Get
            Dim value

            value = Me.availableValuesList(cmbValue.Text)

            If value Is Nothing Or value = "" Then
                value = cmbValue.Text
            End If

            selectedValue = New KeyValuePair(Of String, String)(cmbValue.Text, value)
        End Get
        Set(value As System.Collections.Generic.KeyValuePair(Of String, String))
            cmbValue.Text = value.Key

            If cmbValue.Text = "[SQL-RDNull]" Then
                optNullValue.Checked = True
            ElseIf cmbValue.Text = "[SQL-RDDefault]" Then
                optDefaultValue.Checked = True
            ElseIf cmbValue.Text.StartsWith("[") And cmbValue.Text.EndsWith("]") Then
                cmbValue.Enabled = False
            Else
                cmbValue.Enabled = True

                If availableValuesList.ContainsKey(value.Key) Then
                    availableValuesList(value.Key) = value.Value
                Else
                    availableValuesList.Add(value.Key, value.Value)
                End If
            End If
        End Set
    End Property
    Private Sub optNullValue_CheckedChanged(sender As Object, e As EventArgs) Handles optNullValue.CheckedChanged, optSpecifiedValue.CheckedChanged, optDefaultValue.CheckedChanged
        If optNullValue.Checked Then
            cmbValue.Enabled = False
            cmbValue.Text = "[SQL-RDNull]"
        ElseIf optDefaultValue.Checked Then
            cmbValue.Enabled = False
            cmbValue.Text = "[SQL-RDDefault]"
        Else
            If cmbValue.Text = "[SQL-RDNull]" Or cmbValue.Text = "[SQL-RDDefault]" Then cmbValue.Text = ""

            cmbValue.Enabled = True
        End If
    End Sub


    Private Sub btnCancel_Click(sender As Object, e As EventArgs)
        Me.Parent.Controls.Remove(Me)
    End Sub




    Private Sub ucSingleValueParameter_Load(sender As Object, e As EventArgs) Handles Me.Load
        setupForDragAndDrop(Me.cmbValue)
    End Sub

    Private Sub cmbValue_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbValue.SelectedIndexChanged
        If cmbValue.Text.StartsWith("[") And cmbValue.Text.EndsWith("]") Then
            cmbValue.Enabled = False
        Else
            cmbValue.Enabled = True
        End If
    End Sub
End Class
