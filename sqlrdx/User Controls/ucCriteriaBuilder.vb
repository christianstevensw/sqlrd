﻿Imports DevComponents.DotNetBar
Public Class ucCriteriaBuilder
    Dim ep As ErrorProvider = New ErrorProvider

    Private Function addCriteriaControl() As ucCriteria
        Dim uc As ucCriteria = New ucCriteria
        uc.Visible = True

        Panel1.Controls.Add(uc)
        uc.Dock = DockStyle.Top
        uc.BringToFront()

        AddHandler uc.btnAddCriteria.Click, AddressOf btnAddCriteria_Click
        AddHandler uc.btnRemove.Click, AddressOf btnRemove_Click

        If Panel1.Controls.Count > 1 Then uc.btnRemove.Enabled = True

        Return uc
    End Function
    Private Sub btnAddCriteria_Click(sender As System.Object, e As System.EventArgs)
        '//some validation
        If TypeOf sender Is ButtonX Then
            Dim btn As ButtonX = CType(sender, ButtonX)

            Dim ucMe As ucCriteria = btn.Parent

            If ucMe.cmbOperator.Text = "" Or ucMe.txtCriteria.Text = "" Then
                ep.SetIconAlignment(ucMe.btnRemove, ErrorIconAlignment.MiddleRight)
                ep.SetError(ucMe.btnRemove, "Please complete setting up this criteria first")
                Return
            Else
                ep.SetError(ucMe.btnRemove, "")
            End If
        End If

        addCriteriaControl()

    End Sub

    Private Sub btnRemove_Click(sender As System.Object, e As System.EventArgs)
        Dim btn As ButtonX = CType(sender, ButtonX)

        Dim uc As ucCriteria = btn.Parent

        Panel1.Controls.Remove(uc)

        If Panel1.Controls.Count = 1 Then
            uc = Panel1.Controls(0)

            uc.btnRemove.Enabled = False
        End If
    End Sub

    Public Property criteria As String
        Get
            Dim criterias As String = ""
            Dim arr As ArrayList = New ArrayList

            For Each uc As ucCriteria In Panel1.Controls
                If uc.criteria <> "" Then arr.Add(uc.criteria)
            Next

            arr.Reverse()

            For Each s As String In arr
                criterias = criterias & s & PD
            Next

            Return criterias
        End Get
        Set(value As String)
            Panel1.Controls.Clear()

            For Each criteria As String In value.Split(PD)
                If criteria.Contains("::") Then
                    Dim uc As ucCriteria = addCriteriaControl()
                    uc.criteria = criteria
                ElseIf criteria <> "" Then
                    Dim uc As ucCriteria = addCriteriaControl()
                    uc.criteria = "CONTAINS::" & criteria
                End If
            Next
        End Set
    End Property
    ''' <summary>
    ''' The criteria match
    ''' </summary>
    ''' <value>1 or 0</value>
    ''' <returns>1 or 0</returns>
    ''' <remarks>1 for match all and 0 for match any</remarks>
    Public Property criteriaMatch As Integer
        Get
            If optAll.Checked Then
                Return 1
            Else
                Return 0
            End If
        End Get
        Set(value As Integer)
            If value = 1 Then
                optAll.Checked = True
            Else
                optAny.Checked = True
            End If
        End Set
    End Property

End Class
