﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucParameters
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucParameters))
        Me.btnAddValue = New DevComponents.DotNetBar.ButtonX()
        Me.chkAll = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.lsvValues = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.chkIgnore = New System.Windows.Forms.RadioButton()
        Me.chkNull = New System.Windows.Forms.RadioButton()
        Me.chkLBound = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtValue = New System.Windows.Forms.ComboBox()
        Me.txtValue2 = New System.Windows.Forms.ComboBox()
        Me.chkUBound = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkUBoundInc = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkLBoundInc = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.optSpecificValue = New System.Windows.Forms.RadioButton()
        Me.chkAllValues = New System.Windows.Forms.RadioButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.wait = New DevComponents.DotNetBar.Controls.CircularProgress()
        Me.grpParameter = New DevComponents.DotNetBar.ExpandablePanel()
        Me.lblIndex = New System.Windows.Forms.Label()
        Me.btnGetValues = New DevComponents.DotNetBar.ButtonX()
        Me.GroupPanel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.grpParameter.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnAddValue
        '
        Me.btnAddValue.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnAddValue.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnAddValue.Image = CType(resources.GetObject("btnAddValue.Image"), System.Drawing.Image)
        Me.btnAddValue.Location = New System.Drawing.Point(442, 33)
        Me.btnAddValue.Name = "btnAddValue"
        Me.btnAddValue.Size = New System.Drawing.Size(33, 23)
        Me.btnAddValue.TabIndex = 35
        Me.btnAddValue.Visible = False
        '
        'chkAll
        '
        Me.chkAll.AutoSize = True
        Me.chkAll.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkAll.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAll.Location = New System.Drawing.Point(6, 132)
        Me.chkAll.Name = "chkAll"
        Me.chkAll.Size = New System.Drawing.Size(105, 15)
        Me.chkAll.TabIndex = 34
        Me.chkAll.Text = "Select All Values"
        '
        'lsvValues
        '
        '
        '
        '
        Me.lsvValues.Border.Class = "ListViewBorder"
        Me.lsvValues.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvValues.CheckBoxes = True
        Me.lsvValues.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvValues.Location = New System.Drawing.Point(6, 153)
        Me.lsvValues.Name = "lsvValues"
        Me.lsvValues.Size = New System.Drawing.Size(430, 135)
        Me.lsvValues.TabIndex = 33
        Me.lsvValues.UseCompatibleStateImageBehavior = False
        Me.lsvValues.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Select Value(s)"
        Me.ColumnHeader1.Width = 314
        '
        'chkIgnore
        '
        Me.chkIgnore.AutoSize = True
        Me.chkIgnore.BackColor = System.Drawing.Color.Transparent
        Me.chkIgnore.Location = New System.Drawing.Point(185, 3)
        Me.chkIgnore.Name = "chkIgnore"
        Me.chkIgnore.Size = New System.Drawing.Size(88, 17)
        Me.chkIgnore.TabIndex = 5
        Me.chkIgnore.Text = "Default value"
        Me.chkIgnore.UseVisualStyleBackColor = False
        '
        'chkNull
        '
        Me.chkNull.AutoSize = True
        Me.chkNull.BackColor = System.Drawing.Color.Transparent
        Me.chkNull.Location = New System.Drawing.Point(107, 3)
        Me.chkNull.Name = "chkNull"
        Me.chkNull.Size = New System.Drawing.Size(72, 17)
        Me.chkNull.TabIndex = 3
        Me.chkNull.Text = "Null value"
        Me.chkNull.UseVisualStyleBackColor = False
        '
        'chkLBound
        '
        '
        '
        '
        Me.chkLBound.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkLBound.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkLBound.Location = New System.Drawing.Point(36, 447)
        Me.chkLBound.Name = "chkLBound"
        Me.chkLBound.Size = New System.Drawing.Size(112, 24)
        Me.chkLBound.TabIndex = 6
        Me.chkLBound.Text = "No Lower Bound"
        Me.chkLBound.Visible = False
        '
        'txtValue
        '
        Me.txtValue.ItemHeight = 13
        Me.txtValue.Location = New System.Drawing.Point(5, 35)
        Me.txtValue.Name = "txtValue"
        Me.txtValue.Size = New System.Drawing.Size(431, 21)
        Me.txtValue.Sorted = True
        Me.txtValue.TabIndex = 0
        Me.txtValue.Tag = "memo"
        '
        'txtValue2
        '
        Me.txtValue2.ItemHeight = 13
        Me.txtValue2.Location = New System.Drawing.Point(91, 477)
        Me.txtValue2.Name = "txtValue2"
        Me.txtValue2.Size = New System.Drawing.Size(261, 21)
        Me.txtValue2.TabIndex = 1
        Me.txtValue2.Tag = "memo"
        Me.txtValue2.Visible = False
        '
        'chkUBound
        '
        '
        '
        '
        Me.chkUBound.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkUBound.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkUBound.Location = New System.Drawing.Point(340, 447)
        Me.chkUBound.Name = "chkUBound"
        Me.chkUBound.Size = New System.Drawing.Size(112, 24)
        Me.chkUBound.TabIndex = 7
        Me.chkUBound.Text = "No Upper Bound"
        Me.chkUBound.Visible = False
        '
        'chkUBoundInc
        '
        '
        '
        '
        Me.chkUBoundInc.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkUBoundInc.Checked = True
        Me.chkUBoundInc.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkUBoundInc.CheckValue = "Y"
        Me.chkUBoundInc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkUBoundInc.Location = New System.Drawing.Point(256, 447)
        Me.chkUBoundInc.Name = "chkUBoundInc"
        Me.chkUBoundInc.Size = New System.Drawing.Size(96, 24)
        Me.chkUBoundInc.TabIndex = 4
        Me.chkUBoundInc.Text = "Include Value"
        Me.chkUBoundInc.Visible = False
        '
        'chkLBoundInc
        '
        '
        '
        '
        Me.chkLBoundInc.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkLBoundInc.Checked = True
        Me.chkLBoundInc.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkLBoundInc.CheckValue = "Y"
        Me.chkLBoundInc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkLBoundInc.Location = New System.Drawing.Point(154, 447)
        Me.chkLBoundInc.Name = "chkLBoundInc"
        Me.chkLBoundInc.Size = New System.Drawing.Size(96, 24)
        Me.chkLBoundInc.TabIndex = 2
        Me.chkLBoundInc.Text = "Include Value"
        Me.chkLBoundInc.Visible = False
        '
        'GroupPanel1
        '
        Me.GroupPanel1.BackColor = System.Drawing.Color.Transparent
        Me.GroupPanel1.CanvasColor = System.Drawing.SystemColors.Control
        Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel1.Controls.Add(Me.FlowLayoutPanel1)
        Me.GroupPanel1.Location = New System.Drawing.Point(6, 62)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(430, 53)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderBottomWidth = 1
        Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderLeftWidth = 1
        Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderRightWidth = 1
        Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderTopWidth = 1
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 38
        Me.GroupPanel1.Text = "Set value as:"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel1.Controls.Add(Me.optSpecificValue)
        Me.FlowLayoutPanel1.Controls.Add(Me.chkNull)
        Me.FlowLayoutPanel1.Controls.Add(Me.chkIgnore)
        Me.FlowLayoutPanel1.Controls.Add(Me.chkAllValues)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(7, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(414, 29)
        Me.FlowLayoutPanel1.TabIndex = 40
        '
        'optSpecificValue
        '
        Me.optSpecificValue.AutoSize = True
        Me.optSpecificValue.Checked = True
        Me.optSpecificValue.Location = New System.Drawing.Point(3, 3)
        Me.optSpecificValue.Name = "optSpecificValue"
        Me.optSpecificValue.Size = New System.Drawing.Size(98, 17)
        Me.optSpecificValue.TabIndex = 36
        Me.optSpecificValue.TabStop = True
        Me.optSpecificValue.Text = "Specified value"
        Me.optSpecificValue.UseVisualStyleBackColor = True
        '
        'chkAllValues
        '
        Me.chkAllValues.AutoSize = True
        Me.chkAllValues.Location = New System.Drawing.Point(279, 3)
        Me.chkAllValues.Name = "chkAllValues"
        Me.chkAllValues.Size = New System.Drawing.Size(70, 17)
        Me.chkAllValues.TabIndex = 37
        Me.chkAllValues.Text = "All values"
        Me.chkAllValues.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 122)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(639, 16)
        Me.Panel1.TabIndex = 2
        '
        'Panel2
        '
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(52, 122)
        Me.Panel2.TabIndex = 3
        '
        'Panel3
        '
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel3.Location = New System.Drawing.Point(587, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(52, 122)
        Me.Panel3.TabIndex = 4
        '
        'wait
        '
        Me.wait.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.wait.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.wait.Location = New System.Drawing.Point(481, 35)
        Me.wait.Name = "wait"
        Me.wait.ProgressBarType = DevComponents.DotNetBar.eCircularProgressType.Pie
        Me.wait.Size = New System.Drawing.Size(46, 23)
        Me.wait.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.wait.TabIndex = 39
        Me.wait.Visible = False
        '
        'grpParameter
        '
        Me.grpParameter.CanvasColor = System.Drawing.SystemColors.Control
        Me.grpParameter.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.grpParameter.Controls.Add(Me.lblIndex)
        Me.grpParameter.Controls.Add(Me.btnGetValues)
        Me.grpParameter.Controls.Add(Me.chkLBound)
        Me.grpParameter.Controls.Add(Me.txtValue2)
        Me.grpParameter.Controls.Add(Me.wait)
        Me.grpParameter.Controls.Add(Me.chkUBound)
        Me.grpParameter.Controls.Add(Me.txtValue)
        Me.grpParameter.Controls.Add(Me.chkUBoundInc)
        Me.grpParameter.Controls.Add(Me.GroupPanel1)
        Me.grpParameter.Controls.Add(Me.chkLBoundInc)
        Me.grpParameter.Controls.Add(Me.btnAddValue)
        Me.grpParameter.Controls.Add(Me.chkAll)
        Me.grpParameter.Controls.Add(Me.lsvValues)
        Me.grpParameter.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grpParameter.Location = New System.Drawing.Point(52, 0)
        Me.grpParameter.Name = "grpParameter"
        Me.grpParameter.Size = New System.Drawing.Size(535, 122)
        Me.grpParameter.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.grpParameter.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.grpParameter.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.grpParameter.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.grpParameter.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(111, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(217, Byte), Integer))
        Me.grpParameter.Style.CornerDiameter = 6
        Me.grpParameter.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.grpParameter.Style.GradientAngle = 90
        Me.grpParameter.TabIndex = 5
        Me.grpParameter.TitleStyle.Alignment = System.Drawing.StringAlignment.Center
        Me.grpParameter.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.grpParameter.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.grpParameter.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.grpParameter.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.grpParameter.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.grpParameter.TitleStyle.GradientAngle = 90
        Me.grpParameter.TitleText = "Title Bar"
        '
        'lblIndex
        '
        Me.lblIndex.AutoSize = True
        Me.lblIndex.Location = New System.Drawing.Point(458, 83)
        Me.lblIndex.Name = "lblIndex"
        Me.lblIndex.Size = New System.Drawing.Size(0, 13)
        Me.lblIndex.TabIndex = 41
        Me.lblIndex.Visible = False
        '
        'btnGetValues
        '
        Me.btnGetValues.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnGetValues.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnGetValues.Image = CType(resources.GetObject("btnGetValues.Image"), System.Drawing.Image)
        Me.btnGetValues.Location = New System.Drawing.Point(442, 153)
        Me.btnGetValues.Name = "btnGetValues"
        Me.btnGetValues.Size = New System.Drawing.Size(33, 23)
        Me.btnGetValues.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnGetValues.TabIndex = 40
        Me.btnGetValues.Tooltip = "Get available values"
        '
        'ucParameters
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me.grpParameter)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "ucParameters"
        Me.Size = New System.Drawing.Size(639, 138)
        Me.GroupPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.grpParameter.ResumeLayout(False)
        Me.grpParameter.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents chkIgnore As RadioButton
    Friend WithEvents chkNull As RadioButton
    Friend WithEvents chkLBound As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtValue As System.Windows.Forms.ComboBox
    Friend WithEvents txtValue2 As System.Windows.Forms.ComboBox
    Friend WithEvents chkUBound As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkUBoundInc As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkLBoundInc As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents chkAll As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents lsvValues As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnAddValue As DevComponents.DotNetBar.ButtonX
    Friend WithEvents chkAllValues As System.Windows.Forms.RadioButton
    Friend WithEvents optSpecificValue As System.Windows.Forms.RadioButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents wait As DevComponents.DotNetBar.Controls.CircularProgress
    Friend WithEvents grpParameter As DevComponents.DotNetBar.ExpandablePanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnGetValues As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lblIndex As System.Windows.Forms.Label


End Class
