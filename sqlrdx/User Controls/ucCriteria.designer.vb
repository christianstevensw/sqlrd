﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucCriteria
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucCriteria))
        Me.cmbOperator = New System.Windows.Forms.ComboBox()
        Me.txtCriteria = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btnAddCriteria = New DevComponents.DotNetBar.ButtonX()
        Me.btnRemove = New DevComponents.DotNetBar.ButtonX()
        Me.SuspendLayout()
        '
        'cmbOperator
        '
        Me.cmbOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOperator.FormattingEnabled = True
        Me.cmbOperator.Items.AddRange(New Object() {"STARTS WITH", "ENDS WITH", "CONTAINS", "DOES NOT CONTAIN"})
        Me.cmbOperator.Location = New System.Drawing.Point(3, 3)
        Me.cmbOperator.Name = "cmbOperator"
        Me.cmbOperator.Size = New System.Drawing.Size(125, 21)
        Me.cmbOperator.TabIndex = 0
        '
        'txtCriteria
        '
        Me.txtCriteria.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.txtCriteria.Border.Class = "TextBoxBorder"
        Me.txtCriteria.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCriteria.Location = New System.Drawing.Point(134, 4)
        Me.txtCriteria.Name = "txtCriteria"
        Me.txtCriteria.Size = New System.Drawing.Size(266, 20)
        Me.txtCriteria.TabIndex = 1
        '
        'btnAddCriteria
        '
        Me.btnAddCriteria.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnAddCriteria.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddCriteria.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnAddCriteria.Image = CType(resources.GetObject("btnAddCriteria.Image"), System.Drawing.Image)
        Me.btnAddCriteria.Location = New System.Drawing.Point(406, 4)
        Me.btnAddCriteria.Name = "btnAddCriteria"
        Me.btnAddCriteria.Size = New System.Drawing.Size(41, 20)
        Me.btnAddCriteria.TabIndex = 2
        '
        'btnRemove
        '
        Me.btnRemove.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnRemove.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRemove.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnRemove.Enabled = False
        Me.btnRemove.Image = CType(resources.GetObject("btnRemove.Image"), System.Drawing.Image)
        Me.btnRemove.Location = New System.Drawing.Point(453, 4)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(41, 20)
        Me.btnRemove.TabIndex = 2
        '
        'ucCriteria
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me.btnRemove)
        Me.Controls.Add(Me.btnAddCriteria)
        Me.Controls.Add(Me.txtCriteria)
        Me.Controls.Add(Me.cmbOperator)
        Me.Name = "ucCriteria"
        Me.Size = New System.Drawing.Size(502, 30)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmbOperator As System.Windows.Forms.ComboBox
    Friend WithEvents txtCriteria As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnAddCriteria As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnRemove As DevComponents.DotNetBar.ButtonX

End Class
