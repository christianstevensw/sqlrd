﻿Public Class ucEditorX
    Public Enum format
        TEXT = 0
        HTML = 1
    End Enum
    Dim htmlViewer As frmLiveViewHTML
    Dim livePreview As Boolean = False
    Dim scratchFile As String
    Dim m_bodyFormat As format = format.HTML
    Public m_eventID As Integer
    Public m_eventBased As Boolean

    Public Overrides Property Text() As String
        Get
            If m_bodyFormat = format.HTML Then
                Dim bodystyle As String = ""
                Try

                    If htmlBox.BodyStyle <> "" Then
                        bodystyle = String.Format("style='{0}'", htmlBox.BodyStyle)
                    End If

                    Dim cleanHTML As String = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'><HTML><HEAD> </HEAD><BODY " & bodystyle & ">" & htmlBox.BodyHtml & "</BODY></HTML>"

                    Dim startp, endp As Integer

                    startp = cleanHTML.IndexOf("<HEAD>") + 6
                    endp = cleanHTML.IndexOf("</HEAD>")

                    Dim crap As String = cleanHTML.Substring(startp, endp - startp)

                    cleanHTML = cleanHTML.Remove(startp, crap.Length)

                    Return cleanHTML
                Catch ex As Exception
                    '//use regular expression
                    Dim val As String = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'><HTML><HEAD> </HEAD><BODY " & bodystyle & ">" & htmlBox.BodyHtml & "</BODY></HTML>"
                    val = System.Text.RegularExpressions.Regex.Replace(val, "<HEAD>.*</HEAD>", "<HEAD></HEAD>", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
                    Return val
                End Try
            Else
                Return txtBody.Text
            End If
        End Get
        Set(ByVal value As String)

            If m_bodyFormat = format.HTML Then

                value = value.Replace("&lt;", "<").Replace("&gt;", ">")

                value = value.Replace("%3C", "<").Replace("%3E", ">")

                value = value.Replace("%5B", "[").Replace("%5D", "]")

                htmlBox.BodyHtml = value
            Else
                txtBody.Text = value
            End If
        End Set
    End Property
    Public Property bodyFormat() As format
        Get
            Return m_bodyFormat
        End Get
        Set(ByVal value As format)
            If value = format.HTML Then
                txtBody.Visible = False

                htmlBox.Visible = True
                htmlBox.Dock = DockStyle.Fill
                htmlBox.SetBodyText(txtBody.Text)
            Else
                htmlBox.Visible = False

                txtBody.Visible = True
                txtBody.Dock = DockStyle.Fill
                txtBody.Text = htmlBox.GetBodyText

            End If

            m_bodyFormat = value
        End Set
    End Property
    Private Sub btnBold_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBold.Click
        Dim stag As String = "<b>"
        Dim startPoint As Integer = txtBody.SelectionStart
        Dim endpoint As Integer = startPoint + txtBody.SelectionLength
        txtBody.Text = txtBody.Text.Insert(endpoint, "</b>")
        txtBody.Text = txtBody.Text.Insert(startPoint, "<b>")
    End Sub

    Private Sub btnItalic_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnItalic.Click
        Dim stag As String = "<i>"
        Dim startPoint As Integer = txtBody.SelectionStart
        Dim endpoint As Integer = startPoint + txtBody.SelectionLength
        txtBody.Text = txtBody.Text.Insert(endpoint, "</i>")
        txtBody.Text = txtBody.Text.Insert(startPoint, "<i>")
    End Sub

    Private Sub btnUnderline_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnderline.Click
        Dim stag As String = "<u>"
        Dim startPoint As Integer = txtBody.SelectionStart
        Dim endpoint As Integer = startPoint + txtBody.SelectionLength

        txtBody.Text = txtBody.Text.Insert(endpoint, "</u>")
        txtBody.Text = txtBody.Text.Insert(startPoint, "<u>")
    End Sub

    Private Sub btnStrike_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStrike.Click
        Dim stag As String = "<strike>"
        Dim startPoint As Integer = txtBody.SelectionStart
        Dim endpoint As Integer = startPoint + txtBody.SelectionLength

        txtBody.Text = txtBody.Text.Insert(endpoint, "</strike>")
        txtBody.Text = txtBody.Text.Insert(startPoint, "<strike>")
    End Sub

    Private Sub btnLink_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLink.Click
        Dim link As String = InputBox("Please enter the link", Application.ProductName)
        If link = "" Then Return
        Dim stag As String = "<a href='" & link & "'>"
        Dim startPoint As Integer = txtBody.SelectionStart
        Dim endpoint As Integer = startPoint + txtBody.SelectionLength

        txtBody.Text = txtBody.Text.Insert(endpoint, "</a>")
        txtBody.Text = txtBody.Text.Insert(startPoint, stag)
    End Sub

    Private Sub btnImage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImage.Click
        Dim link As String = InputBox("Please enter the image source", Application.ProductName)
        If link = "" Then Return
        Dim stag As String = "<img src='" & link & "'>"
        Dim startPoint As Integer = txtBody.SelectionStart
        Dim endpoint As Integer = startPoint + txtBody.SelectionLength

        txtBody.Text = txtBody.Text.Insert(endpoint, "</img>")
        txtBody.Text = txtBody.Text.Insert(startPoint, stag)
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        If txtBody.Text = "" Then Return

        If livePreview = False Then
            livePreview = True
            btnPreview.Checked = True

            If htmlViewer Is Nothing Then
                htmlViewer = New frmLiveViewHTML
                htmlViewer.Owner = Me.ParentForm
                htmlViewer.Show()
            ElseIf htmlViewer.IsDisposed Then
                htmlViewer = New frmLiveViewHTML
                htmlViewer.Owner = Me.ParentForm
                htmlViewer.Show()
            ElseIf htmlViewer.Visible = False Then
                htmlViewer.Show()
            End If

            If scratchFile = "" Then scratchFile = Environment.GetEnvironmentVariable("TEMP") & "\" & IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName) & ".html"

            If txtBody.Text.Contains("<html>") = False Then
                IO.File.WriteAllText(scratchFile, txtBody.Text.Replace(Environment.NewLine, "<br>"))
            Else
                IO.File.WriteAllText(scratchFile, txtBody.Text)
            End If

            '  htmlViewer.wb.Navigate(scratchFile)

        Else
            livePreview = False
            btnPreview.Checked = False
            If htmlViewer.IsDisposed = False Then
                htmlViewer.Close()
                htmlViewer = Nothing
            End If
        End If
    End Sub

    Private Sub btnParagraph_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnParagraph.Click
        Dim stag As String = "<p>"
        Dim startPoint As Integer = txtBody.SelectionStart
        Dim endpoint As Integer = startPoint + txtBody.SelectionLength
        txtBody.Text = txtBody.Text.Insert(endpoint, "</p>" & vbCrLf)
        txtBody.Text = txtBody.Text.Insert(startPoint, "<p>")
    End Sub

    Private Sub ucEditor_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        setupForDragAndDrop(txtBody)
        setupForDragAndDrop(htmlBox)
        htmlBox._factoryBtnEditSource.Visible = False
        Dim DefaultFontFamily As String = clsMarsUI.MainUI.ReadRegistry("DefaultEmailFont", "Verdana")
        Dim DefaultFontSizeInPx As Integer = clsMarsUI.MainUI.ReadRegistry("DefaultEmailFontSize", 12)

        htmlBox.DefaultFontSizeInPx = DefaultFontSizeInPx
        htmlBox.DefaultFontFamily = DefaultFontFamily
    End Sub

    Private Sub txtBody_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBody.TextChanged
        If livePreview = True Then
            IO.File.WriteAllText(scratchFile, txtBody.Text.Replace(Environment.NewLine, "<br>"))

            'htmlViewer.wb.Navigate(scratchFile)

            '  htmlViewer.wb.Refresh()
        End If
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFont.Click
        Dim fd As FontDialog = New FontDialog
        fd.ShowColor = True

        If fd.ShowDialog <> DialogResult.Cancel Then
            Dim fn As Font = fd.Font
            Dim col As Color = fd.Color

            Dim stag As String = "<font name='" & fn.Name & "' size='" & fn.SizeInPoints & "pts' color ='" & col.ToKnownColor.ToString & "'>"
            Dim startPoint As Integer = txtBody.SelectionStart
            Dim endpoint As Integer = startPoint + txtBody.SelectionLength
            txtBody.Text = txtBody.Text.Insert(endpoint, "</font>" & vbCrLf)
            txtBody.Text = txtBody.Text.Insert(startPoint, stag)
        End If
    End Sub

    Private Sub ToolStripButton1_Click_1(sender As System.Object, e As System.EventArgs) Handles ToolStripButton1.Click
        Dim editor As frmLiveViewHTML = New frmLiveViewHTML
        editor.m_eventID = Me.m_eventID
        editor.m_eventBased = Me.m_eventBased

        Dim html As String = editor.editHTML(htmlBox.DocumentHtml)

        If html IsNot Nothing Then
            htmlBox.DocumentHtml = html
        End If
    End Sub
End Class
