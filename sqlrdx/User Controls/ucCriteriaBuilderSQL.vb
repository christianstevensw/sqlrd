﻿Imports DevComponents.DotNetBar

Public Class ucCriteriaBuilderSQL
    Dim ep As ErrorProvider = New ErrorProvider
    Public Property m_columns As System.Collections.Generic.List(Of String)
    Public m_dataTypes As Hashtable

    Public Function addCriteriaControl() As ucCriteriaSQL
        Dim uc As ucCriteriaSQL = New ucCriteriaSQL
        uc.Visible = True
        uc.columns = m_columns
        Panel1.Controls.Add(uc)
        uc.Dock = DockStyle.Top
        uc.BringToFront()

        AddHandler uc.btnAddCriteria.Click, AddressOf btnAddCriteria_Click
        AddHandler uc.btnRemove.Click, AddressOf btnRemove_Click

        setupForDragAndDrop(uc.txtCriteria)

        If Panel1.Controls.Count > 1 Then uc.btnRemove.Enabled = True

        Return uc
    End Function

    Private Sub btnAddCriteria_Click(sender As System.Object, e As System.EventArgs)
        '//some validation
        If TypeOf sender Is ButtonX Then
            Dim btn As ButtonX = CType(sender, ButtonX)

            Dim ucMe As ucCriteriaSQL = btn.Parent

            If ucMe.cmbOperator.Text = "" Or ucMe.txtCriteria.Text = "" Or ucMe.cmbAndOr.Text = "" Then
                ep.SetIconAlignment(ucMe.btnRemove, ErrorIconAlignment.MiddleRight)
                ep.SetError(ucMe.btnRemove, "Please complete setting up this criteria first")
                Return
            Else
                ep.SetError(ucMe.btnRemove, "")
            End If
        End If

        addCriteriaControl()

    End Sub

    Private Sub btnRemove_Click(sender As System.Object, e As System.EventArgs)
        Dim btn As ButtonX = CType(sender, ButtonX)

        Dim uc As ucCriteriaSQL = btn.Parent

        Panel1.Controls.Remove(uc)

        If Panel1.Controls.Count = 1 Then
            uc = Panel1.Controls(0)

            uc.btnRemove.Enabled = False
        End If
    End Sub

    Public ReadOnly Property fullCriteria As String
        Get
            Dim criterias As String = ""
            Dim arr As ArrayList = New ArrayList

            For Each uc As ucCriteriaSQL In Panel1.Controls
                If uc.criteria <> "" Then arr.Add(uc.criteria)
            Next

            arr.Reverse()

            For Each s As String In arr
                criterias = criterias & " " & s
            Next

            Dim exceptions As String() = New String() {"and", "or"}

            For Each s As String In exceptions
                If criterias.ToLower.EndsWith(s) Then
                    criterias = criterias.Remove(criterias.Length - (s.Length), s.Length)
                End If
            Next

            Return criterias.Trim
        End Get
    
    End Property
End Class
