﻿Public Class folder
    Dim m_folderID As Integer = 0
    Dim cRow As ADODB.Recordset

    Sub New(ByVal folderID As Integer)
        m_folderID = folderID


        If folderID = 0 Then
            cRow = New ADODB.Recordset
            cRow.Fields.Append("folderid", ADODB.DataTypeEnum.adInteger)
            cRow.Fields.Append("foldername", ADODB.DataTypeEnum.adVarChar, 255)
            cRow.Fields.Append("parent", ADODB.DataTypeEnum.adInteger)
            cRow.Fields.Append("owner", ADODB.DataTypeEnum.adVarChar, 255)

            cRow.Open()
            cRow.AddNew()
            cRow("folderid").Value = 0
            cRow("foldername").Value = "Desktop"
            cRow("parent").Value = -1
            cRow("owner").Value = "system"
        Else
            cRow = clsMarsData.GetData("SELECT * FROM folders WHERE folderid =" & folderID, , , True)
        End If
        'cRow = clsMarsData.GetData("SELECT * FROM folders WHERE folderid =" & folderID, , , True)
    End Sub

    Public Property m_RoleHasFolderAccess As Boolean


    Public ReadOnly Property folderID As Integer
        Get
            Return m_folderID
        End Get
    End Property

    Public ReadOnly Property folderName() As String
        Get
            Return cRow("foldername").Value
        End Get
      
    End Property

    Public ReadOnly Property folderParent() As Integer
        Get
            Return cRow("parent").Value
        End Get
       
    End Property
    Public Function doesFolderContain(ByVal name As String, ByVal type As clsMarsScheduler.enScheduleType) As Boolean
        Select Case type
            Case clsMarsScheduler.enScheduleType.REPORT
                For Each rpt As cssreport In getReports
                    If String.Compare(rpt.reportName, name, True) = 0 Then
                        Return True
                    End If
                Next
            Case clsMarsScheduler.enScheduleType.AUTOMATION
                For Each auto As Automation In getAutomations
                    If String.Compare(auto.AutoName, name, True) = 0 Then
                        Return True
                    End If
                Next
            Case clsMarsScheduler.enScheduleType.EVENTBASED
                For Each evt As EventBased In getEventBased
                    If String.Compare(evt.eventName, name, True) = 0 Then
                        Return True
                    End If
                Next
            Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                For Each evtp As EventBased In getEventBasedPackages
                    If String.Compare(evtp.eventName, name, True) = 0 Then
                        Return True
                    End If
                Next
            Case clsMarsScheduler.enScheduleType.PACKAGE
                For Each pack As Package In getPackages
                    If String.Compare(pack.packageName, name, True) = 0 Then
                        Return True
                    End If
                Next
            Case Else
                Return False
        End Select

        Return False
    End Function

    Public ReadOnly Property getSubFolders As ArrayList
        Get
            Dim SQL As String = "SELECT folderid FROM folders WHERE parent =" & m_folderID

            If gConType = "DAT" Then
                SQL &= " ORDER BY FolderName"
            Else
                SQL &= " ORDER BY CAST(FolderName AS VARCHAR(255))"
            End If

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL, , , True)
            Dim children As ArrayList = New ArrayList

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    Dim fld As folder = New folder(oRs("folderid").Value)

                    children.Add(fld)

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            Return children
        End Get
    End Property

    Public ReadOnly Property getTotalContents() As Integer
        Get
            Return getReports.Count + getPackages.Count + getAutomations.Count + getEventBased.Count + getEventBasedPackages.Count + getSubFolders.Count
        End Get
    End Property
    Public ReadOnly Property getReports() As ArrayList
        Get
            Dim SQL As String = "SELECT reportid FROM reportattr WHERE [parent] = " & m_folderID & " AND (packid = 0 OR packid IS Null) "

            If gRole.ToLower <> "administrator" And m_RoleHasFolderAccess = False Then
                Dim notAdmin = " AND ReportAttr.ReportID IN (SELECT ReportID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                SQL &= notAdmin
            End If

            If gConType = "DAT" Then
                SQL &= " ORDER BY reportName"
            Else
                SQL &= " ORDER BY CAST(reportName AS VARCHAR(255))"
            End If

            Dim rows As ADODB.Recordset = clsMarsData.GetData(SQL, , , True)
            Dim reports As ArrayList = New ArrayList

            If rows IsNot Nothing Then
                Do While rows.EOF = False
                    Dim rpt As cssreport = New cssreport(rows("reportid").Value)

                    reports.Add(rpt)

                    rows.MoveNext()
                Loop

                rows.Close()
            End If

            Return reports
        End Get
    End Property

    Public ReadOnly Property getPackages() As ArrayList
        Get
            Dim SQL As String = "SELECT packid FROM packageattr WHERE [parent] = " & m_folderID

            If gRole.ToLower <> "administrator" And m_RoleHasFolderAccess = False Then
                Dim notAdmin As String = " AND PackageAttr.PackID IN (SELECT PackID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                SQL &= notAdmin
            End If

            If gConType = "DAT" Then
                SQL &= " ORDER BY packageName"
            Else
                SQL &= " ORDER BY CAST(packageName AS VARCHAR(255))"
            End If

            Dim rows As ADODB.Recordset = clsMarsData.GetData(SQL, , , True)
            Dim packages As ArrayList = New ArrayList

            If rows IsNot Nothing Then
                Do While rows.EOF = False
                    Dim pack As Package = New Package(rows("packid").Value)

                    packages.Add(pack)

                    rows.MoveNext()
                Loop

                rows.Close()
            End If

            Return packages
        End Get
    End Property

    Public ReadOnly Property getAutomations As ArrayList
        Get
            Dim SQL As String = "SELECT autoid FROM automationattr WHERE [parent] = " & m_folderID

            If gRole.ToLower <> "administrator" And m_RoleHasFolderAccess = False Then
                Dim sNotAdmin As String = " AND AutomationAttr.AutoID IN (SELECT AutoID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                SQL &= sNotAdmin
            End If

            If gConType = "DAT" Then
                SQL &= " ORDER BY autoName"
            Else
                SQL &= " ORDER BY CAST(autoName AS VARCHAR(255))"
            End If

            Dim rows As ADODB.Recordset = clsMarsData.GetData(SQL, , , True)
            Dim autos As ArrayList = New ArrayList

            If rows IsNot Nothing Then
                Do While rows.EOF = False
                    Dim auto As Automation = New Automation(rows("autoid").Value)

                    autos.Add(auto)

                    rows.MoveNext()
                Loop

                rows.Close()
            End If

            Return autos
        End Get
    End Property

    Public ReadOnly Property getEventBased As ArrayList
        Get
            Dim SQL As String = "SELECT eventid FROM eventattr6 WHERE [parent] = " & m_folderID & " AND (PackID IS NULL OR PackID =0) "

            If gRole.ToLower <> "administrator" And m_RoleHasFolderAccess = False Then
                Dim sNotAdmin As String = " AND EventAttr6.EventID IN (SELECT EventID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                SQL &= sNotAdmin
            End If

            If gConType = "DAT" Then
                SQL &= " ORDER BY eventName"
            Else
                SQL &= " ORDER BY CAST(eventName AS VARCHAR(255))"
            End If

            Dim rows As ADODB.Recordset = clsMarsData.GetData(SQL, , , True)
            Dim events As ArrayList = New ArrayList

            If rows IsNot Nothing Then
                Do While rows.EOF = False
                    Dim eventschedule As EventBased = New EventBased(rows("eventid").Value)

                    events.Add(eventschedule)

                    rows.MoveNext()
                Loop

                rows.Close()
            End If

            Return events
        End Get
    End Property

    Public ReadOnly Property getEventBasedPackages As ArrayList
        Get
            Dim SQL As String = "SELECT eventpackid FROM eventpackageattr WHERE [parent] = " & m_folderID

            If gRole.ToLower <> "administrator" And m_RoleHasFolderAccess = False Then
                Dim sNotAdmin As String = " AND EventPackageAttr.EventPackID IN (SELECT EventPackID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                SQL &= sNotAdmin
            End If

            If gConType = "DAT" Then
                SQL &= " ORDER BY packageName"
            Else
                SQL &= " ORDER BY CAST(packageName AS VARCHAR(255))"
            End If

            Dim rows As ADODB.Recordset = clsMarsData.GetData(SQL, , , True)
            Dim eventPacks As ArrayList = New ArrayList

            If rows IsNot Nothing Then
                Do While rows.EOF = False
                    Dim eventpack As EventBasedPackage = New EventBasedPackage(rows("eventpackid").Value)

                    eventPacks.Add(eventpack)

                    rows.MoveNext()
                Loop

                rows.Close()
            End If

            Return eventPacks
        End Get
    End Property

    Public Function addFolder(ByVal folderName As String, ByRef err As String) As Integer
        Dim childFolders As ArrayList = getSubFolders

        Dim folderExists As Boolean = False

        For Each fld As folder In childFolders
            If fld.folderName.ToLower = folderName.ToLower Then
                folderExists = True
                Exit For
            End If
        Next

        If folderExists Then
            err = "Folder with specified name already exists"
            Return 0
        Else
            Dim vals, cols As String
            Dim newfolderid As Integer = clsMarsData.CreateDataID("folders", "folderid")

            cols = "folderid, foldername,parent"

            vals = newfolderid & "," & _
                "'" & SQLPrepare(folderName) & "'," & _
                Me.folderID

            If clsMarsData.DataItem.InsertData("folders", cols, vals, True) = True Then
                '//the user's group should get access to this here folder too automatically
                If gRole <> "Administrator" Then
                    Dim grp As usergroup = New usergroup(gRole)

                    If grp.isFolderAccessRestricted Then
                        grp.grantAccessToFolder(newfolderid)
                    End If

                End If

                Return newfolderid
            End If
            End If
    End Function

    Public Function getFolderPath() As String
        Try
            Dim parent As Integer = folderID
            Dim folderCollection As ArrayList = New ArrayList


            Do While parent <> 0
                Dim rows As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM folders WHERE folderid =" & parent)

                If rows IsNot Nothing AndAlso rows.EOF = False Then
                    folderCollection.Add(rows("foldername").Value)
                    parent = rows("parent").Value
                Else
                    Return ""
                End If
            Loop

            folderCollection.Reverse() '//reverse the entire list

            Dim path As String = ""

            For Each folder As String In folderCollection
                path &= folder & "\"
            Next

            Return path
        Catch
            Return ""
        End Try
    End Function
End Class
