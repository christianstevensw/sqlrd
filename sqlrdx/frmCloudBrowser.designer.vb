﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCloudBrowser
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tvCloud = New DevComponents.AdvTree.AdvTree()
        Me.Node1 = New DevComponents.AdvTree.Node()
        Me.NodeConnector1 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle1 = New DevComponents.DotNetBar.ElementStyle()
        Me.txtSelectedPath = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnOk = New DevComponents.DotNetBar.ButtonX()
        Me.btnCancel = New DevComponents.DotNetBar.ButtonX()
        CType(Me.tvCloud, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'tvCloud
        '
        Me.tvCloud.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.tvCloud.AllowDrop = True
        Me.tvCloud.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.tvCloud.BackgroundStyle.Class = "TreeBorderKey"
        Me.tvCloud.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.tvCloud.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvCloud.ExpandButtonType = DevComponents.AdvTree.eExpandButtonType.Triangle
        Me.tvCloud.ExpandLineColor = System.Drawing.Color.Transparent
        Me.tvCloud.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.tvCloud.Location = New System.Drawing.Point(0, 0)
        Me.tvCloud.Name = "tvCloud"
        Me.tvCloud.Nodes.AddRange(New DevComponents.AdvTree.Node() {Me.Node1})
        Me.tvCloud.NodesConnector = Me.NodeConnector1
        Me.tvCloud.NodeStyle = Me.ElementStyle1
        Me.tvCloud.PathSeparator = ";"
        Me.tvCloud.Size = New System.Drawing.Size(577, 478)
        Me.tvCloud.Styles.Add(Me.ElementStyle1)
        Me.tvCloud.TabIndex = 0
        Me.tvCloud.Text = "AdvTree1"
        '
        'Node1
        '
        Me.Node1.Expanded = True
        Me.Node1.Name = "Node1"
        Me.Node1.Text = "Node1"
        '
        'NodeConnector1
        '
        Me.NodeConnector1.LineColor = System.Drawing.Color.Transparent
        '
        'ElementStyle1
        '
        Me.ElementStyle1.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle1.Name = "ElementStyle1"
        Me.ElementStyle1.TextColor = System.Drawing.SystemColors.ControlText
        '
        'txtSelectedPath
        '
        Me.txtSelectedPath.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtSelectedPath.Border.Class = "TextBoxBorder"
        Me.txtSelectedPath.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSelectedPath.ForeColor = System.Drawing.Color.Black
        Me.txtSelectedPath.Location = New System.Drawing.Point(3, 3)
        Me.txtSelectedPath.Name = "txtSelectedPath"
        Me.txtSelectedPath.ReadOnly = True
        Me.txtSelectedPath.Size = New System.Drawing.Size(571, 21)
        Me.txtSelectedPath.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnOk)
        Me.Panel1.Controls.Add(Me.btnCancel)
        Me.Panel1.Controls.Add(Me.txtSelectedPath)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 478)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(577, 57)
        Me.Panel1.TabIndex = 2
        '
        'btnOk
        '
        Me.btnOk.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnOk.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.btnOk.Location = New System.Drawing.Point(418, 29)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(75, 23)
        Me.btnOk.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnOk.TabIndex = 2
        Me.btnOk.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.btnCancel.Location = New System.Drawing.Point(499, 29)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "Cancel"
        '
        'frmCloudBrowser
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(577, 535)
        Me.ControlBox = False
        Me.Controls.Add(Me.tvCloud)
        Me.Controls.Add(Me.Panel1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCloudBrowser"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Dropbox"
        CType(Me.tvCloud, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tvCloud As DevComponents.AdvTree.AdvTree
    Friend WithEvents Node1 As DevComponents.AdvTree.Node
    Friend WithEvents NodeConnector1 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle1 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents txtSelectedPath As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnOk As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnCancel As DevComponents.DotNetBar.ButtonX
End Class
