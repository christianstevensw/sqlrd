﻿Imports DevComponents

Imports System.ServiceProcess
Public Class frmMainWin
    Dim iconSize As Size = New Size(32, 32)
    Public nodeToFind As Object
    Private m_ViewStyle As AdvTree.eView = AdvTree.eView.Tile
    Dim oUI As clsMarsUI = New clsMarsUI
    Dim folderImage, forbiddenFolderImage As Image
    Dim folderImageSize As Size = New Size(20, 20)
    Dim folderTree As DevComponents.AdvTree.AdvTree = tvMain
    Dim myTag As Guid
    Dim IsFormLoaded As Boolean
    Public Shared itemToSelect As genericExplorerObject
    Friend WithEvents tmClick As System.Timers.Timer = New System.Timers.Timer
    Dim m_userGroup As usergroup
    Dim m_allowedFolders As System.Collections.Generic.List(Of Integer)
    Dim m_isGroupFolderAccessRestricted As Boolean
    Dim userMan As frmUserManager = New frmUserManager

    Public Property viewStyle() As AdvTree.eView
        Get
            Return m_ViewStyle
        End Get
        Set(ByVal value As AdvTree.eView)
            m_ViewStyle = value

            tvMain.View = value
        End Set
    End Property

    Private Sub cmbTheme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub frmMainWin_FormClosed(sender As Object, e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            tmSchedulerInfo.Stop()
            tmAutoRefreshUI.Stop()
        Catch : End Try

        gwindows.Remove(Me)

        If gwindows.Count = 0 Then
            Application.Exit()
        End If
    End Sub
    Private Sub checkODBCViolation()
        Dim ShowMsg As Boolean = False
        Dim sType As String
        Dim sMsg As String

        If IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.sa6_StoreInODBC) = False And gConType = "ODBC" Then

            sType = "an ODBC/SQL Server"

            ShowMsg = True
        End If

        If ShowMsg = True Then
            sMsg = "Your current SQL-RD edition does not allows the use of " & sType & " datasource. " & _
            "Press OK to migrate your SQL-RD to file system"

            MessageBox.Show(sMsg, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            clsMigration.MigrateSystem(clsMigration.m_datConString, "", "")

            'Dim oSQL As New frmSQLAdmin

            'oSQL._ReturntoDat(True)

            oUI.SaveRegistry("ConType", "DAT")
        End If

    End Sub

    Sub checkRemoteConnection()
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData

        SQL = "SELECT * FROM RemoteSysAttr WHERE AutoConnect =1"

        oRs = clsMarsData.GetData(SQL)

        oUI.BusyProgress(90, "Loading...")

        If oRs.EOF = False Then
            Dim oRemote As New frmRemoteAdmin
            Dim sPath As String

            gPCName = oRemote.ConnectRemote(oRs("livepath").Value)

            If gPCName.Length > 0 Then


                sPath = oRs("livepath").Value

                nWindowCount = 0

                'close data connect
                oData.CloseMainDataConnection()

                ' sCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sPath & "\crdlive.dat;Persist Security Info=False"

                Dim remoteConfig As String = "\\" & gPCName & "\christiansteven\sqlrd\sqlrdlive.config"

                If IO.File.Exists(remoteConfig) = False Then
                    Throw New Exception("Could not connect to the remote server as the  share named 'CHRISTIANSTEVEN' does not exist. Please upgrade server to the latest version and try again.")
                End If

                Dim remoteconType As String = clsMarsUI.MainUI.ReadRegistry("ConType", "DAT", , remoteConfig, True)

                If remoteconType = "DAT" Then
                    sCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sPath & "\sqlrdlive.dat;Persist Security Info=False"
                Else
                    sCon = clsMarsUI.MainUI.ReadRegistry("ConString", "", True, remoteConfig, True)

                    If sCon = "" Then sCon = "Provider=SQLOLEDB.1;Password=cssAdmin12345$;Persist Security Info=True;User ID=sa;Initial Catalog=SLQ-RD;Data Source=" & gPCName & "\SQLRD"
                End If

                oData.OpenMainDataConnection()

                gIsRemoteSystem = True

                oData.UpgradeCRDDb()

                sAppPath = sPath & "\"

                Try
                    Me.Text = "SQL-RD SEVEN " & gsEdition & " Edition - on Machine:\\" & gPCName
                Catch ex As Exception
                    ''console.writeline(ex.ToString)
                End Try

                Try
                    btnRemoteConnect.Enabled = False
                    btnRemoteDisconnect.Enabled = True
                Catch : End Try
            End If

        End If
    End Sub
    Sub processLoadingData()
        If Date.Now.Subtract(New Date(Date.Now.Year, 4, 1)).TotalDays = 0 Or Date.Now.Subtract(New Date(Date.Now.Year, 2, 27)).TotalDays = 0 Then
            Me.Text = "SQL-RD STEVEN - " & gsEdition.ToUpper & " EDITION"
        Else
            Me.Text = "SQL-RD SEVEN - " & gsEdition.ToUpper & " EDITION"
        End If

        If gnEdition = MarsGlobal.gEdition.EVALUATION Or (gnEdition = gEdition.SILVER And nTimeLeft <> 0) Then
            Dim sCustNo As String

            sCustNo = oUI.ReadRegistry("CustNo", "")

            Me.Text &= " [" & nTimeLeft & " DAYS LEFT]"
        End If

        Dim FirstRun As Boolean

        Try
            FirstRun = oUI.ReadRegistry("FirstRun", 1)
        Catch ex As Exception
            FirstRun = True
        End Try

        If FirstRun = True Then
            oUI.SaveRegistry("FirstRun", 0)

            Dim oConfig As New frmOptions

            oConfig.ShowDialog()
        End If

    End Sub

    Private Sub frmMainWin_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            Dim size As String = Me.Width & "," & Me.Height

            oUI.SaveRegistry("frmWindow", size)

            clsMarsUI.MainUI.SaveRegistry("ExplorerWidth", tvExplorer.Width)
            'clsSettingsManager.Appconfig.savetoConfigFile(gConfigFile)

            clsSystemTools.BackConfigFile(clsSystemTools.configSaveTime.onClose)

        Catch ex As Exception

        End Try
    End Sub

    Dim tmAutoRefreshUI As System.Windows.Forms.Timer = New System.Windows.Forms.Timer

    Sub processUIControls()
        Try
            Try
                ExpandableSplitter1.Expanded = Convert.ToBoolean(CType(oUI.ReadRegistry("ExpandSplitter", 1), Integer))
            Catch ex As Exception
                ExpandableSplitter1.Expanded = True
            End Try

            Dim windowState As String = oUI.ReadRegistry("MainWindowState", "Normal")

            Select Case windowState
                Case "Normal"
                    Me.WindowState = FormWindowState.Normal


                    Dim size As String = oUI.ReadRegistry("frmWindow", "824,638")

                    Me.Width = size.Split(",")(0)
                    Me.Height = size.Split(",")(1)

                Case "Maximized"
                    Me.WindowState = FormWindowState.Maximized
                Case "Minimized"
                    Me.WindowState = FormWindowState.Minimized
            End Select

            dnbRight.AutoHide = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("AutoHideDNBRight", 1))
            dnbRight.SelectedDockTab = clsMarsUI.MainUI.ReadRegistry("DNBRightSelectedDockTab", 0)

            Dim dnbWidth As Integer = clsMarsUI.MainUI.ReadRegistry("dnbRightWidth", 0)

            If dnbRight.AutoHide = False And dnbWidth <> 0 Then
                dnbRight.Width = dnbWidth
            End If

            StyleManager1.Style = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("appThemeX", 5))

            Select Case StyleManager1.Style
                Case 3
                    btnVistatheme.Checked = True
                Case 4
                    btnSilvertheme.Checked = True
                Case 5
                    btnBluetheme.Checked = True
                Case 6
                    btnBlackTheme.Checked = True
                Case 7
                    btnWin7theme.Checked = True
                Case 8
                    btnFutureTheme.Checked = True
                Case 9
                    btnMetroTheme.Checked = True
            End Select

            tvExplorer.Width = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("ExplorerWidth", 250))

            Me.RibbonControl1.Expanded = Convert.ToInt16(clsMarsUI.MainUI.ReadRegistry("RBCStatus", 1))

            If RibbonControl1.Expanded Then
                RibbonControl1.SelectedRibbonTabItem = rtabCreate
            End If

            Try
                swFullSystemSearch.Value = Convert.ToInt16(clsMarsUI.MainUI.ReadRegistry("FullSystemSearch", 0))
            Catch ex As Exception
                swFullSystemSearch.Value = False
            End Try


            Try
                Dim autoRefresh As Boolean = Convert.ToInt16(clsMarsUI.MainUI.ReadRegistry("DTRefresh", 0))

                If autoRefresh = True Then
                    Dim refreshInt As Integer = Convert.ToInt16(clsMarsUI.MainUI.ReadRegistry("DTRefreshInterval", 0))

                    tmAutoRefreshUI.Interval = refreshInt * 1000

                    AddHandler tmAutoRefreshUI.Tick, AddressOf uiRefreshTimerElapsed

                    tmAutoRefreshUI.Start()
                End If
            Catch : End Try

            Dim collabo As Boolean = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("collaboration", 0))

            btnCollaborationServer.Enabled = collabo

            If collabo = True Then

                Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT collaboID, serverName FROM collaboratorsAttr")

                cmbCollaborationServer.Items.Clear()

                cmbCollaborationServer.Items.Add("DEFAULT")

                If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                    Do While oRs.EOF = False
                        cmbCollaborationServer.Items.Add(oRs("serverName").Value.ToString.ToUpper)
                        oRs.MoveNext()
                    Loop

                    oRs.Close()
                    oRs = Nothing
                End If
            End If
        Catch : End Try
    End Sub

    Private Sub uiRefreshTimerElapsed(source As Object, e As EventArgs)
        btnRefresh_Click(Nothing, Nothing)
    End Sub
    Private Sub frmMainWin_GotFocus(sender As Object, e As System.EventArgs) Handles Me.GotFocus
        selectedForm = Me
        crdxCommon.executingForm = Me
    End Sub

    Private Function searchTree(key As Integer, Optional parent As AdvTree.Node = Nothing) As AdvTree.Node
        Try
            Dim nx As AdvTree.Node = tvExplorer.FindNodeByDataKey(key)

            If nx IsNot Nothing AndAlso nx.Tag.ToString.ToLower = "folder" Then Return nx

            If parent Is Nothing Then
                For Each nn As AdvTree.Node In tvExplorer.Nodes(0).Nodes
                    If nn.DataKey = key And nn.Tag.ToString.ToLower = "folder" Then
                        Return nn
                    Else
                        If nn.Tag.ToString.ToLower = "folder" Then
                            Dim nz As AdvTree.Node = searchTree(key, nn)

                            If nz IsNot Nothing Then Return nz
                        End If
                    End If
                Next
            Else
                For Each nn As AdvTree.Node In parent.Nodes
                    If nn.DataKey = key And nn.Tag.ToString.ToLower = "folder" Then
                        Return nn
                    Else
                        If nn.Tag.ToString.ToLower = "folder" Then
                            Dim nz As AdvTree.Node = searchTree(key, nn)

                            If nz IsNot Nothing Then Return nz
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Sub processPermissions()
        '//permissions and shit
        If clsMarsSecurity._HasGroupAccess("Activation & Deactivation", , , True) = False Then
            stabActivation.Enabled = False
        Else
            stabActivation.Enabled = True
        End If

        If clsMarsSecurity._HasGroupAccess("Custom Calendar", , , True) = False Then
            btnCustomCals.Enabled = False
        End If

        If clsMarsSecurity._HasGroupAccess("Data Items", , , True) = False Then
            btnDataItems.Enabled = False
        End If

        If clsMarsSecurity._HasGroupAccess("Operational Hours", , , True) = False Then
            btnOperationHours.Enabled = False
        End If

        If clsMarsSecurity._HasGroupAccess("Options", , , True) = False Then
            btnOptions.Enabled = False
            btnOptions1.Enabled = False
        End If

        If clsMarsSecurity._HasGroupAccess("SMTP Servers Manager", , , True) = False Then
            btnSMTPServers.Enabled = False
        End If

        If clsMarsSecurity._HasGroupAccess("System Monitor", , , True) = False Then
            btnSystemMonitor.Enabled = False
            btnSystemMonitorLarge.Enabled = False
            btnCollaboration.Enabled = False
        End If

        If clsMarsSecurity._HasGroupAccess("Remote Administration", , , True) = False Then
            stabRemoteAdmin.Enabled = False
        End If

        If clsMarsSecurity._HasGroupAccess("Export Schedules", , , True) = False Then
            btnExportSchedules.Enabled = False
        End If

        If clsMarsSecurity._HasGroupAccess("Backup & Restore", , , True) = False Then
            stabBackRestore.Enabled = False
        End If

        If clsMarsSecurity._HasGroupAccess("Folder Management", , , True) = False Then
            btnNewFolder.Enabled = False
            btnNewSmartFolder.Enabled = False
        End If

        If clsMarsSecurity._HasGroupAccess("User Manager", , , True) = False Then
            btnUserManager.Enabled = False
        End If


        If clsMarsSecurity._HasGroupAccess("System Migration", , , True) = False Then
            stabSwitch.Enabled = False
            stabMigrate.Enabled = False
            btnLoginInfo.Enabled = False
        End If

        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules", , , True) = False Then
            stabSchedules.Enabled = False
        End If
    End Sub

    Private Sub frmMainWin_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Bar1.Text = "Status"

        RibbonControl1.Enabled = False
        tvExplorer.Enabled = False
        tvMain.Enabled = False
        processUIControls()

        If gIsLoggedIn = False Then
            Dim oSplash As frmSplash = New frmSplash

            oSplash.ShowDialog()

            If oSplash.IsValidated = False Then
                End
            End If

            _FirstRun()

            checkODBCViolation()

            gIsLoggedIn = True
        End If

        processLoadingData()
        checkRemoteConnection()

        gParent = ""

        If isNewActivationSystem Then stabFeatures.Visible = False

        tvExplorer.PathSeparator = "\"

        Dim viewStyleString As String = clsMarsUI.MainUI.ReadRegistry("WindowViewStyle", "tile")

        If viewStyleString = "tile" Then
            viewStyle = AdvTree.eView.Tile
            setIconSize()
        Else
            viewStyle = AdvTree.eView.Tree
            iconSize = New Size(20, 20)
            btnDetailStyle.Checked = True
        End If

        loadFolders()

        buildCrumbBar()

        configureFolderSettingsTable()
        configurecolumnSettingsTable()
        configureFolderObjects()

        If nodeToFind IsNot Nothing Then
            Dim nodes As AdvTree.Node = tvExplorer.FindNodeByDataKey(nodeToFind)

            If nodes IsNot Nothing Then
                tvExplorer.SelectedNode = nodes
                nodes.EnsureVisible()
            End If
        Else
            Dim lastFolderID As Integer = clsMarsUI.MainUI.ReadRegistry("SelectedFolder", 0)

            If lastFolderID <> 0 Then
                Dim node As AdvTree.Node = searchTree(lastFolderID) 'tvExplorer.FindNodeByDataKey(lastFolderID)

                If node IsNot Nothing Then
                    tvExplorer.SelectedNode = node
                    node.EnsureVisible()
                End If
            End If
        End If


        Dim sched As schedulerInfo = New schedulerInfo

        advProps.SelectedObject = sched

        Select Case gConType.ToUpper
            Case "LOCAL"
                btnSwitchToLocal.Enabled = False
                btnSwitchToODBC.Enabled = True
                btnMigrateToLocal.Enabled = False
                btnMigratetoODBC.Enabled = True
                btnLoginInfo.Enabled = False
            Case "ODBC"
                btnSwitchToLocal.Enabled = True
                btnSwitchToODBC.Enabled = False
                btnMigrateToLocal.Enabled = True
                btnMigratetoODBC.Enabled = False
                btnLoginInfo.Enabled = True
            Case "DAT"
                btnSwitchToLocal.Enabled = True
                btnSwitchToODBC.Enabled = True
                btnMigrateToLocal.Enabled = True
                btnMigratetoODBC.Enabled = True
                btnLoginInfo.Enabled = False
        End Select

        '//add me to the hashtable
        Me.Tag = Guid.NewGuid

        gwindows.Add(Me)
        crdxCommon.executingForm = Me

        If MailType <> MarsGlobal.gMailType.SMTP Then
            btnSMTPServers.Enabled = False
        End If

        clsSystemTools.BackConfigFile(clsSystemTools.configSaveTime.onOpen)

        If tvMain.SelectedNode Is Nothing Then ContextMenuBar1.SetContextMenuEx(tvMain, mnudesktopMenu)

        IsFormLoaded = True

        RibbonControl1.Enabled = True
        tvExplorer.Enabled = True
        tvMain.Enabled = True
        tmCheckUpdates.Interval = 5000
        tmCheckUpdates.Start()
        oUI.BusyProgress(100, "Loading...", True)

        updateScheduleCount()

        processPermissions()


        '//show welcome screen or dashboard
        If UcScheduleCount1.numberOfReports < 10 Then
            Dim noShow As Boolean = Convert.ToInt16(clsMarsUI.MainUI.ReadRegistry("DontShowWelcomeScreen", 0))

            If noShow = False Then
                Dim starter As frmStarter = New frmStarter
                starter.ShowDialog()
            End If
        Else
            Try
                Dim DoShow As Boolean = clsMarsUI.MainUI.ReadRegistry("OpenDashOnstartup", True)

                If DoShow = True And gConType <> "DAT" Then
                    Dim p As Process = New Process
                    p.StartInfo.FileName = IO.Path.Combine(sAppPath, "Dashboard\dashboard.exe")

                    p.Start()
                End If
            Catch : End Try
        End If

        Try
            tvExplorer.Nodes(0).Expand()
            tmClick.Interval = 1000
            tmClick.Start()
        Catch : End Try

        tvExplorer.AllowDrop = True
        tvExplorer.DragDropEnabled = True

    End Sub

    Public Sub updateScheduleCount()
        If nMax > 0 Then
            UcScheduleCount1.Visible = True

            Dim reportCount As Integer

            CheckViolation(reportCount, True)

            If reportCount > 50 Then reportCount = 50

            UcScheduleCount1.numberOfReports = reportCount
        End If
    End Sub
    Sub configureFolderObjects()
        If dtfolderObjects Is Nothing Then
            dtfolderObjects = New DataTable("folderObjects")

            If IO.File.Exists(IO.Path.Combine(sAppDataPath, "folderObjects.xml")) Then
                Try
                    dtfolderObjects.ReadXml(IO.Path.Combine(sAppDataPath, "folderObjects.xml"))
                Catch ex As Exception
                    IO.File.Delete(IO.Path.Combine(sAppDataPath, "folderObjects.xml"))

                    With dtfolderObjects.Columns
                        .Add("folderid")
                        .Add("objectid")
                    End With
                End Try
            Else
                With dtfolderObjects.Columns
                    .Add("folderid")
                    .Add("objectid")
                End With
            End If
        End If
    End Sub

    Sub configureFolderSettingsTable()
        '//foldersettings that store the folder view etc
        If dtFolderSettings Is Nothing Then

            dtFolderSettings = New DataTable("folderSettings")

            If IO.File.Exists(IO.Path.Combine(sAppDataPath, "folderSettings.xml")) Then
                Try
                    dtFolderSettings.ReadXml(IO.Path.Combine(sAppDataPath, "folderSettings.xml"))
                Catch
                    '//if there is an issue reading it then delete it
                    IO.File.Delete(IO.Path.Combine(sAppDataPath, "folderSettings.xml"))

                    With dtFolderSettings.Columns
                        .Add("folderid")
                        .Add("viewstyle")
                        .Add("iconSize")
                    End With
                End Try
            Else
                With dtFolderSettings.Columns
                    .Add("folderid")
                    .Add("viewstyle")
                    .Add("iconSize")
                End With
            End If
        End If
    End Sub


    Sub configurecolumnSettingsTable()
        '//foldersettings that store the folder view etc

        Try
            IO.File.Delete(IO.Path.Combine(sAppDataPath, "columnSettings.xml"))
        Catch :End Try

        If dtcolumnFolderSettings Is Nothing Then

            dtcolumnFolderSettings = New DataTable("columnSettings")

            If IO.File.Exists(IO.Path.Combine(sAppDataPath, "nodeColumnSettings.xml")) Then
                Try
                    dtcolumnFolderSettings.ReadXml(IO.Path.Combine(sAppDataPath, "nodeColumnSettings.xml"))
                Catch
                    '//if there is an issue reading it then delete it
                    IO.File.Delete(IO.Path.Combine(sAppDataPath, "nodeColumnSettings.xml"))

                    With dtcolumnFolderSettings.Columns
                        .Add("folderid")
                        .Add("nodetext")
                        .Add("columnName")
                        .Add("width")
                    End With
                End Try
            Else
                With dtcolumnFolderSettings.Columns
                    .Add("folderid")
                    .Add("nodetext")
                    .Add("columnName")
                    .Add("width")
                End With
            End If
        End If
    End Sub

    Sub setIconSize()

        Dim sz As Integer = clsMarsUI.MainUI.ReadRegistry("iconsize", 32)

        iconSize = New Size(sz, sz)

        If sz = 16 Then
            slSize.Value = 0
        ElseIf sz = 24 Then
            slSize.Value = 25
        ElseIf sz = 32 Then
            slSize.Value = 50
        ElseIf sz = 64 Then
            slSize.Value = 75
        Else
            slSize.Value = 32
        End If
    End Sub

    Sub setIconSize(folderID As Integer)
        Dim szString As String = clsMarsUI.MainUI.ReadRegistry("iconsize", 32)
        Dim sz As Integer = 32
        ' Dim rows() As DataRow = dtFolderSettings.Select("folderid =" & folderID)


        For Each r As DataRow In dtFolderSettings.Rows
            If r("folderid") = folderID Then
                sz = r("iconsize")
                Exit For
            End If
        Next

        iconSize = New Size(sz, sz)

        If sz = 16 Then
            slSize.Value = 0
        ElseIf sz = 24 Then
            slSize.Value = 25
        ElseIf sz = 32 Then
            slSize.Value = 50
        ElseIf sz = 64 Then
            slSize.Value = 75
        Else
            slSize.Value = 50
        End If
    End Sub

    Private Function workingTables() As DataTable
        Dim tablesDT As DataTable = Nothing

        Dim tablestoreload() As String = New String() {"reportattr", "scheduleattr", "packageattr", "automationattr", "eventattr6", "eventpackageattr", "destinationattr"}

        tablesDT = New DataTable("reload")

        With tablesDT.Columns
            .Add("TABLE_NAME")
            .Add("TABLE_TYPE")
        End With

        For Each table As String In tablestoreload
            Dim row As DataRow = tablesDT.Rows.Add

            row("TABLE_NAME") = table
            row("TABLE_TYPE") = "TABLE"
        Next

        Return tablesDT
    End Function

    'Sub refreshDatasetOnly(tablesToReload As String())
    '    Dim tablesDT As DataTable = Nothing

    '    If tablesToReload IsNot Nothing Then

    '        tablesDT = New DataTable("reload")

    '        With tablesDT.Columns
    '            .Add("TABLE_NAME")
    '            .Add("TABLE_TYPE")
    '        End With

    '        For Each table As String In tablesToReload
    '            Dim row As DataRow = tablesDT.Rows.Add

    '            row("TABLE_NAME") = table
    '            row("TABLE_TYPE") = "TABLE"
    '        Next
    '    End If

    '    dataLoadIntoDataset(tablesDT)
    'End Sub

    'Sub dataLoadIntoDataset(tablesDT As DataTable)
    '    Dim dm As DataMigrator


    '    Dim srcdsn As String = ""

    '    If mySettings.conType = DataMigrator.dbType.DAT Then
    '        dm = New DataMigrator(DataMigrator.dbType.DAT)
    '    ElseIf mySettings.conType = DataMigrator.dbType.ODBC Then
    '        dm = New DataMigrator(DataMigrator.dbType.ODBC)

    '        Dim conString As String = clsMarsUI.MainUI.ReadRegistry("constring", "", True, , True)

    '        If conString <> "" Then
    '            Dim dsn As String = conString.Split(";")(4).Split("=")(1)
    '            Dim user As String = conString.Split(";")(3).Split("=")(1)
    '            Dim pwd As String = conString.Split(";")(1).Split("=")(1)

    '            srcdsn = "DSN=" & dsn & ";Uid=" & user & ";Pwd=" & pwd & ";"
    '        Else
    '            conString = clsMarsUI.MainUI.ReadRegistry("constring2", "", , , True)

    '            If conString = "" Then Throw New Exception("Could not find valid connection string to database")

    '            Dim dsn As String = conString.Split("|")(0)
    '            Dim user As String = conString.Split("|")(1)
    '            Dim pwd As String = _DecryptDBValue(conString.Split("|")(2))

    '            srcdsn = "DSN=" & dsn & ";Uid=" & user & ";Pwd=" & pwd & ";"
    '        End If
    '    ElseIf mySettings.conType = DataMigrator.dbType.LOCALSQL Then
    '        dm = New DataMigrator(DataMigrator.dbType.LOCALSQL)
    '    End If

    '    '//create the database snapshot and save it to disk

    '    dm.tablesToInclude = tablesDT

    '    mainDS = dm.Export(srcdsn)

    '    dm = Nothing

    'End Sub
    ''' <summary>
    ''' Refreshes the dataset and also reloads the main views
    ''' </summary>
    ''' <param name="folderID"></param>
    ''' <remarks></remarks>
    Sub loadData(Optional ByVal folderID As Integer = 0, Optional reloadTreeView As Boolean = False)
        Try
            tvMain.BeginUpdate()

            Cursor.Current = Cursors.WaitCursor

            If viewStyle = AdvTree.eView.Tile Then
                buildTiles(New folder(folderID))
            Else
                buildDetails(New folder(folderID))
            End If

            Try
                If folderID <> 0 Then
                    Dim nn As AdvTree.Node = searchTree(folderID)
                    Dim fld As folder = New folder(folderID)

                    nn.Text = fld.folderName & " (" & fld.getTotalContents & ")"
                End If

                If reloadTreeView Then loadFolders(folderID)
            Catch : End Try

            Cursor.Current = Cursors.Default
        Catch e As Exception

        Finally
            tvMain.EndUpdate()
        End Try
    End Sub
    ''' <summary>
    ''' reloads the folder tree and optional ID to select a folder
    ''' </summary>
    ''' <param name="selectedfolderID"></param>
    ''' <remarks></remarks>
    Sub loadFolders(Optional selectedfolderID As Integer = 0)
        tvExplorer.BeginUpdate()
        tvExplorer.Nodes.Clear()

        Dim rootNode As AdvTree.Node = New AdvTree.Node("Home")
        rootNode.Tag = "folder"
        rootNode.DataKey = 0
        rootNode.Image = Image.FromFile(getAssetLocation("node.png"))
        tvExplorer.Nodes.Add(rootNode)
        rootNode.Expand()

        Dim topLevel As folder = New folder(0)

        folderImage = resizeImage(My.Resources.Folder_256x2561, folderImageSize)
        forbiddenFolderImage = resizeImage(My.Resources.Folder_256x256_forbidden, folderImageSize)

        For Each fld As folder In topLevel.getSubFolders
            Dim folderName As String = fld.folderName & " (" & fld.getTotalContents & ")"
            Dim folderID As Integer = fld.folderID

            Using node As DevComponents.AdvTree.Node = New DevComponents.AdvTree.Node(folderName)
                node.Tag = "folder"
                node.Image = folderImage
                node.DataKey = folderID

                If gRole <> "Administrator" Then
                    If m_allowedFolders Is Nothing Then
                        If m_userGroup Is Nothing Then
                            m_userGroup = New usergroup(gRole)
                        End If

                        m_allowedFolders = m_userGroup.userAllowedFolders
                        m_isGroupFolderAccessRestricted = m_userGroup.isFolderAccessRestricted
                    End If

                    If m_allowedFolders.Contains(fld.folderID) = False And m_isGroupFolderAccessRestricted Then
                        node.Image = forbiddenFolderImage
                    End If
                End If

                rootNode.Nodes.Add(node)

                populatefolderChildren(folderID, node)
            End Using
        Next

        addSmartFolders()
        addSystemFolders()

        If selectedfolderID <> 0 Then
            Dim nn As AdvTree.Node = searchTree(selectedfolderID)

            If nn IsNot Nothing Then
                tvExplorer.SelectedNode = nn
                nn.EnsureVisible()
            End If
        End If

        tvExplorer.EndUpdate()

        ' _Delay(1)

        'tvExplorer.Refresh()


    End Sub

    Sub expandNode(node As AdvTree.Node)
        Dim parentNode As AdvTree.Node = node
        Dim arr As ArrayList = New ArrayList

        Do
            If parentNode IsNot Nothing Then
                arr.Add(parentNode)
                parentNode = parentNode.Parent
            End If
        Loop Until parentNode Is Nothing

        arr.Reverse()

        For Each nn As AdvTree.Node In arr
            nn.Expand(AdvTree.eTreeAction.Expand)
        Next

        'node.EnsureVisible()
        'node.Expanded = True
        'node.ExpandAll()
        'If node.Parent IsNot Nothing Then
        '    expandNode(node.Parent)
        'End If
    End Sub
    Public Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Dim tmp As AdvTree.Node = tvExplorer.SelectedNode
        Dim tmpID As Integer = 0
        Dim itemNode As AdvTree.Node = tvMain.SelectedNode
        Dim path As String = ""

        If tmp IsNot Nothing Then
            path = tmp.FullPath
            tmpID = tmp.DataKey
        End If


        ' loadData()
        loadFolders()

        tmp = searchTree(tmpID)

        If tmp IsNot Nothing Then
            tvExplorer.SelectedNode = tmp
            tmp.EnsureVisible()
        End If

        If Date.Now.Subtract(New Date(Date.Now.Year, 4, 1)).TotalDays = 0 Or Date.Now.Subtract(New Date(Date.Now.Year, 2, 27)).TotalDays = 0 Then
            Me.Text = "SQL-RD STEVEN - " & path
        Else
            Me.Text = "SQL-RD SEVEN - " & path
        End If


        If itemNode IsNot Nothing Then
            tvMain.SelectedNode = itemNode
            itemNode.EnsureVisible()
        End If
    End Sub
    Private Sub loadEventPackageContents()
        If tvExplorer.SelectedNode Is Nothing Then Return

        tvMain.Nodes.Clear()

        Dim parent As DevComponents.AdvTree.Node = tvExplorer.SelectedNode
        Dim pack As EventBasedPackage = New EventBasedPackage(parent.DataKey)

        '//reload the tables
        Dim tablestoreload() As String = New String() {"reportattr", "scheduleattr", "packageattr", "automationattr", "eventattr6", "eventpackageattr", "destinationattr"}

        '  refreshDatasetOnly(tablestoreload)

        tvMain.BeginUpdate()

        Dim style As AdvTree.eView = getViewStyleForFolder(tvExplorer.SelectedNode.DataKey)

        If style = AdvTree.eView.Tile Then
            buildTiles(pack)
        Else
            buildDetails(pack)
        End If

        tvMain.EndUpdate()
    End Sub
    Private Sub loadPackageContents()
        Try
10:         If tvExplorer.SelectedNode Is Nothing Then Return

20:         tvMain.Nodes.Clear()

30:         Dim parent As DevComponents.AdvTree.Node = tvExplorer.SelectedNode
40:         Dim pack As Package = New Package(parent.DataKey)

            '//reload the tables
50:         Dim tablestoreload() As String = New String() {"reportattr", "scheduleattr", "packageattr", "automationattr", "eventattr6", "eventpackageattr", "destinationattr"}

            '  refreshDatasetOnly(tablestoreload)

60:         tvMain.BeginUpdate()

70:         Dim style As AdvTree.eView = getViewStyleForFolder(tvExplorer.SelectedNode.DataKey)

80:         If style = AdvTree.eView.Tile Then
90:             buildTiles(pack)
            Else
100:            buildDetails(pack)
            End If

110:        tvMain.EndUpdate()
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        Finally
            tvMain.EndUpdate()
        End Try
    End Sub

    Private Sub refreshFolderContents()
10:     Try
20:         If tvExplorer.SelectedNode Is Nothing Then Return

30:         tvMain.Nodes.Clear()

            Dim parent As DevComponents.AdvTree.Node = tvExplorer.SelectedNode
            Dim parentFolder As Object

40:         If parent.Tag = "folder" Then
50:             parentFolder = New folder(parent.DataKey)
60:         ElseIf parent.Tag = "smartfolder" Then
70:             parentFolder = New smartfolder(parent.DataKey)
            End If



            '//reload the tables
80:         Dim tablestoreload() As String = New String() {"reportattr", "scheduleattr", "packageattr", "automationattr", "eventattr6", "eventpackageattr", "destinationattr"}

            ' refreshDatasetOnly(tablestoreload)

90:         tvMain.BeginUpdate()

            Dim style As AdvTree.eView = getViewStyleForFolder(tvExplorer.SelectedNode.DataKey)

100:        If gRole <> "Administrator" Then
110:            m_userGroup = New usergroup(gRole)
120:            m_allowedFolders = m_userGroup.userAllowedFolders
130:            m_isGroupFolderAccessRestricted = m_userGroup.isFolderAccessRestricted
            End If


140:        If style = AdvTree.eView.Tile Then
150:            buildTiles(parentFolder)
160:        Else
170:            buildDetails(parentFolder)
            End If

180:        tvMain.EndUpdate()
190:    Catch ex As Exception
200:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        Finally
            tvMain.EndUpdate()
        End Try
    End Sub


    Private Sub tvExplorer_AfterCellEdit(sender As Object, e As DevComponents.AdvTree.CellEditEventArgs) Handles tvExplorer.AfterCellEdit

        Dim sOld As String = tvExplorer.SelectedNode.Text

        Dim sType As String = tvExplorer.SelectedNode.Tag

        Dim nID As Integer = tvExplorer.SelectedNode.DataKey

        If e.NewText Is Nothing Then e.Cancel = True : Return

        If e.NewText.Length = 0 Then e.Cancel = True : Return

        If clsMarsSecurity._HasGroupAccess("Folder Management") = False Then
            e.Cancel = True
            Return
        End If

        If String.Compare(e.NewText, sOld, True) = 0 Then e.Cancel = True : Return

        Dim oData As New clsMarsData

        If sType = "smartfolder" Then
            If smartfolder.canRename(nID, e.NewText) = False Then
                _ErrorHandle("A smart folder with that name already exists", _
                -91, "frmWindow.tvFolders_AfterLabelEdit", 2019)
                e.Cancel = True

                Return
            End If

            Dim SQL As String

            SQL = "UPDATE SmartFolders SET " & _
            "SmartName = '" & SQLPrepare(e.NewText) & "' WHERE " & _
            "SmartID = " & nID

            If clsMarsData.WriteData(SQL) = False Then
                e.Cancel = True
            End If
        ElseIf sType = "folder" Then
            Dim parentID As Integer = 0
            Dim nn As DevComponents.AdvTree.Node

            Dim ui As clsMarsUI = New clsMarsUI

            If tvExplorer.SelectedNode.Parent Is Nothing Then
                parentID = 0
            Else
                parentID = tvExplorer.SelectedNode.Parent.DataKey
            End If

            If isAllowedAccessToFolder(nID) = False Then
                e.Cancel = True
                Return
            End If

            If ui.candoRename(e.NewText, parentID, clsMarsScheduler.enScheduleType.FOLDER, tvExplorer.SelectedNode.DataKey) = False Then
                MessageBox.Show(ui.renameErrorstring, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                e.Cancel = True
                Return
            End If

            Dim SQL As String

            SQL = "UPDATE folders SET " & _
            "folderName = '" & SQLPrepare(e.NewText) & "' WHERE " & _
            "folderid = " & nID

            If clsMarsData.WriteData(SQL) = False Then
                e.Cancel = True
            End If

        End If
    End Sub

    Dim _ignoreBit As Boolean = False
    Private Sub saveFolderLastSelectedObject(folderID As Integer, objectID As Integer)
        If dtfolderObjects IsNot Nothing Then
            Dim rows() As DataRow = dtfolderObjects.Select("folderid =" & folderID)

            If rows IsNot Nothing AndAlso rows.Length > 1 Then
                Dim row As DataRow = rows(0)

                row("objectid") = objectID
            Else
                Dim row As DataRow = dtfolderObjects.Rows.Add
                row("folderid") = folderID
                row("objectid") = objectID
            End If
        End If
    End Sub

    Private Sub getFolderLastSelectedObject(folderID As Integer)
        If dtfolderObjects IsNot Nothing Then
            For Each row As DataRow In dtfolderObjects.Rows
                If row("folderid") = folderID Then
                    Dim objectID As Integer = row("objectid")

                    Dim nn As AdvTree.Node = tvMain.FindNodeByDataKey(objectID)

                    If nn IsNot Nothing Then
                        tvMain.SelectedNode = nn

                        nn.EnsureVisible()

                        Return
                    End If
                End If
            Next
        End If
    End Sub


    Private Sub tvExplorer_AfterNodeSelect(ByVal sender As Object, ByVal e As DevComponents.AdvTree.AdvTreeNodeEventArgs) Handles tvExplorer.AfterNodeSelect
10:     Try
20:         Cursor.Current = Cursors.WaitCursor

30:         If tvExplorer.SelectedNode Is Nothing Then Return

40:         _ignoreBit = True

50:         If tvExplorer.SelectedNode.Tag = "systemfolder" Then
60:             ContextMenuBar1.SetContextMenuEx(tvExplorer, Nothing)
70:             viewSystemFolder()
80:         ElseIf tvExplorer.SelectedNode.Tag = "smartfolder" Then
90:             ContextMenuBar1.SetContextMenuEx(tvExplorer, mnuSmartFolders)
100:            refreshFolderContents()
110:        ElseIf tvExplorer.SelectedNode.Tag = "package" Then
120:            ContextMenuBar1.SetContextMenuEx(tvExplorer, Nothing)
130:            loadPackageContents()
            ElseIf tvExplorer.SelectedNode.Tag = "event-package" Then
                ContextMenuBar1.SetContextMenuEx(tvExplorer, Nothing)
131:            loadEventPackageContents()
140:        Else
150:            ContextMenuBar1.SetContextMenuEx(tvExplorer, mnuFolders)
160:            refreshFolderContents()
            End If

170:        ucHistory.reset()


180:        If tvExplorer.SelectedNode.Tag = "folder" Then
190:            updateCrumbs()
200:        Else
210:            Me.crumb.Items.Clear()
            End If

220:        ContextMenuBar1.SetContextMenuEx(tvMain, mnudesktopMenu)

230:        clsMarsUI.MainUI.SaveRegistry("SelectedFolder", tvExplorer.SelectedNode.DataKey)

240:        _ignoreBit = False

250:        If tvExplorer.SelectedNode IsNot Nothing Then getFolderLastSelectedObject(tvExplorer.SelectedNode.DataKey)
260:    Catch ex As Exception
270:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl())
280:    Finally
            Try
                tvExplorer.EndUpdate()
            Catch : End Try
290:        Cursor.Current = Cursors.Default
            End Try
    End Sub

    Sub viewSystemFolder()
        If tvExplorer.SelectedNode Is Nothing Then Return

        If tvExplorer.SelectedNode.Tag <> "systemfolder" Then Return

        Dim columns() As String = getDetailColumns()
        Dim sysfolder As systemfolder = New systemfolder

        tvMain.BeginUpdate()
        tvMain.Nodes.Clear()

        Select Case tvExplorer.SelectedNode.Text
            Case "Packages Due"
                Dim packagesDue As ArrayList = sysfolder.getpackagesDue

                If viewStyle = AdvTree.eView.Tile Then
                    addPackageTiles(packagesDue)
                Else
                    addPackagesToDetails(columns, packagesDue)
                End If
            Case "Single Schedules Due"
                Dim singlesDue As ArrayList = sysfolder.getreportsDue
                If viewStyle = AdvTree.eView.Tile Then
                    addReportTiles(singlesDue)
                Else
                    addReportsToDetails(columns, singlesDue)
                End If
            Case "Todays Failed Packages"
                Dim failedPacks As ArrayList = sysfolder.getTodaysFailedPackages

                If viewStyle = AdvTree.eView.Tile Then
                    addPackageTiles(failedPacks)
                Else
                    addPackagesToDetails(columns, failedPacks)
                End If
            Case "Todays Failed Single Schedules"
                Dim failedSingles As ArrayList = sysfolder.getTodaysFailedReports

                If viewStyle = AdvTree.eView.Tile Then
                    addReportTiles(failedSingles)
                Else
                    addReportsToDetails(columns, failedSingles)
                End If
        End Select

        tvMain.EndUpdate()
    End Sub
    Sub updateCrumbs()
        If tvExplorer.SelectedNode Is Nothing Then Return
        Dim node As DevComponents.AdvTree.Node = tvExplorer.SelectedNode
        Dim nodeID As Integer = node.DataKey
        Dim nodePath As String = node.FullPath

        If crumb.Items.Count = 0 Then
            buildCrumbBar()
        End If

        Dim startCrumb As DevComponents.DotNetBar.CrumbBarItem = crumb.Items(0)

        searchCrumbs(nodeID, startCrumb)

    End Sub

    Sub searchCrumbs(ByVal ID As Integer, ByVal searchCrumb As DevComponents.DotNetBar.CrumbBarItem)
        For Each it As DevComponents.DotNetBar.CrumbBarItem In searchCrumb.SubItems
            If it.Tag = ID Then
                crumb.SelectedItem = it
                Exit For
            Else
                searchCrumbs(ID, it)
            End If
        Next
    End Sub
    Sub populatefolderChildren(ByVal folderID As Integer, ByVal parentnode As DevComponents.AdvTree.Node)
        Dim parentFolder As folder = New folder(folderID)
        ' Dim folderImage As Image = resizeImage(My.Resources.Folder_256x2561, 16, 16)

        For Each fld As folder In parentFolder.getSubFolders
            Dim folderName As String = fld.folderName & " (" & fld.getTotalContents & ")"

            Using node As DevComponents.AdvTree.Node = New DevComponents.AdvTree.Node(folderName)
                node.Tag = "folder"
                node.Image = folderImage
                node.DataKey = fld.folderID
                parentnode.Nodes.Add(node)

                If gRole <> "Administrator" Then
                    If m_allowedFolders Is Nothing Then
                        If m_userGroup Is Nothing Then
                            m_userGroup = New usergroup(gRole)
                        End If

                        m_allowedFolders = m_userGroup.userAllowedFolders
                        m_isGroupFolderAccessRestricted = m_userGroup.isFolderAccessRestricted
                    End If

                    If m_allowedFolders.Contains(fld.folderID) = False And m_isGroupFolderAccessRestricted Then
                        node.Image = forbiddenFolderImage
                    End If
                End If

                populatefolderChildren(fld.folderID, node)
            End Using
        Next

        '//add the packages in the folder

        For Each pack As Package In parentFolder.getPackages
            Dim packageName As String = pack.packageName & " (" & pack.totalReports & ")"

            Using node As DevComponents.AdvTree.Node = New AdvTree.Node(packageName)
                node.Tag = "package"
                node.Image = resizeImage(pack.packageImage, 16, 16)
                node.DataKey = pack.ID
                parentnode.Nodes.Add(node)
            End Using
        Next

        '//add event-based packs
        For Each pack As EventBasedPackage In parentFolder.getEventBasedPackages
            Dim packageName As String = pack.eventPackageName & " (" & pack.totalEvents & ")"

            Using node As DevComponents.AdvTree.Node = New AdvTree.Node(packageName)
                node.Tag = "event-package"
                node.Image = resizeImage(pack.packageImage, 16, 16)
                node.DataKey = pack.ID
                parentnode.Nodes.Add(node)
            End Using
        Next
    End Sub

    Function getDetailColumns() As String()
        Dim columns() As String = New String() {"Name", "Execution Time", "Frequency", "Destination", "Last Result", "Last Run", _
          "End Date", "Last Refreshed", "Next Run", "Output Formats", "Repeat", "Repeat Interval", _
          "Repeat Until", "Report Path", "Schedule Description", "Start Date"}

        Dim SQL As String = "SELECT * FROM UserColumns WHERE Owner ='" & gUser & "' ORDER BY ColumnID"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then
            Return columns
        ElseIf oRs.EOF = True Then
            Return columns
        Else
            columns = Nothing
            Dim I As Integer = 0

            ReDim columns(I)

            columns(I) = "Name"

            I += 1

            Do While oRs.EOF = False
                ReDim Preserve columns(I)

                columns(I) = oRs("caption").Value

                I += 1
                oRs.MoveNext()
            Loop

            oRs.Close()
            oRs = Nothing

            Return columns
        End If
    End Function
    Sub buildDetails(ByVal parentFolder As Object)
        Try
10:         If parentFolder Is Nothing Then Return

20:         iconSize = New Size(20, 20)
30:         tvMain.Nodes.Clear()
40:         tvMain.Columns.Clear()

50:         tvMain.View = AdvTree.eView.Tree

            Dim columns() As String = getDetailColumns()

60:         If TypeOf parentFolder Is Package Then
                Dim reports As ArrayList = CType(parentFolder, Package).getReportsinPackage

70:             addReportsToDetails(columns, reports)

80:             Return
90:         ElseIf TypeOf parentFolder Is EventBasedPackage Then
                Dim evts() As EventBased = CType(parentFolder, EventBasedPackage).events
100:            Dim arr As ArrayList = New ArrayList

110:            If evts Is Nothing Then Return

120:            arr.AddRange(evts)

130:            addEventsToDetails(columns, arr)

140:            Return
150:        ElseIf TypeOf parentFolder Is folder Then
                Dim allFolders As ArrayList = parentFolder.getSubFolders

160:            If allFolders.Count > 0 Then
170:                Dim folderNode As AdvTree.Node = New AdvTree.Node("Folders")
180:                tvMain.Nodes.Add(folderNode)


                    '//add folders
190:                For Each fld As folder In allFolders
200:                    Using node As AdvTree.Node = New AdvTree.Node(fld.folderName)

210:                        node.DataKey = fld.folderID
220:                        node.Tag = "folder"

230:                        If gRole <> "Administrator" Then
240:                            If m_allowedFolders.Contains(fld.folderID) = False And m_isGroupFolderAccessRestricted Then
250:                                node.Image = resizeImage(My.Resources.Folder_256x256_forbidden, iconSize)
260:                            Else
270:                                node.Image = resizeImage(My.Resources.Folder_256x2561, iconSize)
                                End If
280:                        Else
290:                            node.Image = resizeImage(My.Resources.Folder_256x2561, iconSize)
                            End If

300:                        folderNode.Nodes.Add(node)
                        End Using
310:                Next

320:                folderNode.Expand()
                End If

330:            If parentFolder.folderID = 0 Then Return
            End If

340:        If m_allowedFolders IsNot Nothing AndAlso m_isGroupFolderAccessRestricted Then
350:            If m_allowedFolders.Contains(parentFolder.folderID) = False Then
360:                Return
370:            Else
380:                CType(parentFolder, folder).m_RoleHasFolderAccess = True
                End If
            End If

            Dim allPackages As ArrayList = parentFolder.getPackages
390:        addPackagesToDetails(columns, allPackages)

            Dim allReports As ArrayList = parentFolder.getReports
400:        addReportsToDetails(columns, allReports)

            Dim allAutos As ArrayList = parentFolder.getAutomations
410:        addAutomationsToDetails(columns, allAutos)

            Dim allEvents As ArrayList = parentFolder.getEventBased
420:        addEventsToDetails(columns, allEvents)

            Dim allEventPackages As ArrayList = parentFolder.getEventBasedPackages
430:        addEventPackagesToDetails(columns, allEventPackages)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try
    End Sub


    Sub addColumnsToNode(ByVal node As AdvTree.Node, ByVal columns() As String)
        Try
            '//add columns to this node
10:         For Each column As String In columns
20:             Dim cHeader As AdvTree.ColumnHeader = New AdvTree.ColumnHeader(column)
30:             cHeader.MinimumWidth = 64

40:             If tvExplorer.SelectedNode IsNot Nothing Then
50:                 cHeader.Width.Absolute = getColumnWidth(tvExplorer.SelectedNode.DataKey, node.Text, column)
                Else
60:                 cHeader.Width.Absolute = 150
                End If

70:             node.NodesColumns.Add(cHeader)
                'AddHandler cHeader.Click, AddressOf genericNodeClick_NodeClick
            Next
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try
    End Sub
    Sub addPackagesToDetails(ByVal columns() As String, ByVal allPackages As ArrayList)
        Try
10:         If allPackages.Count > 0 Then
                Dim normalPackageImage As Image = Nothing
                Dim dynamicPackageImage As Image = Nothing
                Dim datadrivenPackageImage As Image = Nothing

20:             For Each pack As Package In allPackages
30:                 Using node As AdvTree.Node = New AdvTree.Node(pack.packageName)
40:                     ' node.Image = resizeImage(pack.packageImage, iconSize)
50:                     node.DataKey = pack.ID
60:                     node.Tag = "package"

70:                     If pack.isDataDriven Then
                            If datadrivenPackageImage Is Nothing Then
                                datadrivenPackageImage = resizeImage(pack.packageImage, iconSize)
                            End If

                            node.Image = datadrivenPackageImage

                            Dim ddNodes() As AdvTree.Node = findNodes(Nothing, "Data-Driven Packages")

80:                         If ddNodes Is Nothing Then
90:                             Dim ddNode As AdvTree.Node = New AdvTree.Node("Data-Driven Packages")
100:                            addColumnsToNode(ddNode, columns)
110:                            tvMain.Nodes.Add(ddNode)
120:                            ddNode.Nodes.Add(node)
130:                            ddNode.Expand()
140:                        Else
150:                            ddNodes(0).Nodes.Add(node)
                            End If
160:                    ElseIf pack.isDynamic Then
                            If dynamicPackageImage Is Nothing Then
                                dynamicPackageImage = resizeImage(pack.packageImage, iconSize)
                            End If

                            node.Image = dynamicPackageImage

                            Dim dyNodes() As AdvTree.Node = findNodes(Nothing, "Dynamic Packages")

170:                        If dyNodes Is Nothing Then
180:                            Dim dyNode As AdvTree.Node = New AdvTree.Node("Dynamic Packages")
190:                            addColumnsToNode(dyNode, columns)
200:                            tvMain.Nodes.Add(dyNode)
210:                            dyNode.Nodes.Add(node)
220:                            dyNode.Expand()
230:                        Else
240:                            dyNodes(0).Nodes.Add(node)
                            End If
250:                    Else
                            If normalPackageImage Is Nothing Then
                                normalPackageImage = resizeImage(pack.packageImage, iconSize)
                            End If

                            node.Image = normalPackageImage

                            Dim sNodes() As AdvTree.Node = findNodes(Nothing, "Single Packages")

260:                        If sNodes Is Nothing Then
270:                            Dim sNode As AdvTree.Node = New AdvTree.Node("Single Packages")
280:                            addColumnsToNode(sNode, columns)
290:                            tvMain.Nodes.Add(sNode)
300:                            sNode.Nodes.Add(node)
310:                            sNode.Expand()
320:                        Else
330:                            sNodes(0).Nodes.Add(node)
                            End If
                        End If

340:                    For Each column As AdvTree.ColumnHeader In node.Parent.NodesColumns
350:                        Try
360:                            Select Case column.Text
                                    Case "Name"
                                        'do nothing
370:                                Case "Execution Time"
380:                                    node.Cells.Add(New AdvTree.Cell(pack.m_schedule.scheduleTime))
390:                                Case "Frequency"
400:                                    node.Cells.Add(New AdvTree.Cell(pack.m_schedule.frequency))
410:                                Case "Destinations", "Destination"
                                        Dim destString As String = ""

420:                                    If pack.destinations IsNot Nothing Then
430:                                        For Each dest As destination In pack.destinations
440:                                            destString &= dest.destinationType & ", "
450:                                        Next
460:                                    Else
470:                                        destString = ""
                                        End If
480:                                    If destString.Length > 0 Then destString = destString.Remove(destString.Length - 2, 2)

490:                                    node.Cells.Add(New AdvTree.Cell(destString))
500:                                Case "Last Result"
510:                                    node.Cells.Add(New AdvTree.Cell(pack.m_schedule.lastResult))
520:                                Case "Last Run"
530:                                    node.Cells.Add(New AdvTree.Cell(pack.m_schedule.lastRun))
540:                                Case "End Date"
550:                                    node.Cells.Add(New AdvTree.Cell(pack.m_schedule.endDate))
560:                                Case "Last Refreshed"
570:                                    node.Cells.Add(New AdvTree.Cell(""))
580:                                Case "Next Run"
590:                                    node.Cells.Add(New AdvTree.Cell(pack.m_schedule.nextRun))
600:                                Case "Output Formats"
610:                                    node.Cells.Add(New AdvTree.Cell(pack.destinations(0).destinationFormat))
620:                                Case "Repeat Interval"
630:                                    node.Cells.Add(New AdvTree.Cell(pack.m_schedule.repeatInterval))
640:                                Case "Repeat Until"
650:                                    node.Cells.Add(New AdvTree.Cell(pack.m_schedule.repeatUntil))
660:                                Case "Schedule Description"
670:                                    node.Cells.Add(New AdvTree.Cell(pack.m_schedule.description))
680:                                Case "Start Date"
690:                                    node.Cells.Add(New AdvTree.Cell(pack.m_schedule.startDate))
700:                                Case "Repeat"
710:                                    node.Cells.Add(New AdvTree.Cell(pack.m_schedule.repeat.ToString))
720:                                Case "Duration (secs)", "Duration"
730:                                    node.Cells.Add(New AdvTree.Cell(pack.m_schedule.lastRunDuration))
740:                                Case "Owner"
750:                                    node.Cells.Add(New AdvTree.Cell(pack.owner))
760:                                Case Else
770:                                    node.Cells.Add(New AdvTree.Cell("N/A"))
                                End Select
780:                        Catch ex As Exception
790:                            node.Cells.Add(New AdvTree.Cell("#Error Evaluating value#"))
                            End Try
800:                    Next

                    End Using
810:            Next
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try

    End Sub


    Sub addAutomationsToDetails(ByVal columns() As String, ByVal allAutos As ArrayList)
        Try
10:         If allAutos.Count > 0 Then
                Dim autoImage As Image = Nothing

20:             For Each auto As Automation In allAutos
30:                 Using node As AdvTree.Node = New AdvTree.Node(auto.AutoName)
                        If autoImage Is Nothing Then
                            autoImage = resizeImage(auto.automationImage, iconSize)
                        End If

40:                     node.Image = autoImage
50:                     node.DataKey = auto.ID
60:                     node.Tag = "automation"

                        Dim sNodes() As AdvTree.Node = findNodes(Nothing, "Automation Schedules")

70:                     If sNodes Is Nothing Then
80:                         Dim sNode As AdvTree.Node = New AdvTree.Node("Automation Schedules")
90:                         addColumnsToNode(sNode, columns)
100:                        tvMain.Nodes.Add(sNode)
110:                        sNode.Nodes.Add(node)
120:                        sNode.Expand()
130:                    Else
140:                        sNodes(0).Nodes.Add(node)
                        End If

150:                    For Each column As AdvTree.ColumnHeader In node.Parent.NodesColumns
160:                        Try
170:                            Select Case column.Text
                                    Case "Name"
                                        'do nothing
180:                                Case "Execution Time"
190:                                    node.Cells.Add(New AdvTree.Cell(auto.m_schedule.scheduleTime))
200:                                Case "Frequency"
210:                                    node.Cells.Add(New AdvTree.Cell(auto.m_schedule.frequency))
220:                                Case "Last Result"
230:                                    node.Cells.Add(New AdvTree.Cell(auto.m_schedule.lastResult))
240:                                Case "Last Run"
250:                                    node.Cells.Add(New AdvTree.Cell(auto.m_schedule.lastRun))
260:                                Case "End Date"
270:                                    node.Cells.Add(New AdvTree.Cell(auto.m_schedule.endDate))
280:                                Case "Last Refreshed"
290:                                    node.Cells.Add(New AdvTree.Cell(""))
300:                                Case "Next Run"
310:                                    node.Cells.Add(New AdvTree.Cell(auto.m_schedule.nextRun))
320:                                Case "Repeat Interval"
330:                                    node.Cells.Add(New AdvTree.Cell(auto.m_schedule.repeatInterval))
340:                                Case "Repeat Until"
350:                                    node.Cells.Add(New AdvTree.Cell(auto.m_schedule.repeatUntil))
360:                                Case "Schedule Description"
370:                                    node.Cells.Add(New AdvTree.Cell(auto.m_schedule.description))
380:                                Case "Start Date"
390:                                    node.Cells.Add(New AdvTree.Cell(auto.m_schedule.startDate))
400:                                Case "Repeat"
410:                                    node.Cells.Add(New AdvTree.Cell(auto.m_schedule.repeat.ToString))
420:                                Case "Duration (secs)", "Duration"
430:                                    node.Cells.Add(New AdvTree.Cell(auto.m_schedule.lastRunDuration))
440:                                Case "Owner"
450:                                    node.Cells.Add(New AdvTree.Cell(auto.Owner))
460:                                Case Else
470:                                    node.Cells.Add(New AdvTree.Cell("N/A"))
                                End Select
480:                        Catch ex As Exception
490:                            node.Cells.Add(New AdvTree.Cell("#Error Evaluating value#"))
                            End Try
500:                    Next

                        If auto.Status = False Then
                            node.Image = MakeGrayscale3(node.Image)
                        End If
                    End Using
510:            Next
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try
    End Sub


    Sub addEventsToDetails(ByVal columns() As String, ByVal allEvents As ArrayList)
        Try
10:         If allEvents.Count > 0 Then
                Dim eventImage As Image = Nothing

20:             For Each evt As EventBased In allEvents
                    If eventImage Is Nothing Then
                        eventImage = resizeImage(evt.eventImage, iconSize)
                    End If

30:                 Using node As AdvTree.Node = New AdvTree.Node(evt.eventName)
40:                     node.Image = eventImage
50:                     node.DataKey = evt.ID
60:                     node.Tag = "event"

                        Dim sNodes() As AdvTree.Node = findNodes(Nothing, "Event-Based Schedules")

70:                     If sNodes Is Nothing Then
80:                         Dim sNode As AdvTree.Node = New AdvTree.Node("Event-Based Schedules")
90:                         addColumnsToNode(sNode, columns)
100:                        tvMain.Nodes.Add(sNode)
110:                        sNode.Nodes.Add(node)
120:                        sNode.Expand()
130:                    Else
140:                        sNodes(0).Nodes.Add(node)
                        End If

150:                    For Each column As AdvTree.ColumnHeader In node.Parent.NodesColumns
160:                        Try

170:                            Select Case column.Text
                                    Case "Name"
                                        'do nothing
180:                                Case "Execution Time"
190:                                    node.Cells.Add(New AdvTree.Cell("Event-Based"))
200:                                Case "Frequency"
210:                                    node.Cells.Add(New AdvTree.Cell("Event-Based"))
220:                                Case "Last Result"
230:                                    node.Cells.Add(New AdvTree.Cell(evt.LastResult))
240:                                Case "Last Run"
250:                                    node.Cells.Add(New AdvTree.Cell(evt.LastRun))

260:                                Case "Last Refreshed"
270:                                    node.Cells.Add(New AdvTree.Cell(""))
280:                                Case "Next Run"
290:                                    node.Cells.Add(New AdvTree.Cell("Event-Based"))
300:                                Case "Schedule Description"
310:                                    node.Cells.Add(New AdvTree.Cell(""))
320:                                Case "Duration (Secs)", "Duration"
330:                                    node.Cells.Add(New AdvTree.Cell(evt.lastRunDuration))
340:                                Case "Owner"
350:                                    node.Cells.Add(New AdvTree.Cell(evt.Owner))
360:                                Case Else
370:                                    node.Cells.Add(New AdvTree.Cell("N/A"))
                                End Select
380:                        Catch ex As Exception
390:                            node.Cells.Add(New AdvTree.Cell("#Error Evaluating value#"))
                            End Try
400:                    Next

                        If evt.Status = False Then
                            node.Image = MakeGrayscale3(node.Image)
                        End If
                    End Using
410:            Next
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try
    End Sub

    Sub addEventPackagesToDetails(ByVal columns() As String, ByVal allEventPackages As ArrayList)
        Try
10:         If allEventPackages.Count > 0 Then
                Dim epImage As Image = Nothing


20:             For Each evp As EventBasedPackage In allEventPackages
                    If epImage Is Nothing Then
                        epImage = resizeImage(evp.packageImage, iconSize)
                    End If

30:                 Using node As AdvTree.Node = New AdvTree.Node(evp.eventPackageName)
40:                     node.Image = epImage
50:                     node.DataKey = evp.ID
60:                     node.Tag = "event-package"

                        Dim sNodes() As AdvTree.Node = findNodes(Nothing, "Event-Based Packages")

70:                     If sNodes Is Nothing Then
80:                         Dim sNode As AdvTree.Node = New AdvTree.Node("Event-Based Packages")
90:                         addColumnsToNode(sNode, columns)
100:                        tvMain.Nodes.Add(sNode)
110:                        sNode.Nodes.Add(node)
120:                        sNode.Expand()
130:                    Else
140:                        sNodes(0).Nodes.Add(node)
                        End If

150:                    For Each column As AdvTree.ColumnHeader In node.Parent.NodesColumns
160:                        Try
170:                            Select Case column.Text
                                    Case "Name"
                                        'do nothing
180:                                Case "Execution Time"
190:                                    node.Cells.Add(New AdvTree.Cell(evp.m_schedule.scheduleTime))
200:                                Case "Frequency"
210:                                    node.Cells.Add(New AdvTree.Cell(evp.m_schedule.frequency))
220:                                Case "Last Result"
230:                                    node.Cells.Add(New AdvTree.Cell(evp.m_schedule.lastResult))
240:                                Case "Last Run"
250:                                    node.Cells.Add(New AdvTree.Cell(evp.m_schedule.lastRun))
260:                                Case "End Date"
270:                                    node.Cells.Add(New AdvTree.Cell(evp.m_schedule.endDate))
280:                                Case "Last Refreshed"
290:                                    node.Cells.Add(New AdvTree.Cell(""))
300:                                Case "Next Run"
310:                                    node.Cells.Add(New AdvTree.Cell(evp.m_schedule.nextRun))
320:                                Case "Repeat Interval"
330:                                    node.Cells.Add(New AdvTree.Cell(evp.m_schedule.repeatInterval))
340:                                Case "Repeat Until"
350:                                    node.Cells.Add(New AdvTree.Cell(evp.m_schedule.repeatUntil))
360:                                Case "Schedule Description"
370:                                    node.Cells.Add(New AdvTree.Cell(evp.m_schedule.description))
380:                                Case "Start Date"
390:                                    node.Cells.Add(New AdvTree.Cell(evp.m_schedule.startDate))
400:                                Case "Repeat"
410:                                    node.Cells.Add(New AdvTree.Cell(evp.m_schedule.repeat.ToString))
420:                                Case "Duration (secs)", "Duration"
430:                                    node.Cells.Add(New AdvTree.Cell(evp.m_schedule.lastRunDuration))
440:                                Case "Owner"
450:                                    node.Cells.Add(New AdvTree.Cell(evp.Owner))
460:                                Case Else
470:                                    node.Cells.Add(New AdvTree.Cell("N/A"))
                                End Select
480:                        Catch ex As Exception
490:                            node.Cells.Add(New AdvTree.Cell("#Error Evaluating value#"))
                            End Try
500:                    Next

                        If evp.Status = False Then
                            node.Image = MakeGrayscale3(node.Image)
                        End If

                    End Using
510:            Next
            End If

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try
    End Sub

    Sub editEventPackageRow(evp As EventBasedPackage, node As AdvTree.Node)
        Try
            Dim I As Integer = 0

            For Each column As AdvTree.ColumnHeader In node.Parent.NodesColumns
                Try
                    Select Case column.Text
                        Case "Name"
                            'do nothing
                        Case "Execution Time"
                            node.Cells(I).Text = evp.m_schedule.scheduleTime
                        Case "Frequency"
                            node.Cells(I).Text = evp.m_schedule.frequency
                        Case "Last Result"
                            node.Cells(I).Text = evp.m_schedule.lastResult
                        Case "Last Run"
                            node.Cells(I).Text = evp.m_schedule.lastRun
                        Case "End Date"
                            node.Cells(I).Text = evp.m_schedule.endDate
                        Case "Last Refreshed"
                            node.Cells(I).Text = ""
                        Case "Next Run"
                            node.Cells(I).Text = evp.m_schedule.nextRun
                        Case "Repeat Interval"
                            node.Cells(I).Text = evp.m_schedule.repeatInterval
                        Case "Repeat Until"
                            node.Cells(I).Text = evp.m_schedule.repeatUntil
                        Case "Schedule Description"
                            node.Cells(I).Text = evp.m_schedule.description
                        Case "Start Date"
                            node.Cells(I).Text = evp.m_schedule.startDate
                        Case "Repeat"
                            node.Cells(I).Text = evp.m_schedule.repeat.ToString
                        Case "Owner"
                            node.Cells(I).Text = evp.Owner
                        Case Else
                            node.Cells(I).Text = "N/A"
                    End Select
                Catch ex As Exception
                    node.Cells(I).Text = "#Error Evaluating value#"
                End Try

                I += 1
            Next
        Catch : End Try
    End Sub

    Sub editEventRow(evt As EventBased, node As AdvTree.Node)
        Try
            Dim I As Integer = 0

            For Each column As AdvTree.ColumnHeader In node.Parent.NodesColumns
                Try

                    Select Case column.Text
                        Case "Name"
                            'do nothing
                        Case "Execution Time"
                            node.Cells(I).Text = "Event-Based"
                        Case "Frequency"
                            node.Cells(I).Text = "Event-Based"
                        Case "Last Result"
                            node.Cells(I).Text = evt.LastResult
                        Case "Last Run"
                            node.Cells(I).Text = evt.LastRun

                        Case "Last Refreshed"
                            node.Cells(I).Text = ""
                        Case "Next Run"
                            node.Cells(I).Text = "Event-Based"
                        Case "Schedule Description"
                            node.Cells(I).Text = ""
                        Case "Owner"
                            node.Cells(I).Text = evt.Owner
                        Case Else
                            node.Cells(I).Text = "N/A"
                    End Select
                Catch ex As Exception
                    node.Cells(I).Text = "#Error Evaluating value#"
                End Try

                I += 1
            Next
        Catch :End Try
    End Sub
    Sub editAutomationRow(auto As Automation, node As AdvTree.Node)
        Try
            Dim I As Integer

            For Each column As AdvTree.ColumnHeader In node.Parent.NodesColumns
                Try
                    Select Case column.Text
                        Case "Name"
                            'do nothing
                        Case "Execution Time"
                            node.Cells(I).Text = auto.m_schedule.scheduleTime
                        Case "Frequency"
                            node.Cells(I).Text = auto.m_schedule.frequency
                        Case "Last Result"
                            node.Cells(I).Text = auto.m_schedule.lastResult
                        Case "Last Run"
                            node.Cells(I).Text = auto.m_schedule.lastRun
                        Case "End Date"
                            node.Cells(I).Text = auto.m_schedule.endDate
                        Case "Last Refreshed"
                            node.Cells(I).Text = ""
                        Case "Next Run"
                            node.Cells(I).Text = auto.m_schedule.nextRun
                        Case "Repeat Interval"
                            node.Cells(I).Text = auto.m_schedule.repeatInterval
                        Case "Repeat Until"
                            node.Cells(I).Text = auto.m_schedule.repeatUntil
                        Case "Schedule Description"
                            node.Cells(I).Text = auto.m_schedule.description
                        Case "Start Date"
                            node.Cells(I).Text = auto.m_schedule.startDate
                        Case "Repeat"
                            node.Cells(I).Text = auto.m_schedule.repeat.ToString
                        Case "Owner"
                            node.Cells(I).Text = auto.Owner
                        Case Else
                            node.Cells(I).Text = "N/A"
                    End Select
                Catch ex As Exception
                    node.Cells(I).Text = "#Error Evaluating value#"
                End Try
                I += 1
            Next
        Catch : End Try
    End Sub
    Sub editPackageRow(pack As Package, node As AdvTree.Node)
        Try
            Dim I As Integer = 0

            For Each column As AdvTree.ColumnHeader In node.Parent.NodesColumns
                Try
                    Select Case column.Text
                        Case "Name"
                            'do nothing
                        Case "Execution Time"
                            node.Cells(I).Text = pack.m_schedule.scheduleTime
                        Case "Frequency"
                            node.Cells(I).Text = pack.m_schedule.frequency
                        Case "Destinations", "Destination"
                            Dim destString As String = ""

                            If pack.destinations IsNot Nothing Then
                                For Each dest As destination In pack.destinations
                                    destString &= dest.destinationType & ", "
                                Next
                            Else
                                destString = ""
                            End If
                            If destString.Length > 0 Then destString = destString.Remove(destString.Length - 2, 2)

                            node.Cells(I).Text = destString
                        Case "Last Result"
                            node.Cells(I).Text = pack.m_schedule.lastResult
                        Case "Last Run"
                            node.Cells(I).Text = pack.m_schedule.lastRun
                        Case "End Date"
                            node.Cells(I).Text = pack.m_schedule.endDate
                        Case "Last Refreshed"
                            node.Cells(I).Text = ""
                        Case "Next Run"
                            node.Cells(I).Text = pack.m_schedule.nextRun
                        Case "Output Formats"
                            node.Cells(I).Text = pack.destinations(0).destinationFormat
                        Case "Repeat Interval"
                            node.Cells(I).Text = pack.m_schedule.repeatInterval
                        Case "Repeat Until"
                            node.Cells(I).Text = pack.m_schedule.repeatUntil
                        Case "Schedule Description"
                            node.Cells(I).Text = pack.m_schedule.description
                        Case "Start Date"
                            node.Cells(I).Text = pack.m_schedule.startDate
                        Case "Repeat"
                            node.Cells(I).Text = pack.m_schedule.repeat.ToString
                        Case "Owner"
                            node.Cells(I).Text = pack.owner
                        Case Else
                            node.Cells(I).Text = "N/A"
                    End Select
                Catch ex As Exception
                    node.Cells(I).Text = "#Error Evaluating value#"
                End Try

                I += 1
            Next
        Catch : End Try
    End Sub

    Sub editReportRow(rpt As cssreport, node As AdvTree.Node)
        Try
            Dim I As Integer = 0

            For Each column As AdvTree.ColumnHeader In node.Parent.NodesColumns
                Try
                    Select Case column.Text
                        Case "Name"
                            'do noothing
                        Case "Execution Time"
                            If rpt.isPackagedReport Then
                                node.Cells(I).Text = "See parent package"
                            Else
                                node.Cells(I).Text = rpt.m_schedule.scheduleTime
                            End If
                        Case "Frequency"
                            If rpt.isPackagedReport Then
                                node.Cells(I).Text = "See parent package"
                            Else
                                node.Cells(I).Text = rpt.m_schedule.frequency
                            End If
                        Case "Destination", "Destinations"
                            If rpt.isPackagedReport Then
                                node.Cells(I).Text = "See parent package"
                            Else
                                Dim destString As String = ""

                                If rpt.destinations IsNot Nothing Then
                                    For Each dest As destination In rpt.destinations
                                        destString &= dest.destinationType & ", "
                                    Next

                                    If destString.Length > 0 Then destString = destString.Remove(destString.Length - 2, 2)

                                    node.Cells(I).Text = destString
                                Else
                                    node.Cells(I).Text = destString
                                End If
                            End If
                        Case "Last Result"
                            If rpt.isPackagedReport Then
                                node.Cells(I).Text = "See parent package"
                            Else
                                node.Cells(I).Text = rpt.m_schedule.lastResult
                            End If
                        Case "Last Run"
                            If rpt.isPackagedReport Then
                                node.Cells(I).Text = "See parent package"
                            Else
                                node.Cells(I).Text = rpt.m_schedule.lastRun
                            End If
                        Case "End Date"
                            If rpt.isPackagedReport Then
                                node.Cells(I).Text = "See parent package"
                            Else
                                node.Cells(I).Text = rpt.m_schedule.endDate
                            End If
                        Case "Last Refreshed"
                            node.Cells(I).Text = rpt.lastRefreshed
                        Case "Next Run"
                            If rpt.isPackagedReport Then
                                node.Cells(I).Text = "See parent package"
                            Else
                                node.Cells(I).Text = rpt.m_schedule.nextRun
                            End If
                        Case "Output Formats"
                            If rpt.isPackagedReport Then
                                node.Cells(I).Text = rpt.packedReportFormat
                            Else
                                Dim destFormats As String = ""

                                If rpt.destinations IsNot Nothing Then
                                    For Each dest As destination In rpt.destinations
                                        destFormats &= dest.destinationFormat & ", "
                                    Next

                                    If destFormats.Length > 0 Then destFormats = destFormats.Remove(destFormats.Length - 2, 2)

                                    node.Cells(I).Text = destFormats
                                Else
                                    node.Cells(I).Text = destFormats
                                End If
                            End If
                        Case "Repeat Interval"
                            If rpt.isPackagedReport Then
                                node.Cells(I).Text = "See parent package"
                            Else
                                node.Cells(I).Text = rpt.m_schedule.repeatInterval
                            End If
                        Case "Repeat Until"
                            If rpt.isPackagedReport Then
                                node.Cells(I).Text = "See parent package"
                            Else
                                node.Cells(I).Text = rpt.m_schedule.repeatUntil
                            End If
                        Case "Report Path"
                            node.Cells(I).Text = rpt.originalReportFile
                        Case "Schedule Description"
                            If rpt.isPackagedReport Then
                                node.Cells.Add(New AdvTree.Cell("See parent package"))
                            Else
                                node.Cells.Add(New AdvTree.Cell(rpt.m_schedule.description))
                            End If
                        Case "Start Date"
                            If rpt.isPackagedReport Then
                                node.Cells(I).Text = "See parent package"
                            Else
                                node.Cells(I).Text = rpt.m_schedule.startDate
                            End If
                        Case "Repeat"
                            If rpt.isPackagedReport Then
                                node.Cells(I).Text = "See parent package"
                            Else
                                node.Cells(I).Text = rpt.m_schedule.repeat.ToString
                            End If
                        Case "Owner"
                            If rpt.isPackagedReport Then
                                node.Cells(I).Text = "See parent package"
                            Else
                                node.Cells(I).Text = rpt.owner
                            End If
                        Case Else
                            If rpt.isPackagedReport Then
                                node.Cells(I).Text = "See parent package"
                            Else
                                node.Cells(I).Text = "N/A"
                            End If
                    End Select
                Catch ex As Exception
                    node.Cells(I).Text = "#Error Evaluating value#"
                End Try

                I += 1
            Next
        Catch : End Try
    End Sub
    Sub editItemRow(id As Integer, type As clsMarsScheduler.enScheduleType)
        Dim item As AdvTree.Node = tvMain.FindNodeByDataKey(id)
        Dim obj As Object

        Select Case type
            Case clsMarsScheduler.enScheduleType.REPORT
                obj = New cssreport(id)

                item.Image = resizeImage(obj.reportImage, iconSize)
                item.Text = obj.reportName

                editReportRow(obj, item)
            Case clsMarsScheduler.enScheduleType.AUTOMATION

                obj = New Automation(id)
                item.Image = resizeImage(obj.automationImage, iconSize)
                item.Text = obj.AutoName

                editAutomationRow(obj, item)
            Case clsMarsScheduler.enScheduleType.PACKAGE
                obj = New Package(id)
                item.Image = resizeImage(obj.packageImage, iconSize)
                item.Text = obj.packageName

                editPackageRow(obj, item)
            Case clsMarsScheduler.enScheduleType.EVENTBASED
                obj = New EventBased(id)
                item.Image = resizeImage(obj.eventImage, iconSize)
                item.Text = obj.eventName

                editEventRow(obj, item)
            Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                obj = New EventBasedPackage(id)
                item.Image = resizeImage(obj.packageImage, iconSize)
                item.Text = obj.eventPackageName

                editEventPackageRow(obj, item)
        End Select

    End Sub
    Sub addReportsToDetails(ByVal columns() As String, ByVal allReports As ArrayList)
        Try
10:         If allReports.Count > 0 Then
                Dim singleImage, dynamicImage, dataDrivenImage, burstingImage As Image

20:             For Each rpt As cssreport In allReports
30:                 Using node As AdvTree.Node = New AdvTree.Node(rpt.reportName)
40:
50:                     node.DataKey = rpt.ID
60:                     node.Tag = "report"

70:                     If rpt.isdatadriven Then
                            If dataDrivenImage Is Nothing Then
                                dataDrivenImage = resizeImage(rpt.reportImage, iconSize)
                            End If

                            node.Image = dataDrivenImage

                            Dim ddNodes() As AdvTree.Node = findNodes(Nothing, "Data-Driven Schedules")

80:                         If ddNodes Is Nothing Then
90:                             Dim ddNode As AdvTree.Node = New AdvTree.Node("Data-Driven Schedules")
100:                            addColumnsToNode(ddNode, columns)
110:                            tvMain.Nodes.Add(ddNode)
120:                            ddNode.Nodes.Add(node)
130:                            ddNode.Expand()
140:                        Else
150:                            ddNodes(0).Nodes.Add(node)
                            End If
160:                    ElseIf rpt.isdynamic Then
                            If dynamicImage Is Nothing Then
                                dynamicImage = resizeImage(rpt.reportImage, iconSize)
                            End If

                            node.Image = dynamicImage

                            Dim dyNodes() As AdvTree.Node = findNodes(Nothing, "Dynamic Schedules")

170:                        If dyNodes Is Nothing Then
180:                            Dim dyNode As AdvTree.Node = New AdvTree.Node("Dynamic Schedules")
190:                            addColumnsToNode(dyNode, columns)
200:                            tvMain.Nodes.Add(dyNode)
210:                            dyNode.Nodes.Add(node)
220:                            dyNode.Expand()
230:                        Else
240:                            dyNodes(0).Nodes.Add(node)
                            End If
250:                    ElseIf rpt.isbursting Then
                            If burstingImage Is Nothing Then
                                burstingImage = resizeImage(rpt.reportImage, iconSize)
                            End If

                            node.Image = burstingImage
                            Dim dyNodes() As AdvTree.Node = findNodes(Nothing, "Bursting Schedules")

260:                        If dyNodes Is Nothing Then
270:                            Dim dyNode As AdvTree.Node = New AdvTree.Node("Bursting Schedules")
280:                            addColumnsToNode(dyNode, columns)
290:                            tvMain.Nodes.Add(dyNode)
300:                            dyNode.Nodes.Add(node)
310:                            dyNode.Expand()
320:                        Else
330:                            dyNodes(0).Nodes.Add(node)
                            End If
340:                    ElseIf rpt.isPackagedReport Then
                            If singleImage Is Nothing Then
                                singleImage = resizeImage(rpt.reportImage, iconSize)
                            End If

                            node.Image = singleImage
                            Dim sNodes() As AdvTree.Node = findNodes(Nothing, "Package Reports")

350:                        If sNodes Is Nothing Then
360:                            Dim sNode As AdvTree.Node = New AdvTree.Node("Package Reports")
370:                            addColumnsToNode(sNode, columns)
380:                            tvMain.Nodes.Add(sNode)
390:                            sNode.Nodes.Add(node)
400:                            sNode.Expand()
410:                        Else
420:                            sNodes(0).Nodes.Add(node)
                            End If
430:                    Else
                            If singleImage Is Nothing Then
                                singleImage = resizeImage(rpt.reportImage, iconSize)
                            End If

                            node.Image = singleImage

                            Dim sNodes() As AdvTree.Node = findNodes(Nothing, "Single Schedules")

440:                        If sNodes Is Nothing Then
450:                            Dim sNode As AdvTree.Node = New AdvTree.Node("Single Schedules")
460:                            addColumnsToNode(sNode, columns)
470:                            tvMain.Nodes.Add(sNode)
480:                            sNode.Nodes.Add(node)
490:                            sNode.Expand()
500:                        Else
510:                            sNodes(0).Nodes.Add(node)
                            End If
                        End If


520:                    For Each column As AdvTree.ColumnHeader In node.Parent.NodesColumns
530:                        Try
540:                            Select Case column.Text
                                    Case "Name"
                                        'do noothing
550:                                Case "Execution Time"
560:                                    If rpt.isPackagedReport Then
570:                                        node.Cells.Add(New AdvTree.Cell("See parent package"))
580:                                    Else
590:                                        node.Cells.Add(New AdvTree.Cell(rpt.m_schedule.scheduleTime))
                                        End If

600:                                Case "Frequency"
610:                                    If rpt.isPackagedReport Then
620:                                        node.Cells.Add(New AdvTree.Cell("See parent package"))
630:                                    Else
640:                                        node.Cells.Add(New AdvTree.Cell(rpt.m_schedule.frequency))
                                        End If
650:                                Case "Destination", "Destinations"
660:                                    If rpt.isPackagedReport Then
670:                                        node.Cells.Add(New AdvTree.Cell("See parent package"))
680:                                    Else
                                            Dim destString As String = ""

690:                                        If rpt.destinations IsNot Nothing Then

700:                                            For Each dest As destination In rpt.destinations
710:                                                destString &= dest.destinationType & ", "
720:                                            Next

730:                                            If destString.Length > 0 Then destString = destString.Remove(destString.Length - 2, 2)

740:                                            node.Cells.Add(New AdvTree.Cell(destString))
750:                                        Else
760:                                            node.Cells.Add(New AdvTree.Cell(destString))
                                            End If
                                        End If
770:                                Case "Last Result"
780:                                    If rpt.isPackagedReport Then
790:                                        node.Cells.Add(New AdvTree.Cell("See parent package"))
800:                                    Else
810:                                        node.Cells.Add(New AdvTree.Cell(rpt.m_schedule.lastResult))
                                        End If
820:                                Case "Last Run"
830:                                    If rpt.isPackagedReport Then
840:                                        node.Cells.Add(New AdvTree.Cell("See parent package"))
850:                                    Else
860:                                        node.Cells.Add(New AdvTree.Cell(rpt.m_schedule.lastRun))
                                        End If
870:                                Case "Duration (secs)", "Duration"
880:                                    If rpt.isPackagedReport Then
890:                                        node.Cells.Add(New AdvTree.Cell("See parent package"))
900:                                    Else
910:                                        node.Cells.Add(New AdvTree.Cell(rpt.m_schedule.lastRunDuration))
                                        End If
920:                                Case "End Date"
930:                                    If rpt.isPackagedReport Then
940:                                        node.Cells.Add(New AdvTree.Cell("See parent package"))
950:                                    Else
960:                                        node.Cells.Add(New AdvTree.Cell(rpt.m_schedule.endDate))
                                        End If
970:                                Case "Last Refreshed"
980:                                    node.Cells.Add(New AdvTree.Cell(rpt.lastRefreshed))
990:                                Case "Next Run"
1000:                                   If rpt.isPackagedReport Then
1010:                                       node.Cells.Add(New AdvTree.Cell("See parent package"))
1020:                                   Else
1030:                                       node.Cells.Add(New AdvTree.Cell(rpt.m_schedule.nextRun))
                                        End If
1040:                               Case "Output Formats"
1050:                                   If rpt.isPackagedReport Then
1060:                                       node.Cells.Add(New AdvTree.Cell(rpt.packedReportFormat))
1070:                                   Else
                                            Dim destFormats As String = ""

1080:                                       If rpt.destinations IsNot Nothing Then
1090:                                           For Each dest As destination In rpt.destinations
1100:                                               destFormats &= dest.destinationFormat & ", "
1110:                                           Next

1120:                                           If destFormats.Length > 0 Then destFormats = destFormats.Remove(destFormats.Length - 2, 2)

1130:                                           node.Cells.Add(New AdvTree.Cell(destFormats))
1140:                                       Else
1150:                                           node.Cells.Add(New AdvTree.Cell(destFormats))
                                            End If
                                        End If
1160:                               Case "Repeat Interval"
1170:                                   If rpt.isPackagedReport Then
1180:                                       node.Cells.Add(New AdvTree.Cell("See parent package"))
1190:                                   Else
1200:                                       node.Cells.Add(New AdvTree.Cell(rpt.m_schedule.repeatInterval))
                                        End If
1210:                               Case "Repeat Until"
1220:                                   If rpt.isPackagedReport Then
1230:                                       node.Cells.Add(New AdvTree.Cell("See parent package"))
1240:                                   Else
1250:                                       node.Cells.Add(New AdvTree.Cell(rpt.m_schedule.repeatUntil))
                                        End If
1260:                               Case "Report Path"
1270:                                   node.Cells.Add(New AdvTree.Cell(rpt.originalReportFile))
1280:                               Case "Schedule Description"
1290:                                   If rpt.isPackagedReport Then
1300:                                       node.Cells.Add(New AdvTree.Cell("See parent package"))
1310:                                   Else
1320:                                       node.Cells.Add(New AdvTree.Cell(rpt.m_schedule.description))
                                        End If
1330:                               Case "Start Date"
1340:                                   If rpt.isPackagedReport Then
1350:                                       node.Cells.Add(New AdvTree.Cell("See parent package"))
1360:                                   Else
1370:                                       node.Cells.Add(New AdvTree.Cell(rpt.m_schedule.startDate))
                                        End If
1380:                               Case "Repeat"
1390:                                   If rpt.isPackagedReport Then
1400:                                       node.Cells.Add(New AdvTree.Cell("See parent package"))
1410:                                   Else
1420:                                       node.Cells.Add(New AdvTree.Cell(rpt.m_schedule.repeat.ToString))
                                        End If
1430:                               Case "Owner"
1440:                                   If rpt.isPackagedReport Then
1450:                                       node.Cells.Add(New AdvTree.Cell("See parent package"))
1460:                                   Else
1470:                                       node.Cells.Add(New AdvTree.Cell(rpt.owner))
                                        End If
1480:                               Case Else
1490:                                   If rpt.isPackagedReport Then
1500:                                       node.Cells.Add(New AdvTree.Cell("See parent package"))
1510:                                   Else
1520:                                       node.Cells.Add(New AdvTree.Cell("N/A"))
                                        End If
                                End Select
1530:                       Catch ex As Exception
1540:                           node.Cells.Add(New AdvTree.Cell("#Error Evaluating value#"))
                            End Try

                            If rpt.status = False Then
                                node.Image = MakeGrayscale3(node.Image)
                            End If
1550:                   Next


                    End Using
1560:           Next
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try
    End Sub


    Sub addPackageTiles(ByVal allPackages As ArrayList)

        If allPackages.Count > 0 Then
            Dim normalPackageImage As Image = Nothing
            Dim dynamicPackageImage As Image = Nothing
            Dim datadrivenPackageImage As Image = Nothing

            For Each pack As Package In allPackages
                Using node As AdvTree.Node = New AdvTree.Node(pack.packageName)

                    'node.Image = resizeImage(pack.packageImage, iconSize)
                    node.DataKey = pack.ID
                    node.Tag = "package"

                    If pack.isDataDriven Then
                        If datadrivenPackageImage Is Nothing Then
                            datadrivenPackageImage = resizeImage(pack.packageImage, iconSize)
                        End If

                        node.Image = datadrivenPackageImage


                        Dim ddNodes() As AdvTree.Node = findNodes(Nothing, "Data-Driven Packages")

                        If ddNodes Is Nothing Then
                            Dim ddNode As AdvTree.Node = New AdvTree.Node("Data-Driven Packages")
                            tvMain.Nodes.Add(ddNode)
                            ddNode.Nodes.Add(node)
                            ddNode.Expand()
                        Else
                            ddNodes(0).Nodes.Add(node)
                        End If
                    ElseIf pack.isDynamic Then
                        If dynamicPackageImage Is Nothing Then
                            dynamicPackageImage = resizeImage(pack.packageImage, iconSize)
                        End If

                        node.Image = dynamicPackageImage

                        Dim dyNodes() As AdvTree.Node = findNodes(Nothing, "Dynamic Packages")

                        If dyNodes Is Nothing Then
                            Dim dyNode As AdvTree.Node = New AdvTree.Node("Dynamic Packages")
                            tvMain.Nodes.Add(dyNode)
                            dyNode.Nodes.Add(node)
                            dyNode.Expand()
                        Else
                            dyNodes(0).Nodes.Add(node)
                        End If
                    Else
                        If normalPackageImage Is Nothing Then
                            normalPackageImage = resizeImage(pack.packageImage, iconSize)
                        End If

                        node.Image = normalPackageImage

                        Dim sNodes() As AdvTree.Node = findNodes(Nothing, "Single Packages")

                        If sNodes Is Nothing Then
                            Dim sNode As AdvTree.Node = New AdvTree.Node("Single Packages")
                            tvMain.Nodes.Add(sNode)
                            sNode.Nodes.Add(node)
                            sNode.Expand()
                        Else
                            sNodes(0).Nodes.Add(node)
                        End If
                    End If


                    If pack.m_status = False Then
                        node.Image = MakeGrayscale3(node.Image)
                    End If
                    'For Each rpt As crdreport In pack.getReportsinPackage
                    '    Dim nx As AdvTree.Node = New AdvTree.Node(rpt.reportName)
                    '    nx.Image = resizeImage(rpt.reportImage, 24, 24)
                    '    nx.Tag = rpt.ID
                    '    node.Nodes.Add(nx)
                    'Next

                End Using
            Next
        End If
    End Sub

    Sub addReportTiles(ByVal allReports As ArrayList)
        If allReports.Count > 0 Then
            Dim singleImage, dynamicImage, dataDrivenImage, burstingImage As Image


            For Each rpt As cssreport In allReports
                Using node As AdvTree.Node = New AdvTree.Node(rpt.reportName)
                    'node.Image = resizeImage(rpt.reportImage, iconSize)
                    node.Tag = "report"
                    node.DataKey = rpt.ID

                    If rpt.isdatadriven Then
                        If dataDrivenImage Is Nothing Then
                            dataDrivenImage = resizeImage(rpt.reportImage, iconSize)
                        End If

                        node.Image = dataDrivenImage

                        Dim ddNodes() As AdvTree.Node = findNodes(Nothing, "Data-Driven Schedules")

                        If ddNodes Is Nothing Then
                            Dim ddNode As AdvTree.Node = New AdvTree.Node("Data-Driven Schedules")
                            tvMain.Nodes.Add(ddNode)
                            ddNode.Nodes.Add(node)
                            ddNode.Expand()
                        Else
                            ddNodes(0).Nodes.Add(node)
                        End If
                    ElseIf rpt.isdynamic Then
                        If dynamicImage Is Nothing Then
                            dynamicImage = resizeImage(rpt.reportImage, iconSize)
                        End If

                        node.Image = dynamicImage

                        Dim dyNodes() As AdvTree.Node = findNodes(Nothing, "Dynamic Schedules")

                        If dyNodes Is Nothing Then
                            Dim dyNode As AdvTree.Node = New AdvTree.Node("Dynamic Schedules")
                            tvMain.Nodes.Add(dyNode)
                            dyNode.Nodes.Add(node)
                            dyNode.Expand()
                        Else
                            dyNodes(0).Nodes.Add(node)
                        End If
                    ElseIf rpt.isbursting Then
                        If burstingImage Is Nothing Then
                            burstingImage = resizeImage(rpt.reportImage, iconSize)
                        End If

                        node.Image = burstingImage

                        Dim bNodes() As AdvTree.Node = findNodes(Nothing, "Bursting Schedules")

                        If bNodes Is Nothing Then
                            Dim bNode As AdvTree.Node = New AdvTree.Node("Bursting Schedules")
                            tvMain.Nodes.Add(bNode)
                            bNode.Nodes.Add(node)
                            bNode.Expand()
                        Else
                            bNodes(0).Nodes.Add(node)
                        End If
                    ElseIf rpt.isPackagedReport Then
                        If singleImage Is Nothing Then
                            singleImage = resizeImage(rpt.reportImage, iconSize)
                        End If

                        node.Image = singleImage

                        Dim sNodes() As AdvTree.Node = findNodes(Nothing, "Package Reports")

                        If sNodes Is Nothing Then
                            Dim sNode As AdvTree.Node = New AdvTree.Node("Package Reports")
                            tvMain.Nodes.Add(sNode)
                            sNode.Nodes.Add(node)
                            sNode.Expand()
                        Else
                            sNodes(0).Nodes.Add(node)
                        End If
                    Else
                        If singleImage Is Nothing Then
                            singleImage = resizeImage(rpt.reportImage, iconSize)
                        End If

                        node.Image = singleImage

                        Dim sNodes() As AdvTree.Node = findNodes(Nothing, "Single Schedules")

                        If sNodes Is Nothing Then
                            Dim sNode As AdvTree.Node = New AdvTree.Node("Single Schedules")
                            tvMain.Nodes.Add(sNode)
                            sNode.Nodes.Add(node)
                            sNode.Expand()
                        Else
                            sNodes(0).Nodes.Add(node)
                        End If
                    End If

                    If rpt.status = False Then
                        node.Image = MakeGrayscale3(node.Image)
                    End If
                End Using
            Next
        End If
    End Sub
    Sub addAutomationTiles(ByVal allAutos As ArrayList)
        '//add automation

        If allAutos.Count > 0 Then
            Dim automationNode As AdvTree.Node = New AdvTree.Node("Automation Schedules")
            tvMain.Nodes.Add(automationNode)

            Dim autoImage As Image = Nothing

            For Each auto As Automation In allAutos
                Using node As AdvTree.Node = New AdvTree.Node(auto.AutoName)
                    If autoImage Is Nothing Then
                        autoImage = resizeImage(auto.automationImage, iconSize)
                    End If

                    node.Image = autoImage

                    node.DataKey = auto.ID
                    node.Tag = "automation"

                    automationNode.Nodes.Add(node)

                    If auto.Status = False Then
                        node.Image = MakeGrayscale3(node.Image)
                    End If
                End Using
            Next

            automationNode.Expand()
        End If
    End Sub

    Sub addEventBasedTiles(ByVal allEvents As ArrayList)
        If allEvents.Count > 0 Then
            Dim eventNode As AdvTree.Node = New AdvTree.Node("Event-Based Schedules")
            tvMain.Nodes.Add(eventNode)

            Dim eventImage As Image = Nothing

            For Each eventschedule As EventBased In allEvents
                Using node As AdvTree.Node = New AdvTree.Node(eventschedule.eventName)
                    If eventImage Is Nothing Then
                        eventImage = resizeImage(eventschedule.eventImage, iconSize)
                    End If

                    node.Image = eventImage
                    node.DataKey = eventschedule.ID
                    node.Tag = "event"
                    eventNode.Nodes.Add(node)

                    If eventschedule.Status = False Then
                        node.Image = MakeGrayscale3(node.Image)
                    End If
                End Using
            Next

            eventNode.Expand()
        End If
    End Sub

    Sub addEventPackTiles(ByVal allEventPacks As ArrayList)

        If allEventPacks.Count > 0 Then
            Dim eventPackNode As AdvTree.Node = New AdvTree.Node("Event-Based Packages")
            tvMain.Nodes.Add(eventPackNode)

            Dim epImage As Image = Nothing

            For Each eventpackage As EventBasedPackage In allEventPacks

                Using node As AdvTree.Node = New AdvTree.Node(eventpackage.eventPackageName)
                    If epImage Is Nothing Then
                        epImage = resizeImage(eventpackage.packageImage, iconSize)
                    End If

                    node.Image = epImage
                    node.DataKey = eventpackage.ID
                    node.Tag = "event-package"
                    eventPackNode.Nodes.Add(node)

                    If eventpackage.Status = False Then
                        node.Image = MakeGrayscale3(node.Image)
                    End If
                End Using
            Next

            eventPackNode.Expand()
        End If

    End Sub
    Sub buildTiles(ByVal parentFolder As Object)
10:     Try
20:         If parentFolder Is Nothing Then Return
30:         tvMain.Nodes.Clear()

40:         If TypeOf parentFolder Is Package Then
                Dim reports As ArrayList = CType(parentFolder, Package).getReportsinPackage
50:             addReportTiles(reports)
60:             Return
70:         ElseIf TypeOf parentFolder Is EventBasedPackage Then
                Dim evts As EventBased() = CType(parentFolder, EventBasedPackage).events
80:             Dim arr As ArrayList = New ArrayList

90:             If evts Is Nothing Then Return

100:            arr.AddRange(evts)

110:            addEventBasedTiles(arr)

120:            Return
130:        ElseIf TypeOf parentFolder Is folder Then
                Dim allFolders As ArrayList = parentFolder.getSubFolders

140:            If allFolders.Count > 0 Then
150:                Dim folderNode As AdvTree.Node = New AdvTree.Node("Folders")
160:                tvMain.Nodes.Add(folderNode)

                    Dim normalFolderImage As Image = resizeImage(My.Resources.Folder_256x2561, iconSize)
                    Dim forbiddenFolderImage As Image = resizeImage(My.Resources.Folder_256x256_forbidden, iconSize)

                    '//add folders
170:                For Each fld As folder In allFolders
180:                    Using node As AdvTree.Node = New AdvTree.Node(fld.folderName)

190:                        node.DataKey = fld.folderID
200:                        node.Tag = "folder"

210:                        If gRole <> "Administrator" Then
220:                            If m_allowedFolders.Contains(fld.folderID) = False And m_isGroupFolderAccessRestricted Then
230:                                node.Image = forbiddenFolderImage
240:                            Else
250:                                node.Image = normalFolderImage
                                End If
260:                        Else
270:                            node.Image = normalFolderImage
                            End If

280:                        folderNode.Nodes.Add(node)
                        End Using
290:                Next

300:                folderNode.Expand()

                    ' normalFolderImage.Dispose()
                    ' forbiddenFolderImage.Dispose()

                    ' normalFolderImage = Nothing
                    ' forbiddenFolderImage = Nothing
                End If


310:            If parentFolder.folderID = 0 Then Return
            End If

320:        If m_allowedFolders IsNot Nothing AndAlso m_isGroupFolderAccessRestricted Then
330:            If m_allowedFolders.Contains(parentFolder.folderID) = False Then
340:                Return
350:            Else
360:                CType(parentFolder, folder).m_RoleHasFolderAccess = True
                End If
            End If


            '//add packages
            Dim allPackages As ArrayList = parentFolder.getPackages
370:        addPackageTiles(allPackages)

            Dim allReports As ArrayList = parentFolder.getReports
380:        addReportTiles(allReports)

            Dim allAutos As ArrayList = parentFolder.getAutomations
390:        addAutomationTiles(allAutos)

            Dim allEvents As ArrayList = parentFolder.getEventBased
400:        addEventBasedTiles(allEvents)

            Dim allEventPacks As ArrayList = parentFolder.getEventBasedPackages
410:        addEventPackTiles(allEventPacks)
420:    Catch ex As Exception
430:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try
    End Sub


    Public Function findNodes(ByVal parentNode As AdvTree.Node, ByVal searchString As String) As AdvTree.Node()
        Dim results As AdvTree.Node() = Nothing
        Dim I As Integer = 0

        If parentNode Is Nothing Then
            For Each n As AdvTree.Node In tvMain.Nodes
                If String.Compare(n.Text, searchString, True) = 0 Then
                    ReDim Preserve results(I)
                    results(I) = n
                    I += 1
                End If
            Next
        Else
            For Each n As AdvTree.Node In parentNode.Nodes
                If String.Compare(n.Text, searchString, True) = 0 Then
                    ReDim Preserve results(I)
                    results(I) = n
                    I += 1
                End If
            Next
        End If
        Return results
    End Function



    Private Sub swBlack_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim sw As DotNetBar.SwitchButtonItem = CType(sender, DotNetBar.SwitchButtonItem)

        If sw.Value = True Then
            Select Case sw.Tag

            End Select
        End If

    End Sub

#Region "Themes"
    Private Sub btnMetro_Click(sender As Object, e As EventArgs) Handles btnMetroTheme.Click
        StyleManager1.ManagerStyle = DotNetBar.eStyle.Metro   '8
        btnBluetheme.Checked = False
        btnSilvertheme.Checked = False
        btnVistatheme.Checked = False
        btnBlackTheme.Checked = False
        btnWin7theme.Checked = False
        btnFutureTheme.Checked = False

        CType(sender, DevComponents.DotNetBar.ButtonItem).Checked = True
        clsMarsUI.MainUI.SaveRegistry("appThemeX", Convert.ToInt32(StyleManager1.ManagerStyle))
    End Sub
    Private Sub btnBlackTheme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBlackTheme.Click
        StyleManager1.ManagerStyle = DotNetBar.eStyle.Office2010Black '6
        btnBluetheme.Checked = False
        btnSilvertheme.Checked = False
        btnVistatheme.Checked = False
        btnWin7theme.Checked = False
        btnFutureTheme.Checked = False
        btnMetroTheme.Checked = False
        CType(sender, DevComponents.DotNetBar.ButtonItem).Checked = True
        clsMarsUI.MainUI.SaveRegistry("appThemeX", Convert.ToInt32(StyleManager1.ManagerStyle))
    End Sub

    Private Sub btnBluetheme_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBluetheme.Click
        StyleManager1.ManagerStyle = DotNetBar.eStyle.Office2010Blue '5
        btnBlackTheme.Checked = False
        btnSilvertheme.Checked = False
        btnVistatheme.Checked = False
        btnWin7theme.Checked = False
        btnFutureTheme.Checked = False
        btnMetroTheme.Checked = False
        CType(sender, DevComponents.DotNetBar.ButtonItem).Checked = True

        clsMarsUI.MainUI.SaveRegistry("appThemeX", Convert.ToInt32(StyleManager1.ManagerStyle))
    End Sub

    Private Sub btnSilvertheme_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSilvertheme.Click
        StyleManager1.ManagerStyle = DotNetBar.eStyle.Office2010Silver '4
        btnBluetheme.Checked = False
        btnBlackTheme.Checked = False
        btnVistatheme.Checked = False
        btnWin7theme.Checked = False
        btnFutureTheme.Checked = False
        btnMetroTheme.Checked = False
        CType(sender, DevComponents.DotNetBar.ButtonItem).Checked = True
        clsMarsUI.MainUI.SaveRegistry("appThemeX", Convert.ToInt32(StyleManager1.ManagerStyle))
    End Sub

    Private Sub btnVistatheme_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVistatheme.Click
        StyleManager1.ManagerStyle = DotNetBar.eStyle.Office2007VistaGlass '3
        btnBluetheme.Checked = False
        btnSilvertheme.Checked = False
        btnBlackTheme.Checked = False
        btnWin7theme.Checked = False
        btnFutureTheme.Checked = False
        btnMetroTheme.Checked = False
        CType(sender, DevComponents.DotNetBar.ButtonItem).Checked = True
        clsMarsUI.MainUI.SaveRegistry("appThemeX", Convert.ToInt32(StyleManager1.ManagerStyle))
    End Sub

    Private Sub btnWin7theme_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnWin7theme.Click
        StyleManager1.ManagerStyle = DotNetBar.eStyle.Windows7Blue  '7
        btnBluetheme.Checked = False
        btnSilvertheme.Checked = False
        btnVistatheme.Checked = False
        btnBlackTheme.Checked = False
        btnFutureTheme.Checked = False
        btnMetroTheme.Checked = False
        CType(sender, DevComponents.DotNetBar.ButtonItem).Checked = True
        clsMarsUI.MainUI.SaveRegistry("appThemeX", Convert.ToInt32(StyleManager1.ManagerStyle))
    End Sub

    Private Sub btnFuturetheme_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFutureTheme.Click
        StyleManager1.ManagerStyle = DotNetBar.eStyle.VisualStudio2010Blue  '8
        btnBluetheme.Checked = False
        btnSilvertheme.Checked = False
        btnVistatheme.Checked = False
        btnBlackTheme.Checked = False
        btnWin7theme.Checked = False
        btnMetroTheme.Checked = False
        CType(sender, DevComponents.DotNetBar.ButtonItem).Checked = True
        clsMarsUI.MainUI.SaveRegistry("appThemeX", Convert.ToInt32(StyleManager1.ManagerStyle))
    End Sub
#End Region
    Dim tooltip As DevComponents.DotNetBar.SuperTooltip = New DevComponents.DotNetBar.SuperTooltip

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Dim fullSystemSearch As Boolean = swFullSystemSearch.Value

        If fullSystemSearch = False Then
            tooltip.SetSuperTooltip(txtSearch, Nothing)

            Try
                If txtSearch.Text = "" Then
                    For Each nx As AdvTree.Node In tvMain.Nodes
                        For Each ch As AdvTree.Node In nx.Nodes
                            ch.Visible = True
                        Next
                    Next
                Else
                    For Each nx As AdvTree.Node In tvMain.Nodes
                        For Each ch As AdvTree.Node In nx.Nodes
                            If ch.Text.ToLower.Contains(txtSearch.Text.ToLower) = False Then
                                ch.Visible = False
                            Else
                                ch.Visible = True
                            End If
                        Next
                    Next
                End If
            Catch ex As Exception

            End Try
        Else
            Dim SQL As String
            Dim oRs As ADODB.Recordset
            Dim itemList As ArrayList = New ArrayList
            Dim columns() As String = getDetailColumns()

            If txtSearch.Text = "" Then
                tvExplorer_AfterNodeSelect(Nothing, Nothing)
                crumb.Visible = True
                Return
            End If


            Dim info As DevComponents.DotNetBar.SuperTooltipInfo = tooltip.GetSuperTooltip(txtSearch)

            If info Is Nothing And txtSearch.Text.Length < 3 Then
                info = New DevComponents.DotNetBar.SuperTooltipInfo("Search Tip", "", "Search will start after 3 or more characters have been entered. You can also use the wildcard character '%'", Nothing, Nothing, DotNetBar.eTooltipColor.Yellow)
                tooltip.SetSuperTooltip(txtSearch, info)
                tooltip.ShowTooltip(txtSearch)
            Else
                tooltip.SetSuperTooltip(txtSearch, Nothing)
            End If

            If txtSearch.Text.Length < 3 Then Return

            crumb.Visible = False

            AppStatus(True)
            tvMain.Nodes.Clear()
            tvMain.Enabled = False
            txtSearch.Enabled = False
            tvMain.BeginUpdate()


            '//add reports
            SQL = "SELECT reportid FROM reportattr WHERE reporttitle like '%" & txtSearch.Text & "%'"
            oRs = clsMarsData.GetData(SQL)

            Dim rpt As cssreport

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False

                    If cssusers.userCanAccessReport(oRs("reportid").Value) Then
                        rpt = New cssreport(oRs("reportid").Value)

                        itemList.Add(rpt)
                    End If

                    oRs.MoveNext()
                Loop

                If viewStyle = AdvTree.eView.Tile Then
                    addReportTiles(itemList)
                Else
                    addReportsToDetails(columns, itemList)
                End If

                itemList = Nothing
            End If
            '//add packages
            itemList = New ArrayList
            SQL = "SELECT packid FROM packageattr WHERE packagename like '%" & txtSearch.Text & "%'"
            oRs = clsMarsData.GetData(SQL)

            Dim pack As Package

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    If cssusers.userCanAccessPackage(oRs("packid").Value) Then
                        pack = New Package(oRs("packid").Value)
                        itemList.Add(pack)
                    End If

                    oRs.MoveNext()
                Loop

                If viewStyle = AdvTree.eView.Tile Then
                    addPackageTiles(itemList)
                Else
                    addPackagesToDetails(columns, itemList)
                End If

                itemList = Nothing
            End If
            '//add automations
            itemList = New ArrayList

            SQL = "SELECT autoid FROM automationattr WHERE autoname like '%" & txtSearch.Text & "%'"
            oRs = clsMarsData.GetData(SQL)

            Dim auto As Automation

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    If cssusers.userCanAccessAutomation(oRs("autoid").Value) Then
                        auto = New Automation(oRs("autoid").Value)
                        itemList.Add(auto)
                    End If

                    oRs.MoveNext()
                Loop
                If viewStyle = AdvTree.eView.Tile Then
                    addAutomationTiles(itemList)
                Else
                    addAutomationsToDetails(columns, itemList)
                End If

                itemList = Nothing
            End If
            '//add event-baseds
            itemList = New ArrayList

            SQL = "SELECT eventid FROM eventattr6 WHERE eventname like '%" & txtSearch.Text & "%'"
            oRs = clsMarsData.GetData(SQL)

            Dim evt As EventBased

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    If cssusers.userCanAccessEvent(oRs("eventid").Value) Then
                        evt = New EventBased(oRs("eventid").Value)
                        itemList.Add(evt)
                    End If

                    oRs.MoveNext()
                Loop

                If viewStyle = AdvTree.eView.Tile Then
                    addEventBasedTiles(itemList)
                Else
                    addEventsToDetails(columns, itemList)
                End If

                itemList = Nothing
            End If
            '//add event-based packs
            itemList = New ArrayList
            SQL = "SELECT eventpackid FROM eventpackageattr WHERE packagename like '%" & txtSearch.Text & "%'"
            oRs = clsMarsData.GetData(SQL)

            Dim evp As EventBasedPackage

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    If cssusers.userCanAccessEventPackage(oRs("eventpackid").Value) Then
                        evp = New EventBasedPackage(oRs("eventpackid").Value)
                        itemList.Add(evp)
                    End If

                    oRs.MoveNext()
                Loop

                If viewStyle = AdvTree.eView.Tile Then
                    addEventPackTiles(itemList)
                Else
                    addEventPackagesToDetails(columns, itemList)
                End If

                itemList = Nothing
            End If

            oRs = Nothing

            tvMain.Enabled = True
            txtSearch.Enabled = True
            tvMain.EndUpdate()
            txtSearch.Focus()

            AppStatus(False)

        End If
    End Sub


    Private Sub Office2007StartButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Office2007StartButton1.Click
        Try
            If IO.File.Exists(IO.Path.Combine(Application.StartupPath, "communitypad.htm")) Then
                WebBrowser1.Navigate(IO.Path.Combine(Application.StartupPath, "communitypad.htm"))
            Else
                WebBrowser1.Navigate("http://mychristiansteven.com")
            End If
        Catch : End Try
    End Sub

    Dim selectedType As clsMarsScheduler.enScheduleType
    Dim selectedID As Integer
    Dim historyThread As Threading.Thread

    Private Sub tvMain_BeforeNodeDrop(sender As Object, e As DevComponents.AdvTree.TreeDragDropEventArgs) Handles tvMain.BeforeNodeDrop
        Dim id As Integer
        Dim table As String
        Dim parentColumn As String
        Dim keyColumn As String
        Dim type As String
        Dim otype As clsMarsScheduler.enScheduleType

        If clsMarsSecurity._HasGroupAccess("Folder Management") = False Then
            e.Cancel = True
            Return
        End If

        If tvExplorer.SelectedNode Is Nothing Then
            MessageBox.Show("Please select a folder on the left first before dropping items here", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
            e.Cancel = True
            Return
        End If

        For Each nn As AdvTree.Node In e.Nodes
            type = nn.Tag

            If nn.TreeControl Is tvExplorer Or nn.TreeControl Is Nothing Then
                MessageBox.Show("You cannot move a folder into itself", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                e.Cancel = True
                Return
            End If

            Select Case type
                Case "report"
                    table = "reportattr"
                    parentColumn = "parent"
                    keyColumn = "reportid"
                    otype = clsMarsScheduler.enScheduleType.REPORT
                Case "package"
                    table = "packageattr"
                    parentColumn = "parent"
                    keyColumn = "packid"
                    otype = clsMarsScheduler.enScheduleType.PACKAGE
                Case "automation"
                    table = "automationattr"
                    parentColumn = "parent"
                    keyColumn = "autoid"
                    otype = clsMarsScheduler.enScheduleType.AUTOMATION
                Case "event"
                    table = "eventattr6"
                    parentColumn = "parent"
                    keyColumn = "eventid"
                    otype = clsMarsScheduler.enScheduleType.EVENTBASED
                Case "event-package"
                    table = "eventpackageattr"
                    parentColumn = "parent"
                    keyColumn = "eventpackid"
                    otype = clsMarsScheduler.enScheduleType.EVENTPACKAGE
                Case "folder"
                    table = "folders"
                    parentColumn = "parent"
                    keyColumn = "folderid"
                    otype = clsMarsScheduler.enScheduleType.FOLDER
                Case Else
                    e.Cancel = True
                    Return
            End Select

            If clsMarsUI.candoRename(nn.Text, tvExplorer.SelectedNode.DataKey, otype) = False Then
                MessageBox.Show("The destination folder already contains an item named '" & nn.Text & "'. The operation will be cancelled.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                e.Cancel = True
                Exit For
            End If


            id = nn.DataKey


            If tvExplorer.SelectedNode.DataKey = id And type = "folder" Then
                MessageBox.Show("You cannot move a folder into itself. Come on man!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                e.Cancel = True
                Return
            ElseIf clsMarsUI.MainUI.IsMoveValid(nn.DataKey, tvExplorer.SelectedNode.DataKey) = False Then
                MessageBox.Show("You cannot move a folder into its child", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                e.Cancel = True
                Return
            End If

            Dim SQL As String = "UPDATE " & table & " SET " & parentColumn & " = " & tvExplorer.SelectedNode.DataKey & " WHERE " & keyColumn & " = " & id

            clsMarsData.WriteData(SQL)
        Next


        loadData(tvExplorer.SelectedNode.DataKey)

        For Each ff As frmMainWin In crdxCommon.gwindows
            Try
                If ff.Tag <> Me.Tag Then ff.loadData(ff.tvExplorer.SelectedNode.DataKey, True)
            Catch : End Try
        Next

        For Each nn As AdvTree.Node In e.Nodes
            Dim nx As AdvTree.Node = tvMain.FindNodeByDataKey(nn.DataKey)

            If nx IsNot Nothing Then
                tvMain.SelectedNodes.Add(nx)
            End If
        Next
    End Sub

    Private Sub tvMain_AfterNodeSelect(ByVal sender As Object, ByVal e As DevComponents.AdvTree.AdvTreeNodeEventArgs) Handles tvMain.AfterNodeSelect


        If tvMain.SelectedNodes.Count = 0 Then
            ContextMenuBar1.SetContextMenuEx(tvMain, mnudesktopMenu)
        ElseIf tvMain.SelectedNode.Parent Is Nothing Then
            ContextMenuBar1.SetContextMenuEx(tvMain, mnudesktopMenu)
        ElseIf tvMain.SelectedNodes.Count > 1 Then
            ContextMenuBar1.SetContextMenuEx(tvMain, mnuMultipleItems)
        Else
            Dim selectedNode As DevComponents.AdvTree.Node = tvMain.SelectedNode

            If selectedNode.Parent Is Nothing Then Return


            Select Case selectedNode.Parent.Text
                Case "Data-Driven Schedules", "Dynamic Schedules", "Bursting Schedules", "Single Schedules"
                    ContextMenuBar1.SetContextMenuEx(tvMain, Me.btnSingleContext)

                    If selectedNode.Parent.Text <> "Bursting Schedules" Then
                        btnConvertToPackage.Enabled = True
                    Else
                        btnConvertToPackage.Enabled = False
                    End If

                    'ucHistory.m_objectID = selectedNode.Tag
                    'ucHistory.m_scheduleType = clsMarsScheduler.enScheduleType.REPORT
                    'ucHistory.DrawScheduleHistory()
                    selectedID = selectedNode.DataKey
                    selectedType = clsMarsScheduler.enScheduleType.REPORT

                    If historyThread IsNot Nothing Then
                        If historyThread.IsAlive Then
                            historyThread.Abort()
                        End If
                    End If

                    historyThread = New Threading.Thread(AddressOf invokeHistoryDraw)
                    historyThread.Start()

                    '  Dim obj As scheduleProxy = New scheduleProxy(selectedNode.DataKey, clsMarsScheduler.enScheduleType.REPORT)
                    '  schedulePropertyGrid.
                Case "Package Reports"
                    ContextMenuBar1.SetContextMenuEx(tvMain, btnPackedReportMenu)
                Case "Data-Driven Packages", "Single Packages", "Dynamic Packages"
                    ContextMenuBar1.SetContextMenuEx(tvMain, btnPackageContext)
                    selectedID = selectedNode.DataKey
                    selectedType = clsMarsScheduler.enScheduleType.PACKAGE

                    If historyThread IsNot Nothing Then
                        If historyThread.IsAlive Then
                            historyThread.Abort()
                        End If
                    End If

                    historyThread = New Threading.Thread(AddressOf invokeHistoryDraw)
                    historyThread.Start()
                Case "Automation Schedules"
                    ContextMenuBar1.SetContextMenuEx(tvMain, btnAutomationSchedules)
                    selectedID = selectedNode.DataKey
                    selectedType = clsMarsScheduler.enScheduleType.AUTOMATION

                    If historyThread IsNot Nothing Then
                        If historyThread.IsAlive Then
                            historyThread.Abort()
                        End If
                    End If

                    historyThread = New Threading.Thread(AddressOf invokeHistoryDraw)
                    historyThread.Start()
                Case "Event-Based Schedules"
                    ContextMenuBar1.SetContextMenuEx(tvMain, btneventBasedSchedules)
                    selectedID = selectedNode.DataKey
                    selectedType = clsMarsScheduler.enScheduleType.EVENTBASED

                    If historyThread IsNot Nothing Then
                        If historyThread.IsAlive Then
                            historyThread.Abort()
                        End If
                    End If

                    historyThread = New Threading.Thread(AddressOf invokeHistoryDraw)
                    historyThread.Start()
                Case "Event-Based Packages"
                    ContextMenuBar1.SetContextMenuEx(tvMain, btnEventBasedPacks)
                    selectedID = selectedNode.DataKey
                    selectedType = clsMarsScheduler.enScheduleType.EVENTPACKAGE

                    If historyThread IsNot Nothing Then
                        If historyThread.IsAlive Then
                            historyThread.Abort()
                        End If
                    End If

                    historyThread = New Threading.Thread(AddressOf invokeHistoryDraw)
                    historyThread.Start()
                Case "Folders"
                    ContextMenuBar1.SetContextMenuEx(tvMain, mnuFolders)
                    ucHistory.reset()
                Case Else
                    ContextMenuBar1.SetContextMenuEx(tvMain, Nothing)
                    ucHistory.reset()
            End Select

            Try
                If tvExplorer.SelectedNode IsNot Nothing AndAlso tvMain.SelectedNode.Parent IsNot Nothing Then saveFolderLastSelectedObject(tvExplorer.SelectedNode.DataKey, tvMain.SelectedNode.DataKey)
            Catch : End Try
        End If
    End Sub

    Sub invokeHistoryDraw()
        updateHistory(selectedID, selectedType)
    End Sub


    Private Delegate Sub updateHistoryDelegate(ByVal s As Integer, ByVal n As clsMarsScheduler.enScheduleType)
    Public Sub updateHistory(ByVal nID As Integer, ByVal type As clsMarsScheduler.enScheduleType)

        If InvokeRequired = False Then
            ucHistory.m_objectID = nID
            ucHistory.m_scheduleType = type

            If type = clsMarsScheduler.enScheduleType.EVENTBASED Then
                ucHistory.DrawEventHistory()
            Else
                ucHistory.DrawScheduleHistory()
            End If

        Else
            BeginInvoke(New updateHistoryDelegate(AddressOf updateHistory), New Object() {nID, type})
        End If

    End Sub
    Private Sub tvMain_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvMain.DoubleClick
        Try
            If tvMain.SelectedNode Is Nothing Then Return

            If tvMain.SelectedNode.Parent IsNot Nothing Then
                Select Case getSelectedObjectTypeFromParent(tvMain.SelectedNode.Parent.Text)
                    Case "folder"
                        Dim id As Integer = tvMain.SelectedNode.DataKey
                        Dim nn As DevComponents.AdvTree.Node = searchTree(id)

                        If nn IsNot Nothing Then
                            tvExplorer.SelectedNode = nn
                            nn.EnsureVisible()
                        End If
                    Case "report"
                        btnSingleProperties_Click(sender, e)
                    Case "package"
                        mnuPackageProperties_Click(sender, e)
                    Case "automation"
                        mnuAutoProp_Click(sender, e)
                    Case "event"
                        mnuEventProp_Click(sender, e)
                    Case "event-package"
                        mnueventPackProps_Click(sender, e)
                    Case "packed report"
                        btnPackagedProperties_Click(sender, e)
                    Case Else
                        Return
                End Select
            End If
        Catch : End Try
    End Sub

    Private Sub selectSubFolder(ByVal parent As AdvTree.Node, ByVal subfolderName As String)
        For Each nx As AdvTree.Node In parent.Nodes
            If nx.Text.ToLower = subfolderName.ToLower Then
                tvExplorer.SelectedNode = nx
                Exit For
            End If
        Next
    End Sub

    Public Sub buildCrumbBar(Optional ByVal parentFolderID As Integer = 0, Optional ByVal parentBar As DevComponents.DotNetBar.CrumbBarItem = Nothing)
        If parentFolderID = 0 Then crumb.Items.Clear() '//start from scratch


        Dim parentFolder As folder = New folder(parentFolderID)

        If parentBar Is Nothing Then
            parentBar = New DevComponents.DotNetBar.CrumbBarItem
            parentBar.Text = "Home"
            parentBar.Image = Image.FromFile(getAssetLocation("node.png"))
            parentBar.Visible = True
            parentBar.Tag = "0"

            crumb.Items.Add(parentBar)
            crumb.SelectedItem = parentBar
        End If

        Dim allFolders As ArrayList = parentFolder.getSubFolders

        If allFolders.Count > 0 Then
            parentBar.SubItems.Clear()

            '//add folders
            For Each fld As folder In allFolders
                Dim crumbIt As DevComponents.DotNetBar.CrumbBarItem = New DevComponents.DotNetBar.CrumbBarItem
                crumbIt.Text = fld.folderName
                crumbIt.Tag = fld.folderID

                crumbIt.Image = resizeImage(My.Resources.Folder_256x2561, 16, 16)


                parentBar.SubItems.Add(crumbIt)

                buildCrumbBar(fld.folderID, crumbIt)
            Next
        End If
    End Sub

    Private Sub slSize_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles slSize.ValueChanged
        viewStyle = AdvTree.eView.Tile

        If slSize.Value = 0 Then
            slSize.Tooltip = "16x16"
            iconSize = New Size(16, 16)
        ElseIf slSize.Value > 0 And slSize.Value <= 25 Then
            slSize.Tooltip = "24x24"
            iconSize = New Size(24, 24)
        ElseIf slSize.Value > 25 And slSize.Value <= 50 Then
            slSize.Tooltip = "32x32"
            iconSize = New Size(32, 32)
        ElseIf slSize.Value > 50 And slSize.Value <= 75 Then
            slSize.Tooltip = "64x64"
            iconSize = New Size(64, 64)
        ElseIf slSize.Value > 75 And slSize.Value <= 100 Then
            slSize.Tooltip = "128x128"
            iconSize = New Size(128, 128)
        End If

        ' clsMarsUI.MainUI.SaveRegistry("iconsize", iconSize.Width)
        If tvMain.View = AdvTree.eView.Tile Then
            saveViewStyleForFolder("tile")
        Else
            saveViewStyleForFolder("tree")
        End If

        Application.DoEvents()

        Dim T As Threading.Thread = New Threading.Thread(AddressOf resizeIconsA)
        T.Start()

        clsMarsUI.MainUI.SaveRegistry("iconsize", iconSize.Width)

        saveViewStyleForFolder("tile")
    End Sub

    Private Delegate Sub resizeIconsDelegate()

    Sub resizeIconsA()
        If InvokeRequired = False Then
            Dim singleReportImage, dynamicReportImage, datadrivenReportImage, burstingReportImage, singlePackageImage, dynamicPackageImage, datadrivenPackageImage, automationImage, eventImage, eventPackageImage, folderNormalImage, folderForbiddenImage As Image



            For Each topNode As AdvTree.Node In tvMain.Nodes
                If topNode.Parent Is Nothing Then
                    For Each nn As AdvTree.Node In topNode.Nodes
                        Dim type As String = nn.Tag
                        Dim id As Integer = nn.DataKey

                        Select Case type
                            Case "report"
                                Dim obj As cssreport = New cssreport(id)

                                If obj.isdatadriven Then
                                    If datadrivenReportImage Is Nothing Then
                                        datadrivenReportImage = resizeImage(obj.reportImage, iconSize)
                                    End If

                                    nn.Image = datadrivenReportImage
                                ElseIf obj.isdynamic Then
                                    If dynamicReportImage Is Nothing Then
                                        dynamicReportImage = resizeImage(obj.reportImage, iconSize)
                                    End If

                                    nn.Image = dynamicReportImage
                                ElseIf obj.isbursting Then
                                    If burstingReportImage Is Nothing Then
                                        burstingReportImage = resizeImage(obj.reportImage, iconSize)
                                    End If

                                    nn.Image = burstingReportImage
                                Else
                                    If singleReportImage Is Nothing Then
                                        singleReportImage = resizeImage(obj.reportImage, iconSize)
                                    End If

                                    nn.Image = singleReportImage
                                End If

                                If obj.status = False Then
                                    nn.Image = MakeGrayscale3(nn.Image)
                                End If
                                ' nn.Image = resizeImage(obj.reportImage, iconSize)
                            Case "package"
                                Dim obj As Package = New Package(id)

                                If obj.isDataDriven Then
                                    If datadrivenPackageImage Is Nothing Then
                                        datadrivenPackageImage = resizeImage(obj.packageImage, iconSize)
                                    End If

                                    nn.Image = datadrivenPackageImage
                                ElseIf obj.isDynamic Then
                                    If dynamicPackageImage Is Nothing Then
                                        dynamicPackageImage = resizeImage(obj.packageImage, iconSize)
                                    End If

                                    nn.Image = dynamicPackageImage
                                Else
                                    If singlePackageImage Is Nothing Then
                                        singlePackageImage = resizeImage(obj.packageImage, iconSize)
                                    End If

                                    nn.Image = singlePackageImage
                                End If

                                If obj.m_status = False Then
                                    nn.Image = MakeGrayscale3(nn.Image)
                                End If

                                'nn.Image = resizeImage(obj.packageImage, iconSize)
                            Case "automation"
                                Dim obj As Automation = New Automation(id)

                                If automationImage Is Nothing Then
                                    automationImage = resizeImage(obj.automationImage, iconSize)
                                End If

                                nn.Image = automationImage

                                If obj.Status = False Then
                                    nn.Image = MakeGrayscale3(nn.Image)
                                End If
                            Case "event"
                                Dim obj As EventBased = New EventBased(id)

                                If eventImage Is Nothing Then
                                    eventImage = resizeImage(obj.eventImage, iconSize)
                                End If

                                nn.Image = eventImage

                                If obj.Status = False Then
                                    nn.Image = MakeGrayscale3(nn.Image)
                                End If
                            Case "event-package"
                                Dim obj As EventBasedPackage = New EventBasedPackage(id)

                                If eventPackageImage Is Nothing Then
                                    eventPackageImage = resizeImage(obj.packageImage, iconSize)
                                End If

                                If obj.Status = False Then
                                    nn.Image = MakeGrayscale3(nn.Image)
                                End If
                            Case "folder"
                                Dim obj As folder = New folder(id)

                                'nn.Image = resizeImage(My.Resources.Folder_256x2561, iconSize)
                                If gRole <> "Administrator" Then
                                    If m_allowedFolders.Contains(id) = False And m_isGroupFolderAccessRestricted Then
                                        If folderForbiddenImage Is Nothing Then
                                            folderForbiddenImage = resizeImage(My.Resources.Folder_256x256_forbidden, iconSize)
                                        End If

                                        nn.Image = folderForbiddenImage
                                    Else
                                        If folderImage Is Nothing Then
                                            folderImage = resizeImage(My.Resources.Folder_256x2561, iconSize)
                                        End If

                                        nn.Image = folderImage
                                    End If
                                Else
                                    If folderImage Is Nothing Then
                                        folderImage = resizeImage(My.Resources.Folder_256x2561, iconSize)
                                    End If

                                    nn.Image = folderImage
                                End If
                        End Select
                    Next
                Else
                    Dim type As String = topNode.Tag
                    Dim id As Integer = topNode.DataKey

                    Select Case type
                        Case "report"
                            Dim obj As cssreport = New cssreport(id)

                            topNode.Image = resizeImage(obj.reportImage, iconSize)
                        Case "package"
                            Dim obj As Package = New Package(id)

                            topNode.Image = resizeImage(obj.packageImage, iconSize)
                        Case "automation"
                            Dim obj As Automation = New Automation(id)

                            topNode.Image = resizeImage(obj.automationImage, iconSize)
                        Case "event"
                            Dim obj As EventBased = New EventBased(id)

                            topNode.Image = resizeImage(obj.eventImage, iconSize)
                        Case "event-package"
                            Dim obj As EventBasedPackage = New EventBasedPackage(id)

                            topNode.Image = resizeImage(obj.packageImage, iconSize)
                        Case "folder"
                            Dim obj As folder = New folder(id)

                            If gRole <> "Administrator" Then
                                If m_allowedFolders.Contains(id) = False And m_isGroupFolderAccessRestricted Then
                                    topNode.Image = resizeImage(My.Resources.Folder_256x256_forbidden, iconSize)
                                Else
                                    topNode.Image = resizeImage(My.Resources.Folder_256x2561, iconSize)
                                End If
                            Else
                                topNode.Image = resizeImage(My.Resources.Folder_256x2561, iconSize)
                            End If
                    End Select
                End If
            Next
        Else
            BeginInvoke(New resizeIconsDelegate(AddressOf resizeIconsA), Nothing)
        End If


    End Sub
    Sub resizeIcons()
        If InvokeRequired = False Then
            tvExplorer_AfterNodeSelect(Nothing, Nothing)
        Else
            BeginInvoke(New resizeIconsDelegate(AddressOf resizeIcons), Nothing)
        End If
    End Sub

    Private Sub selectCreatedItem()
        If frmMainWin.itemToSelect IsNot Nothing Then '//select the folder where the new schedule has gone into
            Dim nx As AdvTree.Node = Me.searchTree(frmMainWin.itemToSelect.folderID) ' tvExplorer.FindNodeByDataKey(frmMainWin.itemToSelect.folderID)

            If nx IsNot Nothing Then
                tvExplorer.SelectedNode = nx
                nx.Expand()
                nx.EnsureVisible()

                Dim fld As folder = New folder(nx.DataKey)
                nx.Text = fld.folderName & " (" & fld.getTotalContents & ")"

                Dim itemNode As AdvTree.Node = tvMain.FindNodeByText(frmMainWin.itemToSelect.itemName)

                If itemNode IsNot Nothing Then
                    tvMain.SelectedNode = itemNode
                End If
            End If

            frmMainWin.itemToSelect = Nothing
        End If
    End Sub

    Private Sub btnSingleSchedule_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSingleSchedule.Click, mnudtNewSingleSchedule.Click
        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then

            showAboutToViolateWarning()

            If isAllowedAccessToFolder() = False Then Return

            Dim selectedNode As DevComponents.AdvTree.Node = tvExplorer.SelectedNode

            Dim singleSchedule As frmSingleScheduleWizard = New frmSingleScheduleWizard

            If tvExplorer.SelectedNode IsNot Nothing Then
                gParentID = tvExplorer.SelectedNode.DataKey
                gParent = tvExplorer.SelectedNode.FullPath
            End If

            singleSchedule.ShowDialog()


            If selectedNode IsNot Nothing Then loadData(selectedNode.DataKey)

            selectCreatedItem()

            updateScheduleCount()
        End If
    End Sub

    Private Sub btnPackage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPackage.Click, mnudtNewPackage.Click
        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
            showAboutToViolateWarning()

            If isAllowedAccessToFolder() = False Then Return

            Dim packageSchedule As frmPackWizard = New frmPackWizard

            If tvExplorer.SelectedNode IsNot Nothing Then
                gParentID = tvExplorer.SelectedNode.DataKey
                gParent = tvExplorer.SelectedNode.FullPath
            End If

            packageSchedule.ShowDialog()

            If tvExplorer.SelectedNode IsNot Nothing Then loadData(tvExplorer.SelectedNode.DataKey)

            selectCreatedItem()
            updateScheduleCount()

        End If
    End Sub

    Private Sub btnAutomationSchedule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAutomationSchedule.Click, mnudtNewAutomationSchedule.Click
        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
            showAboutToViolateWarning()

            If isAllowedAccessToFolder() = False Then Return

            Dim selectedNode As DevComponents.AdvTree.Node = tvExplorer.SelectedNode

            Dim automationSchedule As frmAutoSchedule = New frmAutoSchedule

            If tvExplorer.SelectedNode IsNot Nothing Then
                gParentID = tvExplorer.SelectedNode.DataKey
                gParent = tvExplorer.SelectedNode.FullPath
            End If

            automationSchedule.ShowDialog()

            If selectedNode IsNot Nothing Then loadData(selectedNode.DataKey)

            selectCreatedItem()
            updateScheduleCount()
        End If
    End Sub

    Private Sub ButtonItem10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem10.Click
        tmSchedulerInfo.Stop()
        Application.Exit()
    End Sub

    Private Sub btnLogOut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogOut.Click

        If MessageBox.Show("Are you sure you would like to log out of SQL-RD?", Application.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
            Process.Start(Application.ExecutablePath)

            End
        End If
    End Sub

    Private Sub ButtonItem6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenReport.Click

    End Sub

    Private Sub btnDynamicSchedule_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDynamicSchedule.Click, mnudtNewDynamicSchedule.Click
        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
            If IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.s2_DynamicSched) = False Then
                _NeedUpgrade(gEdition.ENTERPRISEPRO, btnDynamicSchedule, "Dynamic Schedules")
                Return
            End If

            showAboutToViolateWarning()

            If isAllowedAccessToFolder() = False Then Return

            Dim dyn As frmDynamicSchedule = New frmDynamicSchedule

            If tvExplorer.SelectedNode IsNot Nothing Then
                gParentID = tvExplorer.SelectedNode.DataKey
                gParent = tvExplorer.SelectedNode.FullPath
            End If

            dyn.ShowDialog()

            If tvExplorer.SelectedNode IsNot Nothing Then loadData(tvExplorer.SelectedNode.DataKey)

            selectCreatedItem()
            updateScheduleCount()
        End If
    End Sub

    Private Sub btnDataDriven_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDataDriven.Click, mnudtNewDataDriven.Click

        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
            If IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.s5_DataDrivenSched) = False Then
                _NeedUpgrade(gEdition.ENTERPRISEPRO, btnDataDriven, "Data-Driven Schedules")
                Return
            End If

            showAboutToViolateWarning()

            If isAllowedAccessToFolder() = False Then Return

            Dim selectedNode As DevComponents.AdvTree.Node = tvExplorer.SelectedNode
            Dim dd As frmRDScheduleWizard = New frmRDScheduleWizard

            If tvExplorer.SelectedNode IsNot Nothing Then
                gParentID = tvExplorer.SelectedNode.DataKey
                gParent = tvExplorer.SelectedNode.FullPath
            End If

            dd.ShowDialog()


            If tvExplorer.SelectedNode IsNot Nothing Then loadData(tvExplorer.SelectedNode.DataKey)

            selectCreatedItem()
            updateScheduleCount()
        End If
    End Sub



    Private Sub btnEventBased_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEventBased.Click, mnudtNewEventSchedule.Click
        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
            If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.s3_EventBasedScheds) = False Then
                _NeedUpgrade(gEdition.ENTERPRISEPROPLUS, btnEventBased, "Event-Based Schedules")
                Return
            End If

            showAboutToViolateWarning()

            If isAllowedAccessToFolder() = False Then Return

            Dim selectedNode As DevComponents.AdvTree.Node = tvExplorer.SelectedNode
            Dim eb As frmEventWizard6 = New frmEventWizard6

            If tvExplorer.SelectedNode IsNot Nothing Then
                gParentID = tvExplorer.SelectedNode.DataKey
                gParent = tvExplorer.SelectedNode.FullPath
            End If

            eb.ShowDialog()

            If tvExplorer.SelectedNode IsNot Nothing Then loadData(tvExplorer.SelectedNode.DataKey)

            selectCreatedItem()
            updateScheduleCount()
        End If
    End Sub

    Private Sub btnDataDrivenPackage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDataDrivenPackage.Click, mnudtNewDataDrivenPackage.Click
        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
            If IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.s5_DataDrivenSched) = False Then
                _NeedUpgrade(gEdition.ENTERPRISEPRO, btnDataDriven, "Data-Driven Schedules")
                Return
            End If

            showAboutToViolateWarning()

            If isAllowedAccessToFolder() = False Then Return

            Dim selectedNode As DevComponents.AdvTree.Node = tvExplorer.SelectedNode
            Dim ddp As frmDataDrivenPackage = New frmDataDrivenPackage

            If tvExplorer.SelectedNode IsNot Nothing Then
                gParentID = tvExplorer.SelectedNode.DataKey
                gParent = tvExplorer.SelectedNode.FullPath
            End If

            ddp.ShowDialog()


            If tvExplorer.SelectedNode IsNot Nothing Then loadData(tvExplorer.SelectedNode.DataKey)

            selectCreatedItem()
            updateScheduleCount()
        End If
    End Sub

    Private Sub btnDynamicPackage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDynamicPackage.Click, mnudtNewDynamicPackage.Click
        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
            If IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.s2_DynamicSched) = False Then
                _NeedUpgrade(gEdition.ENTERPRISEPRO, btnDynamicPackage, "Dynamic Schedules")
                Return
            End If

            showAboutToViolateWarning()

            If isAllowedAccessToFolder() = False Then Return

            Dim selectedNode As DevComponents.AdvTree.Node = tvExplorer.SelectedNode
            Dim ddp As frmDynamicPackage = New frmDynamicPackage


            If tvExplorer.SelectedNode IsNot Nothing Then
                gParentID = tvExplorer.SelectedNode.DataKey
                gParent = tvExplorer.SelectedNode.FullPath
            End If

            ddp.ShowDialog()


            If tvExplorer.SelectedNode IsNot Nothing Then loadData(tvExplorer.SelectedNode.DataKey)

            selectCreatedItem()
            updateScheduleCount()
        End If
    End Sub

    Private Sub btnEventBasedPackage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEventBasedPackage.Click, mnudtNewEventPackage.Click
        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
            If IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.s3_EventBasedScheds) = False Then
                _NeedUpgrade(gEdition.ENTERPRISEPRO, btnEventBasedPackage, "Event-Based Schedules")
                Return
            End If

            showAboutToViolateWarning()

            If isAllowedAccessToFolder() = False Then Return

            Dim selectedNode As DevComponents.AdvTree.Node = tvExplorer.SelectedNode
            Dim ebp As frmEventPackage = New frmEventPackage

            If tvExplorer.SelectedNode IsNot Nothing Then
                gParentID = tvExplorer.SelectedNode.DataKey
                gParent = tvExplorer.SelectedNode.FullPath
            End If

            ebp.ShowDialog()

            If tvExplorer.SelectedNode IsNot Nothing Then loadData(tvExplorer.SelectedNode.DataKey)

            selectCreatedItem()
            updateScheduleCount()
        End If
    End Sub

    Private Sub btnCustomCals_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCustomCals.Click
        Dim cal As frmCalendarExplorer = New frmCalendarExplorer

        cal.Show()
    End Sub



    Private Sub btnSingleProperties_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSingleProperties.Click
        If tvMain.SelectedNode Is Nothing Then Return

        Dim rptID As Integer = tvMain.SelectedNode.DataKey
        Dim prop As frmSingleProp = New frmSingleProp

        prop.EditSchedule(rptID)

        editItemRow(rptID, clsMarsScheduler.enScheduleType.REPORT)

        'If tvExplorer.SelectedNode IsNot Nothing Then
        '    loadData(tvExplorer.SelectedNode.DataKey, False)
        'Else
        '    Dim rpt As crdreport = New crdreport(rptID)
        '    Dim parent As Integer = rpt.reportParent

        '    loadData(parent, False)
        'End If


        'Dim node As AdvTree.Node = tvMain.FindNodeByDataKey(rptID)

        'If node IsNot Nothing Then
        '    tvMain.SelectedNode = node
        '    node.EnsureVisible()
        'End If
    End Sub

    Private Sub mnuPackageProperties_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPackageProperties.Click
        Try
            If tvMain.SelectedNode Is Nothing Then Return

            Dim packID As Integer = tvMain.SelectedNode.DataKey
            Dim prop As frmPackageProp = New frmPackageProp

            prop.EditPackage(packID)

            editItemRow(packID, clsMarsScheduler.enScheduleType.PACKAGE)

            'Try
            '    If tvExplorer.SelectedNode IsNot Nothing Then
            '        loadData(tvExplorer.SelectedNode.DataKey, False)
            '    Else
            '        Dim pack As Package = New Package(packID)
            '        Dim parent As Integer = pack.parent

            '        loadData(parent, False)
            '    End If
            'Catch : End Try

            'Dim node As AdvTree.Node = tvMain.FindNodeByDataKey(packID)

            'If node IsNot Nothing Then
            '    tvMain.SelectedNode = node
            '    node.EnsureVisible()
            'End If
        Catch : End Try
    End Sub

    Private Sub mnuAutoProp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAutoProp.Click
        Dim autoID As Integer = tvMain.SelectedNode.DataKey
        Dim prop As frmAutoProp = New frmAutoProp

        prop.EditSchedule(autoID)

        editItemRow(autoID, clsMarsScheduler.enScheduleType.AUTOMATION)

        'If tvExplorer.SelectedNode IsNot Nothing Then
        '    loadData(tvExplorer.SelectedNode.DataKey, False)
        'Else
        '    Dim auto As Automation = New Automation(autoID)
        '    Dim parent As Integer = auto.autoParent

        '    loadData(parent, False)
        'End If

        'Dim node As AdvTree.Node = tvMain.FindNodeByDataKey(autoID)

        'If node IsNot Nothing Then
        '    tvMain.SelectedNode = node
        '    node.EnsureVisible()
        'End If
    End Sub

    Private Sub mnuEventProp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEventProp.Click
        Dim prop As frmEventProp = New frmEventProp

        Dim evtID As Integer = tvMain.SelectedNode.DataKey
        prop.EditSchedule(evtID)

        editItemRow(evtID, clsMarsScheduler.enScheduleType.EVENTBASED)

        'If tvExplorer.SelectedNode IsNot Nothing Then
        '    loadData(tvExplorer.SelectedNode.DataKey, True)
        'Else
        '    Dim ev As EventBased = New EventBased(evtID)
        '    Dim parent As Integer = ev.eventParent

        '    loadData(parent, False)
        'End If

        'Dim node As AdvTree.Node = tvMain.FindNodeByDataKey(evtID)

        'If node IsNot Nothing Then
        '    tvMain.SelectedNode = node
        '    node.EnsureVisible()
        'End If
    End Sub

    Private Sub mnueventPackProps_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnueventPackProps.Click
        Dim prop As frmEventPackageProp = New frmEventPackageProp
        Dim id As Integer = tvMain.SelectedNode.DataKey

        prop.EditPackage(id)

        editItemRow(id, clsMarsScheduler.enScheduleType.EVENTPACKAGE)

        'If tvExplorer.SelectedNode IsNot Nothing Then
        '    loadData(tvExplorer.SelectedNode.DataKey, True)
        'Else
        '    Dim evp As EventBasedPackage = New EventBasedPackage(id)
        '    Dim parent As Integer = evp.packageParent

        '    loadData(parent, False)
        'End If

        'Dim node As AdvTree.Node = tvMain.FindNodeByDataKey(id)

        'If node IsNot Nothing Then
        '    tvMain.SelectedNode = node
        '    node.EnsureVisible()
        'End If
    End Sub


    Private Sub btnCollaboration_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCollaboration.Click
        If IsFeatEnabled(gEdition.CORPORATE, featureCodes.sa1_MultiThreading) = False Then
            _NeedUpgrade(gEdition.CORPORATE, btnCollaboration, "Collaboration")
            Return
        End If


        '//check if we already in another ring
        Dim collabo As Boolean = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("collaboration", 0))
        Dim leader As String = clsMarsUI.MainUI.ReadRegistry("collaborationLeader", "")

        If collabo = True And String.Compare(leader, Environment.MachineName, True) <> 0 Then
            MessageBox.Show("Please configure Collaboration from the Lead Server (" & leader & ")", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        Dim coll As frmCollaborators = New frmCollaborators

        coll.Show()
    End Sub

    Private Sub btnOpenInNewWindow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnfolderOpenInNewWindow.Click
        Dim key As Object

        If folderTree.SelectedNode IsNot Nothing Then
            Dim sel As AdvTree.Node = folderTree.SelectedNode

            If sel.Tag = "folder" Then
                key = sel.DataKey
            End If
        End If

        Dim newForm As frmMainWin = New frmMainWin
        newForm.nodeToFind = key

        newForm.Show()
    End Sub

    Private Sub btnAddressBook_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddressBook.Click
        Dim ad As frmAddressBook = New frmAddressBook

        ad.Show()
    End Sub

    Private Sub btnUserManager_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUserManager.Click
        If clsMarsSecurity._HasGroupAccess("User Manager") = True Then
            If userMan Is Nothing Then
                userMan = New frmUserManager
            ElseIf userMan.IsDisposed Then
                userMan = New frmUserManager
            End If

            userMan.Show()
            userMan.Focus()
            userMan.BringToFront()
        End If
    End Sub

    Private Sub btnSMTPServers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSMTPServers.Click
        Dim smtp As frmSMTPServers = New frmSMTPServers

        smtp.Show()
    End Sub

    Private Sub btnOperationHours_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOperationHours.Click
        Dim ophours As frmOperationsExplorer = New frmOperationsExplorer

        ophours.Show()
    End Sub

    Private Sub btnRemoteConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoteConnect.Click
        Dim remoteCon As frmRemoteAdmin = New frmRemoteAdmin

        crdxCommon.executingForm = Me

        remoteCon.Show()
    End Sub

    Private Sub btnSupportFiles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSupportFiles.Click
        Dim sup As frmSupportFiles = New frmSupportFiles

        sup._CreateSupportFiles()
    End Sub

    Private Sub btnHelp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHelp.Click, btnSmallHelp.Click
        Try
            Process.Start(sAppPath & "sql-rd.chm")
        Catch : End Try
    End Sub

    Private Sub btnDemos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDemos.Click
        Try
            clsWebProcess.Start("http://www.christiansteven.com/sql-rd/demos/sql-rd_demos.htm")
        Catch : End Try
    End Sub

    Private Sub btnforums_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnforums.Click
        Try
            clsWebProcess.Start("http://www.mychristiansteven.com/forum/11-sql-rd")
        Catch : End Try
    End Sub

    Private Sub btnKbase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKbase.Click
        Try
            clsWebProcess.Start("http://mychristiansteven.com")
        Catch : End Try
    End Sub

    Private Sub requestHelp(ByVal sURL As String)
        On Error Resume Next
        Dim Obj As Object
        Dim sCompany As String
        Dim sName As String
        Dim sCustID As String
        Dim I As Integer
        Dim Sel As Object
        Dim cur As String
        Dim X As Integer
        Dim ScheduleType As String
        Dim MailType As String
        Dim Y As Double
        Dim oFile As FileVersionInfo = FileVersionInfo.GetVersionInfo(sAppPath & assemblyName)
        Dim oUI As clsMarsUI = New clsMarsUI

        sCompany = oUI.ReadRegistry("RegCo", "")
        sName = oUI.ReadRegistry("RegFirstName", "") & " " & oUI.ReadRegistry("RegLastName", "")
        sCustID = oUI.ReadRegistry("CustNo", "")
        ScheduleType = oUI.ReadRegistry("SQL-RDService", "")
        MailType = oUI.ReadRegistry("MailType", "")

        Dim wb = CreateObject("InternetExplorer.Application")

        wb.Visible = False

        wb.Navigate(sURL)


        Do While wb.Busy = True
            If I < 100 Then
                I = I + 5
            Else
                I = 0
            End If

            oUI.BusyProgress(I, "Connecting to ChristianSteven Support Server...")

            Y = I / 3

            If Y = CInt(I / 3) Then _Delay(1)
        Loop

        oUI.BusyProgress(90, "Loading system configuration", False)

        _Delay(1)

        AppStatus(False)

        For Each Obj In wb.Document.All.tags("input")
            If Obj.name = "Customer_Number" Then
                Obj.Value = sCustID
            ElseIf Obj.name = "Company_Name" Then
                Obj.Value = sCompany
            ElseIf Obj.name = "Customer_Name" Then
                Obj.Value = sName
            ElseIf Obj.name = "Product_Version" Then
                Obj.Value = oFile.FileVersion
            ElseIf Obj.name = "Build_Number" Then
                ''console.writeline(oFile.Comments.ToLower)

                Obj.Value = oFile.Comments.ToLower.Replace("build", String.Empty).Trim
            ElseIf Obj.name = "Operating_System" Then
                Obj.Value = Environment.OSVersion
            End If
        Next



        For Each Obj In wb.Document.All.tags("select")

            If Obj.name = "Schduler_Type" Then
                Select Case ScheduleType
                    Case "WindowsNT"
                        Obj.selectedIndex = 2
                    Case "WindowsApp"
                        Obj.selectedIndex = 1
                    Case "NONE"
                        Obj.selectedIndex = 0
                    Case Else
                        Obj.selectedIndex = 0
                End Select
            ElseIf Obj.name = "Email_Type" Then
                Select Case MailType.ToLower
                    Case "mapi"
                        If oUI.ReadRegistry("MAPIType", "").ToLower = "single" Then
                            Obj.selectedIndex = 1
                        Else
                            Obj.selectedIndex = 2
                        End If
                    Case "smtp"
                        Obj.selectedIndex = 0
                    Case "groupwise"
                        Obj.selectedIndex = 3
                    Case "none"
                        Obj.selectedIndex = 4
                    Case Else
                        Obj.selectedIndex = 4
                End Select
            End If
        Next

        wb.Visible = True

        Obj = Nothing

        oUI.BusyProgress(100, "", True)
    End Sub

    Private Sub btnLogCall_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogCall.Click
        Try
            Dim sup As frmSupportRequest = New frmSupportRequest
            sup.requestHelp("http://www.christiansteven.com/en/sup_req_sql-rd.htm")
        Catch : End Try
    End Sub

    Private Sub btnActivate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActivate.Click
        Dim oSys As New clsSystemTools

        oSys._ActivateSystem(True, True)
    End Sub

    Private Sub btnDeactivate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeactivate.Click
        Dim oSys As New clsSystemTools

        oSys._DeactivateSystem(True)
    End Sub

    Private Sub btnUpdateFeatures_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateFeatures.Click
        Dim wizard As frmFunctionUpgrade = New frmFunctionUpgrade

        wizard.ShowDialog()
    End Sub

    Private Sub btnValidateFeatures_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnValidateFeatures.Click
        clsMarsData.WriteData("UPDATE scheduleattr SET locked = 0")
        clsMarsData.WriteData("UPDATE eventattr6 SET locked = 0")

        Dim catcher As clsViolatorCatcher = New clsViolatorCatcher

        catcher.processData()
    End Sub

    Private Sub ButtonItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSwitchToLocal.Click
        If clsMarsSecurity._HasGroupAccess("System Migration") = True Then
            Dim sqlEdit As frmLocalSQLCon = New frmLocalSQLCon

            If sqlEdit.editSQLInfo() = True Then
                oUI.SaveRegistry("ConType", "LOCAL")
            Else
                Return
            End If

            MessageBox.Show("SQL-RD will now exit so that you can log into the new system", Application.ProductName, _
                MessageBoxButtons.OK, MessageBoxIcon.Information)

            Process.Start(sAppPath & assemblyName)

            Me.Close()
        End If
    End Sub

    Private Sub btnSwitchToODBC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSwitchToODBC.Click
        Dim s As String = ""

        If s.Length = 0 Then
            If clsMarsSecurity._HasGroupAccess("System Migration") = True Then
                If IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.sa6_StoreInODBC) = False Then
                    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE, btnSwitchToODBC, "ODBC Store")
                    Return
                End If

                If clsMarsSecurity._HasGroupAccess("System Migration") = True Then
                    Dim oLogin As New frmODBCLogin
                    Dim dsn, userid, password As String

                    If oLogin.EditLogins(dsn, userid, password) = True Then
                        Dim odbcCon As ADODB.Connection = New ADODB.Connection

                        odbcCon.Open(dsn, userid, password)

                        Dim dataCopy As frmContentSelector = New frmContentSelector

                        dataCopy.selectContent(odbcCon)

                        oUI.SaveRegistry("ConType", "ODBC")

                        MessageBox.Show("SQL-RD will now exit so that you can log into the new system", Application.ProductName, _
                                  MessageBoxButtons.OK, MessageBoxIcon.Information)

                        Process.Start(sAppPath & assemblyName)

                        Application.Exit()
                    End If
                Else
                    Return
                End If
            End If
        End If


    End Sub

    Private Sub btnMigrateToLocal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMigrateToLocal.Click
        If clsMarsSecurity._HasGroupAccess("System Migration") = True Then
            MessageBox.Show("SQL-RD needs to perform a system backup before proceeding", Application.ProductName, _
            MessageBoxButtons.OK, MessageBoxIcon.Information)

            Dim oTools As New clsSystemTools

            oTools._BackupSystem()

            Dim SQLExists As Boolean = False

            Dim svcs As ServiceController() = ServiceController.GetServices

            For Each svc As ServiceController In svcs
                If svc.DisplayName = clsMigration.m_DatabaseService Then
                    SQLExists = True
                End If
            Next


            If SQLExists = False Then
                Dim sMsg As String

                sMsg = "You do not have a local installation of SQL Server on this machine." & _
                "Please follow the instructions provided in the document located at:" & vbCrLf & _
                "http://www.christiansteven.com/_support/sqlinstall.pdf " & vbCrLf & _
                "and then try migrating again."

                Dim oMsg As New frmCustomMsg

                oMsg.ShowMsg(sMsg, "Migrate to local SQL Server", True)

            Else
                Try
                    Dim svc As ServiceController = New ServiceController(clsMigration.m_DatabaseService)

                    If svc.Status <> ServiceControllerStatus.Running Then
                        svc.Start()
                    End If
                Catch ex As Exception
                    _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 1363)
                End Try

                Dim sMsg As String

                sMsg = "WARNING: If a database called 'SQL-RD' currently exists in your local SQL Server, then it will be dropped and recreated. Proceed?"

                If MessageBox.Show(sMsg, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.No Then
                    Return
                End If

                Dim oSQL As New frmSQLAdmin


                Dim sConn As String = clsMigration.m_localConString
                Dim dataLocation As String = sAppPath & "DATA\"

                If IO.Directory.Exists(dataLocation) = False Then IO.Directory.CreateDirectory(dataLocation)

                If oSQL._CreateDatabase(Environment.MachineName & "\SQL-RD", dataLocation) = True Then

                    If clsMigration.MigrateSystem(sConn, "", "") = True Then

                        oUI.SaveRegistry("ConType", "LOCAL", , , True)
                        oUI.SaveRegistry("ConString", sConn, True, , True)

                        MessageBox.Show("SQL-RD will now exit so that you may log into the new system", _
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

                        Process.Start(sAppPath & assemblyName)

                        Me.Close()
                    End If

                End If

            End If
        End If
    End Sub

    Private Sub btnMigratetoODBC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMigratetoODBC.Click
        If clsMarsSecurity._HasGroupAccess("System Migration") = True Then
            If IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.sa6_StoreInODBC) = False Then
                _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE, btnMigratetoODBC, "ODBC Store")
                Return
            End If

            Dim sMsg As String

            sMsg = "To Migrate to SQL Server (or some other ODBC compliant database):" & vbCrLf & vbCrLf & _
            "1.  Create a new empty database instance in your ODBC Compliant database." & vbCrLf & vbCrLf & _
            "2.  On the SQL-RD PC, create a SYSTEM ODBC DSN. SQL authentication is " & _
            "preferred.  Ensure you select the new database as the 'default database'." & vbCrLf & vbCrLf & _
            "3.  In SQL-RD, go to Tools - System Tools - Migrate to SQL Server/ODBC" & vbCrLf & vbCrLf & _
            "4.  Select your new DSN from the dropdown list and enter your credentials (if you " & _
            "are not using Windows Authentication)" & vbCrLf & vbCrLf & _
            "5.  Click 'Go'." & vbCrLf & vbCrLf & _
            "Your local file system will be migrated to the new database, and SQL-RD will  use this " & _
            "new database as its default database." & vbCrLf & vbCrLf & _
            "Important:" & vbCrLf & vbCrLf & _
            "ChristianSteven Software does not provide support or instructions on how to use SQL Server " & _
            "or other databases.  If you do not have any experience with databases, please contact " & _
            "your database administrator and arrange for the migration to be performed by an expert."

            Dim oMsg As New frmCustomMsg

            If oMsg.ShowMsg(sMsg, "Migrate to SQL Server/ODBC", True, "http://www.christiansteven.com/using_odbc.pdf") = Windows.Forms.DialogResult.Cancel Then
                Return
            End If

            MessageBox.Show("SQL-RD needs to perform a system backup before proceeding", Application.ProductName, _
            MessageBoxButtons.OK, MessageBoxIcon.Information)

            Dim oTools As New clsSystemTools

            oTools._BackupSystem()

            Dim oSQL As New frmSQLAdmin

            oSQL.ShowDialog()
        End If
    End Sub

    Private Sub btnLoginInfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoginInfo.Click
        If clsMarsSecurity._HasGroupAccess("System Migration") = True Then
            Dim oLogin As New frmODBCLogin

            oLogin.EditLogins()
        End If
    End Sub

    Private Sub btnBackup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBackup.Click
        Dim oSys As New clsSystemTools

        oSys._BackupSystem()
    End Sub

    Private Sub btnRestore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRestore.Click
        Dim oSys As New clsSystemTools

        oSys._RestoreSystem()
    End Sub

    Private Sub btnDataItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDataItems.Click
        If IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.i1_Inserts) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE, btnDataItems, "Inserts")

            Return
        End If

        Dim dataItems As frmDataItems = New frmDataItems

        dataItems.ViewDataItems()
    End Sub

    Private Sub btnUserConstants_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUserConstants.Click
        Dim oNew As frmUserConstants = New frmUserConstants 'frmFormulaEditor = New frmFormulaEditor
        oNew.ShowInTaskbar = True

        oNew.Show()
    End Sub

    Private Sub crumb_SelectedItemChanged(ByVal sender As Object, ByVal e As DevComponents.DotNetBar.CrumbBarSelectionEventArgs) Handles crumb.SelectedItemChanged
        Try
            If e.NewSelectedItem Is Nothing Then Return

            If _ignoreBit Then Return

            Try
                If e.NewSelectedItem.Tag = "" Then e.NewSelectedItem.Tag = "0"
            Catch : End Try

            If e.NewSelectedItem.Tag = "0" Then
                'tvExplorer.FindNodeByDataKey(e.NewSelectedItem.Tag)
                For Each n As DevComponents.AdvTree.Node In tvExplorer.Nodes
                    n.CollapseAll()
                    tvExplorer.SelectedNode = Nothing
                Next

                buildTiles(New folder(0))
            Else
                Dim n As DevComponents.AdvTree.Node = searchTree(e.NewSelectedItem.Tag)

                If n IsNot Nothing Then
                    tvExplorer.SelectedNode = n
                    n.EnsureVisible()
                End If
            End If
        Catch : End Try
    End Sub

    Private Sub btnSinglePreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSinglePreview.Click, btnPackedPreview.Click
        If tvMain.SelectedNode Is Nothing Then Return

        Dim id As Integer = tvMain.SelectedNode.DataKey

        Dim rpt As clsMarsReport = New clsMarsReport

        rpt.ViewReport(id, False)

    End Sub

    Private Sub btnSingleExecute_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSingleExecute.Click
        Dim oReport As clsMarsReport = New clsMarsReport
        Dim ShowMsg As Boolean
        Dim oMsg As frmMarsMsg = New frmMarsMsg
        Dim oSchedule As clsMarsScheduler = New clsMarsScheduler
        Dim nReportID As Integer
        Dim oExecute As clsMarsThreading = New clsMarsThreading

        crdxCommon.executingForm = Me

        nReportID = tvMain.SelectedNode.DataKey

        If clsMarsScheduler.isScheduleLocked(nReportID, clsMarsScheduler.enScheduleType.REPORT) = True Then
            showViolatorMessage()
            Return
        End If

        Dim oData As New clsMarsData
        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT Dynamic,Bursting FROM ReportAttr WHERE ReportID = " & nReportID)


        If oRs.EOF = False Then
            If IsNull(oRs.Fields(0).Value, 0) = 1 Then

                oExecute.xReportID = nReportID

                AppStatus(True)
                oExecute.DynamicScheduleThread()
                AppStatus(False)

                clsMarsAudit._LogAudit(tvMain.SelectedNode.Text, clsMarsAudit.ScheduleType.DYNAMIC, clsMarsAudit.AuditAction.EXECUTE)
            ElseIf IsNull(oRs.Fields(1).Value, 0) = 1 Then

                oExecute.xReportID = nReportID

                AppStatus(True)
                oExecute.BurstingScheduleThread()
                AppStatus(False)

                clsMarsAudit._LogAudit(tvMain.SelectedNode.Text, clsMarsAudit.ScheduleType.BURST, clsMarsAudit.AuditAction.EXECUTE)

            Else
                oExecute.xReportID = nReportID

                AppStatus(True)
                oExecute.SingleScheduleThread()
                AppStatus(False)

                clsMarsAudit._LogAudit(tvMain.SelectedNode.Text, clsMarsAudit.ScheduleType.SINGLES, clsMarsAudit.AuditAction.EXECUTE)
            End If
        End If

        editItemRow(nReportID, clsMarsScheduler.enScheduleType.REPORT)

        oRs.Close()

        oData.DisposeRecordset(oRs)
    End Sub

    Private Sub btnSingleCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSingleCopy.Click, btnpackCopy.Click, btnautoCopy.Click, _
        btneventCopy.Click, btneventpackCopy.Click

        If tvMain.SelectedNodes.Count > 0 Then
            Select Case tvMain.SelectedNode.Parent.Text
                Case "Data-Driven Schedules", "Dynamic Schedules", "Bursting Schedules", "Single Schedules"
                    gClipboard(0) = "Report"
                    btnSinglePaste.Enabled = True
                Case "Data-Driven Packages", "Single Packages", "Dynamic Packages"
                    gClipboard(0) = "Package"
                    btnpackPaste.Enabled = True
                Case "Automation Schedules"
                    gClipboard(0) = "Automation"
                    btnautoPaste.Enabled = True
                Case "Event-Based Schedules"
                    gClipboard(0) = "Event"
                    btneventPaste.Enabled = True
                Case "Event-Based Packages"
                    gClipboard(0) = "Event-Package"
                    btneventpackPaste.Enabled = True
                Case "Folders"
                    Return
            End Select

            gClipboard(1) = tvMain.SelectedNode.DataKey

            btnPasteItem.Enabled = True
            mnuDesktopPaste.Enabled = True
            btnSinglePaste.Enabled = True
            btnpackPaste.Enabled = True
            btnautoPaste.Enabled = True
            btneventPaste.Enabled = True
            btneventpackPaste.Enabled = True
        End If
    End Sub

    Private Sub pasteMenuHandler(stype As String)
        Try
            Select Case stype.ToLower
                Case "report"
                    pasteSingle()
                Case "package"
                    pastePackage()
                Case "automation"
                    pasteAuto()
                Case "event"
                    pasteEvent()
                Case "event package", "event-package"
                    pasteEventPackage()
            End Select

            If tvExplorer.SelectedNode IsNot Nothing Then loadData(tvExplorer.SelectedNode.DataKey)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 0)
        End Try
    End Sub
    Private Sub pasteSingle()
        Dim nCopy As Integer
        Dim oReport As clsMarsReport = New clsMarsReport
        Dim nParent As Integer
        Dim sType As String = ""

        oUI.BusyProgress(10, "Loading report...")

        If tvExplorer.SelectedNode Is Nothing Then
            sType = ""
        Else
            sType = tvExplorer.SelectedNode.Tag
        End If

        If sType.ToLower <> "folder" Then
            Dim oFolder As New frmFolders
            Dim nTemp() As String

            nTemp = oFolder.GetFolder()

            nParent = nTemp(1)
        Else
            nParent = tvExplorer.SelectedNode.DataKey
        End If

        nCopy = oReport.CopyReport(gClipboard(1), nParent)

        If nCopy > 0 Then
            Dim oProp As frmSingleProp = New frmSingleProp

            oUI.BusyProgress(99, "Loading new schedule properties...")

            oUI.BusyProgress(, , True)

            oProp.EditSchedule(nCopy)
        End If

    End Sub

    Private Sub btnSingleRename_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSingleRename.Click, btnpackRename.Click, btnautoRename.Click, btneventRename.Click, _
        btneventpackRename.Click, btnPackedRename.Click

        tvMain.SelectedNode.BeginEdit()
    End Sub

    Private Sub tvMain_CellEditEnding(ByVal sender As Object, ByVal e As DevComponents.AdvTree.CellEditEventArgs) Handles tvMain.AfterCellEdit
        On Error Resume Next

        If tvMain.SelectedNode Is Nothing Then Exit Sub
        Dim node As DevComponents.AdvTree.Node = tvMain.SelectedNode

        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim nID As Integer = node.DataKey
        Dim sSelType As String = node.Parent.Text
        Dim sCurrentNode As String
        Dim oRs As New ADODB.Recordset
        Dim sEvent As String
        Dim sType As String
        Dim sName As String
        Dim nParent As Integer
        Dim oUI As clsMarsUI = New clsMarsUI


        sCurrentNode = tvExplorer.SelectedNode.DataKey


        If e.NewText Is Nothing Then Exit Sub

        If e.NewText.Length = 0 Then e.Cancel = True : Return

        If clsMarsSecurity._HasGroupAccess("Folder Management") = False Then
            e.Cancel = True
            Return
        End If

        nParent = tvExplorer.SelectedNode.DataKey 'sCurrentNode.Split(":")(1)

        Select Case sSelType
            Case "Package Reports"
                Dim nCount As Integer
                Dim snewName As String = e.NewText

                If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = False Then
                    e.Cancel = True
                    Return
                End If

                oRs = clsMarsData.GetData("SELECT ReportTitle,PackID,Dynamic,Bursting FROM ReportAttr WHERE ReportID =" & nID)

                Dim nPackID As Integer = 0
                Dim nDynamic As Integer
                Dim nBursting As Integer

                If Not oRs Is Nothing Then
                    If oRs.EOF = False Then
                        sName = oRs(0).Value
                        nPackID = IsNull(oRs(1).Value, 0)
                        nDynamic = IsNull(oRs(2).Value, 0)
                        nBursting = IsNull(oRs(3).Value, 0)
                    End If

                    oRs.Close()
                End If

                If e.NewText.Contains(":") = False Then
                    SQL = "SELECT MAX(PackOrderID) FROM ReportAttr WHERE PackID =" & nPackID

                    oRs = clsMarsData.GetData(SQL)

                    If oRs Is Nothing Then
                        nCount = 1
                    Else
                        If oRs.EOF = False Then
                            Dim sTemp As String = oRs.Fields(0).Value

                            nCount = sTemp

                            nCount += 1
                        Else
                            nCount = 1
                        End If
                    End If

                    snewName = nCount & ":" & snewName

                    oRs.Close()
                End If

                SQL = "UPDATE ReportAttr SET ReportTitle = '" & SQLPrepare(snewName) & "' WHERE " & _
                                    "ReportID = " & nID

                If nPackID > 0 Then
                    clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.PACKED, clsMarsAudit.AuditAction.RENAME)
                ElseIf nDynamic = 1 Then
                    clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.DYNAMIC, clsMarsAudit.AuditAction.RENAME)
                ElseIf nBursting = 1 Then
                    clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.BURST, clsMarsAudit.AuditAction.RENAME)
                Else
                    clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.SINGLES, clsMarsAudit.AuditAction.RENAME)
                End If

                clsMarsData.WriteData(SQL)
            Case "Data-Driven Schedules", "Dynamic Schedules", "Bursting Schedules", "Single Schedules"

                Dim nCount As Integer
                Dim snewName As String = e.NewText

                If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = False Then
                    e.Cancel = True
                    Return
                End If

                'check to see if another with the same name exists in this folder
                If clsMarsUI.candoRename(e.NewText, nParent, clsMarsScheduler.enScheduleType.REPORT, nID) = False Then
                    MessageBox.Show(clsMarsUI.renameErrorstring, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    e.Cancel = True
                    Return
                End If

                oRs = clsMarsData.GetData("SELECT ReportTitle,PackID,Dynamic,Bursting FROM ReportAttr WHERE ReportID =" & nID)

                Dim nPackID As Integer = 0
                Dim nDynamic As Integer
                Dim nBursting As Integer

                If Not oRs Is Nothing Then
                    If oRs.EOF = False Then
                        sName = oRs(0).Value
                        nPackID = IsNull(oRs(1).Value, 0)
                        nDynamic = IsNull(oRs(2).Value, 0)
                        nBursting = IsNull(oRs(3).Value, 0)
                    End If

                    oRs.Close()
                End If

                If sSelType = "packed report" And e.NewText.Contains(":") = False Then
                    SQL = "SELECT MAX(PackOrderID) FROM ReportAttr WHERE PackID =" & nPackID

                    oRs = clsMarsData.GetData(SQL)

                    If oRs Is Nothing Then
                        nCount = 1
                    Else
                        If oRs.EOF = False Then
                            Dim sTemp As String = oRs.Fields(0).Value

                            nCount = sTemp

                            nCount += 1
                        Else
                            nCount = 1
                        End If
                    End If

                    snewName = nCount & ":" & snewName

                    oRs.Close()
                End If

                SQL = "UPDATE ReportAttr SET ReportTitle = '" & SQLPrepare(snewName) & "' WHERE " & _
                                    "ReportID = " & nID

                If nPackID > 0 Then
                    clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.PACKED, clsMarsAudit.AuditAction.RENAME)
                ElseIf nDynamic = 1 Then
                    clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.DYNAMIC, clsMarsAudit.AuditAction.RENAME)
                ElseIf nBursting = 1 Then
                    clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.BURST, clsMarsAudit.AuditAction.RENAME)
                Else
                    clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.SINGLES, clsMarsAudit.AuditAction.RENAME)
                End If

            Case "Data-Driven Packages", "Single Packages", "Dynamic Packages"
                If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = False Then
                    e.Cancel = True
                    Return
                End If

                'check to see if another with the same name exists in this folder
                If clsMarsUI.candoRename(e.NewText, nParent, clsMarsScheduler.enScheduleType.PACKAGE, nID) = False Then
                    MessageBox.Show(clsMarsUI.renameErrorstring, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    e.Cancel = True
                    Return
                End If

                SQL = "UPDATE PackageAttr SET PackageName = '" & SQLPrepare(e.NewText) & "' WHERE " & _
                    "PackID = " & nID

                oRs = clsMarsData.GetData("SELECT PackageName FROM PackageAttr WHERE PackID =" & nID)

                If Not oRs Is Nothing Then
                    If oRs.EOF = False Then
                        sName = oRs(0).Value
                    End If

                    oRs.Close()
                End If

                clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.PACKAGE, clsMarsAudit.AuditAction.RENAME)
            Case "Folders"
                If clsMarsSecurity._HasGroupAccess("Folder Management") = False Then
                    e.Cancel = True
                    Return
                ElseIf isAllowedAccessToFolder(nID) = False Then
                    e.Cancel = True
                    Return
                End If

                If clsMarsUI.candoRename(e.NewText, nParent, clsMarsScheduler.enScheduleType.FOLDER, nID) = False Then
                    MessageBox.Show(clsMarsUI.renameErrorstring, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    e.Cancel = True
                    Return
                End If

                SQL = "UPDATE Folders SET FolderName = '" & SQLPrepare(e.NewText) & "' WHERE " & _
                "FolderID = " & nID

                oRs = clsMarsData.GetData("SELECT FolderName FROM FolderAttr WHERE FolderID =" & nID)

                If Not oRs Is Nothing Then
                    If oRs.EOF = False Then
                        sName = oRs(0).Value
                    End If

                    oRs.Close()
                End If

                clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.FOLDER, clsMarsAudit.AuditAction.RENAME)
            Case "Automation Schedules"
                If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = False Then
                    e.Cancel = True
                    Return
                End If

                'check to see if another with the same name exists in this folder
                If clsMarsUI.candoRename(e.NewText, nParent, clsMarsScheduler.enScheduleType.AUTOMATION, nID) = False Then
                    MessageBox.Show(clsMarsUI.renameErrorstring, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    e.Cancel = True
                    Return
                End If

                SQL = "UPDATE AutomationAttr SET AutoName = '" & SQLPrepare(e.NewText) & "' WHERE " & _
                "AutoID = " & nID

                oRs = clsMarsData.GetData("SELECT AutoName FROM AutomationAttr WHERE AutoID =" & nID)

                If Not oRs Is Nothing Then
                    If oRs.EOF = False Then
                        sName = oRs(0).Value
                    End If

                    oRs.Close()
                End If

                clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.AUTOMATION, clsMarsAudit.AuditAction.RENAME)
            Case "Event-Based Schedules"
                If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = False Then
                    e.Cancel = True
                    Return
                End If

                'check to see if another with the same name exists in this folder
                If clsMarsUI.candoRename(e.NewText, nParent, clsMarsScheduler.enScheduleType.EVENTBASED, nID) = False Then
                    MessageBox.Show(clsMarsUI.renameErrorstring, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    e.Cancel = True
                    Return
                End If

                oRs = clsMarsData.GetData("SELECT EventName FROM EventAttr6 WHERE EventID = " & nID)

                If Not oRs Is Nothing Then
                    If oRs.EOF = False Then
                        sEvent = oRs(0).Value
                    End If

                    oRs.Close()
                End If

                SQL = "UPDATE EventAttr6 SET EventName ='" & SQLPrepare(e.NewText) & "' WHERE EventID = " & nID


                clsMarsAudit._LogAudit(sEvent, clsMarsAudit.ScheduleType.EVENTS, clsMarsAudit.AuditAction.RENAME)
            Case "Event-Based Packages"
                If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = False Then
                    e.Cancel = True
                    Return
                End If

                'check to see if another with the same name exists in this folder
                If clsMarsUI.candoRename(e.NewText, nParent, clsMarsScheduler.enScheduleType.EVENTPACKAGE, nID) = False Then
                    MessageBox.Show(clsMarsUI.renameErrorstring, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    e.Cancel = True
                    Return
                End If

                oRs = clsMarsData.GetData("SELECT PackageName FROM EventPackageAttr WHERE EventPackID = " & nID)

                If Not oRs Is Nothing Then
                    If oRs.EOF = False Then
                        sEvent = oRs(0).Value
                    End If

                    oRs.Close()
                End If

                SQL = "UPDATE EventPackageAttr SET PackageName ='" & SQLPrepare(e.NewText) & "' WHERE EventPackID = " & nID

                clsMarsAudit._LogAudit(sEvent, clsMarsAudit.ScheduleType.EVENTPACKAGE, clsMarsAudit.AuditAction.RENAME)
        End Select

        clsMarsData.WriteData(SQL)

        loadData(sCurrentNode)

        'If sSelType = "Folders" Then

        '    Dim n As AdvTree.Node = tvExplorer.FindNodeByDataKey(sCurrentNode)

        '    If n IsNot Nothing Then
        '        n.EnsureVisible()
        '    End If
        'End If
    End Sub

    Private Sub btnSingleEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSingleEnabled.Click
        If tvMain.SelectedNode Is Nothing Then Return
        Dim n As AdvTree.Node = tvMain.SelectedNode

        Dim oData As clsMarsData = New clsMarsData
        Dim SQL As String
        Dim nValue As Integer
        Dim nReportID As Integer = n.DataKey
        Dim NewImage As Integer
        Dim selectedFolder As AdvTree.Node = tvExplorer.SelectedNode

        Dim rpt As cssreport = New cssreport(nReportID)

        If IO.File.Exists(IO.Path.Combine(sAppPath, "regwizx.exe")) = False Then
            If clsMarsScheduler.isScheduleLocked(nReportID, clsMarsScheduler.enScheduleType.REPORT) = True And btnSingleEnabled.Checked = False Then
                showViolatorMessage()
                Return
            End If
        End If

        SQL = "UPDATE ScheduleAttr SET " & _
            "Status = " & Convert.ToInt32(Not rpt.m_schedule.status) & "," & _
            "DisabledDate = '" & ConDateTime(Date.Now) & "'" & _
            " WHERE ReportID =" & nReportID

        clsMarsData.WriteData(SQL)

        Dim oAction As clsMarsAudit.AuditAction

        If nValue = 0 Then
            oAction = clsMarsAudit.AuditAction.DISABLE
        Else
            oAction = clsMarsAudit.AuditAction.ENABLE
        End If

        If clsMarsData.IsScheduleDynamic(nReportID, "Report") = True Then
            clsMarsAudit._LogAudit(n.Text, clsMarsAudit.ScheduleType.DYNAMIC, oAction)
        ElseIf oData.IsScheduleBursting(nReportID) = True Then
            clsMarsAudit._LogAudit(n.Text, clsMarsAudit.ScheduleType.BURST, oAction)
        Else
            clsMarsAudit._LogAudit(n.Text, clsMarsAudit.ScheduleType.SINGLES, oAction)
        End If

        rpt = Nothing

        rpt = New cssreport(nReportID)

        n.Image = resizeImage(rpt.reportImage, iconSize)

        If rpt.status = False Then
            n.Image = MakeGrayscale3(n.Image)
        End If

        ' loadData(selectedFolder.DataKey)

        '  _Delay(0.5)

        'n.Image = resizeImage(rpt.reportImage, iconSize)

        'If selectedFolder IsNot Nothing Then
        '    tvExplorer.SelectedNode = selectedFolder
        '    selectedFolder.EnsureVisible()
        'End If
    End Sub


    Private Sub btnSingleContext_PopupOpen(ByVal sender As Object, ByVal e As DevComponents.DotNetBar.PopupOpenEventArgs) Handles btnSingleContext.PopupOpen, btnPackageContext.PopupOpen, _
        btnAutomationSchedules.PopupOpen, btneventBasedSchedules.PopupOpen, btnEventBasedPacks.PopupOpen
        If tvMain.SelectedNode Is Nothing Then Return

        If tvMain.SelectedNode.Parent Is Nothing Then Return

        Dim n As AdvTree.Node = tvMain.SelectedNode
        Dim nID As Integer = n.DataKey


        Select Case n.Parent.Text
            Case "Data-Driven Schedules", "Dynamic Schedules", "Bursting Schedules", "Single Schedules"
                Dim rpt As New cssreport(nID)

                btnSingleEnabled.Checked = rpt.m_schedule.status
            Case "Data-Driven Packages", "Single Packages", "Dynamic Packages"
                Dim pack As New Package(nID)
                btnpackEnabled.Checked = pack.m_schedule.status
            Case "Automation Schedules"
                Dim auto As New Automation(nID)

                btnautoEnabled.Checked = auto.m_schedule.status
            Case "Event-Based Schedules"
                Dim evt As EventBased = New EventBased(nID)
                btneventEnabled.Checked = evt.Status
            Case "Event-Based Packages"
                Dim evp As EventBasedPackage = New EventBasedPackage(nID)
                btneventpackEnabled.Checked = evp.m_schedule.status
        End Select
    End Sub

    Private Sub btnSingleRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSingleRefresh.Click, btnPackedRefresh.Click
        If tvMain.SelectedNode Is Nothing Then Return

        Dim nReportID As Integer = tvMain.SelectedNode.DataKey

        Dim oReport As New clsMarsReport

        If oReport.RefreshReport(nReportID) = True Then
            MessageBox.Show("Report refreshed successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, "Please reselect the source reports and then try refreshing the schedule again.")
        End If
    End Sub

    Private Sub DeleteDesktopItem(ByVal sender As System.Object, ByVal e As System.EventArgs, ByRef shouldReturn As Boolean)
        shouldReturn = False
        Dim i As Integer = 1
        Dim count As Integer = 1
        Dim sCurrentNode As AdvTree.Node
        Dim multipleSelected As Boolean = False
        Dim oMultiRes As DialogResult

        If tvMain.SelectedNodes.Count = 0 Then Return

        'If tvMain.SelectedNode.Parent Is Nothing Then Return

        If tvMain.SelectedNodes.Count > 1 Then
            multipleSelected = True
        Else
            multipleSelected = False
        End If

        If tvExplorer.SelectedNode Is Nothing Then
            sCurrentNode = Nothing
        Else
            sCurrentNode = tvExplorer.SelectedNode
        End If

        count = tvMain.SelectedNodes.Count

        If multipleSelected = True Then
            oMultiRes = MessageBox.Show("Are you sure you would like to delete these " & count & " items?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        End If

        tvMain.BeginUpdate()

        Dim theDeleted As ArrayList = New ArrayList

        For Each oI As AdvTree.Node In tvMain.SelectedNodes
            Dim type As String
            Dim nID As Integer

            oUI.BusyProgress(((i / count) * 100), "Deleting items...", False)

            If oI.Parent Is Nothing Then Continue For

            type = oI.Parent.Text
            nID = oI.DataKey

            Select Case type
                Case "Data-Driven Schedules", "Dynamic Schedules", "Bursting Schedules", "Single Schedules"
                    If multipleSelected = True Then
                        If oMultiRes = DialogResult.Yes Then
                            oUI.DeleteReport(nID, 0)
                            theDeleted.Add(oI)
                        End If
                    Else
                        If MessageBox.Show("Delete the '" & oI.Text & "' schedule?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                            oUI.DeleteReport(nID, 0)
                            theDeleted.Add(oI)
                        End If
                    End If
                Case "Data-Driven Packages", "Single Packages", "Dynamic Packages"
                    If multipleSelected = True Then
                        If oMultiRes = DialogResult.Yes Then
                            oUI.DeletePackage(nID)
                            theDeleted.Add(oI)
                        End If
                    Else
                        If MessageBox.Show("Delete the '" & oI.Text & "' package?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                            oUI.DeletePackage(nID)
                            theDeleted.Add(oI)
                        End If
                    End If
                Case "Folders"
                    oUI.DeleteFolder(nID)
                    oI.Remove()
                Case "Automation Schedules"
                    If multipleSelected = True Then
                        If oMultiRes = Windows.Forms.DialogResult.Yes Then
                            oUI.DeleteAutoSchedule(nID)
                            theDeleted.Add(oI)
                        End If
                    Else
                        If MessageBox.Show("Delete the '" & oI.Text & "' automation schedule?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                            oUI.DeleteAutoSchedule(nID)
                            theDeleted.Add(oI)
                        End If
                    End If
                    shouldReturn = True
                Case "Event-Based Schedules"

                    If multipleSelected = True Then
                        If oMultiRes = Windows.Forms.DialogResult.Yes Then
                            clsMarsEvent.DeleteEvent(nID)
                            theDeleted.Add(oI)
                        End If
                    Else
                        If MessageBox.Show("Delete the '" & oI.Text & "' event-based schedule?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                            clsMarsEvent.DeleteEvent(nID)
                            theDeleted.Add(oI)
                        End If
                    End If
                    shouldReturn = True
                Case "packed report"
                    Dim Sql As String = "SELECT PackID FROM ReportAttr WHERE ReportID =" & nID

                    Dim oRs As ADODB.Recordset = clsMarsData.GetData(Sql)

                    If Not oRs Is Nothing And oRs.EOF = False Then
                        oUI.DeleteReport(nID, oRs("packid").Value)
                    End If

                    oRs.Close()
                    theDeleted.Add(oI)
                    shouldReturn = True
                Case "Event-Based Packages"
                    If multipleSelected = True Then
                        If oMultiRes = Windows.Forms.DialogResult.Yes Then
                            clsMarsEvent.DeleteEventPackage(nID)
                            theDeleted.Add(oI)
                        End If
                    Else
                        If MessageBox.Show("Delete the '" & oI.Text & "' event-based package?", Application.ProductName, MessageBoxButtons.YesNo, _
                         MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                            clsMarsEvent.DeleteEventPackage(nID)
                            theDeleted.Add(oI)
                        End If
                    End If
            End Select

            i += 1

        Next

        For Each nn As AdvTree.Node In theDeleted
            nn.Remove()
        Next

        tvMain.EndUpdate()

        tvExplorer.SelectedNode = sCurrentNode

        If tvExplorer.SelectedNode IsNot Nothing Then
            Dim ff As folder = New folder(tvExplorer.SelectedNode.DataKey)
            tvExplorer.SelectedNode.Text = String.Format("{0} ({1})", ff.folderName, ff.getTotalContents)
            'loadData(sCurrentNode.DataKey)
        Else
            'loadData()
        End If

        sCurrentNode.EnsureVisible()

        oUI.BusyProgress(, , True)
    End Sub

    Private Sub btnSingleDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSingleDelete.Click, btnpackDelete.Click, btnautoDelete.Click, btneventDelete.Click, _
        btneventpackDelete.Click

        DeleteDesktopItem(sender, e, False)
    End Sub

    Private Sub btnSingleTest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSingleTest.Click, btnpackTest.Click
        If tvMain.SelectedNodes.Count = 0 Then Return

        Dim oTest As New frmTestDestination
        Dim nn As AdvTree.Node = tvMain.SelectedNode

        Select Case nn.Parent.Text
            Case "Data-Driven Schedules", "Dynamic Schedules", "Bursting Schedules", "Single Schedules"

                If clsMarsScheduler.isScheduleLocked(nn.DataKey, clsMarsScheduler.enScheduleType.REPORT) = True Then
                    showViolatorMessage()
                    Return
                End If

                oTest._SelectDestination(nn.DataKey)
            Case "Data-Driven Packages", "Single Packages", "Dynamic Packages"
                If clsMarsScheduler.isScheduleLocked(nn.DataKey, clsMarsScheduler.enScheduleType.PACKAGE) = True Then
                    showViolatorMessage()
                    Return
                End If

                oTest._SelectDestination(, nn.DataKey)
            Case Else
                Return
        End Select

    End Sub

    Private Sub btnConvertToPackage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConvertToPackage.Click

        If tvMain.SelectedNodes.Count = 0 Then Return

        Dim nReportID As Integer
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As New clsMarsData
        Dim nPackID As Int64 = clsMarsData.CreateDataID("packageattr", "packid")
        Dim sName As String
        Dim oUI As New clsMarsUI
        Dim nn As AdvTree.Node = tvMain.SelectedNode

        nReportID = nn.DataKey
        sName = nn.Text

        Dim sWhere As String = " WHERE ReportID =" & nReportID

        'check for embed
        SQL = "SELECT d.Embed FROM DestinationAttr d INNER JOIN ReportAttr r ON " & _
        "d.ReportID = r.ReportID WHERE r.ReportID = " & nReportID & " AND DestinationType = 'Email'"

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                If oRs(0).Value = 1 Then
                    MessageBox.Show("You cannot convert this schedule to a package as the report is embeded in an email. " & _
                    "Please remove this property and then try again", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    oRs.Close()
                    Return
                End If
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        Dim IsDynamic, IsDataDriven As Boolean
        Dim nParent As Integer

        nParent = tvExplorer.SelectedNode.DataKey

        IsDynamic = clsMarsData.IsScheduleDynamic(nReportID, "report")
        IsDataDriven = clsMarsData.IsScheduleDataDriven(nReportID, "report")

        Do While clsMarsUI.candoRename(sName, nParent, clsMarsScheduler.enScheduleType.PACKAGE, nReportID, IsDynamic, IsDataDriven) = False
            sName = InputBox("A package with the provided name already " & _
            "exists. Please provide a name for the package.", _
            Application.ProductName)

            If sName.Length = 0 Then Return
        Loop


        oUI.BusyProgress(10, "Creating Package...")


        'insert into packageattr
        sCols = "PackID,PackageName,Parent,Retry,AssumeFail,CheckBlank,Owner,FailOnOne,Dynamic,IsDataDriven,StaticDest,autocalc,retryinterval"

        sVals = nPackID & "," & _
        "'" & SQLPrepare(sName) & "'," & _
        "Parent,Retry,AssumeFail,CheckBlank,'" & gUser & "',0," & _
        Convert.ToInt32(IsDynamic) & "," & _
        Convert.ToInt32(IsDataDriven) & ",StaticDest,autocalc,retryinterval"


        SQL = "INSERT INTO PackageAttr (" & sCols & ") " & _
        "SELECT " & sVals & " FROM ReportAttr " & sWhere

        If clsMarsData.WriteData(SQL) = False Then GoTo RollbackTran

        oUI.BusyProgress(30, "Updating report attributes")

        'update reportattr with packid
        SQL = "UPDATE ReportAttr SET " & _
        "ReportTitle = '1:" & SQLPrepare(sName) & "', PackOrderID =1, PackID =" & nPackID & sWhere

        If clsMarsData.WriteData(SQL) = False Then GoTo RollbackTran

        'update scheduleattr with packid and reportid to 0
        oUI.BusyProgress(50, "Updating schedule attributes")

        SQL = "UPDATE ScheduleAttr SET ReportID =0, PackID = " & nPackID & sWhere

        If clsMarsData.WriteData(SQL) = False Then GoTo RollbackTran

        'insert into packagedreportattr from reportattr
        oUI.BusyProgress(70, "Creating package attributes")

        sCols = "AttrID,PackID,ReportID,OutputFormat,CustomExt,AppendDateTime," & _
        "DateTimeFormat,CustomName,Status"

        sVals = clsMarsData.CreateDataID("packagedreportattr", "attrid") & "," & _
        nPackID & "," & _
        nReportID & "," & _
        "OutputFormat,CustomExt,AppendDateTime,DateTimeFormat,CustomName,1"

        SQL = "INSERT INTO PackagedReportAttr (" & sCols & ") " & _
        "SELECT TOP 1 " & sVals & " FROM DestinationAttr " & sWhere

        clsMarsData.WriteData(SQL, False)

        'update destinationattr with packid and reportid to 0
        oUI.BusyProgress(80, "Updating destination attributes")

        SQL = "UPDATE DestinationAttr SET OutputFormat = 'Package', " & _
        "ReportID =0, PackID =" & nPackID & sWhere

        If clsMarsData.WriteData(SQL) = False Then GoTo RollbackTran

        'TODOFORMARS
        'update data driven stuff
        If IsDataDriven = True Then
            sCols = "ddid,reportid,constring,sqlquery,failonone,groupreports,keycolumn,packid"

            sVals = clsMarsData.CreateDataID & ",0,constring,sqlquery,failonone,groupreports,keycolumn," & nPackID

            SQL = "INSERT INTO datadrivenattr (" & sCols & ") SELECT " & sVals & " FROM datadrivenattr WHERE ReportID =" & nReportID

            clsMarsData.WriteData(SQL)
        End If

        If IsDynamic = True Then
            sCols = "dynamicid,keyparameter,keycolumn,constring,query,reportid,packid,autoresume,expirecacheafter"

            sVals = clsMarsData.CreateDataID & ",keyparameter,keycolumn,constring,query,0," & nPackID & ",autoresume,expirecacheafter"

            SQL = "INSERT INTO dynamicattr (" & sCols & ") SELECT " & sVals & " FROM dynamicattr WHERE ReportID =" & nReportID

            clsMarsData.WriteData(SQL)

            sCols = "linkid,reportid,linksql,constring,keycolumn,keyparameter,valuecolumn,table1,table2,joincolumn1,joincolumn2,wherelist,packid"

            sVals = clsMarsData.CreateDataID & ",0,linksql,constring,keycolumn,keyparameter,valuecolumn,table1,table2,joincolumn1,joincolumn2,wherelist," & nPackID

            SQL = "INSERT INTO dynamiclink (" & sCols & ") SELECT " & sVals & " FROM dynamiclink WHERE ReportID =" & nReportID

            clsMarsData.WriteData(SQL)
        End If

        oUI.BusyProgress(95, "Cleaning up")

        loadData(tvExplorer.SelectedNode.DataKey)

        oUI.BusyProgress(, , True)

        Return
RollbackTran:
        clsMarsData.WriteData("DELETE FROM PackageAttr WHERE PackID =" & nPackID)
        clsMarsData.WriteData("UPDATE ReportAttr SET PackID =0 WHERE ReportID =" & nReportID)
        clsMarsData.WriteData("UPDATE ScheduleAttr SET PackID =0, ReportID =" & nReportID & _
        " WHERE PackID = " & nPackID)
        clsMarsData.WriteData("DELETE FROM PackagedReportAttr WHERE PackID = " & nPackID)
        clsMarsData.WriteData("UPDATE DestinationAttr SET PackID =0, ReportID = " & nReportID & _
        " WHERE PackID = " & nPackID)
        oUI.BusyProgress(, , True)
    End Sub

    Private Sub btnAdHocEmail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdHocEmail.Click
        Dim frmAdHoc As New frmAdhocSend
        Dim nID As Integer

        nID = tvMain.SelectedNode.DataKey

        frmAdHoc.AdHocSend(nID)
    End Sub

    Private Sub btnAdHocView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdHocView.Click
        Try
            Dim nID As Integer

            clsMarsUI.MainUI.BusyProgress(20, "Collecting data...")

            nID = tvMain.SelectedNode.DataKey

            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT CachePath FROM ReportAttr WHERE ReportID =" & nID)

            If oRs IsNot Nothing Then
                If oRs.EOF = False Then
                    Dim path As String = IsNull(oRs(0).Value, "")

                    clsMarsUI.MainUI.BusyProgress(60, "Opening report...")
#If CRYSTAL_VER < 11.6 Then
                    '   Dim oRpt As CRAXDRT.Report = oApp.OpenReport(path)
#Else
                    Dim oRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = New CrystalDecisions.CrystalReports.Engine.ReportClass
                    oRpt.FileName = path
                    oRpt.Load(path, CrystalDecisions.Shared.OpenReportMethod.OpenReportByDefault)

                    While oRpt.IsLoaded = False
                        _Delay(1)
                    End While
#End If
                    Dim oPrev As frmPreview = New frmPreview

                    clsMarsUI.MainUI.BusyProgress(90, "Rendering report...")

                    '   oPrev.PreviewReport(oRpt, False)

                    clsMarsUI.MainUI.BusyProgress(100, , True)
                End If
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            clsMarsUI.MainUI.BusyProgress(100, , True)
        End Try
    End Sub

    Private Sub btnpackAddReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnpackAddReport.Click
        Dim oForm As frmPackedReport
        Dim sReturn() As String
        Dim rs As ADODB.Recordset
        Dim SQL As String
        Dim nCount As Integer
        Dim oData As clsMarsData = New clsMarsData
        Dim nPackId As Integer
        Dim ofd As New OpenFileDialog

        nPackId = tvMain.SelectedNode.DataKey

        SQL = "SELECT MAX(PackOrderID) FROM ReportAttr WHERE PackID = " & nPackId

        rs = clsMarsData.GetData(SQL)

        If rs Is Nothing Then
            nCount = 1
        Else
            Try
                Dim sTemp As String = rs.Fields(0).Value

                nCount = sTemp

                nCount += 1
            Catch
                nCount = 1
            End Try
        End If

        rs.Close()

        oForm = New frmPackedReport

        sReturn = oForm.AddReport(nCount, nPackId)

        If sReturn IsNot Nothing AndAlso sReturn(0) IsNot Nothing Then

            MessageBox.Show("Report(s) added successfully!", Application.ProductName, _
            MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub


    Private Sub btnpackRefreshPack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnpackRefreshPack.Click
        If tvMain.SelectedNodes.Count = 0 Then Return

        Dim nPackID As Integer
        Dim nReportID As Integer
        Dim oData As New clsMarsData
        Dim oReport As New clsMarsReport
        Dim Ok As Boolean
        Dim I As Integer = 1

        nPackID = tvMain.SelectedNode.DataKey

        Ok = oReport.RefreshPackage(nPackID)

        If Ok = True Then
            MessageBox.Show("All reports in the package have been refreshed successfully", Application.ProductName, _
            MessageBoxButtons.OK, MessageBoxIcon.Information)

            oUI.BusyProgress(, , True)
        Else
            _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine)
        End If
    End Sub

    Private Sub btnpackSplit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnpackSplit.Click
        'get all reports and set packid to 0
        Dim SQL As String
        Dim oData As New clsMarsData
        Dim oUI As New clsMarsUI
        Dim oRs, oRs2 As ADODB.Recordset
        Dim sName As String
        Dim nReportID As Integer
        Dim sWhere As String
        Dim sCols, sVals As String
        Dim nParent As Integer
        Dim I As Int32 = 1

        If tvMain.SelectedNodes.Count = 0 Then Return

        Dim nPackID As Integer

        nPackID = tvMain.SelectedNode.DataKey

        sWhere = " WHERE PackID = " & nPackID

        SQL = "SELECT * FROM ReportAttr " & sWhere

        oRs = clsMarsData.GetData(SQL, ADODB.CursorTypeEnum.adOpenStatic)

        Try
            oUI.BusyProgress(10, "Retrieving packaged reports...")

            If oRs.EOF = True Then
                MessageBox.Show("You cannot split a package that does not contain any reports", _
                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                oRs.Close()
                Return
            End If

            Do While oRs.EOF = False
                nReportID = oRs("reportid").Value
                sName = oRs("reporttitle").Value
                sName = sName.Split(":")(1)
                nParent = tvExplorer.SelectedNode.DataKey

                Do While clsMarsUI.candoRename(sName, nParent, clsMarsScheduler.enScheduleType.REPORT) = False
                    sName = InputBox("Please provide a new name for the single schedule", _
                    Application.ProductName, sName & "1")

                    If sName.Length = 0 Then Return
                Loop

                oUI.BusyProgress(30, "Converting report " & I & " of " & oRs.RecordCount & " to single schedule")

                SQL = "UPDATE ReportAttr SET " & _
                "PackID = 0, " & _
                "ReportTitle = '" & SQLPrepare(sName) & "'," & _
                "Parent = " & nParent & " WHERE " & _
                "ReportID = " & nReportID

                clsMarsData.WriteData(SQL)

                'copy the destination for the report
                Dim OutputFormat, CustomExt, DateTimeFormat, CustomName As String
                Dim AppendDateTime As Integer


                oUI.BusyProgress(50, "Updating report attributes")

                oRs2 = clsMarsData.GetData("SELECT * FROM PackagedReportAttr " & _
                "WHERE ReportID = " & nReportID)

                If oRs2.EOF = False Then
                    OutputFormat = oRs2("outputformat").Value
                    CustomExt = oRs2("customext").Value
                    DateTimeFormat = oRs2("datetimeformat").Value
                    CustomName = oRs2("customname").Value
                    AppendDateTime = oRs2("appenddatetime").Value
                End If

                oRs2.Close()

                oUI.BusyProgress(70, "Copying package destinations")

                oRs2 = clsMarsData.GetData("SELECT * FROM DestinationAttr" & sWhere)

                Do While oRs2.EOF = False
                    Dim newID As Integer = clsMarsData.CreateDataID("destinationattr", "destinationid")

                    sCols = ""

                    For Each field As ADODB.Field In oRs2.Fields
                        Select Case field.Name.ToLower
                            Case "collate"
                                sCols &= "[Collate],"
                            Case "type"
                                sCols &= "[Type],"
                            Case "to"
                                sCols &= "[To],"
                            Case "subject"
                                sCols &= "[Subject],"
                            Case Else
                                sCols &= field.Name & ","
                        End Select
                    Next

                    sCols = sCols.Substring(0, sCols.Length - 1).ToLower

                    sVals = sCols

                    sVals = sVals.Replace("outputformat", "'" & OutputFormat & "'")
                    sVals = sVals.Replace("customext", "'" & CustomExt & "'")
                    sVals = sVals.Replace("datetimeformat", "'" & DateTimeFormat & "'")
                    sVals = sVals.Replace("customname", "'" & CustomName & "'")
                    sVals = sVals.Replace("appenddatetime", AppendDateTime)
                    sVals = sVals.Replace("packid", 0)
                    sVals = sVals.Replace("destinationid", newID)
                    sVals = sVals.Replace("reportid", nReportID)

                    SQL = "INSERT INTO DestinationAttr (" & sCols & ") SELECT " & sVals & " FROM DestinationAttr WHERE DestinationID =" & oRs2(0).Value

                    If clsMarsData.WriteData(SQL) = False Then

                    End If

                    oRs2.MoveNext()
                Loop

                oRs2.Close()

                'copy the packages schedule details

                oUI.BusyProgress(80, "Copying schedule attributes")

                sCols = "ScheduleID,Frequency,StartDate,EndDate,NextRun," & _
                "StartTime,Repeat,RepeatInterval,RepeatUntil,Status,ReportID," & _
                "PackID,AutoID,Description,Keyword,CalendarName,SmartID,UseException,ExceptionCalendar"

                Dim nScheduleID As Integer = clsMarsData.CreateDataID("scheduleattr", "scheduleid")

                sVals = nScheduleID & "," & _
                "Frequency,StartDate,EndDate,NextRun," & _
                "StartTime,Repeat,RepeatInterval,RepeatUntil,Status," & _
                nReportID & "," & _
                "0,AutoID,Description,Keyword,CalendarName,SmartID,UseException,ExceptionCalendar"


                SQL = "INSERT INTO ScheduleAttr (" & sCols & ") " & _
                "SELECT " & sVals & " FROM ScheduleAttr " & sWhere

                clsMarsData.WriteData(SQL)

                sCols = "OptionID,WeekNo,DayNo,MonthsIn,nCount,ScheduleID,ScheduleTYpe,UseOptions"

                sVals = clsMarsData.CreateDataID("scheduleoptions", "optionid") & "," & _
                "WeekNo,DayNo,MonthsIn,nCount," & _
                nScheduleID & ",ScheduleTYpe,UseOptions"

                SQL = "INSERT INTO ScheduleOptions (" & sCols & ") " & _
                "SELECT " & sVals & " FROM ScheduleOptions WHERE ScheduleID =" & _
                "(SELECT ScheduleID FROM ScheduleAttr" & sWhere & ")"

                clsMarsData.WriteData(SQL)

                'copy the tasks

                oUI.BusyProgress(90, "Copying custom tasks")

                oRs2 = clsMarsData.GetData("SELECT TaskID FROM Tasks WHERE " & _
                "ScheduleID =(SELECT ScheduleID FROM ScheduleAttr" & sWhere & ")")

                Do While oRs2.EOF = False
                    sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramPath," & _
                    "ProgramParameters,WindowStyle,PrinterName,FileList," & _
                    "PauseInt,ReplaceFiles,Msg,SendTo,CC,Bcc,Subject,OrderID," & _
                    "FTPServer,FTPPort,FTPUser,FTPPassword,FTPDirectory," & _
                    "RunWhen,AfterType"

                    sVals = clsMarsData.CreateDataID("tasks", "taskid") & "," & _
                    nScheduleID & "," & _
                    "TaskType,TaskName,ProgramPath," & _
                    "ProgramParameters,WindowStyle,PrinterName,FileList," & _
                    "PauseInt,ReplaceFiles,Msg,SendTo,CC,Bcc,Subject,OrderID," & _
                    "FTPServer,FTPPort,FTPUser,FTPPassword,FTPDirectory," & _
                    "RunWhen,AfterType"

                    SQL = "INSERT INTO Tasks (" & sCols & ") " & _
                    "SELECT " & sVals & " FROM Tasks WHERE " & _
                    "TaskID =" & oRs2.Fields(0).Value

                    clsMarsData.WriteData(SQL)

                    oRs2.MoveNext()
                Loop

                oRs2.Close()

                oRs.MoveNext()
            Loop

            oRs.Close()

            oUI.BusyProgress(95, "Deleting package")

            oUI.DeletePackage(nPackID)

            oUI.BusyProgress(100, "Cleaning up")

            loadData(tvExplorer.SelectedNode.DataKey)

            oUI.BusyProgress(, , True)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, "frmWindow.mnuSplitPackage_Click", _
            _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub btnpackAdhoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnpackAdhoc.Click
        If tvMain.SelectedNode Is Nothing Then Return
        Dim packid As Integer = tvMain.SelectedNode.DataKey

        Dim adhocer As frmAdhocSend = New frmAdhocSend

        adhocer.AdHocSend(0, packid)
    End Sub

    Private Sub btnCreateSingleShortcut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreateSingleShortcut.Click, btnpackShortcut.Click, _
        btnautoShortcut.Click, btneventShortcut.Click, btneventpackShortcut.Click

        If tvMain.SelectedNode Is Nothing Then Return

        Dim nn As AdvTree.Node = tvMain.SelectedNode
        Dim type As clsMarsUI.enScheduleType
        Select Case nn.Parent.Text
            Case "Data-Driven Schedules", "Dynamic Schedules", "Bursting Schedules", "Single Schedules"
                type = clsMarsUI.enScheduleType.REPORT
            Case "Data-Driven Packages", "Single Packages", "Dynamic Packages"
                type = clsMarsUI.enScheduleType.PACKAGE
            Case "Automation Schedules"
                type = clsMarsUI.enScheduleType.AUTOMATION
            Case "Event-Based Schedules"
                type = clsMarsUI.enScheduleType.EVENTBASED
            Case "Event-Based Packages"
                type = clsMarsUI.enScheduleType.EVENTPACKAGE
        End Select

        Dim fbd As New FolderBrowserDialog

        fbd.ShowNewFolderButton = True

        fbd.Description = "Please select the folder to place the shortcut"

        If fbd.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
            If clsMarsUI.MainUI.createScheduleShortcut(nn.DataKey, _
            type, fbd.SelectedPath, nn.Text) = True Then
                MessageBox.Show("The shortcut has been created successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub

    Private Sub pastePackage()

        If gClipboard(0) <> "Package" Then Return

        Dim nCopy As Integer
        Dim oReport As clsMarsReport = New clsMarsReport
        Dim nParent As Integer
        Dim sType As String = ""

        If tvExplorer.SelectedNode Is Nothing Then
            sType = ""
        Else
            sType = tvExplorer.SelectedNode.Tag
        End If

        If sType.ToLower <> "folder" Then
            Dim oFolder As New frmFolders
            Dim nTemp() As String

            nTemp = oFolder.GetFolder()

            nParent = nTemp(1)
        Else
            nParent = tvExplorer.SelectedNode.DataKey
        End If

        nCopy = oReport.CopyPackage(gClipboard(1), nParent)

        If nCopy > 0 Then
            Dim oProp As frmPackageProp = New frmPackageProp

            ' oProp.tabProperties.SelectedTabIndex = 2

            oProp.EditPackage(nCopy)
        End If
    End Sub

    Private Sub btnAutomationSchedules_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAutomationSchedules.Click

    End Sub

    Private Sub pasteAuto()
        Dim nCopy As Integer
        Dim oAuto As clsMarsAutoSchedule = New clsMarsAutoSchedule
        Dim nParent As Integer
        Dim sType As String = ""

        If tvExplorer.SelectedNode Is Nothing Then
            sType = ""
        Else
            sType = tvExplorer.SelectedNode.Tag
        End If

        If sType.ToLower <> "folder" Then
            Dim oFolder As New frmFolders
            Dim nTemp() As String

            nTemp = oFolder.GetFolder()

            nParent = nTemp(1)
        Else
            nParent = tvExplorer.SelectedNode.DataKey
        End If

        nCopy = oAuto.CopyAuto(gClipboard(1), nParent)
    End Sub

    Private Sub btnautoEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnautoEnabled.Click
        If tvMain.SelectedNode Is Nothing Then Return
        Dim n As AdvTree.Node = tvMain.SelectedNode

        Dim oData As clsMarsData = New clsMarsData
        Dim SQL As String
        Dim nValue As Integer
        Dim nautoID As Integer = n.DataKey
        Dim NewImage As Integer
        Dim selectedFolder As AdvTree.Node = tvExplorer.SelectedNode

        Dim auto As Automation = New Automation(nautoID)

        If IO.File.Exists(IO.Path.Combine(sAppPath, "regwizx.exe")) = False Then
            If clsMarsScheduler.isScheduleLocked(nautoID, clsMarsScheduler.enScheduleType.AUTOMATION) = True And btnautoEnabled.Checked = False Then
                showViolatorMessage()
                Return
            End If
        End If

        SQL = "UPDATE ScheduleAttr SET " & _
            "Status = " & Convert.ToInt32(Not auto.m_schedule.status) & "," & _
            "DisabledDate = '" & ConDateTime(Date.Now) & "'" & _
            " WHERE autoid =" & nautoID

        clsMarsData.WriteData(SQL)

        Dim oAction As clsMarsAudit.AuditAction

        If nValue = 0 Then
            oAction = clsMarsAudit.AuditAction.DISABLE
        Else
            oAction = clsMarsAudit.AuditAction.ENABLE
        End If

        clsMarsAudit._LogAudit(n.Text, clsMarsAudit.ScheduleType.AUTOMATION, oAction)

        auto = Nothing
        auto = New Automation(nautoID)
        n.Image = resizeImage(auto.automationImage, iconSize)

        If auto.Status = False Then
            n.Image = MakeGrayscale3(n.Image)
        End If
        'loadData(selectedFolder.DataKey)
    End Sub

    Private Sub btnpackEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnpackEnabled.Click
        If tvMain.SelectedNode Is Nothing Then Return
        Dim n As AdvTree.Node = tvMain.SelectedNode

        Dim oData As clsMarsData = New clsMarsData
        Dim SQL As String
        Dim nValue As Integer
        Dim npackID As Integer = n.DataKey
        Dim NewImage As Integer
        Dim selectedFolder As AdvTree.Node = tvExplorer.SelectedNode

        Dim pack As Package = New Package(npackID)

        If IO.File.Exists(IO.Path.Combine(sAppPath, "regwizx.exe")) = False Then
            If clsMarsScheduler.isScheduleLocked(npackID, clsMarsScheduler.enScheduleType.PACKAGE) = True And btnpackEnabled.Checked = False Then
                showViolatorMessage()
                Return
            End If
        End If

        SQL = "UPDATE ScheduleAttr SET " & _
            "Status = " & Convert.ToInt32(Not pack.m_schedule.status) & "," & _
            "DisabledDate = '" & ConDateTime(Date.Now) & "'" & _
            " WHERE packid =" & npackID

        clsMarsData.WriteData(SQL)

        Dim oAction As clsMarsAudit.AuditAction

        If nValue = 0 Then
            oAction = clsMarsAudit.AuditAction.DISABLE
        Else
            oAction = clsMarsAudit.AuditAction.ENABLE
        End If

        If clsMarsData.IsScheduleDynamic(npackID, "Package") = True Then
            clsMarsAudit._LogAudit(n.Text, clsMarsAudit.ScheduleType.DYNAMIC, oAction)
        Else
            clsMarsAudit._LogAudit(n.Text, clsMarsAudit.ScheduleType.PACKAGE, oAction)
        End If

        ' loadData(selectedFolder.DataKey)
        pack = Nothing
        pack = New Package(npackID)
        n.Image = resizeImage(pack.packageImage, iconSize)

        If pack.m_status = False Then
            n.Image = MakeGrayscale3(n.Image)
        End If

        If tvExplorer.SelectedNode IsNot Nothing Then
            For Each nn As AdvTree.Node In tvExplorer.SelectedNode.Nodes
                If nn.DataKey = npackID Then
                    nn.Image = resizeImage(pack.packageImage, 16, 16)
                    Exit For
                End If
            Next
        End If
    End Sub

    Private Sub pasteEvent()
        Dim sType As String = ""
        Dim nParent As Integer

        If tvExplorer.SelectedNode Is Nothing Then
            sType = ""
        Else
            sType = tvExplorer.SelectedNode.Tag
        End If

        If sType.ToLower <> "folder" Then
            Dim oFolder As New frmFolders
            Dim nTemp() As String

            nTemp = oFolder.GetFolder()

            nParent = nTemp(1)
        Else
            nParent = tvExplorer.SelectedNode.DataKey
        End If

        Dim oEvent As New clsMarsEvent

        oEvent.PasteSchedule(gClipboard(1), _
                             tvExplorer.SelectedNode.DataKey, "folder")

    End Sub

    Private Sub pasteEventPackage()
        If gClipboard(0) <> "Event Package" And gClipboard(0) <> "Event-Package" Then Return

        Dim nCopy As Integer
        Dim nParent As Integer
        Dim sType As String = ""

        Dim oEvent As clsMarsEvent = New clsMarsEvent

        If tvExplorer.SelectedNode Is Nothing Then
            sType = ""
        Else
            sType = tvExplorer.SelectedNode.Tag
        End If

        If sType.ToLower <> "folder" Then
            Dim oFolder As New frmFolders
            Dim nTemp() As String

            nTemp = oFolder.GetFolder()

            nParent = nTemp(1)
        Else
            nParent = tvExplorer.SelectedNode.DataKey
        End If

        nCopy = oEvent.PastePackageSchedule(gClipboard(1), nParent)

        If nCopy > 0 Then
            Dim oProp As frmEventPackageProp = New frmEventPackageProp

            oProp.tabProperties.SelectedTab = oProp.tabGeneral

            oProp.EditPackage(nCopy)
        End If
    End Sub

    Private Sub btneventExecute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btneventExecute.Click
        If tvExplorer.SelectedNodes.Count = 0 Then Return

        crdxCommon.executingForm = Me

        Dim oT As New clsMarsThreading

        Dim nEventID As Integer

        nEventID = tvMain.SelectedNode.DataKey

        oT.xEventID = nEventID

        oT.EventScheduleThread()

        clsMarsAudit._LogAudit(tvMain.SelectedNode.Text, clsMarsAudit.ScheduleType.EVENTS, clsMarsAudit.AuditAction.EXECUTE)

        editItemRow(nEventID, clsMarsScheduler.enScheduleType.EVENTBASED)
    End Sub

    Private Sub btneventpackAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btneventpackAddNew.Click
        If tvMain.SelectedNodes.Count = 0 Then Return

        Dim oEvent As New frmEventWizard6
        Dim eventDtls As Hashtable
        Dim eventName As String = ""
        Dim eventID As Integer = 0

        Dim nPackID As Integer = tvMain.SelectedNode.DataKey
        Dim SQL As String = "SELECT MAX(PackOrderID) FROM EventAttr6 WHERE PackID =" & nPackID
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then Return

        Dim PackOrderID As Integer

        PackOrderID = IsNull(oRs.Fields(0).Value, -1) + 1

        oRs.Close()

        oEvent.txtLocation.Tag = "99999"
        oEvent.txtLocation.Text = "\Package"
        oEvent.txtLocation.Enabled = False
        oEvent.btnBrowse.Enabled = False
        eventDtls = oEvent.AddSchedule(PackOrderID)

        If eventDtls IsNot Nothing Then
            eventID = eventDtls.Item("ID")

            SQL = "UPDATE EventAttr6 SET PackID =" & nPackID & " WHERE EventID =" & eventID

            clsMarsData.WriteData(SQL)

            loadData(tvExplorer.SelectedNode.DataKey)

            MessageBox.Show("Schedule added successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        End If
    End Sub

    Private Sub btneventpackAddExisting_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btneventpackAddExisting.Click
        If tvMain.SelectedNodes.Count = 0 Then Return

        Dim values As Hashtable
        Dim getEvents As frmFolders = New frmFolders

        getEvents.m_eventOnly = True

        values = getEvents.GetEventBasedSchedule

        If values IsNot Nothing Then
            Dim nPackID As Integer = tvMain.SelectedNode.DataKey
            Dim SQL As String = "SELECT MAX(PackOrderID) FROM EventAttr6 WHERE PackID =" & nPackID
            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then Return

            Dim PackOrderID As Integer

            PackOrderID = IsNull(oRs.Fields(0).Value, -1) + 1

            oRs.Close()

            Dim eventID As Integer = values.Item("ID")

            SQL = "UPDATE EventAttr6 SET PackID =" & nPackID & ",PackOrderID =" & PackOrderID & " WHERE EventID =" & eventID

            clsMarsData.WriteData(SQL)

            loadData(tvExplorer.SelectedNode.DataKey)

            MessageBox.Show("Schedule added successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        End If
    End Sub

    Private Sub btneventpackOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btneventpackOpen.Click

    End Sub

    Private Sub btneventpackExecute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btneventpackExecute.Click
        Dim oEvent As clsMarsEvent = New clsMarsEvent
        Dim oSchedule As clsMarsScheduler = New clsMarsScheduler
        Dim Success As Boolean
        Dim nPackID As Integer
        Dim oExecute As clsMarsThreading = New clsMarsThreading

        crdxCommon.executingForm = Me

        nPackID = tvMain.SelectedNode.DataKey

        oExecute.xPackID = nPackID
        AppStatus(True)
        oExecute.EventPackageScheduleThread()
        AppStatus(False)

        clsMarsAudit._LogAudit(tvExplorer.SelectedNode.Text, clsMarsAudit.ScheduleType.EVENTPACKAGE, clsMarsAudit.AuditAction.EXECUTE)

        editItemRow(nPackID, clsMarsScheduler.enScheduleType.EVENTPACKAGE)
    End Sub

    Private Sub btneventpackSplit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btneventpackSplit.Click
        Dim oRes As DialogResult = MessageBox.Show("Delete the package after splitting it?", Application.ProductName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)

        If oRes = Windows.Forms.DialogResult.Cancel Then Return

        If tvMain.SelectedNodes.Count = 0 Then Return

        Dim nPackID As Integer = tvMain.SelectedNode.DataKey

        Dim nParent As Integer = tvExplorer.SelectedNode.DataKey

        Dim SQL As String = "UPDATE EventAttr6 SET PackID = 0, Parent = " & nParent & " WHERE PackID =" & nPackID

        clsMarsData.WriteData(SQL)

        If oRes = Windows.Forms.DialogResult.Yes Then
            Dim oEvent As New clsMarsEvent

            oEvent.DeleteEventPackage(nPackID)
        End If

        MessageBox.Show("The package has been split successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        loadData(tvExplorer.SelectedNode.DataKey)

    End Sub



    Private Sub btnfolderMove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnfolderMove.Click
        If clsMarsSecurity._HasGroupAccess("Folder Management") = False Then
            Return
        End If

        Dim nFolderID As Integer = folderTree.SelectedNode.DataKey

        If isAllowedAccessToFolder(nFolderID) = False Then
            Return
        End If

        oUI.MoveFolder(nFolderID)

        loadData(nFolderID)
    End Sub

    Private Sub btnfolderDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnfolderDelete.Click
        If folderTree.SelectedNodes.Count = 0 Then Return

        If isAllowedAccessToFolder(folderTree.SelectedNode.DataKey) = False Then
            Return
        End If

        Dim oResponse As DialogResult
        Dim multiSelected As Boolean

        If folderTree.SelectedNodes.Count > 1 Then
            multiSelected = True
            oResponse = MessageBox.Show("Delete the selected folders?", Application.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Question)

            If oResponse = Windows.Forms.DialogResult.Cancel Then Return
        End If


        Dim deletedFolders As ArrayList = New ArrayList

        For Each oI As DevComponents.AdvTree.Node In folderTree.SelectedNodes
            deletedFolders.Add(oI)

            If oI.Tag = "folder" Then
                If multiSelected = False Then
                    oResponse = MessageBox.Show("Delete the '" & oI.Text & "' folder? All contents will be lost.", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                End If

                If oResponse = DialogResult.Yes Then
                    If oUI.DeleteFolder(oI.DataKey) = False Then
                        Return
                    End If
                End If
            End If
        Next

        Try
            For Each node As DevComponents.AdvTree.Node In deletedFolders
                Dim nx As DevComponents.AdvTree.Node = tvExplorer.FindNodeByDataKey(node.DataKey)

                If nx IsNot Nothing Then
                    tvExplorer.SelectedNode = nx.Parent
                    nx.Remove()
                End If

                node.Remove()
            Next

            If tvExplorer.SelectedNode IsNot Nothing Then
                buildCrumbBar(tvExplorer.SelectedNode.DataKey, crumb.SelectedItem)

                Dim fld As folder = New folder(tvExplorer.SelectedNode.DataKey)

                tvExplorer.SelectedNode.Text = fld.folderName & " (" & fld.getTotalContents & ")"
            End If
        Catch : End Try

    End Sub

    Private Sub btnfolderRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnfolderRefresh.Click
        If folderTree.SelectedNode Is Nothing Then Return

        If isAllowedAccessToFolder(folderTree.SelectedNode.DataKey) = False Then
            Return
        End If

        Bar1.AutoHide = False

        Dim oFolder As New clsFolders

        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim nID As Integer = folderTree.SelectedNode.DataKey

        oFolder.RefreshFolder(nID)

        If folderTree.SelectedNode IsNot Nothing Then loadData(folderTree.SelectedNode.DataKey)

        Bar1.AutoHide = True

        MessageBox.Show("The requested task has completed", Application.ProductName, MessageBoxButtons.OK, _
        MessageBoxIcon.Information)
    End Sub

    Private Sub btnfolderDisable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnfolderDisable.Click
        If folderTree.SelectedNode Is Nothing Then Return

        If isAllowedAccessToFolder(folderTree.SelectedNode.DataKey) = False Then
            Return
        End If

        Dim oFolder As New clsFolders

        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim nID As Integer = folderTree.SelectedNode.DataKey

        oFolder.DisableFolder(nID)

        If tvExplorer.SelectedNode IsNot Nothing Then
            changePackageColor(False, tvExplorer.SelectedNode)
        Else
            Dim selectedNode As AdvTree.Node = tvExplorer.FindNodeByDataKey(nID)

            If selectedNode IsNot Nothing Then
                changePackageColor(False, selectedNode)
            End If
        End If


        If folderTree.SelectedNode IsNot Nothing Then loadData(folderTree.SelectedNode.DataKey)

        MessageBox.Show("The requested task has completed", Application.ProductName, MessageBoxButtons.OK, _
        MessageBoxIcon.Information)

    End Sub

    Sub changePackageColor(color As Boolean, topNode As AdvTree.Node)
        Dim enabledPackageImage As Image = resizeImage(My.Resources.box2, 16, 16)
        Dim enabledEventPackageImage As Image = resizeImage(My.Resources.cube_molecule2, 16, 16)

        For Each nn As AdvTree.Node In topNode.Nodes
            If nn.Tag = "package" Then
                If color = False Then
                    nn.Image = MakeGrayscale3(nn.Image)
                Else
                    nn.Image = enabledPackageImage
                End If
            ElseIf nn.Tag = "event-package" Then
                If color = False Then
                    nn.Image = MakeGrayscale3(nn.Image)
                Else
                    nn.Image = enabledEventPackageImage
                End If
            End If

            changePackageColor(color, nn)
        Next
    End Sub
    Private Sub btnfolderEnable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnfolderEnable.Click
        If folderTree.SelectedNode Is Nothing Then Return

        If isAllowedAccessToFolder(folderTree.SelectedNode.DataKey) = False Then
            Return
        End If

        Dim oFolder As New clsFolders

        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim nID As Integer = folderTree.SelectedNode.DataKey

        oFolder.EnableFolder(nID)

        If tvExplorer.SelectedNode IsNot Nothing Then
            changePackageColor(True, tvExplorer.SelectedNode)
        Else
            Dim selectedNode As AdvTree.Node = tvExplorer.FindNodeByDataKey(nID)

            If selectedNode IsNot Nothing Then
                changePackageColor(True, selectedNode)
            End If
        End If

        If folderTree.SelectedNode IsNot Nothing Then loadData(folderTree.SelectedNode.DataKey)

        MessageBox.Show("The requested task has completed", Application.ProductName, MessageBoxButtons.OK, _
        MessageBoxIcon.Information)
    End Sub

    Private Sub btnfolderExecute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnfolderExecute.Click
        If folderTree.SelectedNode Is Nothing Then Return

        If isAllowedAccessToFolder(folderTree.SelectedNode.DataKey) = False Then
            Return
        End If

        Dim oFolder As New clsFolders

        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim nID As Integer = folderTree.SelectedNode.DataKey

        oFolder.ExecuteFolder(nID)

        MessageBox.Show("The requested task has completed", Application.ProductName, MessageBoxButtons.OK, _
        MessageBoxIcon.Information)
    End Sub

    Private Sub btnNewFolder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewFolder.Click, mnudtNewFolder.Click, btnfolderNew.Click

        If clsMarsSecurity._HasGroupAccess("Folder Management") = False Then
            Return
        ElseIf isAllowedAccessToFolder() = False Then
            Return
        End If

        Dim currentFolder As AdvTree.Node
        Dim parent As folder
        Dim newFolderName As String = InputBox("Enter the new folder name", Application.ProductName, "Untitled Folder")
        Dim err As String = ""

        If newFolderName = "" Then Return

        If tvExplorer.SelectedNode Is Nothing Then
            parent = New folder(0)
        Else
            currentFolder = tvExplorer.SelectedNode
            parent = New folder(currentFolder.DataKey)
        End If

        Dim newFolderID As Integer = parent.addFolder(newFolderName, err)

        If newFolderID > 0 Then
            loadData(parent.folderID, True)

            Dim parentNode As AdvTree.Node = searchTree(parent.folderID)

            If parentNode IsNot Nothing Then
                tvExplorer.SelectedNode = parentNode
                parentNode.Expand()
            End If

            Dim newNode As DevComponents.AdvTree.Node = tvMain.FindNodeByDataKey(newFolderID)

            If newNode IsNot Nothing Then
                tvMain.SelectedNode = newNode
                newNode.EnsureVisible()
            End If

            buildCrumbBar(parent.folderID, crumb.SelectedItem)
        Else
            MessageBox.Show("An error occured: " & err, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub btnDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDetails.Click
        Dim vv As frmViewOptons = New frmViewOptons

        If vv.getViewColumns() IsNot Nothing Then
            If tvExplorer.SelectedNode Is Nothing Then Return

            buildDetails(New folder(tvExplorer.SelectedNode.DataKey))
        End If
    End Sub

    Private Sub addSmartFolders()
        Dim SQL As String
        Dim oRs As New ADODB.Recordset
        Dim sNotAdmin As String
        Dim oNode As DevComponents.AdvTree.Node


        SQL = "SELECT * FROM SmartFolders"

        If gRole.ToLower <> "administrator" Then
            sNotAdmin = " WHERE SmartID IN (SELECT SmartID FROM UserView WHERE " & _
            "UserID = '" & gUser & "')"

            SQL &= sNotAdmin
        End If

        SQL &= " ORDER BY SmartName"

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Dim smartFolderImage As Image = resizeImage(My.Resources.Folder_cubes_256x256, folderImageSize)

            Do While oRs.EOF = False
                oNode = New DevComponents.AdvTree.Node(oRs("smartname").Value)

                oNode.Image = smartFolderImage
                oNode.DataKey = oRs("smartid").Value
                oNode.Tag = "smartfolder"
                tvExplorer.Nodes.Add(oNode)

                oRs.MoveNext()
            Loop
        End If

        oRs.Close()

        Dim addLines As Boolean = False

        oRs = clsMarsData.GetData("SELECT * FROM SystemFolders", ADODB.CursorTypeEnum.adOpenStatic)

        If oRs.RecordCount > 4 Or oRs.EOF = True Then
            clsMarsData.WriteData("DELETE FROM SystemFolders")
            addLines = True
        End If

        If addLines = True Then
            Dim sCols As String = "FolderID,FolderName,FolderSQL"
            Dim sVals As String

            sVals = "1,'Packages Due','SELECT * FROM PackageAttr INNER JOIN ScheduleAttr ON " & _
            "PackageAttr.PackID = ScheduleAttr.PackID WHERE ScheduleAttr.NextRun <= Now() AND Status =1'"

            clsMarsData.WriteData("INSERT INTO SystemFolders (" & sCols & ") VALUES (" & sVals & ")", False)

            sVals = "2,'Single Schedules Due','SELECT * FROM ReportAttr INNER JOIN ScheduleAttr ON " & _
            "ReportAttr.ReportID = ScheduleAttr.ReportID WHERE ScheduleAttr.NextRun <= Now() AND Status =1'"

            clsMarsData.WriteData("INSERT INTO SystemFolders (" & sCols & ") VALUES (" & sVals & ")", False)

            SQL = "SELECT * FROM (PackageAttr INNER JOIN " & _
            "ScheduleHistory ON PackageAttr.PackID = ScheduleHistory.PackID) INNER JOIN " & _
            "ScheduleAttr ON PackageAttr.PackID = ScheduleAttr.PackID WHERE Success =0 AND " & _
            "(Year(entrydate) & '-' & month(entrydate) & '-' & day(entrydate)) = " & _
            "Year(Now()) & '-' & month(now()) & '-' & day(now())"

            sVals = "3,'Todays Failed Packages','" & SQLPrepare(SQL) & "'"

            clsMarsData.WriteData("INSERT INTO SystemFolders (" & sCols & ") VALUES (" & sVals & ")", False)

            SQL = "SELECT * FROM (ReportAttr INNER JOIN " & _
            "ScheduleHistory ON ReportAttr.ReportID = ScheduleHistory.ReportID) INNER JOIN " & _
            "ScheduleAttr ON ReportAttr.ReportID = ScheduleAttr.ReportID WHERE Success =0 AND " & _
            "(Year(entrydate) & '-' & month(entrydate) & '-' & day(entrydate)) = " & _
            "Year(Now()) & '-' & month(now()) & '-' & day(now())"

            sVals = "4,'Todays Failed Single Schedules','" & SQLPrepare(SQL) & "'"

            clsMarsData.WriteData("INSERT INTO SystemFolders (" & sCols & ") VALUES (" & sVals & ")", False)
        End If

        oRs.Close()
    End Sub

    Sub addSystemFolders()

        Dim oRs As ADODB.Recordset
        Dim oNode As DevComponents.AdvTree.Node

        oRs = clsMarsData.GetData("SELECT * FROM SystemFolders")

        Dim sysFolderImage As Image = resizeImage(My.Resources.Folder_info_256x256, folderImageSize)

        Do While oRs.EOF = False
            oNode = New DevComponents.AdvTree.Node(oRs("foldername").Value)

            With oNode
                .DataKey = oRs("folderid").Value
                .Tag = "systemfolder"
                .Image = sysFolderImage
                tvExplorer.Nodes.Add(oNode)
            End With

            oRs.MoveNext()
        Loop

        oRs.Close()
    End Sub

    Private Sub btnOutlook_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOutlook.Click

        Dim outlook As frmWindow = New frmWindow

        outlook.Show()
    End Sub

    Private Sub btnSingleScheduleData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSingleScheduleData.Click

        Dim SQL As String = "SELECT reportattr.*, destinationattr.*, scheduleattr.* " & _
        "FROM (destinationattr INNER JOIN reportattr ON destinationattr.reportid = reportattr.reportid) " & _
        "INNER JOIN scheduleattr ON reportattr.reportid = scheduleattr.reportid"

        Dim oDump As frmHistoryReport = New frmHistoryReport

        oDump.systemDumpReport(SQL, "Single Schedules")
    End Sub

    Private Sub btnPackageScheduleData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPackageScheduleData.Click

        Dim SQL As String = "SELECT packageattr.*, scheduleattr.*, reportattr.*, destinationattr.* " & _
        "FROM ((packageattr INNER JOIN scheduleattr ON packageattr.packid = scheduleattr.packid) INNER JOIN " & _
        "destinationattr ON packageattr.packid = destinationattr.packid) LEFT JOIN " & _
        "reportattr ON packageattr.packid = reportattr.packid"

        Dim oDump As frmHistoryReport = New frmHistoryReport

        oDump.systemDumpReport(SQL, "Package Schedules")

    End Sub

    Private Sub btnEventBasedData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEventBasedData.Click
        Dim SQL As String = "SELECT EventAttr6.*, EventConditions.* " & _
        "FROM (EventAttr6 INNER JOIN EventConditions ON EventAttr6.EventID = EventConditions.EventID) " & _
        "WHERE (PackID=0 OR PackID IS NULL)"

        Dim oDump As frmHistoryReport = New frmHistoryReport

        oDump.systemDumpReport(SQL, "Event-Based Schedules")
    End Sub

    Private Sub btnEventPackageData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEventPackageData.Click
        Dim SQL As String = "SELECT EventPackageAttr.*, EventAttr6.*, EventConditions.*, ScheduleAttr.* " & _
       "FROM (EventPackageAttr INNER JOIN ScheduleAttr ON EventPackageAttr.EventPackID = ScheduleAttr.EventPackID) " & _
       "INNER JOIN (EventAttr6 INNER JOIN EventConditions ON EventAttr6.EventID = EventConditions.EventID) ON " & _
       "EventPackageAttr.EventPackID = EventAttr6.PackID"

        Dim oDump As frmHistoryReport = New frmHistoryReport

        oDump.systemDumpReport(SQL, "Event-Based Package Schedules")

    End Sub

    Private Sub btnAutomationScheduleData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAutomationScheduleData.Click
        Dim SQL As String = "SELECT automationattr.*, tasks.*, scheduleattr.* " & _
        "FROM (tasks INNER JOIN scheduleattr ON tasks.scheduleid = scheduleattr.scheduleid) " & _
        "INNER JOIN automationattr ON automationattr.autoid = scheduleattr.autoid"

        Dim oDump As frmHistoryReport = New frmHistoryReport

        oDump.systemDumpReport(SQL, "Automation Schedules")
    End Sub

    Private Sub btnpackExecute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnpackExecute.Click
        Dim oPackage As clsMarsReport = New clsMarsReport
        Dim oSchedule As clsMarsScheduler = New clsMarsScheduler
        Dim Success As Boolean
        Dim nPackID As Integer
        Dim oExecute As clsMarsThreading = New clsMarsThreading

        crdxCommon.executingForm = Me

        nPackID = tvMain.SelectedNode.DataKey

        If clsMarsScheduler.isScheduleLocked(nPackID, clsMarsScheduler.enScheduleType.PACKAGE) = True Then
            showViolatorMessage()
            Return
        End If

        oExecute.xPackID = nPackID

        AppStatus(True)
        oExecute.PackageScheduleThread()
        AppStatus(False)

        clsMarsAudit._LogAudit(tvMain.SelectedNode.Text, clsMarsAudit.ScheduleType.PACKAGE, clsMarsAudit.AuditAction.EXECUTE)

        editItemRow(nPackID, clsMarsScheduler.enScheduleType.PACKAGE)
    End Sub

    Private Sub btnautoExecute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnautoExecute.Click
        Dim oThread As New clsMarsThreading

        If tvMain.SelectedNodes.Count = 0 Then Exit Sub

        crdxCommon.executingForm = Me

        Dim nID As Integer = tvMain.SelectedNode.DataKey

        If clsMarsScheduler.isScheduleLocked(nID, clsMarsScheduler.enScheduleType.AUTOMATION) = True Then
            showViolatorMessage()
            Return
        End If

        oThread.xAutoID = nID

        AppStatus(True)

        oThread.AutoScheduleThread()

        AppStatus(False)

        clsMarsAudit._LogAudit(tvMain.SelectedNode.Text, clsMarsAudit.ScheduleType.AUTOMATION, clsMarsAudit.AuditAction.EXECUTE)

        editItemRow(nID, clsMarsScheduler.enScheduleType.AUTOMATION)
    End Sub

    Private Sub btnOptions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOptions.Click, btnOptions1.Click
        Dim opt As frmOptions = New frmOptions

        opt.ShowDialog()

        Try
            Dim autoRefresh As Boolean = Convert.ToInt16(clsMarsUI.MainUI.ReadRegistry("DTRefresh", 0))

            If autoRefresh = True Then
                If tmAutoRefreshUI.Enabled = False Then
                    Dim refreshInt As Integer = Convert.ToInt16(clsMarsUI.MainUI.ReadRegistry("DTRefreshInterval", 0))

                    tmAutoRefreshUI.Interval = refreshInt * 1000

                    AddHandler tmAutoRefreshUI.Tick, AddressOf uiRefreshTimerElapsed

                    tmAutoRefreshUI.Start()
                End If
            Else
                tmAutoRefreshUI.Stop()
                tmAutoRefreshUI.Enabled = False
            End If
        Catch : End Try

    End Sub

    Private Sub btnAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAbout.Click, btnAboutToo.Click
        If IO.File.Exists(IO.Path.Combine(sAppPath, "regwizx.exe")) Then
            Dim about As frmAboutX = New frmAboutX

            about.ShowDialog()
        Else
            Dim about As frmAbout = New frmAbout

            about.ShowDialog()
        End If
    End Sub

    Private Sub btnExportSchedules_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportSchedules.Click
        Dim exp As frmExportWizard = New frmExportWizard

        exp.ShowDialog()

    End Sub

    Private Sub btnNewSmartFolder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewSmartFolder.Click, mnudtNewSmartFolder.Click
        If clsMarsSecurity._HasGroupAccess("Folder Management") = False Then
            Return
        End If

        Dim smart As frmSmartFolders = New frmSmartFolders

        smart.ShowDialog()

        loadFolders()
    End Sub

    Private Sub btnSystemMonitor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSystemMonitor.Click

        If gSysmonInstance Is Nothing Then
            gSysmonInstance = New frmSystemMonitor
        ElseIf gSysmonInstance.IsDisposed Then
            gSysmonInstance = Nothing
            gSysmonInstance = New frmSystemMonitor
        End If

        If gSysmonInstance.WindowState = FormWindowState.Minimized Then
            gSysmonInstance.WindowState = FormWindowState.Maximized
        End If

        gSysmonInstance.Show()
        gSysmonInstance.Focus()
        gSysmonInstance.BringToFront()
    End Sub

    Private Sub tmSchedulerInfo_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tmSchedulerInfo.Tick
        Try
            Dim sc As schedulerInfo = New schedulerInfo

            advProps.SelectedObject = sc

            Dim svc As clsServiceController = New clsServiceController

            UcSchedulerControler1.schedulerStatus = svc.m_serviceStatus

            sc = Nothing
        Catch : End Try

    End Sub

   

    Private Sub ButtonItem8_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem8.Click
        Me.Close()
    End Sub

    Private Sub moveObject()
        Try
            If tvMain.SelectedNode Is Nothing Then Return

            Dim oFolders As frmFolders = New frmFolders
            Dim tempType As String = tvMain.SelectedNode.Tag
            Dim parent As Integer = 0
            Dim resultVal() As String = oFolders.GetFolder("Please select the folder to move to")
            Dim currentFolderID As Integer = tvExplorer.SelectedNode.DataKey
            If resultVal(0) = "" Then Return

            parent = resultVal(1)

            Dim it As AdvTree.Node = tvMain.SelectedNode

            Dim sType As String = Me.getSelectedObjectTypeFromParent(it.Parent.Text)
            Dim nID As Integer = it.DataKey
            Dim SQL As String = ""

            Select Case sType.ToLower
                Case "report"
                    SQL = "UPDATE ReportAttr SET Parent =" & parent & " WHERE ReportID =" & nID
                Case "package"
                    SQL = "UPDATE PackageAttr SET Parent =" & parent & " WHERE PackID =" & nID
                Case "automation"
                    SQL = "UPDATE AutomationAttr SET Parent =" & parent & " WHERE AutoID =" & nID
                Case "event"
                    SQL = "UPDATE EventAttr6 SET Parent =" & parent & " WHERE EventID =" & nID
                Case "event-package"
                    SQL = "UPDATE EventPackageAttr SET Parent =" & parent & " WHERE EventPackID =" & nID
                Case "folder"
                    If nID = parent Then
                        MessageBox.Show("A folder cannot be moved into itself", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    ElseIf clsMarsUI.MainUI.IsMoveValid(nID, parent) = False Then
                        MessageBox.Show("Cannot move the folder as the destination is a subfolder of the source folder", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If

                    SQL = "UPDATE Folders SET Parent = " & parent & " WHERE FolderID = " & nID
                Case "packed report"
                    Dim newID As Integer
                    Dim newName As String
                    Dim oldName As String = it.Text
                    Dim oRs As ADODB.Recordset

                    SQL = "SELECT MAX(PackOrderID) FROM ReportAttr WHERE PackID =" & parent

                    oRs = clsMarsData.GetData(SQL)

                    If oRs IsNot Nothing Then
                        newID += 1
                    Else
                        newID = 1
                    End If

                    oRs.Close()

                    newName = newID & ":" & oldName.Split(":")(1)

                    SQL = "UPDATE ReportAttr SET " & _
                    "PackID =" & parent & "," & _
                    "ReportTitle ='" & SQLPrepare(newName) & "'," & _
                    "PackOrderID =" & newID & " WHERE ReportID =" & nID
                Case Else
                    Return
            End Select

            clsMarsData.WriteData(SQL)

            tvMain_AfterNodeSelect(Nothing, Nothing)

            Dim nn As AdvTree.Node = tvExplorer.FindNodeByDataKey(parent)

            If nn IsNot Nothing Then
                Dim fld As folder = New folder(parent)
                nn.Text = fld.folderName & "(" & fld.getTotalContents & ")"
            End If

            tvExplorer.SelectedNode = Nothing

            nn = tvExplorer.FindNodeByDataKey(currentFolderID)

            If nn IsNot Nothing Then
                Dim fld As folder = New folder(currentFolderID)
                nn.Text = fld.folderName & "(" & fld.getTotalContents & ")"
                tvExplorer.SelectedNode = nn
                nn.EnsureVisible()
            End If
            'loadData(tvExplorer.SelectedNode.DataKey)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try
    End Sub

    Private Sub btnmltMove_Click(sender As System.Object, e As System.EventArgs) Handles btnmltMove.Click
        If tvMain.SelectedNodes.Count = 0 Then Return


        Dim oFolders As frmFolders = New frmFolders
        Dim tempType As String = tvMain.SelectedNode.Tag
        Dim parent As Integer = 0

        'If tempType = "Packed Report" Then
        '    oFolders.m_packageOnly = True

        '    parent = oFolders.GetPackage()

        '    If parent = 0 Then Return
        'Else
        Dim resultVal() As String = oFolders.GetFolder("Please select the folder to move to")

        If resultVal(0) = "" Then Return

        parent = resultVal(1)
        'End If

        For Each it As DevComponents.AdvTree.Node In tvMain.SelectedNodes
            If it.Parent Is Nothing Then Continue For

            Dim sType As String = Me.getSelectedObjectTypeFromParent(it.Parent.Text)
            Dim nID As Integer = it.DataKey
            Dim SQL As String = ""

            Select Case sType.ToLower
                Case "report"
                    SQL = "UPDATE ReportAttr SET Parent =" & parent & " WHERE ReportID =" & nID
                Case "package"
                    SQL = "UPDATE PackageAttr SET Parent =" & parent & " WHERE PackID =" & nID
                Case "automation"
                    SQL = "UPDATE AutomationAttr SET Parent =" & parent & " WHERE AutoID =" & nID
                Case "event"
                    SQL = "UPDATE EventAttr6 SET Parent =" & parent & " WHERE EventID =" & nID
                Case "event-package"
                    SQL = "UPDATE EventPackageAttr SET Parent =" & parent & " WHERE EventPackID =" & nID
                Case "folder"
                    If nID = parent Then
                        MessageBox.Show("A folder cannot be moved into itself", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Continue For
                    ElseIf clsMarsUI.MainUI.IsMoveValid(nID, parent) = False Then
                        MessageBox.Show("Cannot move the folder as the destination is a subfolder of the source folder", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Continue For
                    End If

                    SQL = "UPDATE Folders SET Parent = " & parent & " WHERE FolderID = " & nID
                Case "packed report"
                    Dim newID As Integer
                    Dim newName As String
                    Dim oldName As String = it.Text
                    Dim oRs As ADODB.Recordset

                    SQL = "SELECT MAX(PackOrderID) FROM ReportAttr WHERE PackID =" & parent

                    oRs = clsMarsData.GetData(SQL)

                    If oRs IsNot Nothing Then
                        newID += 1
                    Else
                        newID = 1
                    End If

                    oRs.Close()

                    newName = newID & ":" & oldName.Split(":")(1)

                    SQL = "UPDATE ReportAttr SET " & _
                    "PackID =" & parent & "," & _
                    "ReportTitle ='" & SQLPrepare(newName) & "'," & _
                    "PackOrderID =" & newID & " WHERE ReportID =" & nID
                Case Else
                    Continue For
            End Select

            clsMarsData.WriteData(SQL)
        Next

        loadData(tvExplorer.SelectedNode.DataKey)
    End Sub

    ''' <summary>
    ''' return one of the following: report, package, automation, event, event-based, folder, none
    ''' </summary>
    ''' <param name="parent"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function getSelectedObjectTypeFromParent(parent As String) As String
        Select Case parent
            Case "Data-Driven Schedules", "Dynamic Schedules", "Bursting Schedules", "Single Schedules"
                Return "report"
            Case "Data-Driven Packages", "Single Packages", "Dynamic Packages"
                Return "package"
            Case "Automation Schedules"
                Return "automation"
            Case "Event-Based Schedules"
                Return "event"
            Case "Event-Based Packages"
                Return "event-package"
            Case "Folders"
                Return "folder"
            Case "Package Reports"
                Return "packed report"
            Case Else
                Return "none"
        End Select
    End Function

    Private Sub btnmltRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnmltRefresh.Click
        Dim i As Integer = 0

        For Each it As DevComponents.AdvTree.Node In tvMain.SelectedNodes
            If it.Parent Is Nothing Then Continue For

            crdxCommon.executingForm = Me

            clsMarsUI.BusyProgress((i / tvMain.SelectedNodes.Count) * 100, "Refreshing " & it.Text)

            Dim sType As String = getSelectedObjectTypeFromParent(it.Parent.Text)
            Dim nID As Integer = it.DataKey

            Select Case sType.ToLower
                Case "report"
                    clsMarsReport.oReport.RefreshReport(nID)
                Case "packed report"
                    clsMarsReport.oReport.RefreshReport(nID)
                Case "package"
                    clsMarsReport.oReport.RefreshPackage(nID)
            End Select
        Next

        MessageBox.Show("The request has completed", Application.ProductName, MessageBoxButtons.OK)

        clsMarsUI.BusyProgress(100, "", True)
    End Sub

    Private Sub btnmltExecute_Click(sender As System.Object, e As System.EventArgs) Handles btnmltExecute.Click
        Dim limitMsg As String = "Your current SQL-RD edition only allows for the execution of " & nMax & " schedules at a time."

        If IsFeatEnabled(gEdition.NULL, modFeatCodes.g1_Unlimit_Sched) = False Then
            Select Case gnEdition
                Case gEdition.BRONZE
                    If tvMain.SelectedNodes.Count > nMax Then
                        MessageBox.Show(limitMsg, _
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Return
                    End If
                Case gEdition.SILVER
                    If tvMain.SelectedNodes.Count > nMax Then
                        MessageBox.Show(limitMsg, _
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Return
                    End If
                Case gEdition.GOLD
                    If tvMain.SelectedNodes.Count > nMax Then
                        MessageBox.Show(limitMsg, _
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Return
                    End If
            End Select
        End If

        For Each it As DevComponents.AdvTree.Node In tvMain.SelectedNodes
            If it.Parent Is Nothing Then Continue For

            Dim sType As String = getSelectedObjectTypeFromParent(it.Parent.Text)
            Dim nID As Integer = it.DataKey

            Select Case sType.ToLower
                Case "report"
                    Dim oT As New clsMarsThreading

                    oT.xReportID = nID

                    If clsMarsData.IsScheduleBursting(nID) = True Then
                        oT.BurstingScheduleThread()
                    ElseIf clsMarsData.IsScheduleDynamic(nID, "report") = True Then
                        oT.DynamicScheduleThread()
                    Else
                        oT.SingleScheduleThread()
                    End If
                Case "packed report"
                    'Dim oT As New clsMarsThreading
                    'Dim nPackID As Integer = tvFolders.SelectedNode.datakey.Split(":")(1)

                    'oT.xPackID = nPackID
                    'oT.xReportID = nID

                    'oT.PackageScheduleThread()
                Case "package"
                    Dim oT As New clsMarsThreading
                    oT.xPackID = nID
                    oT.PackageScheduleThread()
                Case "automation"
                    Dim oT As New clsMarsThreading
                    oT.xAutoID = nID
                    oT.AutoScheduleThread()
                Case "event"
                    Dim oT As New clsMarsThreading
                    oT.xEventID = nID
                    oT.EventScheduleThread()
                Case "event-package"
                    Dim oT As New clsMarsThreading
                    oT.xPackID = nID
                    oT.EventPackageScheduleThread()
            End Select

            _Delay(1.5)
        Next

    End Sub

    Private Sub btnmltConvertToPackage_Click(sender As System.Object, e As System.EventArgs) Handles btnmltConvertToPackage.Click
        Dim schedules As Hashtable = New Hashtable

        For Each it As DevComponents.AdvTree.Node In tvMain.SelectedNodes
            If it.Parent Is Nothing Then Continue For

            Dim sType As String = getSelectedObjectTypeFromParent(it.Parent.Text)
            Dim nID As Integer = it.DataKey

            If sType.ToLower = "report" Then
                schedules.Add(it.Text, nID)
            End If
        Next

        Dim selektor As frmItemSelector = New frmItemSelector

        Dim templateID As Integer = selektor.chooseTemplate(schedules)


        If templateID > 0 Then
            If clsMarsReport.convertReportstoPackage(schedules, templateID) = True Then
                loadData(tvExplorer.SelectedNode.DataKey)
            End If
        End If
    End Sub

    Private Sub btnmltProperties_Click(sender As System.Object, e As System.EventArgs) Handles btnmltProperties.Click
        '<schedulesTable>
        '<ID>Integer</ID>
        '<ScheduleType>String</ScheduleType>
        '<ScheduleID>Integer</ScheduleID>
        '</schedulesTable>
        Dim schedulesTable As DataTable = New DataTable

        With schedulesTable
            .Columns.Add("ID")
            .Columns.Add("scheduleType")
            .Columns.Add("scheduleID")
        End With

        For Each it As DevComponents.AdvTree.Node In tvMain.SelectedNodes
            Dim sType As String = getSelectedObjectTypeFromParent(it.Parent.Text)
            Dim nID As Integer = it.DataKey

            Dim row As DataRow = schedulesTable.Rows.Add

            row("ID") = nID
            row("scheduleType") = sType
            row("scheduleID") = ""
        Next

        Dim mltProp As frmCollectiveProp = New frmCollectiveProp

        mltProp.editSchedules(schedulesTable)
    End Sub

    Private Sub btnmltMoveToPackage_Click(sender As System.Object, e As System.EventArgs) Handles btnmltMoveToPackage.Click
        'first check if all the items are reports or event-based schedules
        Dim itemType As String = ""

        For Each it As DevComponents.AdvTree.Node In tvMain.SelectedNodes
            If it.Parent Is Nothing Then Continue For

            Dim type As String = getSelectedObjectTypeFromParent(it.Parent.Text)

            If itemType = "" Then
                itemType = type
            ElseIf itemType <> type Then
                MessageBox.Show("Please select either all Single Report Schedules or all Event-Based Schedules. You cannot mix and match okay?", _
                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If
        Next

        If itemType.ToLower = "report" Then
            Dim oFolder As frmFolders = New frmFolders
            oFolder.m_packageOnly = True
            Dim result As String() = oFolder.GetFolder("Please select the package to move the schedules into")

            If result(0) = "" Then Return

            Dim packID = result(1)
            Dim reportIDs() As Integer
            Dim I As Integer = 0

            For Each it As DevComponents.AdvTree.Node In tvMain.SelectedNodes
                Dim type As String = getSelectedObjectTypeFromParent(it.Parent.Text)
                Dim nID As Integer = it.DataKey

                If type.ToLower = "report" Then
                    ReDim Preserve reportIDs(I)

                    reportIDs(I) = nID

                    I += 1
                End If
            Next

            clsMarsReport.moveintoPackage(packID, reportIDs)

            loadData(tvExplorer.SelectedNode.DataKey)
        ElseIf itemType.ToLower = "event" Then
            Dim oFolder As frmFolders = New frmFolders
            oFolder.m_eventPackage = True

            Dim result As String() = oFolder.GetFolder("Please select the Event-Based Package to move the schedules into")

            If result(0) = "" Then Return

            Dim packID = result(1)
            Dim eventIDs() As Integer
            Dim I As Integer = 0

            For Each it As DevComponents.AdvTree.Node In tvMain.SelectedNodes
                Dim type As String = getSelectedObjectTypeFromParent(it.Parent.Text)
                Dim nID As Integer = it.DataKey

                If type.ToLower = "event" Then
                    ReDim Preserve eventIDs(I)

                    eventIDs(I) = nID

                    I += 1
                End If
            Next

            clsMarsEvent.moveintoEventPackage(packID, eventIDs)

            loadData(tvExplorer.SelectedNode.DataKey)
        End If

    End Sub

    Private Sub btnmltDelete_Click(sender As System.Object, e As System.EventArgs) Handles btnmltDelete.Click
        DeleteDesktopItem(sender, e, False)
    End Sub

    Private Sub btnmltEnable_Click(sender As System.Object, e As System.EventArgs) Handles btnmltEnable.Click
        If tvMain.SelectedNodes.Count = 0 Then Return
        Dim nID As Integer
        Dim type As String
        Dim table, column As String

        For Each item As DevComponents.AdvTree.Node In tvMain.SelectedNodes
            If item.Parent Is Nothing Then Continue For

            nID = item.DataKey
            type = getSelectedObjectTypeFromParent(item.Parent.Text)

            Select Case type
                Case "report"
                    table = "scheduleattr"
                    column = "reportid"
                Case "package"
                    table = "scheduleattr"
                    column = "packid"
                Case "automation"
                    table = "scheduleattr"
                    column = "autoid"
                Case "event"
                    table = "eventattr6"
                    column = "reportid"
                Case "event-package"
                    table = "scheduleattr"
                    column = "eventpackid"
            End Select

            Dim SQL As String = "UPDATE " & table & " SET Status = 1  WHERE " & column & " = " & nID

            clsMarsData.WriteData(SQL)
        Next

        MessageBox.Show("The operation has completed", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        loadData(tvExplorer.SelectedNode.DataKey)
    End Sub

    Private Sub btnmltDisable_Click(sender As System.Object, e As System.EventArgs) Handles btnmltDisable.Click
        If tvMain.SelectedNodes.Count = 0 Then Return
        Dim nID As Integer
        Dim type As String
        Dim table, column As String

        For Each item As DevComponents.AdvTree.Node In tvMain.SelectedNodes
            If item.Parent Is Nothing Then Continue For

            nID = item.DataKey
            type = getSelectedObjectTypeFromParent(item.Parent.Text)

            Select Case type
                Case "report"
                    table = "scheduleattr"
                    column = "reportid"
                Case "package"
                    table = "scheduleattr"
                    column = "packid"
                Case "automation"
                    table = "scheduleattr"
                    column = "autoid"
                Case "event"
                    table = "eventattr6"
                    column = "eventid"
                Case "event-package"
                    table = "scheduleattr"
                    column = "eventpackid"
            End Select

            Dim SQL As String = "UPDATE " & table & " SET Status = 0,  DisabledDate = '" & ConDateTime(Date.Now) & "' WHERE " & column & " = " & nID

            clsMarsData.WriteData(SQL)
        Next

        MessageBox.Show("The operation has completed", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        loadData(tvExplorer.SelectedNode.DataKey)
    End Sub

    Private Sub tvExplorer_BeforeCellEdit(sender As Object, e As DevComponents.AdvTree.CellEditEventArgs) Handles tvExplorer.BeforeCellEdit
        Dim test As String = tvExplorer.SelectedNode.Tag
        If test.ToLower = "folder" Then
            Dim folderID As Integer = tvExplorer.SelectedNode.DataKey

            Dim fld As folder = New folder(folderID)

            tvExplorer.SelectedNode.Text = fld.folderName
        Else
            Dim folderid As Integer = tvExplorer.SelectedNode.DataKey
            Dim fld As smartfolder = New smartfolder(folderid)
            tvExplorer.SelectedNode.Text = fld.folderName
        End If

    End Sub

    Private Sub tvExplorer_BeforeNodeDrop(sender As Object, e As DevComponents.AdvTree.TreeDragDropEventArgs) Handles tvExplorer.BeforeNodeDrop
        Dim id As Integer
        Dim parentid As Integer
        Dim table As String
        Dim parentColumn As String
        Dim keyColumn As String
        Dim type As String
        Dim otype As clsMarsScheduler.enScheduleType


        For Each nn As AdvTree.Node In e.Nodes
            type = nn.Tag
            id = nn.DataKey

            If e.NewParentNode IsNot Nothing Then
                parentid = e.NewParentNode.DataKey

                Dim parentType As String = e.NewParentNode.Tag

                If parentType <> "folder" Then
                    parentid = 0
                End If
            Else
                parentid = 0
            End If

            Select Case type
                Case "report"
                    table = "reportattr"
                    parentColumn = "parent"
                    keyColumn = "reportid"
                    otype = clsMarsScheduler.enScheduleType.REPORT
                Case "package"
                    table = "packageattr"
                    parentColumn = "parent"
                    keyColumn = "packid"
                    otype = clsMarsScheduler.enScheduleType.PACKAGE
                Case "automation"
                    table = "automationattr"
                    parentColumn = "parent"
                    keyColumn = "autoid"
                    otype = clsMarsScheduler.enScheduleType.AUTOMATION
                Case "event"
                    table = "eventattr6"
                    parentColumn = "parent"
                    keyColumn = "eventid"
                    otype = clsMarsScheduler.enScheduleType.EVENTBASED
                Case "event-package"
                    table = "eventpackageattr"
                    parentColumn = "parent"
                    keyColumn = "eventpackid"
                    otype = clsMarsScheduler.enScheduleType.EVENTPACKAGE
                Case "folder"
                    table = "folders"
                    parentColumn = "parent"
                    keyColumn = "folderid"
                    otype = clsMarsScheduler.enScheduleType.FOLDER


                    If clsMarsUI.MainUI.IsMoveValid(nn.DataKey, parentid) = False Then
                        MessageBox.Show("Cannot move the folder as the destination is a subfolder of the source folder", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        e.Cancel = True
                    ElseIf nn.DataKey = parentid Then
                        MessageBox.Show("A folder cannot be moved into itself", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        e.Cancel = True
                    End If
                Case Else
                    e.Cancel = True
            End Select

            If clsMarsUI.candoRename(nn.Text, parentid, otype) = False Then
                MessageBox.Show("The destination folder already contains an item named '" & nn.Text & "'. The operation will be cancelled.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                e.Cancel = True
                Exit For
            ElseIf type <> "folder" And parentid = 0 Then
                MessageBox.Show("You cannot move this type of object to the root", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                e.Cancel = True
            End If

            ' id = nn.DataKey

            Dim SQL As String = "UPDATE " & table & " SET " & parentColumn & " = " & parentid & " WHERE " & keyColumn & " = " & id

            If e.Cancel = False Then clsMarsData.WriteData(SQL)
        Next

        If e.Cancel = False Then


            Dim tmp As AdvTree.Node = e.NewParentNode


            '  loadData(parentid)

            crumb.Items.Clear()

            For Each ff As frmMainWin In crdxCommon.gwindows
                Try
                    If ff.Tag <> Me.Tag Then
                        ff.loadData(ff.tvExplorer.SelectedNode.DataKey, True)
                        ff.buildCrumbBar()
                    End If
                Catch : End Try
            Next

            buildCrumbBar()
            updateCrumbs()

            If e.NewParentNode IsNot Nothing Then
                Dim parentNode As AdvTree.Node = e.NewParentNode.Parent

                Do While parentNode IsNot Nothing
                    parentNode.Expand()

                    parentNode = parentNode.Parent
                Loop

5:              tvExplorer.SelectedNode = e.NewParentNode
                e.NewParentNode.EnsureVisible()
            End If

            For Each nn As AdvTree.Node In e.Nodes
                Dim nx As AdvTree.Node = tvMain.FindNodeByDataKey(nn.DataKey)

                If nx IsNot Nothing Then
                    tvMain.SelectedNodes.Add(nx)
                End If
            Next
        End If

        e.Cancel = True
    End Sub
    Private Sub tvExplorer_BeforeNodeDropold(sender As Object, e As DevComponents.AdvTree.TreeDragDropEventArgs)
        Dim id As Integer
        Dim parentid As Integer
        Dim table As String
        Dim parentColumn As String
        Dim keyColumn As String
        Dim type As String
        Dim otype As clsMarsScheduler.enScheduleType

        If clsMarsSecurity._HasGroupAccess("Folder Management") = False Then
            e.Cancel = True
            Return
        End If


        For Each nn As AdvTree.Node In e.Nodes
            type = nn.Tag
            id = nn.DataKey

            If e.NewParentNode IsNot Nothing Then
                parentid = e.NewParentNode.DataKey

                Dim parentType As String = e.NewParentNode.Tag

                If parentType <> "folder" Then
                    parentid = 0
                End If
            Else
                parentid = 0
            End If

            Select Case type
                Case "report"
                    table = "reportattr"
                    parentColumn = "parent"
                    keyColumn = "reportid"
                    otype = clsMarsScheduler.enScheduleType.REPORT
                Case "package"
                    table = "packageattr"
                    parentColumn = "parent"
                    keyColumn = "packid"
                    otype = clsMarsScheduler.enScheduleType.PACKAGE
                Case "automation"
                    table = "automationattr"
                    parentColumn = "parent"
                    keyColumn = "autoid"
                    otype = clsMarsScheduler.enScheduleType.AUTOMATION
                Case "event"
                    table = "eventattr6"
                    parentColumn = "parent"
                    keyColumn = "eventid"
                    otype = clsMarsScheduler.enScheduleType.EVENTBASED
                Case "event-package"
                    table = "eventpackageattr"
                    parentColumn = "parent"
                    keyColumn = "eventpackid"
                    otype = clsMarsScheduler.enScheduleType.EVENTPACKAGE
                Case "folder"
                    table = "folders"
                    parentColumn = "parent"
                    keyColumn = "folderid"
                    otype = clsMarsScheduler.enScheduleType.FOLDER


                    If clsMarsUI.MainUI.IsMoveValid(nn.DataKey, parentid) = False Then
                        MessageBox.Show("Cannot move the folder as the destination is a subfolder of the source folder", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        e.Cancel = True
                    ElseIf nn.DataKey = parentid Then
                        MessageBox.Show("A folder cannot be moved into itself", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        e.Cancel = True
                    End If
                Case Else
                    e.Cancel = True
            End Select

            If clsMarsUI.candoRename(nn.Text, parentid, otype) = False Then
                MessageBox.Show("The destination folder already contains an item named '" & nn.Text & "'. The operation will be cancelled.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                e.Cancel = True
                Exit For
            ElseIf type <> "folder" And parentid = 0 Then
                MessageBox.Show("You cannot move this type of object to the root or into a package", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                e.Cancel = True
            End If

            ' id = nn.DataKey

            Dim SQL As String = "UPDATE " & table & " SET " & parentColumn & " = " & parentid & " WHERE " & keyColumn & " = " & id

            If e.Cancel = False Then clsMarsData.WriteData(SQL)
        Next

        If e.Cancel = False Then


            Dim tmp As AdvTree.Node = e.NewParentNode


            loadData(parentid)

            crumb.Items.Clear()

            For Each ff As frmMainWin In crdxCommon.gwindows
                Try
                    If ff.Tag <> Me.Tag Then
                        ff.loadData(ff.tvExplorer.SelectedNode.DataKey, True)
                        ff.buildCrumbBar()
                    End If
                Catch : End Try
            Next

            For Each nn As AdvTree.Node In e.Nodes
                Dim nx As AdvTree.Node = tvMain.FindNodeByDataKey(nn.DataKey)

                If nx IsNot Nothing Then
                    tvMain.SelectedNodes.Add(nx)
                End If
            Next

            Dim fl As AdvTree.Node = tvExplorer.FindNodeByDataKey(parentid)

            If fl IsNot Nothing Then
                tvExplorer.SelectedNode = fl
                fl.EnsureVisible()

                Do While fl.Parent IsNot Nothing
                    fl.Expand()

                    fl = fl.Parent
                Loop
            End If

            buildCrumbBar()
            updateCrumbs()
        End If
    End Sub

    Private Sub tvExplorer_Click(sender As System.Object, e As System.EventArgs) Handles tvExplorer.Click

    End Sub

    Private Sub btnSmartRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnSmartRefresh.Click
        Dim sType As String
        Dim nID As Integer
        Dim oReport As New clsMarsReport
        Bar1.AutoHide = False
        Dim I As Integer = 0

        For Each parentNode As DevComponents.AdvTree.Node In tvMain.Nodes
            For Each itemNode As DevComponents.AdvTree.Node In parentNode.Nodes
                clsMarsUI.MainUI.BusyProgress((I / parentNode.Nodes.Count) * 100, "Refreshing reports")
                sType = itemNode.Tag
                nID = itemNode.DataKey

                Select Case sType.ToLower
                    Case "report"
                        oReport.RefreshReport(nID)
                    Case "package"
                        oReport.RefreshPackage(nID)
                End Select

                I += 1
            Next
            I = 0
        Next
        clsMarsUI.MainUI.BusyProgress(100, "", True)
        Bar1.AutoHide = True
        MessageBox.Show("The requested operation completed successfully", Application.ProductName, _
        MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub btnSmartDisable_Click(sender As System.Object, e As System.EventArgs) Handles btnSmartDisable.Click, btnSmartEnable.Click
        Dim sType As String
        Dim nID As Integer
        Dim oReport As New clsMarsReport
        Dim sTemp As String
        Dim SQL As String
        Dim newstatus As Integer

        sTemp = tvExplorer.SelectedNode.Text

        Dim btn As DevComponents.DotNetBar.ButtonItem = CType(sender, DevComponents.DotNetBar.ButtonItem)

        If btn.Name = "btnSmartDisable" Then
            newstatus = 0
        Else

            If clsMarsScheduler.areAnyScheduleLocked = True Or clsMarsEvent.areEventsLocked = True Then
                MessageBox.Show("Sorry, cannot enable all schedules in the folder as some schedules are locked.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            newstatus = 1
        End If



        For Each o As DevComponents.AdvTree.Node In tvMain.Nodes
            If o.Parent Is Nothing Then
                For Each child As DevComponents.AdvTree.Node In o.Nodes
                    sType = child.Tag
                    nID = child.DataKey

                    Select Case sType.ToLower
                        Case "report"
                            SQL = "UPDATE ScheduleAttr SET " & _
                            "Status = " & newstatus & ", " & _
                            "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                            "WHERE ReportID = " & nID
                        Case "package"
                            SQL = "UPDATE ScheduleAttr SET " & _
                            "Status = " & newstatus & ", " & _
                            "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                            "WHERE PackID = " & nID
                        Case "automation"
                            SQL = "UPDATE ScheduleAttr SET " & _
                            "Status = " & newstatus & ", " & _
                            "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                            "WHERE AutoID = " & nID
                        Case "event"
                            SQL = "UPDATE EventAttr6 SET " & _
                            "Status = " & newstatus & ", " & _
                            "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                            "WHERE EventID = " & nID
                        Case "event-package"
                            SQL = "UPDATE scheduleattr SET " & _
                             "Status = " & newstatus & ", " & _
                             "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                             "WHERE EventPackID = " & nID
                    End Select

                    clsMarsData.WriteData(SQL)
                Next
            Else
                sType = o.Tag
                nID = o.DataKey

                Select Case sType.ToLower
                    Case "report"
                        SQL = "UPDATE ScheduleAttr SET " & _
                        "Status = " & newstatus & ", " & _
                        "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                        "WHERE ReportID = " & nID
                    Case "package"
                        SQL = "UPDATE ScheduleAttr SET " & _
                        "Status = " & newstatus & ", " & _
                        "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                        "WHERE PackID = " & nID
                    Case "automation"
                        SQL = "UPDATE ScheduleAttr SET " & _
                        "Status = " & newstatus & ", " & _
                        "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                        "WHERE AutoID = " & nID
                    Case "event"
                        SQL = "UPDATE EventAttr6 SET " & _
                        "Status = " & newstatus & ", " & _
                        "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                        "WHERE EventID = " & nID
                    Case "event-package"
                        SQL = "UPDATE scheduleattr SET " & _
                         "Status = " & newstatus & ", " & _
                         "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                         "WHERE EventPackID = " & nID
                End Select

                clsMarsData.WriteData(SQL)
            End If
        Next

        'oUI._RefreshView(Me)
        tvExplorer.SelectedNode = Nothing

        Dim nn As DevComponents.AdvTree.Node = tvExplorer.FindNodeByText(sTemp)

        If nn IsNot Nothing Then
            tvExplorer.SelectedNode = nn
        End If

        MessageBox.Show("The requested operation completed successfully", Application.ProductName, _
        MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub btnSmartEnable_Click(sender As System.Object, e As System.EventArgs) 'Handles btnSmartEnable.Click
        Dim sType As String
        Dim nID As Integer
        Dim oReport As New clsMarsReport
        Dim sTemp As String
        Dim SQL As String

        If clsMarsScheduler.areAnyScheduleLocked = True Or clsMarsEvent.areEventsLocked = True Then
            MessageBox.Show("Sorry, cannot enable all schedules in the folder as some schedules are locked.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If

        sTemp = tvExplorer.SelectedNode.Text

        For Each o As DevComponents.AdvTree.Node In tvMain.Nodes
            sType = getSelectedObjectTypeFromParent(o.Parent.Text)
            nID = o.DataKey

            Select Case sType.ToLower
                Case "report"
                    SQL = "UPDATE ScheduleAttr SET " & _
                    "Status = 1, " & _
                    "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                    "WHERE ReportID = " & nID
                Case "package"
                    SQL = "UPDATE ScheduleAttr SET " & _
                    "Status = 1, " & _
                    "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                    "WHERE PackID = " & nID
                Case "automation"
                    SQL = "UPDATE ScheduleAttr SET " & _
                    "Status = 1, " & _
                    "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                    "WHERE AutoID = " & nID
                Case "event"
                    SQL = "UPDATE EventAttr6 SET " & _
                    "Status = 1, " & _
                    "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                    "WHERE EventID = " & nID
                Case "event-package"
                    SQL = "UPDATE scheduleattr SET " & _
                     "Status = 1, " & _
                     "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                     "WHERE EventPackID = " & nID
            End Select

            clsMarsData.WriteData(SQL)
        Next

        'oUI._RefreshView(Me)
        tvExplorer.SelectedNode = Nothing

        Dim nn As DevComponents.AdvTree.Node = tvExplorer.FindNodeByText(sTemp)

        If nn IsNot Nothing Then
            tvExplorer.SelectedNode = nn
        End If

        MessageBox.Show("The requested operation completed successfully", Application.ProductName, _
        MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub btnSmartExecute_Click(sender As System.Object, e As System.EventArgs) Handles btnSmartExecute.Click
        Dim sType As String
        Dim nID As Integer
        Dim oReport As New clsMarsReport
        Dim oAuto As New clsMarsAutoSchedule
        Dim oEvent As New clsMarsEvent
        Dim oSchedule As New clsMarsScheduler

        If clsMarsScheduler.areAnyScheduleLocked = True Or clsMarsEvent.areEventsLocked = True Then
            MessageBox.Show("Sorry, cannot execute all schedules in the folder as some schedules are locked.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If

        For Each parentNode As DevComponents.AdvTree.Node In tvMain.Nodes
            For Each itemNode As DevComponents.AdvTree.Node In parentNode.Nodes
                sType = itemNode.Tag
                nID = itemNode.DataKey

                Select Case sType.ToLower
                    Case "report"
                        Dim oRs As ADODB.Recordset

                        oRs = clsMarsData.GetData("SELECT Dynamic FROM ReportAttr WHERE ReportID =" & nID)

                        Try
                            If oRs.EOF = False Then
                                If IsNull(oRs.Fields(0).Value) = "1" Then
                                    If oReport.RunDynamicSchedule(nID) = True Then
                                        oSchedule.SetScheduleHistory(True, , nID)
                                    Else
                                        oSchedule.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", nID)
                                    End If
                                Else
                                    If oReport.RunSingleSchedule(nID) = True Then
                                        oSchedule.SetScheduleHistory(True, , nID)
                                    Else
                                        oSchedule.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", nID)
                                    End If
                                End If
                            End If
                        Catch
                            If oReport.RunSingleSchedule(nID) = True Then
                                oSchedule.SetScheduleHistory(True, , nID)
                            Else
                                oSchedule.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", nID)
                            End If
                        End Try
                    Case "package"

                        If clsMarsData.IsScheduleDynamic(nID, "Package") = True Then
                            If oReport.RunDynamicPackageSchedule(nID, False) = True Then
                                oSchedule.SetScheduleHistory(True, "", , nID, , , , oReport.m_HistoryID)
                            Else
                                oSchedule.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", , nID, , , , oReport.m_HistoryID)
                            End If
                        Else
                            If oReport.RunPackageSchedule(nID) = True Then
                                oSchedule.SetScheduleHistory(True, , , nID, , , , oReport.m_HistoryID)
                            Else
                                oSchedule.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", , nID, , , , oReport.m_HistoryID)
                            End If
                        End If

                    Case "automation"
                        oAuto.ExecuteAutomationSchedule(nID)
                    Case "event"
                        oEvent.RunEvents6(nID)
                    Case "event-package"
                        oEvent.RunEventPackage(nID)
                End Select
            Next
        Next

        MessageBox.Show("The requested operation completed successfully", Application.ProductName, _
        MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub btnSmartProperties_Click(sender As System.Object, e As System.EventArgs) Handles btnSmartProperties.Click
        If clsMarsSecurity._HasGroupAccess("Smart Folder Mngmnt") = True Then
            Dim oForm As New frmSmartProp

            Dim nID As Integer = tvExplorer.SelectedNode.DataKey

            oForm.EditSmartFolder(nID)
        End If
    End Sub

    Private Sub btnSmartViewReport_Click(sender As System.Object, e As System.EventArgs) Handles btnSmartViewReport.Click
        Try
            Dim nID As Integer

            nID = tvExplorer.SelectedNode.DataKey

            Dim SmartSQL() As String

            SmartSQL = oUI.SmartQueryBuilder(nID)

            Dim oSmart As New frmSmartReport

            oSmart._RenderReport(SmartSQL, tvExplorer.SelectedNode.Text)
        Catch : End Try
    End Sub

    Private Sub btnSmartRename_Click(sender As System.Object, e As System.EventArgs) Handles btnSmartRename.Click
        tvExplorer.SelectedNode.BeginEdit()
    End Sub

    Private Sub btnSmartDelete_Click(sender As System.Object, e As System.EventArgs) Handles btnSmartDelete.Click
        'For Each nn As DevComponents.AdvTree.Node In tvMain.Nodes
        '    tvMain.SelectedNodes.Add(nn)
        'Next

        'DeleteDesktopItem(sender, e, False)
        If tvExplorer.SelectedNode Is Nothing Then Return

        If clsMarsSecurity._HasGroupAccess("Smart Folder Mngmnt") = True Then
            Dim nID As Integer = tvExplorer.SelectedNode.DataKey

            Dim oRes As DialogResult = MessageBox.Show("Delete the '" & tvExplorer.SelectedNode.Text & "' smart folder?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If oRes = DialogResult.Yes Then

                oUI.DropSmartFolder(nID)

                tvExplorer.SelectedNode.Remove()

                'tvMain.Nodes.Clear()
            End If
        End If
    End Sub

    Private Sub tvExplorer_GotFocus(sender As Object, e As System.EventArgs) Handles tvExplorer.GotFocus
        folderTree = tvExplorer
    End Sub

    Private Sub tvMain_GotFocus(sender As Object, e As System.EventArgs) Handles tvMain.GotFocus
        folderTree = tvMain
    End Sub

    Private Sub btnFolderRename_Click(sender As System.Object, e As System.EventArgs) Handles btnFolderRename.Click
        If folderTree.SelectedNode Is Nothing Then Return

        If isAllowedAccessToFolder(folderTree.SelectedNode.DataKey) = False Then
            Return
        End If


        folderTree.SelectedNode.BeginEdit()
    End Sub

    Private Sub btnRemoteDisconnect_Click(sender As System.Object, e As System.EventArgs) Handles btnRemoteDisconnect.Click
        crdxCommon.executingForm = Me

        Dim frm As frmRemoteAdmin = New frmRemoteAdmin

        frm.Disconnect()

        btnRemoteConnect.Enabled = True
        btnRemoteDisconnect.Enabled = False
    End Sub

    Private Sub ExpandableSplitter1_ExpandedChanged(sender As System.Object, e As DevComponents.DotNetBar.ExpandedChangeEventArgs) Handles ExpandableSplitter1.ExpandedChanged
        If ExpandableSplitter1.Expanded = True Then
            oUI.SaveRegistry("ExpandSplitter", 1)
        Else
            oUI.SaveRegistry("ExpandSplitter", 0)
        End If
    End Sub



    Private Sub frmMainWin_Resize(sender As Object, e As System.EventArgs) Handles Me.Resize
        If IsFormLoaded = False Then Return

        Select Case Me.WindowState
            Case FormWindowState.Maximized
                oUI.SaveRegistry("MainWindowState", "Maximized")
            Case FormWindowState.Normal
                oUI.SaveRegistry("MainWindowState", "Normal")

                oUI.SaveRegistry("frmWindow", Me.Width & "," & Me.Height)
            Case FormWindowState.Minimized
                oUI.SaveRegistry("MainWindowState", "Minimized")
        End Select
    End Sub

    Private Sub frmMainWin_ResizeEnd(sender As Object, e As System.EventArgs) Handles Me.ResizeEnd
        Try
            If IsFormLoaded = False Then Return

            oUI.SaveRegistry("frmWindow", Me.Width & "," & Me.Height)
        Catch : End Try
    End Sub
    Private Sub btnDetailStyle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDetailStyle.Click
        saveViewStyleForFolder("tree")

        btnDetailStyle.Checked = True
        viewStyle = AdvTree.eView.Tree
        tvExplorer_AfterNodeSelect(Nothing, Nothing)
        '  clsMarsUI.MainUI.SaveRegistry("WindowViewStyle", "tree")
        btnTileStyle.Checked = False
        slSize.Enabled = False
    End Sub
    Private Sub btnTileStyle_Click(sender As System.Object, e As System.EventArgs) Handles btnTileStyle.Click
        btnDetailStyle.Checked = False
        btnTileStyle.Checked = True
        slSize.Enabled = True
        viewStyle = AdvTree.eView.Tile

        saveViewStyleForFolder("tile")

        tvMain.Columns.Clear()

        tvMain.GridColumnLines = False
        ' tvMain.Refresh()

        '  clsMarsUI.MainUI.SaveRegistry("WindowViewStyle", "tile")

        If tvExplorer.SelectedNode Is Nothing Then
            setIconSize()
        Else
            setIconSize(tvExplorer.SelectedNode.DataKey)
        End If

        tvExplorer_AfterNodeSelect(Nothing, Nothing)

    End Sub

    Sub saveViewStyleForFolder(style As String)
        '//save info for this window
        Try
            Dim folderID As Integer = 0

            If tvExplorer.SelectedNode IsNot Nothing Then
                folderID = tvExplorer.SelectedNode.DataKey
            ElseIf crumb.SelectedItem IsNot Nothing Then
                folderID = crumb.SelectedItem.Tag
            Else
                Return
            End If

            Dim fldRow As DataRow = Nothing

            For Each rr As DataRow In dtFolderSettings.Rows
                If rr("folderid") = folderID Then
                    fldRow = rr
                    Exit For
                End If
            Next

            If fldRow IsNot Nothing Then
                Dim row As DataRow = fldRow
                row("viewstyle") = style
                row("iconsize") = iconSize.Width
            Else
                Dim row As DataRow = dtFolderSettings.Rows.Add
                row("folderid") = folderID
                row("viewstyle") = style
                row("iconsize") = iconSize.Width
            End If
        Catch : End Try
    End Sub

    Function getViewStyleForFolder(folderID As Integer) As AdvTree.eView
        Try
            '//save info for this window
            Dim style As String = ""
            Dim sz As Integer = 32
            If folderID = 0 Then Return AdvTree.eView.Tile


            For Each row As DataRow In dtFolderSettings.Rows
                If row("folderid") = folderID Then
                    style = row("viewstyle")
                    sz = row("iconsize")
                    iconSize = New Size(sz, sz)
                    Exit For
                End If
            Next

            If style = "" Then style = clsMarsUI.MainUI.ReadRegistry("WindowViewStyle", "tile")


            If style = "tile" Then
                btnTileStyle.Checked = True
                btnDetailStyle.Checked = False
                tvMain.View = AdvTree.eView.Tile
                tvMain.Columns.Clear()
                tvMain.GridColumnLines = False
                setIconSize(folderID)
                Return AdvTree.eView.Tile
            Else
                tvMain.View = AdvTree.eView.Tree
                btnTileStyle.Checked = False
                btnDetailStyle.Checked = True
                Return AdvTree.eView.Tree
            End If
        Catch ex As Exception
            Return AdvTree.eView.Tile
        End Try
    End Function

    Private Sub tvMain_ColumnResized(sender As Object, e As System.EventArgs) Handles tvMain.ColumnResized
        Try
            '//save info for this window
            Dim folderID As Integer = 0

            Dim colx As DevComponents.AdvTree.ColumnHeader = CType(sender, DevComponents.AdvTree.ColumnHeader)


            If tvExplorer.SelectedNode IsNot Nothing Then
                folderID = tvExplorer.SelectedNode.DataKey
            ElseIf crumb.SelectedItem IsNot Nothing Then
                folderID = crumb.SelectedItem.Tag
            Else
                Return
            End If


            For Each nn As AdvTree.Node In tvMain.Nodes
                For Each cc As AdvTree.ColumnHeader In nn.NodesColumns
                    If cc.Text = colx.Text Then
                        cc.Width.Absolute = colx.Width.Absolute
                    End If
                Next
            Next
            '//delete current setting for this folder (shit)
            Dim rows() As DataRow = dtcolumnFolderSettings.Select("folderid = " & folderID)

            If rows IsNot Nothing Then
                For Each row As DataRow In rows
                    dtcolumnFolderSettings.Rows.Remove(row)
                Next

                For Each parentnode As AdvTree.Node In tvMain.Nodes
                    For Each col As AdvTree.ColumnHeader In parentnode.NodesColumns
                        Dim row As DataRow = dtcolumnFolderSettings.Rows.Add
                        row("folderid") = folderID
                        row("nodetext") = parentnode.Text
                        row("columnname") = col.Text
                        row("width") = col.Width.Absolute
                    Next
                Next
            End If
        Catch : End Try
    End Sub

    Function getColumnWidth(folderID As Integer, nodeText As String, columnName As String) As Integer
        Try
            If dtcolumnFolderSettings Is Nothing Then Return 0

            ' Dim rows() As DataRow = dtcolumnFolderSettings.Select("folderid =" & folderID & " AND columnName ='" & SQLPrepare(columnName) & "'")

            For Each row As DataRow In dtcolumnFolderSettings.Rows
                If row("folderid") = folderID And row("columnname") = columnName And row("nodetext") = nodeText Then
                    Return row("width")
                End If
            Next

            Return 150
        Catch ex As Exception
            Return 150
        End Try
        'If rows IsNot Nothing AndAlso rows.Length > 0 Then
        '    Dim row As DataRow = rows(0)

        '    Return row("width")
        'Else
        '    Return 0
        'End If
    End Function

    Private Sub dnbRight_AutoHideChanged(sender As Object, e As System.EventArgs) Handles dnbRight.AutoHideChanged
        clsMarsUI.MainUI.SaveRegistry("AutoHideDNBRight", Convert.ToInt32(dnbRight.AutoHide))

        If dnbRight.AutoHide Then
            tmSchedulerInfo.Stop()
        Else
            tmSchedulerInfo.Start()
        End If
    End Sub

    Private Sub dnbRight_BeforeAutoHideDisplayed(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles dnbRight.BeforeAutoHideDisplayed
        tmSchedulerInfo.Start()
    End Sub

    Private Sub dnbRight_BeforeAutoHideHidden(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles dnbRight.BeforeAutoHideHidden
        tmSchedulerInfo.Stop()
    End Sub

    Private Sub dnbRight_DockTabChange(sender As Object, e As DevComponents.DotNetBar.DockTabChangeEventArgs) Handles dnbRight.DockTabChange
        clsMarsUI.MainUI.SaveRegistry("DNBRightSelectedDockTab", dnbRight.SelectedDockTab)
    End Sub

    Private Sub btnSystemMonitorLarge_Click(sender As System.Object, e As System.EventArgs) Handles btnSystemMonitorLarge.Click
        btnSystemMonitor_Click(sender, e)
    End Sub


    Private Sub btnPasteItem_Click(sender As System.Object, e As System.EventArgs) Handles btnPasteItem.Click, btnSinglePaste.Click, _
        btnpackPaste.Click, btnautoPaste.Click, btneventPaste.Click, btneventpackPaste.Click

        If gClipboard Is Nothing Then Return

        pasteMenuHandler(gClipboard(0))

    End Sub


    Private Sub RibbonControl1_ExpandedChanged(sender As Object, e As System.EventArgs) Handles RibbonControl1.ExpandedChanged
        clsMarsUI.MainUI.SaveRegistry("RBCStatus", Convert.ToInt16(RibbonControl1.Expanded))
    End Sub

    Private Sub mnuDesktopPaste_Click(sender As System.Object, e As System.EventArgs) Handles mnuDesktopPaste.Click
        btnPasteItem_Click(sender, e)
    End Sub

    Private Sub btneventBasedSchedules_Click(sender As System.Object, e As System.EventArgs) Handles btneventBasedSchedules.Click

    End Sub

    Private Sub btneventEnabled_Click(sender As System.Object, e As System.EventArgs) Handles btneventEnabled.Click
        If tvMain.SelectedNode Is Nothing Then Return

        Dim oData As clsMarsData = New clsMarsData
        Dim SQL As String
        Dim nValue As Integer
        Dim nID As Integer = tvMain.SelectedNode.DataKey
        Dim olsv As AdvTree.Node

        olsv = tvMain.SelectedNode

        If IO.File.Exists(IO.Path.Combine(sAppPath, "regwizx.exe")) = False Then
            If clsMarsEvent.isEventLocked(nID) Then
                showViolatorMessage()
                Return
            End If
        End If

        Select Case btneventEnabled.Checked
            Case False
                nValue = 1
                btneventEnabled.Checked = True
            Case True
                nValue = 0
                btneventEnabled.Checked = False
        End Select

        SQL = "UPDATE EventAttr6 SET " & _
            "Status = " & nValue & "," & _
            "DisabledDate = '" & ConDateTime(Date.Now) & "'" & _
            " WHERE EventID =" & nID

        clsMarsData.WriteData(SQL)

        If nValue = 1 Then
            clsMarsAudit._LogAudit(olsv.Text, clsMarsAudit.ScheduleType.EVENTS, clsMarsAudit.AuditAction.ENABLE)
        Else
            clsMarsAudit._LogAudit(olsv.Text, clsMarsAudit.ScheduleType.EVENTS, clsMarsAudit.AuditAction.DISABLE)
        End If

        Dim evt As EventBased = New EventBased(nID)

        olsv.Image = resizeImage(evt.eventImage, iconSize)

        If evt.Status = False Then
            olsv.Image = MakeGrayscale3(olsv.Image)
        End If
        ' loadData(tvExplorer.SelectedNode.DataKey)
    End Sub

    Private Sub btneventpackEnabled_Click(sender As System.Object, e As System.EventArgs) Handles btneventpackEnabled.Click
        Dim oData As clsMarsData = New clsMarsData
        Dim SQL As String
        Dim nValue As Integer
        Dim nPackID As Integer = tvMain.SelectedNode.DataKey
        Dim olsv As AdvTree.Node

        olsv = tvMain.SelectedNode

        If IO.File.Exists(IO.Path.Combine(sAppPath, "regwizx.exe")) = False Then
            If clsMarsScheduler.isScheduleLocked(nPackID, clsMarsScheduler.enScheduleType.EVENTPACKAGE) Then
                showViolatorMessage()
                Return
            End If
        End If

        Select Case btneventpackEnabled.Checked
            Case False
                nValue = 1
                btneventpackEnabled.Checked = True
            Case True
                nValue = 0
                btneventpackEnabled.Checked = False
        End Select

        SQL = "UPDATE ScheduleAttr SET " & _
            "Status = " & nValue & "," & _
            "DisabledDate = '" & ConDateTime(Date.Now) & "'" & _
            " WHERE EventPackID = " & nPackID

        clsMarsData.WriteData(SQL)

        Dim oAction As clsMarsAudit.AuditAction

        If nValue = 0 Then
            oAction = clsMarsAudit.AuditAction.DISABLE
        Else
            oAction = clsMarsAudit.AuditAction.ENABLE
        End If

        clsMarsAudit._LogAudit(olsv.Text, clsMarsAudit.ScheduleType.EVENTPACKAGE, oAction)


        Dim evp As EventBasedPackage = New EventBasedPackage(nPackID)

        olsv.Image = resizeImage(evp.packageImage, iconSize)

        If evp.Status = False Then
            olsv.Image = MakeGrayscale3(olsv.Image)
        End If

        If tvExplorer.SelectedNode IsNot Nothing Then
            For Each nn As AdvTree.Node In tvExplorer.SelectedNode.Nodes
                If nn.DataKey = nPackID Then
                    nn.Image = resizeImage(evp.packageImage, 16, 16)
                    Exit For
                End If
            Next
        End If
        '  loadData(tvExplorer.SelectedNode.DataKey)
    End Sub


    Private Sub cmbCollaborationServer_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cmbCollaborationServer.SelectedIndexChanged
        '//get the id of the selected
        If IsFormLoaded = False Then Return

        If tvMain.SelectedNode Is Nothing Then Return

        Dim collaboID As Integer = 0

        If cmbCollaborationServer.Text = "DEFAULT" Then
            collaboID = 0
        Else
            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT collaboID from collaboratorsAttr WHERE serverName ='" & SQLPrepare(cmbCollaborationServer.Text) & "'")

            If oRs Is Nothing Then
                collaboID = 0
            ElseIf oRs.EOF = True Then
                collaboID = 0
            Else
                collaboID = IsNull(oRs(0).Value, 0)
            End If
        End If


        For Each nn As AdvTree.Node In tvMain.SelectedNodes
            Dim type As String = nn.Tag
            Dim ID As Integer = nn.DataKey
            Dim SQL As String = "UPDATE scheduleAttr SET collaborationServer =" & collaboID & " WHERE "

            Select Case type.ToLower
                Case "report"
                    SQL &= " reportid = " & ID
                Case "package"
                    SQL &= " packid = " & ID
                Case "automation"
                    SQL &= " autoid = " & ID
                Case "event-package", "event package"
                    SQL &= " eventpackid = " & ID
                Case Else
                    Continue For
            End Select

            clsMarsData.WriteData(SQL)
        Next

        MessageBox.Show("The requested task has completed successfully.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub



    Public dimmer As frmDimmer

    Private Sub Bar1_AutoHideChanged(sender As Object, e As System.EventArgs) Handles Bar1.AutoHideChanged
        Try
            If Bar1.AutoHide = False Then
                pgx.Visible = True
                'changeUIStatus(False)
                crdxCommon.executingForm = Me
            Else
                pgx.Visible = False
                'changeUIStatus(True)

            End If
        Catch : End Try
    End Sub

    Sub changeUIStatus(status As Boolean)
        If IsFormLoaded Then
            If status = False Then
                dimmer = New frmDimmer
                dimmer.SetBounds(Me.Left + 8, Me.Top + 8, Me.ClientRectangle.Width, Me.ClientRectangle.Height)
                dimmer.Height = Me.Height - 80
                dimmer.Owner = Me
                dimmer.ShowInTaskbar = False
                dimmer.Opacity = 0
                dimmer.Show()

                For I As Double = 0 To 0.6 Step 0.01
                    dimmer.Opacity = I
                    Application.DoEvents()
                    System.Threading.Thread.Sleep(10)
                Next


            Else
                Dim I As Double = 0.6

                Do
                    I = I - 0.01
                    dimmer.Opacity = I
                    Application.DoEvents()
                    System.Threading.Thread.Sleep(10)
                Loop Until I <= 0

                dimmer.Hide()
                dimmer = Nothing
                Me.Show()
            End If
        End If
        'Me.RibbonControl1.Enabled = status
        'Me.tvExplorer.Enabled = status
        'Me.tvMain.Enabled = status
        'Me.dnbRight.Enabled = status
    End Sub


    Private Sub btnQuickEmail_Click(sender As System.Object, e As System.EventArgs) Handles btnQuickEmail.Click
        If MailType = gMailType.NONE Then
            MessageBox.Show("You must set up your email settings in Options before you can send an email!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        Else
            Dim emailSend As frmTaskEmail = New frmTaskEmail

            emailSend.quickEmailTask()
        End If
    End Sub

    Private Sub btnReportDesign_Click(sender As System.Object, e As System.EventArgs) Handles btnReportDesign.Click, btnPackedDesign.Click
        If tvMain.SelectedNode Is Nothing Then Return

        Dim rptNode As AdvTree.Node = tvMain.SelectedNode
        Dim rptID As Integer = rptNode.DataKey
        Dim rptObj As cssreport = New cssreport(rptID)

        rptObj.editReportDesign()

    End Sub

    Private Sub btnOpenPackage_Click(sender As System.Object, e As System.EventArgs) Handles btnOpenPackage.Click
        If tvMain.SelectedNode Is Nothing Then Return

        Dim packageViewer As frmOpenPackage = New frmOpenPackage
        Dim packNode As AdvTree.Node = tvMain.SelectedNode
        Dim pack As Package = New Package(packNode.DataKey)

        packageViewer.openPackage(packNode.Text, packNode.DataKey, pack.isDataDriven, pack.isDynamic)

        pack = Nothing
    End Sub


    Private Sub dnbRight_Resize(sender As Object, e As System.EventArgs) Handles dnbRight.Resize
        clsMarsUI.MainUI.SaveRegistry("dnbRightWidth", dnbRight.Width)
    End Sub

    Private Sub btnUpdateCheck_Click(sender As System.Object, e As System.EventArgs) Handles btnUpdateCheck.Click
        Cursor = Cursors.WaitCursor

        Dim oCheck As clsSystemTools = New clsSystemTools

        oCheck.checkforUpdates(gDownloadUrl, True)

        Cursor = Cursors.Default
    End Sub


    Private Sub tmCheckUpdates_Tick(sender As Object, e As System.EventArgs) Handles tmCheckUpdates.Tick
        tmCheckUpdates.Stop()
        tmCheckUpdates.Enabled = False
        Dim oCheck As clsSystemTools = New clsSystemTools

        oCheck.checkforUpdates(gDownloadUrl, False)
    End Sub


    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        frmMain.m_UserCancel = True
    End Sub

    Private Sub btnPackagedProperties_Click(sender As System.Object, e As System.EventArgs) Handles btnPackagedProperties.Click
        Dim rptID As Integer = tvMain.SelectedNode.DataKey
        Dim prop As frmPackedReport = New frmPackedReport

        prop.EditReport(rptID)

        editItemRow(rptID, clsMarsScheduler.enScheduleType.REPORT)

        'If tvExplorer.SelectedNode IsNot Nothing Then
        '    loadPackageContents()
        'End If

        'Dim node As AdvTree.Node = tvMain.FindNodeByDataKey(rptID)

        'If node IsNot Nothing Then
        '    tvMain.SelectedNode = node
        '    node.EnsureVisible()
        'End If
    End Sub

    Private Sub btnPackedStatus_Click(sender As System.Object, e As System.EventArgs) Handles btnPackedStatus.Click
        If tvMain.SelectedNode Is Nothing Then Return

        Dim oData As clsMarsData = New clsMarsData
        Dim SQL As String
        Dim nValue As Integer
        Dim nReportID As Integer = tvMain.SelectedNode.DataKey

        If btnPackedStatus.Checked Then
            '//disable the report
            nValue = 0
        Else
            '//enable the report
            nValue = 1
        End If

        SQL = "UPDATE PackagedReportAttr SET " & _
            "Status = " & nValue & "," & _
            "DisabledDate = '" & ConDateTime(Date.Now) & "'" & _
            " WHERE ReportID = " & nReportID

        clsMarsData.WriteData(SQL)

        If nValue = 0 Then
            tvMain.SelectedNode.Image = MakeGrayscale3(tvMain.SelectedNode.Image)
        Else
            tvMain.SelectedNode.Image = resizeImage(My.Resources.document_chart2, tvMain.SelectedNode.Image.Size)
        End If

        If nValue = 1 Then
            clsMarsAudit._LogAudit(tvMain.SelectedNode.Text, clsMarsAudit.ScheduleType.PACKED, clsMarsAudit.AuditAction.ENABLE)
        Else
            clsMarsAudit._LogAudit(tvMain.SelectedNode.Text, clsMarsAudit.ScheduleType.PACKED, clsMarsAudit.AuditAction.DISABLE)
        End If
    End Sub


    Private Sub btnPackedExecute_Click(sender As System.Object, e As System.EventArgs) Handles btnPackedExecute.Click
        Dim oPackage As clsMarsReport = New clsMarsReport
        Dim oSchedule As clsMarsScheduler = New clsMarsScheduler
        Dim Success As Boolean
        Dim nPackID As Integer
        Dim oExecute As clsMarsThreading = New clsMarsThreading

        crdxCommon.executingForm = Me

        Dim nReportID As Integer = tvMain.SelectedNode.DataKey
        Dim rpt As cssreport = New cssreport(nReportID)

        nPackID = rpt.packid

        If clsMarsScheduler.isScheduleLocked(nPackID, clsMarsScheduler.enScheduleType.PACKAGE) = True Then
            showViolatorMessage()
            Return
        End If

        oExecute.xPackID = nPackID
        oExecute.xReportID = nReportID
        AppStatus(True)
        oExecute.PackageScheduleThread()
        AppStatus(False)

        clsMarsAudit._LogAudit(tvMain.SelectedNode.Text, clsMarsAudit.ScheduleType.PACKAGE, clsMarsAudit.AuditAction.EXECUTE)
    End Sub

    Private Sub btnPackedDelete_Click(sender As System.Object, e As System.EventArgs) Handles btnPackedDelete.Click

        Dim response As DialogResult

        If tvMain.SelectedNodes.Count = 0 Then
            Return
        ElseIf tvMain.SelectedNodes.Count = 1 Then
            response = MessageBox.Show("Delete the " & tvMain.SelectedNode.Text & " report?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        Else
            response = MessageBox.Show("Delete the selected " & tvMain.SelectedNodes.Count & " reports from the package?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        End If

        If response = Windows.Forms.DialogResult.No Then Return

        Dim itemsToRemove As ArrayList = New ArrayList

        For Each nn As AdvTree.Node In tvMain.SelectedNodes
            Dim nID As Integer = nn.DataKey
            Dim oData As clsMarsData = New clsMarsData
            Dim oRs As ADODB.Recordset
            Dim SQL As String
            Dim packID As Integer
            Dim rpt As cssreport = New cssreport(nID)

            packID = rpt.packid

            If packID <> 0 Then
                oUI.DeleteReport(nID, packID)
            End If

            itemsToRemove.Add(nn)
        Next

        For Each nn As AdvTree.Node In itemsToRemove
            tvMain.Nodes.Remove(nn)
        Next

        itemsToRemove = Nothing


        If tvExplorer.SelectedNode IsNot Nothing Then
            Dim packid As Integer = tvExplorer.SelectedNode.DataKey
            Dim pack As Package = New Package(packid)

            tvExplorer.SelectedNode.Text = pack.packageName & " (" & pack.totalReportsLive & ")"
        End If
    End Sub

    Private Sub btnPackedAdhoc_Click(sender As System.Object, e As System.EventArgs) Handles btnPackedAdhoc.Click
        If tvMain.SelectedNode Is Nothing Then Return

        Try
            Dim nID As Integer

            clsMarsUI.MainUI.BusyProgress(20, "Collecting data...")

            nID = tvMain.SelectedNode.DataKey

            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT CachePath FROM ReportAttr WHERE ReportID =" & nID)

            If oRs IsNot Nothing Then
                If oRs.EOF = False Then
                    Dim path As String = IsNull(oRs(0).Value, "")

                    clsMarsUI.MainUI.BusyProgress(60, "Opening report...")
#If CRYSTAL_VER < 11.6 Then
                    ' Dim oRpt As CRAXDRT.Report = oApp.OpenReport(path)
#Else
                    Dim oRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = New CrystalDecisions.CrystalReports.Engine.ReportClass
                    oRpt.FileName = path
                    oRpt.Load(path, CrystalDecisions.Shared.OpenReportMethod.OpenReportByDefault)

                                While oRpt.IsLoaded = False
                                    _Delay(1)
                                End While
#End If
                    Dim oPrev As frmPreview = New frmPreview

                    clsMarsUI.MainUI.BusyProgress(90, "Rendering report...")

                    '   oPrev.PreviewReport(oRpt, False)

                    clsMarsUI.MainUI.BusyProgress(100, , True)
                End If
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            clsMarsUI.MainUI.BusyProgress(100, , True)
        End Try
    End Sub


    Private Sub btnPackedReportMenu_PopupOpen(sender As Object, e As DevComponents.DotNetBar.PopupOpenEventArgs) Handles btnPackedReportMenu.PopupOpen
        If tvMain.SelectedNode Is Nothing Then Return

        Dim reportID As Integer = tvMain.SelectedNode.DataKey
        Dim rpt As cssreport = New cssreport(reportID)

        btnPackedStatus.Checked = rpt.packedStatus
    End Sub

    Private Sub btnDashboard_Click(sender As System.Object, e As System.EventArgs) Handles btnDashboard.Click, btnDashboardA.Click
        If gConType <> "DAT" Then
            Dim p As Process = New Process
            p.StartInfo.FileName = IO.Path.Combine(sAppPath, "Dashboard\dashboard.exe")

            p.Start()
        Else
            MessageBox.Show("The SQL-RD Dashboard is currently not compatible with SQL-RD systems running using the 'DAT' storage option. Please switch to LOCAL SQL SERVER or ODBC storage to use the SQL-RD Dashboard.", Application.ProductName, _
                              MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub tmClick_Elapsed(sender As Object, e As System.Timers.ElapsedEventArgs) Handles tmClick.Elapsed
        btnDateAndTime.Text = Date.Now
        btnDateAndTime.Refresh()
    End Sub

    Private Sub btnProfServices_Click(sender As System.Object, e As System.EventArgs) Handles btnProfServices.Click
        Process.Start("http://www.christiansteven.com/professional-services.html")
    End Sub

    Private Sub ButtonItem1_Click_1(sender As System.Object, e As System.EventArgs) Handles btnUnifyFolderStyles.Click
        If tvExplorer.SelectedNode Is Nothing Then Return

        Dim res As DialogResult = MessageBox.Show("Are you sure you would like to set all folders to the current folder's style?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If res = Windows.Forms.DialogResult.No Then Return

        Try
            Dim nn As AdvTree.Node = tvExplorer.SelectedNode
            Dim origin As Integer = nn.DataKey
            Dim style As String = "tile"
            Dim iconSize As String = 32


            Dim rx As DataRow
            For Each xx As DataRow In dtFolderSettings.Select
                If xx("folderid") = origin Then
                    rx = xx
                    Exit For
                End If
            Next

            If rx Is Nothing Then Return

            style = rx("viewstyle")
            iconSize = rx("iconsize")


            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT folderid FROM folders")

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    Dim folderID As Integer = oRs(0).Value

                    If folderID <> origin Then
                        '//save the style
                        Try
                            Dim fldRow As DataRow = Nothing

                            For Each rr As DataRow In dtFolderSettings.Rows
                                If rr("folderid") = folderID Then
                                    fldRow = rr
                                    Exit For
                                End If
                            Next

                            If fldRow IsNot Nothing Then
                                Dim row As DataRow = fldRow
                                row("viewstyle") = style
                                row("iconsize") = iconSize
                            Else
                                Dim row As DataRow = dtFolderSettings.Rows.Add
                                row("folderid") = folderID
                                row("viewstyle") = style
                                row("iconsize") = iconSize
                            End If
                        Catch : End Try


                        '//delete current setting for this folder (shit)
                        Dim rows() As DataRow = dtcolumnFolderSettings.Select("folderid = " & folderID)

                        If rows IsNot Nothing Then
                            For Each row As DataRow In rows
                                dtcolumnFolderSettings.Rows.Remove(row)
                            Next

                            For Each parentnode As AdvTree.Node In tvMain.Nodes
                                For Each col As AdvTree.ColumnHeader In parentnode.NodesColumns
                                    Dim row As DataRow = dtcolumnFolderSettings.Rows.Add
                                    row("folderid") = folderID
                                    row("nodetext") = parentnode.Text
                                    row("columnname") = col.Text
                                    row("width") = col.Width.Absolute
                                Next
                            Next
                        End If
                    End If

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If
        Catch : End Try
    End Sub


    Private Function isAllowedAccessToFolder() As Boolean
        If tvExplorer.SelectedNode Is Nothing Then Return True

        If m_allowedFolders IsNot Nothing AndAlso m_isGroupFolderAccessRestricted Then
            Dim folderID As Integer = tvExplorer.SelectedNode.DataKey

            If m_allowedFolders.Contains(folderID) = False Then
                MessageBox.Show("Access denied to this folder. Please contact your administrator.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)

                Return False
            Else
                Return True
            End If
        Else
            Return True
        End If
    End Function

    Private Function isAllowedAccessToFolder(folderId As Integer) As Boolean

        If m_allowedFolders IsNot Nothing AndAlso m_isGroupFolderAccessRestricted Then

            If m_allowedFolders.Contains(folderID) = False Then
                MessageBox.Show("Access denied to this folder. Please contact your administrator.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)

                Return False
            Else
                Return True
            End If
        Else
            Return True
        End If
    End Function

    Private Sub btnBurstingPackage_Click(sender As Object, e As EventArgs) Handles btnBurstingPackage.Click

    End Sub

    Private Sub tvMain_Click(sender As Object, e As EventArgs) Handles tvMain.Click

    End Sub

    Private Sub btnConsole_Click(sender As Object, e As EventArgs) Handles btnConsole.Click
        Dim unifiedLogin As Boolean = Convert.ToInt16(clsMarsUI.MainUI.ReadRegistry("unifiedlogin", 0))

        If unifiedLogin = False Then
            Dim auth As frmRemoteLogin = New frmRemoteLogin

            If auth.AdminCheck = True Then
                Dim console As frmSystemConsole = New frmSystemConsole

                console.Show()
            End If
        Else
            Dim user As cssusers.NTDomainUser = New cssusers.NTDomainUser(gUser)

            If user.userRole.ToLower <> "administrator" Then
                MessageBox.Show("The user you have selected is not a member of the Administrators group.", _
                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                Dim console As frmSystemConsole = New frmSystemConsole

                console.Show()
            End If
        End If
    End Sub

    Private Sub swFullSystemSearch_ValueChanged(sender As Object, e As EventArgs) Handles swFullSystemSearch.ValueChanged
        clsMarsUI.MainUI.SaveRegistry("FullSystemSearch", Convert.ToInt16(swFullSystemSearch.Value))

      
    End Sub

   
    Private Sub swFullSystemSearch_ValueChanging(sender As Object, e As EventArgs) Handles swFullSystemSearch.ValueChanging
        txtSearch.Text = ""
    End Sub

    Private Sub btnMovePackedReport_Click(sender As Object, e As EventArgs) Handles btnMovePackedReport.Click, btnMoveAuto.Click, btnMoveEvent.Click, btnMoveEventPackage.Click, btnMovePackage.Click, btnMoveSingle.Click
        moveObject()
    End Sub
End Class
