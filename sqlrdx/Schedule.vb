﻿Public Class Schedule
    Public m_scheduleid As Integer
    Dim concernedRow As ADODB.Recordset
    Dim m_Type As parentscheduleType
    Dim m_objectID As Integer
    Dim newSchedule As Boolean = False
    Dim WithEvents dtSchedule As DataTable
    Dim dateFormat As String = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToString
    Public Enum parentscheduleType As Integer
        REPORT = 0
        PACKAGE = 1
        AUTOMATION = 2
        EVENTBASED = 3
        EVENTBASEDPACKAGE = 4

        eventPACKAGE

    End Enum

    Public Enum childscheduleType As Integer
        SINGLEREPORT = 0
        DYNAMICSINGLE = 1
        DATADRIVENSINGLE = 2
        BURSTINGSINGLE = 3
        SINGLEPACKAGE = 4
        DYNAMICPACKAGE = 5
        DATADRIVENPACKAGE = 6
        EVENTBASEDPACKAGE = 7
        AUTOMATION = 8
    End Enum

    Private Function getRelevantColumn(ByVal type As parentscheduleType, ByRef tableName As String) As String
        Dim columnName As String = ""

        Select Case type
            Case parentscheduleType.AUTOMATION
                columnName = "autoid"
            Case parentscheduleType.EVENTBASED
                columnName = "eventid"
                tableName = "eventattr6"
            Case parentscheduleType.EVENTBASEDPACKAGE
                columnName = "eventpackid"
            Case parentscheduleType.PACKAGE
                columnName = "packid"
            Case parentscheduleType.REPORT
                columnName = "reportid"
        End Select

        Return columnName
    End Function

    Sub New(ByVal scheduleID As Integer)
        m_scheduleid = scheduleID

        concernedRow = clsMarsData.GetData("SELECT * FROM scheduleattr WHERE scheduleid = " & scheduleID)

    End Sub

    Public ReadOnly Property ID As Integer
        Get
            Return m_scheduleid
        End Get
    End Property

    Sub New(ByVal objectid As Integer, ByVal type As parentscheduleType)
        Dim columnName As String = ""
        Dim tableName As String = "scheduleattr"

        m_Type = type
        m_objectID = objectid

        columnName = getRelevantColumn(type, tableName)

        concernedRow = clsMarsData.GetData("SELECT * FROM scheduleattr WHERE " & columnName & " = " & objectid)

        If concernedRow IsNot Nothing AndAlso concernedRow.EOF = False Then
            m_scheduleid = concernedRow("scheduleid").Value
        Else
            m_scheduleid = 0
        End If
    End Sub

    Public ReadOnly Property reportid() As Integer
        Get
            Return concernedRow("reportid").Value
        End Get
       
    End Property

    Public ReadOnly Property packid() As Integer
        Get
            Return concernedRow("packid").Value
        End Get
       
    End Property

    Public ReadOnly Property autoid() As Integer
        Get
            Return concernedRow("autoid").Value
        End Get
      
    End Property
    Public ReadOnly Property scheduleID() As Integer
        Get
            Return m_scheduleid
        End Get
    End Property

    Public ReadOnly Property lastRunDuration As Double
        Get
            Try
                Dim fin As Date = lastRun
                Dim start As Date = lastRunStart

                Return fin.Subtract(start).TotalSeconds
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Public ReadOnly Property lastRunStart(Optional format As String = "") As String
        Get
            If format = "" Then
                format = dateFormat & " HH:mm:ss"
            End If

            Dim tableName As String = ""
            Dim columnName As String = getRelevantColumn(m_Type, tableName)
            Dim dateCol As String = ""
            Dim dtHistory As String

            If m_Type = parentscheduleType.EVENTBASED Then
                dtHistory = "eventhistory"
                dateCol = "startdate"
            Else
                dtHistory = "schedulehistory"
                dateCol = "startdate"
            End If

            Dim row As ADODB.Recordset = clsMarsData.GetData("SELECT TOP 1 * FROM " & dtHistory & " WHERE " & columnName & " = " & m_objectID & " ORDER BY " & dateCol & " DESC")

            If row IsNot Nothing AndAlso row.EOF = False Then
                Dim startdate As Date = row(dateCol).Value

                Return startdate.ToString(format)
            Else
                Return "Never Run"
            End If
        End Get
    End Property


    Public ReadOnly Property lastRun(Optional format As String = "") As String
        Get
            If format = "" Then
                format = dateFormat & " HH:mm:ss"
            End If

            Dim tableName As String = ""
            Dim columnName As String = getRelevantColumn(m_Type, tableName)
            Dim dateCol As String = ""
            Dim dtHistory As String

            If m_Type = parentscheduleType.EVENTBASED Then
                dtHistory = "eventhistory"
                dateCol = "lastfired"
            Else
                dtHistory = "schedulehistory"
                dateCol = "entrydate"
            End If

            Dim row As ADODB.Recordset = clsMarsData.GetData("SELECT TOP 1 * FROM " & dtHistory & " WHERE " & columnName & " = " & m_objectID & " ORDER BY " & dateCol & " DESC")

            If row IsNot Nothing AndAlso row.EOF = False Then
                Dim entrydate As Date = row(dateCol).Value

                Return entrydate.ToString(format)
            Else
                Return "Never Run"
            End If

        End Get
    End Property

    Public ReadOnly Property nextRun(Optional format As String = "") As String
        Get
            If format = "" Then
                format = dateFormat & " HH:mm:ss"
            End If

            Dim dt As Date = IsNull(concernedRow("nextrun").Value, Nothing)

            Return dt.ToString(format)
        End Get

    End Property

    Public ReadOnly Property lastResult() As String
        Get
            Dim tableName As String = ""
            Dim columnName As String = getRelevantColumn(m_Type, tableName)

            Dim dataCol As String = "success"

            '//Dim dv As DataView = New DataView(dtHistory, columnName & " = " & m_objectID, dataCol & " DESC", DataViewRowState.CurrentRows)

            Dim row As ADODB.Recordset = clsMarsData.GetData("SELECT TOP 1 * FROM schedulehistory WHERE " & columnName & " = " & m_objectID & " ORDER BY entrydate DESC")

            If row IsNot Nothing AndAlso row.EOF = False Then
                Dim success As Boolean = CType(row(dataCol).Value, Boolean)
                Dim msg As String = IsNull(row("errmsg").Value, "")

                If success = True Then
                    Return "Success"
                Else
                    Return "Failed: " & msg
                End If

                row.Close()
                row = Nothing
            Else
                Return "Never Run"
            End If

        End Get
    End Property

    Public Sub Dispose()
        Try
            Me.concernedRow.Close()
            concernedRow = Nothing
        Catch : End Try
    End Sub
    Public Property status() As Boolean
        Get
            Try
                Return IsNull(concernedRow("status").Value, True)
            Catch
                Return False
            End Try
        End Get
        Set(value As Boolean)
            clsMarsData.WriteData("UPDATE scheduleattr SET status =" & Convert.ToInt16(value) & " WHERE scheduleid =" & scheduleID)
        End Set
    End Property

    Public ReadOnly Property description() As String
        Get
            Return IsNull(concernedRow("description").Value, "")
        End Get

    End Property

    Public ReadOnly Property keywords() As String
        Get
            Return IsNull(concernedRow("keyword").Value, "")
        End Get

    End Property

    Public ReadOnly Property customcalendar() As String
        Get
            Return IsNull(concernedRow("calendarname").Value, "")
        End Get

    End Property

    Public ReadOnly Property exceptioncalendar() As String
        Get
            Return IsNull(concernedRow("exceptioncalendar").Value)
        End Get

    End Property

    Public ReadOnly Property useexception() As Boolean
        Get
            Return IsNull(concernedRow("useexception").Value, False)
        End Get

    End Property

    Public ReadOnly Property startDate(Optional format As String = "") As String
        Get
            If format = "" Then
                format = dateFormat
            End If

            Dim dt As Date = IsNull(concernedRow("startdate").Value, Nothing)

            Return dt.ToString(format)
        End Get

    End Property

    Public ReadOnly Property endDate(Optional format As String = "") As String
        Get
            If format = "" Then
                format = dateFormat
            End If

            Dim dt As Date = IsNull(concernedRow("enddate").Value, Nothing)

            If dt.Subtract(Now).TotalDays > 18250 Then
                Return "Never"
            Else
                Return dt.ToString(format)
            End If

        End Get

    End Property

    Public ReadOnly Property scheduleTime() As String
        Get
            Try
                Return IsNull(concernedRow("starttime").Value, Nothing)
            Catch
                Return Nothing
            End Try
        End Get

    End Property

    Public ReadOnly Property nexttorun() As Date
        Get
            Return IsNull(concernedRow("nextrun").Value, Nothing)
        End Get

    End Property




    Public ReadOnly Property repeat() As Boolean
        Get
            Return IsNull(concernedRow("repeat").Value, Nothing)
        End Get

    End Property

    Public ReadOnly Property repeatInterval() As Integer
        Get
            Return IsNull(concernedRow("repeatinterval").Value, Nothing)
        End Get

    End Property

    Public ReadOnly Property repeatUnit() As String
        Get
            Return IsNull(concernedRow("repeatunit").Value, "seconds")
        End Get

    End Property

    Public ReadOnly Property repeatUntil() As String
        Get
            Return IsNull(concernedRow("repeatuntil").Value, Nothing)
        End Get

    End Property

    Public ReadOnly Property frequency() As String
        Get
            Return IsNull(concernedRow("frequency").Value, Nothing)
        End Get

    End Property


End Class
