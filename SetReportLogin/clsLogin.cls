VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public Function SetReportLogin(oRpt As CRAXDRT.Report, sUser As String, sPassword As String, _
Optional sxServer As String = "", Optional sDatabase As String = "") As CRAXDRT.Report
    
    Dim I As Integer
    
10    For I = 1 To oRpt.Database.Tables.Count
20         Call oRpt.Database.Tables(I).SetLogOnInfo(sxServer, sDatabase, sUser, sPassword)
30     Next
    
End Function

