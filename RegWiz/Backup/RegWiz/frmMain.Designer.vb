<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblMessage = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Footer1 = New WizardFooter.Footer
        Me.Step1 = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        Me.Step2 = New System.Windows.Forms.Panel
        Me.chkEmail = New System.Windows.Forms.CheckBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Step3 = New System.Windows.Forms.Panel
        Me.chkAdmin = New System.Windows.Forms.CheckBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Step4 = New System.Windows.Forms.Panel
        Me.chkTerm = New System.Windows.Forms.CheckBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Step5 = New System.Windows.Forms.Panel
        Me.Label5 = New System.Windows.Forms.Label
        Me.Step6 = New System.Windows.Forms.Panel
        Me.btnCopy2 = New System.Windows.Forms.Button
        Me.btnCopy1 = New System.Windows.Forms.Button
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtFingerPrint = New System.Windows.Forms.TextBox
        Me.txtCustNo = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Step7 = New System.Windows.Forms.Panel
        Me.btnActivate = New System.Windows.Forms.Button
        Me.txtKey = New System.Windows.Forms.TextBox
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Step8 = New System.Windows.Forms.Panel
        Me.Label9 = New System.Windows.Forms.Label
        Me.Step9 = New System.Windows.Forms.Panel
        Me.txtLicenseNumber = New System.Windows.Forms.MaskedTextBox
        Me.btnRegister = New System.Windows.Forms.Button
        Me.txtLastName = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtCustNo1 = New System.Windows.Forms.TextBox
        Me.txtCompanyName = New System.Windows.Forms.TextBox
        Me.txtFirstName = New System.Windows.Forms.TextBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Step10 = New System.Windows.Forms.Panel
        Me.chkRestart = New System.Windows.Forms.CheckBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnNext = New System.Windows.Forms.Button
        Me.btnBack = New System.Windows.Forms.Button
        Me.btnFinish = New System.Windows.Forms.Button
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step1.SuspendLayout()
        Me.Step2.SuspendLayout()
        Me.Step3.SuspendLayout()
        Me.Step4.SuspendLayout()
        Me.Step5.SuspendLayout()
        Me.Step6.SuspendLayout()
        Me.Step7.SuspendLayout()
        Me.Step8.SuspendLayout()
        Me.Step9.SuspendLayout()
        Me.Step10.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.lblMessage)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(440, 77)
        Me.Panel1.TabIndex = 0
        '
        'lblMessage
        '
        Me.lblMessage.AutoSize = True
        Me.lblMessage.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(101, 22)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(96, 24)
        Me.lblMessage.TabIndex = 1
        Me.lblMessage.Text = "Message"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(3, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(92, 71)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Footer1
        '
        Me.Footer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Footer1.Location = New System.Drawing.Point(0, 387)
        Me.Footer1.Name = "Footer1"
        Me.Footer1.Size = New System.Drawing.Size(587, 18)
        Me.Footer1.TabIndex = 1
        '
        'Step1
        '
        Me.Step1.Controls.Add(Me.Label1)
        Me.Step1.Location = New System.Drawing.Point(3, 92)
        Me.Step1.Name = "Step1"
        Me.Step1.Size = New System.Drawing.Size(429, 289)
        Me.Step1.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(1, Byte), True)
        Me.Label1.Location = New System.Drawing.Point(9, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(416, 260)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = resources.GetString("Label1.Text")
        '
        'Step2
        '
        Me.Step2.Controls.Add(Me.chkEmail)
        Me.Step2.Controls.Add(Me.Label2)
        Me.Step2.Location = New System.Drawing.Point(3, 92)
        Me.Step2.Name = "Step2"
        Me.Step2.Size = New System.Drawing.Size(429, 289)
        Me.Step2.TabIndex = 4
        '
        'chkEmail
        '
        Me.chkEmail.Location = New System.Drawing.Point(12, 142)
        Me.chkEmail.Name = "chkEmail"
        Me.chkEmail.Size = New System.Drawing.Size(413, 60)
        Me.chkEmail.TabIndex = 1
        Me.chkEmail.Text = "I have ensured that my email system can receive all emails from ""christiansteven." & _
            "com"" irrespective of content"
        Me.chkEmail.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(9, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(416, 127)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = resources.GetString("Label2.Text")
        '
        'Step3
        '
        Me.Step3.Controls.Add(Me.chkAdmin)
        Me.Step3.Controls.Add(Me.Label3)
        Me.Step3.Location = New System.Drawing.Point(3, 92)
        Me.Step3.Name = "Step3"
        Me.Step3.Size = New System.Drawing.Size(429, 289)
        Me.Step3.TabIndex = 5
        '
        'chkAdmin
        '
        Me.chkAdmin.AutoSize = True
        Me.chkAdmin.Location = New System.Drawing.Point(4, 162)
        Me.chkAdmin.Name = "chkAdmin"
        Me.chkAdmin.Size = New System.Drawing.Size(324, 20)
        Me.chkAdmin.TabIndex = 1
        Me.chkAdmin.Text = "I am logged into this PC as the LOCAL Administrator"
        Me.chkAdmin.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoEllipsis = True
        Me.Label3.Location = New System.Drawing.Point(3, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(414, 118)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = resources.GetString("Label3.Text")
        '
        'Step4
        '
        Me.Step4.Controls.Add(Me.chkTerm)
        Me.Step4.Controls.Add(Me.Label4)
        Me.Step4.Location = New System.Drawing.Point(3, 92)
        Me.Step4.Name = "Step4"
        Me.Step4.Size = New System.Drawing.Size(429, 289)
        Me.Step4.TabIndex = 6
        '
        'chkTerm
        '
        Me.chkTerm.Location = New System.Drawing.Point(9, 187)
        Me.chkTerm.Name = "chkTerm"
        Me.chkTerm.Size = New System.Drawing.Size(408, 48)
        Me.chkTerm.TabIndex = 1
        Me.chkTerm.Text = "I am NOT using Terminal Services or Windows XP Remote Control to control the PC."
        Me.chkTerm.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoEllipsis = True
        Me.Label4.Location = New System.Drawing.Point(3, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(414, 121)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = resources.GetString("Label4.Text")
        '
        'Step5
        '
        Me.Step5.Controls.Add(Me.Label5)
        Me.Step5.Location = New System.Drawing.Point(3, 92)
        Me.Step5.Name = "Step5"
        Me.Step5.Size = New System.Drawing.Size(429, 289)
        Me.Step5.TabIndex = 7
        '
        'Label5
        '
        Me.Label5.AutoEllipsis = True
        Me.Label5.Location = New System.Drawing.Point(3, 9)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(414, 260)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = resources.GetString("Label5.Text")
        '
        'Step6
        '
        Me.Step6.Controls.Add(Me.btnCopy2)
        Me.Step6.Controls.Add(Me.btnCopy1)
        Me.Step6.Controls.Add(Me.Label8)
        Me.Step6.Controls.Add(Me.txtFingerPrint)
        Me.Step6.Controls.Add(Me.txtCustNo)
        Me.Step6.Controls.Add(Me.Label7)
        Me.Step6.Controls.Add(Me.Label6)
        Me.Step6.Location = New System.Drawing.Point(3, 92)
        Me.Step6.Name = "Step6"
        Me.Step6.Size = New System.Drawing.Size(429, 289)
        Me.Step6.TabIndex = 8
        '
        'btnCopy2
        '
        Me.btnCopy2.Image = CType(resources.GetObject("btnCopy2.Image"), System.Drawing.Image)
        Me.btnCopy2.Location = New System.Drawing.Point(256, 42)
        Me.btnCopy2.Name = "btnCopy2"
        Me.btnCopy2.Size = New System.Drawing.Size(75, 23)
        Me.btnCopy2.TabIndex = 3
        Me.btnCopy2.UseVisualStyleBackColor = True
        '
        'btnCopy1
        '
        Me.btnCopy1.Image = CType(resources.GetObject("btnCopy1.Image"), System.Drawing.Image)
        Me.btnCopy1.Location = New System.Drawing.Point(256, 6)
        Me.btnCopy1.Name = "btnCopy1"
        Me.btnCopy1.Size = New System.Drawing.Size(75, 23)
        Me.btnCopy1.TabIndex = 1
        Me.btnCopy1.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(6, 72)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(356, 217)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = resources.GetString("Label8.Text")
        '
        'txtFingerPrint
        '
        Me.txtFingerPrint.Location = New System.Drawing.Point(154, 43)
        Me.txtFingerPrint.Name = "txtFingerPrint"
        Me.txtFingerPrint.ReadOnly = True
        Me.txtFingerPrint.Size = New System.Drawing.Size(96, 23)
        Me.txtFingerPrint.TabIndex = 2
        '
        'txtCustNo
        '
        Me.txtCustNo.Location = New System.Drawing.Point(154, 6)
        Me.txtCustNo.Name = "txtCustNo"
        Me.txtCustNo.Size = New System.Drawing.Size(96, 23)
        Me.txtCustNo.TabIndex = 0
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(3, 49)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(135, 16)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Hardware Finger-Print"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(3, 9)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(76, 16)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Customer #"
        '
        'Step7
        '
        Me.Step7.Controls.Add(Me.btnActivate)
        Me.Step7.Controls.Add(Me.txtKey)
        Me.Step7.Controls.Add(Me.txtName)
        Me.Step7.Controls.Add(Me.Label10)
        Me.Step7.Controls.Add(Me.Label11)
        Me.Step7.Location = New System.Drawing.Point(3, 92)
        Me.Step7.Name = "Step7"
        Me.Step7.Size = New System.Drawing.Size(429, 289)
        Me.Step7.TabIndex = 9
        '
        'btnActivate
        '
        Me.btnActivate.Location = New System.Drawing.Point(217, 82)
        Me.btnActivate.Name = "btnActivate"
        Me.btnActivate.Size = New System.Drawing.Size(93, 28)
        Me.btnActivate.TabIndex = 2
        Me.btnActivate.Text = "&Activate"
        Me.btnActivate.UseVisualStyleBackColor = True
        '
        'txtKey
        '
        Me.txtKey.Location = New System.Drawing.Point(123, 43)
        Me.txtKey.Name = "txtKey"
        Me.txtKey.Size = New System.Drawing.Size(280, 23)
        Me.txtKey.TabIndex = 1
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(123, 6)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(280, 23)
        Me.txtName.TabIndex = 0
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(3, 49)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(87, 16)
        Me.Label10.TabIndex = 1
        Me.Label10.Text = "Activation Key"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(3, 9)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(98, 16)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Company Name"
        '
        'Step8
        '
        Me.Step8.Controls.Add(Me.Label9)
        Me.Step8.Location = New System.Drawing.Point(3, 92)
        Me.Step8.Name = "Step8"
        Me.Step8.Size = New System.Drawing.Size(429, 289)
        Me.Step8.TabIndex = 10
        '
        'Label9
        '
        Me.Label9.AutoEllipsis = True
        Me.Label9.Location = New System.Drawing.Point(3, 9)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(414, 201)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = resources.GetString("Label9.Text")
        '
        'Step9
        '
        Me.Step9.Controls.Add(Me.txtLicenseNumber)
        Me.Step9.Controls.Add(Me.btnRegister)
        Me.Step9.Controls.Add(Me.txtLastName)
        Me.Step9.Controls.Add(Me.Label14)
        Me.Step9.Controls.Add(Me.txtCustNo1)
        Me.Step9.Controls.Add(Me.txtCompanyName)
        Me.Step9.Controls.Add(Me.txtFirstName)
        Me.Step9.Controls.Add(Me.Label17)
        Me.Step9.Controls.Add(Me.Label16)
        Me.Step9.Controls.Add(Me.Label15)
        Me.Step9.Controls.Add(Me.Label13)
        Me.Step9.Controls.Add(Me.Label12)
        Me.Step9.Location = New System.Drawing.Point(3, 92)
        Me.Step9.Name = "Step9"
        Me.Step9.Size = New System.Drawing.Size(429, 289)
        Me.Step9.TabIndex = 11
        '
        'txtLicenseNumber
        '
        Me.txtLicenseNumber.Location = New System.Drawing.Point(123, 137)
        Me.txtLicenseNumber.Mask = ">aaaa-aaaa-aaaaaaaaaaaaaaaaaaaaaaaaaaaa"
        Me.txtLicenseNumber.Name = "txtLicenseNumber"
        Me.txtLicenseNumber.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.txtLicenseNumber.Size = New System.Drawing.Size(281, 23)
        Me.txtLicenseNumber.TabIndex = 6
        Me.txtLicenseNumber.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals
        '
        'btnRegister
        '
        Me.btnRegister.Location = New System.Drawing.Point(218, 195)
        Me.btnRegister.Name = "btnRegister"
        Me.btnRegister.Size = New System.Drawing.Size(93, 31)
        Me.btnRegister.TabIndex = 5
        Me.btnRegister.Text = "Register"
        Me.btnRegister.UseVisualStyleBackColor = True
        '
        'txtLastName
        '
        Me.txtLastName.Location = New System.Drawing.Point(123, 78)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(280, 23)
        Me.txtLastName.TabIndex = 1
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(9, 81)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(68, 16)
        Me.Label14.TabIndex = 1
        Me.Label14.Text = "Last Name"
        '
        'txtCustNo1
        '
        Me.txtCustNo1.Location = New System.Drawing.Point(123, 166)
        Me.txtCustNo1.Name = "txtCustNo1"
        Me.txtCustNo1.Size = New System.Drawing.Size(282, 23)
        Me.txtCustNo1.TabIndex = 4
        '
        'txtCompanyName
        '
        Me.txtCompanyName.Location = New System.Drawing.Point(123, 108)
        Me.txtCompanyName.Name = "txtCompanyName"
        Me.txtCompanyName.Size = New System.Drawing.Size(281, 23)
        Me.txtCompanyName.TabIndex = 2
        '
        'txtFirstName
        '
        Me.txtFirstName.Location = New System.Drawing.Point(123, 49)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(280, 23)
        Me.txtFirstName.TabIndex = 0
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(8, 169)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(76, 16)
        Me.Label17.TabIndex = 1
        Me.Label17.Text = "Customer #"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(8, 140)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(63, 16)
        Me.Label16.TabIndex = 1
        Me.Label16.Text = "License #"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(9, 111)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(98, 16)
        Me.Label15.TabIndex = 1
        Me.Label15.Text = "Company Name"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(9, 49)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(70, 16)
        Me.Label13.TabIndex = 1
        Me.Label13.Text = "First Name"
        '
        'Label12
        '
        Me.Label12.AutoEllipsis = True
        Me.Label12.Location = New System.Drawing.Point(3, 9)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(546, 19)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Please enter your registration details below:"
        '
        'Step10
        '
        Me.Step10.Controls.Add(Me.chkRestart)
        Me.Step10.Controls.Add(Me.Label18)
        Me.Step10.Location = New System.Drawing.Point(3, 92)
        Me.Step10.Name = "Step10"
        Me.Step10.Size = New System.Drawing.Size(429, 289)
        Me.Step10.TabIndex = 12
        '
        'chkRestart
        '
        Me.chkRestart.AutoSize = True
        Me.chkRestart.Checked = True
        Me.chkRestart.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkRestart.Location = New System.Drawing.Point(6, 139)
        Me.chkRestart.Name = "chkRestart"
        Me.chkRestart.Size = New System.Drawing.Size(217, 20)
        Me.chkRestart.TabIndex = 1
        Me.chkRestart.Text = "Restart application and scheduler"
        Me.chkRestart.UseVisualStyleBackColor = True
        '
        'Label18
        '
        Me.Label18.AutoEllipsis = True
        Me.Label18.Location = New System.Drawing.Point(3, 9)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(414, 108)
        Me.Label18.TabIndex = 0
        Me.Label18.Text = resources.GetString("Label18.Text")
        '
        'btnCancel
        '
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(276, 407)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 25)
        Me.btnCancel.TabIndex = 13
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNext.Location = New System.Drawing.Point(357, 407)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(75, 25)
        Me.btnNext.TabIndex = 14
        Me.btnNext.Text = "&Next>"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnBack
        '
        Me.btnBack.Enabled = False
        Me.btnBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBack.Location = New System.Drawing.Point(195, 407)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(75, 25)
        Me.btnBack.TabIndex = 15
        Me.btnBack.Text = "<&Back"
        Me.btnBack.UseVisualStyleBackColor = True
        '
        'btnFinish
        '
        Me.btnFinish.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnFinish.Location = New System.Drawing.Point(357, 407)
        Me.btnFinish.Name = "btnFinish"
        Me.btnFinish.Size = New System.Drawing.Size(75, 25)
        Me.btnFinish.TabIndex = 16
        Me.btnFinish.Text = "&Finish"
        Me.btnFinish.UseVisualStyleBackColor = True
        Me.btnFinish.Visible = False
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(440, 437)
        Me.Controls.Add(Me.Step1)
        Me.Controls.Add(Me.Step7)
        Me.Controls.Add(Me.Step6)
        Me.Controls.Add(Me.Step9)
        Me.Controls.Add(Me.Step4)
        Me.Controls.Add(Me.Step5)
        Me.Controls.Add(Me.Step8)
        Me.Controls.Add(Me.Step10)
        Me.Controls.Add(Me.Step3)
        Me.Controls.Add(Me.Step2)
        Me.Controls.Add(Me.btnBack)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.Footer1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btnFinish)
        Me.Controls.Add(Me.btnNext)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.Navy
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Registration and Activation Wizard"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step1.ResumeLayout(False)
        Me.Step2.ResumeLayout(False)
        Me.Step3.ResumeLayout(False)
        Me.Step3.PerformLayout()
        Me.Step4.ResumeLayout(False)
        Me.Step5.ResumeLayout(False)
        Me.Step6.ResumeLayout(False)
        Me.Step6.PerformLayout()
        Me.Step7.ResumeLayout(False)
        Me.Step7.PerformLayout()
        Me.Step8.ResumeLayout(False)
        Me.Step9.ResumeLayout(False)
        Me.Step9.PerformLayout()
        Me.Step10.ResumeLayout(False)
        Me.Step10.PerformLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents Footer1 As WizardFooter.Footer
    Friend WithEvents Step1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Step2 As System.Windows.Forms.Panel
    Friend WithEvents chkEmail As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Step3 As System.Windows.Forms.Panel
    Friend WithEvents chkAdmin As System.Windows.Forms.CheckBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Step4 As System.Windows.Forms.Panel
    Friend WithEvents chkTerm As System.Windows.Forms.CheckBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Step5 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Step6 As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtFingerPrint As System.Windows.Forms.TextBox
    Friend WithEvents txtCustNo As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Step7 As System.Windows.Forms.Panel
    Friend WithEvents btnActivate As System.Windows.Forms.Button
    Friend WithEvents txtKey As System.Windows.Forms.TextBox
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Step8 As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Step9 As System.Windows.Forms.Panel
    Friend WithEvents btnRegister As System.Windows.Forms.Button
    Friend WithEvents txtLastName As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtCompanyName As System.Windows.Forms.TextBox
    Friend WithEvents txtFirstName As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtCustNo1 As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Step10 As System.Windows.Forms.Panel
    Friend WithEvents chkRestart As System.Windows.Forms.CheckBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents btnBack As System.Windows.Forms.Button
    Friend WithEvents btnFinish As System.Windows.Forms.Button
    Friend WithEvents btnCopy2 As System.Windows.Forms.Button
    Friend WithEvents btnCopy1 As System.Windows.Forms.Button
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents txtLicenseNumber As System.Windows.Forms.MaskedTextBox

End Class
