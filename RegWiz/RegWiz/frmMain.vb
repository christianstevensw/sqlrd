Imports ChristianSteven.Settings.clsSettingsManager
Imports RegWiz.clsLicenser
Public Class frmMain
    Private Const STR_MsgCaption As String = "Registration and Activation Wizard"
    Dim nStep As Integer

    Dim sKey As String
    Dim Skip As Boolean
    Dim appPath As String = Application.StartupPath
    Dim configFile As String
    Dim steps As List(Of Panel)
    Dim STR_Msg(9) As String

    Const STR_Key As String = "Software\ChristianSteven\CRD"
    Const STR_configName As String = "crdlive.config"
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property

    Private Sub InitCaptions()
        STR_Msg(0) = "Registration && Activation Wizard"
        STR_Msg(1) = "Email Check"
        STR_Msg(2) = "Local Administrator Check"
        STR_Msg(3) = "Terminal Services Check"
        STR_Msg(4) = "Registration Details"
        STR_Msg(5) = "Enter Registration Details"
        STR_Msg(6) = "Activation"
        STR_Msg(7) = "Manual Activation"
        STR_Msg(8) = "Enter Activation Details"
        STR_Msg(9) = "Finish"
    End Sub
    Private Sub InitSteps()
        steps = New List(Of Panel)

        steps.Add(Step1)
        steps.Add(Step2)
        steps.Add(Step3)
        steps.Add(Step4)
        steps.Add(Step5)
        steps.Add(Step6)
        steps.Add(Step7)
        steps.Add(Step8)
        steps.Add(Step9)
        steps.Add(Step10)
    End Sub
    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'Dim arm As MarsStart.clsArmadillo
            '
            'Try
            '    arm = New MarsStart.clsArmadillo
            'Catch
            '    Dim proc As Process = New Process

            '    proc.Start(appPath & "\link.exe")

            '    proc.WaitForExit()

            '    arm = New MarsStart.clsArmadillo
            'End Try



            If appPath.EndsWith("\") = False Then appPath &= "\"

            configFile = appPath & STR_configName

            InitCaptions()

            InitSteps()

            nStep = 0

            lblMessage.Text = STR_Msg(nStep)

            Step1.BringToFront()

            txtCustNo.Text = Appconfig.GetSettingValue("CustNo", "", configFile)
            txtCustNo1.Text = txtCustNo.Text

            txtLicenseNumber.Text = Appconfig.GetSettingValue("RegNo", "", configFile)

            txtFirstName.Text = Appconfig.GetSettingValue("RegFirstName", "", configFile)
            txtLastName.Text = Appconfig.GetSettingValue("RegLastName", "", configFile)

            txtCompanyName.Text = Appconfig.GetSettingValue("RegCo", "", configFile)
            txtName.Text = txtCompanyName.Text

            'txtFingerPrint.Text = arm.GetHardwareFingerPrint

            btnNext.BringToFront()
        Catch ex As Exception
            MessageBox.Show("The Registration & Activation wizard has encountered a fatal error and needs to close." & vbCrLf & _
            "Error Description: " & ex.Message & " [" & Err.Number & "]", "Registration & Activation Wizard", MessageBoxButtons.OK, _
             MessageBoxIcon.Exclamation)
            End
        End Try

    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Select Case nStep
            Case 9
                btnNext.Visible = True
                btnFinish.Visible = False
                btnCancel.Enabled = True
                nStep = nStep - 1
            Case 8
                nStep = nStep - 1
                btnNext.Enabled = True
            Case 7
                nStep = nStep - 1
            Case 6
                If Skip = True Then
                    nStep = nStep - 2
                Else
                    nStep = nStep - 1
                End If

                btnNext.Enabled = True
            Case 5
                nStep = nStep - 1
                btnNext.Enabled = True
            Case 4
                nStep = nStep - 1
                btnNext.Enabled = True
            Case 3
                nStep = nStep - 1
                btnNext.Enabled = chkAdmin.Checked
            Case 2
                nStep = nStep - 1
                btnNext.Enabled = chkEmail.Checked
            Case 1
                nStep = nStep - 1
                btnBack.Enabled = False
                btnNext.Enabled = True
        End Select

        lblMessage.Text = STR_Msg(nStep)

        steps(nStep).BringToFront()
    End Sub

    Private Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Select Case nStep
            Case 0
                btnNext.Enabled = chkEmail.Checked
                btnBack.Enabled = True
                nStep = nStep + 1
            Case 1
                btnNext.Enabled = chkAdmin.Checked
                nStep = nStep + 1
            Case 2
                btnNext.Enabled = chkTerm.Checked
                nStep = nStep + 1
            Case 3
                nStep = nStep + 1
            Case 4
                Dim oActivate As frmActivate = New frmActivate

                Using oActivate
                    If oActivate.GetActivationCode(txtFingerPrint.Text, txtCustNo.Text) = True Then
                        nStep = nStep + 2
                        btnNext.Enabled = False
                        Skip = True
                    Else
                        nStep = nStep + 1
                        Skip = False
                    End If
                End Using
            Case 5
                nStep = nStep + 1
            Case 6
                nStep = nStep + 1
            Case 7
                nStep = nStep + 1
                btnNext.Enabled = False
            Case 8
                btnFinish.Visible = True
                btnNext.Visible = False
                btnCancel.Enabled = False
                nStep = nStep + 1
        End Select

        lblMessage.Text = STR_Msg(nStep)

        steps(nStep).BringToFront()
    End Sub

    Private Sub chkEmail_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles chkEmail.CheckedChanged, chkAdmin.CheckedChanged, chkTerm.CheckedChanged
        Dim checkBox As CheckBox = CType(sender, CheckBox)

        btnNext.Enabled = checkBox.Checked

    End Sub

    Private Sub btnActivate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActivate.Click
        Dim arm As MarsStart.clsArmadillo = New MarsStart.clsArmadillo

        If arm.InstallKey(txtName.Text, txtKey.Text) = False Then
            ep.SetError(txtKey, "The name/key you have provided does not appear to be valid. Please try again.")
            txtKey.Focus()
            btnNext.Enabled = False
        Else
            MessageBox.Show("The key you have provided is VALID and has been stored. Press NEXT to continue.", _
            STR_MsgCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
            btnNext.Enabled = True
        End If

#If DEBUG Then
        btnNext.Enabled = True
#End If
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        txtCompanyName.Text = txtName.Text
    End Sub

    Private Sub btnRegister_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegister.Click
        If txtCustNo1.Text.Length = 0 Then
            ep.SetError(txtCustNo1, "Please enter your customer number")
            txtCustNo1.Focus()
            Return
        End If

        Dim fullName As String = txtFirstName.Text & txtLastName.Text
        Dim licenseInfo As String = m_licenser.GetLicenseInfo(txtLicenseNumber.Text, fullName, txtCompanyName.Text)

        Dim nStart As Integer = licenseInfo.Length - 8
        Dim nEnd As Integer = licenseInfo.Length

        Dim supportDate As String = licenseInfo.Split(",")(1)

        Dim license As String = m_licenser.CreateLicense(fullName, txtCompanyName.Text, txtCustNo1.Text, supportDate)

        If txtLicenseNumber.Text <> license Then
            ep.SetError(txtLicenseNumber, "Invalid license number. Please check to make sure that all fields have been entered correctly.")
            txtLicenseNumber.Focus()
            btnNext.Enabled = False
            Return
        Else
            btnNext.Enabled = True
            MessageBox.Show("The license number you have entered is CORRECT and has been saved", STR_MsgCaption, _
            MessageBoxButtons.OK, MessageBoxIcon.Information)

            With Appconfig
                .SaveSettingValue("RegNo", txtLicenseNumber.Text, configFile)
                .SaveSettingValue("RegUser", txtFirstName.Text & " " & txtLastName.Text, configFile)
                .SaveSettingValue("RegFirstName", txtFirstName.Text, configFile)
                .SaveSettingValue("RegLastName", txtLastName.Text, configFile)
                .SaveSettingValue("RegCo", txtCompanyName.Text, configFile)
                .SaveSettingValue("CustNo", txtCustNo1.Text, configFile)
            End With
        End If


    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If MessageBox.Show("Exit the Registration & Activation Wizard?", STR_MsgCaption, _
        MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

            Close()
        End If
    End Sub

    Private Sub txtKey_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles txtKey.TextChanged, txtCustNo1.TextChanged
        Dim txtBox As TextBox = CType(sender, TextBox)

        ep.SetError(txtBox, String.Empty)

    End Sub

    Private Sub btnFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        Try
            If chkRestart.Checked Then
                Dim spath As String = appPath & "crd5.exe"

                Process.Start(spath)
            End If
        Catch : End Try

        Close()
    End Sub

    
    
    Private Sub txtLicenseNumber_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        '5B5D671D4EC9
        If txtLicenseNumber.Text.Length = 4 Then
            txtLicenseNumber.SelectedText = "-"
        ElseIf txtLicenseNumber.Text.Length = 9 Then
            txtLicenseNumber.SelectedText = "-"
        End If
    End Sub
End Class
