Public Class frmActivate
    Public Function GetActivationCode(ByVal sFingerPrint As String, ByVal sCustNo As String) As Boolean

        Try
            If CheckInternet() = False Then
                Return False
            End If

            wb.Navigate("http://www.christiansteven.com/crd/members/activate.htm")

            Do
                Application.DoEvents()
            Loop Until wb.Busy = False

            Dim Obj

            For Each Obj In wb.Document.All.tags("input")
                If Obj.name = "Fingerprint" Then
                    Obj.Value = sFingerPrint
                ElseIf Obj.name = "custid" Then
                    Obj.Value = sCustNo
                End If
            Next

            Me.ShowDialog()

            Return True
        Catch ex As Exception
            Console.WriteLine(ex.Message)
            Return False
        End Try
    End Function

    Private Function CheckInternet() As Boolean
        Dim o As System.Net.IPHostEntry
        Dim TcpAddress As String = ""

        Try
            o = System.Net.Dns.GetHostEntry("www.christiansteven.com")

            For Each s As System.Net.IPAddress In o.AddressList
                TcpAddress = s.ToString
            Next

            Dim t As New Net.Sockets.TcpClient

            t.Connect(TcpAddress, 80)

            t.Close()

            Return True

        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub btnFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        Close()
    End Sub
End Class