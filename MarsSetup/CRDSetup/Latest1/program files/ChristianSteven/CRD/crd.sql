CREATE TABLE [AutomationAttr] (
	[AutoID] [int] DEFAULT 0 PRIMARY KEY,
	[AutoName] [varchar] (50)  ,
	[Parent] [int] DEFAULT 0 ,
	[Owner] [varchar] (50)  
) 
GO

CREATE TABLE [BlankReportAlert] (
	[BlankID] [int] DEFAULT 0 PRIMARY KEY,
	[Type] [text]  ,
	[To] [text]  ,
	[Msg] [text]  ,
	[ReportID] [int] DEFAULT 0 ,
	[PackID] [int] DEFAULT 0 ,
	[Subject] [text]  
)  
GO

CREATE TABLE [CRDUsers] (
	[UserID] [varchar] (50)  PRIMARY KEY,
	[Password] [varchar] (50)  ,
	[FirstName] [varchar] (50)  ,
	[LastName] [varchar] (50)  ,
	[UserRole] [varchar] (50)  ,
	[LastModBy] [varchar] (50)  ,
	[LastModDate] [datetime] NULL 
) 
GO

CREATE TABLE [CalendarAttr] (
	[CalendarName] [varchar] (50)  ,
	[CalendarDate] [varchar] (50)  ,
	[CalendarID] [int] DEFAULT 0 
) 
GO

CREATE TABLE [ContactAttr] (
	[ContactID] [int] DEFAULT 0 PRIMARY KEY,
	[ContactName] [varchar] (50)  ,
	[ContactType] [varchar] (50)  
) 
GO

CREATE TABLE [ContactDetail] (
	[DetailID] [int] DEFAULT 0 PRIMARY KEY,
	[ContactID] [int] DEFAULT 0 ,
	[EmailAddress] [text]  ,
	[PhoneNumber] [varchar] (50)  ,
	[FaxNumber] [varchar] (50)  
)  
GO

CREATE TABLE [CustomSections] (
	[ReportID] [int] DEFAULT 0 ,
	[SectionNo] [int] DEFAULT 0 ,
	[Suppress] [int] DEFAULT 0 
) 
GO

CREATE TABLE [DBLink] (
	[LinkID] [int] DEFAULT 0 PRIMARY KEY,
	[ReportID] [float] DEFAULT 0 ,
	[LinkSQL] [text]  ,
	[LinkODBC] [varchar] (255)  ,
	[KeyParameter] [varchar] (255)  ,
	[KeyTable] [varchar] (55)  ,
	[KeyColumn] [varchar] (55)  ,
	[EmailColumn] [varchar] (55)  
)  
GO

CREATE TABLE [DataItems] (
	[ItemID] [int] DEFAULT 0 PRIMARY KEY,
	[ItemName] [varchar] (50)  ,
	[ConString] [varchar] (50)  ,
	[SQLQuery] [text]  
)  
GO

CREATE TABLE [DeferDeliveryAttr] (
	[DeferID] [int] DEFAULT 0 PRIMARY KEY,
	[DestinationID] [int] DEFAULT 0 ,
	[DueDate] [datetime] NULL ,
	[FilePath] [text]  ,
	[FileName] [varchar] (50)  
)  
GO

CREATE TABLE [DestinationAttr] (
	[DestinationID] [int] DEFAULT 0 PRIMARY KEY,
	[DestinationType] [varchar] (50)  ,
	[SendTo] [text]  ,
	[CC] [text]  ,
	[BCC] [text]  ,
	[Subject] [text]  ,
	[Message] [text]  ,
	[Extras] [text]  ,
	[OutputPath] [varchar] (255)  ,
	[PrinterName] [text]  ,
	[PrinterDriver] [varchar] (50)  ,
	[PrinterPort] [varchar] (50)  ,
	[Orientation] [varchar] (50)  ,
	[PageFrom] [int] DEFAULT 0 ,
	[PageTo] [int] DEFAULT 0 ,
	[Copies] [int] DEFAULT 0 ,
	[Collate] [int] DEFAULT 0 ,
	[ReportID] [int] DEFAULT 0 ,
	[PackID] [int] DEFAULT 0 ,
	[FTPServer] [varchar] (50)  ,
	[FTPUserName] [varchar] (50)  ,
	[FTPPassword] [varchar] (50)  ,
	[FTPPath] [varchar] (255)  ,
	[DestinationName] [varchar] (50)  ,
	[OutputFormat] [varchar] (50)  ,
	[CustomExt] [varchar] (50)  ,
	[AppendDateTime] [int] DEFAULT 0 ,
	[DateTimeFormat] [varchar] (50)  ,
	[CustomName] [text]  ,
	[IncludeAttach] [int] DEFAULT 0 ,
	[Compress] [int] DEFAULT 0 ,
	[Embed] [int] DEFAULT 0 ,
	[MailFormat] [varchar] (50)  ,
	[HTMLSplit] [int] DEFAULT 0 ,
	[DeferDelivery] [int] DEFAULT 0 ,
	[DeferBy] [varchar] (15)  ,
	[UseBursting] [int] DEFAULT 0 ,
	[SMTPServer] [varchar] (50)  
)  
GO

CREATE TABLE [DynamicAttr] (
	[DynamicID] [int] DEFAULT 0 PRIMARY KEY,
	[KeyParameter] [varchar] (50)  ,
	[KeyColumn] [varchar] (50)  ,
	[ConString] [text]  ,
	[Query] [text]  ,
	[ReportID] [int] DEFAULT 0 
)  
GO

CREATE TABLE [DynamicLink] (
	[LinkID] [int] DEFAULT 0 PRIMARY KEY,
	[ReportID] [int] DEFAULT 0 ,
	[LinkSQL] [text]  ,
	[ConString] [varchar] (50)  ,
	[KeyColumn] [varchar] (50)  ,
	[KeyParameter] [varchar] (50)  ,
	[ValueColumn] [varchar] (50)  ,
	[Table1] [varchar] (50)  ,
	[Table2] [varchar] (50)  ,
	[JoinColumn1] [varchar] (50)  ,
	[JoinColumn2] [varchar] (50)  ,
	[WhereList] [text]  
)  
GO

CREATE TABLE [EmailLog] (
	[LogID] [int] DEFAULT 0 PRIMARY KEY,
	[ScheduleName] [varchar] (50)  ,
	[SendTo] [text]  ,
	[Subject] [text]  ,
	[DateSent] [datetime] NULL 
)  
GO

CREATE TABLE [EventAttr] (
	[EventID] [int] DEFAULT 0 PRIMARY KEY,
	[EventName] [varchar] (50)  ,
	[Parent] [int] DEFAULT 0 ,
	[EventType] [varchar] (50)  ,
	[FireWhen] [int] DEFAULT 0 ,
	[FilePath] [varchar] (255)  ,
	[ModDateTime] [datetime] NULL ,
	[Search1] [text]  ,
	[Search2] [text]  ,
	[PopServer] [varchar] (50)  ,
	[PopUserID] [varchar] (50)  ,
	[PopPassword] [varchar] (50)  ,
	[Status] [int] DEFAULT 0 ,
	[Owner] [varchar] (50)  ,
	[PopRemove] [int] DEFAULT 0 ,
	[Description] [text]  ,
	[KeyWord] [text]  ,
	[DisabledDate] [datetime] NULL 
)  
GO

CREATE TABLE [EventHistory] (
	[HistoryID] [int] DEFAULT 0 PRIMARY KEY,
	[EventID] [int] DEFAULT 0 ,
	[LastFired] [datetime] NULL ,
	[Status] [int] DEFAULT 0 ,
	[ErrMsg] [text]  
)  
GO

CREATE TABLE [EventSchedule] (
	[ID] [int] DEFAULT 0 PRIMARY KEY,
	[EventID] [int] DEFAULT 0 ,
	[ScheduleID] [int] DEFAULT 0 ,
	[ScheduleType] [varchar] (50)  ,
	[Status] [int] DEFAULT 0 
) 
GO

CREATE TABLE [Folders] (
	[FolderID] [int] DEFAULT 0 PRIMARY KEY,
	[FolderName] [varchar] (255) ,
	[Parent] [int] DEFAULT 0 
)  
GO

CREATE TABLE [FormulaAttr] (
	[FormulaID] [int] DEFAULT 0 PRIMARY KEY,
	[FormulaName] [varchar] (50)  ,
	[FormulaDef] [text]  
)  
GO

CREATE TABLE [HouseKeepingAttr] (
	[HouseID] [int] DEFAULT 0 PRIMARY KEY,
	[DestinationID] [int] DEFAULT 0 ,
	[AgeValue] [int] DEFAULT 0 ,
	[AgeUnit] [varchar] (50)  
) 
GO

CREATE TABLE [PackageAttr] (
	[PackID] [int] DEFAULT 0 PRIMARY KEY,
	[PackageName] [text]  ,
	[Parent] [int] DEFAULT 0 ,
	[Retry] [int] DEFAULT 0 ,
	[AssumeFail] [int] DEFAULT 0 ,
	[CheckBlank] [int] DEFAULT 0 ,
	[Owner] [varchar] (50)  ,
	[FailOnOne] [int] DEFAULT 0 ,
	[MergePDF] [int] DEFAULT 0 ,
	[MergeXL] [int] DEFAULT 0 ,
	[MergePDFName] [varchar] (50)  ,
	[MergeXLName] [varchar] (50)  ,
	[DateTimeStamp] [int] DEFAULT 0 ,
	[StampFormat] [varchar] (50)  
)  
GO

CREATE TABLE [PackageOptions] (
	[OptionID] [int] DEFAULT 0 PRIMARY KEY,
	[PackID] [int] DEFAULT 0 ,
	[PDFPassword] [varchar] (50)  ,
	[UserPassword] [varchar] (50)  ,
	[PDFWatermark] [varchar] (50)  ,
	[CanPrint] [int] DEFAULT 0 ,
	[CanCopy] [int] DEFAULT 0 ,
	[CanEdit] [int] DEFAULT 0 ,
	[CanNotes] [int] DEFAULT 0 ,
	[CanFill] [int] DEFAULT 0 ,
	[CanAccess] [int] DEFAULT 0 ,
	[CanAssemble] [int] DEFAULT 0 ,
	[CanPrintFull] [int] DEFAULT 0 ,
	[PDFSecurity] [int] DEFAULT 0 ,
	[InfoTitle] [varchar] (50)  ,
	[InfoAuthor] [varchar] (50)  ,
	[InfoSubject] [varchar] (50)  ,
	[InfoProducer] [varchar] (50)  ,
	[InfoKeywords] [text]  ,
	[InfoCreated] [datetime] NULL 
)  
GO

CREATE TABLE [PackagedReportAttr] (
	[AttrID] [int] DEFAULT 0 PRIMARY KEY,
	[PackID] [int] DEFAULT 0 ,
	[ReportID] [int] DEFAULT 0 ,
	[OutputFormat] [varchar] (50)  ,
	[Customext] [varchar] (50)  ,
	[AppendDateTime] [varchar] (50)  ,
	[DateTimeFormat] [varchar] (50)  ,
	[CustomName] [varchar] (50)  
) 
GO

CREATE TABLE [PrinterAttr] (
	[PrinterID] [int] DEFAULT 0 PRIMARY KEY,
	[DestinationID] [int] DEFAULT 0 ,
	[PrinterName] [text]  ,
	[Orientation] [varchar] (25)  ,
	[PrinterDriver] [text]  ,
	[PrinterPort] [text]  ,
	[PageFrom] [int] DEFAULT 0 ,
	[PageTo] [int] DEFAULT 0 ,
	[Copies] [int] DEFAULT 0 ,
	[Collate] [int] DEFAULT 0 ,
	[PagesPerReport] [int] DEFAULT 0 ,
	[PrintMethod] [varchar] (25)  ,
	[PaperSize] [varchar] (50)  
)  
GO

CREATE TABLE [RefresherAttr] (
	[RefreshID] [int] DEFAULT 0 PRIMARY KEY,
	[ObjectType] [varchar] (50)  ,
	[Frequency] [varchar] (50)  ,
	[ObjectName] [varchar] (50)  ,
	[ObjectID] [int] DEFAULT 0 ,
	[LastRun] [datetime] NULL ,
	[NextRun] [datetime] NULL ,
	[RepeatEvery] [int] DEFAULT 0 ,
	[StartTime] [varchar] (50)  
) 
GO

CREATE TABLE [RemoteSysAttr] (
	[RemoteID] [int] DEFAULT 0 PRIMARY KEY,
	[PCName] [varchar] (55)  ,
	[LivePath] [text]  ,
	[AutoConnect] [int] DEFAULT 0 
)  
GO

CREATE TABLE [ReportAttr] (
	[ReportID] [int] DEFAULT 0 PRIMARY KEY,
	[DatabasePath] [varchar] (255)  ,
	[ReportName] [text]  ,
	[PackID] [int] DEFAULT 0 ,
	[Parent] [int] DEFAULT 0 ,
	[DatabasePassword] [varchar] (50)  ,
	[ReportTitle] [text]  ,
	[Retry] [int] DEFAULT 0 ,
	[AssumeFail] [int] DEFAULT 0 ,
	[CheckBlank] [int] DEFAULT 0 ,
	[SelectionFormula] [text]  ,
	[Dynamic] [int] DEFAULT 0 ,
	[IncludeAttach] [int] DEFAULT 0 ,
	[Owner] [varchar] (50)  ,
	[RptUserID] [varchar] (50)  ,
	[RptPassword] [varchar] (50)  ,
	[RptServer] [varchar] (50)  ,
	[RptDatabase] [varchar] (50)  ,
	[UseLogin] [int] DEFAULT 0 ,
	[UseSavedData] [int] DEFAULT 0 ,
	[CachePath] [text]  ,
	[LastRefreshed] [datetime] NULL ,
	[AutoRefresh] [int] DEFAULT 0 
)  
GO

CREATE TABLE [ReportOptions] (
	[OptionID] [int] DEFAULT 0 PRIMARY KEY,
	[HTMLMerge] [int] DEFAULT 0 ,
	[PDFWatermark] [varchar] (50)  ,
	[PDFPassword] [varchar] (50)  ,
	[DestinationID] [int] DEFAULT 0 ,
	[ReportID] [int] DEFAULT 0 ,
	[ColWidth] [varchar] (50)  ,
	[ExportHF] [int] DEFAULT 0 ,
	[PageBreak] [int] DEFAULT 0 ,
	[ConvertDate] [int] DEFAULT 0 ,
	[PageFrom] [int] DEFAULT 0 ,
	[PageTo] [int] DEFAULT 0 ,
	[sCharacter] [varchar] (50)  ,
	[sDelimiter] [varchar] (50)  ,
	[ODBC] [text]  ,
	[TableName] [varchar] (50)  ,
	[LinesPerPage] [int] DEFAULT 0 ,
	[UserPassword] [varchar] (50)  ,
	[CanPrint] [int] DEFAULT 0 ,
	[CanCopy] [int] DEFAULT 0 ,
	[CanEdit] [int] DEFAULT 0 ,
	[CanNotes] [int] DEFAULT 0 ,
	[CanFill] [int] DEFAULT 0 ,
	[CanAccess] [int] DEFAULT 0 ,
	[CanAssemble] [int] DEFAULT 0 ,
	[CanPrintFull] [int] DEFAULT 0 ,
	[PDFSecurity] [int] DEFAULT 0 ,
	[HTMLNavigator] [int] DEFAULT 0 ,
	[WorkSheetName] [varchar] (50)  ,
	[InfoTitle] [varchar] (50)  ,
	[InfoAuthor] [varchar] (50)  ,
	[InfoSubject] [varchar] (50)  ,
	[InfoProducer] [varchar] (50)  ,
	[InfoKeywords] [text]  ,
	[InfoCreated] [datetime] NULL 
)  
GO

CREATE TABLE [ReportParameter] (
	[ParID] [int] DEFAULT 0 PRIMARY KEY,
	[ReportID] [int] DEFAULT 0 ,
	[ParName] [text]  ,
	[ParValue] [text]  ,
	[ParType] [int] DEFAULT 0 
)  
GO

CREATE TABLE [ReportSnapshots] (
	[SnapID] [int] DEFAULT 0 PRIMARY KEY,
	[ReportID] [int] DEFAULT 0 ,
	[KeepSnap] [int] DEFAULT 0 
) 
GO

CREATE TABLE [SMTPServers] (
	[SMTPID] [int] DEFAULT 0 PRIMARY KEY,
	[SMTPName] [varchar] (50)  ,
	[SMTPServer] [varchar] (50)  ,
	[SMTPUser] [varchar] (50)  ,
	[SMTPPassword] [varchar] (50)  ,
	[SMTPSenderName] [varchar] (50)  ,
	[SMTPSenderAddress] [varchar] (50)  ,
	[SMTPTimeout] [int] DEFAULT 0 ,
	[IsBackup] [int] DEFAULT 0 
) 
GO

CREATE TABLE [ScheduleAttr] (
	[ScheduleID] [int] DEFAULT 0 PRIMARY KEY,
	[Frequency] [varchar] (50)  ,
	[StartDate] [datetime] NULL ,
	[EndDate] [datetime] NULL ,
	[NextRun] [datetime] NULL ,
	[StartTime] [varchar] (50)  ,
	[Repeat] [int] DEFAULT 0 ,
	[RepeatInterval] [varchar] (50)  ,
	[RepeatUntil] [datetime] NULL ,
	[Status] [int] DEFAULT 0 ,
	[ReportID] [int] DEFAULT 0 ,
	[PackID] [int] DEFAULT 0 ,
	[AutoID] [int] DEFAULT 0 ,
	[Description] [text]  ,
	[KeyWord] [text]  ,
	[CalendarName] [varchar] (50)  ,
	[DisabledDate] [datetime] NULL 
)  
GO

CREATE TABLE [ScheduleHistory] (
	[HistoryID] [int] DEFAULT 0 PRIMARY KEY,
	[ReportID] [int] DEFAULT 0 ,
	[PackID] [int] DEFAULT 0 ,
	[AutoID] [int] DEFAULT 0 ,
	[EntryDate] [datetime] NULL ,
	[Success] [int] DEFAULT 0 ,
	[ErrMsg] [text]  ,
	[StartDate] [datetime] NULL 
)  
GO

CREATE TABLE [ScheduleOptions] (
	[OptionID] [int] DEFAULT 0 PRIMARY KEY,
	[WeekNo] [int] DEFAULT 0 ,
	[DayNo] [int] DEFAULT 0 ,
	[MonthsIn] [text]  ,
	[nCount] [int] DEFAULT 0 ,
	[ScheduleID] [int] DEFAULT 0 ,
	[ScheduleType] [varchar] (50)  
)  
GO

CREATE TABLE [SendWait] (
	[QueueNumber] [int] DEFAULT 0 PRIMARY KEY,
	[Addresses] [text]  ,
	[Subject] [text]  ,
	[Body] [text]  ,
	[SendType] [varchar] (55)  ,
	[Attach] [text]  ,
	[Numattach] [int] DEFAULT 0 ,
	[Extras] [text]  ,
	[SendCC] [text]  ,
	[SendBCC] [text]  ,
	[sName] [text]  ,
	[InBody] [varchar] (15)  ,
	[sFormat] [varchar] (25)  ,
	[FileList] [text]  ,
	[LastResult] [text]  ,
	[LastAttempt] [datetime] NULL ,
	[MailFormat] [varchar] (50)  ,
	[IncludeAttach] [int] DEFAULT 0 ,
	[SMTPName] [varchar] (50)  
)  
GO

CREATE TABLE [Slaves] (
	[SlaveID] [int] DEFAULT 0 PRIMARY KEY,
	[SlaveName] [varchar] (255)  ,
	[SlavePath] [text]  
)  
GO

CREATE TABLE [SmartFolderAttr] (
	[SmartDefID] [int] DEFAULT 0 PRIMARY KEY,
	[SmartID] [int] DEFAULT 0 ,
	[SmartColumn] [varchar] (50)  ,
	[SmartCriteria] [text]  
)  
GO

CREATE TABLE [SmartFolders] (
	[SmartID] [int] DEFAULT 0 PRIMARY KEY,
	[SmartName] [varchar] (50)  ,
	[SmartDesc] [text]  ,
	[SmartType] [varchar] (10)  ,
	[Owner] [varchar] (50)  
)  
GO

CREATE TABLE [SnapshotsAttr] (
	[AttrID] [int] DEFAULT 0 PRIMARY KEY,
	[ReportID] [int] DEFAULT 0 ,
	[SnapPath] [text]  ,
	[DateCreated] [datetime] NULL 
)  
GO

CREATE TABLE [SubReportLogin] (
	[SubLoginID] [int] DEFAULT 0 PRIMARY KEY,
	[ReportID] [int] DEFAULT 0 ,
	[SubName] [text]  ,
	[RptServer] [text]  ,
	[RptDatabase] [text]  ,
	[RptUserID] [text]  ,
	[RptPassword] [text]  
)  
GO

CREATE TABLE [SubReportParameters] (
	[SubParID] [int] DEFAULT 0 PRIMARY KEY,
	[ReportID] [int] DEFAULT 0 ,
	[SubName] [text]  ,
	[ParName] [text]  ,
	[ParValue] [text]  ,
	[ParType] [int] DEFAULT 0 
)  
GO

CREATE TABLE [TaskManager] (
	[MonitorID] [int] DEFAULT 0 PRIMARY KEY,
	[ReportID] [int] DEFAULT 0 ,
	[PackID] [int] DEFAULT 0 ,
	[EntryDate] [datetime] NULL ,
	[PCName] [varchar] (50)  ,
	[Status] [varchar] (50)  
) 
GO

CREATE TABLE [Tasks] (
	[TaskID] [int] DEFAULT 0 PRIMARY KEY,
	[ScheduleID] [int] DEFAULT 0 ,
	[TaskType] [varchar] (50)  ,
	[TaskName] [varchar] (50)  ,
	[ProgramPath] [varchar] (255)  ,
	[ProgramParameters] [varchar] (50)  ,
	[WindowStyle] [varchar] (50)  ,
	[PrinterName] [varchar] (50)  ,
	[Filelist] [text]  ,
	[PauseInt] [int] DEFAULT 0 ,
	[ReplaceFiles] [int] DEFAULT 0 ,
	[Msg] [text]  ,
	[SendTo] [text]  ,
	[CC] [text]  ,
	[Bcc] [text]  ,
	[Subject] [varchar] (255)  ,
	[OrderID] [int] DEFAULT 0 ,
	[FTPServer] [varchar] (50)  ,
	[FTPPort] [int] DEFAULT 0 ,
	[FTPUser] [varchar] (50)  ,
	[FTPPassword] [varchar] (50)  ,
	[FTPDirectory] [varchar] (255)  ,
	[RunWhen] [varchar] (50)  ,
	[AfterType] [varchar] (50)  
)  
GO

CREATE TABLE [UserColumns] (
	[ColumnID] [int] DEFAULT 0 PRIMARY KEY,
	[Caption] [varchar] (50)  ,
	[DataColumn] [varchar] (50)  ,
	[Owner] [varchar] (50)  
) 
GO

CREATE TABLE [UserView] (
	[ViewID] [int] DEFAULT 0 PRIMARY KEY,
	[UserID] [varchar] (50)  ,
	[ReportID] [int] DEFAULT 0 ,
	[PackID] [int] DEFAULT 0 ,
	[AutoID] [int] DEFAULT 0 ,
	[FolderID] [int] DEFAULT 0 ,
	[SmartID] [int] DEFAULT 0 ,
	[EventID] [int] DEFAULT 0 
) 
GO

CREATE TABLE [tmpFolders] (
	[FolderID] [int] DEFAULT 0 ,
	[FolderName] [text]  ,
	[Parent] [int] DEFAULT 0 
)  
GO

