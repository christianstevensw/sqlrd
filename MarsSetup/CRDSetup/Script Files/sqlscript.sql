CREATE TABLE [AutomationAttr] (
	[AutoID] [int] NOT NULL CONSTRAINT [DF__Automatio__AutoI__77BFCB91] DEFAULT (0),
	[AutoName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Parent] [int] NULL CONSTRAINT [DF__Automatio__Paren__78B3EFCA] DEFAULT (0),
	[Owner] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[AutoID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO


CREATE TABLE [BlankReportAlert] (
	[BlankID] [int] NOT NULL CONSTRAINT [DF__BlankRepo__Blank__7B905C75] DEFAULT (0),
	[Type] [text] COLLATE Latin1_General_CI_AS NULL ,
	[To] [text] COLLATE Latin1_General_CI_AS NULL ,
	[Msg] [text] COLLATE Latin1_General_CI_AS NULL ,
	[ReportID] [int] NULL CONSTRAINT [DF__BlankRepo__Repor__7C8480AE] DEFAULT (0),
	[PackID] [int] NULL CONSTRAINT [DF__BlankRepo__PackI__7D78A4E7] DEFAULT (0),
	[Subject] [text] COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[BlankID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [BurstingAttr] (
	[BurstID] [int] NOT NULL CONSTRAINT [DF__BurstingA__Burst__00551192] DEFAULT (0),
	[DestinationID] [int] NULL CONSTRAINT [DF__BurstingA__Desti__014935CB] DEFAULT (0),
	[BurstType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[GroupColumn] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[GroupValue] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[SendTo] [text] COLLATE Latin1_General_CI_AS NULL ,
	[Subject] [text] COLLATE Latin1_General_CI_AS NULL ,
	[Message] [text] COLLATE Latin1_General_CI_AS NULL ,
	[SendCC] [text] COLLATE Latin1_General_CI_AS NULL ,
	[SendBCC] [text] COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[BurstID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [CalendarAttr] (
	[CalendarName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[CalendarDate] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[CalendarID] [int] NULL CONSTRAINT [DF__CalendarA__Calen__0519C6AF] DEFAULT (0)
) ON [PRIMARY]
GO


CREATE TABLE [ContactAttr] (
	[ContactID] [int] NOT NULL CONSTRAINT [DF__ContactAt__Conta__07F6335A] DEFAULT (0),
	[ContactName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[ContactType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[ContactID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO


CREATE TABLE [ContactDetail] (
	[DetailID] [int] NOT NULL CONSTRAINT [DF__ContactDe__Detai__0AD2A005] DEFAULT (0),
	[ContactID] [int] NULL CONSTRAINT [DF__ContactDe__Conta__0BC6C43E] DEFAULT (0),
	[EmailAddress] [text] COLLATE Latin1_General_CI_AS NULL ,
	[PhoneNumber] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[FaxNumber] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[DetailID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [CRDUsers] (
	[UserID] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Password] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[FirstName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[LastName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[UserRole] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[LastModBy] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[LastModDate] [smalldatetime] NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[UserID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO


CREATE TABLE [CustomSections] (
	[ReportID] [int] NULL CONSTRAINT [DF__CustomSec__Repor__0DAF0CB0] DEFAULT (0),
	[SectionNo] [int] NULL CONSTRAINT [DF__CustomSec__Secti__0EA330E9] DEFAULT (0),
	[Suppress] [int] NULL CONSTRAINT [DF__CustomSec__Suppr__0F975522] DEFAULT (0)
) ON [PRIMARY]
GO


CREATE TABLE [DataItems] (
	[ItemID] [int] NOT NULL CONSTRAINT [DF__DataItems__ItemI__164452B1] DEFAULT (0),
	[ItemName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[ConString] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[SQLQuery] [text] COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[ItemID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [DBLink] (
	[LinkID] [int] NOT NULL CONSTRAINT [DF__DBLink__LinkID__1273C1CD] DEFAULT (0),
	[ReportID] [float] NULL CONSTRAINT [DF__DBLink__ReportID__1367E606] DEFAULT (0),
	[LinkSQL] [text] COLLATE Latin1_General_CI_AS NULL ,
	[LinkODBC] [varchar] (255) COLLATE Latin1_General_CI_AS NULL ,
	[KeyParameter] [varchar] (255) COLLATE Latin1_General_CI_AS NULL ,
	[KeyTable] [varchar] (55) COLLATE Latin1_General_CI_AS NULL ,
	[KeyColumn] [varchar] (55) COLLATE Latin1_General_CI_AS NULL ,
	[EmailColumn] [varchar] (55) COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[LinkID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [DeferDeliveryAttr] (
	[DeferID] [int] NOT NULL CONSTRAINT [DF__DeferDeli__Defer__1920BF5C] DEFAULT (0),
	[DestinationID] [int] NULL CONSTRAINT [DF__DeferDeli__Desti__1A14E395] DEFAULT (0),
	[DueDate] [smalldatetime] NULL ,
	[FilePath] [text] COLLATE Latin1_General_CI_AS NULL ,
	[FileName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[DeferID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [DestinationAttr] (
	[DestinationID] [int] NOT NULL CONSTRAINT [DF__Destinati__Desti__1CF15040] DEFAULT (0),
	[DestinationType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[SendTo] [text] COLLATE Latin1_General_CI_AS NULL ,
	[CC] [text] COLLATE Latin1_General_CI_AS NULL ,
	[BCC] [text] COLLATE Latin1_General_CI_AS NULL ,
	[Subject] [text] COLLATE Latin1_General_CI_AS NULL ,
	[Message] [text] COLLATE Latin1_General_CI_AS NULL ,
	[Extras] [text] COLLATE Latin1_General_CI_AS NULL ,
	[OutputPath] [varchar] (255) COLLATE Latin1_General_CI_AS NULL ,
	[PrinterName] [text] COLLATE Latin1_General_CI_AS NULL ,
	[PrinterDriver] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[PrinterPort] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Orientation] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[PageFrom] [int] NULL CONSTRAINT [DF__Destinati__PageF__1DE57479] DEFAULT (0),
	[PageTo] [int] NULL CONSTRAINT [DF__Destinati__PageT__1ED998B2] DEFAULT (0),
	[Copies] [int] NULL CONSTRAINT [DF__Destinati__Copie__1FCDBCEB] DEFAULT (0),
	[Collate] [int] NULL CONSTRAINT [DF__Destinati__Colla__20C1E124] DEFAULT (0),
	[ReportID] [int] NULL CONSTRAINT [DF__Destinati__Repor__21B6055D] DEFAULT (0),
	[PackID] [int] NULL CONSTRAINT [DF__Destinati__PackI__22AA2996] DEFAULT (0),
	[FTPServer] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[FTPUserName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[FTPPassword] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[FTPPath] [varchar] (255) COLLATE Latin1_General_CI_AS NULL ,
	[DestinationName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[OutputFormat] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[CustomExt] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[AppendDateTime] [int] NULL CONSTRAINT [DF__Destinati__Appen__239E4DCF] DEFAULT (0),
	[DateTimeFormat] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[CustomName] [text] COLLATE Latin1_General_CI_AS NULL ,
	[IncludeAttach] [int] NULL CONSTRAINT [DF__Destinati__Inclu__24927208] DEFAULT (0),
	[Compress] [int] NULL CONSTRAINT [DF__Destinati__Compr__25869641] DEFAULT (0),
	[Embed] [int] NULL CONSTRAINT [DF__Destinati__Embed__267ABA7A] DEFAULT (0),
	[MailFormat] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[HTMLSplit] [int] NULL CONSTRAINT [DF__Destinati__HTMLS__276EDEB3] DEFAULT (0),
	[DeferDelivery] [int] NULL CONSTRAINT [DF__Destinati__Defer__286302EC] DEFAULT (0),
	[DeferBy] [varchar] (15) COLLATE Latin1_General_CI_AS NULL ,
	[UseBursting] [int] NULL CONSTRAINT [DF__Destinati__UseBu__29572725] DEFAULT (0),
	[SMTPServer] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[DestinationID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [DynamicAttr] (
	[DynamicID] [int] NOT NULL CONSTRAINT [DF__DynamicAt__Dynam__2C3393D0] DEFAULT (0),
	[KeyParameter] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[KeyColumn] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[ConString] [text] COLLATE Latin1_General_CI_AS NULL ,
	[Query] [text] COLLATE Latin1_General_CI_AS NULL ,
	[ReportID] [int] NULL CONSTRAINT [DF__DynamicAt__Repor__2D27B809] DEFAULT (0),
	 PRIMARY KEY  CLUSTERED 
	(
		[DynamicID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [DynamicLink] (
	[LinkID] [int] NOT NULL CONSTRAINT [DF__DynamicLi__LinkI__300424B4] DEFAULT (0),
	[ReportID] [int] NULL CONSTRAINT [DF__DynamicLi__Repor__30F848ED] DEFAULT (0),
	[LinkSQL] [text] COLLATE Latin1_General_CI_AS NULL ,
	[ConString] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[KeyColumn] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[KeyParameter] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[ValueColumn] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Table1] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Table2] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[JoinColumn1] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[JoinColumn2] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[WhereList] [text] COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[LinkID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [EmailLog] (
	[LogID] [int] NOT NULL CONSTRAINT [DF__EmailLog__LogID__33D4B598] DEFAULT (0),
	[ScheduleName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[SendTo] [text] COLLATE Latin1_General_CI_AS NULL ,
	[Subject] [text] COLLATE Latin1_General_CI_AS NULL ,
	[DateSent] [smalldatetime] NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[LogID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [EventAttr] (
	[EventID] [int] NOT NULL CONSTRAINT [DF__EventAttr__Event__36B12243] DEFAULT (0),
	[EventName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Parent] [int] NULL CONSTRAINT [DF__EventAttr__Paren__37A5467C] DEFAULT (0),
	[EventType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[FireWhen] [int] NULL CONSTRAINT [DF__EventAttr__FireW__38996AB5] DEFAULT (0),
	[FilePath] [varchar] (255) COLLATE Latin1_General_CI_AS NULL ,
	[ModDateTime] [smalldatetime] NULL ,
	[Search1] [text] COLLATE Latin1_General_CI_AS NULL ,
	[Search2] [text] COLLATE Latin1_General_CI_AS NULL ,
	[PopServer] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[PopUserID] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[PopPassword] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Status] [int] NULL CONSTRAINT [DF__EventAttr__Statu__398D8EEE] DEFAULT (0),
	[Owner] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[PopRemove] [int] NULL CONSTRAINT [DF__EventAttr__PopRe__3A81B327] DEFAULT (0),
	[Description] [text] COLLATE Latin1_General_CI_AS NULL ,
	[KeyWord] [text] COLLATE Latin1_General_CI_AS NULL ,
	[DisabledDate] [smalldatetime] NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[EventID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [EventHistory] (
	[HistoryID] [int] NOT NULL CONSTRAINT [DF__EventHist__Histo__3D5E1FD2] DEFAULT (0),
	[EventID] [int] NULL CONSTRAINT [DF__EventHist__Event__3E52440B] DEFAULT (0),
	[LastFired] [smalldatetime] NULL ,
	[Status] [int] NULL CONSTRAINT [DF__EventHist__Statu__3F466844] DEFAULT (0),
	[ErrMsg] [text] COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[HistoryID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [EventSchedule] (
	[ID] [int] NOT NULL CONSTRAINT [DF__EventSchedul__ID__4222D4EF] DEFAULT (0),
	[EventID] [int] NULL CONSTRAINT [DF__EventSche__Event__4316F928] DEFAULT (0),
	[ScheduleID] [int] NULL CONSTRAINT [DF__EventSche__Sched__440B1D61] DEFAULT (0),
	[ScheduleType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Status] [int] NULL CONSTRAINT [DF__EventSche__Statu__44FF419A] DEFAULT (0),
	 PRIMARY KEY  CLUSTERED 
	(
		[ID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO


CREATE TABLE [Folders] (
	[FolderID] [int] NOT NULL CONSTRAINT [DF__Folders__FolderI__47DBAE45] DEFAULT (0),
	[FolderName] [varchar] (255) COLLATE Latin1_General_CI_AS NULL ,
	[Parent] [int] NULL CONSTRAINT [DF__Folders__Parent__48CFD27E] DEFAULT (0),
	CONSTRAINT [PK__Folders__46E78A0C] PRIMARY KEY  CLUSTERED 
	(
		[FolderID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO


CREATE TABLE [FormulaAttr] (
	[FormulaID] [int] NOT NULL CONSTRAINT [DF__FormulaAt__Formu__4BAC3F29] DEFAULT (0),
	[FormulaName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[FormulaDef] [text] COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[FormulaID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [HouseKeepingAttr] (
	[HouseID] [int] NOT NULL CONSTRAINT [DF__HouseKeep__House__4E88ABD4] DEFAULT (0),
	[DestinationID] [int] NULL CONSTRAINT [DF__HouseKeep__Desti__4F7CD00D] DEFAULT (0),
	[AgeValue] [int] NULL CONSTRAINT [DF__HouseKeep__AgeVa__5070F446] DEFAULT (0),
	[AgeUnit] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[HouseID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO


CREATE TABLE [PackageAttr] (
	[PackID] [int] NOT NULL CONSTRAINT [DF__PackageAt__PackI__534D60F1] DEFAULT (0),
	[PackageName] [text] COLLATE Latin1_General_CI_AS NULL ,
	[Parent] [int] NULL CONSTRAINT [DF__PackageAt__Paren__5441852A] DEFAULT (0),
	[Retry] [int] NULL CONSTRAINT [DF__PackageAt__Retry__5535A963] DEFAULT (0),
	[AssumeFail] [int] NULL CONSTRAINT [DF__PackageAt__Assum__5629CD9C] DEFAULT (0),
	[CheckBlank] [int] NULL CONSTRAINT [DF__PackageAt__Check__571DF1D5] DEFAULT (0),
	[Owner] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[FailOnOne] [int] NULL CONSTRAINT [DF__PackageAt__FailO__5812160E] DEFAULT (0),
	[MergePDF] [int] NULL CONSTRAINT [DF__PackageAt__Merge__59063A47] DEFAULT (0),
	[MergeXL] [int] NULL CONSTRAINT [DF__PackageAt__Merge__59FA5E80] DEFAULT (0),
	[MergePDFName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[MergeXLName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[DateTimeStamp] [int] NULL CONSTRAINT [DF__PackageAt__DateT__5AEE82B9] DEFAULT (0),
	[StampFormat] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[PackID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [PackagedReportAttr] (
	[AttrID] [int] NOT NULL CONSTRAINT [DF__PackagedR__AttrI__6A30C649] DEFAULT (0),
	[PackID] [int] NULL CONSTRAINT [DF__PackagedR__PackI__6B24EA82] DEFAULT (0),
	[ReportID] [int] NULL CONSTRAINT [DF__PackagedR__Repor__6C190EBB] DEFAULT (0),
	[OutputFormat] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Customext] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[AppendDateTime] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[DateTimeFormat] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[CustomName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[AttrID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO


CREATE TABLE [PackageOptions] (
	[OptionID] [int] NOT NULL CONSTRAINT [DF__PackageOp__Optio__5DCAEF64] DEFAULT (0),
	[PackID] [int] NULL CONSTRAINT [DF__PackageOp__PackI__5EBF139D] DEFAULT (0),
	[PDFPassword] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[UserPassword] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[PDFWatermark] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[CanPrint] [int] NULL CONSTRAINT [DF__PackageOp__CanPr__5FB337D6] DEFAULT (0),
	[CanCopy] [int] NULL CONSTRAINT [DF__PackageOp__CanCo__60A75C0F] DEFAULT (0),
	[CanEdit] [int] NULL CONSTRAINT [DF__PackageOp__CanEd__619B8048] DEFAULT (0),
	[CanNotes] [int] NULL CONSTRAINT [DF__PackageOp__CanNo__628FA481] DEFAULT (0),
	[CanFill] [int] NULL CONSTRAINT [DF__PackageOp__CanFi__6383C8BA] DEFAULT (0),
	[CanAccess] [int] NULL CONSTRAINT [DF__PackageOp__CanAc__6477ECF3] DEFAULT (0),
	[CanAssemble] [int] NULL CONSTRAINT [DF__PackageOp__CanAs__656C112C] DEFAULT (0),
	[CanPrintFull] [int] NULL CONSTRAINT [DF__PackageOp__CanPr__66603565] DEFAULT (0),
	[PDFSecurity] [int] NULL CONSTRAINT [DF__PackageOp__PDFSe__6754599E] DEFAULT (0),
	[InfoTitle] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[InfoAuthor] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[InfoSubject] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[InfoProducer] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[InfoKeywords] [text] COLLATE Latin1_General_CI_AS NULL ,
	[InfoCreated] [smalldatetime] NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[OptionID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [PrinterAttr] (
	[PrinterID] [int] NOT NULL CONSTRAINT [DF__PrinterAt__Print__6EF57B66] DEFAULT (0),
	[DestinationID] [int] NULL CONSTRAINT [DF__PrinterAt__Desti__6FE99F9F] DEFAULT (0),
	[PrinterName] [text] COLLATE Latin1_General_CI_AS NULL ,
	[Orientation] [varchar] (25) COLLATE Latin1_General_CI_AS NULL ,
	[PrinterDriver] [text] COLLATE Latin1_General_CI_AS NULL ,
	[PrinterPort] [text] COLLATE Latin1_General_CI_AS NULL ,
	[PageFrom] [int] NULL CONSTRAINT [DF__PrinterAt__PageF__70DDC3D8] DEFAULT (0),
	[PageTo] [int] NULL CONSTRAINT [DF__PrinterAt__PageT__71D1E811] DEFAULT (0),
	[Copies] [int] NULL CONSTRAINT [DF__PrinterAt__Copie__72C60C4A] DEFAULT (0),
	[Collate] [int] NULL CONSTRAINT [DF__PrinterAt__Colla__73BA3083] DEFAULT (0),
	[PagesPerReport] [int] NULL CONSTRAINT [DF__PrinterAt__Pages__74AE54BC] DEFAULT (0),
	[PrintMethod] [varchar] (25) COLLATE Latin1_General_CI_AS NULL ,
	[PaperSize] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[PrinterID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [RefresherAttr] (
	[RefreshID] [int] NOT NULL CONSTRAINT [DF__Refresher__Refre__778AC167] DEFAULT (0),
	[ObjectType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Frequency] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[ObjectName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[ObjectID] [int] NULL CONSTRAINT [DF__Refresher__Objec__787EE5A0] DEFAULT (0),
	[LastRun] [smalldatetime] NULL ,
	[NextRun] [smalldatetime] NULL ,
	[RepeatEvery] [int] NULL CONSTRAINT [DF__Refresher__Repea__797309D9] DEFAULT (0),
	[StartTime] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[RefreshID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO


CREATE TABLE [RemoteSysAttr] (
	[RemoteID] [int] NOT NULL CONSTRAINT [DF__RemoteSys__Remot__7C4F7684] DEFAULT (0),
	[PCName] [varchar] (55) COLLATE Latin1_General_CI_AS NULL ,
	[LivePath] [text] COLLATE Latin1_General_CI_AS NULL ,
	[AutoConnect] [int] NULL CONSTRAINT [DF__RemoteSys__AutoC__7D439ABD] DEFAULT (0),
	 PRIMARY KEY  CLUSTERED 
	(
		[RemoteID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [ReportAttr] (
	[ReportID] [int] NOT NULL CONSTRAINT [DF__ReportAtt__Repor__00200768] DEFAULT (0),
	[DatabasePath] [varchar] (255) COLLATE Latin1_General_CI_AS NULL ,
	[ReportName] [text] COLLATE Latin1_General_CI_AS NULL ,
	[PackID] [int] NULL CONSTRAINT [DF__ReportAtt__PackI__01142BA1] DEFAULT (0),
	[Parent] [int] NULL CONSTRAINT [DF__ReportAtt__Paren__02084FDA] DEFAULT (0),
	[DatabasePassword] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[ReportTitle] [text] COLLATE Latin1_General_CI_AS NULL ,
	[Retry] [int] NULL CONSTRAINT [DF__ReportAtt__Retry__02FC7413] DEFAULT (0),
	[AssumeFail] [int] NULL CONSTRAINT [DF__ReportAtt__Assum__03F0984C] DEFAULT (0),
	[CheckBlank] [int] NULL CONSTRAINT [DF__ReportAtt__Check__04E4BC85] DEFAULT (0),
	[SelectionFormula] [text] COLLATE Latin1_General_CI_AS NULL ,
	[Dynamic] [int] NULL CONSTRAINT [DF__ReportAtt__Dynam__05D8E0BE] DEFAULT (0),
	[IncludeAttach] [int] NULL CONSTRAINT [DF__ReportAtt__Inclu__06CD04F7] DEFAULT (0),
	[Owner] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[RptUserID] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[RptPassword] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[RptServer] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[RptDatabase] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[UseLogin] [int] NULL CONSTRAINT [DF__ReportAtt__UseLo__07C12930] DEFAULT (0),
	[UseSavedData] [int] NULL CONSTRAINT [DF__ReportAtt__UseSa__08B54D69] DEFAULT (0),
	[CachePath] [text] COLLATE Latin1_General_CI_AS NULL ,
	[LastRefreshed] [smalldatetime] NULL ,
	[AutoRefresh] [int] NULL CONSTRAINT [DF__ReportAtt__AutoR__09A971A2] DEFAULT (0),
	 PRIMARY KEY  CLUSTERED 
	(
		[ReportID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [ReportOptions] (
	[OptionID] [int] NOT NULL CONSTRAINT [DF__ReportOpt__Optio__0C85DE4D] DEFAULT (0),
	[HTMLMerge] [int] NULL CONSTRAINT [DF__ReportOpt__HTMLM__0D7A0286] DEFAULT (0),
	[PDFWatermark] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[PDFPassword] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[DestinationID] [int] NULL CONSTRAINT [DF__ReportOpt__Desti__0E6E26BF] DEFAULT (0),
	[ReportID] [int] NULL CONSTRAINT [DF__ReportOpt__Repor__0F624AF8] DEFAULT (0),
	[ColWidth] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[ExportHF] [int] NULL CONSTRAINT [DF__ReportOpt__Expor__10566F31] DEFAULT (0),
	[PageBreak] [int] NULL CONSTRAINT [DF__ReportOpt__PageB__114A936A] DEFAULT (0),
	[ConvertDate] [int] NULL CONSTRAINT [DF__ReportOpt__Conve__123EB7A3] DEFAULT (0),
	[PageFrom] [int] NULL CONSTRAINT [DF__ReportOpt__PageF__1332DBDC] DEFAULT (0),
	[PageTo] [int] NULL CONSTRAINT [DF__ReportOpt__PageT__14270015] DEFAULT (0),
	[sCharacter] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[sDelimiter] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[ODBC] [text] COLLATE Latin1_General_CI_AS NULL ,
	[TableName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[LinesPerPage] [int] NULL CONSTRAINT [DF__ReportOpt__Lines__151B244E] DEFAULT (0),
	[UserPassword] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[CanPrint] [int] NULL CONSTRAINT [DF__ReportOpt__CanPr__160F4887] DEFAULT (0),
	[CanCopy] [int] NULL CONSTRAINT [DF__ReportOpt__CanCo__17036CC0] DEFAULT (0),
	[CanEdit] [int] NULL CONSTRAINT [DF__ReportOpt__CanEd__17F790F9] DEFAULT (0),
	[CanNotes] [int] NULL CONSTRAINT [DF__ReportOpt__CanNo__18EBB532] DEFAULT (0),
	[CanFill] [int] NULL CONSTRAINT [DF__ReportOpt__CanFi__19DFD96B] DEFAULT (0),
	[CanAccess] [int] NULL CONSTRAINT [DF__ReportOpt__CanAc__1AD3FDA4] DEFAULT (0),
	[CanAssemble] [int] NULL CONSTRAINT [DF__ReportOpt__CanAs__1BC821DD] DEFAULT (0),
	[CanPrintFull] [int] NULL CONSTRAINT [DF__ReportOpt__CanPr__1CBC4616] DEFAULT (0),
	[PDFSecurity] [int] NULL CONSTRAINT [DF__ReportOpt__PDFSe__1DB06A4F] DEFAULT (0),
	[HTMLNavigator] [int] NULL CONSTRAINT [DF__ReportOpt__HTMLN__1EA48E88] DEFAULT (0),
	[WorkSheetName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[InfoTitle] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[InfoAuthor] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[InfoSubject] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[InfoProducer] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[InfoKeywords] [text] COLLATE Latin1_General_CI_AS NULL ,
	[InfoCreated] [smalldatetime] NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[OptionID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [ReportParameter] (
	[ParID] [int] NOT NULL CONSTRAINT [DF__ReportPar__ParID__2180FB33] DEFAULT (0),
	[ReportID] [int] NULL CONSTRAINT [DF__ReportPar__Repor__22751F6C] DEFAULT (0),
	[ParName] [text] COLLATE Latin1_General_CI_AS NULL ,
	[ParValue] [text] COLLATE Latin1_General_CI_AS NULL ,
	[ParType] [int] NULL CONSTRAINT [DF__ReportPar__ParTy__236943A5] DEFAULT (0),
	 PRIMARY KEY  CLUSTERED 
	(
		[ParID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [ReportSnapshots] (
	[SnapID] [int] NOT NULL CONSTRAINT [DF__ReportSna__SnapI__2645B050] DEFAULT (0),
	[ReportID] [int] NULL CONSTRAINT [DF__ReportSna__Repor__2739D489] DEFAULT (0),
	[KeepSnap] [int] NULL CONSTRAINT [DF__ReportSna__KeepS__282DF8C2] DEFAULT (0),
	 PRIMARY KEY  CLUSTERED 
	(
		[SnapID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO


CREATE TABLE [ScheduleAttr] (
	[ScheduleID] [int] NOT NULL CONSTRAINT [DF__ScheduleA__Sched__2FCF1A8A] DEFAULT (0),
	[Frequency] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[StartDate] [smalldatetime] NULL ,
	[EndDate] [smalldatetime] NULL ,
	[NextRun] [smalldatetime] NULL ,
	[StartTime] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Repeat] [int] NULL CONSTRAINT [DF__ScheduleA__Repea__30C33EC3] DEFAULT (0),
	[RepeatInterval] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[RepeatUntil] [smalldatetime] NULL ,
	[Status] [int] NULL CONSTRAINT [DF__ScheduleA__Statu__31B762FC] DEFAULT (0),
	[ReportID] [int] NULL CONSTRAINT [DF__ScheduleA__Repor__32AB8735] DEFAULT (0),
	[PackID] [int] NULL CONSTRAINT [DF__ScheduleA__PackI__339FAB6E] DEFAULT (0),
	[AutoID] [int] NULL CONSTRAINT [DF__ScheduleA__AutoI__3493CFA7] DEFAULT (0),
	[Description] [text] COLLATE Latin1_General_CI_AS NULL ,
	[KeyWord] [text] COLLATE Latin1_General_CI_AS NULL ,
	[CalendarName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[DisabledDate] [smalldatetime] NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[ScheduleID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [ScheduleHistory] (
	[HistoryID] [int] NOT NULL CONSTRAINT [DF__ScheduleH__Histo__37703C52] DEFAULT (0),
	[ReportID] [int] NULL CONSTRAINT [DF__ScheduleH__Repor__3864608B] DEFAULT (0),
	[PackID] [int] NULL CONSTRAINT [DF__ScheduleH__PackI__395884C4] DEFAULT (0),
	[AutoID] [int] NULL CONSTRAINT [DF__ScheduleH__AutoI__3A4CA8FD] DEFAULT (0),
	[EntryDate] [smalldatetime] NULL ,
	[Success] [int] NULL CONSTRAINT [DF__ScheduleH__Succe__3B40CD36] DEFAULT (0),
	[ErrMsg] [text] COLLATE Latin1_General_CI_AS NULL ,
	[StartDate] [smalldatetime] NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[HistoryID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [ScheduleOptions] (
	[OptionID] [int] NOT NULL CONSTRAINT [DF__ScheduleO__Optio__3E1D39E1] DEFAULT (0),
	[WeekNo] [int] NULL CONSTRAINT [DF__ScheduleO__WeekN__3F115E1A] DEFAULT (0),
	[DayNo] [int] NULL CONSTRAINT [DF__ScheduleO__DayNo__40058253] DEFAULT (0),
	[MonthsIn] [text] COLLATE Latin1_General_CI_AS NULL ,
	[nCount] [int] NULL CONSTRAINT [DF__ScheduleO__nCoun__40F9A68C] DEFAULT (0),
	[ScheduleID] [int] NULL CONSTRAINT [DF__ScheduleO__Sched__41EDCAC5] DEFAULT (0),
	[ScheduleType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[OptionID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [SendWait] (
	[QueueNumber] [int] NOT NULL CONSTRAINT [DF__SendWait__QueueN__44CA3770] DEFAULT (0),
	[Addresses] [text] COLLATE Latin1_General_CI_AS NULL ,
	[Subject] [text] COLLATE Latin1_General_CI_AS NULL ,
	[Body] [text] COLLATE Latin1_General_CI_AS NULL ,
	[SendType] [varchar] (55) COLLATE Latin1_General_CI_AS NULL ,
	[Attach] [text] COLLATE Latin1_General_CI_AS NULL ,
	[Numattach] [int] NULL CONSTRAINT [DF__SendWait__Numatt__45BE5BA9] DEFAULT (0),
	[Extras] [text] COLLATE Latin1_General_CI_AS NULL ,
	[SendCC] [text] COLLATE Latin1_General_CI_AS NULL ,
	[SendBCC] [text] COLLATE Latin1_General_CI_AS NULL ,
	[sName] [text] COLLATE Latin1_General_CI_AS NULL ,
	[InBody] [varchar] (15) COLLATE Latin1_General_CI_AS NULL ,
	[sFormat] [varchar] (25) COLLATE Latin1_General_CI_AS NULL ,
	[FileList] [text] COLLATE Latin1_General_CI_AS NULL ,
	[LastResult] [text] COLLATE Latin1_General_CI_AS NULL ,
	[LastAttempt] [smalldatetime] NULL ,
	[MailFormat] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[IncludeAttach] [int] NULL CONSTRAINT [DF__SendWait__Includ__46B27FE2] DEFAULT (0),
	[SMTPName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[QueueNumber]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [Slaves] (
	[SlaveID] [int] NOT NULL CONSTRAINT [DF__Slaves__SlaveID__498EEC8D] DEFAULT (0),
	[SlaveName] [varchar] (255) COLLATE Latin1_General_CI_AS NULL ,
	[SlavePath] [text] COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[SlaveID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [SmartFolderAttr] (
	[SmartDefID] [int] NOT NULL CONSTRAINT [DF__SmartFold__Smart__4C6B5938] DEFAULT (0),
	[SmartID] [int] NULL CONSTRAINT [DF__SmartFold__Smart__4D5F7D71] DEFAULT (0),
	[SmartColumn] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[SmartCriteria] [text] COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[SmartDefID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [SmartFolders] (
	[SmartID] [int] NOT NULL CONSTRAINT [DF__SmartFold__Smart__503BEA1C] DEFAULT (0),
	[SmartName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[SmartDesc] [text] COLLATE Latin1_General_CI_AS NULL ,
	[SmartType] [varchar] (10) COLLATE Latin1_General_CI_AS NULL ,
	[Owner] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[SmartID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [SMTPServers] (
	[SMTPID] [int] NOT NULL CONSTRAINT [DF__SMTPServe__SMTPI__2B0A656D] DEFAULT (0),
	[SMTPName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[SMTPServer] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[SMTPUser] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[SMTPPassword] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[SMTPSenderName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[SMTPSenderAddress] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[SMTPTimeout] [int] NULL CONSTRAINT [DF__SMTPServe__SMTPT__2BFE89A6] DEFAULT (0),
	[IsBackup] [int] NULL CONSTRAINT [DF__SMTPServe__IsBac__2CF2ADDF] DEFAULT (0),
	 PRIMARY KEY  CLUSTERED 
	(
		[SMTPID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO


CREATE TABLE [SnapshotsAttr] (
	[AttrID] [int] NOT NULL CONSTRAINT [DF__Snapshots__AttrI__531856C7] DEFAULT (0),
	[ReportID] [int] NULL CONSTRAINT [DF__Snapshots__Repor__540C7B00] DEFAULT (0),
	[SnapPath] [text] COLLATE Latin1_General_CI_AS NULL ,
	[DateCreated] [smalldatetime] NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[AttrID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [SubReportLogin] (
	[SubLoginID] [int] NOT NULL CONSTRAINT [DF__SubReport__SubLo__56E8E7AB] DEFAULT (0),
	[ReportID] [int] NULL CONSTRAINT [DF__SubReport__Repor__57DD0BE4] DEFAULT (0),
	[SubName] [text] COLLATE Latin1_General_CI_AS NULL ,
	[RptServer] [text] COLLATE Latin1_General_CI_AS NULL ,
	[RptDatabase] [text] COLLATE Latin1_General_CI_AS NULL ,
	[RptUserID] [text] COLLATE Latin1_General_CI_AS NULL ,
	[RptPassword] [text] COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[SubLoginID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [SubReportParameters] (
	[SubParID] [int] NOT NULL CONSTRAINT [DF__SubReport__SubPa__5AB9788F] DEFAULT (0),
	[ReportID] [int] NULL CONSTRAINT [DF__SubReport__Repor__5BAD9CC8] DEFAULT (0),
	[SubName] [text] COLLATE Latin1_General_CI_AS NULL ,
	[ParName] [text] COLLATE Latin1_General_CI_AS NULL ,
	[ParValue] [text] COLLATE Latin1_General_CI_AS NULL ,
	[ParType] [int] NULL CONSTRAINT [DF__SubReport__ParTy__5CA1C101] DEFAULT (0),
	 PRIMARY KEY  CLUSTERED 
	(
		[SubParID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [TaskManager] (
	[MonitorID] [int] NOT NULL CONSTRAINT [DF__TaskManag__Monit__5F7E2DAC] DEFAULT (0),
	[ReportID] [int] NULL CONSTRAINT [DF__TaskManag__Repor__607251E5] DEFAULT (0),
	[PackID] [int] NULL CONSTRAINT [DF__TaskManag__PackI__6166761E] DEFAULT (0),
	[EntryDate] [smalldatetime] NULL ,
	[PCName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Status] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[MonitorID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO


CREATE TABLE [Tasks] (
	[TaskID] [int] NOT NULL CONSTRAINT [DF__Tasks__TaskID__6442E2C9] DEFAULT (0),
	[ScheduleID] [int] NULL CONSTRAINT [DF__Tasks__ScheduleI__65370702] DEFAULT (0),
	[TaskType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[TaskName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[ProgramPath] [varchar] (255) COLLATE Latin1_General_CI_AS NULL ,
	[ProgramParameters] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[WindowStyle] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[PrinterName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Filelist] [text] COLLATE Latin1_General_CI_AS NULL ,
	[PauseInt] [int] NULL CONSTRAINT [DF__Tasks__PauseInt__662B2B3B] DEFAULT (0),
	[ReplaceFiles] [int] NULL CONSTRAINT [DF__Tasks__ReplaceFi__671F4F74] DEFAULT (0),
	[Msg] [text] COLLATE Latin1_General_CI_AS NULL ,
	[SendTo] [text] COLLATE Latin1_General_CI_AS NULL ,
	[CC] [text] COLLATE Latin1_General_CI_AS NULL ,
	[Bcc] [text] COLLATE Latin1_General_CI_AS NULL ,
	[Subject] [varchar] (255) COLLATE Latin1_General_CI_AS NULL ,
	[OrderID] [int] NULL CONSTRAINT [DF__Tasks__OrderID__681373AD] DEFAULT (0),
	[FTPServer] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[FTPPort] [int] NULL CONSTRAINT [DF__Tasks__FTPPort__690797E6] DEFAULT (0),
	[FTPUser] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[FTPPassword] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[FTPDirectory] [varchar] (255) COLLATE Latin1_General_CI_AS NULL ,
	[RunWhen] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[AfterType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[TaskID]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [tmpFolders] (
	[FolderID] [int] NULL CONSTRAINT [DF__tmpFolder__Folde__76619304] DEFAULT (0),
	[FolderName] [text] COLLATE Latin1_General_CI_AS NULL ,
	[Parent] [int] NULL CONSTRAINT [DF__tmpFolder__Paren__7755B73D] DEFAULT (0)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [UserColumns] (
	[ColumnID] [int] NOT NULL CONSTRAINT [DF__UserColum__Colum__6BE40491] DEFAULT (0),
	[Caption] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[DataColumn] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[Owner] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED 
	(
		[ColumnID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO


CREATE TABLE [UserView] (
	[ViewID] [int] NOT NULL CONSTRAINT [DF__UserView__ViewID__6EC0713C] DEFAULT (0),
	[UserID] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[ReportID] [int] NULL CONSTRAINT [DF__UserView__Report__6FB49575] DEFAULT (0),
	[PackID] [int] NULL CONSTRAINT [DF__UserView__PackID__70A8B9AE] DEFAULT (0),
	[AutoID] [int] NULL CONSTRAINT [DF__UserView__AutoID__719CDDE7] DEFAULT (0),
	[FolderID] [int] NULL CONSTRAINT [DF__UserView__Folder__72910220] DEFAULT (0),
	[SmartID] [int] NULL CONSTRAINT [DF__UserView__SmartI__73852659] DEFAULT (0),
	[EventID] [int] NULL CONSTRAINT [DF__UserView__EventI__74794A92] DEFAULT (0),
	 PRIMARY KEY  CLUSTERED 
	(
		[ViewID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO


