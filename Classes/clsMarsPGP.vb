Imports System.IO
Imports SBPGP
Friend Class clsMarsPGP

    Sub New()
        SBUtils.Unit.SetLicenseKey("625C9EAEB01E560A1A398A04B15E82AA5DF82346FB0C754F918F552E7B7FE9BFC49FE6378178334793379FB0226FBE08A9E340C7DBB89C7A765B06F03D991E13B997AD4EF32C5FDF6B10EA28CEBA7F3B134325B887C2D6B004E9A540754770FA7B24651DF86C51F7989167CE7F63D9BAE19285AB92A463AF8660D1A675E007DB77CDAA5D4FD161E1819EB791173719B3FA9FBF4D6B9C0F277A1EF4E297FCA1632C8F2BE43B1D920312B7446CF18DB861F3B7A494021B59105D3EC53B72FAB1F6422CEEB15CD1AD8EB3F5DC1F49F3C393705395D78BAAB02A991D7BDB815AD71C06FEDC25561659C0D86C591CE501B497C50FB0031655D89FFC87E040BA9DB076")
    End Sub
    Public Function EncryptFile(inFile As String, nDestinationID As Integer) As Object()
        Dim oResult(1) As Object

        Try

            Dim inF, outF As System.IO.FileStream
            Dim info As System.IO.FileInfo
            Dim pgpWriter As TElPGPWriter = New TElPGPWriter

            pgpWriter.Armor = True
            pgpWriter.ArmorHeaders.Clear()
            pgpWriter.ArmorHeaders.Add("Version: EldoS OpenPGPBlackbox (.NET edition)")
            pgpWriter.ArmorBoundary = "PGP MESSAGE"
            pgpWriter.Compress = True

            Dim pubKeyring As SBPGPKeys.TElPGPKeyring = New SBPGPKeys.TElPGPKeyring
            Dim keyring As SBPGPKeys.TElPGPKeyring = New SBPGPKeys.TElPGPKeyring


            Dim oRs As ADODB.Recordset

            oRs = clsMarsData.GetData("SELECT * FROM PGPAttr WHERE DestinationID = " & nDestinationID)

            Dim sUserID As String
            Dim sKeyLoc As String
            Dim outFile As String = GetDirectory(inFile) & IO.Path.GetRandomFileName & ".pgp"
            Dim AddExt As Boolean = False


            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                sUserID = oRs("pgpid").Value
                sKeyLoc = oRs("keylocation").Value

                Try
                    AddExt = oRs("pgpext").Value
                Catch : End Try

                If AddExt = True Then
                    oResult(1) = ".pgp"
                Else
                    oResult(1) = ""
                End If

                Dim PubRing As String
                Dim SecRing As String

                If sKeyLoc.Contains("|") Then
                    PubRing = sKeyLoc.Split("|")(0)
                    SecRing = sKeyLoc.Split("|")(1)
                Else
                    If sKeyLoc.EndsWith("\") = False Then sKeyLoc &= "\"

                    PubRing = sKeyLoc & "pubring.pkr"
                    SecRing = sKeyLoc & "secring.skr"
                End If

                keyring.Load(PubRing, SecRing)

                pubKeyring.AddPublicKey(keyring.PublicKeys(0))
                pubKeyring.AddSecretKey(keyring.SecretKeys(0))

                Dim secKeyring As SBPGPKeys.TElPGPKeyring = New SBPGPKeys.TElPGPKeyring
                secKeyring.AddSecretKey(keyring.SecretKeys(0))

                pgpWriter.EncryptingKeys = pubKeyring
                pgpWriter.SigningKeys = secKeyring

                pgpWriter.EncryptionType = SBPGP.TSBPGPEncryptionType.etPublicKey

                info = New System.IO.FileInfo(inFile)
                pgpWriter.Filename = info.Name
                pgpWriter.InputIsText = True
                pgpWriter.Passphrases.Clear()

                'pgpWriter.Passphrases.Add(sUserID)

                pgpWriter.Protection = SBPGPConstants.TSBPGPProtectionType.ptHigh

                pgpWriter.SignBufferingMethod = SBPGP.TSBPGPSignBufferingMethod.sbmRewind


                pgpWriter.SymmetricKeyAlgorithm = SBPGPConstants.Unit.SB_PGP_ALGORITHM_SK_AES256

                pgpWriter.Timestamp = DateTime.Now
                pgpWriter.UseNewFeatures = False
                pgpWriter.UseOldPackets = False

                inF = New System.IO.FileStream(inFile, FileMode.Open)

                outF = New System.IO.FileStream(outFile, FileMode.Create)

                pgpWriter.Encrypt(inF, outF, 0)

                outF.Close()

                inF.Close()


                If IO.File.Exists(inFile & oResult(1)) Then
                    IO.File.Delete(inFile & oResult(1))
                End If

                IO.File.Move(outFile, inFile & oResult(1))

                oResult(0) = True

                Return oResult
            Else
                oResult(0) = True
                oResult(1) = ""

                Return oResult
            End If
        Catch ex As Exception

            gErrorDesc = ex.Message
            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
            gErrorLine = Erl()
            gErrorNumber = Err.Number

            oResult(0) = False
            oResult(1) = ""

            Return oResult
        End Try
    End Function
    Public Function EncryptFileOld(ByVal inFile As String, _
    ByVal nDestinationID As Integer) As Object()



        Dim oResult(1) As Object

        Try
10:         Dim oPGP As PGPEncode.PGPEncode = New PGPEncode.PGPEncode
            Dim sError As String
            Dim oSuccess As Boolean
            Dim sResult As String
            Dim oRs As ADODB.Recordset



20:         oRs = clsMarsData.GetData("SELECT * FROM PGPAttr WHERE DestinationID = " & nDestinationID)

            Dim sUserID As String
            Dim sKeyLoc As String
30:         Dim outFile As String = GetDirectory(inFile) & clsMarsData.CreateDataID("", "") & ".pgp"
            Dim AddExt As Boolean = False


40:         If oRs IsNot Nothing AndAlso oRs.EOF = False Then
50:             sUserID = oRs("pgpid").Value
60:             sKeyLoc = oRs("keylocation").Value



                Try
61:                 AddExt = oRs("pgpext").Value
                Catch : End Try

                If AddExt = True Then
62:                 oResult(1) = ".pgp"
                Else
                    oResult(1) = ""
                End If

                Dim PubRing As String
                Dim SecRing As String

70:             If sKeyLoc.IndexOf("|") > -1 Then
80:                 PubRing = sKeyLoc.Split("|")(0)
90:                 SecRing = sKeyLoc.Split("|")(1)
                Else
100:                If sKeyLoc.EndsWith("\") = False Then sKeyLoc &= "\"

110:                PubRing = sKeyLoc & "pubring.pkr"
120:                SecRing = sKeyLoc & "secring.skr"
                End If
                'encrypt the file
121:            sResult = oPGP.EncodeFile(inFile, outFile, sUserID, PubRing, SecRing)


122:            oSuccess = sResult.Split("|")(0)
123:            sError = sResult.Split("|")(1)

130:            If oSuccess = True Then
140:                If IO.File.Exists(inFile & oResult(1)) Then
141:                    IO.File.Delete(inFile & oResult(1))
142:                End If

150:                IO.File.Move(outFile, inFile & oResult(1))

151:                oResult(0) = True
                    oResult(1) = inFile & oResult(1)

160:                Return oResult
170:            Else


180:                gErrorDesc = sError
190:                gErrorNumber = -214765890
200:                gErrorSource = "clsMarsPGP.EncryptFile"
210:                gErrorLine = 9

211:                oResult(0) = False

220:                Return oResult
                End If
            Else
230:            oResult(0) = True
231:            oResult(1) = ""

232:            Return oResult
            End If
        Catch ex As Exception

            gErrorDesc = ex.Message
            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
            gErrorLine = Erl()
            gErrorNumber = Err.Number

            oResult(0) = False
            oResult(1) = ""

            Return oResult
        End Try

    End Function


End Class
