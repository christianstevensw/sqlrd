Imports System.IO
Imports PDFtoolkitX
Imports PDF2ManyX
Imports System.Management
Imports Gnostice.PDFOne
Imports System.Runtime.InteropServices
Imports System.Drawing.Imaging
Imports System.Collections.Generic
Imports System.Drawing.Printing

Friend Class clsMarsPDF

    <DllImport("gdi32.dll")> _
    Private Shared Function CopyEnhMetaFile(hemfSrc As IntPtr, lpszFile As [String]) As IntPtr
    End Function

    <DllImport("gdi32.dll")> _
    Private Shared Function DeleteEnhMetaFile(hemf As IntPtr) As Boolean
    End Function


    Dim oData As New clsMarsData
    Const pdfOneLicense As String = "629A-3888-32A9-11A3-AB3F-9FE7-44E7-C694" ' "4B3F-5BFD-016D-B261-0526-6C3E-33D4-2E0E" '
    Public metaFiles As List(Of String)
    Dim printingPage As Integer
    Dim lastPage As Integer

    Private Sub ConvertPDFToMetaFileAndSendToPrinter(pdfFile As String, printerName As String, fromPage As Integer, toPage As Integer, copies As Integer)
        ' Create a metafile

#If DEBUG Then
        ' pdfFile = "\\vmware-host\Shared Folders\Downloads\REFERED PATIENT VISITS TEST.pdf"
#End If

        Dim metafile As Metafile
        metaFiles = New List(Of String)

        ' Create a handle to a metafile
        Dim iptrMetafileHandle As IntPtr, iptrMetafileHandleCopy As IntPtr

        ' Load a PDF document
        Dim doc As New PDFDocument(pdfOneLicense)
        ' Load the input document and get its page count
        doc.Load(pdfFile)

        ' Iterate through all pages in the document
        Dim i As Integer = 1

        While i <= doc.GetPageCount()
            ' Export current page as a metafile
            metafile = doc.GetPageMetafile(i)

            ' Get a handle to the metafile
            iptrMetafileHandle = metafile.GetHenhmetafile()

            ' Export metafile to an image file
            Dim metapath As String = IO.Path.Combine(clsMarsReport.m_OutputFolder, Guid.NewGuid.ToString & ".emf")

            iptrMetafileHandleCopy = CopyEnhMetaFile(iptrMetafileHandle, metapath)

            metaFiles.Add(metapath)

            ' Delete source and copied metafiles from memory
            DeleteEnhMetaFile(iptrMetafileHandle)
            DeleteEnhMetaFile(iptrMetafileHandleCopy)

            '        // To save metafile as a raster image,
            '        // comment four previous statements.
            '        // Metafile.Save does not save to EMF/WMF.
            '        Metafile.Save(
            '            "image_of_page_#" + i.ToString() + ".png",
            '            ImageFormat.Png);
            '        

            ' Release any resources used by the metafile
            metafile.Dispose()
            i += 1
        End While

        ' Clean up
        doc.Close()

        doc.Dispose()

        PrintEMFFiles(printerName, fromPage, toPage, copies)

    End Sub

    Public Sub PrintEMFFiles(pName As String, fromPage As Integer, toPage As Integer, nCopies As Integer)
        If metaFiles Is Nothing Or metaFiles.Count <= 0 Then
            Throw New ArgumentException("An image is required to print.")
        End If

        Dim printer As String = pName

        If (String.IsNullOrEmpty(printer)) Then
            Throw New ArgumentException("A printer is required.")
        End If

        lastPage = toPage

        If lastPage = 0 Then lastPage = metaFiles.Count

        Dim printerSettings As PrinterSettings = New PrinterSettings()
        printerSettings.MaximumPage = toPage
        printerSettings.MinimumPage = 1
        printerSettings.PrintRange = PrintRange.AllPages
        printerSettings.PrinterName = pName
        printerSettings.Copies = ncopies

        Dim pd As System.Drawing.Printing.PrintDocument = New System.Drawing.Printing.PrintDocument

        pd.DefaultPageSettings = pd.DefaultPageSettings
        pd.PrinterSettings = printerSettings

        AddHandler pd.PrintPage, AddressOf pd_PrintPage

        pd.Print()

    End Sub

    Private Sub pd_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs)

        Dim page As System.Drawing.Imaging.Metafile = System.Drawing.Imaging.Metafile.FromFile(metaFiles(printingPage))
        Dim h, w As Integer

        h = page.Height
        w = page.Width

        If h < w Then e.PageSettings.Landscape = True


        Dim ScaleFac As Integer = 100
        While (ScaleFac * page.Width / page.HorizontalResolution > e.PageBounds.Width Or ScaleFac * page.Height / page.VerticalResolution > e.PageBounds.Height) And ScaleFac > 2
            ScaleFac -= 1
        End While

        Dim sz As New SizeF(ScaleFac * page.Width / page.HorizontalResolution, ScaleFac * page.Height / page.VerticalResolution)
        Dim p As New PointF((e.PageBounds.Width - sz.Width) / 2, (e.PageBounds.Height - sz.Height) / 2)

        e.Graphics.DrawImage(page, p)

        ' e.Graphics.DrawImage(page, 0, 0, w, h)

        printingPage += 1

        If printingPage >= lastPage Or printingPage >= metaFiles.Count Then
            e.HasMorePages = False
        Else
            e.HasMorePages = True
        End If

    End Sub


    Public Function IsPrinterReady(PrinterName As String) As Boolean
        Try
            Dim checkPrinterStatus As Boolean = Convert.ToInt16(clsMarsUI.MainUI.ReadRegistry("CheckPrinterBeforePrinting", "1"))

            If checkPrinterStatus Then
                Dim scope As New ManagementScope("\root\cimv2")
                scope.Connect()
                Dim searcher As New ManagementObjectSearcher("select * from Win32_Printer")

                Dim strPrinter As String = String.Empty
                For Each printer As ManagementObject In searcher.[Get]()
                    strPrinter = printer("Name").ToString()
                    If strPrinter.ToLower() = PrinterName.ToLower() Then
                        Return printer("PrinterStatus").ToString().ToLower().Equals("3")
                    End If
                Next
                Return False
            Else
                Return True
            End If
        Catch
            Return True
        End Try

    End Function

    Public Function MergePDFFiles_A(ByVal sFiles() As String, ByVal sOutput As String, Optional ByVal AddBookmarks As Boolean = False, _
           Optional ByVal sParentTree As String = "", Optional ByVal isDataDriven As Boolean = False, Optional sortByDateCreated As Boolean = False) As String
10:     Try
            Dim nPages() As Integer
            Dim sGroups() As String
            Dim outputFolder As String = clsMarsReport.m_OutputFolder & "\{" & Guid.NewGuid.ToString & "}\"

20:         clsMarsParser.Parser.ParseDirectory(outputFolder)

            If isDataDriven Then clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Output folder: " & outputFolder, True)

            Dim finalPDF As String = outputFolder & sOutput

            If isDataDriven Then clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Final PDF file: " & finalPDF, True)

30:         If IO.File.Exists(finalPDF) Then IO.File.Delete(finalPDF)

            '//lets sort the files out
40:         Dim dt As DataTable = New DataTable
50:         dt.Columns.Add("filename")
60:         dt.Columns.Add("datecreated")

70:         For Each f As String In sFiles
                Dim r As DataRow = dt.Rows.Add
80:             r("filename") = f
90:             r("datecreated") = New IO.FileInfo(f).CreationTime
100:        Next

110:        Using doc As PDFDocument = New PDFDocument(pdfOneLicense)
120:            doc.OpenAfterCreate = False

130:            Dim arr As ArrayList = New ArrayList
                Dim I As Integer = 0
                Dim rows() As DataRow

140:            If sortByDateCreated Then
150:                rows = dt.Select("", "datecreated ASC")
160:            Else
170:                rows = dt.Select("")
                End If


180:            For Each r As DataRow In rows
                    Dim f As String = r("filename")

                    If isDataDriven Then clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Adding file: " & f, True)

190:                arr.Add(f)

200:                If AddBookmarks = True Then
210:                    ReDim Preserve nPages(I)
220:                    ReDim Preserve sGroups(I)

                        If isDataDriven Then clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Loading PDF file: " & f, True)

230:                    doc.Load(f)

240:                    nPages(I) = doc.GetPageCount
250:                    sGroups(I) = IO.Path.GetFileNameWithoutExtension(f)

260:                    doc.Close()
                    End If

270:                I += 1
280:            Next

                '// redundant Dim sMergePath As String = sAppPath & "Output\{" & clsMarsData.CreateDataID("", "") & "}.pdf" 'sOutput

290:            If isDataDriven Then clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Creating instance of PDFDoc", True)

300:            Using doc2 As PDFDocument = New PDFDocument(pdfOneLicense)

310:                If isDataDriven Then clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Invoking merge of all files in array", True)

320:                doc2.Merge(arr)

330:                If isDataDriven Then clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Saving PDFDoc to " & finalPDF, True)

340:                doc2.Save(finalPDF)

350:                If isDataDriven Then clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Closing PDFDoc", True)

360:                doc2.Close()
                End Using


370:            If AddBookmarks = True And isDataDriven = False Then
380:                System.Threading.Thread.Sleep(5000)

390:                Try
                        Dim temp As String = Me.AddBookMarks(finalPDF, nPages, sGroups, sParentTree)

400:                    IO.File.Copy(temp, finalPDF, True)
410:                Catch
420:                    Return finalPDF
                        '  IO.File.Copy(finalPDF, finalPDF, True)
                    End Try
                End If

430:            If isDataDriven Then clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Files HAVE BEEN merged to " & finalPDF, True)

440:            Return finalPDF
            End Using
450:    Catch e As Exception

            _ErrorHandle(e.ToString, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)

460:        clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "PDF Merging error: " & e.Message, True)

470:        Return ""
        End Try
    End Function


    Public Sub printPDFByShell(sFile As String, nCopies As Integer, sPrinter As String)



        For i As Integer = 0 To nCopies - 1
            Dim process As New Process()

            process.StartInfo.FileName = sFile
            process.StartInfo.UseShellExecute = True
            process.StartInfo.CreateNoWindow = True
            process.StartInfo.Verb = "printto"
            process.StartInfo.Arguments = """" & sPrinter & """"
            process.Start()

            process.WaitForInputIdle()

            System.Threading.Thread.Sleep(10000)

            process.Kill()
        Next
    End Sub

    Public Function PrintPDFviaEMF(ByVal sFile As String, ByVal nCopies As Integer, _
         ByVal sPrinter As String, Optional ByVal nPageFrom As Integer = 0, Optional ByVal nPageTo As Integer = 0) As Boolean
        If IsPrinterReady(sPrinter) = False Then
            Throw New Exception("Printer reported as offline by Windows")
        End If


        ConvertPDFToMetaFileAndSendToPrinter(sFile, sPrinter, nPageFrom, nPageTo, nCopies)
    End Function
    Public Function PrintPDF(ByVal sFile As String, ByVal nCopies As Integer, _
           ByVal sPrinter As String, Optional ByVal nPageFrom As Integer = 0, Optional ByVal nPageTo As Integer = 0) As Boolean
        Try
            Dim checkPrinterStatus As Boolean = Convert.ToInt16(clsMarsUI.MainUI.ReadRegistry("PrecheckPrinterStatus", 0))

            If checkPrinterStatus Then
                If IsPrinterReady(sPrinter) = False Then
                    Throw New Exception("Printer reported as offline by Windows")
                End If
            End If

#If DEBUG Then
            sFile = "\\vmware-host\Shared Folders\Downloads\REFERED PATIENT VISITS TEST.pdf"
#End If

            ' ConvertPDFToMetaFileAndSendToPrinter(sFile, sPrinter, nPageFrom, nPageTo, nCopies)


            Dim printByShell As Boolean = clsMarsUI.MainUI.ReadRegistry("PrintUsingShell", 0)


            If printByShell Then
                printPDFByShell(sFile, nCopies, sPrinter)
                Return True
            End If

10:         Dim doc As Gnostice.PDFOne.PDFDocument = New Gnostice.PDFOne.PDFDocument(pdfOneLicense)
20:         Dim pdfprinter As Gnostice.PDFOne.PDFPrinter.PDFPrinter = New Gnostice.PDFOne.PDFPrinter.PDFPrinter
            ' Dim err As Gnostice.PDFOne.PDFPrinter.PrintErrorDelegate
            Dim landscape As Boolean



            '//load the pdf document
30:         doc.Load(sFile)
40:         pdfprinter.LoadDocument(sFile)

50:         If pdfprinter.PDFLoaded = False Then
60:             Throw New Exception("Failed to load document for printing")
            End If

70:         pdfprinter.ReversePageOrder = False

80:         Dim options As Printing.PrinterSettings = New Printing.PrinterSettings

90:         options.PrinterName = sPrinter

100:        options.Copies = nCopies

110:        options.DefaultPageSettings.Margins.Left = 0
120:        options.DefaultPageSettings.Margins.Right = 0
130:        options.DefaultPageSettings.Margins.Top = 0
140:        options.DefaultPageSettings.Margins.Bottom = 0


150:        If nPageFrom = 0 And nPageTo = 0 Then
160:            options.PrintRange = Printing.PrintRange.AllPages

170:            pdfprinter.PrintOptions = options
                pdfprinter.AutoRotate = True
180:            pdfprinter.PageScaleType = Gnostice.PDFOne.PDFPrinter.PrintScaleType.FitToPrintableArea  ' Gnostice.PDFOne.PDFPrinter.PrintScaleType.FitToPrintableArea ' Gnostice.PDFOne.PDFPrinter.PrintScaleType.ReduceToPrintableArea
190:            pdfprinter.Print()
200:        Else
210:            options.PrintRange = Printing.PrintRange.SomePages

220:            If nPageFrom > doc.GetPageCount Then nPageFrom = 1
230:            If nPageTo > doc.GetPageCount Then nPageTo = doc.GetPageCount

240:            options.FromPage = nPageFrom
250:            options.ToPage = nPageTo
260:            pdfprinter.PrintOptions = options
270:            pdfprinter.PageScaleType = Gnostice.PDFOne.PDFPrinter.PrintScaleType.None
280:            pdfprinter.Print()
            End If


290:        pdfprinter.CloseDocument()
300:        doc.Close()
310:        pdfprinter.Dispose()
320:        doc.Dispose()
        Catch ex As Exception
            Dim message As String = "Error in PrintPDF function: " & ex.Message & vbCrLf & Erl()
            Throw New Exception(message)
        End Try
    End Function


    Public Function PrintPDFv1(ByVal sFile As String, ByVal nCopies As Integer, _
    ByVal sPrinter As String, Optional ByVal nPageFrom As Integer = 0, Optional ByVal nPageTo As Integer = 0) As Boolean
        Dim oPDF As iSED.QuickPDFClass
        Dim nOptions As Integer

        oPDF = New iSED.QuickPDFClass

        oPDF.UnlockKey("B34B74C95F359046BEFB789480980C03")

        If oPDF.Unlocked = 1 Then
            oPDF.LoadFromFile(sFile)

            nOptions = oPDF.PrintOptions(1, 1, ExtractFileName(sFile))

            If nPageFrom = 0 And nPageTo = 0 Then
                nPageFrom = 1
                nPageTo = oPDF.PageCount
            End If

            For I As Integer = 1 To nCopies
                oPDF.PrintDocument(sPrinter, nPageFrom, nPageTo, nOptions)
            Next
        End If

        Return True
    End Function

    Public Function AddBookMarks_A(ByVal sFile As String, ByVal nPages() As Integer, _
    ByVal sGroups() As String, ByVal sReportName As String) As String
        Try
            Using doc As Gnostice.PDFOne.PDFDocument = New Gnostice.PDFOne.PDFDocument(pdfOneLicense)

                doc.Load(sFile)

                doc.PageMode = Gnostice.PDFOne.PDFPageMode.UseThumbs

                '//add the top most bookmark
                Dim parent As PDFBookmark = doc.AddBookmark(sReportName, Nothing, 1, True)
                Dim targetPages() As Integer

                'load all the pages into the targetpages array
                ReDim targetPages(nPages.GetUpperBound(0))
                Dim targetPage As Integer

                For I As Integer = 0 To nPages.GetUpperBound(0)
                    targetPages(I) = targetPage

                    targetPage += nPages(I)
                Next

                For i As Integer = 0 To nPages.GetUpperBound(0)
                    doc.AddBookmark(sGroups(i), parent, targetPages(i) + 1, True)
                Next
                'create the bookmarks backwards (last one first)
                ' Dim n As Integer = nPages.GetUpperBound(0)

                ''  Do
                'doc.AddBookmark(sGroups(n), parent, targetPages(n), True)
                '  n -= 1
                '  Loop Until n < 0

                'save the new fle
                Dim sNewFile As String = clsMarsReport.m_OutputFolder & "\{" & clsMarsData.CreateDataID("", "") & "}.pdf"
                doc.OpenAfterCreate = False
                doc.Save(sNewFile)
                doc.Close()

                Return sNewFile

            End Using
        Catch
            Return sFile
        End Try
    End Function

    Public Function AddBookMarks(ByVal sFile As String, ByVal nPages() As Integer, _
    ByVal sGroups() As String, ByVal sReportName As String) As String

        Return AddBookMarks_A(sFile, nPages, sGroups, sReportName)

        Dim oPDF As PDFtoolkitX.gtPDFDocumentXClass = New PDFtoolkitX.gtPDFDocumentXClass  'iSED.QuickPDFClass = New iSED.QuickPDFClass

        Dim TargetPage As Integer = 1
        Dim TargetPages() As Integer

        oPDF.ActivateLicense("E91934A3-91FC-486D-A1F8-8330DA5E3BC6")

        Dim PDFDest As gtPDFDestinationX

        Dim Outline1 As gtPDFOutlineX

        'Load the Document    
        oPDF.LoadFromFile(sFile)

        'Set the parent bookmark destination settings    
        PDFDest = oPDF.GetDestination

        PDFDest.Page = 1

        PDFDest.DestinationType = TxgtPDFDestinationType.dtFit

        Outline1 = oPDF.CreateNewBookmark(sReportName, PDFDest)

        'load all the pages into the targetpages array
        ReDim TargetPages(nPages.GetUpperBound(0))

        For I As Integer = 0 To nPages.GetUpperBound(0)
            TargetPages(I) = TargetPage

            TargetPage += nPages(I)
        Next

        'create the bookmarks backwards (last one first)
        Dim n As Integer = nPages.GetUpperBound(0)

        Do
            PDFDest.Page = TargetPages(n)
            PDFDest.DestinationType = TxgtPDFDestinationType.dtFit

            Outline1.AddChild(sGroups(n), PDFDest)

            n -= 1
        Loop Until n < 0

        'save the new fle
        Dim sNewFile As String = clsMarsReport.m_OutputFolder & "{" & clsMarsData.CreateDataID & "}.pdf"

        oPDF.ShowSetupDialog = False
        oPDF.OpenAfterSave = False
        oPDF.SaveToFile(sNewFile)

        oPDF = Nothing

        Return sNewFile

    End Function
    Public Sub setPDFExpiry(pdfFile As String, expiryDate As Date)
        Try
            Using doc As Gnostice.PDFOne.PDFDocument = New Gnostice.PDFOne.PDFDocument(pdfOneLicense)

                doc.Load(pdfFile)

                Dim js As String = My.Resources.finaldate ' "c:\users\steven\desktop\finaldate.js")

                '  js = "app.alert('This document is no longer valid.  Please contact the author'); this.closeDoc(true);"

                js = js.Replace("##/##/####", expiryDate.ToString("MM/dd/yyyy"))

                doc.AddOpenActionJavaScript(js)

                Dim newFile As String = sAppPath & "Output\{" & Guid.NewGuid.ToString & "}.pdf"

                doc.Save(newFile)
                doc.Close()

                System.Threading.Thread.Sleep(1000)

                IO.File.Delete(pdfFile)

                IO.File.Move(newFile, pdfFile)
            End Using
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, , True, True)
        End Try
    End Sub
    Public Function SetPDFSummary(ByVal sTitle As String, ByVal sAuthor As String, ByVal szSubject As String, ByVal sKeywords As String, _
    ByVal sCreated As Date, ByVal sProducer As String, ByVal sFile As String, doExpire As Boolean, expiryDate As Date) As Boolean
        Try

            If doExpire Then
                setPDFExpiry(sFile, expiryDate)
            End If


            '            Dim oPDF As PDFtoolkitX.gtPDFDocumentXClass = New PDFtoolkitX.gtPDFDocumentXClass  'iSED.QuickPDFClass = New iSED.QuickPDFClass
            '            Dim sPath As String

            '            oPDF.ActivateLicense("E91934A3-91FC-486D-A1F8-8330DA5E3BC6")

            '21:         If sAuthor.Length = 0 And sKeywords.Length = 0 And sProducer.Length = 0 And _
            '          szSubject.Length = 0 And sTitle.Length = 0 Then
            '22:             Return True
            '23:         End If

            '50:         With oPDF
            '60:             .LoadFromFile(sFile)
            '                With .DocInfo
            '70:                 .Author = sAuthor
            '80:                 .Title = sTitle
            '90:                 .Subject = szSubject
            '100:                .Keywords = sKeywords
            '110:                .Producer = sProducer
            '120:                .Creator = "SQL-RD"
            '                    .CreationDate = sCreated
            '121:                .ModDate = sCreated
            '130:            End With
            '            End With

            Dim sPath As String = ""

140:        sPath = clsMarsReport.m_OutputFolder & "{" & clsMarsData.CreateDataID & "}.pdf"

            Using doc As Gnostice.PDFOne.PDFDocument = New Gnostice.PDFOne.PDFDocument(pdfOneLicense)
                doc.Load(sFile)
                doc.DocInfo.Author = sAuthor
                doc.DocInfo.Title = sTitle
                doc.DocInfo.Subject = szSubject
                doc.DocInfo.Keywords = sKeywords
                doc.DocInfo.Creator = "SQL-RD"

                doc.Save(sPath)
            End Using

            '141:        oPDF.ShowSetupDialog = False
            '142:        oPDF.OpenAfterSave = False

            '150:        oPDF.SaveToFile(sPath)

            '160:        oPDF = Nothing

170:        System.IO.File.Copy(sPath, sFile, True)

180:        System.IO.File.Delete(sPath)

            SaveTextToFile(Date.Now & ": PDF Properties set successfully!", sAppPath & "pdf.log", , True)
190:        Return True

200:    Catch ex As Exception
            SaveTextToFile(Date.Now & ": " & ex.Message & " @ " & Erl(), sAppPath & "pdf.log", , True)
            Return False
        End Try

    End Function

    Public Function addWaterMarkToFile(m_pdfFile As String, watermarkType As String, caption As String, font As String, imageFile As String,
                                 overlay As Boolean, angle As Integer, verticalPos As String, horizontalPos As String,
                                 ByRef errInfo As Exception) As Boolean
        Try
            Dim sPath As String = ""

            watermarkType = watermarkType.ToLower

            sPath = System.IO.Path.Combine(clsMarsReport.m_OutputFolder, Guid.NewGuid.ToString & ".pdf")

            Using pdfreader As iTextSharp.text.pdf.PdfReader = New iTextSharp.text.pdf.PdfReader(m_pdfFile)
                If watermarkType = "text" Then
                    Using stream As New System.IO.FileStream(sPath, FileMode.OpenOrCreate)
                        Using pdfStamper As New iTextSharp.text.pdf.PdfStamper(pdfreader, stream)
                            'iterate through all pages in source pdf
                            For pageIndex As Integer = 1 To pdfreader.NumberOfPages
                                'Rectangle class in iText represent geomatric representation... in this case, rectanle object would contain page geomatry
                                Dim pageRectangle As iTextSharp.text.Rectangle = pdfreader.GetPageSizeWithRotation(pageIndex)
                                'pdfcontentbyte object contains graphics and text content of page returned by pdfstamper
                                Dim pdfData As iTextSharp.text.pdf.PdfContentByte

                                If overlay Then
                                    pdfData = pdfStamper.GetOverContent(pageIndex)
                                Else
                                    pdfData = pdfStamper.GetUnderContent(pageIndex)
                                End If

                                Dim fontName = "", fontStyle As String
                                Dim fontColor As Double
                                Dim fontSize As Integer

                                Dim fontDtls As String = font.Split(";")(0)
                                fontStyle = font.Split(";")(1)

                                Try
                                    fontColor = font.Split(";")(2)
                                Catch
                                    fontColor = 35
                                End Try

                                fontDtls = fontDtls.Replace("[Font:", "").Replace("]", "").Trim  '[Font:name=blah,size=18]

                                For Each s As String In fontDtls.Split(",")
                                    If s.Split("=")(0).Trim.ToLower = "name" Then
                                        fontName = s.Split("=")(1)
                                    ElseIf s.Split("=")(0).Trim.ToLower = "size" Then
                                        fontSize = s.Split("=")(1)
                                    End If
                                Next

                                Dim bold, underline, strikethrough, italic As Boolean

                                For Each s As String In fontStyle.Split(",")
                                    If s.ToLower.Trim = "bold" Then
                                        bold = True
                                    ElseIf s.ToLower.Trim = "underline" Then
                                        underline = True
                                    ElseIf s.ToLower.Trim = "strikeout" Then
                                        strikethrough = True
                                    ElseIf s.ToLower.Trim = "italic" Then
                                        italic = True
                                    End If
                                Next

                                Dim bFont As iTextSharp.text.pdf.BaseFont

                                Select Case fontName
                                    Case "Courier"
                                        bFont = iTextSharp.text.pdf.BaseFont.CreateFont(iTextSharp.text.pdf.BaseFont.COURIER, iTextSharp.text.pdf.BaseFont.CP1252, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
                                    Case "Courier Bold"
                                        bFont = iTextSharp.text.pdf.BaseFont.CreateFont(iTextSharp.text.pdf.BaseFont.COURIER_BOLD, iTextSharp.text.pdf.BaseFont.CP1252, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
                                    Case "Helvetica"
                                        bFont = iTextSharp.text.pdf.BaseFont.CreateFont(iTextSharp.text.pdf.BaseFont.HELVETICA, iTextSharp.text.pdf.BaseFont.CP1252, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
                                    Case "Helvetica Bold"
                                        bFont = iTextSharp.text.pdf.BaseFont.CreateFont(iTextSharp.text.pdf.BaseFont.HELVETICA_BOLD, iTextSharp.text.pdf.BaseFont.CP1252, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
                                    Case "Symbol"
                                        bFont = iTextSharp.text.pdf.BaseFont.CreateFont(iTextSharp.text.pdf.BaseFont.SYMBOL, iTextSharp.text.pdf.BaseFont.CP1252, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
                                    Case "Times"
                                        bFont = iTextSharp.text.pdf.BaseFont.CreateFont(iTextSharp.text.pdf.BaseFont.TIMES_ROMAN, iTextSharp.text.pdf.BaseFont.CP1252, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
                                    Case "Times Bold"
                                        bFont = iTextSharp.text.pdf.BaseFont.CreateFont(iTextSharp.text.pdf.BaseFont.TIMES_BOLD, iTextSharp.text.pdf.BaseFont.CP1252, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
                                    Case "Times Bold Italic"
                                        bFont = iTextSharp.text.pdf.BaseFont.CreateFont(iTextSharp.text.pdf.BaseFont.TIMES_BOLDITALIC, iTextSharp.text.pdf.BaseFont.CP1252, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
                                    Case "Times Italic"
                                        bFont = iTextSharp.text.pdf.BaseFont.CreateFont(iTextSharp.text.pdf.BaseFont.TIMES_ITALIC, iTextSharp.text.pdf.BaseFont.CP1252, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
                                    Case Else
                                        bFont = iTextSharp.text.pdf.BaseFont.CreateFont(iTextSharp.text.pdf.BaseFont.COURIER, iTextSharp.text.pdf.BaseFont.CP1252, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
                                End Select

                                'create fontsize for watermark
                                pdfData.SetFontAndSize(bFont, fontSize)
                                'create new graphics state and assign opacity
                                Dim graphicsState As New iTextSharp.text.pdf.PdfGState()
                                graphicsState.FillOpacity = 0.4F
                                'set graphics state to pdfcontentbyte
                                pdfData.SetGState(graphicsState)
                                'set color of watermark

                                Dim col As System.Drawing.Color = Color.FromKnownColor(fontColor)

                                pdfData.SetRGBColorFill(col.R, col.G, col.B)

                                'indicates start of writing of text
                                pdfData.BeginText()
                                'show text as per position and rotation

                                Select Case verticalPos.ToLower.Trim
                                    Case "middle", "center"
                                        Select Case horizontalPos.ToLower.Trim
                                            Case "left"
                                                pdfData.ShowTextAligned(iTextSharp.text.Element.ALIGN_LEFT, caption, 0, pageRectangle.Height / 2, angle)
                                            Case "right"
                                                pdfData.ShowTextAligned(iTextSharp.text.Element.ALIGN_RIGHT, caption, pageRectangle.Width, pageRectangle.Height / 2, angle)
                                            Case "center", "middle"
                                                pdfData.ShowTextAligned(iTextSharp.text.Element.ALIGN_CENTER, caption, pageRectangle.Width / 2, pageRectangle.Height / 2, angle)
                                        End Select
                                    Case "bottom"
                                        Select Case horizontalPos.ToLower.Trim
                                            Case "left"
                                                pdfData.ShowTextAligned(iTextSharp.text.Element.ALIGN_LEFT, caption, 0, 0, angle)
                                            Case "right"
                                                pdfData.ShowTextAligned(iTextSharp.text.Element.ALIGN_RIGHT, caption, pageRectangle.Width, 0, angle)
                                            Case "center", "middle"
                                                pdfData.ShowTextAligned(iTextSharp.text.Element.ALIGN_CENTER, caption, pageRectangle.Width / 2, 0, angle)
                                        End Select
                                    Case "top"
                                        Select Case horizontalPos.ToLower.Trim
                                            Case "left"
                                                pdfData.ShowTextAligned(iTextSharp.text.Element.ALIGN_LEFT, caption, 0, (pageRectangle.Height - fontSize), angle)
                                            Case "right"
                                                pdfData.ShowTextAligned(iTextSharp.text.Element.ALIGN_RIGHT, caption, pageRectangle.Width, (pageRectangle.Height - fontSize), angle)
                                            Case "center", "middle"
                                                pdfData.ShowTextAligned(iTextSharp.text.Element.ALIGN_CENTER, caption, pageRectangle.Width / 2, (pageRectangle.Height - fontSize), angle)
                                        End Select
                                End Select

                                pdfData.ShowTextAligned(iTextSharp.text.Element.ALIGN_CENTER, caption, pageRectangle.Width / 2, pageRectangle.Height / 2, angle)
                                'call endText to invalid font set
                                pdfData.EndText()
                            Next

                            pdfStamper.Close()
                        End Using
                        stream.Close()
                    End Using
                Else
                    Using pdfStamper = New iTextSharp.text.pdf.PdfStamper(pdfreader, New FileStream(sPath, FileMode.Create))
                        Dim image As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(imageFile)
                        image.RotationDegrees = angle
                        image.Alignment = image.ALIGN_MIDDLE + image.ALIGN_CENTER

                        For I As Integer = 1 To pdfreader.NumberOfPages
                            Dim rect As iTextSharp.text.Rectangle = pdfreader.GetPageSizeWithRotation(I)

                            Select Case verticalPos.ToLower.Trim
                                Case "middle", "center"
                                    Select Case horizontalPos.ToLower.Trim
                                        Case "left"
                                            image.SetAbsolutePosition(0, rect.Height / 2)
                                        Case "right"
                                            image.SetAbsolutePosition(rect.Width - image.Width, rect.Height / 2)
                                        Case "center", "middle"
                                            image.SetAbsolutePosition(rect.Width / 2, rect.Height / 2)
                                    End Select
                                Case "bottom"
                                    Select Case horizontalPos.ToLower.Trim
                                        Case "left"
                                            image.SetAbsolutePosition(0, 0)
                                        Case "right"
                                            image.SetAbsolutePosition(rect.Width - image.Width, 0)
                                        Case "center", "middle"
                                            image.SetAbsolutePosition(rect.Width / 2, 0)
                                    End Select
                                Case "top"
                                    Select Case horizontalPos.ToLower.Trim
                                        Case "left"
                                            image.SetAbsolutePosition(0, rect.Height - image.Height)
                                        Case "right"
                                            image.SetAbsolutePosition(rect.Width - image.Width, rect.Height - image.Height)
                                        Case "center", "middle"
                                            image.SetAbsolutePosition(rect.Width / 2, rect.Height - image.Height)
                                    End Select
                            End Select

                            Dim content

                            If overlay Then
                                content = pdfStamper.GetOverContent(I)
                            Else
                                content = pdfStamper.GetunderContent(I)
                            End If

                            content.AddImage(image)
                        Next

                        pdfStamper.Close()
                    End Using
                End If
            End Using

            System.IO.File.Copy(sPath, m_pdfFile, True)
            System.IO.File.Delete(sPath)

            Return True
        Catch ex As Exception
            errInfo = ex
            Return False
        End Try

    End Function


    Public Function AddWatermark(ByVal sFile As String, ByVal sMark As String, ByVal nID As Integer) As Object

        If sMark Is Nothing Then Return Nothing

        If sMark.Length = 0 Then Return Nothing

        '  Dim oPDF As New PDFtoolkitX.gtPDFDocumentXClass

        'oPDF.ActivateLicense("E91934A3-91FC-486D-A1F8-8330DA5E3BC6")

        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM StampAttr WHERE ForeignID =" & nID)
        Dim errInfo As Exception = Nothing
        Dim ok As Boolean

        If oRs IsNot Nothing Then
            If oRs.EOF = True Then

                ok = Me.addWaterMarkToFile(sFile, "text", sMark, "[Font:name=Courier,size=18;;]", Nothing, True, 45, "middle", "center", errInfo)

            Else
                Dim stampType As String = IsNull(oRs("stamptype").Value)
                Dim overlay As String = IsNull(oRs("overlayunderlay").Value)
                Dim verticalPos As String = IsNull(oRs("verticalpos").Value)
                Dim horizontalPos As String = IsNull(oRs("horizontalpos").Value)
                Dim stampAngle As Integer = IsNull(oRs("stampangle").Value, 0)

                Select Case stampType.ToUpper
                    Case "TEXT"
                        Dim font As String = oRs("fontdetails").Value

                        Dim fontDtls As String = font.Split(";")(0)
                        Dim fontStyle As String = font.Split(";")(1)
                        Dim fontColor As Double = font.Split(";")(2)
                        Dim fontName As String = ""
                        Dim fontSize As Double = 12

                        fontDtls = fontDtls.Replace("[Font:", "").Replace("]", "").Trim

                        For Each s As String In fontDtls.Split(",")
                            If s.Split("=")(0).Trim.ToLower = "name" Then
                                fontName = s.Split("=")(1)
                            ElseIf s.Split("=")(0).Trim.ToLower = "size" Then
                                fontSize = s.Split("=")(1)
                            End If
                        Next

                        Dim dooverlay As Boolean = False

                        If overlay.ToLower = "overlay" Then
                            dooverlay = True
                        End If

                        ok = addWaterMarkToFile(sFile, "text", sMark, font, "", dooverlay, stampAngle, verticalPos, horizontalPos, errInfo)

                    Case "GRAPHIC"

                        Dim imageFile As String = oRs("imagefile").Value
                        Dim oparse As New clsMarsParser
                        Dim dooverlay As Boolean = False

                        If overlay.ToLower = "overlay" Then
                            dooverlay = True
                        End If

                        ok = addWaterMarkToFile(sFile, "graphic", sMark, "", imageFile, dooverlay, stampAngle, verticalPos, horizontalPos, errInfo)
                End Select
            End If
        End If


        Return sFile
    End Function

    Public Function SetPDFPermissions(ByVal sFile As String, ByVal CanPrint As Integer, ByVal CanCopy As Integer, _
 ByVal CanEdit As Integer, ByVal CanNotes As Integer, ByVal CanFill As Integer, ByVal CanAccess As Integer, _
 ByVal CanAssemble As Integer, ByVal CanPrintFull As Integer, ByVal sOwnerPass As String, ByVal sUserPass As String) As Boolean
        Dim oparse As New clsMarsParser
        Try
            Using doc As PDFDocument = New PDFDocument(pdfOneLicense)
                doc.Load(sFile)
                doc.OpenAfterCreate = False
                doc.Security.Enabled = True
                doc.Security.Level = PDFEncryptionLevel.Level128Bit
                If sOwnerPass <> "" Then doc.Security.OwnerPassword = oparse.ParseString(_DecryptDBValue(sOwnerPass))
                If sUserPass <> "" Then doc.Security.UserPassword = oparse.ParseString(_DecryptDBValue(sUserPass))

                Dim perm As PDFUserPermissions = PDFUserPermissions.None

                If Convert.ToBoolean(CanCopy) Then
                    perm = PDFUserPermissions.ContentCopyExtract
                End If

                If Convert.ToBoolean(CanAccess) Then
                    perm += PDFUserPermissions.ContentAccessibility
                End If

                If Convert.ToBoolean(CanNotes) Then
                    perm += PDFUserPermissions.Commenting
                End If

                If Convert.ToBoolean(CanAssemble) Then
                    perm += PDFUserPermissions.DocAssembly
                End If

                If Convert.ToBoolean(CanFill) Then
                    perm += PDFUserPermissions.FormFillSign
                End If

                If Convert.ToBoolean(CanPrintFull) Then
                    perm += PDFUserPermissions.HighResPrint
                End If

                If Convert.ToBoolean(CanEdit) Then
                    perm += PDFUserPermissions.DocModify
                End If

                If Convert.ToBoolean(CanPrint) Then
                    perm += PDFUserPermissions.LowResPrint
                End If

                doc.Security.UserPermissions = perm

                Dim sNewFile As String = GetDirectory(sFile) & "{" & clsMarsData.CreateDataID("", "") & "}.pdf"

                doc.Save(sNewFile)

                doc.Close()

                IO.File.Delete(sFile)
                IO.File.Move(sNewFile, sFile)
            End Using
        Catch ex As Exception

        End Try
    End Function
    Public Function SetPDFPermissionsold(ByVal sFile As String, ByVal CanPrint As Integer, ByVal CanCopy As Integer, _
    ByVal CanEdit As Integer, ByVal CanNotes As Integer, ByVal CanFill As Integer, ByVal CanAccess As Integer, _
    ByVal CanAssemble As Integer, ByVal CanPrintFull As Integer, ByVal sOwnerPass As String, ByVal sUserPass As String) As Boolean
        Dim oPDF As New PDFtoolkitX.gtPDFDocumentXClass
        Dim oparse As New clsMarsParser

        oPDF.ActivateLicense("E91934A3-91FC-486D-A1F8-8330DA5E3BC6")

        oPDF.LoadFromFile(sFile)

        oPDF.Encryption.Enabled = True
        oPDF.Encryption.Level = PDFtoolkitX.TxgtPDFEncryptionLevel.el128bit
        oPDF.Encryption.OwnerPassword = oparse.ParseString(_DecryptDBValue(sOwnerPass))
        oPDF.Encryption.UserPassword = oparse.ParseString(_DecryptDBValue(sUserPass))

        With oPDF.Encryption.UserPermission
            .AllowCopy = Convert.ToBoolean(CanCopy)
            .AllowAccessibility = Convert.ToBoolean(CanAccess)
            .AllowAnnotation = Convert.ToBoolean(CanNotes)
            .AllowDocAssembly = Convert.ToBoolean(CanAssemble)
            .AllowFormFill = Convert.ToBoolean(CanFill)
            .AllowHighResPrint = Convert.ToBoolean(CanPrintFull)
            .AllowModify = Convert.ToBoolean(CanEdit)
            .AllowPrint = Convert.ToBoolean(CanPrint)
        End With

        oPDF.ShowSetupDialog = False
        oPDF.OpenAfterSave = False

        Dim sNewFile As String = GetDirectory(sFile) & "{" & clsMarsData.CreateDataID & "}.pdf"

        oPDF.SaveToFile(sNewFile)

        IO.File.Delete(sFile)
        IO.File.Move(sNewFile, sFile)

    End Function


    Public Function MergePDFFiles(ByVal sFiles() As String, ByVal sOutput As String, Optional ByVal AddBookmarks As Boolean = False, _
    Optional ByVal sParentTree As String = "") As String

        Return MergePDFFiles_A(sFiles, sOutput, AddBookmarks, sParentTree, False, True)

        Dim oPDF As PDFtoolkitX.gtPDFDocumentXClass = New PDFtoolkitX.gtPDFDocumentXClass

        oPDF.ActivateLicense("E91934A3-91FC-486D-A1F8-8330DA5E3BC6")

        Dim nPages() As Integer
        Dim sGroups() As String

        If AddBookmarks = True Then
            Try
                Dim I As Integer = 0
                Dim pdfDoc As iSED.QuickPDF = New iSED.QuickPDFClass

                pdfDoc.UnlockKey("B34B74C95F359046BEFB789480980C03")

                For Each s As String In sFiles
                    ReDim Preserve nPages(I)
                    ReDim Preserve sGroups(I)

                    pdfDoc.LoadFromFile(s)

                    nPages(I) = pdfDoc.PageCount
                    sGroups(I) = ExtractFileName(s)

                    If sGroups(I).ToString.ToLower.IndexOf(".") > -1 Then
                        sGroups(I) = sGroups(I).ToString.Split(".")(0)
                    End If

                    I += 1
                Next
            Catch : End Try
        End If

        oPDF.MergeDocs(sFiles)

        Dim sMergePath As String = clsMarsReport.m_OutputFolder & "{" & clsMarsData.CreateDataID & "}.pdf" 'sOutput

        oPDF.ShowSetupDialog = False
        oPDF.OpenAfterSave = False

        oPDF.SaveToFile(sMergePath)

        Dim finalPDF As String = clsMarsReport.m_OutputFolder & sOutput
        Try
            If AddBookmarks = True Then
                Dim Temp As String = Me.AddBookMarks(sMergePath, nPages, sGroups, sParentTree)

                IO.File.Copy(Temp, finalPDF, True)
            Else
                IO.File.Copy(sMergePath, finalPDF, True)
            End If
        Catch : End Try

        Return finalPDF
    End Function
    Public Function _ConvertFromPDF(ByVal ToFormat As TgtRenderTypeX, ByVal sIn As String, _
    ByVal sFilename As String, ByVal sDirectory As String, ByVal DeleteOriginal As Boolean) As String
        Dim oPDF As PDF2ManyX.gtPDF2ManyX = New PDF2ManyX.gtPDF2ManyX

        oPDF.ActivateLicense("172550B5-649A-4D82-9B5C-3ADBD014F125")

        If sDirectory.EndsWith("\") = False Then sDirectory &= "\"

        With oPDF
            .InputPDFFile = sIn
            .OutputFileName = sDirectory & sFilename
            .OpenAfterCreate = False

            If ToFormat = TgtRenderTypeX.rtXHTML Or ToFormat = TgtRenderTypeX.rtHTML Then
                .HTMLOptions.PrefAutoScroll = False
                .HTMLOptions.PrefOptimizeForIE = True
                .HTMLOptions.PrefAddTOC = False
            End If

            .RenderDocument(ToFormat)
        End With

        If DeleteOriginal = True Then IO.File.Delete(sIn)

        Dim sExt As String

        Select Case ToFormat
            Case TgtRenderTypeX.rtHTML, TgtRenderTypeX.rtXHTML
                sExt = ".htm"
            Case TgtRenderTypeX.rtTXT
                sExt = ".txt"
            Case TgtRenderTypeX.rtRTF
                sExt = ".rtf"
            Case Else
                sExt = String.Empty
        End Select

        Return oPDF.OutputFileName & sExt
    End Function


End Class
