Imports System.Threading
Imports System.Management

Friend Class clsMarsThreading
    Public xReportID As Integer = 0
    Public xPackID As Integer = 0
    Public xAutoID As Integer = 0
    Public xEventID As Integer = 0

    Private ReadOnly Property m_outofProcessExecution() As Boolean
        Get
            Try
                Dim value As Boolean = Convert.ToBoolean(Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("RunOutOfProcess", 1)))
                Return value
            Catch
                Return False
            End Try
        End Get
    End Property
    <DebuggerStepThrough> _
    Public Shared Function GetUserName(ByVal processid As Integer)
        Dim query As String = "Select * From Win32_Process Where ProcessID = " & processid
        Dim selectQuery As SelectQuery = New SelectQuery(query)
        Dim searcher As ManagementObjectSearcher = New ManagementObjectSearcher(selectQuery)
        Dim y As System.Management.ManagementObjectCollection
        y = searcher.Get
        For Each proc As ManagementObject In y
            Dim s(1) As String
            proc.InvokeMethod("GetOwner", CType(s, Object()))
            Dim n As String = proc("Name").ToString()

            Return ("User: " & s(1) & "\\" & s(0))

        Next

        Return "#error#"
    End Function

    Private Shared Function GetServiceUser() As String
        Try
            Dim serviceType As String = clsMarsUI.MainUI.ReadRegistry("SQL-RDService", "NONE")
            Dim searchAssembly As String = ""

            If serviceType = "NONE" Then
                Return GetUserName(Process.GetCurrentProcess.Id)
            Else
                If serviceType.ToLower = "windowsnt" Then
                    searchAssembly = "sqlrdsvc"
                Else
                    searchAssembly = "sqlrdapp"
                End If
            End If

            Dim svcUser As String = ""

            For Each p As Process In Process.GetProcessesByName(searchAssembly)
                svcUser = GetUserName(p.Id)
            Next

            If svcUser = "" Then
                svcUser = GetUserName(Process.GetCurrentProcess.Id)
            End If

            Return svcUser
        Catch ex As Exception
            Return GetUserName(Process.GetCurrentProcess.Id)
        End Try

    End Function

    Private Shared Function GetServiceSession() As Integer
        Try
            Dim serviceType As String = clsMarsUI.MainUI.ReadRegistry("SQL-RDService", "NONE")
            Dim searchAssembly As String = ""

            If serviceType = "NONE" Then
                Return Process.GetCurrentProcess.SessionId
            Else
                If serviceType.ToLower = "windowsnt" Then
                    searchAssembly = "sqlrdsvc"
                Else
                    searchAssembly = "sqlrdapp"
                End If
            End If


            Dim svcSession As Integer = 0

            For Each p As Process In Process.GetProcessesByName(searchAssembly)
                svcSession = p.SessionId
            Next

            If svcSession = 0 Then
                svcSession = Process.GetCurrentProcess.SessionId
            End If

            Return svcSession
        Catch ex As Exception
            Return Process.GetCurrentProcess.SessionId
        End Try

    End Function
    Public Shared Function GetThreadCount() As Integer
        Dim I As Integer = 0
        Dim svcUser As String = GetServiceUser()

        For Each p As Process In Process.GetProcessesByName("sqlrd")
            Try
                Dim appUser As String = GetUserName(p.Id)

                If svcUser.ToLower = appUser.ToLower And appUser.ToLower <> "#error#" Then
                    If p.MainWindowTitle = "" And p.Id <> Process.GetCurrentProcess.Id And p.SessionId = GetServiceSession() Then
                        clsMarsDebug.writeToDebug("scheduler.debug", "Running thread ID: " & p.Id)
                        I += 1
                    End If
                End If
            Catch : End Try
        Next

        Return I
    End Function

    Public Shared Function GetThreadDetailCount(Optional ByVal ExcludeProcessID As Integer = 0) As Integer
        Dim I As Integer = 0

        Try
            CleanThreadManagerDetail()

            Dim SQL As String = "SELECT COUNT(*) FROM ThreadManagerDetail"

            'Exclude the specified processId from the result count if required
            If (ExcludeProcessID > 0) Then
                SQL += " WHERE ProcessID <> " & ExcludeProcessID
            End If

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If (oRs IsNot Nothing AndAlso oRs.EOF = False) Then
                I = oRs(0).Value
            End If
        Catch : End Try

        Return I
    End Function

    Public Shared Sub CleanThreadManagerDetail()
        Dim processes As Process() = Process.GetProcesses()
        Dim processID As Integer = 0
        Dim delStr As String = ""

        Try
            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT DISTINCT ProcessID FROM ThreadManagerDetail")

            Do While (oRs IsNot Nothing AndAlso oRs.EOF = False)
                processID = oRs("ProcessID").Value()

                If (DoesProcessExist(processID, processes) = False) Then
                    If (delStr.Length() > 0) Then
                        delStr += "," & processID
                    Else
                        delStr = processID
                    End If
                End If

                oRs.MoveNext()
            Loop

            If (delStr.Length() > 0) Then
                Dim sql As String = "DELETE FROM ThreadManagerDetail WHERE ProcessID IN (" & delStr & ")"
                clsMarsData.WriteData(sql)
            End If
        Catch : End Try

    End Sub

    Private Shared Function DoesProcessExist(ByVal id As Integer, Optional ByVal list As Process() = Nothing) As Boolean
        Dim rc As Boolean = False
        Dim processes As Process() = list

        If (processes Is Nothing) Then
            processes = Process.GetProcesses()
        End If

        For Each p As Process In processes
            If (p.Id() = id) Then
                rc = True
                Exit For
            End If
        Next

        Return rc
    End Function

    Private Sub showWatcher()
        If crdxCommon.pWatcher Is Nothing Then
            crdxCommon.pWatcher = New frmOOPwatcher
            crdxCommon.pWatcher.ShowInTaskbar = True
            ' crdxCommon.pWatcher.Owner = crdxCommon.executingForm
            crdxCommon.pWatcher.Show()
            crdxCommon.pWatcher.tmCheck.Enabled = True
            crdxCommon.pWatcher.tmExecutor.Enabled = True
        Else
            If crdxCommon.pWatcher.IsDisposed Then
                crdxCommon.pWatcher = New frmOOPwatcher
                crdxCommon.pWatcher.ShowInTaskbar = True
                crdxCommon.pWatcher.Show()
            End If

            crdxCommon.pWatcher.Visible = True
            crdxCommon.pWatcher.tmCheck.Enabled = True
            crdxCommon.pWatcher.tmExecutor.Enabled = True
        End If
    End Sub

    Private Function IsDuplicateOOP(ByVal name As String, ByVal type As String) As Boolean
        If crdxCommon.dtOOP Is Nothing Then Return False

        Dim rows() As DataRow = crdxCommon.dtOOP.Select("Name ='" & SQLPrepare(name) & "' AND Type ='" & SQLPrepare(type) & "'")

        If rows.Length = 0 Then
            Return False
        Else
            Return True
        End If

    End Function
    'With Me.dtOOP.Columns
    '           .Add("Name")
    '           .Add("Type")
    '           .Add("PID")
    '           .Add("ScheduleID")
    '           .Add("Status")
    '           .Add("StatusValue")
    '           .Add("Argument")
    '           .Add("EntryDate")
    '       End With


    Public Sub EventPackageScheduleThread(Optional ByVal outOfProcessAlready As Boolean = False)

        If clsMarsScheduler.isScheduleLocked(xPackID, clsMarsScheduler.enScheduleType.EVENTPACKAGE) Then
            MessageBox.Show("Sorry, cannot execute this schedule as it is locked due to it using functionality that has not been purchased.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If

        If Me.m_outofProcessExecution = True And outOfProcessAlready = False Then


            Dim name As String = clsMarsScheduler.globalItem.GetScheduleName(xPackID, _
            clsMarsScheduler.enScheduleType.EVENTPACKAGE)

            Dim type As String = "Event-Based Package"

            If Me.IsDuplicateOOP(name, type) = True Then
                MessageBox.Show("The selected schedule is already being executed. Please wait until the current instance has finished executing", _
                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If

            Try
                Dim row As DataRow = getProcessWatcherTable.Rows.Add

                row("Name") = name
                row("Type") = type
                row("PID") = 0
                row("ScheduleID") = xPackID
                row("Status") = "Waiting..."
                row("StatusValue") = 0
                row("Argument") = "oop ep " & xPackID
                row("EntryDate") = Now
            Catch : End Try

            showWatcher()

            Return
        End If

RUN:
        Dim Success As Boolean
        Dim oSchedule As clsMarsScheduler = New clsMarsScheduler
        Dim oEvent As clsMarsEvent = New clsMarsEvent

        'Dim ScheduleID As Integer

        gsID(0) = "Event Package"
        gsID(1) = xPackID

        gErrorCollection = ""

        ScheduleStart = Now

        Success = oEvent.RunEventPackage(xPackID)

        If Success = True Then
            'ScheduleID = oSchedule.GetScheduleID(0, , , , xPackID)

            oSchedule.SetScheduleHistory(True, , , , , , xPackID)

            If outOfProcessAlready = False Then MessageBox.Show("Schedule execution completed!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        Else
            oSchedule.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", , , , , xPackID)
            _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, gErrorSuggest)
        End If

        clsMarsUI.MainUI.BusyProgress(, , True)

        oEvent = Nothing
        oSchedule = Nothing
        gScheduleName = String.Empty

        Try
            crdxCommon.executingForm.Bar1.AutoHide = True
        Catch : End Try

    End Sub
    Public Sub EventScheduleThread(Optional ByVal outOfProcessAlready As Boolean = False)

        If clsMarsEvent.isEventLocked(xEventID) Then
            MessageBox.Show("Sorry, cannot execute this schedule as it is locked due to it using functionality that has not been purchased.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If

        If Me.m_outofProcessExecution = True And outOfProcessAlready = False Then



            Dim name As String = clsMarsScheduler.globalItem.GetScheduleName(xEventID, _
            clsMarsScheduler.enScheduleType.EVENTBASED)
            Dim type As String = "Event-Based Schedule"

            If Me.IsDuplicateOOP(name, type) = True Then
                MessageBox.Show("The selected schedule is already being executed. Please wait until the current instance has finished executing", _
                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If

            Try
                Dim row As DataRow = getProcessWatcherTable.Rows.Add

                row("Name") = name
                row("Type") = type
                row("PID") = 0
                row("ScheduleID") = xEventID
                row("Status") = "Waiting..."
                row("StatusValue") = 0
                row("Argument") = "oop e " & xEventID
                row("EntryDate") = Now
            Catch : End Try

            showWatcher()

            Return
        End If

RUN:
        gsID(0) = "Event-Based"
        gsID(1) = xEventID

        clsMarsEvent.RunEvents6(xEventID)

        If outOfProcessAlready = False Then MessageBox.Show("The event based schedule has completed", Application.ProductName, MessageBoxButtons.OK, _
        MessageBoxIcon.Information)
        gScheduleName = String.Empty

        Try
            crdxCommon.executingForm.Bar1.AutoHide = True
        Catch : End Try
    End Sub
    Public Sub AutoScheduleThread(Optional ByVal outOfProcessAlready As Boolean = False)

        If clsMarsScheduler.isScheduleLocked(xAutoID, clsMarsScheduler.enScheduleType.AUTOMATION) Then
            MessageBox.Show("Sorry, cannot execute this schedule as it is locked due to it using functionality that has not been purchased.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If

        If Me.m_outofProcessExecution = True And outOfProcessAlready = False Then



            Dim name As String = clsMarsScheduler.globalItem.GetScheduleName(xAutoID, _
            clsMarsScheduler.enScheduleType.AUTOMATION)

            Dim type As String = "Automation Schedule"

            If Me.IsDuplicateOOP(name, type) = True Then
                MessageBox.Show("The selected schedule is already being executed. Please wait until the current instance has finished executing", _
                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If

            Try
                Dim row As DataRow = getProcessWatcherTable.Rows.Add

                row("Name") = name
                row("Type") = type
                row("PID") = 0
                row("ScheduleID") = xAutoID
                row("Status") = "Waiting..."
                row("StatusValue") = 0
                row("Argument") = "oop a " & xAutoID
                row("EntryDate") = Now
            Catch : End Try

            showWatcher()

            Return
        End If

RUN:
        Dim oAuto As New clsMarsAutoSchedule

        gsID(0) = "Automation"
        gsID(1) = xAutoID

        oAuto.ExecuteAutomationSchedule(xAutoID)

        If outOfProcessAlready = False Then MessageBox.Show("Schedule execution completed!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        oAuto = Nothing
        gScheduleName = String.Empty

        Try
            crdxCommon.executingForm.Bar1.AutoHide = True
        Catch : End Try
    End Sub
    Public Sub BurstingScheduleThread(Optional ByVal outOfProcessAlready As Boolean = False)

        If clsMarsScheduler.isScheduleLocked(xReportID, clsMarsScheduler.enScheduleType.REPORT) Then
            MessageBox.Show("Sorry, cannot execute this schedule as it is locked due to it using functionality that has not been purchased.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If

        If Me.m_outofProcessExecution = True And outOfProcessAlready = False Then



            Dim name As String = clsMarsScheduler.globalItem.GetScheduleName(xReportID, _
            clsMarsScheduler.enScheduleType.REPORT)

            Dim type As String = "Bursting Schedule"

            If Me.IsDuplicateOOP(name, type) = True Then
                MessageBox.Show("The selected schedule is already being executed. Please wait until the current instance has finished executing", _
                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If

            Try
                Dim row As DataRow = getProcessWatcherTable.Rows.Add

                row("Name") = name
                row("Type") = type
                row("PID") = 0
                row("ScheduleID") = xReportID
                row("Status") = "Waiting..."
                row("StatusValue") = 0
                row("Argument") = "oop b " & xReportID
                row("EntryDate") = Now
            Catch : End Try

            showWatcher()

            Return
        End If

RUN:
        Dim Success As Boolean
        Dim oSchedule As New clsMarsScheduler
        Dim oReport As New clsMarsReport
        Dim ScheduleID As Integer
        Dim oSet As New frmSetPrintDate

        gsID(0) = "Report"
        gsID(1) = xReportID

        ScheduleStart = Now

        Success = True 'oReport.RunBurstingSchedule(xReportID, , oSet.GetPrintDateTime)

        If Success = True Then
            ScheduleID = oSchedule.GetScheduleID(xReportID)

            oSchedule.SetScheduleHistory(True, , xReportID)

            If outOfProcessAlready = False Then MessageBox.Show("Schedule execution completed!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            oSchedule.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", xReportID)
            _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, gErrorSuggest)
        End If

        clsMarsUI.MainUI.BusyProgress(, , True)

        oReport = Nothing
        oSchedule = Nothing
        gScheduleName = String.Empty

        Try
            crdxCommon.executingForm.Bar1.AutoHide = True
        Catch : End Try
    End Sub
    Public Sub DynamicScheduleThread(Optional ByVal outOfProcessAlready As Boolean = False)

        If clsMarsScheduler.isScheduleLocked(xReportID, clsMarsScheduler.enScheduleType.REPORT) Then
            MessageBox.Show("Sorry, cannot execute this schedule as it is locked due to it using functionality that has not been purchased.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If

        ScheduleStart = Now

        If Me.m_outofProcessExecution = True And outOfProcessAlready = False Then



            Dim name As String = clsMarsScheduler.globalItem.GetScheduleName(xReportID, _
            clsMarsScheduler.enScheduleType.REPORT)

            Dim type As String = "Dynamic Schedule"

            If Me.IsDuplicateOOP(name, type) = True Then
                MessageBox.Show("The selected schedule is already being executed. Please wait until the current instance has finished executing", _
                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If

            Try
                Dim row As DataRow = getProcessWatcherTable.Rows.Add

                row("Name") = name
                row("Type") = type
                row("PID") = 0
                row("ScheduleID") = xReportID
                row("Status") = "Waiting..."
                row("StatusValue") = 0
                row("Argument") = "oop d " & xReportID
                row("EntryDate") = Now
            Catch : End Try

            showWatcher()

            Return
        End If

RUN:
        Dim Success As Boolean
        Dim oSchedule As clsMarsScheduler = New clsMarsScheduler
        Dim oReport As clsMarsReport = New clsMarsReport
        Dim ScheduleID As Integer
        Dim oSetDate As frmSetPrintDate = New frmSetPrintDate

        gsID(0) = "Report"
        gsID(1) = xReportID

        ScheduleStart = Now

        Success = oReport.RunDynamicSchedule(xReportID)

        If oReport.sDynamicError.Length > 0 Then
            Dim oMsg As New frmCustomMsg

            oMsg.ShowMsg("The following key values returned errors:" & vbCrLf & vbCrLf & _
            oReport.sDynamicError, "Dynamic Schedule Results", False)
        End If

        If Success = True Then
            ScheduleID = oSchedule.GetScheduleID(xReportID)

            oSchedule.SetScheduleHistory(True, , xReportID)

            If outOfProcessAlready = False Then MessageBox.Show("Schedule execution completed!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            oSchedule.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", xReportID)
            '_ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, gErrorSuggest)
        End If

        clsMarsUI.MainUI.BusyProgress(, , True)

        oReport = Nothing
        oSchedule = Nothing
        gScheduleName = String.Empty

        Try
            crdxCommon.executingForm.Bar1.AutoHide = True
        Catch : End Try
    End Sub

    Public Sub SingleScheduleThread(Optional ByVal outOfProcessAlready As Boolean = False)
        If clsMarsScheduler.isScheduleLocked(xReportID, clsMarsScheduler.enScheduleType.REPORT) Then
            MessageBox.Show("Sorry, cannot execute this schedule as it is locked due to it using functionality that has not been purchased.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If

        Dim isDataDriven As Boolean = clsMarsData.IsScheduleDataDriven(xReportID)

        If Me.m_outofProcessExecution = True And outOfProcessAlready = False Then

            Dim name As String = clsMarsScheduler.globalItem.GetScheduleName(xReportID, _
            clsMarsScheduler.enScheduleType.REPORT)

            Dim type As String

            If isDataDriven = False Then
                type = "Single Schedule"
            Else
                type = "Data Driven Schedule"
            End If

            If Me.IsDuplicateOOP(name, type) = True Then
                MessageBox.Show("The selected schedule is already being executed. Please wait until the current instance has finished executing", _
                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If

            Try
                Dim row As DataRow = getProcessWatcherTable.Rows.Add

                row("Name") = name
                row("Type") = type
                row("PID") = 0
                row("ScheduleID") = xReportID
                row("Status") = "Waiting..."
                row("StatusValue") = 0
                row("Argument") = "oop s " & xReportID
                row("EntryDate") = Now
            Catch : End Try

            showWatcher()

            Return
        End If

RUN:
        Dim Success As Boolean
        Dim oSchedule As clsMarsScheduler = New clsMarsScheduler
        Dim oReport As clsMarsReport = New clsMarsReport
        Dim ScheduleID As Integer
        Dim oSetDate As frmSetPrintDate = New frmSetPrintDate
        'Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT IsDataDriven FROM ReportAttr WHERE ReportID =" & xReportID)


        gsID(0) = "Report"
        gsID(1) = xReportID

        ScheduleStart = Now
        gErrorCollection = ""

        If isDataDriven = False Then
            Success = oReport.RunSingleSchedule(xReportID)
        Else
            Success = oReport.RunDataDrivenSchedule(xReportID)
        End If

        If Success = True Then
            ScheduleID = oSchedule.GetScheduleID(xReportID)

            oSchedule.SetScheduleHistory(True, , xReportID)

            If gErrorCollection = "" Then
                If outOfProcessAlready = False Then MessageBox.Show("Schedule execution completed!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                Dim oMsg As frmCustomMsg = New frmCustomMsg

                oMsg.ShowMsg("Schedule completed with the following destination failures:" & vbCrLf & vbCrLf & _
                gErrorCollection, "Single Schedule Results")
            End If
        Else
            oSchedule.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", xReportID)
            '_ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, gErrorSuggest)
        End If

        clsMarsUI.MainUI.BusyProgress(, , True)

        oReport = Nothing
        oSchedule = Nothing
        gScheduleName = String.Empty

        Try
            crdxCommon.executingForm.Bar1.AutoHide = True
        Catch : End Try
    End Sub

    Public Sub PackageScheduleThread(Optional ByVal outOfProcessAlready As Boolean = False)
        If clsMarsScheduler.isScheduleLocked(xPackID, clsMarsScheduler.enScheduleType.PACKAGE) Then
            MessageBox.Show("Sorry, cannot execute this schedule as it is locked due to it using functionality that has not been purchased.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If

        If Me.m_outofProcessExecution = True And outOfProcessAlready = False Then


            Dim name As String = clsMarsScheduler.globalItem.GetScheduleName(xPackID, _
            clsMarsScheduler.enScheduleType.PACKAGE)

            Dim type As String = "Package Schedule"

            If Me.IsDuplicateOOP(name, type) = True Then
                MessageBox.Show("The selected schedule is already being executed. Please wait until the current instance has finished executing", _
                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If

            'Dim p As Process = New Process

            'With p.StartInfo
            '    .FileName = sAppPath & sqlrd.assemblyName

            '    If xReportID = 0 Then
            '        .Arguments = "oop p " & xPackID
            '    Else
            '        .Arguments = "oop p " & xPackID & ":" & xReportID
            '    End If
            'End With

            'p.Start()

            Dim sArgument As String

            If xReportID = 0 Then
                sArgument = "oop p " & xPackID
            Else
                sArgument = "oop p " & xPackID & ":" & xReportID
            End If

            Try
                Dim row As DataRow = getProcessWatcherTable.Rows.Add

                row("Name") = name
                row("Type") = type
                row("PID") = 0
                row("ScheduleID") = xPackID
                row("Status") = "Waiting..."
                row("StatusValue") = 0
                row("Argument") = sArgument
                row("EntryDate") = Now
            Catch : End Try

            showWatcher()

            Return
        End If

RUN:
        Dim Success As Boolean
        Dim oSchedule As clsMarsScheduler = New clsMarsScheduler
        Dim oReport As clsMarsReport = New clsMarsReport
        Dim ScheduleID As Integer
        Dim oSetDate As frmSetPrintDate = New frmSetPrintDate

        gsID(0) = "Package"
        gsID(1) = xPackID

        gErrorCollection = ""

        ScheduleStart = Now

        If clsMarsData.IsScheduleDynamic(xPackID, "Package") = True Then
            Success = oReport.RunDynamicPackageSchedule(xPackID, , )
        ElseIf clsMarsData.IsScheduleDataDriven(xPackID, "Package") = True Then
            Success = oReport.RunDataDrivenPackage(xPackID)
        Else
            If xReportID = 0 Then
                Success = oReport.RunPackageSchedule(xPackID, , , , , , , , )
            Else
                Success = oReport.RunPackageSchedule(xPackID, , , xReportID, , , , , )
            End If
        End If

        If Success = True Then
            ScheduleID = oSchedule.GetScheduleID(0, xPackID)

            oSchedule.SetScheduleHistory(True, , , xPackID, , , , oReport.m_HistoryID)

            If gErrorCollection = "" Then
                If outOfProcessAlready = False Then MessageBox.Show("Schedule execution completed!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                Dim oMsg As frmCustomMsg = New frmCustomMsg

                oMsg.ShowMsg("Schedule completed with the following destination failures:" & vbCrLf & vbCrLf & _
                gErrorCollection, "Package Schedule Results")
            End If
        Else
            oSchedule.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", , xPackID, , , , oReport.m_HistoryID)
            _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, gErrorSuggest, , True)
        End If

        clsMarsUI.MainUI.BusyProgress(, , True)

        oReport = Nothing
        oSchedule = Nothing
        gScheduleName = String.Empty

        Try
            crdxCommon.executingForm.Bar1.AutoHide = True
        Catch : End Try
    End Sub

    Public Sub PreviewReport()

        Dim oReport As clsMarsReport = New clsMarsReport
        Dim oSet As frmSetPrintDate = New frmSetPrintDate

        oReport.ViewReport(xReportID)
    End Sub
End Class
