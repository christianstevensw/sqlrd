Imports System
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Imports Microsoft.Win32

Friend Class clsMarsSecurity
    Private key() As Byte = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24}
    Private iv() As Byte = {65, 110, 68, 26, 69, 178, 200, 219}

    Declare Function LogonUserA Lib "advapi32.dll" (ByVal lpszUsername As String, _
                        ByVal lpszDomain As String, _
                        ByVal lpszPassword As String, _
                        ByVal dwLogonType As Integer, _
                        ByVal dwLogonProvider As Integer, _
                        ByRef phToken As IntPtr) As Integer

    Const LOGON32_LOGON_INTERACTIVE As Long = 2
    Const LOGON32_LOGON_NETWORK As Long = 3
    Const LOGON32_PROVIDER_DEFAULT As Long = 0
    Const LOGON32_PROVIDER_WINNT50 As Long = 3
    Const LOGON32_PROVIDER_WINNT40 As Long = 2
    Const LOGON32_PROVIDER_WINNT35 As Long = 1
    Dim oUI As New clsMarsUI


    Public Function _VerifyLogin(ByVal sUser As String, ByVal sPassword As String, ByVal sDomain As String) As Boolean
        Dim lReturn As Long
        Dim lToken As IntPtr = IntPtr.Zero

        lReturn = LogonUserA(sUser, sDomain, sPassword, _
                LOGON32_LOGON_INTERACTIVE, _
                LOGON32_PROVIDER_DEFAULT, lToken)

        If lReturn = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    Public Function _Encrypt(ByVal plainText As String) As Byte()
        ' Declare a UTF8Encoding object so we may use the GetByte 
        ' method to transform the plainText into a Byte array. 
        Try
            Dim utf8encoder As UTF8Encoding = New UTF8Encoding
            Dim inputInBytes() As Byte = utf8encoder.GetBytes(plainText)

            ' Create a new TripleDES service provider

            Dim tdesProvider As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider


            ' The ICryptTransform interface uses the TripleDES 
            ' crypt provider along with encryption key and init vector 
            ' information 
            Dim cryptoTransform As ICryptoTransform = tdesProvider.CreateEncryptor(Me.key, Me.iv)

            ' All cryptographic functions need a stream to output the 
            ' encrypted information. Here we declare a memory stream 
            ' for this purpose. 
            Dim encryptedStream As MemoryStream = New MemoryStream

            Dim cryptStream As CryptoStream = New CryptoStream(encryptedStream, cryptoTransform, CryptoStreamMode.Write)


            ' Write the encrypted information to the stream. Flush the information 
            ' when done to ensure everything is out of the buffer. 
            cryptStream.Write(inputInBytes, 0, inputInBytes.Length)
            cryptStream.FlushFinalBlock()
            encryptedStream.Position = 0

            ' Read the stream back into a Byte array and return it to the calling 
            ' method. 
            Dim result(encryptedStream.Length - 1) As Byte
            encryptedStream.Read(result, 0, encryptedStream.Length)
            cryptStream.Close()
            Return result
        Catch
        End Try
    End Function
    Public Function _Decrypt(ByVal inputInBytes() As Byte) As String
        ' UTFEncoding is used to transform the decrypted Byte Array 
        ' information back into a string. 
        Try
            Dim utf8encoder As UTF8Encoding = New UTF8Encoding

            Dim tdesProvider As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider

            ' As before we must provide the encryption/decryption key along with 
            ' the init vector. 
            Dim cryptoTransform As ICryptoTransform = tdesProvider.CreateDecryptor(Me.key, Me.iv)

            ' Provide a memory stream to decrypt information into 
            Dim decryptedStream As MemoryStream = New MemoryStream
            Dim cryptStream As CryptoStream = New CryptoStream(decryptedStream, cryptoTransform, CryptoStreamMode.Write)
            cryptStream.Write(inputInBytes, 0, inputInBytes.Length)
            cryptStream.FlushFinalBlock()
            decryptedStream.Position = 0

            ' Read the memory stream and convert it back into a string 
            Dim result(decryptedStream.Length - 1) As Byte
            decryptedStream.Read(result, 0, decryptedStream.Length)
            cryptStream.Close()
            Dim myutf As UTF8Encoding = New UTF8Encoding
            Return myutf.GetString(result)
        Catch
            Return String.Empty
        End Try
    End Function

    Public Function _ValidatePassword(ByVal sUserID As String, ByVal sPassword As String, _
    Optional ByVal AdminCheck As Boolean = False) As Boolean
        Dim oData As New clsMarsData
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oSec As New clsMarsSecurity
        Dim Valid As Boolean
        Dim StoredPassword As String

        SQL = "SELECT Password, UserRole, ByteKey FROM CRDUsers WHERE UserID = '" & SQLPrepare(sUserID) & "'"

        If AdminCheck = True Then
            SQL &= " AND UserRole = 'Administrator'"
        End If

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then

            If sUserID.ToLower = "sqlrdadmin" Then
                StoredPassword = "SQLRDAdmin"
            Else
                If IsNull(oRs("bytekey").Value, 0) = 0 Then
                    StoredPassword = Decrypt(IsNull(oRs(0).Value), "qwer][po")
                Else
                    StoredPassword = _DecryptDBValue(IsNull(oRs(0).Value))
                End If
            End If

            If StoredPassword = sPassword Then
                Valid = True
                gRole = oRs(1).Value
            Else
                Valid = False
            End If
        Else
            If sUserID.ToLower = "sqlrdadmin" And sPassword.ToLower = "sqlrdadmin" Then
                Valid = True
                gRole = "Administrator"
            Else
                Valid = False
            End If
        End If

        Return Valid
    End Function

    Public Function iEnc(ByVal PlainText As String, ByVal WatchWord As String)
        Dim encFac As Long
        Dim curVal As Long
        Dim I As Integer

        For I = 1 To Len(WatchWord)
            encFac = encFac + Asc(Mid(WatchWord, I, 1))
        Next

        For I = 1 To Len(PlainText)
            curVal = curVal + Asc(Mid(PlainText, I, 1)) * encFac
        Next

        iEnc = curVal
    End Function

    Public Function _CheckLicense(ByVal sLicense As String, ByVal sCompany As String, ByVal sUser As String) As Boolean
        Dim RegNo(2) As String

        RegNo(0) = iEnc(sUser & sCompany, "mchungula")
        RegNo(1) = iEnc(sUser & "blantyre" & sCompany, "chookoo")
        RegNo(2) = iEnc(sUser & sCompany, "tanika")

        Dim FullReg As String = RegNo(0) & "-" & RegNo(1) & "-" & RegNo(2)

        If FullReg = sLicense Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function _CreateLicense(ByVal sCompany As String, _
    ByVal sUser As String) As String
        Dim RegNo(2) As String

        RegNo(0) = iEnc(sUser & sCompany, "mchungula")
        RegNo(1) = iEnc(sUser & "blantyre" & sCompany, "chookoo")
        RegNo(2) = iEnc(sUser & sCompany, "tanika")

        Dim FullReg As String = RegNo(0) & "-" & RegNo(1) & "-" & RegNo(2)

        Return FullReg
    End Function

    Public Sub _EncryptDBValuesOnce()
        Try
            Dim IsProtected As Boolean

            IsProtected = oUI.ReadRegistry("IProtect", 0)

            If IsProtected = False Then
                Dim oRs As ADODB.Recordset
                Dim oData As New clsMarsData
                Dim nID As Integer
                Dim sValue As String
                Dim sValue2 As String
                Dim SQL As String

                oRs = clsMarsData.GetData("SELECT * FROM ReportAttr")

                Do While oRs.EOF = False
                    nID = oRs("reportid").Value
                    sValue = IsNull(oRs("rptpassword").Value)

                    SQL = "UPDATE ReportAttr SET RptPassword ='" & SQLPrepare(_EncryptDBValue(sValue)) & "' WHERE ReportID =" & nID

                    clsMarsData.WriteData(SQL, False)

                    oRs.MoveNext()
                Loop

                oRs.Close()

                oRs = clsMarsData.GetData("SELECT * FROM SubReportLogin")

                Do While oRs.EOF = False
                    nID = oRs("subloginid").Value
                    sValue = IsNull(oRs("rptpassword").Value)

                    SQL = "UPDATE SubReportLogin SET RptPassword ='" & SQLPrepare(_EncryptDBValue(sValue)) & "' WHERE SubLoginID =" & nID

                    clsMarsData.WriteData(SQL, False)

                    oRs.MoveNext()
                Loop

                oRs.Close()

                oRs = clsMarsData.GetData("SELECT * FROM ReportOptions")

                Do While oRs.EOF = False
                    nID = oRs("optionid").Value
                    sValue = IsNull(oRs("pdfpassword").Value)
                    sValue2 = IsNull(oRs("userpassword").Value)

                    SQL = "UPDATE ReportOptions SET " & _
                    "PDFPassword='" & SQLPrepare(_EncryptDBValue(sValue)) & "', " & _
                    "UserPassword ='" & SQLPrepare(_EncryptDBValue(sValue2)) & "' WHERE OptionID =" & nID

                    clsMarsData.WriteData(SQL, False)

                    oRs.MoveNext()
                Loop

                oRs.Close()


                oRs = clsMarsData.GetData("SELECT * FROM PackageOptions")

                Do While oRs.EOF = False
                    nID = oRs("optionid").Value
                    sValue = IsNull(oRs("pdfpassword").Value)
                    sValue2 = IsNull(oRs("userpassword").Value)

                    SQL = "UPDATE PackageOptions SET " & _
                    "PDFPassword='" & SQLPrepare(_EncryptDBValue(sValue)) & "', " & _
                    "UserPassword ='" & SQLPrepare(_EncryptDBValue(sValue2)) & "' WHERE OptionID =" & nID

                    clsMarsData.WriteData(SQL, False)

                    oRs.MoveNext()
                Loop

                oRs.Close()

                oUI.SaveRegistry("IProtect", 1)

            End If
        Catch : End Try
    End Sub
    Public Shared Function hasUserBeenAssignToObject(ByVal objectID As Integer, ByVal objectType As clsMarsScheduler.enScheduleType) As Boolean
        Dim column As String = ""
        Dim result As Boolean
        Dim table As String

        Select Case objectType
            Case clsMarsScheduler.enScheduleType.AUTOMATION
                column = "autoid"
                table = "automationattr"
            Case clsMarsScheduler.enScheduleType.EVENTBASED
                column = "eventid"
                table = "eventattr6"
            Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                column = "eventpackid"
                table = "eventpackageattr"
            Case clsMarsScheduler.enScheduleType.PACKAGE
                column = "packid"
                table = "packageattr"
            Case clsMarsScheduler.enScheduleType.REPORT
                column = "reportid"
                table = "reportattr"
            Case clsMarsScheduler.enScheduleType.SMARTFOLDER
                column = "smartid"
                table = "smartattr"
            Case Else
                Return True
        End Select

        Dim SQL As String = "SELECT COUNT(*) FROM userview WHERE " & column & " = " & objectID & " AND userid = '" & SQLPrepare(gUser) & "'"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            Dim count As Integer = oRs(0).Value

            oRs.Close()

            If count = 0 Then
                result = False
            Else
                result = True
            End If
        End If

        If result = True Then Return True

        SQL = "SELECT owner FROM " & table & " WHERE " & column & " = " & objectID

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Dim owner As String = IsNull(oRs(0).Value, "")

            oRs.Close()

            If owner.ToLower = gUser.ToLower Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function
    Public Shared Function _HasGroupAccess(ByVal sAccess As String, Optional ByVal objectID As Integer = 0, _
                                           Optional ByVal objectType As clsMarsScheduler.enScheduleType = clsMarsScheduler.enScheduleType.NONE, _
                                           Optional ByVal suppressMessage As Boolean = False) As Boolean
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oReturn As Boolean

        Try
            If gRole.ToLower = "administrator" Or gRole = "" Then
                oReturn = True
            ElseIf gRole.ToLower = "power user" And sAccess = "Create/Edit Schedules" Then
                oReturn = True
            ElseIf gRole.ToLower = "user" And sAccess = "User Manager" Then
                oReturn = False
            Else
                '//for power user group and editing other schedules...
                '//if the role has power group permission then enable creating of schedules
                Dim powerGroup As Boolean = isPowerRole(gRole)

                If powerGroup = True And sAccess = "Create/Edit Schedules" And objectID <> 0 And objectType <> clsMarsScheduler.enScheduleType.NONE Then
                    Dim objectOwner As String = getObjectOwner(objectID, objectType)
                    Dim ownerRole As String = ""

                    If objectOwner <> "" Then
                        ownerRole = getOwnerRole(objectOwner)

                        '//if the owner role is the same as the current role then return a YES!"
                        If ownerRole.ToLower = gRole.ToLower Then
                            oReturn = True
                        Else
                            oReturn = hasUserBeenAssignToObject(objectID, objectType)
                        End If
                    End If
                Else
                    SQL = "SELECT * FROM GroupAttr G INNER JOIN GroupPermissions P ON G.GroupID = P.GroupID WHERE " & _
                    "G.GroupName ='" & SQLPrepare(gRole) & "' AND P.PermDesc = '" & SQLPrepare(_EncryptDBValue(sAccess)) & "'"

                    oRs = clsMarsData.GetData(SQL)

                    If oRs Is Nothing Then
                        oReturn = False
                    ElseIf oRs.EOF = True Then
                        oRs.Close()
                        oReturn = False
                    Else
                        If oRs("permvalue").Value = 1458 Then
                            oReturn = True
                        Else
                            oReturn = False
                        End If

                        oRs.Close()
                    End If
                End If
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            oReturn = False
        Finally
            If oReturn = False And suppressMessage = False Then
                MessageBox.Show("Access denied! The '" & gRole & "' group does not have rights to '" & sAccess & "'. Please contact your SQL-RD administrator.", _
                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End Try

        Return oReturn
    End Function
    'for file encryption
    Public Enum CryptoAction
        'Define the enumeration for CryptoAction.
        ActionEncrypt = 1
        ActionDecrypt = 2
    End Enum

    Public Function CreateKey(ByVal strPassword As String) As Byte()
        'Convert strPassword to an array and store in chrData.
        Dim chrData() As Char = strPassword.ToCharArray
        'Use intLength to get strPassword size.
        Dim intLength As Integer = chrData.GetUpperBound(0)
        'Declare bytDataToHash and make it the same size as chrData.
        Dim bytDataToHash(intLength) As Byte

        'Use For Next to convert and store chrData into bytDataToHash.
        For i As Integer = 0 To chrData.GetUpperBound(0)
            bytDataToHash(i) = CByte(Asc(chrData(i)))
        Next

        'Declare what hash to use.
        Dim SHA512 As New System.Security.Cryptography.SHA512Managed
        'Declare bytResult, Hash bytDataToHash and store it in bytResult.
        Dim bytResult As Byte() = SHA512.ComputeHash(bytDataToHash)
        'Declare bytKey(31).  It will hold 256 bits.
        Dim bytKey(31) As Byte

        'Use For Next to put a specific size (256 bits) of 
        'bytResult into bytKey. The 0 To 31 will put the first 256 bits
        'of 512 bits into bytKey.
        For i As Integer = 0 To 31
            bytKey(i) = bytResult(i)
        Next

        Return bytKey 'Return the key.
    End Function
    Public Function CreateIV(ByVal strPassword As String) As Byte()
        'Convert strPassword to an array and store in chrData.
        Dim chrData() As Char = strPassword.ToCharArray
        'Use intLength to get strPassword size.
        Dim intLength As Integer = chrData.GetUpperBound(0)
        'Declare bytDataToHash and make it the same size as chrData.
        Dim bytDataToHash(intLength) As Byte

        'Use For Next to convert and store chrData into bytDataToHash.
        For i As Integer = 0 To chrData.GetUpperBound(0)
            bytDataToHash(i) = CByte(Asc(chrData(i)))
        Next

        'Declare what hash to use.
        Dim SHA512 As New System.Security.Cryptography.SHA512Managed
        'Declare bytResult, Hash bytDataToHash and store it in bytResult.
        Dim bytResult As Byte() = SHA512.ComputeHash(bytDataToHash)
        'Declare bytIV(15).  It will hold 128 bits.
        Dim bytIV(15) As Byte

        'Use For Next to put a specific size (128 bits) of bytResult into bytIV.
        'The 0 To 30 for bytKey used the first 256 bits of the hashed password.
        'The 32 To 47 will put the next 128 bits into bytIV.
        For i As Integer = 32 To 47
            bytIV(i - 32) = bytResult(i)
        Next

        Return bytIV 'Return the IV.
    End Function

    Public Sub EncryptOrDecryptFile(ByVal strInputFile As String, _
                                 ByVal strOutputFile As String, _
                                 ByVal bytKey() As Byte, _
                                 ByVal bytIV() As Byte, _
                                 ByVal Direction As CryptoAction)
        Try 'In case of errors.

            'Setup file streams to handle input and output.
            Dim fsInput As FileStream = New System.IO.FileStream(strInputFile, FileMode.Open, _
                                                  FileAccess.Read)
            Dim fsOutput As FileStream = New System.IO.FileStream(strOutputFile, _
                                                   FileMode.OpenOrCreate, _
                                                   FileAccess.Write)

            fsOutput.SetLength(0) 'make sure fsOutput is empty

            'Declare variables for encrypt/decrypt process.
            Dim bytBuffer(4096) As Byte 'holds a block of bytes for processing
            Dim lngBytesProcessed As Long = 0 'running count of bytes processed
            Dim lngFileLength As Long = fsInput.Length 'the input file's length
            Dim intBytesInCurrentBlock As Integer 'current bytes being processed
            Dim csCryptoStream As CryptoStream
            'Declare your CryptoServiceProvider.
            Dim cspRijndael As New System.Security.Cryptography.RijndaelManaged
            'Setup Progress Bar
            'Determine if ecryption or decryption and setup CryptoStream.
            Select Case Direction
                Case CryptoAction.ActionEncrypt
                    csCryptoStream = New CryptoStream(fsOutput, _
                    cspRijndael.CreateEncryptor(bytKey, bytIV), _
                    CryptoStreamMode.Write)

                Case CryptoAction.ActionDecrypt
                    csCryptoStream = New CryptoStream(fsOutput, _
                    cspRijndael.CreateDecryptor(bytKey, bytIV), _
                    CryptoStreamMode.Write)
            End Select

            'Use While to loop until all of the file is processed.
            While lngBytesProcessed < lngFileLength
                'Read file with the input filestream.
                intBytesInCurrentBlock = fsInput.Read(bytBuffer, 0, 4096)
                'Write output file with the cryptostream.
                csCryptoStream.Write(bytBuffer, 0, intBytesInCurrentBlock)
                'Update lngBytesProcessed
                lngBytesProcessed = lngBytesProcessed + _
                                        CLng(intBytesInCurrentBlock)

            End While

            'Close FileStreams and CryptoStream.
            csCryptoStream.Close()
            fsInput.Close()
            fsOutput.Close()
        Catch : End Try
    End Sub

    Public Shared Function hasAccessToObject(ByVal objectID As Integer, ByVal objType As clsMarsScheduler.enScheduleType) As Boolean

    End Function
    Public Shared Function getObjectOwner(ByVal nId As Integer, ByVal type As clsMarsScheduler.enScheduleType) As String
        Dim SQL As String
        Dim table As String
        Dim idcol As String
        Dim owner As String = ""

        Dim oRs As ADODB.Recordset

        Select Case type
            Case clsMarsScheduler.enScheduleType.AUTOMATION
                table = "automationattr"
                idcol = "autoid"
            Case clsMarsScheduler.enScheduleType.EVENTBASED
                table = "eventattr6"
                idcol = "eventid"
            Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                table = "eventpackageattr"
                idcol = "eventpackid"
            Case clsMarsScheduler.enScheduleType.PACKAGE
                table = "packageattr"
                idcol = "packid"
            Case clsMarsScheduler.enScheduleType.REPORT
                table = "reportattr"
                idcol = "reportid"
        End Select

        SQL = "SELECT owner FROM " & table & " WHERE " & idcol & " = " & nId

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            owner = IsNull(oRs("owner").Value, "")
            oRs.Close()
        End If

        Return owner
    End Function

    Public Shared Function getOwnerRole(ByVal owner As String) As String
        Dim SQL As String = "SELECT userrole FROM crdusers WHERE userid ='" & SQLPrepare(owner) & "'"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim userRole As String = ""

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            userRole = IsNull(oRs("userrole").Value)

            oRs.Close()
        End If

        Return userRole
    End Function

    Public Shared Function getObjectOwnerRole(ByVal nid As Integer, ByVal type As clsMarsScheduler.enScheduleType)
        Dim objOwner As String = getObjectOwner(nid, type)
        Dim objOwnerRole As String = getOwnerRole(objOwner)

        Return objOwnerRole
    End Function
    Public Shared Function isPowerRole(ByVal role As String) As Boolean
        Dim SQL As String = "SELECT * FROM GroupAttr G INNER JOIN GroupPermissions P ON G.GroupID = P.GroupID WHERE " & _
                  "G.GroupName ='" & SQLPrepare(role) & "' AND P.PermDesc = '" & SQLPrepare(_EncryptDBValue("Full access to other group members' schedules")) & "' AND permvalue = 1458"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            oRs.Close()
            Return True
        Else
            Return False
        End If
    End Function
End Class
