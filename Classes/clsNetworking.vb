Imports DotRas

Friend Class clsNetworking

    Dim oUI As New clsMarsUI
    Dim sError As String = ""
    Dim nError As Integer = 0
    Dim IsDone As Boolean = False
    Dim dialer As RasDialer
    Dim connectionHandle As RasHandle
    Sub New()
        dialer = New RasDialer
        dialer.PhoneBookPath = RasPhoneBook.GetPhoneBookPath(RasPhoneBookType.AllUsers)
    End Sub
    Public Sub _Disconnect()
        Try
            If dialer IsNot Nothing Then
                Dim connection As RasConnection = RasConnection.GetActiveConnectionByHandle(connectionHandle)
                If (connection IsNot Nothing) Then
                    ' The connection has been found, disconnect it.
                    connection.HangUp()
                End If

                dialer.Dispose()
                oUI.BusyProgress(, , True)
            End If
        Catch : End Try
    End Sub
    Public Function _DialConnection(ByVal sName As String, Optional ByVal ScheduleInfo As String = "") As Boolean
        Try
            dialer.EntryName = sName

            clsMarsUI.BusyProgress(50, "Connecting to " & sName)

            connectionHandle = dialer.Dial()

            Return True
        Catch ex As Exception
            gErrorDesc = ScheduleInfo & " " & ex.Message
            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
            gErrorLine = _GetLineNumber(ex.StackTrace)
            gErrorNumber = Err.Number
            Return False
        End Try
    End Function

   
End Class
