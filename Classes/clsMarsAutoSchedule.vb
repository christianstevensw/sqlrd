Friend Class clsMarsAutoSchedule
    Public Function CopyAuto(ByVal nClipboard As Integer, ByVal nParent As Integer)
        On Error GoTo Trap
        Dim SQL As String
        Dim sWhere As String = " WHERE AutoID = " & nClipboard
        Dim sColumns As String
        Dim sValues As String
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim AutoID As Integer = clsMarsData.CreateDataID("automationattr", "autoid")
        Dim Ok As Boolean
        Dim ReportTitle As String
        Dim NewScheduleID As Integer

        Dim oUI As clsMarsUI = New clsMarsUI

RetryName:

        ReportTitle = InputBox("Please enter a name for the schedule", Application.ProductName, "")

        If ReportTitle = "" Then Exit Function

        If clsMarsUI.candoRename(ReportTitle, nParent, clsMarsScheduler.enScheduleType.AUTOMATION) = False Then
            Dim retry As DialogResult = MessageBox.Show("An Automation schedule with this name already exists in this location. Rename and try again?", _
            Application.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation)

            If retry = DialogResult.OK Then
                GoTo retryName
            Else
                Return Nothing
            End If
        End If

        'copy the report attributes

        oUI.BusyProgress(30, "Copying schedule attributes...")

        sColumns = "AutoID,AutoName,Parent,Owner"

        sValues = AutoID & "," & _
            "'" & SQLPrepare(ReportTitle) & "', " & nParent & ", '" & gUser & "'"

        SQL = "INSERT INTO AutomationAttr (" & sColumns & ") " & _
        "SELECT " & sValues & " FROM AutomationAttr " & sWhere

        Ok = clsMarsData.WriteData(SQL)

        'copy the schedule attributes
        If Ok = True Then
            oUI.BusyProgress(55, "Copying schedule data...")

            sColumns = "ScheduleID,Frequency,StartDate,EndDate,NextRun,StartTime," & _
            "Repeat,RepeatInterval,RepeatUntil,Status,AutoID"

            NewScheduleID = clsMarsData.CreateDataID("scheduleattr", "scheduleid")

            sValues = NewScheduleID & "," & _
            "Frequency,StartDate,EndDate,NextRun,StartTime," & _
            "Repeat,RepeatInterval,RepeatUntil,Status," & AutoID

            SQL = "INSERT INTO ScheduleAttr (" & sColumns & ") " & _
            "SELECT " & sValues & " FROM ScheduleAttr " & sWhere

            Ok = clsMarsData.WriteData(SQL)
        End If

        'copy tasks
        If Ok = True Then
            Dim oSchedule As New clsMarsScheduler
            Dim ScheduleID As Integer = oSchedule.GetScheduleID(, , nClipboard)
            Dim oTasks As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM Tasks WHERE ScheduleID =" & ScheduleID)

            If oTasks IsNot Nothing Then
                Do While oTasks.EOF = False
                    Dim taskID As Integer = oTasks("taskid").Value

                    oData.CopyRow("Tasks", taskID, " WHERE TaskID =" & taskID, "TaskID", , , , , , , NewScheduleID, , , )

                    oTasks.MoveNext()
                Loop

                oTasks.Close()
            End If
        End If

        If Ok = True Then
            Dim oProp As New frmAutoProp

            oUI.BusyProgress(99, "Loading new schedule properties...")

            oUI.BusyProgress(, , True)

            oProp.EditSchedule(AutoID)

            'oUI.RefreshView(oWindow(nWindowCurrent))
        End If

        Return AutoID
Trap:
        gErrorDesc = Err.Description
        gErrorNumber = Err.Number
        gErrorSource = "clsMarsAutoSchedule._CopyAuto"
        gErrorLine = Err.Erl

    End Function

    Public Sub ExecuteAutomationSchedule(ByVal nID As Integer)
        Dim Success As Boolean
        Dim oTask As New clsMarsTask
        Dim ScheduleID As Integer
        Dim oSchedule As New clsMarsScheduler
        Dim ok As Boolean

        ScheduleStart = Now

        gScheduleOwner = clsMarsScheduler.getScheduleOwner(nID, clsMarsScheduler.enScheduleType.AUTOMATION)
        gScheduleFolderName = clsMarsUI.getObjectParentName(nID, clsMarsScheduler.enScheduleType.AUTOMATION)

        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT AutoName FROM AutomationAttr WHERE AutoID =" & nID)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then
                gScheduleName = oRs(0).Value
            End If

            oRs.Close()
        End If

        ScheduleID = oSchedule.GetScheduleID(, , nID)

        ok = oTask.ProcessTasks(ScheduleID, "NONE")

        If ok = True Then
            oSchedule.SetScheduleHistory(True, , , , nID)
        Else
            oSchedule.SetScheduleHistory(False, gErrorDesc, , , nID)
            _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine)
        End If

        gScheduleOwner = ""
    End Sub

    Public Function RunAutomationSchedule(ByVal nID As Integer) As Boolean
        Dim Success As Boolean
        Dim oTask As New clsMarsTask
        Dim ScheduleID As Integer
        Dim oSchedule As New clsMarsScheduler
        Dim ok As Boolean

        ScheduleStart = Now

        gScheduleOwner = clsMarsScheduler.getScheduleOwner(nID, clsMarsScheduler.enScheduleType.AUTOMATION)
        gScheduleFolderName = clsMarsUI.getObjectParentName(nID, clsMarsScheduler.enScheduleType.AUTOMATION)

        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT AutoName FROM AutomationAttr WHERE AutoID =" & nID)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then
                gScheduleName = oRs(0).Value
            End If

            oRs.Close()
        End If

        ScheduleID = oSchedule.GetScheduleID(, , nID)

        ok = oTask.ProcessTasks(ScheduleID, "NONE")

        If ok = True Then
            oSchedule.SetScheduleHistory(True, , , , nID)
        Else
            oSchedule.SetScheduleHistory(False, gErrorDesc, , , nID)
        End If

        gScheduleOwner = ""
        Return ok
    End Function
End Class
