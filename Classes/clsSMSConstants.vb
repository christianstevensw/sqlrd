Friend Class clsSMSConstants


    Public asUNICODE_OFF As Integer = 0                          ' Non Unicode (default)
    Public asUNICODE_ON As Integer = 1                           ' Unicode

    Public asMESSAGE_GSM As Integer = 0                          ' Send Through GSM or other SMS device
    Public asMESSAGE_PROVIDER As Integer = 1                     ' Send Through SMSC dial-in Provider
    Public asMESSAGE_PAGER As Integer = 2                        ' Send Message to numeric pager

    Public asPROVIDER_TYPE_UCP As Integer = 0                    ' UCP Provider
    Public asPROVIDER_TYPE_TAP_DEFAULT As Integer = 1            ' Standard TAP provider
    Public asPROVIDER_TYPE_TAP_NOLF As Integer = 2               ' TAP provider, no request for LineFeed
    Public asPROVIDER_TYPE_TAP_NOEOT As Integer = 3              ' TAP provider, no request for EOT

    Public asDATAFORMAT_DEFAULT As Integer = 0                   ' Inherit TAPI settings, or use default COM port settings
    Public asDATAFORMAT_8N1 As Integer = 1                       ' Use 8 databits, 1 stopbit, none parity
    Public asDATAFORMAT_7E1 As Integer = 2                       ' Use 7 databits, 1 stopbit, even parity

    Public asERR_SUCCESS As Integer = 0                          ' Success
    Public asERR_EVALUATIONEXPIRED As Integer = 1                ' Evaluation period of the software has expired
    Public asERR_INVALIDLICENSE As Integer = 2                   ' License code is invalid
    Public asERR_RECEIVENOTLICENSED As Integer = 3               ' License allows only to send messages, not to receive messages

    Public asERR_INVALIDPROVIDER As Integer = 10                 ' Provider is invalid

    Public asERR_INVALIDSENDER As Integer = 30                   ' Sender ID is invalid
    Public asERR_INVALIDRECIPIENT As Integer = 31                ' Recipient ID is invalid
    Public asERR_INVALIDMESSAGE As Integer = 32                  ' Message is invalid
    Public asERR_MESSAGETOOLONG As Integer = 33                  ' Message is too long

    Public asERR_ISRECEIVING As Integer = 40                     ' Software is in receive mode
    Public asERR_CANTRECEIVE As Integer = 41                     ' Cannot set receive mode

    Public asERR_MODEMRESPONSE_NONE As Integer = 50              ' No response at all
    Public asERR_MODEMRESPONSE_UNEXPECTED As Integer = 51        ' Unexpected response from modem
    Public asERR_MODEMRESPONSE_ERROR As Integer = 52             ' Error response from modem
    Public asERR_MODEMRESPONSE_NODIALTONE As Integer = 53        ' No dialtone detected upon attempt to connect to remote party
    Public asERR_MODEMRESPONSE_NOCARRIER As Integer = 54         ' No carrier reported while connecting to remote party
    Public asERR_MODEMRESPONSE_NOANSWER As Integer = 55          ' No answer reported while connecting to remote party
    Public asERR_MODEMRESPONSE_BUSY As Integer = 56              ' Busy tone reported while connecting to remote party


    Public asERR_UCP_INVALIDPROVIDER As Integer = 60             ' Invalid UCP provider
    Public asERR_UCP_NORESPONSEFROMPROVIDER As Integer = 61      ' No repsonse from UCP provider
    Public asERR_UCP_PROVIDERSYNTAXERROR As Integer = 62         ' UCP provider reporeted a syntax error
    Public asERR_UCP_PROVIDERNACK As Integer = 63                ' UCP provider rejected the message (negative acknowledgement)


    Public asERR_TAPPHACEC_IDENTIFACKFAILED As Integer = 70      ' Error during inital negotiation phase
    Public asERR_TAPPHASEC_INVALIDPAGERNUMBER As Integer = 71    ' Reported by some providers, when subscriber number is formatted as 003164789564 instead of 3164789564
    Public asERR_TAPPHASEC_NOMESSAGETOTHISPAGE As Integer = 72   ' Recipient number has correct syntax, but sending to this number is prohibited by provider
    Public asERR_TAPPHASEC_INVALIDMESSAGE As Integer = 73        ' Invalid message
    Public asERR_TAPPHASEC_TRANSACTIONACKNOTRECEIVED As Integer = 74  ' Message was sent, but no ACK received


    Public asCOMMERR_GENERICERROR As Integer = 100               ' Generic port Error
    Public asCOMMERR_INVALIDPARAM As Integer = 101               ' Invalid port parameter
    Public asCOMMERR_INVALIDPORT As Integer = 102                ' Invalid port. Most probably, port does not exist or is in use
    Public asCOMMERR_PORTALREADYOPENED As Integer = 103          ' Unable to perform operation because port it already opened
    Public asCOMMERR_OPENPORTFAILED As Integer = 104             ' Generic open port error
    Public asCOMMERR_INITIALIZEPORTFAILED As Integer = 105       ' Unable to initialize port
    Public asCOMMERR_WRITEPORTFAILED As Integer = 106            ' Unable to write to port
    Public asCOMMERR_WRITEPORTDIRECTFAILED As Integer = 107      ' Unable to write directly to port
    Public asCOMMERR_READPORTFAILED As Integer = 108             ' Unable to read from port
    Public asCOMMERR_GETCOMSTATEFAILED As Integer = 109          ' Unable to retrieve device information
    Public asCOMMERR_SETCOMSTATEFAILED As Integer = 110          ' Unable to change device settings
    Public asCOMMERR_PURGECOMFAILED As Integer = 111             ' Unable to purge the port
    Public asCOMMERR_TIMEOUT As Integer = 112                    ' Port timeout
    Public asCOMMERR_COMMANDNOTSUPPORTED As Integer = 113        ' AT Command not recognized on this device

    Public asTAPIERR_CANTINITIALIZE As Integer = 200             ' Unable to initialize TAPI Support
    Public asTAPIERR_CANTSELECTDEVICE As Integer = 201           ' Cannot find specified device
    Public asTAPIERR_CANTOPENDEVICE As Integer = 202             ' Cannot open selected device



End Class


