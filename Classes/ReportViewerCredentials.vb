﻿Imports System.Net
Imports System.Web
Public Class ReportViewerCredentials
    Implements Microsoft.Reporting.WinForms.IReportServerCredentials

    Private _userName As String

    Private _password As String

    Private _domain As String

    Public Sub New(ByVal userName As String, ByVal password As String, ByVal domain As String)

        _userName = userName

        _password = password

        _domain = domain

    End Sub

    Public Sub New()

        _userName = Nothing

        _password = Nothing

        _domain = Nothing

    End Sub

    Public ReadOnly Property ImpersonationUser() As System.Security.Principal.WindowsIdentity Implements Microsoft.Reporting.WinForms.IReportServerCredentials.ImpersonationUser

        Get

            Return Nothing

        End Get

    End Property

    Public ReadOnly Property NetworkCredentials() As System.Net.ICredentials Implements Microsoft.Reporting.WinForms.IReportServerCredentials.NetworkCredentials

        Get

            'Return New Net.NetworkCredential(_userName, _password, _domain)

            'Return null for custom security extension

            Return Nothing

        End Get

    End Property

    Public Function GetFormsCredentials(ByRef authCookie As System.Net.Cookie, ByRef userName As String, ByRef password As String, ByRef authority As String) As Boolean Implements Microsoft.Reporting.WinForms.IReportServerCredentials.GetFormsCredentials

        userName = password = authority = Nothing

        Dim cookie As HttpCookie = System.Web.HttpContext.Current.Request.Cookies("sqlAuthCookie")

        If cookie Is Nothing Then

            HttpContext.Current.Response.Redirect("login.aspx")

        End If

        Dim netCookie As Cookie = New Cookie(cookie.Name, cookie.Value)

        If cookie.Domain Is Nothing Then

            netCookie.Domain = HttpContext.Current.Request.ServerVariables("SERVER_NAME").ToUpper

        End If

        netCookie.Expires = cookie.Expires

        netCookie.Path = cookie.Path

        netCookie.Secure = cookie.Secure

        authCookie = netCookie

        Return True

    End Function
End Class
