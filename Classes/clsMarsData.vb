Imports System.Data.OleDb
Imports System.IO
Imports Microsoft.Win32
Imports System.Random
Imports System.Diagnostics
Imports System.Reflection

Friend Class clsMarsData
    Dim sPath As String
    Dim oRs As ADODB.Recordset

    Public Shared DataItem As New clsMarsData
    Public Shared UseExecute As Boolean

    Public Enum OSBitType
        Bit32 = 0
        Bit64 = 1
    End Enum

    Public Function OSBit() As OSBitType
        Dim pSize As Integer
        Dim xmpPlatform As OSBitType

        pSize = UIntPtr.Size

        If pSize = 8 Then

            xmpPlatform = OSBitType.Bit64

        Else

            xmpPlatform = OSBitType.Bit32

        End If

        Return xmpPlatform
    End Function
    Function WhatsMyName() As String

        Dim StackFrame As StackFrame = New StackFrame()
        Dim MethodBase As MethodBase = StackFrame.GetMethod
        Return MethodBase.Name

        WhoCalledMe()
    End Function

    Shared Function WhoCalledMe() As String
        Dim StackTrace As StackTrace = New StackTrace
        Dim StackFrame As StackFrame = StackTrace.GetFrame(1)
        Dim MethodBase As MethodBase = StackFrame.GetMethod

        Return MethodBase.Name
    End Function

    Public Sub setLocalDBOptions()
        Try
            If gConType <> "LOCAL" Then Return

            '//set recovery mode to simple
            Dim SQL As String = "ALTER DATABASE [SQL-RD] SET RECOVERY SIMPLE"
            clsMarsData.WriteData(SQL, False)

            '//create index on scheduleattr for the nextrun
            SQL = "CREATE INDEX ind_nextrun on ScheduleAttr (NextRun DESC)"
            clsMarsData.WriteData(SQL, False)

            '//set file growth to unrestricted
            SQL = "ALTER DATABASE [SQL-RD] MODIFY File (NAME = 'Live_Data', MAXSIZE = UNLIMITED)"
            clsMarsData.WriteData(SQL, False)

            '//set auto-shrink to ON
            SQL = "EXEC SP_DBOPTION 'SQL-RD' , 'autoshrink' , 'on'"
            clsMarsData.WriteData(SQL, False)

            '//set log file max size to 300MB
            SQL = "ALTER DATABASE [SQL-RD] MODIFY FILE (Name = 'Live_log', MAXSIZE = 300MB)"
            clsMarsData.WriteData(SQL, False)

            '//get the date when the database was last shrunk
            Dim lastShrunk As Date = clsMarsUI.MainUI.ReadRegistry("dbccLastExecute", Date.Now)

            '//if the last shrunk date is a month ago then lets shrink our files
            If Date.Now.Subtract(lastShrunk).TotalDays > 30 Then
                SQL = "DBCC SHRINKFILE ('Live_log', 1)"
                clsMarsData.WriteData(SQL, False)
                SQL = "DBCC SHRINKFILE ('Live_Data', 1)"
                clsMarsData.WriteData(SQL, False)
                clsMarsUI.MainUI.SaveRegistry("dbccLastExecute", Date.Now)
            End If

            createUTCDateFunctionOnDB()
        Catch : End Try
    End Sub

    Public Shared Function DBFunctionExists(functionName As String) As Boolean
        Dim SQL As String = String.Format("select OBJECT_ID ('{0}', 'FN')", functionName)
        Dim ors As ADODB.Recordset = GetData(SQL)

        If ors IsNot Nothing AndAlso ors.EOF = False Then
            Dim objectid = IsNull(ors(0).Value)

            If objectid <> "" Then
                DBFunctionExists = True
            Else
                DBFunctionExists = False
            End If

            ors.Close()
            ors = Nothing
        Else
            DBFunctionExists = False
        End If
    End Function

    Private Sub createUTCDateFunctionOnDB()

        If gConType = "DAT" Then Return

        'dbo.convertToUTCDate
        Dim SQL As String
        Dim ex As Exception

        If DBFunctionExists("dbo.convertToUTCDate") Then
            Return
        Else
            SQL = String.Format("CREATE FUNCTION dbo.convertToUTCDate(@thedate DATETIME) " & _
            "RETURNS DateTime " & _
            "AS " & _
            "BEGIN " & _
            "DECLARE @utcdate DATETIME; " & _
            "SELECT @utcdate = DATEADD(second, DATEDIFF(second, GETDATE(), GETUTCDATE()), @thedate); " & _
            "RETURN @utcdate; " & _
            "END;")

            WriteData(SQL, False)
        End If
    End Sub
    Public Shared Function fillDataset(ByRef ds As DataSet, ByVal SQL As String)

        SQL = prepforbinaryCollation(SQL)

        If gConType = "LOCAL" Then
            Dim sqlcon As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(clsMigration.m_localConStringdotNET)

            Using sqlcon
                Dim sqlAd As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(SQL, sqlcon)

                sqlAd.Fill(ds)
                sqlcon.Close()
            End Using
        Else
            'Con = "Provider=MSDASQL.1;Password=" & .txtPassword.Text & ";Persist Security Info=True;User ID=" & .txtUserID.Text & ";Data Source=" & .cmbDSN.Text
            'Con2 = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & _EncryptDBValue(.txtPassword.Text)

            Dim dsn, userid, password As String
            Dim conString As String = clsMarsUI.MainUI.ReadRegistry("ConString", "")


            If conString <> "" Then
                conString = clsMarsUI.MainUI.ReadRegistry("ConString", "", True)
                dsn = conString.Split(";")(4)
                dsn = dsn.Split("=")(1)
                userid = conString.Split(";")(3)
                userid = userid.Split("=")(1)
                password = conString.Split(";")(1)
                password = password.Split("=")(1)
            Else
                conString = clsMarsUI.MainUI.ReadRegistry("ConString2", "", True)
                dsn = conString.Split("|")(0)
                userid = conString.Split("|")(1)
                password = _DecryptDBValue(conString.Split("|")(2))
            End If

            Dim odbccon As System.Data.Odbc.OdbcConnection = New System.Data.Odbc.OdbcConnection("Dsn=" & dsn & ";Uid=" & userid & ";Pwd=" & password)

            Using odbccon
                Dim odbcAd As Odbc.OdbcDataAdapter = New Odbc.OdbcDataAdapter(SQL, odbccon)

                odbcAd.Fill(ds)
                odbccon.Close()
            End Using
        End If
    End Function
    <DebuggerStepThrough()> _
       Public Shared Function prepforbinaryCollation(ByVal input As String) As String
        Try
            Dim output As String = ""
            Dim conserve As Boolean = False

            For Each s As String In input
                'check to see if string conservation should be turned on or off

                If s = "'" And conserve = False Then
                    conserve = True
                ElseIf s = "'" And conserve = True Then
                    conserve = False
                End If

                If conserve = False Then
                    output &= s.ToLower
                Else
                    output &= s
                End If
            Next

            Return output
        Catch ex As Exception
            Return input
        End Try
    End Function

    Public Shared Sub IamAlive()
        Try
            Dim alivePath As String = sAppPath & "processAlive\"

            If IO.Directory.Exists(alivePath) = False Then
                IO.Directory.CreateDirectory(alivePath)
            End If

            SaveTextToFile(Date.Now, alivePath & Process.GetCurrentProcess.Id, , False)
        Catch : End Try
    End Sub

    Public Function cacheRecordset(ByVal oRs As ADODB.Recordset) As DataTable
10:     Dim dt As DataTable = New DataTable

20:     For Each fld As ADODB.Field In oRs.Fields
30:         dt.Columns.Add(fld.Name)
40:     Next

50:     Do While oRs.EOF = False
            Dim row As DataRow = dt.Rows.Add

60:         For Each fld As ADODB.Field In oRs.Fields
70:             row(fld.Name) = IsNull(fld.Value)
80:         Next

90:         oRs.MoveNext()
100:    Loop


120:    Return dt
    End Function

    Public Shared Sub ValidateFolderStruct()
        Dim SQL As String = "SELECT FolderID,Parent FROM Folders WHERE Parent > 0"
        Dim oRs As ADODB.Recordset
        Dim newParent As Integer = 0

        oRs = GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim folderID As Integer = oRs(0).Value
                Dim parentID As Integer = oRs(1).Value

                If parentID > 0 Then
                    If DoesParentExist(parentID) = False Then
                        If newParent = 0 Then newParent = GetRecoveredFolder()

                        SQL = "UPDATE Folders SET Parent = " & newParent & " WHERE FolderID = " & folderID

                        WriteData(SQL)
                    End If
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub

    Private Shared Function DoesParentExist(ByVal parentID As Integer) As Boolean
        Dim SQL As String = "SELECT * FROM Folders WHERE FolderID = " & parentID
        Dim oRs As ADODB.Recordset = GetData(SQL)

        If oRs IsNot Nothing Then
            If oRs.EOF = True Then
                oRs.Close()
                Return False
            Else
                oRs.Close()
                Return True
            End If
        Else
            Return False
        End If
    End Function
    Public Shared Sub ValidateSystemStruct()
        Try
            Dim SQL As String
            Dim recoveredID As Integer = 0

            SQL = "SELECT ReportID, Parent FROM ReportAttr WHERE (Parent NOT IN (SELECT FolderID FROM Folders) AND PackID = 0) OR (Parent = 0 AND PackID = 0)"

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing Then

                Do While oRs.EOF = False
                    If recoveredID = 0 Then recoveredID = clsMarsData.GetRecoveredFolder

                    Dim reportID As Integer = oRs.Fields("reportid").Value

                    SQL = "UPDATE ReportAttr SET Parent = " & recoveredID & " WHERE ReportID =" & reportID

                    clsMarsData.WriteData(SQL)

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            SQL = "SELECT PackID FROM PackageAttr WHERE Parent NOT IN (SELECT FolderID FROM Folders)"

            oRs = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False

                    If recoveredID = 0 Then recoveredID = clsMarsData.GetRecoveredFolder

                    Dim packID As Integer = oRs.Fields("packID").Value

                    SQL = "UPDATE PackageAttr SET Parent = " & recoveredID & " WHERE PackID =" & packID

                    clsMarsData.WriteData(SQL)

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            SQL = "SELECT AutoID FROM AutomationAttr WHERE Parent NOT IN (SELECT FolderID FROM Folders)"

            oRs = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    If recoveredID = 0 Then recoveredID = clsMarsData.GetRecoveredFolder

                    Dim autoID As Integer = oRs.Fields("autoid").Value

                    SQL = "UPDATE AutomationAttr SET Parent = " & recoveredID & " WHERE AutoID =" & autoID

                    clsMarsData.WriteData(SQL)

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            SQL = "SELECT EventID FROM EventAttr6 WHERE Parent NOT IN (SELECT FolderID FROM Folders) AND PackID =0"

            oRs = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    If recoveredID = 0 Then recoveredID = clsMarsData.GetRecoveredFolder

                    Dim eventID As Integer = oRs.Fields("eventid").Value

                    SQL = "UPDATE EventAttr6 SET Parent = " & recoveredID & " WHERE EventID =" & eventID

                    clsMarsData.WriteData(SQL)

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            SQL = "SELECT EventPackID FROM EventPackageAttr WHERE Parent NOT IN (SELECT FolderID FROM Folders)"

            oRs = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    If recoveredID = 0 Then recoveredID = clsMarsData.GetRecoveredFolder

                    Dim eventPackID As Integer = oRs.Fields("eventpackid").Value

                    SQL = "UPDATE EventPackageAttr SET Parent = " & recoveredID & " WHERE EventPackID =" & eventPackID

                    clsMarsData.WriteData(SQL)

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            ValidateFolderStruct()
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), "", True, True)
        End Try

    End Sub

    Private Shared Function GetRecoveredFolder() As Integer
        Dim SQL As String = "SELECT FolderID FROM Folders WHERE FolderName LIKE '.recovered'"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then
                Dim folderID As Integer = oRs.Fields("folderid").Value

                oRs.Close()

                Return folderID
            Else

                oRs.Close()

                Dim folderID As Integer = clsMarsData.CreateDataID("folders", "folderid")

                Dim cols As String = "FolderID,FolderName,Parent"

                Dim vals As String = folderID & "," & _
                "'.recovered', 0"

                SQL = "INSERT INTO Folders (" & cols & ") VALUES (" & vals & ")"

                clsMarsData.WriteData(SQL)

                Return folderID
            End If
        End If
    End Function

    Public Shared Sub CleanThreadManager()
        Try
            Dim SQL As String

            SQL = "DELETE FROM ThreadManager WHERE EntryDate IS NULL"

            clsMarsData.WriteData(SQL)

            SQL = "DELETE FROM ThreadManager WHERE DATEDIFF([h],EntryDate,[date]) >= 6"

            If gConType = "DAT" Then
                SQL = SQL.Replace("[h]", "'h'").Replace("[date]", "Now()")
            Else
                SQL = SQL.Replace("[h]", "hh").Replace("[date]", "GetDate()")
            End If

            clsMarsData.WriteData(SQL, False)

            SQL = "SELECT * FROM ThreadManager"

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
            Dim ReportID, PackID, AutoID, EventID, processID As Integer
            Dim entryDate As Date
            Dim autoCalc As Boolean

            Do While oRs.EOF = False
                ReportID = IsNull(oRs("reportid").Value, 0)
                PackID = IsNull(oRs("packid").Value, 0)
                AutoID = IsNull(oRs("autoid").Value, 0)
                EventID = IsNull(oRs("eventid").Value, 0)
                entryDate = oRs("entrydate").Value
                processID = IsNull(oRs("processid").Value, 0)

                Dim failAfter As Decimal = 0
                'Dim scheduleID As Integer = clsMarsScheduler.GetScheduleID(ReportID, PackID, AutoID)

                If ReportID > 0 Then
                    SQL = "SELECT AssumeFail,AutoCalc FROM ReportAttr WHERE ReportID =" & ReportID
                ElseIf PackID > 0 Then
                    SQL = "SELECT AssumeFail,AutoCalc FROM PackageAttr WHERE PackID =" & PackID
                ElseIf EventID > 0 Then
                    SQL = "SELECT AutoFailAfter,AutoCalc FROM EventAttr6 WHERE EventID =" & EventID
                Else
                    GoTo skip
                End If

                Dim oRs1 As ADODB.Recordset = clsMarsData.GetData(SQL)

                If oRs1 Is Nothing Then GoTo skip

                If oRs1.EOF = False Then
                    failAfter = IsNull(oRs1.Fields(0).Value, 0)
                    autoCalc = IsNull(oRs1.Fields(1).Value, 0)
                End If

                If autoCalc = True Then
                    failAfter = clsMarsScheduler.globalItem.getScheduleDuration(ReportID, PackID, 0, 0, EventID, clsMarsScheduler.enCalcType.MEDIAN, 3)
                End If

                If failAfter = 0 Then GoTo skip

                If Now.Subtract(entryDate).TotalMinutes > failAfter Then
                    If EventID = 0 Then
                        clsMarsScheduler.globalItem.SetScheduleHistory(False, "Schedule removed from execution queue as the execution time exceeded " & failAfter & " minutes.", ReportID, PackID)
                    Else
                        clsMarsEvent.SaveEventHistory(EventID, False, "Schedule removed from execution queue as the execution time exceeded " & failAfter & " minutes.")
                    End If

                    If EventID > 0 Then
                        SQL = "DELETE FROM ThreadManager WHERE EventID =" & EventID
                    ElseIf ReportID > 0 Then
                        SQL = "DELETE FROM ThreadManager WHERE ReportID =" & ReportID
                    ElseIf PackID > 0 Then
                        SQL = "DELETE FROM ThreadManager WHERE PackID =" & PackID
                    Else
                        GoTo skip
                    End If

                    clsMarsData.WriteData(SQL)
                End If
skip:
                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Public Shared Sub CreateSampeDSN()

        On Error Resume Next

        Dim systemPath As String = Environment.GetFolderPath(Environment.SpecialFolder.System)

        If systemPath.EndsWith("\") = False Then systemPath &= "\"

        clsMarsUI.MainUI.SaveRegistry(Registry.LocalMachine, "SOFTWARE\ODBC\ODBC.INI\SQL-RD Samples", "DBQ", sAppPath & "Samples\sampledb.mdb")
        clsMarsUI.MainUI.SaveRegistry(Registry.LocalMachine, "SOFTWARE\ODBC\ODBC.INI\SQL-RD Samples", "Driver", systemPath & "odbcjt32.dll")
        clsMarsUI.MainUI.SaveRegistry(Registry.LocalMachine, "SOFTWARE\ODBC\ODBC.INI\SQL-RD Samples", "DriverId", 25)
        clsMarsUI.MainUI.SaveRegistry(Registry.LocalMachine, "SOFTWARE\ODBC\ODBC.INI\SQL-RD Samples", "FIL", "MS Access;")
        clsMarsUI.MainUI.SaveRegistry(Registry.LocalMachine, "SOFTWARE\ODBC\ODBC.INI\SQL-RD Samples", "SafeTransactions", 0)
        clsMarsUI.MainUI.SaveRegistry(Registry.LocalMachine, "SOFTWARE\ODBC\ODBC.INI\SQL-RD Samples\Engines\Jet", "MaxBufferSize", 2048)
        clsMarsUI.MainUI.SaveRegistry(Registry.LocalMachine, "SOFTWARE\ODBC\ODBC.INI\SQL-RD Samples\Engines\Jet", "PageTimeout", 5)
        clsMarsUI.MainUI.SaveRegistry(Registry.LocalMachine, "SOFTWARE\ODBC\ODBC.INI\SQL-RD Samples\Engines\Jet", "Threads", 3)
        clsMarsUI.MainUI.SaveRegistry(Registry.LocalMachine, "SOFTWARE\ODBC\ODBC.INI\SQL-RD Samples\Engines\Jet", "UserCommitSync", "Yes")

        clsMarsUI.MainUI.SaveRegistry(Registry.LocalMachine, "SOFTWARE\ODBC\ODBC.INI\ODBC Data Sources", "SQL-RD Samples", "Microsoft Access Driver (*.mdb)")

    End Sub
    Private Shared Function TestConnection()
#If DEBUG Then
        Return Nothing
#End If

        Dim nRetry As Integer

        Try
            Dim oRs As ADODB.Recordset = New ADODB.Recordset

            oRs.Open("SELECT TOP 1 * FROM ReportAttr", gCon, ADODB.CursorTypeEnum.adOpenStatic)

            oRs.Close()

            Return Nothing

retry:
            Try
                gCon.Close()
            Catch : End Try

            gCon.ConnectionTimeout = 0
            gCon.CommandTimeout = 0
            gCon.Open(sCon)
        Catch ex As Exception
            _ErrorHandle("SQL-RD has lost connection to your database. SQL-RD will now exit.", _
            -21478000000, "clsMarsData._TestConnection", 29, "Please check your database server and your network then restart your PC and retry.")
            End
        End Try
    End Function

    Public Shared Function GetData(ByVal sQuery As String, _
        Optional ByVal oLock As ADODB.CursorTypeEnum = ADODB.CursorTypeEnum.adOpenForwardOnly, _
        Optional ByRef errInfo As Exception = Nothing, Optional newConnection As Boolean = False) As ADODB.Recordset

        Dim oRs As ADODB.Recordset

        sQuery = prepforbinaryCollation(sQuery)
Retry:

        Try

            If gConType <> "DAT" Then oLock = ADODB.CursorTypeEnum.adOpenStatic

            If (gConType = "DAT" AndAlso clsMarsData.UseExecute = True) Then
                oRs = gCon.Execute(sQuery)
            Else
                If newConnection = False Then
                    oRs = New ADODB.Recordset
                    oRs.CursorLocation = ADODB.CursorLocationEnum.adUseClient
                    oRs.Open(sQuery, gCon, oLock, ADODB.LockTypeEnum.adLockReadOnly)
                    oRs.ActiveConnection = Nothing
                Else
                    Dim tempCon As ADODB.Connection = New ADODB.Connection

                    tempCon.Open(gCon.ConnectionString)

                    oRs = New ADODB.Recordset
                    oRs.CursorLocation = ADODB.CursorLocationEnum.adUseClient

                    oRs.Open(sQuery, tempCon, oLock, ADODB.LockTypeEnum.adLockReadOnly)
                    oRs.ActiveConnection = Nothing

                    tempCon.Close()
                End If
            End If

            Return oRs

        Catch ex As System.Exception
            errInfo = ex
            If (ex.Message.ToLower.Contains("sql server does not exist") Or ex.Message.Contains("timeout") Or _
            ex.Message.ToLower.Contains("communication link failure") Or ex.Message.ToLower.Contains("network")) And RunEditor = True Then
                Dim res As DialogResult

                res = MessageBox.Show("A problem occured connecting to the SQL-RD database. Press 'RETRY' to retry or 'Cancel' to exit SQL-RD.", Application.ProductName, _
                 MessageBoxButtons.RetryCancel, MessageBoxIcon.Error)

                If res = DialogResult.Retry Then
                    Dim diagnostic As clsSystemTools = New clsSystemTools

                    If RunEditor = True Then diagnostic.RunDatabaseDiagnostic()

                    clsMarsData.DataItem.CloseMainDataConnection()
                    clsMarsData.DataItem.OpenMainDataConnection()
                    GoTo Retry
                Else
                    End
                End If
            ElseIf ex.Message.ToLower.Contains("connection failure") Then
                clsMarsData.DataItem.OpenMainDataConnection()
                GoTo retry
            End If
        End Try
    End Function

    Public Shared Function ConvertToFieldType(ByVal SQL As String, ByVal iCon As ADODB.Connection, _
    ByVal sColumn As String, ByVal sValue As Object) As Object
        Try
            Dim oField As ADODB.Field
            Dim oType As ADODB.DataTypeEnum
            Dim oRs As ADODB.Recordset
            Dim sFrom() As String

            Dim n As Integer = SQL.ToLower.IndexOf("from")

            SQL = SQL.Substring(n, SQL.Length - n)

            SQL = "SELECT * " & SQL

            oRs = New ADODB.Recordset

            oRs.Open(SQL, iCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

            Try
                sColumn = sColumn.Split(".")(1)
            Catch : End Try

            oField = oRs(sColumn)

            oType = oField.Type

            oRs.Close()

            Select Case oType
                Case ADODB.DataTypeEnum.adBigInt, ADODB.DataTypeEnum.adDecimal, _
                ADODB.DataTypeEnum.adDouble, ADODB.DataTypeEnum.adInteger, ADODB.DataTypeEnum.adNumeric, _
                ADODB.DataTypeEnum.adSingle, ADODB.DataTypeEnum.adSmallInt, ADODB.DataTypeEnum.adTinyInt, _
                ADODB.DataTypeEnum.adVarNumeric
                    Return IsNull(sValue, 0)
                Case ADODB.DataTypeEnum.adDate, ADODB.DataTypeEnum.adDBDate, ADODB.DataTypeEnum.adDBTime, ADODB.DataTypeEnum.adDBTimeStamp
                    Return "'" & ConDateTime(IsNull(sValue, Now)) & "'"
                Case Else
                    sValue = SQLPrepare(IsNull(sValue, ""))

                    Return "'" & sValue & "'"
            End Select
        Catch ex As Exception
            Try
                Int32.Parse(sValue)

                Return sValue
            Catch
                sValue = SQLPrepare(sValue)

                Return "'" & sValue & "'"
            End Try
        End Try
    End Function

    <DebuggerStepThrough()> Public Shared Function IsScheduleBursting(ByVal nID As Integer) As Boolean
        Dim oRs As ADODB.Recordset
        Dim sTable As String
        Dim sColumn As String
        Dim oReturn As Boolean

        oRs = GetData("SELECT Bursting FROM ReportAttr WHERE ReportID =" & nID)

        If oRs Is Nothing Then
            Return False
        ElseIf oRs.EOF = True Then
            Return False
        Else
            If IsNull(oRs(0).Value) = "1" Then
                oReturn = True
            Else
                oReturn = False
            End If
        End If

        oRs.Close()
        Return oReturn

    End Function
    <DebuggerStepThrough()> Public Shared Function IsScheduleDataDriven(ByVal nID As Integer, Optional ByVal sType As String = "report") As Boolean
        Try
            Dim oRs As ADODB.Recordset
            Dim SQL As String

            If String.Compare(sType, "report", True) = 0 Then
                SQL = "SELECT IsDataDriven FROM ReportAttr WHERE ReportID =" & nID
            Else
                SQL = "SELECT IsDataDriven FROM PackageAttr WHERE PackID =" & nID
            End If

            oRs = GetData(SQL)

            If oRs Is Nothing Then Return False

            If oRs.EOF = True Then Return False

            Return Convert.ToBoolean(oRs(0).Value)
        Catch
            Return False
        End Try
    End Function
    <DebuggerStepThrough()> Public Shared Function IsScheduleDynamic(ByVal nID As Integer, ByVal sType As String) As Boolean
        Dim oRs As ADODB.Recordset
        Dim sTable As String
        Dim sColumn As String
        Dim oReturn As Boolean

        If sType.ToLower = "report" Then
            sTable = "ReportAttr"
            sColumn = "ReportID"
        ElseIf sType.ToLower = "package" Then
            sTable = "PackageAttr"
            sColumn = "PackID"
        End If

        oRs = GetData("SELECT Dynamic FROM " & sTable & " WHERE " & sColumn & " =" & nID)

        If oRs Is Nothing Then
            Return False
        ElseIf oRs.EOF = True Then
            Return False
        Else
            If IsNull(oRs(0).Value) = "1" Then
                oReturn = True
            Else
                oReturn = False
            End If
        End If

        Return oReturn

    End Function
    Public Function DisposeRecordset(ByVal oRs As ADODB.Recordset)
        On Error Resume Next

        'If oRs.State = 1 Then
        '    oRs.Close()
        'End If

        oRs = Nothing
    End Function

    Public Function InsertData(ByVal sTable As String, ByVal sCols As String, ByVal sVals As String, _
    Optional ByVal ShowError As Boolean = True) As Boolean
        Dim SQL As String

        SQL = "INSERT INTO " & sTable & " (" & sCols & ") VALUES (" & sVals & ")"

        Return WriteData(SQL, ShowError)
    End Function
    '<DebuggerStepThrough()> _
    Public Shared Function WriteData(ByVal sQuery As String, _
    Optional ByVal ShowError As Boolean = True, Optional ByVal Persist As Boolean = False, Optional ByRef errInfo As Exception = Nothing) As Boolean
        Dim oRs As ADODB.Recordset = New ADODB.Recordset
        'Dim oCon As ADODB.Connection = New ADODB.Connection
        'oCon = New ADODB.Connection
        Dim nTry As Integer = 1

        Try
            If gConType <> "DAT" And RunEditor = True Then
                'TestConnection()
            End If

            sQuery = prepforbinaryCollation(sQuery)

Retry:
            gCon.Execute(sQuery)

            Try

                'If sQuery.ToLower.Contains("destinationattr") Then
                '    Dim shotcaller As String
                '    Dim StackTrace As StackTrace = New StackTrace
                '    Dim StackFrame As StackFrame = StackTrace.GetFrame(1)
                '    Dim MethodBase As MethodBase = StackFrame.GetMethod

                '    shotcaller = MethodBase.Name

                '    clsMarsDebug.writeToDebug("destinationtrace.log", gUser & " (" & shotcaller & ") - " & " " & sQuery, True)
                'End If

            Catch : End Try
            Return True
        Catch ex As System.Exception
            errInfo = ex
            Dim suggest As String = ""

            If ex.Message.ToLower.IndexOf("the log file for database") > -1 Then
                suggest = "The above error is being reported by the SQL Server  because the SQL Server transaction log is full." & _
                "You will need to ask your database administrator to perform some maintenance routines on the SQL Server to truncate the transaction log." & _
                "This is a fairly routine error in SQL Server so he/she should know what to do.  You will see more information on this error at" & _
                "http://www.google.com/search?sourceid=navclient&ie=UTF-8&rls=GGLG,GGLG:2005-20,GGLG:en&q=free+log+space+in+sql+server." & _
                "He/She will also need to write some automated routines in SQL server to regularly clear down the log file so that this " & _
                "problem doesn't happen again."
            ElseIf (ex.Message.ToLower.Contains("sql server does not exist") Or ex.Message.Contains("timeout") Or _
            ex.Message.ToLower.Contains("communication link failure") Or ex.Message.ToLower.Contains("network")) And RunEditor = True Then
                Dim res As DialogResult

                res = MessageBox.Show("A problem occured connecting to the SQL-RD database. Press 'RETRY' to retry or 'Cancel' to exit SQL-RD.", Application.ProductName, _
                 MessageBoxButtons.RetryCancel, MessageBoxIcon.Error)

                If res = DialogResult.Retry Then
                    clsMarsData.DataItem.CloseMainDataConnection()
                    clsMarsData.DataItem.OpenMainDataConnection()
                    GoTo Retry
                Else
                    End
                End If
            End If

            If Persist = False Then
                If ShowError = True Then
                    _ErrorHandle(ex.Message & " (" & sQuery & ")", Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), suggest)
                End If
                Return False
            Else
                _Delay(3)

                nTry += 1

                If nTry < 60 Then
                    GoTo Retry
                Else
                    If ShowError = True Then
                        _ErrorHandle(ex.Message & " (" & sQuery & ")", Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), suggest)
                    End If
                    Return False
                End If
            End If
        End Try
    End Function

    Public Sub CreateDB()
        Dim sType As String
        Dim oUI As New clsMarsUI
        Dim sCon As String
        Dim logFile As String = "connectiondebug.log"

        clsMarsDebug.writeToDebug(logFile, "...................Start.......................")
        sType = oUI.ReadRegistry("ConType", "DAT")

        clsMarsDebug.writeToDebug(logFile, String.Format("Connection Type: {0}", sType))

        sCon = oUI.ReadRegistry("ConString", "", True, , True)

        If sType = "DAT" Then
            If System.IO.File.Exists(Application.ExecutablePath.ToLower.Replace(assemblyName, "sqlrdlive.dat")) = False Then
                System.IO.File.Copy(Application.ExecutablePath.ToLower.Replace(assemblyName, "sqlrd.dat"), _
                Application.ExecutablePath.ToLower.Replace(assemblyName, "sqlrdlive.dat"))

                IO.File.Delete(Application.StartupPath & "\sqlrdbuild.dll")
            End If

        ElseIf sType = "LOCAL" Then
            Dim oTest As New ADODB.Connection
            Dim retryDone As Boolean = False
RETRY:
            clsMarsDebug.writeToDebug(logFile, String.Format("Encrypted Connection String: {0}", _EncryptDBValue(sCon)))

            If sCon = "" Then
                clsMarsDebug.writeToDebug(logFile, String.Format("Connection found to be blank"))

                Dim sqlCon As frmLocalSQLCon = New frmLocalSQLCon

                If RunEditor = True Then
                    If sqlCon.editSQLInfo() = True Then
                        Process.Start(sAppPath & "sqlrd.exe")
                        End
                    Else
                        End
                    End If
                End If

            End If
            clsMarsDebug.writeToDebug(logFile, String.Format("Testing the connection to the database"))

            Try
                oTest.Open(sCon)
                oTest.Close()
                oTest = Nothing

                clsMarsDebug.writeToDebug(logFile, String.Format("Test connection succeeded"))
            Catch ex As Exception
                clsMarsDebug.writeToDebug(logFile, String.Format("Error testing connection: {0}", ex.ToString))

                If RunEditor = False Then
                    '//we are going to test connecting 5 times before giving up


                    Dim count As Integer = 1
                    Dim connected As Boolean = False

                    Do
                        clsMarsDebug.writeToDebug(logFile, String.Format("Retrying connecting to the database #{0}", count))

                        Try
                            Dim testCon As ADODB.Connection = New ADODB.Connection
                            testCon.Open(sCon)
                            connected = True
                            testCon.Close()
                            testCon = Nothing
                            clsMarsDebug.writeToDebug(logFile, String.Format("Connection test succeeded"))
                        Catch
                            System.Threading.Thread.Sleep(1500)
                            count += 1
                        End Try
                    Loop Until (count > 5 Or connected = True)

                    If connected = False Then
                        clsMarsDebug.writeToDebug(logFile, String.Format("Connecting to database failed after 5 attempts "))
                        _ErrorHandle("SQL-RD failed to connect to the SQL-RD database and will exit. Please check that the SQL-RD SQL Server is running." & ControlChars.CrLf & ex.Message, Err.Number, _
                        Reflection.MethodBase.GetCurrentMethod.Name, Erl, "", , True, 1)
                        End
                    End If
                Else

                    If retryDone = False Then
                        retryDone = True

                        Dim response As DialogResult = MessageBox.Show("SQL-RD could not connect to the database. Click OK to get SQL-RD to try and restart the database service or click Cancel to exit", Application.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Error)

                        If response = DialogResult.OK Then
                            'check that the service exists
                            Dim svcs As ServiceProcess.ServiceController() = ServiceProcess.ServiceController.GetServices()
                            Dim svcExists As Boolean = False

                            For Each svc As ServiceProcess.ServiceController In svcs
                                If String.Compare(svc.DisplayName, clsMigration.m_DatabaseService, True) = 0 Then
                                    svcExists = True
                                    Exit For
                                End If
                            Next

                            '//if the service doesnt exist then install it and migrate
                            If svcExists = False Then
                                response = MessageBox.Show("The SQL-RD database service is missing. Would you like SQL-RD to install the service now?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

                                If response = DialogResult.Yes Then
                                    Dim mig As clsMigration = New clsMigration

                                    mig.MigrateSystemDBtoLocal()

                                    GoTo RETRY
                                Else
                                    End
                                End If
                            Else
                                '//the service exists then we should just restart it
                                Dim svc As ServiceProcess.ServiceController = New ServiceProcess.ServiceController(clsMigration.m_DatabaseService)

                                Try
                                    If svc.Status <> ServiceProcess.ServiceControllerStatus.Running Then
                                        svc.Start()
                                    End If

                                    System.Threading.Thread.Sleep(10000)

                                    GoTo RETRY
                                Catch exc As Exception
                                    MessageBox.Show("SQL-RD could not start the database service due to the following error: " & ControlChars.CrLf & exc.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                                    End
                                End Try
                            End If
                        Else
                            End
                        End If
                    Else
                        MessageBox.Show("SQL-RD failed to connect to the database server due to the error: " & ControlChars.CrLf & ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End
                    End If
                End If
            End Try
        End If

        Try
            If IO.File.Exists(Application.StartupPath & "\eventlog.dat") = False Then
                If IO.File.Exists(Application.StartupPath & "\eventlogtemp.dat") = True Then
                    IO.File.Copy(Application.StartupPath & "\eventlogtemp.dat", Application.StartupPath & "\eventlog.dat")
                End If
            End If
        Catch : End Try
    End Sub

    Public Sub ClearEmailLog()
        Try
            Dim SQL As String
            Dim nInt As Integer = clsMarsUI.MainUI.ReadRegistry("KeepLogs", 14)
            Dim sUnit As String
            Dim comms As New ArrayList

            If gConType = "DAT" Then
                sUnit = "'d'"
            Else
                sUnit = "d"
            End If

            SQL = "SELECT * FROM EmailLog WHERE " & _
                        "DATEDIFF(" & sUnit & ", DateSent, [CurrentDate]) > " & nInt

            If gConType = "DAT" Then
                SQL = SQL.Replace("[CurrentDate]", "Now()")
            Else
                SQL = SQL.Replace("[CurrentDate]", "GetDate()")
            End If

            Dim oRs As ADODB.Recordset = GetData(SQL)

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    Dim filePath As String = IsNull(oRs("MessageFile").Value, "")
                    Dim nID As Integer = oRs("logid").Value

                    If filePath <> "" Then
                        Try
                            IO.File.Delete(filePath)
                        Catch : End Try
                    End If

                    comms.Add("DELETE FROM EmailLog WHERE LogID =" & nID)

                    oRs.MoveNext()
                Loop

                oRs.Close()

                For Each s As String In comms
                    WriteData(s)
                Next

                Dim dir As String = clsMarsUI.MainUI.ReadRegistry("SentMessagesFolder", sAppPath & "Sent Messages")

                If IO.Directory.Exists(dir) Then
                    For Each s As String In IO.Directory.GetFiles(dir)
                        Dim dateCreated As Date = IO.File.GetCreationTime(s)

                        If Date.Now.Subtract(dateCreated).TotalDays > nInt Then
                            Try
                                IO.File.Delete(s)
                            Catch : End Try
                        End If
                    Next
                End If
            End If
        Catch : End Try

    End Sub
    Public Sub ClearHistory()

        Try
            Dim nInt As Integer
            Dim SQL As String
            Dim sUnit As String
            Dim oUI As New clsMarsUI

            nInt = oUI.ReadRegistry("KeepSingle", 14)


            If gConType = "DAT" Then
                sUnit = "'d'"
            Else
                sUnit = "d"
            End If

            'for singles
            SQL = "DELETE FROM ScheduleHistory WHERE PackID = 0 AND AutoID = 0 AND " & _
            "DATEDIFF(" & sUnit & ", EntryDate, [CurrentDate]) > " & nInt

            If gConType = "DAT" Then
                SQL = SQL.Replace("[CurrentDate]", "Now()")
            Else
                SQL = SQL.Replace("[CurrentDate]", "GetDate()")
            End If

            Me.WriteData(SQL, False)

            nInt = oUI.ReadRegistry("KeepPackage", 14)

            'for packages
            SQL = "DELETE FROM ScheduleHistory WHERE PackID > 0 AND " & _
            "DATEDIFF(" & sUnit & ", EntryDate, [CurrentDate]) > " & nInt

            If gConType = "DAT" Then
                SQL = SQL.Replace("[CurrentDate]", "Now()")
            Else
                SQL = SQL.Replace("[CurrentDate]", "GetDate()")
            End If

            Me.WriteData(SQL, False)

            'for automation schedules
            nInt = oUI.ReadRegistry("KeepAuto", 14)

            SQL = "DELETE FROM ScheduleHistory WHERE AutoID > 0 AND " & _
                    "DATEDIFF(" & sUnit & ", EntryDate, [CurrentDate]) > " & nInt

            If gConType = "DAT" Then
                SQL = SQL.Replace("[CurrentDate]", "Now()")
            Else
                SQL = SQL.Replace("[CurrentDate]", "GetDate()")
            End If

            Me.WriteData(SQL, False)

            'for email logs
            'nInt = oUI.ReadRegistry("KeepLogs", 14)

            'SQL = "DELETE FROM EmailLog WHERE " & _
            '            "DATEDIFF(" & sUnit & ", DateSent, [CurrentDate]) > " & nInt

            'If gConType = "DAT" Then
            '    SQL = SQL.Replace("[CurrentDate]", "Now()")
            'Else
            '    SQL = SQL.Replace("[CurrentDate]", "GetDate()")
            'End If

            'Me.WriteData(SQL, False)

            ClearEmailLog()

            'for event history
            nInt = oUI.ReadRegistry("KeepEvent", 14)

            SQL = "DELETE FROM EventHistory WHERE " & _
                        "DATEDIFF(" & sUnit & ", LastFired, [CurrentDate]) > " & nInt

            If gConType = "DAT" Then
                SQL = SQL.Replace("[CurrentDate]", "Now()")
            Else
                SQL = SQL.Replace("[CurrentDate]", "GetDate()")
            End If

            Me.WriteData(SQL, False)

            'for event based packages
            nInt = oUI.ReadRegistry("KeepEP", 14)

            SQL = "DELETE FROM ScheduleHistory WHERE EventPackID > 0 AND " & _
                    "DATEDIFF(" & sUnit & ", EntryDate, [CurrentDate]) > " & nInt

            If gConType = "DAT" Then
                SQL = SQL.Replace("[CurrentDate]", "Now()")
            Else
                SQL = SQL.Replace("[CurrentDate]", "GetDate()")
            End If

            Me.WriteData(SQL, False)

            clsMarsData.WriteData("DELETE FROM HistoryDetailAttr WHERE HistoryID NOT IN (SELECT HistoryID FROM ScheduleHistory) AND HistoryID NOT IN (SELECT HistoryID FROM EventHistory)", False)

        Catch : End Try
    End Sub



    Public Sub CleanDB(Optional ByVal reportID As Integer = 99999, Optional ByVal packID As Integer = 99999, _
    Optional ByVal scheduleID As Integer = 99999, Optional ByVal destinationID As Integer = 99999, _
    Optional ByVal eventID As Integer = 99999, Optional ByVal foreignID As Integer = 99999, Optional ByVal conditionID As Integer = 99999, _
    Optional ByVal oLoader As frmLoader = Nothing)
        Try

            Dim sItem

            With Me
                If oLoader IsNot Nothing Then oLoader.SetMsg("Cleaning  up  system  database...10%")
                .WriteData("DELETE FROM ReportAttr WHERE PackID = " & packID, False)
                .WriteData("DELETE FROM PGPAttr WHERE DestinationID IN (SELECT DestinationID FROM DestinationAttr WHERE ReportID =" & reportID & ")", False)
                .WriteData("DELETE FROM PGPAttr WHERE DestinationID IN (SELECT DestinationID FROM DestinationAttr WHERE PackID = " & packID & ")", False)
                .WriteData("DELETE FROM ReportParameter WHERE ReportID = " & reportID, False)
                .WriteData("DELETE FROM Tasks WHERE ScheduleID = " & scheduleID, False)

                If oLoader IsNot Nothing Then oLoader.SetMsg("Cleaning  up  system  database...20%")
                .WriteData("DELETE FROM DestinationAttr WHERE ReportID = " & reportID, False)
                .WriteData("DELETE FROM DestinationAttr WHERE PackID = " & packID, False)
                .WriteData("DELETE FROM PrinterAttr WHERE DestinationID = " & destinationID, False)
                .WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID = " & scheduleID, False)
                .WriteData("DELETE FROM DynamicAttr WHERE ReportID = " & reportID & " AND PackID = " & packID, False)
                .WriteData("DELETE FROM DynamicLink WHERE ReportID = " & reportID & " AND PackID = " & packID, False)

                If oLoader IsNot Nothing Then oLoader.SetMsg("Cleaning  up  system  database...30%")
                .WriteData("UPDATE ReportAttr SET PackID = 0 WHERE PackID IS NULL", False)
                .WriteData("UPDATE ReportAttr SET Dynamic = 0 WHERE Dynamic IS NULL", False)
                .WriteData("DELETE FROM PackagedReportAttr WHERE PackID = " & packID, False)
                .WriteData("DELETE FROM ReportOptions WHERE DestinationID = " & destinationID & " AND ReportID = " & reportID, False)

                If oLoader IsNot Nothing Then oLoader.SetMsg("Cleaning  up  system  database...40%")
                .WriteData("DELETE FROM SubReportParameters WHERE ReportID = " & reportID, False)
                .WriteData("DELETE FROM SubReportParameters WHERE ReportID = " & reportID, False)
                .WriteData("DELETE FROM PackageOptions WHERE PackID = " & packID, False)
                .WriteData("DELETE FROM BurstingAttr WHERE DestinationID = " & destinationID, False)

                If oLoader IsNot Nothing Then oLoader.SetMsg("Cleaning  up  system  database...50%")
                .WriteData("DELETE FROM BurstAttr WHERE ReportID = " & reportID, False)
                .WriteData("DELETE FROM ReportTable WHERE ReportID = " & reportID, False)
                .WriteData("DELETE FROM SubReportTable WHERE ReportID = " & reportID, False)
                .WriteData("DELETE FROM EventSubAttr WHERE EventID = " & eventID)

                If oLoader IsNot Nothing Then oLoader.SetMsg("Cleaning  up  system  database...70%")
                .WriteData("DELETE FROM ReadReceiptsAttr WHERE DestinationID = " & destinationID, False)
                .WriteData("DELETE FROM EventConditions WHERE EventID = " & eventID, False)
                .WriteData("DELETE FROM ReadReceiptsLog WHERE DestinationID NOT IN (SELECT DestinationID FROM DestinationAttr)", False)

                If oLoader IsNot Nothing Then oLoader.SetMsg("Cleaning  up  system  database...90%")
                .WriteData("DELETE FROM ReadReceiptsAttr WHERE DestinationID NOT IN (SELECT DestinationID FROM DestinationAttr)", False)
                .WriteData("DELETE FROM StampAttr WHERE ForeignID = " & foreignID, False)
                .WriteData("DELETE FROM ForwardMailAttr WHERE ConditionID = " & conditionID, False)
                .WriteData("DELETE FROM DataDrivenAttr WHERE ReportID = " & reportID, False)
                .WriteData("DELETE FROM reportdatasource WHERE ReportID = " & reportID, False)

                sqlrd.ChristianStevenMessaging.messageQ.cleanOldMessagesAsync()


                If oLoader IsNot Nothing Then oLoader.SetMsg("Cleaning  up  system  database...100%")
                '.CleanThreadManager()

                Dim s As String

                s = Environment.GetEnvironmentVariable("TEMP")

                IO.File.Delete(s & "\dotnetfx.exe")
                IO.File.Delete(s & "\installer.exe")


                If IO.Directory.Exists("c:\sqlrd_setup\") Then
                    For Each f As String In IO.Directory.GetFiles("c:\sqlrd_setup\")
                        IO.File.Delete(f)
                    Next

                    IO.Directory.Delete("c:\sqlrd_setup\")
                End If

                clsMarsDebug.manageLogs()
            End With
        Catch : End Try

    End Sub
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Public Sub GetDsns(ByVal oCombo As ComboBox)
        Try
            Dim oReg As RegistryKey
            Dim sVal As String

            If OSBit() = OSBitType.Bit32 Then
                oReg = Registry.LocalMachine.OpenSubKey("Software\ODBC\ODBC.INI\ODBC Data Sources")
            Else
                oReg = Registry.LocalMachine.OpenSubKey("Software\Wow6432Node\ODBC\ODBC.INI\ODBC Data Sources")
            End If

            oCombo.Items.Clear()

            oCombo.Items.Add("<New>")

            For Each sVal In oReg.GetValueNames
                oCombo.Items.Add(sVal)
            Next
        Catch : End Try
    End Sub

    Public Sub GetStoredProcedures(ByVal oCombo As ComboBox, ByVal sDSN As String, _
    Optional ByVal sUserID As String = "", Optional ByVal sPassword As String = "")
        Try
            Dim db As New ADOX.Catalog
            Dim Conn As New ADODB.Connection
            Dim str As String
            Dim cKey As Integer
            Dim tKey As Integer
            Dim sName As String

            oCombo.Items.Clear()
            Conn.ConnectionTimeout = 0
            Conn.CommandTimeout = 0
            Conn.Open(sDSN, sUserID, sPassword)

            db.ActiveConnection = Conn

            For I As Int32 = 0 To db.Procedures.Count - 1
                sName = db.Procedures(I).Name

                If sName.IndexOf(";") > 0 Then
                    sName = sName.Split(";")(0)
                End If

                oCombo.Items.Add(sName)
            Next

            Return
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub
    Public Overloads Sub GetTables(ByVal oCombo As ComboBox, ByVal sCon As String, Optional ByVal IncludeViews As Boolean = True)
        Try
            Dim tb As New ADOX.Table
            Dim cl As New ADOX.Column
            Dim db As New ADOX.Catalog
            Dim Conn As New ADODB.Connection
            Dim str As String
            Dim cKey As Integer
            Dim tKey As Integer
            Dim tName As String
            Dim sDatabase As String
            Dim ShowSystem As Boolean = False

            oCombo.Items.Clear()

            With Conn
                .Open(sCon)

                Try
                    sDatabase = .ConnectionString
                Catch ex As Exception
                    sDatabase = String.Empty
                End Try
            End With

            If sDatabase.ToLower.IndexOf("excel") > -1 Then
                ShowSystem = True
            End If

            db.ActiveConnection = Conn

            For Each tb In db.Tables
                If ShowSystem = True Then
                    If IncludeViews = True Then
                        If tb.Type = "TABLE" Or tb.Type = "SYSTEM TABLE" Or tb.Type = "VIEW" Then
                            If InStr(1, tb.Name, " ") > 0 Or InStr(1, tb.Name, "$") > 0 Or InStr(1, tb.Name, "-") > 0 Then
                                tName = "[" & tb.Name & "]"
                            Else
                                tName = tb.Name
                            End If

                            oCombo.Items.Add(tName)
                        End If
                    Else
                        If tb.Type = "TABLE" Or tb.Type = "SYSTEM TABLE" Then
                            If InStr(1, tb.Name, " ") > 0 Or InStr(1, tb.Name, "$") > 0 Or InStr(1, tb.Name, "-") > 0 Then
                                tName = "[" & tb.Name & "]"
                            Else
                                tName = tb.Name
                            End If

                            oCombo.Items.Add(tName)
                        End If
                    End If
                Else
                    If IncludeViews = True Then
                        If tb.Type = "TABLE" Or tb.Type = "VIEW" Then
                            If InStr(1, tb.Name, " ") > 0 Or InStr(1, tb.Name, "$") > 0 Or InStr(1, tb.Name, "-") > 0 Then
                                tName = "[" & tb.Name & "]"
                            Else
                                tName = tb.Name
                            End If

                            oCombo.Items.Add(tName)
                        End If
                    Else
                        If tb.Type = "TABLE" Then
                            If InStr(1, tb.Name, " ") > 0 Or InStr(1, tb.Name, "$") > 0 Or InStr(1, tb.Name, "-") > 0 Then
                                tName = "[" & tb.Name & "]"
                            Else
                                tName = tb.Name
                            End If

                            oCombo.Items.Add(tName)
                        End If
                    End If
                End If

            Next

            tb = Nothing
            cl = Nothing
            db = Nothing
            Conn = Nothing
            Exit Sub
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try

    End Sub
    Public Overloads Sub GetTables(ByVal oCombo As ComboBox, ByVal sDSN As String, _
    ByVal sUserID As String, ByVal sPassword As String)
        Try
            Dim tb As New ADOX.Table
            Dim cl As New ADOX.Column
            Dim db As New ADOX.Catalog
            Dim Conn As New ADODB.Connection
            Dim str As String
            Dim cKey As Integer
            Dim tKey As Integer
            Dim tName As String
            Dim sDatabase As String
            Dim ShowSystem As Boolean = False

            oCombo.Items.Clear()

            With Conn
                .Open(sDSN, sUserID, sPassword)

                Try
                    sDatabase = .ConnectionString
                Catch ex As Exception
                    sDatabase = String.Empty
                End Try
            End With

            If sDatabase.ToLower.IndexOf("excel") > -1 Then
                ShowSystem = True
            End If

            db.ActiveConnection = Conn

            For Each tb In db.Tables
                If ShowSystem = True Then
                    If tb.Type = "TABLE" Or tb.Type = "SYSTEM TABLE" Or tb.Type = "VIEW" Then
                        If InStr(1, tb.Name, " ") > 0 Or InStr(1, tb.Name, "$") > 0 Or InStr(1, tb.Name, "-") > 0 Then
                            tName = "[" & tb.Name & "]"
                        Else
                            tName = tb.Name
                        End If

                        oCombo.Items.Add(tName)
                    End If
                Else
                    If tb.Type = "TABLE" Or tb.Type = "VIEW" Then
                        If InStr(1, tb.Name, " ") > 0 Or InStr(1, tb.Name, "$") > 0 Or InStr(1, tb.Name, "-") > 0 Then
                            tName = "[" & tb.Name & "]"
                        Else
                            tName = tb.Name
                        End If

                        oCombo.Items.Add(tName)
                    End If
                End If

            Next

            tb = Nothing
            cl = Nothing
            db = Nothing
            Conn = Nothing
            Exit Sub
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try

    End Sub
    Public Overloads Function GetColumns(ByVal sTable As String) As String
        Try
            Dim tb As New ADOX.Table
            Dim cl As New ADOX.Column
            Dim db As New ADOX.Catalog
            Dim str As String
            Dim cKey As Integer
            Dim tKey As Integer
            Dim tName As String
            Dim cName As String
            Dim AlreadyIn As Boolean
            Dim output As String = ""

            db.ActiveConnection = gCon

            tb = db.Tables(sTable)

            For Each cl In tb.Columns
                cName = "[" & cl.Name & "]"

                output &= cName & ","
            Next

            tb = Nothing
            cl = Nothing
            db = Nothing

            output = output.Substring(0, (output.Length - 1))

            Return output
        Catch ex As Exception
            Return ""
        End Try
    End Function
    Public Overloads Function GetColumns(ByVal sCon As String, ByVal sTable As String) As DataTable
        Try
            Dim tb As New ADOX.Table
            Dim cl As New ADOX.Column
            Dim db As New ADOX.Catalog
            Dim Conn As New ADODB.Connection
            Dim str As String
            Dim cKey As Integer
            Dim tKey As Integer
            Dim tName As String
            Dim cName As String
            Dim AlreadyIn As Boolean
            Dim result As New DataTable("TableDef")

            result.Columns.Add("Name", System.Type.GetType("System.String"))
            result.Columns.Add("Type", System.Type.GetType("System.Int32"))
            result.Columns.Add("Length", System.Type.GetType("System.Int32"))


            With Conn
                .Open(sCon)
            End With

            db.ActiveConnection = Conn

            For Each tb In db.Tables
                AlreadyIn = False

                If InStr(1, tb.Name, " ") > 0 Or InStr(1, tb.Name, "$") > 0 Or InStr(1, tb.Name, "-") > 0 Then
                    tName = "[" & tb.Name & "]"
                Else
                    tName = tb.Name
                End If

                If tName = sTable Then
                    For Each cl In tb.Columns
                        If InStr(1, cl.Name, " ") > 0 Or InStr(1, cl.Name, "$") > 0 Or InStr(1, cl.Name, "-") > 0 Then
                            cName = "[" & cl.Name & "]"
                        Else
                            cName = cl.Name
                        End If

                        Dim s(2) As String

                        s(0) = cl.Name
                        s(1) = cl.Type
                        s(2) = cl.DefinedSize

                        result.Rows.Add(s)
                    Next
                End If
            Next

            tb = Nothing
            cl = Nothing
            db = Nothing
            Conn = Nothing

            Return result
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))

            Return Nothing
        End Try

    End Function
    Public Overloads Function GetColumnsFromTable(ByVal sTable As String, sDsn As String, username As String, password As String) As ArrayList
        Try
            Dim tb As New ADOX.Table
            Dim cl As New ADOX.Column
            Dim db As New ADOX.Catalog
            Dim Conn As New ADODB.Connection
            Dim str As String
            Dim cKey As Integer
            Dim tKey As Integer
            Dim tName As String
            Dim cName As String
            Dim AlreadyIn As Boolean
            Dim result As ArrayList = New ArrayList


            With Conn
                .Open(sDsn, username, password)
            End With

            db.ActiveConnection = Conn

            For Each tb In db.Tables
                AlreadyIn = False

                If InStr(1, tb.Name, " ") > 0 Or InStr(1, tb.Name, "$") > 0 Or InStr(1, tb.Name, "-") > 0 Then
                    tName = "[" & tb.Name & "]"
                Else
                    tName = tb.Name
                End If

                If tName = sTable Then
                    For Each cl In tb.Columns
                        If InStr(1, cl.Name, " ") > 0 Or InStr(1, cl.Name, "$") > 0 Or InStr(1, cl.Name, "-") > 0 Then
                            cName = "[" & cl.Name & "]"
                        Else
                            cName = cl.Name
                        End If

                        result.Add(cName)
                    Next
                End If
            Next

            Conn = Nothing

            Return result
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))

            Return Nothing
        End Try

    End Function
    Public Overloads Sub GetColumns(ByVal oCombo As ComboBox, ByVal sDSN As String, _
    ByVal sTable As String, _
    Optional ByVal sUserID As String = "", Optional ByVal sPassword As String = "")
        Try
            Dim tb As New ADOX.Table
            Dim cl As New ADOX.Column
            Dim db As New ADOX.Catalog
            Dim Conn As New ADODB.Connection
            Dim str As String
            Dim cKey As Integer
            Dim tKey As Integer
            Dim tName As String
            Dim cName As String
            Dim AlreadyIn As Boolean

            oCombo.Items.Clear()

            With Conn
                .Open(sDSN, sUserID, sPassword)
            End With


            db.ActiveConnection = Conn

            For Each tb In db.Tables
                AlreadyIn = False

                If InStr(1, tb.Name, " ") > 0 Or InStr(1, tb.Name, "$") > 0 Or InStr(1, tb.Name, "-") > 0 Then
                    tName = "[" & tb.Name & "]"
                Else
                    tName = tb.Name
                End If

                If tName = sTable Then
                    For Each cl In tb.Columns
                        If InStr(1, cl.Name, " ") > 0 Or InStr(1, cl.Name, "$") > 0 Or InStr(1, cl.Name, "-") > 0 Then
                            cName = "[" & cl.Name & "]"
                        Else
                            cName = cl.Name
                        End If

                        Dim obj As ComboBox.ObjectCollection

                        Try
                            obj = oCombo.Items

                            For i As Integer = 0 To obj.Count - 1

                                ''console.writeLine(obj.Item(i).Name)

                                If obj.Item(i).Name = cName Then
                                    AlreadyIn = True
                                    Exit For
                                End If
                            Next
                        Catch ex As Exception
                            ''console.writeLine(ex.Message)
                        End Try

                        If AlreadyIn = False Then oCombo.Items.Add(New clsMyList(cName, cl.Type))
                    Next
                End If
            Next
            tb = Nothing
            cl = Nothing
            db = Nothing
            Conn = Nothing
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try

    End Sub

    Public Sub ReturnDistinctValues(ByVal oCombo As ComboBox, ByVal sDSN As String, ByVal sUserID As String, _
    ByVal sPassword As String, ByVal sCol As String, ByVal sTable As String)
        On Error GoTo Hell

        Dim SQL As String
        Dim oCon As ADODB.Connection = New ADODB.Connection
        Dim oRs As ADODB.Recordset = New ADODB.Recordset

        SQL = "SELECT DISTINCT TOP 100  " & sCol & " FROM " & sTable
        oCon.ConnectionTimeout = 0
        oCon.CommandTimeout = 0
        oCon.Open(sDSN, sUserID, sPassword)

        oRs.Open(SQL, oCon, ADODB.CursorTypeEnum.adOpenForwardOnly)

        oCombo.Items.Clear()

        Do While oRs.EOF = False
            oCombo.Items.Add(oRs.Fields(0).Value)
            oRs.MoveNext()
        Loop

        DisposeRecordset(oRs)

        Return
Hell:

    End Sub

    Public Shared Function crapCreateDataID(ByVal tableName As String, ByVal keyColumn As String) As Integer
        Try
            Dim oRs As ADODB.Recordset
            Dim SQL As String

            If tableName = "" Or keyColumn = "" Then
                System.Threading.Thread.Sleep(1000)
                Return Date.Now.Subtract(New Date(2002, 8, 29)).TotalSeconds
            End If

            SQL = "SELECT MAX(" & keyColumn & ") FROM " & tableName

            oRs = GetData(SQL)

            Dim value As Integer

            Try
                value = oRs(0).Value
            Catch
                value = 0
            End Try

            oRs.Close()

            If value = 99998 Then
                value += 2
            Else
                Return value + 1
            End If
        Catch
            Return CreateDataID(tableName, keyColumn)
        End Try
    End Function

    Public Shared Function GetMaxFromTable(ByVal tableName As String, ByVal keyColumn As String) As Int64
        Dim oRs As ADODB.Recordset = GetData("SELECT MAX(" & keyColumn & ") FROM " & tableName)
        Dim value As Integer = 0

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then
                value = oRs(0).Value + 1
            End If

            oRs.Close()
        Else
            value = 0
        End If

        Return value
    End Function

    Public Shared Function CreateDataID(Optional ByVal tableName As String = "", _
                                        Optional ByVal keyColumn As String = "", _
                                        Optional ByVal nonTable As Boolean = False, _
                                        Optional ByVal greaterThanID As Int64 = 0) As Int64

        Dim Datum As Date
        Dim CodeValue As Int64

        Try
            Datum = New Date(2000, 1, 1)

            CodeValue = Now.Subtract(Datum).TotalSeconds

            Dim tempCode As Long = CodeValue

            If tableName.Length > 0 And keyColumn.Length > 0 Then
                CodeValue = makeCodeValue(CodeValue)

                Do While (IsDuplicate(tableName, keyColumn, CodeValue, False) = True OrElse CodeValue <= greaterThanID)
                    Dim value As Int64 = GetMaxFromTable(tableName, keyColumn)

                    If value > 0 Then
                        CodeValue = value + 1
                    Else
                        CodeValue += 1
                    End If
                Loop
            End If
        Catch
            Datum = New Date(1977, 2, 27)

            _Delay(1)

            CodeValue = Date.Now.Subtract(Datum).TotalSeconds
        End Try


        Return CodeValue
    End Function
    '<DebuggerStepThrough()> _
    Public Shared Function CRAPPERCreateDataID(Optional ByVal tableName As String = "", _
    Optional ByVal keyColumn As String = "", Optional ByVal nonTable As Boolean = False) As Int64

        Dim Datum As Date
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim CodeValue As Int64

        Try
            SQL = "SELECT MAX(CodeValue) FROM CodeGen"

            oRs = GetData(SQL)

            If oRs.EOF = True Then
                Datum = New Date(1977, 2, 27)

                _Delay(1)

                CodeValue = Date.Now.Subtract(Datum).TotalSeconds

                WriteData("INSERT INTO CodeGen (CodeValue) VALUES(" & 10000 & ")", False, True)
            Else
                CodeValue = oRs(0).Value

                If CodeValue = 99999 Then
                    CodeValue = 99999 + 1
                End If

                Dim tempCode As Long = CodeValue

                If tableName.Length > 0 And keyColumn.Length > 0 Then
                    CodeValue = makeCodeValue(CodeValue)

                    Do While IsDuplicate(tableName, keyColumn, CodeValue, False) = True
                        Dim value As Int64 = GetMaxFromTable(tableName, keyColumn)

                        If value > 0 Then
                            CodeValue = value + 1
                        Else
                            CodeValue += 1
                        End If
                    Loop
                End If

                WriteData("UPDATE CodeGen SET CodeValue = " & tempCode + 1, False, True)
            End If

            oRs.Close()

            oRs = Nothing
        Catch
            Datum = New Date(1977, 2, 27)

            _Delay(1)

            CodeValue = Date.Now.Subtract(Datum).TotalSeconds
        End Try


        Return CodeValue
    End Function

    Public Shared Function makeCodeValue(ByVal codeValue As Int64) As Long
        Dim unifiedLogin As Boolean = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("UnifiedLogin", 0))
        Dim actualID As Int64 = 0
        Dim SQL As String

        If unifiedLogin = True Then
            SQL = "SELECT UserNumber FROM DomainAttr WHERE DomainName = '" & SQLPrepare(gUser) & "'"
        Else
            SQL = "SELECT UserNumber FROM CRDUsers WHERE UserID = '" & SQLPrepare(gUser) & "'"
        End If

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then
                actualID = oRs(0).Value
            End If

            oRs.Close()
        End If

        codeValue = actualID + codeValue

        Return codeValue
    End Function


    Public Shared Sub CreateXML(ByRef oRs As ADODB.Recordset, ByVal sPath As String)
        Try
            If System.IO.File.Exists(sPath) = True Then _
            System.IO.File.Delete(sPath)
        Catch
        End Try

        Try
            Dim parse As New clsMarsParser

            parse.ParseDirectory(GetDirectory(sPath))
            oRs.Save(sPath, ADODB.PersistFormatEnum.adPersistXML)
            oRs.Close()

            Exit Sub
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Public Shared Function GetXML(ByVal sPath As String) As ADODB.Recordset
        Try
            Dim oRs As ADODB.Recordset
            oRs = New ADODB.Recordset
            oRs.CursorLocation = ADODB.CursorLocationEnum.adUseClient
            oRs.CursorType = ADODB.CursorTypeEnum.adOpenStatic
            oRs.Open(sPath, , , ADODB.LockTypeEnum.adLockOptimistic)

            Return oRs

            Exit Function
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            Return Nothing
        End Try
    End Function

    Public Sub OpenMainDataConnection(Optional ByVal conString As String = "")
        Dim retry As Integer = 0
        Dim temp As String

        If conString <> "" Then
            temp = conString
        Else
            temp = sCon
        End If
RETRY:
        Try
            gCon = New ADODB.Connection
            gCon.Mode = ADODB.ConnectModeEnum.adModeShareDenyNone

            Try
                gCon.ConnectionTimeout = 0
                gCon.CommandTimeout = 0
                gCon.Open(temp)
            Catch ex As Exception
                'try the alternative con string
                Dim tempCon As String = clsMarsUI.MainUI.ReadRegistry("ConString2", "")

                If tempCon = "" Then Throw ex

                Dim dsn As String = tempCon.Split("|")(0)
                Dim user As String = tempCon.Split("|")(1)
                Dim pass As String = _DecryptDBValue(tempCon.Split("|")(2))

                gCon = New ADODB.Connection
                gCon.Mode = ADODB.ConnectModeEnum.adModeShareDenyNone
                gCon.Open(dsn, user, pass)
            End Try
        Catch ex As Exception
            Dim lastSent As Date
            Dim sMsg As String = ""

            If ex.Message.ToLower.IndexOf("login failed") > -1 Or ex.Message.ToLower.IndexOf("timeout expired") > -1 Then
                sMsg = "The SQL-RD schedule database is not available and SQL-RD cannot connect to it. When it is back on line and SQL-RD can connect to it this error will go away"
            End If

            lastSent = clsMarsUI.MainUI.ReadRegistry("ConnectionErrorLastSent", Date.Now.AddHours(-1))

            If Date.Now.Subtract(lastSent).TotalMinutes > 60 Then
                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))

                clsMarsUI.MainUI.SaveRegistry("ConnectionErrorLastSent", ConDateTime(Now))
            End If

            If retry <= 3 Then
                _Delay(3)
                retry += 1
                GoTo RETRY
            End If

            If gConType = "DAT" Then
                MessageBox.Show("Error connecting to the SQL-RD database: " & ex.ToString)
                End
            Else
                If RunEditor = True Then
                    Dim oLogin As New frmODBCLogin

                    oLogin.EditLogins()
                End If
                End
            End If
        End Try
    End Sub

    Public Sub CloseMainDataConnection()
        Try
            gCon.Close()
            gCon = Nothing
        Catch ex As Exception

        End Try
    End Sub

    Public Shared Function IsDuplicate(ByVal sTable As String, _
    ByVal sColumn As String, _
    ByVal sValue As String, ByVal CheckOwner As Boolean, _
    Optional ByVal recordCheckID As Integer = 0, Optional ByVal checkIDCol As String = "", Optional ByVal CheckCount As Integer = 0, _
    Optional ByVal objectOwner As String = "") As Boolean
        Dim SQL As String
        Dim sTest As String

        Try
            Int32.Parse(sValue)

            sTest = " = " & SQLPrepare(sValue)
        Catch ex As Exception
            sTest = " LIKE '" & SQLPrepare(sValue) & "'"
        End Try

        Try
            If recordCheckID = 0 Then
                SQL = "SELECT " & sColumn & " FROM " & sTable & " WHERE " & _
                sColumn & sTest

                If CheckOwner = True Then
                    If objectOwner = "" Then objectOwner = gUser

                    SQL &= " AND Owner ='" & objectOwner & "'"
                End If

                Dim oRs As ADODB.Recordset = GetData(SQL, ADODB.CursorTypeEnum.adOpenStatic)

                If oRs.EOF = False Then
                    ' clsMarsDebug.writeToDebug("dynamicThreading_" & Process.GetCurrentProcess.Id & ".log", "Found duplicate", True)

                    If CheckCount = 0 Then
                        Return True
                    Else
                        If oRs.RecordCount > CheckCount Then
                            Return True
                        Else
                            Return False
                        End If
                    End If
                Else
                    'clsMarsDebug.writeToDebug("dynamicThreading_" & Process.GetCurrentProcess.Id & ".log", "Found no duplicate", True)
                    Return False
                End If
            Else
                SQL = "SELECT " & sColumn & " FROM " & sTable & " WHERE " & sColumn & sTest & " AND " & checkIDCol & " <> " & recordCheckID

                If CheckOwner = True Then
                    If objectOwner = "" Then objectOwner = gUser

                    SQL &= " AND Owner ='" & objectOwner & "'"
                End If

                Dim ors As ADODB.Recordset = clsMarsData.GetData(SQL, ADODB.CursorTypeEnum.adOpenStatic)

                If ors.EOF = False Then
                    If CheckCount = 0 Then
                        Return True
                    Else
                        If ors.RecordCount > CheckCount Then
                            Return True
                        Else
                            Return False
                        End If
                    End If
                Else
                    Return False
                End If
            End If
        Catch ex As Exception
            '   clsMarsDebug.writeToDebug("dynamicThreading_" & Process.GetCurrentProcess.Id & ".log", "Error determining duplicates in table", True)
            Return False
        End Try
    End Function

    Private Function NeedsDBUpdate() As Boolean
        Dim buildPath As String = sAppPath & "sqlrdbuild.dll"
        Dim build As String
        Dim result As Boolean

        If IO.File.Exists(buildPath) Then
            build = sqlrd.MarsCommon.ReadTextFromFile(buildPath)
        Else
            build = ""
        End If

        Dim currentBuild As String
        Dim assemblyInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        currentBuild = assemblyInfo.FileVersion

        If currentBuild <> build Then
            result = True

            sqlrd.MarsCommon.SaveTextToFile(currentBuild, buildPath, , False, False)
        Else
            result = False
        End If

        Return result

    End Function

    Public Shared Function GetNewUserNumber(ByVal tableName As String) As Integer
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim value As Integer = 0

        SQL = "SELECT MAX(UserNumber) FROM " & tableName

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then
                value = IsNull(oRs(0).Value, 0)
            End If

            oRs.Close()
        End If

        Return (value + 1)
    End Function
    Private Sub AddNumberToUser()
        Try
            Dim SQL As String = "SELECT * FROM CRDUsers WHERE UserNumber IS NULL OR UserNumber = ''"
            Dim oRs As ADODB.Recordset = Me.GetData(SQL)
            Dim I As Integer = 1

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    Dim UserID As String = oRs("userid").Value

                    SQL = "UPDATE CRDUsers SET UserNumber = " & I & " WHERE UserID ='" & SQLPrepare(UserID) & "'"

                    Me.WriteData(SQL)

                    I += 1

                    oRs.MoveNext()
                Loop

                oRs.Close()

                I = 1

                SQL = "SELECT * FROM DomainAttr WHERE UserNumber IS NULL OR UserNumber = ''"

                oRs = Me.GetData(SQL)

                If oRs IsNot Nothing Then
                    Do While oRs.EOF = False
                        Dim userID As String = oRs("domainName").Value

                        SQL = "UPDATE DomainAttr SET UserNumber = " & I & " WHERE DomainName ='" & SQLPrepare(userID) & "'"

                        Me.WriteData(SQL)

                        I += 1

                        oRs.MoveNext()
                    Loop
                End If

                oRs.Close()
            End If
        Catch : End Try
    End Sub

    Public Sub UpgradeCRDDbThread()
        UpgradeCRDDb()
    End Sub
    Public Sub UpgradeCRDDb(Optional ByVal sLoc As String = "", Optional ByVal sUser As String = "", Optional ByVal sPassword As String = "", _
    Optional ByVal updateProgress As Boolean = False, Optional ByVal loader As frmLoader = Nothing)


        Dim SQL() As String
        Dim sIn As String
        Dim I As Integer = 1
        Dim total As Integer = 1
        Dim fileDate As Date
        Dim lastFileDate As Date

        If sLoc.Length = 0 Then
            Try
                If File.Exists(sAppPath & "sqlrdupgrade.sql") = True Then
                    'Try
                    '    lastFileDate = clsMarsUI.MainUI.ReadRegistry("LastDBUpdate", New Date(1977, 2, 27))
                    '    fileDate = File.GetLastWriteTimeUtc(sAppPath & "sqlrdupgrade.sql")

                    '    If lastFileDate.Subtract(New Date(1977, 2, 27)).TotalDays > 0 Then 'if the date is not 1977-02-27
                    '        If Convert.ToInt32(fileDate.Subtract(lastFileDate).TotalDays) = 0 Then 'if the filedate hasnt changed
                    '            Dim updateCount As Integer = clsMarsUI.MainUI.ReadRegistry("UpdateDBCount", 0) 'check the update as we need to force it to run 5 times

                    '            If updateCount > 5 Then
                    '                Return
                    '            Else
                    '                clsMarsUI.MainUI.SaveRegistry("UpdateDBCount", updateCount + 1, False, , True)
                    '            End If
                    '        Else
                    '            clsMarsUI.MainUI.SaveRegistry("LastDBUpdate", fileDate, , , True)
                    '            clsMarsUI.MainUI.SaveRegistry("UpdateDBCount", 0)
                    '        End If
                    '    Else
                    '        clsMarsUI.MainUI.SaveRegistry("LastDBUpdate", fileDate, , , True)
                    '        clsMarsUI.MainUI.SaveRegistry("UpdateDBCount", 0)
                    '    End If
                    'Catch : End Try

                    sIn = ReadTextFromFile(sAppPath & "sqlrdupgrade.sql")

                    SQL = sIn.Split(";")

                    Try
                        total = SQL.Length
                    Catch : End Try

                    For Each s As String In SQL
                        Try
                            If updateProgress = True And loader IsNot Nothing Then
                                Dim perc As Integer = (I / total) * 100

                                loader.SetMsg("Verifying database Integrity..." & perc & "%")
                            End If
                        Catch : End Try

                        If s.Length > 0 Then
                            Try

                                WriteData(s, False)

                            Catch : End Try
                        End If
                        I += 1

                    Next
                End If

                ModifyFolders()
                AddNumberToUser()
            Catch ex As Exception
                '_ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            End Try

            'do one off upgrades
            Try
                Dim IncludeAttach As Integer
                Dim oUI As New clsMarsUI

                IncludeAttach = oUI.ReadRegistry("IAttach", 0)

                If IncludeAttach = 0 Then
                    Dim s As String

                    s = "UPDATE DestinationAttr SET IncludeAttach = 1 WHERE OutputFormat <> 'HTML (*.htm)'"

                    Try
                        WriteData(s, False)
                    Catch : End Try

                    oUI.SaveRegistry("IAttach", 1)

                End If
            Catch ex As Exception

            End Try

            clsMarsEvent.Convert5to6()
        Else
            Try
                Dim oCon As New ADODB.Connection
                oCon.ConnectionTimeout = 0
                oCon.CommandTimeout = 0
                oCon.Open(sLoc, sUser, sPassword)

                If File.Exists(sAppPath & "sqlrdupgrade.sql") = True Then
                    sIn = ReadTextFromFile(sAppPath & "sqlrdupgrade.sql")

                    SQL = sIn.Split(";")

                    For Each s As String In SQL
                        If s.Length > 0 Then
                            Try
                                oCon.Execute(s)
                            Catch : End Try
                        End If
                    Next
                End If
            Catch ex As Exception
                '_ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            End Try
        End If

        Try
            Me.changeParameterDelimiter()
        Catch : End Try
    End Sub

    Public Sub ModifyFolders()
        WriteData("DELETE FROM Folders WHERE FolderName LIKE '%{AmaniTest}%'", False)
        Return
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String

        Try
            WriteData("DELETE FROM Folders WHERE FolderName LIKE '%{AmaniTest}%'", False)
            WriteData("DROP TABLE tmpFolders", False)

            sCols = "FolderID,FolderName,Parent"

            sVals = "99999,'{AmaniTest}',0"

            SQL = "INSERT INTO Folders (" & sCols & ") VALUES (" & sVals & ")"

            WriteData(SQL, False)

            If WriteData(SQL, False) = False Then

                'create a temp folder
                SQL = "SELECT * INTO tmpFolders FROM Folders"

                If WriteData(SQL, False) = True Then

                    'delete the old table
                    SQL = "DROP TABLE Folders"

                    WriteData(SQL, False)

                    'create a new table
                    SQL = "SELECT * INTO Folders FROM tmpFolders"

                    WriteData(SQL, False)

                    SQL = "ALTER TABLE Folders ALTER COLUMN FolderID INTEGER PRIMARY KEY"

                    WriteData(SQL, False)
                End If
            End If

            WriteData("DELETE FROM Folders WHERE FolderName LIKE '%{AmaniTest}%'", False)

        Catch : End Try
    End Sub

    Public Function CopyRow(ByVal sTable As String, _
                            ByVal nOriginalID As Integer, _
                            ByVal sWhere As String, _
                            ByVal sIDCol As String, _
                            Optional ByVal sNameCol As String = "", _
                            Optional ByVal sNameValue As String = "", _
                            Optional ByVal nReportID As Integer = 0, _
                            Optional ByVal nPackID As Integer = 0, _
                            Optional ByVal nAutoID As Integer = 0, _
                            Optional ByVal nEventID As Integer = 0, _
                            Optional ByVal nScheduleID As Integer = 0, _
                            Optional ByVal nDestID As Integer = 0, _
                            Optional ByVal CachePath As String = "", _
                            Optional ByVal nParent As Integer = 0, _
                            Optional ByVal nEventPackID As Integer = 0, _
                            Optional ByVal nSmartID As Integer = 0, _
                            Optional ByVal nLastID As Integer = 0, _
                            Optional overWriteValues As Hashtable = Nothing) As Integer

        Dim oRs As ADODB.Recordset

        oRs = GetData("SELECT * FROM " & sTable & " " & sWhere)

        Dim sCols As String
        Dim sVals As String
        Dim NewID As Integer = Me.CreateDataID(sTable, sIDCol, , nLastID)

        If oRs Is Nothing Then Return 0

        For I As Integer = 0 To oRs.Fields.Count - 1
            Select Case oRs.Fields(I).Name.ToLower
                Case "collate"
                    sCols &= "[Collate],"
                Case "type"
                    sCols &= "[Type],"
                Case "to"
                    sCols &= "[To],"
                Case "subject"
                    sCols &= "[Subject],"
                Case Else
                    sCols &= oRs.Fields(I).Name & ","
            End Select
        Next

        sCols = sCols.Substring(0, sCols.Length - 1).ToLower

        sVals = sCols

        sVals = sVals.Replace(sIDCol.ToLower, NewID)

        If sNameCol.Length > 0 Then
            sVals = sVals.Replace(sNameCol.ToLower, "'" & SQLPrepare(sNameValue) & "'")
        End If

        Dim newVals As String

        For Each s As String In sVals.Split(",")
            Select Case s
                Case "reportid"
                    newVals &= nReportID & ","
                Case "packid"
                    newVals &= nPackID & ","
                Case "autoid"
                    newVals &= nAutoID & ","
                Case "eventid"
                    newVals &= nEventID & ","
                Case "scheduleid"
                    newVals &= nScheduleID & ","
                Case "destinationid", "foreignid"
                    newVals &= nDestID & ","
                Case "parent"
                    newVals &= nParent & ","
                Case "eventpackid"
                    newVals &= nEventPackID & ","
                Case "smartid"
                    newVals &= nSmartID & ","
                Case Else
                    newVals &= s & ","
            End Select
        Next

        sVals = newVals.Substring(0, newVals.Length - 1)

        If overWriteValues IsNot Nothing Then
            For Each de As DictionaryEntry In overWriteValues
                sVals = sVals.ToLower.Replace(de.Key.ToString.ToLower, de.Value)
            Next
        End If

        'If nReportID > 0 Then sVals = sVals.Replace("reportid", nReportID)

        'If nPackID > 0 Then sVals = sVals.Replace("packid", nPackID)

        'If nAutoID > 0 Then sVals = sVals.Replace("autoid", nAutoID)

        'If nEventID > 0 Then sVals = sVals.Replace("eventid", nEventID)

        'If nScheduleID > 0 Then sVals = sVals.Replace("scheduleid", nScheduleID)

        'If nDestID > 0 Then sVals = sVals.Replace("destinationid", nDestID)

        'If nParent > 0 Then sVals = sVals.Replace("parent", nParent)

        'If nEventPackID > 0 Then sVals = sVals.Replace("eventpackid", nEventPackID)

        'If CachePath.Length > 0 Then
        '    sVals = sVals.Replace("cachepath", "'" & SQLPrepare(CachePath) & "'")
        'End If

        Dim SQL As String

        SQL = "INSERT INTO " & sTable & "(" & sCols & ") " & _
        "SELECT " & sVals & " FROM " & sTable & " " & sWhere

        ''console.writeLine(SQL)

        If Me.WriteData(SQL) = True Then
            Return NewID
        Else
            Return 0
        End If

    End Function

    Public Function UpdatePackOrder()
        Try
            Dim oRs As ADODB.Recordset
            Dim SQL As String
            Dim sName As String
            Dim nOrder As Int32
            Dim nReportID As Int32
            Dim oUI As New clsMarsUI

            SQL = "SELECT ReportID, ReportTitle FROM ReportAttr WHERE PackID > 0 AND PackOrderID IS NULL"

            oRs = Me.GetData(SQL)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    nReportID = oRs(0).Value
                    sName = oRs(1).Value

                    If sName.IndexOf(":") > -1 Then
                        nOrder = sName.Split(":")(0)

                        SQL = "UPDATE ReportAttr SET PackOrderID = " & nOrder & " WHERE ReportID =" & nReportID

                        Me.WriteData(SQL, False)
                    End If

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If
        Catch : End Try
    End Function

    Public Shared Function GetColumnIndex(ByVal columnName As String, ByVal oRs As ADODB.Recordset) As Integer
        Dim I As Integer = 0
        Dim result As Integer = 0

        For Each field As ADODB.Field In oRs.Fields

            If field.Name.ToLower.IndexOf(columnName.ToLower) > -1 Then
                result = I
                Return result
            End If

            I += 1
        Next

        Return result
    End Function

    Private Function changeParameterDelimiter()
        Dim SQL As String
        Dim parID As Integer
        Dim parValue As String

        Dim changePar As String = clsMarsUI.MainUI.ReadRegistry("changeParameterDelimiter", 0)

        If changePar = 0 Then
            SQL = "SELECT ParID,ParValue FROM ReportParameter WHERE ParValue LIKE '%|%'"

            Dim oRs As ADODB.Recordset = GetData(SQL)

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    parID = oRs("parid").Value
                    parValue = oRs("parvalue").Value

                    parValue = parValue.Replace("|", PD)

                    WriteData("UPDATE ReportParameter SET parValue = '" & SQLPrepare(parValue) & "' WHERE ParID =" & parID)

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            SQL = "SELECT SubParID,ParValue FROM SubReportParameters WHERE ParValue LIKE '%|%'"

            oRs = GetData(SQL)

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    parID = oRs("subparid").Value
                    parValue = oRs("parvalue").Value

                    parValue = parValue.Replace("|", PD)

                    WriteData("UPDATE SubReportParameters SET parValue = '" & SQLPrepare(parValue) & "' WHERE SubParID =" & parID)

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            clsMarsUI.MainUI.SaveRegistry("changeParameterDelimiter", 1)
        End If
    End Function

End Class
