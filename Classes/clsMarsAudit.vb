Imports Microsoft.Win32

Friend Class clsMarsAudit
    Shared oUI As New clsMarsUI
    Shared oSec As New clsMarsSecurity
    Shared sDSN As String
    Shared sUser As String
    Shared sPassword As String

    Public Enum ScheduleType
        SINGLES = 1
        PACKAGE = 2
        AUTOMATION = 3
        EVENTS = 4
        BURST = 5
        DYNAMIC = 6
        PACKED = 7
        FOLDER = 8
        EVENTPACKAGE = 9
    End Enum

    Public Enum AuditAction
        CREATE = 1
        DELETE = 2
        EDIT = 3
        REFRESH = 4
        RENAME = 5
        ENABLE = 6
        DISABLE = 7
        EXECUTE = 8
    End Enum

    Private Shared Sub _SetAuditInfo()
        sDSN = oUI.ReadRegistry("AuditDSN", "")
        sUser = oUI.ReadRegistry("AuditUserName", "")
        sPassword = oUI.ReadRegistry("AuditPassword", "", True)
    End Sub
    Public Shared Sub _LogAudit(ByVal ScheduleName As String, ByVal oScheduleType As clsMarsAudit.ScheduleType, ByVal Action As clsMarsAudit.AuditAction)

        Try
            If oUI.ReadRegistry("AuditTrail", 0) = 0 Then
                Return
            End If

            _SetAuditInfo()

            Dim SQL As String
            Dim sCols As String
            Dim sVals As String
            Dim oCon As New ADODB.Connection

            sCols = "AuditID,EntryDate,UserID,ScheduleType,ScheduleName,AuditAction"

            sVals = clsMarsData.CreateDataID("crdaudittrail", "auditid") & "," & _
            "'" & ConDateTime(Now) & "'," & _
            "'" & gUser & "'," & _
            "'" & oScheduleType.ToString & "'," & _
            "'" & SQLPrepare(ScheduleName) & "'," & _
            "'" & Action.ToString & "'"

            SQL = "INSERT INTO CRDAuditTrail (" & sCols & ") VALUES (" & sVals & ")"

            oCon.ConnectionTimeout = 0
            oCon.CommandTimeout = 0
            oCon.Open(sDSN, sUser, sPassword)

            oCon.Execute(SQL)

            oCon.Close()

            oCon = Nothing
        Catch : End Try

    End Sub

    Public Function _ViewTrail(ByVal oList As ListView) As Boolean
        Try
            _SetAuditInfo()

            Dim SQL As String
            Dim oCon As New ADODB.Connection
            Dim oRs As New ADODB.Recordset
            Dim oItem As ListViewItem

            If sDSN = "" Then Return True
            oCon.ConnectionTimeout = 0
            oCon.CommandTimeout = 0
            oCon.Open(sDSN, sUser, sPassword)

            SQL = "SELECT * FROM CRDAuditTrail ORDER BY EntryDate"

            oRs.Open(SQL, oCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

            oList.Items.Clear()

            Do While oRs.EOF = False
                oItem = New ListViewItem

                With oItem
                    .Text = Convert.ToString(oRs("entrydate").Value)

                    With .SubItems
                        .Add(oRs("userid").Value)
                        .Add(oRs("scheduletype").Value)
                        .Add(oRs("schedulename").Value)
                        .Add(oRs("auditaction").Value)
                    End With
                End With

                oList.Items.Add(oItem)

                oRs.MoveNext()
            Loop

            oRs.Close()

            oCon.Close()

            Return True
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), "Please verify the Audit trail ODBC information in options")
            oUI.SaveRegistry(Registry.LocalMachine, sKey, "AuditTrail", 0)

            Return False
        End Try
    End Function
End Class
