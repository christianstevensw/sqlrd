Imports System.DirectoryServices

Friend Class clsMarsUsers
    Dim oData As New clsMarsData
    Public Enum enViewType
        ViewSingle = 0
        ViewPackage = 1
        ViewAutomation = 2
        ViewFolder = 3
        ViewSmartFolder = 4
        ViewEvent = 5
        ViewEventPackage = 6
    End Enum



    Public Sub DeleteNTUser(ByVal sUserID As String)
        Dim SQL As String
        Dim sWhere As String

        sWhere = " WHERE DomainName = '" & SQLPrepare(sUserID) & "'"

        SQL = "DELETE FROM DomainAttr " & sWhere

        clsMarsData.WriteData(SQL)

        SQL = "DELETE FROM UserColumns WHERE Owner = '" & sUserID & "'"

        clsMarsData.WriteData(SQL, False)

        SQL = "DELETE FROM UserView WHERE UserID = '" & sUserID & "'"

        clsMarsData.WriteData(SQL, False)
    End Sub

    Public Sub DeleteUser(ByVal sUserID As String)
        Dim SQL As String
        Dim sWhere As String

        sWhere = " WHERE UserID = '" & sUserID & "'"

        SQL = "DELETE FROM CRDUsers " & sWhere

        clsMarsData.WriteData(SQL)

        SQL = "DELETE FROM UserColumns WHERE Owner = '" & sUserID & "'"

        clsMarsData.WriteData(SQL, False)

        SQL = "DELETE FROM UserView WHERE UserID = '" & sUserID & "'"

        clsMarsData.WriteData(SQL, False)
    End Sub

    Public Function AddUser(ByVal sUserID As String, ByVal sPassword As String, _
    ByVal sFirstName As String, ByVal sLastName As String, ByVal sRole As String, ByVal AutoLogin As Boolean, ByVal lokiEnabled As Boolean) _
    As Boolean
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sCols As String
        Dim sVals As String
        Dim sWhere As String

        sWhere = " WHERE UserID = '" & sUserID & "'"

        SQL = "SELECT * FROM CRDUsers " & sWhere

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                SQL = "[Password] = '" & SQLPrepare(_EncryptDBValue(sPassword)) & "'," & _
                "[FirstName] = '" & SQLPrepare(sFirstName) & "'," & _
                "[LastName] = '" & SQLPrepare(sLastName) & "'," & _
                "[UserRole] ='" & SQLPrepare(sRole) & "'," & _
                "[LastModBy] = '" & SQLPrepare(gUser) & "'," & _
                "[LastModDate] = '" & ConDateTime(Date.Now) & "'," & _
                "[AutoLogin] = " & Convert.ToInt32(AutoLogin) & "," & _
                "[lokiEnabled] = " & Convert.ToInt32(lokiEnabled) & "," & _
                "[bytekey] = 1 "

                SQL = "UPDATE CRDUsers SET " & SQL & sWhere
            Else
                sCols = "[UserID],[Password],[FirstName],[LastName],[UserRole],[LastModBy],[LastModDate],[AutoLogin],[lokiEnabled],[UserNumber],[bytekey]"
                sVals = "'" & sUserID & "'," & _
                "'" & SQLPrepare(_EncryptDBValue(sPassword)) & "'," & _
                "'" & SQLPrepare(sFirstName) & "'," & _
                "'" & SQLPrepare(sLastName) & "'," & _
                "'" & SQLPrepare(sRole) & "'," & _
                "'" & SQLPrepare(gUser) & "'," & _
                "'" & ConDateTime(Date.Now) & "'," & _
                Convert.ToInt32(AutoLogin) & "," & _
                Convert.ToInt32(lokiEnabled) & "," & _
                clsMarsData.GetNewUserNumber("CRDUsers") & "," & _
                1

                SQL = "INSERT INTO CRDUsers (" & sCols & ") VALUES (" & sVals & ")"

            End If

            Dim oReturn As Boolean

            oReturn = clsMarsData.WriteData(SQL)

            If AutoLogin = True Then
                oReturn = clsMarsData.WriteData("UPDATE CRDUsers SET AutoLogin = 0 WHERE UserID <> '" & sUserID & "'")
            End If

            Return oReturn
        End If
    End Function

    Public Function reEncryptPasswords()

        Dim encrypter As clsSettingsManager = New clsSettingsManager()
        Dim SQL As String = "SELECT * FROM CRDUsers WHERE (byteKey = 0 OR byteKey IS NULL)"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim userid, password As String

                userid = oRs("userid").Value
                password = oRs("password").Value

                password = Decrypt(password, "qwer][po")

                password = _EncryptDBValue(password)

                clsMarsData.WriteData("UPDATE crdusers SET password ='" & SQLPrepare(password) & "', byteKey =1 WHERE userid ='" & SQLPrepare(userid) & "'")

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

    End Function
    Public Sub ListNTUsers(ByVal oTree As TreeView)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oRs1 As ADODB.Recordset
        Dim oParent As TreeNode

        SQL = "SELECT DISTINCT UserRole FROM DomainAttr"

        oRs = clsMarsData.GetData(SQL)

        oTree.Nodes.Clear()

        oParent = oTree.Nodes.Add("NT Users")
        oParent.Tag = "Root"
        oParent.ImageIndex = 2
        oParent.SelectedImageIndex = 2


        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                Dim oNode As New TreeNode

                oNode.Text = oRs("userrole").Value
                oNode.ImageIndex = 0
                oNode.SelectedImageIndex = 0
                oNode.Tag = "Role"

                SQL = "SELECT DISTINCT(DomainName) FROM DomainAttr WHERE UserRole ='" & oNode.Text & "'"

                oRs1 = clsMarsData.GetData(SQL)

                If Not oRs1 Is Nothing Then
                    Do While oRs1.EOF = False
                        Dim oChild As New TreeNode

                        oChild.Text = oRs1("domainname").Value

                        oChild.ImageIndex = 1
                        oChild.SelectedImageIndex = 1
                        oChild.Tag = "UserID"
                        oNode.Nodes.Add(oChild)

                        oRs1.MoveNext()
                    Loop

                    oRs1.Close()
                End If

                oParent.Nodes.Add(oNode)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        oData.DisposeRecordset(oRs)
        oData.DisposeRecordset(oRs1)

        oTree.Nodes(0).ExpandAll()

    End Sub

    Public Sub ListUsers(ByVal oTree As TreeView)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oRs1 As ADODB.Recordset
        Dim oParent As TreeNode

        SQL = "SELECT DISTINCT UserRole FROM CRDUsers WHERE UserID <> 'SQLRDAdmin'"

        oRs = clsMarsData.GetData(SQL)

        oTree.Nodes.Clear()

        oParent = oTree.Nodes.Add("SQL-RD Users")
        oParent.Tag = "Root"
        oParent.ImageIndex = 2
        oParent.SelectedImageIndex = 2


        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                Dim oNode As New TreeNode

                oNode.Text = oRs("userrole").Value
                oNode.ImageIndex = 0
                oNode.SelectedImageIndex = 0
                oNode.Tag = "Role"

                SQL = "SELECT UserID FROM CRDUsers WHERE UserRole ='" & oNode.Text & "' AND UserID <> 'SQLRDAdmin'"
                oRs1 = clsMarsData.GetData(SQL)

                If Not oRs1 Is Nothing Then
                    Do While oRs1.EOF = False
                        Dim oChild As New TreeNode

                        oChild.Text = oRs1("userid").Value

                        oChild.ImageIndex = 1
                        oChild.SelectedImageIndex = 1
                        oChild.Tag = "UserID:" & oRs1("userid").Value
                        oNode.Nodes.Add(oChild)

                        oRs1.MoveNext()
                    Loop

                    oRs1.Close()
                End If

                oParent.Nodes.Add(oNode)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        oData.DisposeRecordset(oRs)
        oData.DisposeRecordset(oRs1)

        oTree.Nodes(0).ExpandAll()

    End Sub

    Public Sub GiveOwnership(ByVal nid As Integer, ByVal sUserID As String, ByVal sType As enViewType)
        Dim SQL As String
        Dim sCol As String
        Dim sVals As String
        Dim table As String

        Select Case sType
            Case enViewType.ViewSingle
                sCol = "ReportID"
                table = "reportattr"
            Case enViewType.ViewPackage
                sCol = "PackID"
                table = "packageattr"
            Case enViewType.ViewAutomation
                sCol = "AutoID"
                table = "automationattr"
            Case enViewType.ViewSmartFolder
                sCol = "SmartID"
                table = "smartfolderattr"
            Case enViewType.ViewEvent
                sCol = "EventID"
                table = "eventattr6"
            Case enViewType.ViewEventPackage
                sCol = "EventPackID"
                table = "eventpackageattr"
            Case enViewType.ViewFolder
                Dim folder As clsFolders = New clsFolders

                folder.giveOwnershipOfChildrenToUser(sUserID, nid)

                Return
            Case Else
                Return
        End Select

        SQL = "UPDATE " & table & " SET owner = '" & sUserID & "' WHERE " & sCol & " = " & nid

        clsMarsData.WriteData(SQL)
    End Sub
    Public Sub AssignView(ByVal nID As Integer, ByVal sUserID As String, ByVal sType As enViewType)
        Dim SQL As String
        Dim sCol As String
        Dim sCols As String
        Dim sVals As String

        Select Case sType
            Case enViewType.ViewSingle
                sCol = "ReportID"
            Case enViewType.ViewPackage
                sCol = "PackID"
            Case enViewType.ViewAutomation
                sCol = "AutoID"
            Case enViewType.ViewFolder
                sCol = "FolderID"
            Case enViewType.ViewSmartFolder
                sCol = "SmartID"
            Case enViewType.ViewEvent
                sCol = "EventID"
            Case enViewType.ViewEventPackage
                sCol = "EventPackID"
        End Select

        sCols = "ViewID,UserID," & sCol & ""

        sVals = clsMarsData.CreateDataID("userview", "viewid") & "," & _
        "'" & sUserID & "'," & nID

        SQL = "INSERT INTO UserView (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

    End Sub

    Public Shared Function canEnableUserForLoki(ByRef message As String, ctrl As Object) As Boolean
        '//check the number of loki enabled users
        Dim SQL As String = "SELECT COUNT(*) FROM crdusers WHERE lokienabled = 1"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim lokiUsers As Integer

        If oRs IsNot Nothing Then
            lokiUsers = oRs(0).Value

            oRs.Close()

            oRs = Nothing
        End If

        If lokiUsers < 1 Then
            Return True '//if no loki enabled users then allow 
        Else

            If gnEdition = gEdition.CORPORATE Or gnEdition = gEdition.EVALUATION Then Return True

            '//they have but how many exactly
            Dim upgradeKey As String = clsMarsUI.MainUI.ReadRegistry("UpgradeKey", "", True)
            Dim CustID As String = clsMarsUI.MainUI.ReadRegistry("custNo", "")


            If upgradeKey <> "" And CustID <> "" Then
                Dim keycustID As String = upgradeKey.Split("|")(0) '//the custid has to be checked

                If keycustID <> CustID Then
                    message = "The Upgrade key you have does not contain the correct format and values to be used to validate this request"
                    Return False
                End If

                '//check the key
                Dim lokiKey As String = ""

                For Each s As String In upgradeKey.Split("|")
                    If s.ToLower.StartsWith("lk") Then
                        lokiKey = s
                        Exit For
                    End If
                Next

                If lokiKey = "" Or lokiKey.Length <= 2 Then '//the key doesn't exist
                    _NeedUpgrade(gEdition.CORPORATE, ctrl, "You currently do not have the ability to add Web users.")
                Else
                    Dim allowedUsers As Integer = lokiKey.Remove(0, 2)

                    If allowedUsers >= lokiUsers Then
                        Return True
                    Else
                        message = "You have reached the maximum number of Loki enabled users allowed. Please contact our sales department to purchase more."
                        Return False
                    End If
                End If
            End If
        End If

    End Function

    Public Shared Function lokiAllowedUsersExceeded() As Boolean
        '//check the number of loki enabled users
        Dim SQL As String = "SELECT COUNT(*) FROM crdusers WHERE lokienabled = 1"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim lokiUsers As Integer

        If gnEdition = gEdition.CORPORATE Or gnEdition = gEdition.EVALUATION Then Return False

        If oRs IsNot Nothing Then
            lokiUsers = oRs(0).Value

            oRs.Close()

            oRs = Nothing
        End If

        If lokiUsers <= 1 Then
            Return False
        Else

            '//they have but how many exactly
            Dim upgradeKey As String = clsMarsUI.MainUI.ReadRegistry("UpgradeKey", "", True)
            Dim CustID As String = clsMarsUI.MainUI.ReadRegistry("custNo", "")

            If upgradeKey <> "" And CustID <> "" Then
                Dim keycustID As String = upgradeKey.Split("|")(0) '//the custid has to be checked

                If keycustID <> CustID Then
                    _ErrorHandle("The Upgrade key you have does not contain the correct format and values to be used to validate this request. All Loki enabled users will be disabled.", -100467041, Reflection.MethodBase.GetCurrentMethod.Name, _
                                   0, , True, True)
                    Return True
                End If

                '//check the key
                Dim lokiKey As String = ""

                For Each s As String In upgradeKey.Split("|")
                    If s.ToLower.StartsWith("lk") Then
                        lokiKey = s
                        Exit For
                    End If
                Next

                If lokiKey = "" Or lokiKey.Length <= 2 Then '//the key doesn't exist
                    _ErrorHandle("You currently do not have the ability to add Loki users. All Loki enabled users will be disabled.", -100467041, Reflection.MethodBase.GetCurrentMethod.Name, 0, , True, True)
                Else
                    Dim allowedUsers As Integer = lokiKey.Remove(0, 2)

                    If allowedUsers <= lokiUsers Then
                        Return False
                    Else
                        _ErrorHandle("You have exceeded the maximum number of Loki enabled users allowed. Please contact our sales department to purchase more. All Loki enabled users will be disabled.", -100467041, _
                                       Reflection.MethodBase.GetCurrentMethod.Name, 0, , True, True)
                        Return True
                    End If
                End If
            End If
        End If
    End Function

    Public Function Check_If_Member_Of_AD_Group(ByVal username As String, _
    ByVal grouptoCheck As String, _
    ByVal domain As String, _
    ByVal ADlogin As String, _
    ByVal ADpassword As String) _
    As Boolean

        'This is a function that receives a username to see if it's a
        'member of a specific group in AD.


        Try
            'First let's put the whole thing in a nice big try catch, and
            'catch any errors.

            Dim EntryString As String
            EntryString = "LDAP://" & domain
            'Above, we setup the LDAP basic entry string.


            Dim myDE As DirectoryEntry
            'Above, I dimension my DirectoryEntry object


            grouptoCheck = grouptoCheck.ToLower()
            'The groups returned may have different combinations of
            'lowercase and uppercase, so let's go ahead
            'and make grouptoCheck lowercase.


            If (ADlogin <> "" AndAlso ADpassword <> "") Then
                'If they provided a password, then add it
                'as an argument to the function
                'I recently learned about AndAlso, and it's pretty
                'cool. Basically it does not worry about checking
                'the next condition if the first one is not true.
                myDE = New DirectoryEntry(EntryString, ADlogin, ADpassword)
                'Above, we create a new instance of the Directory Entry
                'Includes login and password
            Else
                'Else, use the account credentials of the machine 
                'making the request. You might not be able to get 
                'away with this if your production server does not have 
                'rights to query Active Directory.
                'Then again, there are workarounds for anything.
                myDE = New DirectoryEntry(EntryString)
                'Above, we create a new instance of the Directory Entry
                'Does not include login and password
            End If

            Dim myDirectorySearcher As New DirectorySearcher(myDE)
            'Above we create new instance of a DirectorySearcher
            'We also specify the Directory Entry as an argument.

            myDirectorySearcher.Filter = "sAMAccountName=" & username
            'Above we specify to filter our results where
            'sAMAccountName is equal to our username passed in.
            myDirectorySearcher.PropertiesToLoad.Add("MemberOf")
            'We only care about the MemberOf Properties, and we
            'specify that above.

            Dim myresult As SearchResult = myDirectorySearcher.FindOne()
            'SearchResult is a node in Active Directory that is returned
            'during a search through System.DirectoryServices.DirectorySearcher
            'Above, we dim a myresult object, and assign a node returned
            'from myDirectorySearcher.FindOne()
            'I've never heard of similar login Id's in Active Directory, 
            'so I don't think we need to call FindAll(), so Instead 
            'we call FindOne()


            Dim NumberOfGroups As Integer
            NumberOfGroups = myresult.Properties("memberOf").Count() - 1
            'Above we get the number of groups the user is a memberOf, 
            'and store it in a variable. It is zero indexed, so we
            'remove 1 so we can loop through it.

            Dim tempString As String
            'A temp string that we will use to get only what we
            'need from the MemberOf string property

#If DEBUG Then
            '  MsgBox(grouptoCheck)
#End If

            While (NumberOfGroups >= 0)
                tempString = myresult.Properties("MemberOf").Item(NumberOfGroups)
                tempString = tempString.Substring(0, tempString.IndexOf(",", 0))
                'Above we set tempString to the first index of "," starting
                'from the zeroth element of itself.
                tempString = tempString.Replace("CN=", "")
                'Above, we remove the "CN=" from the beginning of the string
                tempString = tempString.ToLower() 'Lets make all letters lowercase
                tempString = tempString.Trim()
                'Finnally, we trim any blank characters from the edges

#If DEBUG Then
                '   MsgBox(tempString)
#End If
                If (grouptoCheck = tempString) Then
                    Return True
                End If
                'If we have a match, the return is true
                'username is a member of grouptoCheck

                NumberOfGroups = NumberOfGroups - 1
            End While


            'If the code reaches here, there was no match.
            'Return false
            Return False


        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, "", True)

            Return False
        End Try


    End Function
End Class
