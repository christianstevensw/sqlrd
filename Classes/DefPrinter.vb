Imports System.Runtime.InteropServices

Public Class DefPrinter
    Private Structure helper
        Private dummy As Integer
        Public Const VER_PLATFORM_WIN32_WINDOWS As Integer = 1
        Public Const PRINTER_ATTRIBUTE_DEFAULT As Integer = 4
        Public Const HWND_BROADCAST As Integer = &HFFFF
        Public Const WM_SETTINGCHANGE As Integer = &H1A
        <DllImport("winspool.drv")> _
        Public Overloads Shared Function SetDefaultPrinter(<MarshalAs(UnmanagedType.LPStr)> ByVal pszPrinter As String) As Integer
        End Function
        <DllImport("winspool.drv")> _
        Public Overloads Shared Function OpenPrinter(<MarshalAs(UnmanagedType.LPStr)> ByVal pPrinterName As String, _
                   ByRef phPrinter As IntPtr, ByVal pDefault As Integer) As Integer
        End Function
        <DllImport("winspool.drv")> _
        Public Overloads Shared Function GetPrinter(ByVal hPrinter As IntPtr, ByVal Level As Integer, ByVal pPrinter As IntPtr, ByVal cbBuf As Integer, ByRef pcbNeeded As Integer) As Integer
        End Function
        <DllImport("winspool.drv")> _
        Public Overloads Shared Function SetPrinter(ByVal hPrinter As IntPtr, ByVal Level As Integer, ByVal pPrinter As IntPtr, ByVal Command As Integer) As Integer
        End Function
        <DllImport("winspool.drv")> _
        Public Overloads Shared Function ClosePrinter(ByVal hPrinter As IntPtr) As Integer
        End Function
        <DllImport("winspool.drv")> _
        Public Overloads Shared Function GetDefaultPrinter(ByVal pszBuffer As Byte(), ByRef pcchBuffer As Integer) As Integer
        End Function
        <DllImport("kernel32")> _
        Public Overloads Shared Function GetProfileString(<MarshalAs(UnmanagedType.LPStr)> ByVal lpAppName As String, _
        <MarshalAs(UnmanagedType.LPStr)> ByVal lpKeyName As String, <MarshalAs(UnmanagedType.LPStr)> ByVal lpDefault As String, _
        ByVal lpReturnedString As Byte(), ByVal nSize As Integer) As Integer
        End Function
        <DllImport("kernel32")> _
        Public Overloads Shared Function WriteProfileString(<MarshalAs(UnmanagedType.LPStr)> ByVal lpAppName As String, _
           <MarshalAs(UnmanagedType.LPStr)> ByVal lpKeyName As String, _
           <MarshalAs(UnmanagedType.LPStr)> ByVal lpString As String) As Integer
        End Function
        <DllImport("user32", EntryPoint:="SendNotifyMessageA")> _
        Public Overloads Shared Function SendNotifyMessageStr(ByVal hWnd As Integer, ByVal Msg As Integer, ByVal wParam As Integer, <MarshalAs(UnmanagedType.LPStr)> ByVal lParam As String) As Integer
        End Function

        <StructLayout(LayoutKind.Sequential)> _
        Public Structure PRINTER_INFO_2
            <MarshalAs(UnmanagedType.LPStr)> _
            Public pServerName As String
            <MarshalAs(UnmanagedType.LPStr)> _
            Public pPrinterName As String
            <MarshalAs(UnmanagedType.LPStr)> _
            Public pShareName As String
            <MarshalAs(UnmanagedType.LPStr)> _
            Public pPortName As String
            <MarshalAs(UnmanagedType.LPStr)> _
            Public pDriverName As String
            <MarshalAs(UnmanagedType.LPStr)> _
            Public pComment As String
            <MarshalAs(UnmanagedType.LPStr)> _
            Public pLocation As String
            Public pDevMode As Integer
            <MarshalAs(UnmanagedType.LPStr)> _
            Public pSepFile As String
            <MarshalAs(UnmanagedType.LPStr)> _
            Public pPrintProcessor As String
            <MarshalAs(UnmanagedType.LPStr)> _
            Public pDatatype As String
            <MarshalAs(UnmanagedType.LPStr)> _
            Public pParameters As String
            Public pSecurityDescriptor As Integer
            Public Attributes As Integer
            Public Priority As Integer
            Public DefaultPriority As Integer
            Public StartTime As Integer
            Public UntilTime As Integer
            Public Status As Integer
            Public cJobs As Integer
            Public AveragePPM As Integer
        End Structure
    End Structure


    <DebuggerStepThrough()> Shared Sub SetDefPrinter(ByVal DefPrinter As String)
        Dim dwNeed As Integer = 0
        Dim dwReturn As Integer = 0
        Dim pi2 As helper.PRINTER_INFO_2 = New helper.PRINTER_INFO_2
        Dim hPrinter As IntPtr = IntPtr.Zero
        Dim Buffer As String = ""

        If System.Environment.OSVersion.Version.Major >= 5 Then
            helper.SetDefaultPrinter(DefPrinter)
        Else
            If ((helper.OpenPrinter(DefPrinter, hPrinter, 0) = 0) Or (hPrinter.ToInt32() = 0)) Then Exit Sub
            Dim pData As IntPtr = IntPtr.Zero
            helper.GetPrinter(hPrinter, 2, IntPtr.Zero, 0, dwNeed)
            pData = Marshal.AllocHGlobal(dwNeed)
            helper.GetPrinter(hPrinter, 2, pData, dwNeed, dwNeed)
            pi2 = CType(Marshal.PtrToStructure(pData, GetType(helper.PRINTER_INFO_2)), helper.PRINTER_INFO_2)
            If System.Environment.OSVersion.Platform = helper.VER_PLATFORM_WIN32_WINDOWS Then
                pi2.Attributes = pi2.Attributes Or helper.PRINTER_ATTRIBUTE_DEFAULT
                If (helper.SetPrinter(hPrinter, 2, pData, 0) = 0) Then
                    Marshal.FreeHGlobal(pData)
                    Exit Sub

                End If
            Else
                Buffer = DefPrinter + "," + pi2.pDriverName + "," + pi2.pPortName
                If (helper.WriteProfileString("windows", "device", Buffer) = 0) Then Exit Sub
                helper.SendNotifyMessageStr(helper.HWND_BROADCAST, helper.WM_SETTINGCHANGE, 0, "windows")
                Marshal.FreeHGlobal(pData)
                helper.ClosePrinter(hPrinter)
            End If
        End If

        _Delay(2)
    End Sub

    <DebuggerStepThrough()> Public Shared Sub GetDefPrinter(ByRef DefPrinter As String)
        Dim dwNeed As Integer = 0
        Dim Buffer As String = ""
        DefPrinter = Nothing
        Dim bt() As Byte
        ' 2000/XP
        If System.Environment.OSVersion.Version.Major >= 5 Then
            dwNeed = 128
            helper.GetDefaultPrinter(bt, dwNeed)
            If (dwNeed > 0) Then

                ReDim bt(dwNeed)
                helper.GetDefaultPrinter(bt, dwNeed)
                Dim k As Integer
                For k = 0 To (bt.Length - 1)
                    If (bt(k) = 0) Then Exit For
                    Buffer += Microsoft.VisualBasic.ChrW(bt(k))
                Next k
                DefPrinter = Buffer
            End If
            ' 95/98, NT 4.0
        Else
            Dim dwCopied As Integer
            ReDim bt(256)
            dwCopied = helper.GetProfileString("windows", "device", ",,,", bt, 255)
            Dim k As Integer
            For k = 0 To bt.Length - 1
                Dim ch As Char = Microsoft.VisualBasic.ChrW(bt(k))
                If (ch = ",") Then Exit For
                Buffer += ch
            Next k
            DefPrinter = Buffer
        End If
    End Sub

End Class
