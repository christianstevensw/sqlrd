Imports Microsoft.Win32
Imports System.Net
Imports System.Net.Sockets
Imports sqlrd.clsMarsUI

Friend Class clsClustering
    Dim oData As New clsMarsData

    Public Sub ObeyCluster()
        Try
            Dim sClusterMaster As String
            Dim oUI As New clsMarsUI
            Dim listener As clsListener = New clsListener
            Dim masterConfig As String

            sClusterMaster = oUI.ReadRegistry("ClusterMaster", "")

            If sClusterMaster.Length = 0 Then
                Throw New Exception("Could not find master's system path. Please setup the master again")
            End If

            oData.CloseMainDataConnection()

            If sClusterMaster.EndsWith("\") = False Then sClusterMaster &= "\"

            masterConfig = sClusterMaster & "sqlrdlive.config"

            gConfigFile = masterConfig

            If gConType = "DAT" Then
                sCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sClusterMaster & "sqlrdlive.dat;Persist Security Info=False"
            Else
                sCon = oUI.ReadRegistry("ConString", "", True)
            End If

            oData.OpenMainDataConnection()

            sAppPath = sClusterMaster

            listener.m_listenPort = MainUI.ReadRegistry("ClusterPort", 2908)
            listener.Listen()

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try

    End Sub
    Public Sub RunCluster()

        Dim oRs As New ADODB.Recordset
        Dim SQL As String
        Dim Slaves() As String
        Dim I As Integer = 0
        Dim nReportID As Integer = 0
        Dim nPackID As Integer = 0
        Dim nAutoID As Integer = 0
        Dim UseUniversal As Boolean = False

        Try
            UseUniversal = Convert.ToBoolean(Convert.ToInt32(MainUI.ReadRegistry("UniversalTime", 0)))
        Catch ex As Exception
            UseUniversal = False
        End Try

        Try
            'check which slaves are active
            SQL = "SELECT SlaveName, SlavePath,IPAddress FROM Slaves WHERE " & _
                "SlavePath <> '' AND SlavePath IS NOT NULL"

            oRs = clsMarsData.GetData(SQL)

            'set slave(0) as this pc
            ReDim Slaves(I)

            Slaves(I) = My.Computer.Name

            I += 1

            Do While oRs.EOF = False
                If SlaveAlive(IsNull(oRs("slavename").Value)) = True Then
                    ReDim Preserve Slaves(I)
                    Slaves(I) = oRs("ipaddress").Value
                    I += 1
                End If
                oRs.MoveNext()
            Loop

            oRs.Close()

            I = 0

            SQL = "SELECT * FROM ScheduleAttr WHERE Status = 1"

            oRs = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then Return

            Do While oRs.EOF = False
                Dim NextRun As Date = oRs("nextrun").Value
                Dim CurrentTime As Date = Now
                Dim exceptionCal As String = ""
                Dim useException As Boolean = False

                If UseUniversal = True Then
                    NextRun = NextRun.ToUniversalTime
                    CurrentTime = Now.ToUniversalTime
                End If

                If ConDateTime(NextRun) <= ConDateTime(CurrentTime) Then
                    Dim data As String = ""

                    nPackID = IsNull(oRs("PackID").Value, 0)
                    nReportID = IsNull(oRs("ReportID").Value, 0)
                    nAutoID = IsNull(oRs("autoid").Value, 0)
                    exceptionCal = IsNull(oRs("exceptioncalendar").Value)

                    Try
                        useException = oRs("useexception").Value
                    Catch ex As Exception
                        useException = False
                    End Try


                    If clsMarsScheduler.IsDateAnException(NextRun, exceptionCal) = False Then

                        If nPackID > 0 Then
                            data = "package;" & nPackID
                        ElseIf nReportID > 0 Then
                            data = "report;" & nReportID
                        ElseIf nAutoID > 0 Then
                            data = "report;" & nAutoID
                        End If

                        Dim sender As New clsListener
                        sender.m_tcpMsg = data
                        sender.m_TcpAddress = Slaves(I)
                        sender.m_listenPort = MainUI.ReadRegistry("ClusterPort", 2908)

                        If Slaves(I) = My.Computer.Name Then 'run the schedule on this pc
                            sender.m_recievedData = data
                            Dim send As New Threading.Thread(AddressOf sender.ProcessData)

                            send.Start()
                        Else 'assign it to a slave
                            Dim send As New Threading.Thread(AddressOf sender.TcpSend)

                            send.Start()
                        End If
                    End If

                    If I >= Slaves.GetUpperBound(0) Then
                        'assign next report to the first slave
                        I = 0
                    Else
                        'assign next report to the next slave
                        I += 1
                    End If
                Else
                    gErrorDesc = String.Empty
                    Dim OriginalNextRun As Date = oRs("nextrun").Value
                    Dim nScheduleID As Integer = oRs("scheduleid").Value
                    Dim sFrequency As String = oRs("Frequency").Value
                    Dim StartDate As Date = oRs("StartDate").Value
                    Dim StartTime As Date = oRs("StartTime").Value
                    Dim sCal As String = IsNull(oRs("calendarname").Value)
                    Dim nSmartID As Integer = IsNull(oRs("smartid").Value, 0)
                    Dim nRepeat As Double = 0
                    Dim sUntil As String = ""
                    Dim Repeat As Boolean = False
                    Dim RepeatUnit As String = "hours"

                    Try
                        Repeat = Convert.ToBoolean(oRs("repeat").Value)
                    Catch
                        Repeat = False
                    End Try

                    Try
                        nRepeat = oRs("repeatinterval").Value
                    Catch
                        nRepeat = 0
                    End Try

                    Try
                        sUntil = ConTime(oRs("RepeatUntil").Value)
                    Catch
                        sUntil = IsNull(oRs("RepeatUntil").Value, "")
                    End Try

                    Try
                        RepeatUnit = IsNull(oRs("repeatunit").Value, "hours")
                    Catch ex As Exception
                        RepeatUnit = "hours"
                    End Try

                    Dim nextToRun As Date = clsMarsScheduler.globalItem.GetNextRun(nScheduleID, sFrequency, StartDate, StartTime, Repeat, nRepeat, sUntil, _
                     sCal, , , , RepeatUnit)

                    SQL = "UPDATE ScheduleAttr SET NextRun = '" & _
                            ConDateTime(nextToRun) & "' WHERE ScheduleID =" & nScheduleID

                    clsMarsData.WriteData(SQL)
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()

        Catch ex As Exception
            If Err.Number = 9 Then
                _ErrorHandle("No slaves could be found", Err.Number, "clsClustering.RunCluster", _GetLineNumber(ex.StackTrace), _
                "Please make sure that you have slaves in your cluster that they are running")
            Else
                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            End If
        End Try

    End Sub
    Public Function SlaveAlive(ByVal slaveName As String) As Boolean

        Try
            Dim hostEntry As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry(slaveName)
            Dim tcpAddress As String = ""

            For Each s As System.Net.IPAddress In hostEntry.AddressList
                tcpAddress = s.ToString
            Next

            Return True

            Dim test As New clsListener

            test.m_listenPort = MainUI.ReadRegistry("ClusterPort", 2908)
            test.m_TcpAddress = tcpAddress
            test.m_tcpMsg = "Testing"
            test.TcpSend()

            If test.m_recievedData Is Nothing Then
                Throw New Exception("No response recieved from slave")
            End If

            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
    '    Public Sub CheckCluster()
    '        Try
    '            Dim SlaveTimeout As Integer
    '            Dim SQL As String
    '            Dim oRs As ADODB.Recordset
    '            Dim oData As New clsMarsData
    '            Dim Slaves() As String
    '            Dim Cons() As String
    '            Dim I As Integer
    '            Dim CurrentSlave As String
    '            Dim CurrentIndex As Integer
    '            Dim NewIndex As Integer
    '            Dim ReportID As String
    '            Dim PackID As String
    '            Dim sDateDiff As String

    '            If gConType = "DAT" Then
    '                sDateDiff = "'s'"
    '            Else
    '                sDateDiff = "s"
    '            End If
    '            'get our active slaces

    '            SQL = "SELECT SlaveName, SlavePath FROM Slaves WHERE " & _
    '                "SlavePath <> '' AND SlavePath IS NOT NULL"

    '            oRs = clsMarsData.GetData(SQL)


    '            Do While oRs.EOF = False
    '                If SlaveAlive(IsNull(oRs("slavepath").Value)) Then
    '                    ReDim Preserve Slaves(I)
    '                    ReDim Preserve Cons(I)

    '                    Slaves(I) = oRs("slavename").Value
    '                    Cons(I) = oRs("slavepath").Value

    '                    I = I + 1
    '                End If
    'ResumeHere:
    '                oRs.MoveNext()
    '            Loop

    '            oRs.Close()

    '            'get the schedules that haven't been picked up
    '            Dim oUI As New clsMarsUI

    '            SlaveTimeout = oUI.ReadRegistry("SlaveTimeout", 25)

    '            SQL = "SELECT * FROM TaskManager WHERE " & _
    '            "(Status <> 'Locked' OR Status IS NULL) AND DATEDIFF(" & sDateDiff & ",EntryDate,'" & ConDateTime(Date.Now) & "') > " & SlaveTimeout

    '            oRs = clsMarsData.GetData(SQL)

    '            If Not oRs Is Nothing Then
    '                Do While oRs.EOF = False
    '                    CurrentSlave = oRs("pcname").Value

    '                    Try
    '                        ReportID = oRs("reportid").Value
    '                    Catch
    '                        ReportID = 0
    '                    End Try

    '                    Try
    '                        PackID = oRs("packid").Value
    '                    Catch
    '                        PackID = 0
    '                    End Try

    '                    For I = 0 To Slaves.GetUpperBound(0)
    '                        If Slaves(I) = CurrentSlave Then
    '                            CurrentIndex = I
    '                            Exit For
    '                        End If
    '                    Next

    '                    If CurrentIndex >= Slaves.GetUpperBound(0) Then
    '                        NewIndex = 0
    '                    Else
    '                        For I = CurrentIndex + 1 To Slaves.GetUpperBound(0)
    '                            If SlaveAlive(Cons(I)) Then
    '                                NewIndex = I
    '                                Exit For
    '                            End If
    '                        Next
    '                    End If

    '                    If ReportID > 0 Then
    '                        clsMarsData.WriteData("UPDATE TaskManager SET " & _
    '                            "EntryDate = '" & ConDateTime(Date.Now) & "'," & _
    '                            "PCName = '" & SQLPrepare(Slaves(NewIndex)) & "' " & _
    '                            "WHERE ReportID = " & ReportID)
    '                    ElseIf PackID > 0 Then
    '                        clsMarsData.WriteData("UPDATE TaskManager SET " & _
    '                            "EntryDate = '" & ConDateTime(Date.Now) & "'," & _
    '                            "PCName = '" & SQLPrepare(Slaves(NewIndex)) & "' " & _
    '                                        "WHERE PackID = " & PackID)
    '                    End If

    '                    oRs.MoveNext()
    '                Loop
    '            End If

    '            oRs.Close()

    '            oRs = Nothing
    '        Catch : End Try
    '    End Sub


End Class
