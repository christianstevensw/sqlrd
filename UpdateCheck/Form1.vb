Imports Xceed.Ftp
Imports System.IO
Imports Microsoft.Win32

Public Class Form1
    Inherits System.Windows.Forms.Form
    Dim sValue As String
    Friend WithEvents oFtp As Xceed.Ftp.FtpClient
    Dim sCurrent As String
    Dim sxShow As Boolean = False
    Dim oT As Threading.Thread
    Friend WithEvents fbs As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents wb As System.Windows.Forms.WebBrowser
    Dim nStart As Date
    Friend WithEvents bgw As System.ComponentModel.BackgroundWorker
    Dim isLoading As Boolean = False
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents NotifyIcon1 As System.Windows.Forms.NotifyIcon
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents pg As System.Windows.Forms.ProgressBar
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label2 = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.pg = New System.Windows.Forms.ProgressBar
        Me.fbs = New System.Windows.Forms.FolderBrowserDialog
        Me.wb = New System.Windows.Forms.WebBrowser
        Me.bgw = New System.ComponentModel.BackgroundWorker
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.ForeColor = System.Drawing.Color.Navy
        Me.Label1.Location = New System.Drawing.Point(8, 88)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(448, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Status"
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(0, 152)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(472, 8)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Button1.Location = New System.Drawing.Point(384, 168)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Cancel"
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Button2.Location = New System.Drawing.Point(8, 168)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(48, 23)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "Hide"
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.Icon = CType(resources.GetObject("NotifyIcon1.Icon"), System.Drawing.Icon)
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Location = New System.Drawing.Point(-8, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(488, 64)
        Me.Panel1.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(80, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(304, 23)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "CRD Update"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(16, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(56, 50)
        Me.PictureBox1.TabIndex = 5
        Me.PictureBox1.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.White
        Me.GroupBox2.Location = New System.Drawing.Point(-16, 63)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(488, 8)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        '
        'pg
        '
        Me.pg.Location = New System.Drawing.Point(8, 104)
        Me.pg.Name = "pg"
        Me.pg.Size = New System.Drawing.Size(448, 16)
        Me.pg.TabIndex = 6
        '
        'wb
        '
        Me.wb.Location = New System.Drawing.Point(11, 213)
        Me.wb.MinimumSize = New System.Drawing.Size(20, 20)
        Me.wb.Name = "wb"
        Me.wb.Size = New System.Drawing.Size(441, 234)
        Me.wb.TabIndex = 7
        '
        'bgw
        '
        Me.bgw.WorkerSupportsCancellation = True
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(464, 197)
        Me.Controls.Add(Me.wb)
        Me.Controls.Add(Me.pg)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CRD Update "
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property
    Private Delegate Function SetValuesDelegate(ByVal s As String, ByVal n As Integer) As Object

    Private Function SetValues(ByVal labelValue As String, ByVal progressValue As Integer) As Object
        Label1.Text = labelValue
        pg.Value = progressValue

        Return Nothing
    End Function
    Public Sub ShowMsg(ByVal sMsg As String, ByVal sValue As Integer)
        Try
            If NotifyIcon1.Visible = False Then


                If InvokeRequired = True Then
                    BeginInvoke(New SetValuesDelegate(AddressOf SetValues), New Object() {sMsg, sValue})
                    Return
                End If
            Else
                NotifyIcon1.Text = sMsg
                Application.DoEvents()
            End If
        Catch ex As Exception
#If DEBUG Then
            MsgBox(ex.Message)
#End If
        End Try
    End Sub

    Public Sub _ShowMsg(ByVal sMsg As String, ByVal sValue As String)
        Try
            If NotifyIcon1.Visible = False Then

                Me.Label1.Text = sMsg
                Me.pg.Value = sValue

                Application.DoEvents()
            Else
                NotifyIcon1.Text = sMsg
                Application.DoEvents()
            End If
        Catch : End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If MessageBox.Show("Cancel the CRD Update?", "CRD Update", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            NotifyIcon1.Visible = False

            Try
                bgw.CancelAsync()
            Catch : End Try

            Application.Exit()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.WindowState = FormWindowState.Minimized

        Me.Hide()

        NotifyIcon1.Visible = True
    End Sub

    Private Sub NotifyIcon1_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles NotifyIcon1.MouseDown

    End Sub

    Private Sub NotifyIcon1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles NotifyIcon1.DoubleClick
        Me.WindowState = FormWindowState.Normal
        Me.Show()
        NotifyIcon1.Visible = False
        Me.Height = 224
        Me.Width = 470
    End Sub

    Public Sub _CheckUpdates()
        Dim UpdateFound As Boolean

        Try
            oFtp = New Xceed.Ftp.FtpClient

            Me._ShowMsg("Logging in to ChristianSteven Server...", 10)

            oFtp.Connect("christiansteven.fileburst.com")

            oFtp.Login("christiansteven", "admin")

            oFtp.ChangeCurrentFolder("/httpdocs/crd")

            Me._ShowMsg("Checking version information...", 20)

            oFtp.ReceiveFile("updatecheck.cfg", Application.StartupPath & "\updatecheck.cfg")

            oFtp.Disconnect()

            Dim sLatest As String = _ReadTextFile(Application.StartupPath & "\updatecheck.cfg")

            IO.File.Delete(Application.StartupPath & "\updatecheck.cfg")

            If _ToDate(sCurrent) < _ToDate(sLatest) Then
                UpdateFound = True
            End If

            If UpdateFound = True Then
                'Dim fbd As New FolderBrowserDialog

                Dim oRes As DialogResult = MessageBox.Show("A newer version of CRD is available for download. Would you like to download and install it now?", _
                "Update Check", MessageBoxButtons.YesNo, MessageBoxIcon.Question)

                If oRes = Windows.Forms.DialogResult.Yes Then

                    If _CheckMaint() = False Then
                        If MessageBox.Show("CRD Update cannot verify your maintenance status. Please visit the Member Area and download the latest version manually.", "CRD Update", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) = DialogResult.OK Then
                            Process.Start("http://www.christiansteven.com/members.htm")
                            oFtp.Disconnect()
                            End
                        Else
                            oFtp.Disconnect()
                            End
                        End If

                    End If

                    Dim proc As Process = New Process

                    With proc
                        .StartInfo.FileName = "iexplore"
                        .StartInfo.Arguments = "http://download.christiansteven.com/crd/CRDSetup.exe"
                        .Start()
                    End With



                    '                    With fbs
                    '                        Dim sPath As String

                    '                        .Description = "Please select destination folder..."
                    '                        .ShowNewFolderButton = True
                    'Retry:
                    '                        .ShowDialog()

                    '                        sPath = .SelectedPath

                    '                        If sPath.Length = 0 Then End

                    '                        If IO.File.Exists(.SelectedPath & "\CRDSetup.exe") Then
                    '                            If MessageBox.Show("This will overwrite the existing installer file. Proceed?", "UpdateCheck", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No Then
                    '                                GoTo Retry
                    '                            Else
                    '                                IO.File.Delete(.SelectedPath & "\CRDSetup.exe")
                    '                            End If
                    '                        End If
                    '                    End With

                    '                    sxShow = True

                    '                    nStart = Now

                    '                    bgw.RunWorkerAsync()
                Else
                    End
                End If
            Else
                MessageBox.Show("Your CRD installation is up to date", "Update Check", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End
            End If

            End
        Catch ex As Exception

            Try
                oFtp.Disconnect()
            Catch : End Try

            MessageBox.Show("Cannot conect to the ChristianSteven Update Server. Try to download the latest update directly from the Member Area.", "Oooops!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Application.Exit()
        End Try

    End Sub

    
    Private Function _GetCurrentBuild() As String
        Try
            Dim sBuild As String
            Dim oFile As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.StartupPath & "\crd.exe")

            sBuild = oFile.Comments

            sBuild = sBuild.Split("/")(0).ToLower.Replace("build", String.Empty).Trim

            Return sBuild
        Catch
            Return ""
        End Try
    End Function
    Private Function _ReadTextFile(ByVal sPath As String) As String
        On Error Resume Next

        Dim sr As StreamReader = File.OpenText(sPath)
        Dim sInput As String

        sInput = sr.ReadToEnd

        sr.Close()

        Return sInput
    End Function

    Private Sub oFtp_FileTransferStatus(ByVal sender As Object, ByVal e As Xceed.Ftp.FileTransferStatusEventArgs) Handles oFtp.FileTransferStatus
        If sxShow = False Then Return

        Dim kbLeft As Long = (e.BytesTotal - e.BytesTransferred) / 1000

        Try
            Application.DoEvents()
            Me.ShowMsg("Downloading update..." & e.AllBytesPercent & "% (" & kbLeft & " KBytes left to download)", e.AllBytesPercent)
        Catch
            Application.DoEvents()
            Me.ShowMsg("Downloading update...", "")
        End Try
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        Me.Show()

        Application.DoEvents()

        sCurrent = Me._GetCurrentBuild()

        Me._CheckUpdates()
    End Sub

    Private Function _CheckMaint() As Boolean

        Dim oValue As Boolean
        Dim sCustID As String
        Dim oSet As New clsSettingsManager

        Try
            sCustID = clsSettingsManager.Appconfig.GetSettingValue("CustNo", "", Application.StartupPath & "\crdlive.config")

            If sCustID Is Nothing Then Return False

            wb.Navigate("http://www.christiansteven.com/_supp/checknum.cgi")

            Do
                _ShowMsg("Contacting server...", 40)
                Application.DoEvents()
            Loop Until wb.ReadyState = WebBrowserReadyState.Complete

            For Each o As System.Windows.Forms.HtmlElement In wb.Document.All
                If o.Name = "number" Then

                    o.SetAttribute("value", sCustID)
                    Application.DoEvents()
                    Exit For
                End If
            Next

            For Each f As System.Windows.Forms.HtmlElement In wb.Document.Forms
                f.InvokeMember("submit")
            Next

            Do While isLoading = True
                _ShowMsg("Waiting for reply...", 50)
                Application.DoEvents()
            Loop

            Dim s As String = wb.Document.Body.InnerText

            If s.IndexOf("Error") > -1 Then
                oValue = False
            ElseIf s.Length > 50 Then
                oValue = False
            ElseIf s.IndexOf("OK") > -1 Then
                oValue = True
            Else
                oValue = False
            End If

        Catch ex As Exception
            MessageBox.Show("An error was encountered during client validation. Please try again later", _
            "CRD Update", MessageBoxButtons.OK, MessageBoxIcon.Error)
            oValue = False
        End Try

        Return oValue

    End Function

    Private Function _ToDate(ByVal sIn As String) As Date
        Dim nYear As Integer
        Dim nMonth As Integer
        Dim nDay As Integer

        nYear = sIn.Substring(0, 4)
        nMonth = sIn.Substring(4, 2)
        nDay = sIn.Substring(6, 2)

        Dim nDate As Date = New Date(nYear, nMonth, nDay)

        Return nDate
    End Function

    Private Sub fbd_HelpRequest(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub wb_Navigated(ByVal sender As Object, ByVal e As System.Windows.Forms.WebBrowserNavigatedEventArgs) Handles wb.Navigated
        isLoading = False
    End Sub

    Private Sub wb_Navigating(ByVal sender As Object, ByVal e As System.Windows.Forms.WebBrowserNavigatingEventArgs) Handles wb.Navigating
        isLoading = True
    End Sub

    Private Sub bgw_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgw.DoWork
        oFtp.ReceiveFile("CRDSetup.exe", fbs.SelectedPath & "\CRDSetup.exe")

        Try
            oFtp.Disconnect()
        Catch : End Try

        Process.Start(fbs.SelectedPath & "\CRDSetup.exe")

        End
    End Sub

End Class
