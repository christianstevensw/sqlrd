Imports Microsoft.Win32
Imports System.ServiceProcess
Imports CrystalRefresh.clsSettingsManager
Public Class clsMain
    Dim sKey As String = "Software\ChristianSteven\CRD"
    Dim gConfigFile As String = Application.StartupPath & "\crdlive.config"

    Shared Sub Main()
        Dim oMain As New clsMain

        If Environment.GetCommandLineArgs.Length = 1 Then
            oMain._Action(True)
        Else
            oMain._Action(False)
        End If
    End Sub

    Public Sub _Action(ByVal Progress As Boolean)
        Try
            Dim Scheduler As String
            Dim oForm As New Form1
            Dim SvcRun As Boolean
            Dim AppRun As Boolean

            oForm.ProgressShow = Progress

            'stop the scheduler
            oForm._ShowProgress("Stopping the CRD scheduler...")

            Scheduler = _ReadRegistry("CRDService", "NONE", gConfigFile)

            Select Case Scheduler.ToLower
                Case "windowsapp"
                    For Each o As Process In Process.GetProcessesByName("crdapp")
                        SvcRun = True
                        o.Kill()
                    Next
                Case "windowsnt"
                    Dim Svc As New ServiceController("CRD Monitor")

                    If Svc.Status <> ServiceControllerStatus.Stopped Then
                        SvcRun = True
                        Svc.Stop()
                    End If

                    Svc = New ServiceController("CRD")

                    If Svc.Status <> ServiceControllerStatus.Stopped Then
                        SvcRun = True
                        Svc.Stop()
                    End If
            End Select

            'kill CRD
            oForm._ShowProgress("Stopping CRD...")

            For Each o As Process In Process.GetProcessesByName("crd")
                AppRun = True
                o.Kill()
            Next

            'get Crystal version
            oForm._ShowProgress("Validating Crystal version...")

            Dim sPath As String = Application.StartupPath

            If sPath.EndsWith("\") = False Then sPath &= "\"

            Dim oFile As FileVersionInfo = FileVersionInfo.GetVersionInfo(sPath & "crd.exe")

            Dim sComment As String = oFile.Comments
            Dim sVer As String = sComment.Split("/")(1)
            Dim sDll As String
            Dim sExe As String

            oFile = Nothing

            'delete file
            oForm._ShowProgress("Cleaning Interop files...")

            Select Case sVer
                Case "85"
                    sDll = "interop.CRAXDRT85.dll"
                    sExe = "interop85.dll"
                Case "90"
                    sDll = "interop.CRAXDRT90.dll"
                    sExe = "interop90.dll"
                Case "100"
                    sDll = "interop.CRAXDRT10.dll"
                    sExe = "interop10.dll"
                Case "110"
                    sDll = "interop.CRAXDRT11.dll"
                    sExe = "interop11.dll"
                Case "115"
                    sDll = "interop.CRAXDRT11.dll"
                    sExe = "interop115.dll"
                Case "120"
                    sDll = "interop.CRAXDRT11.dll"
                    sExe = "interop12.dll"
            End Select

            System.Threading.Thread.Sleep(5000)

            'System.IO.File.Delete(sPath & "interop.CRAXDRT.dll")
            System.IO.File.Delete(sPath & "crd.exe")

            'copy file
            oForm._ShowProgress("Refreshing Interop files...")

            'IO.File.Copy(sPath & sDll, sPath & "Interop.CRAXDRT.dll", True)
            IO.File.Copy(sPath & sExe, sPath & "crd.exe", True)

            'restart scheduler
            If SvcRun = True Then
                oForm._ShowProgress("Restarting CRD scheduler...")

                Select Case Scheduler.ToLower
                    Case "windowsapp"
                        Process.Start(sPath & "crdapp.exe")
                    Case "windowsnt"
                        Dim Svc As New ServiceController("CRD")

                        If Svc.Status = ServiceControllerStatus.Stopped Then
                            Svc.Start()
                        End If

                        Svc = New ServiceController("CRD Monitor")

                        If Svc.Status = ServiceControllerStatus.Stopped Then
                            Svc.Start()
                        End If
                End Select
            End If

            'restart CRD
            If AppRun = True Then
                oForm._ShowProgress("Restarting CRD...")
                Process.Start(sPath & "crd.exe")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "CRD Refresh", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            Application.Exit()
        End Try

        Application.Exit()

    End Sub

    Public Overloads Function _ReadRegistry(ByVal SettingName As String, ByVal DefaultValue As Object, ByVal ConfigFile As String) As Object
        Return Appconfig.GetSettingValue(SettingName, DefaultValue, ConfigFile)
    End Function
    Public Overloads Function _ReadRegistry(ByVal ParentKey As RegistryKey, ByVal SubKey As String, _
    ByVal ValueName As String, ByRef Value As Object) As Object

        Dim Key As RegistryKey
        Dim Temp

        Temp = Value

        Try
            'Open the registry key.
            Key = ParentKey.OpenSubKey(SubKey, True)

            If Key Is Nothing Then 'if the key doesn't exist
                Return Temp
            End If

            'Get the value.
            Value = Key.GetValue(ValueName)

            Key.Close()

            If Value Is Nothing Then
                Return Temp
            Else
                Return Value
            End If
        Catch e As Exception
            Return Temp
        End Try
    End Function
End Class
