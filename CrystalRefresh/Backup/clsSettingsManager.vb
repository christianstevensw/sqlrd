Imports System
Imports System.Xml
Imports System.IO
Imports System.Security.Cryptography

Public Class clsSettingsManager

    Dim mProvider As TripleDESCryptoServiceProvider

    Public Shared Appconfig As New clsSettingsManager


    Sub New()
        mProvider = New TripleDESCryptoServiceProvider
        mProvider.Key = New Byte() {112, 223, 122, 83, 173, 22, 186, 153, 229, 183, 73, 133, 124, 124, 132, 13}
        mProvider.IV = New Byte() {173, 224, 14, 43, 253, 103, 82, 212}
    End Sub
    

    Public Function GetSettingValue(ByVal SettingName As String, _
    ByVal DefaultValue As Object, ByVal sConfigFile As String, Optional ByVal Decrypt As Boolean = False) As Object

        Dim oDoc As XmlDocument = New Xml.XmlDocument
        Dim oNodes As XmlNodeList
        Dim oNode As XmlNode


        If IO.File.Exists(sConfigFile) = False Then
            CreateSettingsFile(sConfigFile)
        End If

        oDoc.Load(sConfigFile)

        oNodes = oDoc.GetElementsByTagName("setting")

        For Each oNode In oNodes
            Dim Attr As String = ""

            Attr = oNode.Attributes("name").Value

            If Attr.ToLower = SettingName.ToLower Then
                DefaultValue = oNode.ChildNodes(0).InnerText

                If Decrypt = True Then
                    DefaultValue = DecryptString(DefaultValue)
                End If
                Exit For
            End If
        Next

        Return DefaultValue
    End Function

    Public Function DeleteSetting(ByVal SettingName As String, ByVal sConfigFile As String) As Boolean

        Dim oDoc As XmlDocument = New Xml.XmlDocument
        Dim oNodes As XmlNodeList
        Dim oNode As XmlNode

        If IO.File.Exists(sConfigFile) = False Then
            CreateSettingsFile(sConfigFile)
        End If

        oDoc.Load(sConfigFile)

        oNodes = oDoc.GetElementsByTagName("setting")


        For Each oNode In oNodes
            Dim Attr As String

            Attr = oNode.Attributes("name").Value

            If Attr.ToLower = SettingName.ToLower Then
                Exit For
            End If
        Next

        If Not oNode Is Nothing Then
            Dim ParentNode As XmlNodeList

            ParentNode = oDoc.GetElementsByTagName("settings")

            For Each iNode As XmlNode In ParentNode
                iNode.RemoveChild(oNode)
            Next

            oDoc.Save(sConfigFile)
        End If

        Return True

    End Function
    Private Function SettingExists(ByVal SettingName As String, ByVal oDoc As XmlDocument) As Boolean
        Dim oNodes As XmlNodeList
        Dim oNode As XmlNode

        oNodes = oDoc.GetElementsByTagName("setting")

        For Each oNode In oNodes
            Dim Attr As String

            Attr = oNode.Attributes("name").Value

            If Attr.ToLower = SettingName.ToLower Then
                Return True
            End If
        Next

        Return False
    End Function

    Public Function SaveSettingValue(ByVal SettingName As String, ByVal SettingValue As Object, _
    ByVal sConfigFile As String, Optional ByVal Encrypt As Boolean = False) As Boolean

        Dim oDoc As XmlDocument = New Xml.XmlDocument
        Dim oNodes As XmlNodeList
        Dim oNode As XmlNode

        Try
            If IO.File.Exists(sConfigFile) = False Then
                CreateSettingsFile(sConfigFile)
            End If

            oDoc.Load(sConfigFile)

            oNodes = oDoc.GetElementsByTagName("setting")

            If Encrypt = True Then
                SettingValue = EncryptString(SettingValue)
            End If

            If SettingExists(SettingName, oDoc) = True Then
                For Each oNode In oNodes
                    Dim Attr As String

                    Attr = oNode.Attributes("name").Value

                    If Attr.ToLower = SettingName.ToLower Then
                        oNode.ChildNodes(0).InnerText = SettingValue

                        Exit For
                    End If
                Next
            Else
                CreateSettingValue(oDoc, SettingName, SettingValue, sConfigFile)
            End If

            oDoc.Save(sConfigFile)

            Return True
        Catch
            MsgBox(Err.Description)
            Return False
        End Try
    End Function

    Private Function CreateSettingValue(ByVal oDoc As Xml.XmlDocument, ByVal SettingName As String, ByVal SettingValue As Object, _
    ByVal sconfigfile As String) As Boolean

        Dim NewNode As XmlNode

        'create a new setting node
        NewNode = oDoc.CreateElement("setting")

        Dim NewAttr As XmlAttribute

        'add the name attribute
        NewAttr = oDoc.CreateAttribute("name")

        'give the attribute a value
        NewAttr.Value = SettingName

        NewNode.Attributes.Append(NewAttr)

        'give the setting its value
        Dim ValueNode As XmlNode

        ValueNode = oDoc.CreateElement("value")

        'add the value node to the setting node
        NewNode.AppendChild(ValueNode)

        'add the value of the setting
        ValueNode.AppendChild(oDoc.CreateTextNode(SettingValue))

        Dim ParentNode As XmlNodeList

        ParentNode = oDoc.GetElementsByTagName("settings")

        For Each oNode As XmlNode In ParentNode
            oNode.AppendChild(NewNode)
        Next

        oDoc.Save(sConfigFile)

    End Function

    Private Function EncryptString(ByVal AString As String) As String
        If AString = String.Empty Then
            Return AString
        Else
            Dim encryptedData() As Byte
            Dim dataStream As MemoryStream

            Dim encryptor As ICryptoTransform
            encryptor = mProvider.CreateEncryptor()

            Try
                dataStream = New MemoryStream

                Dim encryptedStream As CryptoStream
                Try
                    'Create the encrypted stream
                    encryptedStream = New CryptoStream(dataStream, encryptor, CryptoStreamMode.Write)

                    Dim theWriter As StreamWriter
                    Try
                        'Write the string to memory via the encryption algorithm
                        theWriter = New StreamWriter(encryptedStream)
                        'Write the string to the memory stream
                        theWriter.Write(AString)

                        'End the writing
                        theWriter.Flush()
                        encryptedStream.FlushFinalBlock()

                        'Position back at start
                        dataStream.Position = 0

                        'Create area for data
                        ReDim encryptedData(CInt(dataStream.Length))

                        'Read data from memory
                        dataStream.Read(encryptedData, 0, CInt(dataStream.Length))

                        'Convert to String
                        Return Convert.ToBase64String(encryptedData, 0, encryptedData.Length)
                    Finally
                        theWriter.Close()
                    End Try
                Finally
                    encryptedStream.Close()
                End Try
            Finally
                dataStream.Close()
            End Try
        End If
    End Function
    Private Function DecryptString(ByVal AString As String) As String
        If AString = String.Empty Then
            Return AString
        Else
            Dim encryptedData() As Byte
            Dim dataStream As MemoryStream
            Dim encryptedStream As CryptoStream
            Dim strLen As Integer

            'Get the byte data
            encryptedData = Convert.FromBase64String(AString)

            Try
                dataStream = New MemoryStream
                Try
                    'Create decryptor and stream
                    Dim decryptor As ICryptoTransform
                    decryptor = mProvider.CreateDecryptor()
                    encryptedStream = New CryptoStream(dataStream, decryptor, CryptoStreamMode.Write)

                    'Write the decrypted data to the memory stream
                    encryptedStream.Write(encryptedData, 0, encryptedData.Length - 1)
                    encryptedStream.FlushFinalBlock()

                    'Position back at start
                    dataStream.Position = 0

                    'Determine length of decrypted string
                    strLen = CInt(dataStream.Length)

                    'Create area for data
                    ReDim encryptedData(strLen - 1)

                    'Read decrypted data to byte()
                    dataStream.Read(encryptedData, 0, strLen)

                    'Construct string from byte()
                    Dim retStr As String

                    Dim i As Integer
                    For i = 0 To strLen - 1
                        retStr += Chr(encryptedData(i))
                    Next

                    'Return result
                    Return retStr
                Finally
                    encryptedStream.Close()
                End Try
            Finally
                dataStream.Close()
            End Try
        End If
    End Function

    Private Function CreateSettingsFile(ByVal sConfigFile As String) As Boolean
        Dim sInit As String

        sInit = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "utf-8" & Chr(34) & " ?>"

        sInit &= Environment.NewLine & "<settings>"
        sInit &= Environment.NewLine & "</settings>"

        Dim oWriter As StreamWriter

        oWriter = New StreamWriter(sConfigFile)

        oWriter.Write(sInit)

        oWriter.Close()
    End Function

    
End Class
