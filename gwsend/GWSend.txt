Description

GWSend originally was written as a Delphi sample code that illustrated how to send messages using the basic Groupwise options, e.g. filling the recipient list, adding attachments, specifying
user names, etc.
The Delphi sample code (not up to the current release) can be downloaded from the Novell Developer web site (search for "DGWCMDLN")

Since the code creates a Win32 console application, the sample can also be used as a tool to send GW messages from the command line or from batch files.



Prerequisites
To run the application, you must have the Novell GroupWise client software installed - some features require GW v6.5.


Version history

2000-11   Initial version

2002-11   Additional features (parameters: login name & password)

2004-02   Added features
          * allow for recipients external to the GW address book
          * allow for multiple attachments/recipients
            separate multiple objects by ";"
          * allow placeholder "Self" as recipient
          * added command line option "/Priority"

2004-12   v1.31 - Added features
	        * send appointments

2005-10   v1.40 - Added numerous features
          * a variety of task/appointment options
          * delayed delivery
          * notification options

2005-10   v1.50 - Added numerous features
          * optional USER parameter for multi-login - lets you to login to multiple accounts
          * optional IPA parameter to specify the IP address of your GW server
          * optional IPP parameter to specify the IP port of your GW server
          * optional PROMPT parameter to determine GW behavior if Login is not successful
          * optional PROXY parameter to send mail with proxy accounts
          * attachment parameter may include wildcards




Syntax:  GWSend /T[o]=<text>  [Optional Parameters]



Standard Options:
  /T[o]=<rcp>             Message recipients          (separated by ";")
  /C[c]=<rcp>             Copy these recipients       (separated by ";")
  /B[c]=<rcp>             Blind Copy these recipients (separated by ";")
  /S[ubject]=<text>       Specify Subject
  /M[essage]=<text>       Specify Message (one line) - see also '/FileMsg'

Appointment Options:
  /D[ate]=<date time>     Appointment: Start
  /E[nd]=<[date ]time>    Appointment: End       (alternative to "/Length")
  /L[ength]=<time>        Appointment: Duration  (alternative to "/End")
  /Pl[ace]=<text>         Appointment: Location

Task Options:
  /D[ate]=<date>          Task: Start date
  /Due=<date>             Task: Due date
  /TaskPr[iority]=1..100  Task: Priority

Additional Options:
  /A[ttach]=<file>        Attach files (separated by ";")
  /Prom[pt]=Y[es]|N[o}    Display login dialog if neccessary: yes/no (default: yes)
  /U[ser]=<text>          Specify GW User Name
  /Pa[ssword]=<text>      Specify GW User Password
  /Prox[y]=<text>         Specify GW Proxy ID
  /IPA=<text>             Specify IP address to Post Office
  /IPP=<text>             Specify Port of Post Office
  /Prio[rity]=Hi|Lo       Specify Message Priority
  /Del[ivery]=<date>      Deliver on specified date
  /Fr[omText]=<text>      Modify text in the 'From' field
  /G[roupmembers]         Resolve Group and send to members
  /Fi[leMsg]=<file>       Specify text file containing message
                            (the text file may contain placeholders &&0..&&9)
  /Vx=<text>              Specify Variable /V0 .. /V9 for mail merge
  /Priv[ate]              Private message (invisible for proxy)
  /ReplyR[equested]       Reply Requested
  /NotifyWhenOpened       Notify Option
  /NotifyWhenDeleted      Notify Option
  /NotifyWhenAccepted     Notify Option (appt/task)
  /NotifyWhenDeclined     Notify Option (appt/task)
  /NotifyWhenCompleted    Notify Option (task)
  /Notify                 All Notify Options


Date/time format for tasks/appointments:
  * Date and time parameters must use the current local date/time format.
    In the US, this is commonly MM/DD/YY HH:MM:SS format.
  * Specifying AM or PM as part of the time is optional, as are the seconds.
  * Use 24-hour time (7:45 PM is entered as 19:45) if AM/PM is not specified.
  * Appointment "End" and "Length" parameters are mutually exclusive.
  * Appointment "End" parameter may or may not include a date.
  * Your Local Date Format: "dd.MM.yyyy hh:mm:ss"
  * Your Local Time Format: "hh:mm:ss"/"hh:mm"
  * Alternatively, these identifiers may be used:
       NOW, NOON, TONIGHT, TOMORROW, YESTERDAY, NEXTWEEK, NEXTMONTH, NEXTYEAR

If any parameter contains blanks, include it in double quotes


Examples:
  GWSend /T="John Doe" /Bc=self /Pri=Hi /Subj=BlahBlah /Att=c:\this.doc;d:\temp\*.txt
  GWSend /T=Someone@here.com;someone@there.com /S="Some Letter" /M="Hey Jude!"
  GWSend /To=JDoe@novell.com /S="Bulk Mail" /F="F:\templ.txt" /V1=xyz /V2="a b"
  GWSend /T="Sue Doe" /S=Meet /D="29.10.2005 16:00:00" /E="17:30:00" /Pl=Here
  GWSend /T="SDoe"    /S=Meet /D="29.10.2005 16:00:00" /L="01:30:00"
  GWSend /T=Marketing /S=Act /D=tomorrow /Due=nextweek /Notify /Prompt=YES
  GWSend /T=MyTeam    /S="Happy New Year" /ipa=199.99.99.99 /ipp=1677 /Deli="31.12.2006"






Notes:

In rare cases, dependent on the GW configuration, target addresses cannot be resolved.
Try one of the alternative input format for the /To, /Cc, /Bc name format - e.g.:
  /To=JDoe            (Short Name)
  /To="John Doe"      (Display Name)
  /To=JDoe@acme.org   (Full Name)
  /To=JDoe.gwacme     (Short Name with GW domain)


