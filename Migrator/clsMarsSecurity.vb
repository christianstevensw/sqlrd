Imports System
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography


Public Class clsMarsSecurity
    Private key() As Byte = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24}
    Private iv() As Byte = {65, 110, 68, 26, 69, 178, 200, 219}

    Declare Function LogonUserA Lib "advapi32.dll" (ByVal lpszUsername As String, _
                        ByVal lpszDomain As String, _
                        ByVal lpszPassword As String, _
                        ByVal dwLogonType As Integer, _
                        ByVal dwLogonProvider As Integer, _
                        ByRef phToken As IntPtr) As Integer

    Const LOGON32_LOGON_INTERACTIVE As Long = 2
    Const LOGON32_LOGON_NETWORK As Long = 3
    Const LOGON32_PROVIDER_DEFAULT As Long = 0
    Const LOGON32_PROVIDER_WINNT50 As Long = 3
    Const LOGON32_PROVIDER_WINNT40 As Long = 2
    Const LOGON32_PROVIDER_WINNT35 As Long = 1


    Public Function _VerifyLogin(ByVal sUser As String, ByVal sPassword As String, ByVal sDomain As String) As Boolean
        Dim lReturn As Long
        Dim lToken As IntPtr = IntPtr.Zero

        lReturn = LogonUserA(sUser, sDomain, sPassword, _
                LOGON32_LOGON_INTERACTIVE, _
                LOGON32_PROVIDER_DEFAULT, lToken)

        If lReturn = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    Public Function _Encrypt(ByVal plainText As String) As Byte()
        ' Declare a UTF8Encoding object so we may use the GetByte 
        ' method to transform the plainText into a Byte array. 
        Dim utf8encoder As UTF8Encoding = New UTF8Encoding
        Dim inputInBytes() As Byte = utf8encoder.GetBytes(plainText)

        ' Create a new TripleDES service provider

        Dim tdesProvider As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider


        ' The ICryptTransform interface uses the TripleDES 
        ' crypt provider along with encryption key and init vector 
        ' information 
        Dim cryptoTransform As ICryptoTransform = tdesProvider.CreateEncryptor(Me.key, Me.iv)

        ' All cryptographic functions need a stream to output the 
        ' encrypted information. Here we declare a memory stream 
        ' for this purpose. 
        Dim encryptedStream As MemoryStream = New MemoryStream

        Dim cryptStream As CryptoStream = New CryptoStream(encryptedStream, cryptoTransform, CryptoStreamMode.Write)


        ' Write the encrypted information to the stream. Flush the information 
        ' when done to ensure everything is out of the buffer. 
        cryptStream.Write(inputInBytes, 0, inputInBytes.Length)
        cryptStream.FlushFinalBlock()
        encryptedStream.Position = 0

        ' Read the stream back into a Byte array and return it to the calling 
        ' method. 
        Dim result(encryptedStream.Length - 1) As Byte
        encryptedStream.Read(result, 0, encryptedStream.Length)
        cryptStream.Close()
        Return result
    End Function
    Public Function _Decrypt(ByVal inputInBytes() As Byte) As String
        ' UTFEncoding is used to transform the decrypted Byte Array 
        ' information back into a string. 
        Dim utf8encoder As UTF8Encoding = New UTF8Encoding

        Dim tdesProvider As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider

        ' As before we must provide the encryption/decryption key along with 
        ' the init vector. 
        Dim cryptoTransform As ICryptoTransform = tdesProvider.CreateDecryptor(Me.key, Me.iv)

        ' Provide a memory stream to decrypt information into 
        Dim decryptedStream As MemoryStream = New MemoryStream
        Dim cryptStream As CryptoStream = New CryptoStream(decryptedStream, cryptoTransform, CryptoStreamMode.Write)
        cryptStream.Write(inputInBytes, 0, inputInBytes.Length)
        cryptStream.FlushFinalBlock()
        decryptedStream.Position = 0

        ' Read the memory stream and convert it back into a string 
        Dim result(decryptedStream.Length - 1) As Byte
        decryptedStream.Read(result, 0, decryptedStream.Length)
        cryptStream.Close()
        Dim myutf As UTF8Encoding = New UTF8Encoding
        Return myutf.GetString(result)
    End Function

    

    Public Function iEnc(ByVal PlainText As String, ByVal WatchWord As String)
        Dim encFac As Long
        Dim curVal As Long
        Dim I As Integer

        For I = 1 To Len(WatchWord)
            encFac = encFac + Asc(Mid(WatchWord, I, 1))
        Next

        For I = 1 To Len(PlainText)
            curVal = curVal + Asc(Mid(PlainText, I, 1)) * encFac
        Next

        iEnc = curVal
    End Function

    Public Function _CheckLicense(ByVal sLicense As String, ByVal sCompany As String, ByVal sUser As String) As Boolean
        Dim RegNo(2) As String

        RegNo(0) = iEnc(sUser & sCompany, "mchungula")
        RegNo(1) = iEnc(sUser & "blantyre" & sCompany, "chookoo")
        RegNo(2) = iEnc(sUser & sCompany, "tanika")

        Dim FullReg As String = RegNo(0) & "-" & RegNo(1) & "-" & RegNo(2)

        If FullReg = sLicense Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function _CreateLicense(ByVal sCompany As String, _
    ByVal sUser As String) As String
        Dim RegNo(2) As String

        RegNo(0) = iEnc(sUser & sCompany, "mchungula")
        RegNo(1) = iEnc(sUser & "blantyre" & sCompany, "chookoo")
        RegNo(2) = iEnc(sUser & sCompany, "tanika")

        Dim FullReg As String = RegNo(0) & "-" & RegNo(1) & "-" & RegNo(2)

        Return FullReg
    End Function

End Class
