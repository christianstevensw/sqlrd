Imports System.Threading
Public Class Form1
    Inherits System.Windows.Forms.Form
    Dim oT As Thread
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtcrd4 As System.Windows.Forms.TextBox
    Friend WithEvents cmdCrd4 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmdcrd5 As System.Windows.Forms.Button
    Friend WithEvents txtcrd5 As System.Windows.Forms.TextBox
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents cmdStart As System.Windows.Forms.Button
    Friend WithEvents lsvProgress As System.Windows.Forms.ListBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Form1))
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtcrd4 = New System.Windows.Forms.TextBox
        Me.cmdCrd4 = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.cmdcrd5 = New System.Windows.Forms.Button
        Me.txtcrd5 = New System.Windows.Forms.TextBox
        Me.ofd = New System.Windows.Forms.OpenFileDialog
        Me.cmdStart = New System.Windows.Forms.Button
        Me.lsvProgress = New System.Windows.Forms.ListBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label4 = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.ForeColor = System.Drawing.Color.Navy
        Me.Label1.Location = New System.Drawing.Point(8, 72)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(320, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Please select the CRD4 system file (reportdesklive.dat)"
        '
        'txtcrd4
        '
        Me.txtcrd4.BackColor = System.Drawing.Color.White
        Me.txtcrd4.ForeColor = System.Drawing.Color.FromArgb(CType(0, Byte), CType(0, Byte), CType(192, Byte))
        Me.txtcrd4.Location = New System.Drawing.Point(8, 88)
        Me.txtcrd4.Name = "txtcrd4"
        Me.txtcrd4.ReadOnly = True
        Me.txtcrd4.Size = New System.Drawing.Size(312, 21)
        Me.txtcrd4.TabIndex = 1
        Me.txtcrd4.Text = ""
        '
        'cmdCrd4
        '
        Me.cmdCrd4.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCrd4.Image = CType(resources.GetObject("cmdCrd4.Image"), System.Drawing.Image)
        Me.cmdCrd4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCrd4.Location = New System.Drawing.Point(328, 88)
        Me.cmdCrd4.Name = "cmdCrd4"
        Me.cmdCrd4.Size = New System.Drawing.Size(40, 21)
        Me.cmdCrd4.TabIndex = 2
        Me.cmdCrd4.Text = "..."
        Me.cmdCrd4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.ForeColor = System.Drawing.Color.Navy
        Me.Label2.Location = New System.Drawing.Point(8, 120)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(272, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Please select the CRD5 system file (crdlive.dat)"
        '
        'cmdcrd5
        '
        Me.cmdcrd5.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdcrd5.Image = CType(resources.GetObject("cmdcrd5.Image"), System.Drawing.Image)
        Me.cmdcrd5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdcrd5.Location = New System.Drawing.Point(328, 136)
        Me.cmdcrd5.Name = "cmdcrd5"
        Me.cmdcrd5.Size = New System.Drawing.Size(40, 21)
        Me.cmdcrd5.TabIndex = 2
        Me.cmdcrd5.Text = "..."
        Me.cmdcrd5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtcrd5
        '
        Me.txtcrd5.BackColor = System.Drawing.Color.White
        Me.txtcrd5.ForeColor = System.Drawing.Color.FromArgb(CType(0, Byte), CType(0, Byte), CType(192, Byte))
        Me.txtcrd5.Location = New System.Drawing.Point(8, 136)
        Me.txtcrd5.Name = "txtcrd5"
        Me.txtcrd5.ReadOnly = True
        Me.txtcrd5.Size = New System.Drawing.Size(312, 21)
        Me.txtcrd5.TabIndex = 1
        Me.txtcrd5.Text = ""
        '
        'cmdStart
        '
        Me.cmdStart.Enabled = False
        Me.cmdStart.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdStart.Image = CType(resources.GetObject("cmdStart.Image"), System.Drawing.Image)
        Me.cmdStart.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdStart.Location = New System.Drawing.Point(152, 184)
        Me.cmdStart.Name = "cmdStart"
        Me.cmdStart.TabIndex = 3
        Me.cmdStart.Text = "Start"
        '
        'lsvProgress
        '
        Me.lsvProgress.ForeColor = System.Drawing.Color.Blue
        Me.lsvProgress.Location = New System.Drawing.Point(8, 240)
        Me.lsvProgress.Name = "lsvProgress"
        Me.lsvProgress.Size = New System.Drawing.Size(360, 108)
        Me.lsvProgress.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(8, 224)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 16)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Progress"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(384, 64)
        Me.Panel1.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Navy
        Me.Label4.Location = New System.Drawing.Point(8, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(232, 32)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Migration Wizard"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(312, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(56, 50)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(-16, 60)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(408, 8)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Location = New System.Drawing.Point(-8, 168)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(392, 8)
        Me.GroupBox2.TabIndex = 8
        Me.GroupBox2.TabStop = False
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(8, 352)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(360, 16)
        Me.ProgressBar1.TabIndex = 9
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(378, 211)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lsvProgress)
        Me.Controls.Add(Me.cmdStart)
        Me.Controls.Add(Me.cmdCrd4)
        Me.Controls.Add(Me.txtcrd4)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cmdcrd5)
        Me.Controls.Add(Me.txtcrd5)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CRD Migration Wizard"
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub cmdCrd4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCrd4.Click
        With ofd
            .FileName = ""
            .Title = "Please select the CRD4 system file ('reportdesklive.dat')"
            .Filter = "ReportDeskLive.dat|ReportDeskLive.dat"
            .ShowDialog()

            If .FileName.Length = 0 Then
                Return
            ElseIf .FileName.ToLower.IndexOf("reportdesklive.dat") = -1 Then
                MessageBox.Show("Invalid file selection, please try again", Application.ProductName, _
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If

            txtcrd4.Text = .FileName

        End With
    End Sub

    Private Sub cmdcrd5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdcrd5.Click
        With ofd
            .FileName = ""
            .Title = "Please select the CRD5 system file ('crdlive.dat')"
            .Filter = "CRDLive.dat|CRDLive.dat"
            .ShowDialog()

            If .FileName.Length = 0 Then
                Return
            ElseIf .FileName.ToLower.IndexOf("crdlive.dat") = -1 Then
                MessageBox.Show("Invalid file selection, please try again", Application.ProductName, _
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If

            txtcrd5.Text = .FileName

        End With
    End Sub

    Private Sub txtcrd4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtcrd4.TextChanged
        If txtcrd4.Text.Length = 0 Or txtcrd5.Text.Length = 0 Then
            cmdStart.Enabled = False
        Else
            cmdStart.Enabled = True
        End If
    End Sub

    Private Sub txtcrd5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtcrd5.TextChanged
        If txtcrd4.Text.Length = 0 Or txtcrd5.Text.Length = 0 Then
            cmdStart.Enabled = False
        Else
            cmdStart.Enabled = True
        End If
    End Sub

    Private Sub cmdStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdStart.Click

        If cmdStart.Text.ToLower = "start" Then

            Me.Height = 408

            cmdStart.Text = "Stop"

            Dim o As New clsMain

            o._CheckVersion(txtcrd4.Text)

            o.crd4path = txtcrd4.Text
            o.crd5path = txtcrd5.Text
            o._Connect()
            o.lsv = lsvProgress
            o.pb = ProgressBar1
            o.o = Me

            'oT = New Thread(AddressOf o._Action)

            'oT.Name = "_Action"

            'oT.Start()

            o._Action()

        Else
            If MessageBox.Show("Cancel the current operation?", Application.ProductName, _
            MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                Try
                    oT.Abort()
                    ProgressBar1.Value = 0
                    Me.Text = "CRD Migration Wizard"
                    lsvProgress.Items.Add("Operation cancelled by user...")
                Catch : End Try

                cmdStart.Text = "Start"
            End If
        End If

    End Sub

    Private Sub lsvProgress_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvProgress.SelectedIndexChanged

    End Sub
End Class
'to migrate from crd4 to 5, we will need to move the following
'- single schedules - check
'- packages - check
'- dynamic schedules
'- event based schedules
'- automation schedules
'- options
'- address book entries -
'- user calendars
'- user formulas
'- i think thats
