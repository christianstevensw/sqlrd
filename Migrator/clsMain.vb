Imports Microsoft.Win32
Imports System.Threading
Public Class clsMain
    Public o As Form1
    Public lsv As ListBox
    Public crd4path As String
    Public crd5path As String
    Public ocrd4 As ADODB.Connection
    Public ocrd5 As ADODB.Connection
    Public pb As ProgressBar
    Public gID As Integer = 0

    Shared Sub Main()
        Application.EnableVisualStyles()
        Application.DoEvents()

        Dim sMsg As String

        sMsg = "IMPORTANT MESSAGE:" & vbCrLf & vbCrLf & _
        "This Migration Wizard has been developed to ease migration of schedules from CRD 4 to CRD 5. " & _
        "However, due to design changes between the two versions, not all properties required for CRD 5 " & _
        "can be populated by the Migration Wizard." & vbCrLf & _
        "You are strongly advised to check all properties of migrated schedules and to test them " & _
        "thoroughly before going 'live'. In some rare cases you may need to delete and write them afresh." & vbCrLf & _
        "Please ensure that all CRD 5 scheduling services are switched off before running the Migration Wizard." & _
        "Do not turn them on again until you have fully manually tested the imported schedules."

        MessageBox.Show(sMsg, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        Dim oo As New clsMain

        oo.o = New Form1

        Application.Run(oo.o)

        MessageBox.Show(sMsg, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Public Sub _UpdateFolderID()
        Dim oRs As New ADODB.Recordset
        Dim SQL As String
        Dim oRs1 As New ADODB.Recordset
        Dim I As Integer
        Dim nCount As Integer

        'ocrd4.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & crd4path & ";Persist Security Info=False")

        'ocrd5.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & crd5path & ";Persist Security Info=False")

        'can't copy all as it might create duplicate folderid
        'so we have to check the folder table for existing duplicates
        'and then change these to another value
        SQL = "SELECT * FROM Folders"

        oRs.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

        nCount = oRs.RecordCount

        Do While oRs.EOF = False

            Dim nFolderID As Integer = oRs("folderid").Value

            SQL = "SELECT * FROM Folders WHERE FolderID =" & oRs("folderid").Value

            oRs1.Open(SQL, ocrd5, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

            If oRs1.EOF = False Then 'we have a duplicate
                Dim nTemp As Integer = _CreateDataID()

                With ocrd5
                    .Execute("UPDATE Folders SET FolderID =" & nTemp & " WHERE " & _
                    "FolderID =" & nFolderID)
                    .Execute("UPDATE Folders SET Parent =" & nTemp & " WHERE " & _
                    "Parent =" & nFolderID)
                    .Execute("UPDATE ReportAttr SET Parent =" & nTemp & " WHERE " & _
                    "Parent =" & nFolderID)
                    .Execute("UPDATE PackageAttr SET Parent =" & nTemp & " WHERE " & _
                    "Parent =" & nFolderID)
                    .Execute("UPDATE AutomationAttr SET Parent =" & nTemp & " WHERE " & _
                    "Parent =" & nFolderID)
                    .Execute("UPDATE EventAttr SET Parent =" & nTemp & " WHERE " & _
                    "Parent =" & nFolderID)
                End With
            End If

            oRs1.Close()

            oRs.MoveNext()
        Loop

        oRs.Close()

        'ocrd4.Close()

        'ocrd5.Close()
    End Sub

    Public Function _CreateMainFolder() As Integer
        Dim SQL As String
        Dim oRs As New ADODB.Recordset
        Dim nFolderID As Integer

        'oCon.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & crd5path & ";Persist Security Info=False")

        SQL = "SELECT Folderid FROM Folders WHERE FolderName ='Imported From CRD4'"

        oRs.Open(SQL, ocrd5, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

        If oRs.EOF = False Then
            nFolderID = oRs.Fields(0).Value
            oRs.Close()
        Else
            oRs.Close()

            nFolderID = _CreateDataID()

            SQL = "INSERT INTO Folders (FolderID,FolderName,Parent) VALUES (" & _
            nFolderID & "," & _
            "'Imported From CRD4',0)"

            ocrd5.Execute(SQL)

            'ocrd5.Close()
        End If

        Return nFolderID
    End Function
    Public Sub _MoveFolders()
        Dim oRs As New ADODB.Recordset
        'Dim ocrd4 As New ADODB.Connection
        'Dim ocrd5 As New ADODB.Connection
        Dim nReportID As Integer
        Dim nScheduleID As Integer
        Dim DestID As Integer
        Dim I As Integer
        Dim nCount As Integer
        Dim nMaster As Integer

        'check to see if an id exists in the current db
        'update the ones that exist to something else
        'update all schedules to new parent
        'copy the new folder structures in

        Try
            Dim SQL As String
            Dim sCols As String
            Dim sVals As String
            Dim Dynamic As Integer
            Dim LoginReq As Integer
            Dim UseData As Integer
            Dim sTitle As String
            Dim oRs1 As New ADODB.Recordset
            Dim sFolder As String
            Dim nFolderID As Integer
            Dim nTemp As Integer

            lsv.Items.Clear()

            _Status("Importing directory structure...")

            Application.DoEvents()

            nMaster = _CreateMainFolder()

            'ocrd4.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & crd4path & ";Persist Security Info=False")

            'ocrd5.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & crd5path & ";Persist Security Info=False")

            'check to see if folders have been imported already
            SQL = "SELECT * FROM Folders WHERE Parent =" & nMaster

            oRs.Open(SQL, ocrd5, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

            If oRs.EOF = False Then
                oRs.Close()
                Return
            End If

            oRs.Close()

            SQL = "SELECT * FROM Folders"

            oRs.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

            nCount = oRs.RecordCount

            _UpdateFolderID()

            Do While oRs.EOF = False
                I += 1
                pb.Value = (I / nCount) * 100

                o.Text = "Folder " & I & " of " & nCount

                lsv.Items.Clear()

                _Status("Importing folder structure....")

                sFolder = oRs("foldername").Value
                nFolderID = oRs("parent").Value

                If nFolderID = 0 Then nFolderID = nMaster

                'If sFolder.Length > 49 Then
                '    Try
                '        sFolder = sFolder.Substring(0, 49)
                '    Catch
                '        sFolder = Microsoft.VisualBasic.Left(sFolder, 49)
                '    End Try
                'End If

                'lets create the folder now
                sCols = "FolderID,FolderName,Parent"

                sVals = oRs("folderid").Value & "," & _
                "'" & SQLPrepare(sFolder) & "'," & _
                nFolderID

                SQL = "INSERT INTO Folders (" & sCols & ") VALUES (" & sVals & ")"

                Try : ocrd5.Execute(SQL) : Catch : End Try

                oRs.MoveNext()

            Loop

            oRs.Close()

            'ocrd4.Close()

            'ocrd5.Close()
        Catch ex As Exception

        End Try
    End Sub
    Public Function _GetFolderID(ByVal sFolderName As String) As Integer
        Dim oRs As New ADODB.Recordset

        Dim SQL As String

        'oCon.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & crd5path & ";Persist Security Info=False")

        SQL = "SELECT FolderID FROM Folders WHERE FolderName = '" & SQLPrepare(sFolderName) & "' AND " & _
        "FolderID = (SELECT MIN(FolderID) FROM Folders WHERE FolderName ='" & SQLPrepare(sFolderName) & "')"

        Try
            oRs.Open(SQL, ocrd5, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)
        Catch
            _Connect()
            oRs.Open(SQL, ocrd5, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)
        End Try

        If oRs.EOF = False Then
            Return oRs.Fields(0).Value
        Else
            Dim nID As Integer
            nID = _CreateDataID()

            SQL = "INSERT INTO Folders(FolderID,FolderName,Parent) VALUES (" & _
            nID & "," & _
            "'" & SQLPrepare(sFolderName) & "',0)"

            ocrd5.Execute(SQL)

            Return nID
        End If

        oRs.Close()

        'oCon.Close()
    End Function
    Public Sub _Status(ByVal sIn As String)
        On Error Resume Next
        lsv.Items.Add(sIn)
        Application.DoEvents()

        lsv.TopIndex = lsv.Items.Count - 1
    End Sub
    Public Sub _MovePackages(ByVal crd4path As String, ByVal crd5path As String, ByVal lsv As ListBox)
        Dim oRs As New ADODB.Recordset

        Dim nReportID As Integer
        Dim nScheduleID As Integer
        Dim nPackID As Integer
        Dim DestID As Integer
        Dim oRs1 As New ADODB.Recordset
        Dim oRs2 As New ADODB.Recordset
        Dim I As Integer
        Dim nCount As Integer

        Try
            Dim SQL As String
            Dim sCols As String
            Dim sVals As String
            Dim Dynamic As Integer
            Dim LoginReq As Integer
            Dim UseData As Integer
            Dim sTitle As String
            Dim Cache4 As String
            Dim Cache5 As String
            Dim NextRun As String

            Cache4 = crd4path.ToLower.Replace("reportdesklive.dat", "Cache\")

            Cache5 = crd5path.ToLower.Replace("crdlive.dat", "Cache\")

            lsv.Items.Clear()

            _Status("Importing Packages...")


            Dim Parent As Integer

            'ocrd4.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & crd4path & ";Persist Security Info=False")

            'ocrd5.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & crd5path & ";Persist Security Info=False")

            SQL = "SELECT * FROM ReportBatch"

            oRs.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

            nCount = oRs.RecordCount

            Do While oRs.EOF = False

                Dim sPackage As String = oRs("batchname").Value

                'If sPackage.Length > 49 Then

                '    Try
                '        sPackage = sPackage.Substring(0, 49)
                '    Catch
                '        sPackage = Microsoft.VisualBasic.Left(sPackage, 49)
                '    End Try
                'End If

                I += 1

                o.Text = "Package " & I & " of " & nCount

                pb.Value = (I / nCount) * 100
                _Status("Importing package '" & oRs("batchname").Value & "'...")


                Parent = _GetFolderID(oRs("parent").Value)

                'for packageattr------------------------------------------------

                sCols = "PackID,PackageName,Parent,Retry,AssumeFail,CheckBlank,Owner," & _
                "FailOnOne,MergePDF,MergeXL,MergePDFName,MergeXLName"

                nPackID = _CreateDataID()

                sVals = nPackID & "," & _
                "'" & SQLPrepare(sPackage) & "'," & _
                Parent & "," & _
                oRs("retry").Value & "," & _
                "0,0,'sys',0,0,0,'',''"

                SQL = "INSERT INTO PackageAttr(" & sCols & ") VALUES (" & sVals & ")"

                ocrd5.Execute(SQL)

                _Status("Importing schedule...")
                Application.DoEvents()

                'for scheduleattr-----------------------------------------------
                Dim Repeat As Integer
                Dim Status As Integer
                Dim RepeatInt As Integer
                Dim nUntil As Date
                Dim nDisable As Date

                Try
                    Int32.Parse(oRs("repeat").Value)
                    Repeat = 1
                    RepeatInt = oRs("repeat").Value
                Catch ex As Exception
                    Repeat = 0
                    RepeatInt = 0
                End Try

                Try
                    nUntil = oRs("until").Value
                Catch ex As Exception
                    nUntil = "00:00"
                End Try

                Try
                    nDisable = oRs("disabledate").Value
                Catch ex As Exception
                    nDisable = Now.Date
                End Try

                If oRs("status").Value = "T" Then
                    Status = 1
                Else
                    Status = 0
                End If

                Try
                    NextRun = oRs("nextrundate").Value & " " & oRs("runtime").Value
                Catch ex As Exception
                    NextRun = oRs("nextrundate").Value
                End Try

                nScheduleID = _CreateDataID()

                sCols = "scheduleid,frequency,startdate,enddate,nextrun,starttime," & _
                "repeat,repeatinterval,repeatuntil," & _
                "status,reportid,packid,autoid,description,keyword,calendarname,disableddate"

                sVals = nScheduleID & "," & _
                "'" & SQLPrepare(oRs("frequency").Value) & "'," & _
                "'" & oRs("startdate").Value & "'," & _
                "'" & oRs("enddate").Value & "'," & _
                "'" & NextRun & "'," & _
                "'" & oRs("starttime").Value & "'," & _
                Repeat & "," & _
                RepeatInt & "," & _
                "'" & nUntil & "'," & _
                Status & "," & _
                "0," & _
                nPackID & ",0," & _
                "'" & SQLPrepare(oRs("scheduledesc").Value) & "'," & _
                "''," & _
                "'" & SQLPrepare(oRs("calendarname").Value) & "'," & _
                "'" & nDisable & "'"

                SQL = "INSERT INTO ScheduleAttr (" & sCols & ") VALUES (" & sVals & ")"

                Console.WriteLine(SQL)

                ocrd5.Execute(SQL)

                _MoveTasks("Package", oRs(0).Value, nScheduleID, lsv)

                SQL = "SELECT * FROM ScheduleDetail WHERE PackID = " & oRs.Fields(0).Value

                oRs1.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                Do While oRs1.EOF = False
                    Dim nOptId As Integer = _CreateDataID()

                    sCols = "optionid,weekno,dayno,monthsin,ncount,scheduleid,scheduletype"

                    sVals = nOptId & "," & _
                    oRs1("weekno").Value & "," & _
                    oRs1("dayno").Value & "," & _
                    "'" & oRs1("monthsin").Value & "," & _
                    "0," & _
                    nScheduleID & "," & _
                    "'Monthly'"

                    SQL = "INSERT INTO ScheduleOptions (" & sCols & ") VALUES (" & sVals & ")"

                    ocrd5.Execute(SQL)

                    oRs1.MoveNext()
                Loop

                oRs1.Close()

                'for destinationattr---------------------------------------------

                _Status("Importing destination...")
                Application.DoEvents()

                DestID = _CreateDataID()

                Dim nStamp As Integer
                Dim Zip As Integer
                Dim Embed As Integer


                nStamp = 0

                If oRs("collate").Value = "Z" Then
                    Zip = 1
                Else
                    Zip = 0
                End If

                Embed = 0

                sCols = "destinationid,destinationtype,sendto,cc,bcc,subject,message,extras," & _
                "outputpath, reportid, packid, " & _
                "ftpserver,ftpusername,ftppassword,ftppath,destinationname,outputformat," & _
                "customext,appenddatetime,datetimeformat," & _
                "customname,compress,embed,mailformat,htmlsplit,deferdelivery,deferby," & _
                "smtpserver,encryptzip,encryptziplevel,encryptzipcode,usedun,dunname,includeattach"

                sVals = DestID & "," & _
                "'" & oRs("rptdestination").Value & "'," & _
                "'" & SQLPrepare(oRs("recipients").Value) & "'," & _
                "'" & SQLPrepare(oRs("sendcc").Value) & "'," & _
                "'" & SQLPrepare(oRs("sendbcc").Value) & "'," & _
                "'" & SQLPrepare(oRs("subject").Value) & "'," & _
                "'" & SQLPrepare(oRs("message").Value) & "'," & _
                "'" & SQLPrepare(oRs("emailattachments").Value) & "'," & _
                "'" & oRs("destination").Value & "'," & _
                "0," & _
                nPackID & "," & _
                "'" & SQLPrepare(oRs("ftpserver").Value) & "'," & _
                "'" & SQLPrepare(oRs("ftpusername").Value) & "'," & _
                "'" & SQLPrepare(oRs("ftppassword").Value) & "'," & _
                "'" & SQLPrepare(oRs("ftppath").Value) & "'," & _
                "'" & SQLPrepare(sPackage) & "'," & _
                "'Package'," & _
                "'" & SQLPrepare(oRs("orientation").Value) & "'," & _
                nStamp & "," & _
                "''," & _
                "''," & _
                Zip & "," & _
                0 & "," & _
                "'TEXT'," & _
                "0,0,0,'',0,0,'',0,'',1"

                SQL = "INSERT INTO DestinationAttr (" & sCols & ") VALUES (" & sVals & ")"

                ocrd5.Execute(SQL)

                'for reportattr ------------------------------------------------------------------------ 

                _Status("Importing reports...")
                Application.DoEvents()

                SQL = "SELECT * FROM ReportBatchDtls WHERE ReportID =" & oRs.Fields(0).Value

                oRs1.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                Do While oRs1.EOF = False

                    nReportID = _CreateDataID()

                    Try
                        If SQLPrepare(oRs1("LoginReq").Value).ToLower = "t" Then
                            LoginReq = 1
                        Else
                            LoginReq = 0
                        End If
                    Catch ex As Exception
                        LoginReq = 0
                    End Try

                    Dim sFile As String
                    Dim n As Integer
                    Dim ParID As Integer

                    sTitle = oRs1("reporttitle").Value

                    If sTitle.Length > 49 Then
                        Try
                            sTitle = sTitle.Substring(0, 49)
                        Catch
                            sTitle = Microsoft.VisualBasic.Left(sTitle, 50)
                        End Try
                    End If

                    sTitle = sTitle.Replace(">", ":")

                    sFile = oRs1("cachepath").Value

                    n = sFile.Split("\").GetUpperBound(0)

                    sFile = sFile.Split("\")(n)

                    sCols = "reportid,databasepath,reportname,packid,parent," & _
                    "reporttitle,retry,assumefail," & _
                    "checkblank,selectionformula,dynamic,includeattach,owner,rptuserid," & _
                    "rptpassword,rptserver,rptdatabase," & _
                    "uselogin,usesaveddata,cachepath,lastrefreshed"

                    sVals = nReportID & "," & _
                    "'" & SQLPrepare(oRs1("reportpath").Value) & "'," & _
                    "'" & SQLPrepare(sTitle) & "'," & _
                    nPackID & "," & _
                    0 & "," & _
                    "'" & SQLPrepare(sTitle) & "'," & _
                    0 & "," & _
                    0 & "," & _
                    "0," & _
                    "'" & SQLPrepare(oRs1("selectionformula").Value) & "'," & _
                    0 & "," & _
                    "1,'sys'," & _
                    "'" & SQLPrepare(oRs1("loginid").Value) & "'," & _
                    "'" & SQLPrepare(_EncryptDBValue(oRs1("loginpassword").Value)) & "'," & _
                    "'" & SQLPrepare(oRs1("reportserver").Value) & "'," & _
                    "'" & SQLPrepare(oRs1("reportdatabase").Value) & "'," & _
                    LoginReq & "," & _
                    0 & "," & _
                    "'" & SQLPrepare(Cache5 & sFile) & "',Now()"

                    SQL = "INSERT INTO ReportAttr (" & sCols & ") VALUES (" & sVals & ")"

                    ocrd5.Execute(SQL)


                    'packged report attr-------------------------------------------------------------
                    _Status("Importing report attributes...")
                    Application.DoEvents()

                    sCols = "attrid,packid,reportid,outputformat,customext,appenddatetime,datetimeformat,customname"

                    Try
                        nStamp = oRs1("dtstamp").Value
                    Catch
                        nStamp = 0
                    End Try

                    sVals = _CreateDataID() & "," & _
                    nPackID & "," & _
                    nReportID & "," & _
                    "'" & oRs1("reportformat").Value & "'," & _
                    "'" & SQLPrepare(oRs1("customext").Value) & "'," & _
                    nStamp & "," & _
                    "'" & oRs1("dtstampformat").Value & "',''"

                    SQL = "INSERT INTO PackagedReportAttr (" & sCols & ") VALUES (" & sVals & ")"

                    ocrd5.Execute(SQL)

                    'report parameters---------------------------------
                    _Status("Importing report parameters...")
                    Application.DoEvents()

                    SQL = "SELECT * FROM ReportBatchPar WHERE ReportID =" & oRs("reportid").Value & " AND " & _
                    "DetailID = " & oRs1("detailid").Value

                    oRs2.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                    Do While oRs2.EOF = False
                        ParID = _CreateDataID()

                        sCols = "ParID,ReportID,ParName,ParValue,ParType"

                        sVals = ParID & "," & _
                        nReportID & "," & _
                        "''," & _
                        "'" & SQLPrepare(oRs2("parvalue").Value) & "'," & _
                        oRs2("partype").Value

                        SQL = "INSERT INTO ReportParameter (" & sCols & ") VALUES (" & sVals & ")"

                        ocrd5.Execute(SQL)

                        oRs2.MoveNext()
                    Loop

                    oRs2.Close()


                    Try
                        With ocrd5
                            .Execute("UPDATE packagedreportattr SET OutputFormat ='Acrobat Format (*.pdf)' WHERE OutputFormat = 'Acrobat Format (PDF)'")
                            .Execute("UPDATE packagedreportattr SET OutputFormat ='Crystal Reports (*.rpt)' WHERE OutputFormat = 'Crystal Reports'")
                            .Execute("UPDATE packagedreportattr SET OutputFormat ='CSV (*.csv)' WHERE OutputFormat = 'CSV'")
                            .Execute("UPDATE packagedreportattr SET OutputFormat ='MS Excel - Data Only (*.xls)' WHERE OutputFormat = 'MS Excel - Data Only'")
                            .Execute("UPDATE packagedreportattr SET OutputFormat ='MS Excel 7 (*.xls)' WHERE OutputFormat = 'MS Excel 7'")
                            .Execute("UPDATE packagedreportattr SET OutputFormat ='MS Excel 8 (*.xls)' WHERE OutputFormat = 'MS Excel 8'")
                            .Execute("UPDATE packagedreportattr SET OutputFormat ='MS Excel 97-2000 (*.xls)' WHERE OutputFormat = 'MS Excel 97-2000'")
                            .Execute("UPDATE packagedreportattr SET OutputFormat ='ODBC (*.odbc)' WHERE OutputFormat = 'ODBC'")
                            .Execute("UPDATE packagedreportattr SET OutputFormat ='MS Word (*.doc)' WHERE OutputFormat = 'MS Word'")
                            .Execute("UPDATE packagedreportattr SET OutputFormat ='Rich Text Format (*.rtf)' WHERE OutputFormat = 'Rich Text'")
                            .Execute("UPDATE packagedreportattr SET OutputFormat ='Tab Seperated (*.txt)' WHERE OutputFormat = 'Tab Seperated'")
                            .Execute("UPDATE packagedreportattr SET OutputFormat ='Text (*.txt)' WHERE OutputFormat = 'Text'")
                            .Execute("UPDATE packagedreportattr SET OutputFormat ='XML (*.xml)' WHERE OutputFormat = 'XML'")
                            .Execute("UPDATE packagedreportattr SET OutputFormat ='HTML (*.htm)' WHERE OutputFormat = 'HTML'")

                        End With
                    Catch : End Try

                    'printer options------------------------------------------------------------------------

                    _Status("Importing printer attributes...")
                    Application.DoEvents()

                    Dim PrinterID As Integer

                    sCols = "printerid,destinationid,printername,orientation,printerdriver," & _
                    "printerport,pagefrom,pageto,copies,[collate],pagesperreport,printmethod,papersize"

                    SQL = "SELECT * FROM SchedulePrinter WHERE ReportID = " & oRs1("detailid").Value & " AND " & _
                    "PackID = " & oRs.Fields(0).Value

                    oRs2.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                    Do While oRs2.EOF = False
                        PrinterID = _CreateDataID()

                        sVals = PrinterID & "," & _
                        DestID & "," & _
                        "'" & SQLPrepare(oRs2("printername").Value) & "'," & _
                        "'" & SQLPrepare(oRs2("orientation").Value) & "'," & _
                        "'" & SQLPrepare(oRs2("printerdriver").Value) & "'," & _
                        "'" & SQLPrepare(oRs2("printerport").Value) & "'," & _
                        oRs2("pagefrom").Value & "," & _
                        oRs2("pageto").Value & "," & _
                        oRs2("copies").Value & "," & _
                        oRs2("collate").Value & "," & _
                        oRs2("pagesperreport").Value & "'," & _
                        "'" & SQLPrepare(oRs2("printmethod").Value) & "'," & _
                        "'" & SQLPrepare(oRs2("papersize").Value) & "'"

                        SQL = "INSERT INTO PrinterAttr (" & sCols & ") VALUES (" & sVals & ")"

                        ocrd5.Execute(SQL)

                        oRs2.MoveNext()
                    Loop

                    oRs2.Close()

                    'subreports-----------------------------------------

                    _Status("Importing Subreport details...")
                    Application.DoEvents()

                    SQL = "select * from sublogin WHERE ReportID = " & oRs1("detailid").Value & " AND " & _
                    "PackID = " & oRs.Fields(0).Value

                    oRs2.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                    Do While oRs2.EOF = False
                        sCols = "subloginid,reportid,subname,rptserver,rptdatabase,rptuserid,rptpassword"

                        sVals = _CreateDataID() & "," & _
                        nReportID & "," & _
                        "'" & SQLPrepare(oRs2("subname").Value) & "'," & _
                        "'" & SQLPrepare(oRs2("sserver").Value) & "'," & _
                        "'" & SQLPrepare(oRs2("sdatabase").Value) & "'," & _
                        "'" & SQLPrepare(oRs2("suser").Value) & "'," & _
                        "'" & SQLPrepare(oRs2("spassword").Value) & "'"

                        SQL = "INSERT INTO SubReportLogin (" & sCols & ") VALUES (" & sVals & ")"

                        ocrd5.Execute(SQL)

                        oRs2.MoveNext()
                    Loop

                    oRs2.Close()

                    SQL = "SELECT * FROM SubParameters WHERE ReportID = " & oRs1("detailid").Value & " AND " & _
                    "PackID = " & oRs.Fields(0).Value

                    oRs2.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                    Do While oRs2.EOF = False

                        sCols = "subparid,reportid,subname,parname,parvalue,partype"

                        sVals = _CreateDataID() & "," & _
                        nReportID & "," & _
                        "'" & SQLPrepare(oRs2("subname").Value) & "'," & _
                        "'" & SQLPrepare(oRs2("parname").Value) & "'," & _
                        "'" & SQLPrepare(oRs2("parvalue").Value) & "'," & _
                        oRs2("partype").Value

                        SQL = "INSERT INTO SubReportParameters (" & sCols & ") VALUES (" & sVals & ")"

                        ocrd5.Execute(SQL)

                        oRs2.MoveNext()

                    Loop

                    oRs2.Close()

                    'report options -------------------------------------

                    _Status("Importing report options...")
                    Application.DoEvents()

                    sCols = "optionid,htmlmerge,pdfwatermark,pdfpassword,destinationid,reportid," & _
                    "colwidth,exporthf,pagebreak,convertdate,pagefrom,pageto,scharacter,sdelimiter,odbc," & _
                    "tablename,linesperpage,userpassword,canprint,cancopy,canedit,cannotes,canfill,canaccess," & _
                    "canassemble,canprintfull,pdfsecurity"

                    SQL = "SELECT * FROM RptOptions WHERE ReportID = " & oRs1("detailid").Value & " AND " & _
                    "PackID = " & oRs.Fields(0).Value

                    oRs2.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                    Do While oRs2.EOF = False
                        sVals = _CreateDataID() & "," & _
                        "0,'',''," & _
                        DestID & "," & _
                        nReportID & "," & _
                        "'" & oRs2("colwidth").Value & "'," & _
                        oRs2("exporthf").Value & "," & _
                        oRs2("pagebreak").Value & "," & _
                        oRs2("convertdate").Value & "," & _
                        oRs2("pagefrom").Value & "," & _
                        oRs2("pageto").Value & "," & _
                        "'" & SQLPrepare(oRs2("scharacter").Value) & "'," & _
                        "'" & SQLPrepare(oRs2("sdelimiter").Value) & "'," & _
                        "'" & SQLPrepare(oRs2("odbc").Value) & "'," & _
                        "'" & SQLPrepare(oRs2("tablename").Value) & "'," & _
                        25 & "," & _
                        "'',1,1,1,1,1,1,1,1,0"

                        SQL = "INSERT INTO ReportOptions (" & sCols & ") VALUES (" & sVals & ")"

                        ocrd5.Execute(SQL)

                        oRs2.MoveNext()
                    Loop

                    oRs2.Close()

                    _Status("Copying report to cache...")
                    Application.DoEvents()

                    Try
                        System.IO.File.Copy(oRs1("cachepath").Value, Cache5 & sFile, True)
                    Catch : End Try

                    oRs1.MoveNext()
                Loop

                oRs1.Close()

                _Status("++++++++++++++++++++++++++++++")
                Application.DoEvents()

NextPack:
                oRs.MoveNext()
            Loop

            oRs.Close()

            'ocrd4.Close()
            'ocrd5.Close()

        Catch ex As Exception

            ocrd5.Execute("DELETE FROM ReportAttr WHERE PackID = " & nPackID)
            ocrd5.Execute("DELETE FROM ScheduleAttr WHERE packID =" & nPackID)
            ocrd5.Execute("DELETE FROM DestinationAttr WHERE packID =" & nPackID)
            ocrd5.Execute("DELETE FROM ReportParameter WHERE ReportID IN (SELECT " & _
            "ReportID FROM ReportAttr WHERE PackID =" & nPackID & ")")
            ocrd5.Execute("DELETE FROM ScheduleOptions WHERE ScheduleID =" & nScheduleID)
            ocrd5.Execute("DELETE FROM PrinterAttr WHERE Destinationid =" & DestID)
            ocrd5.Execute("DELETE FROM ReportOptions WHERE ReportID IN (SELECT " & _
            "ReportID FROM ReportAttr WHERE PackID =" & nPackID & ")")
            ocrd5.Execute("DELETE FROM SubReportLogin WHERE ReportID IN (SELECT " & _
            "ReportID FROM ReportAttr WHERE PackID =" & nPackID & ")")
            ocrd5.Execute("DELETE FROM SubReportParameters WHERE ReportID IN (SELECT " & _
            "ReportID FROM ReportAttr WHERE PackID =" & nPackID & ")")

            If MessageBox.Show(ex.Message & vbCrLf & Err.Number & vbCrLf & ex.TargetSite.GetCurrentMethod.Name & vbCrLf & "Continue?", _
            Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.Yes Then

                GoTo NextPack
            End If

        End Try
    End Sub
    Public Function _EncryptDBValue(ByVal sValue) As String

        sValue &= ""

        Dim sReturn As String

        sReturn = Encrypt(sValue, "preovitriolic")

        If sReturn Is Nothing Then
            Return String.Empty
        Else
            Return sReturn
        End If
    End Function
    Public Function Encrypt(ByVal PlainText, ByVal WatchWord)

        'Other Dim Things
        Dim intKey
        Dim intX
        Dim strChar
        Dim EncText

        'all Mighty visual Basic lines give us the Key
        For intX = 1 To Len(WatchWord)
            intKey = intKey + Asc(Mid(WatchWord, intX)) / 20
        Next

        'the brother of decrypt function.but it earns its
        'living by encrypting the data
        For intX = 1 To Len(PlainText)
            strChar = Mid(PlainText, intX, 1)
            EncText = EncText & Chr(Asc(strChar) Xor intKey)
        Next

        'the ever so generous functios should return something.
        Encrypt = EncText
    End Function
    Public Sub _MoveSingles()
        Dim oRs As New ADODB.Recordset
        'Dim ocrd4 As New ADODB.Connection
        'Dim ocrd5 As New ADODB.Connection
        Dim nReportID As Integer
        Dim nScheduleID As Integer
        Dim DestID As Integer
        Dim I As Integer
        Dim nCount As Integer

        Try
            Dim SQL As String
            Dim sCols As String
            Dim sVals As String
            Dim Dynamic As Integer
            Dim LoginReq As Integer
            Dim UseData As Integer
            Dim sTitle As String
            Dim Cache4 As String
            Dim Cache5 As String
            Dim Parent As Integer

            Cache4 = crd4path.ToLower.Replace("reportdesklive.dat", "Cache\")

            Cache5 = crd5path.ToLower.Replace("crdlive.dat", "Cache\")

            lsv.Items.Clear()

            _Status("Importing Schedules...")
            Application.DoEvents()


            'ocrd4.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & crd4path & ";Persist Security Info=False")

            'ocrd5.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & crd5path & ";Persist Security Info=False")

            'for reportattr--------------------------------------------------------------------------------------------


            SQL = "SELECT * FROM ReportScheduler r LEFT OUTER JOIN ScheduleAttr s ON r.ReportID = s.ReportID"

            oRs.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

            nCount = oRs.RecordCount

            Do While oRs.EOF = False
                I += 1

                Parent = _GetFolderID(oRs("parent").Value)

                pb.Value = (I / nCount) * 100

                o.Text = "Single schedule " & I & " of " & nCount

                Dim sFile As String
                Dim n As Integer

                sFile = oRs("reportpath").Value

                n = sFile.Split("\").GetUpperBound(0)

                sFile = sFile.Split("\")(n)

                If oRs("client").Value & "" <> "Dynamic" Then
                    Dynamic = 0
                Else
                    Dynamic = 1
                End If

                If oRs("loginreq").Value & "" = "T" Then
                    LoginReq = 1
                Else
                    LoginReq = 0
                End If

                If oRs("usesaveddata").Value & "" = "" Then
                    UseData = 0
                ElseIf oRs("usesaveddata").Value & "" = "0" Then
                    UseData = 0
                Else
                    UseData = 1
                End If

                sTitle = oRs("ReportTitle").Value

                'If sTitle.Length > 49 Then
                '    Try
                '        sTitle = sTitle.Substring(0, 49)
                '    Catch
                '        sTitle = Microsoft.VisualBasic.Left(sTitle, 49)
                '    End Try
                'End If

                _Status("Importing report '" & sTitle & "'...")
                Application.DoEvents()

                nReportID = _CreateDataID()

                sCols = "reportid,databasepath,reportname,packid,parent,reporttitle,retry,assumefail," & _
                "checkblank,selectionformula,dynamic,includeattach,owner,rptuserid,rptpassword,rptserver,rptdatabase," & _
                "uselogin,usesaveddata,cachepath,lastrefreshed"

                sVals = nReportID & "," & _
                "'" & SQLPrepare(oRs("originalpath").Value) & "'," & _
                "'" & SQLPrepare(sTitle) & "'," & _
                "0," & _
                Parent & "," & _
                "'" & SQLPrepare(sTitle) & "'," & _
                oRs("retry").Value & "," & _
                0 & "," & _
                "0," & _
                "'" & SQLPrepare(oRs("selectionform").Value) & "'," & _
                Dynamic & "," & _
                "1,'sys'," & _
                "'" & SQLPrepare(oRs("username").Value) & "'," & _
                "'" & SQLPrepare(_EncryptDBValue(oRs("password").Value)) & "'," & _
                "'" & SQLPrepare(oRs("reportserver").Value) & "'," & _
                "'" & SQLPrepare(oRs("reportdatabase").Value) & "'," & _
                LoginReq & "," & _
                UseData & "," & _
                "'" & SQLPrepare(Cache5 & sFile) & "',Now()"

                SQL = "INSERT INTO ReportAttr (" & sCols & ") VALUES (" & sVals & ")"

                ocrd5.Execute(SQL)

                _Status("Importing schedule attributes...")
                Application.DoEvents()

                'scheduleattr-----------------------------------------------------------------------------------------
                Dim Repeat As Integer
                Dim Status As Integer
                Dim RepeatInt As Integer
                Dim nUntil As Date
                Dim nDisable As Date
                Dim NextRun As String

                Try
                    Int32.Parse(oRs("repeat").Value)
                    Repeat = 1
                    RepeatInt = oRs("repeat").Value
                Catch ex As Exception
                    Repeat = 0
                    RepeatInt = 0
                End Try

                Try
                    nUntil = oRs("until").Value
                Catch ex As Exception
                    nUntil = "00:00"
                End Try

                Try
                    nDisable = oRs("disabledate").Value
                Catch ex As Exception
                    nDisable = Now.Date
                End Try

                If oRs("status").Value = "T" Then
                    Status = 1
                Else
                    Status = 0
                End If

                Try
                    NextRun = oRs("nextrun").Value & " " & oRs("runat").Value
                Catch ex As Exception
                    NextRun = oRs("nextrun").Value
                End Try

                nScheduleID = _CreateDataID()

                sCols = "scheduleid,frequency,startdate,enddate,nextrun,starttime," & _
                "repeat,repeatinterval,repeatuntil," & _
                "status,reportid,packid,autoid,description,keyword,calendarname,disableddate"

                sVals = nScheduleID & "," & _
                "'" & SQLPrepare(oRs("frequency").Value) & "'," & _
                "'" & SQLPrepare(oRs("startdate").Value) & "'," & _
                "'" & SQLPrepare(oRs("enddate").Value) & "'," & _
                "'" & NextRun & "'," & _
                "'" & SQLPrepare(oRs("starttime").Value) & "'," & _
                Repeat & "," & _
                RepeatInt & "," & _
                "'" & nUntil & "'," & _
                Status & "," & _
                nReportID & ",0,0," & _
                "'" & SQLPrepare(oRs("scheduledesc").Value) & "'," & _
                "''," & _
                "'" & SQLPrepare(oRs("calendarname").Value) & "'," & _
                "'" & nDisable & "'"

                SQL = "INSERT INTO ScheduleAttr (" & sCols & ") VALUES (" & sVals & ")"


                ocrd5.Execute(SQL)

                _MoveTasks("Single", oRs(0).Value, nScheduleID, lsv)

                _Status("Importing destination attributes...")
                Application.DoEvents()

                'destinationattr--------------------------------------------------------------------------------------
                DestID = _CreateDataID()

                Dim nStamp As Integer
                Dim Zip As Integer
                Dim Embed As Integer

                If (oRs("dtstampformat").Value & "").Length > 0 Then
                    nStamp = 1
                Else
                    nStamp = 0
                End If

                If oRs("collate").Value = "Z" Then
                    Zip = 1
                Else
                    Zip = 0
                End If

                If oRs("printerport").Value = "T" Then
                    Embed = 1
                Else
                    Embed = 0
                End If

                sCols = "destinationid,destinationtype,sendto,cc,bcc,subject,message,extras," & _
                "outputpath, reportid, packid, " & _
                "ftpserver,ftpusername,ftppassword,ftppath,destinationname,outputformat," & _
                "customext,appenddatetime,datetimeformat," & _
                "customname,compress,embed,mailformat,htmlsplit,deferdelivery,deferby," & _
                "smtpserver,encryptzip,encryptziplevel,encryptzipcode,usedun,dunname,includeattach"

                If sTitle.Length > 49 Then
                    Try
                        sTitle = sTitle.Substring(0, 49)
                    Catch
                        sTitle = Microsoft.VisualBasic.Left(sTitle, 49)
                    End Try
                End If


                sVals = DestID & "," & _
                "'" & oRs("rptdestination").Value & "'," & _
                "'" & SQLPrepare(oRs("sendto").Value) & "'," & _
                "'" & SQLPrepare(oRs("sendcc").Value) & "'," & _
                "'" & SQLPrepare(oRs("sendbcc").Value) & "'," & _
                "'" & SQLPrepare(oRs("subject").Value) & "'," & _
                "'" & SQLPrepare(oRs("message").Value) & "'," & _
                "'" & SQLPrepare(oRs("emailattachments").Value) & "'," & _
                "'" & SQLPrepare(oRs("outputpath").Value) & "'," & _
                nReportID & ",0," & _
                "'" & SQLPrepare(oRs("ftpserver").Value) & "'," & _
                "'" & SQLPrepare(oRs("ftpusername").Value) & "'," & _
                "'" & SQLPrepare(oRs("ftppassword").Value) & "'," & _
                "'" & SQLPrepare(oRs("ftppath").Value) & "'," & _
                "'" & SQLPrepare(sTitle) & "'," & _
                "'" & oRs("format").Value & "'," & _
                "'" & SQLPrepare(oRs("orientation").Value) & "'," & _
                nStamp & "," & _
                "'" & SQLPrepare(oRs("dtstampformat").Value) & "'," & _
                "'" & SQLPrepare(oRs("opname").Value) & "'," & _
                Zip & "," & _
                Embed & "," & _
                "'TEXT'," & _
                "0,0,0,'',0,0,'',0,'',1"

                SQL = "INSERT INTO DestinationAttr (" & sCols & ") VALUES (" & sVals & ")"

                Console.WriteLine(SQL)

                ocrd5.Execute(SQL)

                _Status("Importing report parameters...")
                Application.DoEvents()

                'reportparameters------------------------------------------------------------------------------------
                SQL = "SELECT * FROM ReportParameter WHERE ReportID = " & oRs.Fields(0).Value

                Dim oRs1 As New ADODB.Recordset
                Dim ParID As Integer

                oRs1.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                Do While oRs1.EOF = False
                    ParID = _CreateDataID()

                    sCols = "ParID,ReportID,ParName,ParValue,ParType"

                    sVals = ParID & "," & _
                    nReportID & "," & _
                    "'" & SQLPrepare(oRs1("parname").Value) & "'," & _
                    "'" & SQLPrepare(oRs1("parvalue").Value) & "'," & _
                    oRs1("partype").Value

                    SQL = "INSERT INTO ReportParameter (" & sCols & ") VALUES (" & sVals & ")"

                    ocrd5.Execute(SQL)

                    oRs1.MoveNext()
                Loop

                oRs1.Close()

                _Status("Importing schedule options...")
                Application.DoEvents()

                'schedule options----------------------------------------------------------------------------------

                SQL = "SELECT * FROM ScheduleDetail WHERE ReportID = " & oRs.Fields(0).Value

                oRs1.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)


                Do While oRs1.EOF = False
                    Dim nOptId As Integer = _CreateDataID()

                    sCols = "optionid,weekno,dayno,monthsin,ncount,scheduleid,scheduletype"

                    sVals = nOptId & "," & _
                    oRs1("weekno").Value & "," & _
                    oRs1("dayno").Value & "," & _
                    "'" & oRs1("monthsin").Value & "'," & _
                    "0," & _
                    nScheduleID & "," & _
                    "'Monthly'"

                    SQL = "INSERT INTO ScheduleOptions (" & sCols & ") VALUES (" & sVals & ")"

                    ocrd5.Execute(SQL)

                    oRs1.MoveNext()
                Loop

                oRs1.Close()

                'printer options------------------------------------------------------------------------

                _Status("Importing printer attributes...")
                Application.DoEvents()

                Dim PrinterID As Integer

                sCols = "printerid,destinationid,printername,orientation,printerdriver," & _
                "printerport,pagefrom,pageto,copies,[collate],pagesperreport,printmethod,papersize"

                SQL = "SELECT * FROM SchedulePrinter WHERE ReportID = " & oRs.Fields(0).Value

                oRs1.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                Do While oRs1.EOF = False
                    PrinterID = _CreateDataID()

                    sVals = PrinterID & "," & _
                    DestID & "," & _
                    "'" & SQLPrepare(oRs1("printername").Value) & "'," & _
                    "'" & SQLPrepare(oRs1("orientation").Value) & "'," & _
                    "'" & SQLPrepare(oRs1("printerdriver").Value) & "'," & _
                    "'" & SQLPrepare(oRs1("printerport").Value) & "'," & _
                    oRs1("pagefrom").Value & "," & _
                    oRs1("pageto").Value & "," & _
                    oRs1("copies").Value & "," & _
                    oRs1("collate").Value & "," & _
                    oRs1("pagesperreport").Value & "," & _
                    "'" & SQLPrepare(oRs1("printmethod").Value) & "'," & _
                    "'" & SQLPrepare(oRs1("papersize").Value) & "'"

                    SQL = "INSERT INTO PrinterAttr (" & sCols & ") VALUES (" & sVals & ")"

                    ocrd5.Execute(SQL)

                    oRs1.MoveNext()
                Loop

                oRs1.Close()

                _Status("Importing Subreport details...")
                Application.DoEvents()

                SQL = "select * from sublogin where reportid = " & oRs.Fields(0).Value

                oRs1.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                Do While oRs1.EOF = False
                    sCols = "subloginid,reportid,subname,rptserver,rptdatabase,rptuserid,rptpassword"

                    sVals = _CreateDataID() & "," & _
                    nReportID & "," & _
                    "'" & SQLPrepare(oRs1("subname").Value) & "'," & _
                    "'" & SQLPrepare(oRs1("sserver").Value) & "'," & _
                    "'" & SQLPrepare(oRs1("sdatabase").Value) & "'," & _
                    "'" & SQLPrepare(oRs1("suser").Value) & "'," & _
                    "'" & SQLPrepare(oRs1("spassword").Value) & "'"

                    SQL = "INSERT INTO SubReportLogin (" & sCols & ") VALUES (" & sVals & ")"

                    ocrd5.Execute(SQL)

                    oRs1.MoveNext()
                Loop

                oRs1.Close()

                SQL = "SELECT * FROM SubParameters WHERE ReportID =" & oRs.Fields(0).Value

                oRs1.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                Do While oRs1.EOF = False

                    sCols = "subparid,reportid,subname,parname,parvalue,partype"

                    sVals = _CreateDataID() & "," & _
                    nReportID & "," & _
                    "'" & SQLPrepare(oRs1("subname").Value) & "'," & _
                    "'" & SQLPrepare(oRs1("parname").Value) & "'," & _
                    "'" & SQLPrepare(oRs1("parvalue").Value) & "'," & _
                    oRs1("partype").Value

                    SQL = "INSERT INTO SubReportParameters (" & sCols & ") VALUES (" & sVals & ")"

                    ocrd5.Execute(SQL)

                    oRs1.MoveNext()

                Loop

                oRs1.Close()

                SQL = "SELECT * FROM RptOptions WHERE ReportID = " & oRs.Fields(0).Value


                oRs1.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                Do While oRs1.EOF = False
                    sCols = "optionid,htmlmerge,pdfwatermark,pdfpassword,destinationid,reportid," & _
                    "colwidth,exporthf,pagebreak,convertdate,pagefrom,pageto,scharacter,sdelimiter,odbc," & _
                    "tablename,linesperpage,userpassword,canprint,cancopy,canedit,cannotes,canfill,canaccess," & _
                    "canassemble,canprintfull,pdfsecurity"

                    sVals = _CreateDataID() & "," & _
                    "0,'',''," & _
                    DestID & "," & _
                    nReportID & "," & _
                    "'" & oRs1("colwidth").Value & "'," & _
                    oRs1("exporthf").Value & "," & _
                    oRs1("pagebreak").Value & "," & _
                    oRs1("convertdate").Value & "," & _
                    oRs1("pagefrom").Value & "," & _
                    oRs1("pageto").Value & "," & _
                    "'" & SQLPrepare(oRs1("scharacter").Value) & "'," & _
                    "'" & SQLPrepare(oRs1("sdelimiter").Value) & "'," & _
                    "'" & SQLPrepare(oRs1("odbc").Value) & "'," & _
                    "'" & SQLPrepare(oRs1("tablename").Value) & "'," & _
                    25 & "," & _
                    "'',1,1,1,1,1,1,1,1,0"

                    SQL = "INSERT INTO ReportOptions (" & sCols & ") VALUES (" & sVals & ")"

                    ocrd5.Execute(SQL)

                    oRs1.MoveNext()
                Loop

                oRs1.Close()

                'dynamic report crap-----------------------------------------

                _Status("Copying dynamic schedule data...")
                Application.DoEvents()

                SQL = "SELECT * FROM DBLink d LEFT OUTER JOIN DBQuery x ON d.ReportID = x.ReportID WHERE d.ReportID =" & oRs.Fields(0).Value

                oRs1.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                If oRs1.EOF = False Then
                    Dim sODBC As String
                    Dim sDSN As String
                    Dim sUser As String
                    Dim sPassword As String
                    Dim sKeyParameter As String
                    Dim sKeyColumn As String
                    Dim sLinkSQL As String

                    sODBC = SQLPrepare(oRs1("odbc").Value)
                    sKeyParameter = oRs1("keyparameter").Value
                    sKeyParameter = sKeyParameter.Replace("{?", String.Empty).Replace("}", String.Empty)
                    sKeyColumn = oRs1("keytable").Value & "." & oRs1("keycolumn").Value

                    If sODBC.Length > 0 Then
                        sDSN = sODBC.Split(";")(1).Replace("DSN=", String.Empty)
                        sUser = sODBC.Split(";")(2).Replace("UID=", String.Empty)
                        sPassword = sODBC.Split(";")(3).Replace("PWD=", String.Empty)
                    End If

                    sODBC = sDSN & "|" & sUser & "|" & sPassword & "|"

                    sCols = "DynamicID,KeyParameter,KeyColumn,ConString,Query,ReportID"

                    sVals = _CreateDataID() & "," & _
                    "'" & SQLPrepare(sKeyParameter) & "'," & _
                    "'" & SQLPrepare(sKeyColumn) & "'," & _
                    "'" & SQLPrepare(sODBC) & "'," & _
                    "'" & SQLPrepare(oRs1("query").Value) & "'," & _
                    nReportID

                    SQL = "INSERT INTO DynamicAttr (" & sCols & ") VALUES (" & sVals & ")"

                    ocrd5.Execute(SQL)

                    sODBC = oRs1("linkodbc").Value

                    If sODBC.Length > 0 Then
                        sDSN = sODBC.Split(";")(1).Replace("DSN=", String.Empty)
                        sUser = sODBC.Split(";")(2).Replace("UID=", String.Empty)
                        sPassword = sODBC.Split(";")(3).Replace("PWD=", String.Empty)
                    End If

                    sODBC = sDSN & "|" & sUser & "|" & sPassword & "|"

                    sCols = "LinkID,ReportID,LinkSQL,ConString,KeyColumn,KeyParameter,ValueColumn,Table1,Table2," & _
                    "JoinColumn1,JoinColumn2,WhereList"

                    'remove the old style queries from the linksql
                    Dim s1 As String
                    Dim s2 As String
                    Dim nSplit As Integer
                    Dim sWhere() As String
                    Dim sFinal As String = ""

                    sLinkSQL = oRs1("linksql").Value

                    If sLinkSQL.ToLower.IndexOf("where") > -1 Then
                        nSplit = sLinkSQL.ToLower.IndexOf("where")
                        s1 = sLinkSQL.Substring(0, nSplit)
                        s2 = sLinkSQL.ToLower.Substring(nSplit, sLinkSQL.Length - nSplit).Replace("where", String.Empty)

                        If s2.IndexOf("{?") > -1 And s2.IndexOf("}") > -1 Then
                            sWhere = Split(s2, " and ")

                            For Each s As String In sWhere
                                If s.IndexOf("{?") = -1 And s.IndexOf("}") Then
                                    If sFinal.Length = 0 Then
                                        sFinal &= s
                                    Else
                                        sFinal &= " AND " & s
                                    End If
                                End If
                            Next
                        End If
                    Else
                        s1 = sLinkSQL
                    End If

                    If sFinal.Length > 0 Then
                        sLinkSQL = s1 & " WHERE " & sFinal
                    Else
                        sLinkSQL = s1
                    End If

                    sVals = _CreateDataID() & "," & _
                    nReportID & "," & _
                    "'" & SQLPrepare(sLinkSQL) & "'," & _
                    "'" & SQLPrepare(sODBC) & "'," & _
                    "'" & SQLPrepare(sKeyColumn) & "'," & _
                    "'" & SQLPrepare(sKeyParameter) & "'," & _
                    "'" & SQLPrepare(oRs1("emailcolumn").Value) & "'," & _
                    "'','','','',''"

                    SQL = "INSERT INTO DynamicLink (" & sCols & ") VALUES (" & sVals & ")"

                    ocrd5.Execute(SQL)

                End If

                oRs1.Close()

                'copying to cache

                _Status("Copying report to cache")
                Application.DoEvents()

                Try
                    System.IO.File.Copy(oRs("reportpath").Value, Cache5 & sFile, True)
                Catch : End Try

                _Status("++++++++++++++++++++++++++")
                Application.DoEvents()

NextReport:
                oRs.MoveNext()
            Loop

            oRs.Close()

            'ocrd4.Close()

            Try
                With ocrd5
                    .Execute("UPDATE DestinationAttr SET OutputFormat ='Acrobat Format (*.pdf)' WHERE OutputFormat = 'Acrobat Format (PDF)'")
                    .Execute("UPDATE DestinationAttr SET OutputFormat ='Crystal Reports (*.rpt)' WHERE OutputFormat = 'Crystal Reports'")
                    .Execute("UPDATE DestinationAttr SET OutputFormat ='CSV (*.csv)' WHERE OutputFormat = 'CSV'")
                    .Execute("UPDATE DestinationAttr SET OutputFormat ='MS Excel - Data Only (*.xls)' WHERE OutputFormat = 'MS Excel - Data Only'")
                    .Execute("UPDATE DestinationAttr SET OutputFormat ='MS Excel 7 (*.xls)' WHERE OutputFormat = 'MS Excel 7'")
                    .Execute("UPDATE DestinationAttr SET OutputFormat ='MS Excel 8 (*.xls)' WHERE OutputFormat = 'MS Excel 8'")
                    .Execute("UPDATE DestinationAttr SET OutputFormat ='MS Excel 97-2000 (*.xls)' WHERE OutputFormat = 'MS Excel 97-2000'")
                    .Execute("UPDATE DestinationAttr SET OutputFormat ='ODBC (*.odbc)' WHERE OutputFormat = 'ODBC'")
                    .Execute("UPDATE DestinationAttr SET OutputFormat ='MS Word (*.doc)' WHERE OutputFormat = 'MS Word'")
                    .Execute("UPDATE DestinationAttr SET OutputFormat ='Rich Text Format (*.rtf)' WHERE OutputFormat = 'Rich Text'")
                    .Execute("UPDATE DestinationAttr SET OutputFormat ='Tab Seperated (*.txt)' WHERE OutputFormat = 'Tab Seperated'")
                    .Execute("UPDATE DestinationAttr SET OutputFormat ='Text (*.txt)' WHERE OutputFormat = 'Text'")
                    .Execute("UPDATE DestinationAttr SET OutputFormat ='XML (*.xml)' WHERE OutputFormat = 'XML'")
                    .Execute("UPDATE DestinationAttr SET OutputFormat ='HTML (*.htm)' WHERE OutputFormat = 'HTML'")
                End With
            Catch : End Try


            'ocrd5.Close()

            Return
        Catch ex As Exception
            ocrd5.Execute("DELETE FROM ReportAttr WHERE ReportID = " & nReportID)
            ocrd5.Execute("DELETE FROM ScheduleAttr WHERE ReportID =" & nReportID)
            ocrd5.Execute("DELETE FROM DestinationAttr WHERE ReportID =" & nReportID)
            ocrd5.Execute("DELETE FROM ReportParameter WHERE ReportID =" & nReportID)
            ocrd5.Execute("DELETE FROM ScheduleOptions WHERE ScheduleID =" & nScheduleID)
            ocrd5.Execute("DELETE FROM PrinterAttr WHERE Destinationid =" & DestID)
            ocrd5.Execute("DELETE FROM ReportOptions WHERE  ReportID =" & nReportID)
            ocrd5.Execute("DELETE FROM SubReportLogin WHERE ReportID =" & nReportID)
            ocrd5.Execute("DELETE FROM SubReportParameters WHERE ReportID =" & nReportID)
            ocrd5.Execute("DELETE FROM DynamicLink WHERE ReportID =" & nReportID)
            ocrd5.Execute("DELETE FROM DynamicAttr WHERE ReportID =" & nReportID)

            If MessageBox.Show(ex.Message & vbCrLf & Err.Number & vbCrLf & ex.TargetSite.GetCurrentMethod.Name & vbCrLf & "Continue?", _
            Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.Yes Then

                GoTo NextReport
            End If
        End Try

    End Sub

    Public Function SQLPrepare(ByVal sIn) As String
        sIn &= ""

        Return sIn.Replace("'", "''")
    End Function
    Public Function _CreateDataID() As Int64
        Dim Datum As Date
        'If gID = 0 Then
        Datum = New Date(1977, 2, 27)

        System.Threading.Thread.Sleep(1000)

        gID = Date.Now.Subtract(Datum).TotalSeconds

        Return gID
        'Else
        '   gID += 1

        '   Return gID
        ' End If
    End Function

    Public Sub _MoveAddressBook(ByVal crd4path As String, ByVal crd5path As String, ByVal lsv As ListBox)
        Dim oRs As New ADODB.Recordset
        'Dim ocrd4 As New ADODB.Connection
        'Dim ocrd5 As New ADODB.Connection
        Dim nReportID As Integer
        Dim nScheduleID As Integer
        Dim DestID As Integer
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim nID As Integer
        Dim I As Integer
        Dim nCount As Integer

        Try
            'ocrd4.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & crd4path & ";Persist Security Info=False")

            'ocrd5.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & crd5path & ";Persist Security Info=False")

            SQL = "SELECT * FROM AddressBook"

            lsv.Items.Clear()

            _Status("Importing address book...")
            Application.DoEvents()

            oRs.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

            nCount = oRs.RecordCount

            Do While oRs.EOF = False
                I += 1

                pb.Value = (I / nCount) * 100

                o.Text = "Entry " & I & " of " & nCount

                nID = _CreateDataID()

                sCols = "contactid,contactname,contacttype"

                sVals = nID & "," & _
                "'" & SQLPrepare(oRs("entryname").Value) & "'," & _
                "'contact'"

                SQL = "INSERT INTO ContactAttr (" & sCols & ") VALUES (" & sVals & ")"

                ocrd5.Execute(SQL)


                sCols = "DetailID,ContactID,EmailAddress,PhoneNumber,FaxNumber"

                sVals = _CreateDataID() & "," & _
                nID & "," & _
                "'" & SQLPrepare(oRs("emailaddress").Value) & "'," & _
                "'',''"

                SQL = "INSERT INTO ContactDetail (" & sCols & ") VALUES (" & sVals & ")"

                ocrd5.Execute(SQL)

                oRs.MoveNext()

            Loop


            oRs.Close()

            'ocrd4.Close()
            'ocrd5.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message & vbCrLf & Err.Number & vbCrLf & ex.TargetSite.GetCurrentMethod.Name, _
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            ocrd5.Execute("DELETE FROM ContactDetail WHERE ContactID = " & nID)
            ocrd5.Execute("DELETE FROM ContactAttr WHERE ContactID =" & nID)

        End Try

    End Sub

    Public Sub _MoveCalendar(ByVal crd4path As String, ByVal crd5path As String, ByVal lsv As ListBox)
        Dim oRs As New ADODB.Recordset
        'Dim ocrd4 As New ADODB.Connection
        'Dim ocrd5 As New ADODB.Connection
        Dim nReportID As Integer
        Dim nScheduleID As Integer
        Dim DestID As Integer
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim nID As Integer
        Dim I As Integer
        Dim nCount As Integer

        _Status("Importing custom calendars...")
        Application.DoEvents()

        Try
            'ocrd4.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & crd4path & ";Persist Security Info=False")

            'ocrd5.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & crd5path & ";Persist Security Info=False")

            SQL = "SELECT * FROM Calendar"

            oRs.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

            nCount = oRs.RecordCount

            Do While oRs.EOF = False

                I += 1

                pb.Value = (I / nCount) * 100

                o.Text = "Entry " & I & " of " & nCount

                sCols = "calendarname,calendardate,calendarid"

                sVals = "'" & oRs("calendarname").Value & "'," & _
                "'" & oRs("calendardate").Value & "'," & _
                _CreateDataID()

                SQL = "insert into calendarattr (" & sCols & ") values (" & sVals & ")"

                ocrd5.Execute(SQL)

                oRs.MoveNext()
            Loop

            oRs.Close()

            'ocrd4.Close()
            'ocrd5.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message & vbCrLf & Err.Number & vbCrLf & ex.TargetSite.GetCurrentMethod.Name, _
            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Public Sub _MoveFormulas(ByVal crd4path As String, ByVal crd5path As String, ByVal lsv As ListBox)
        Dim oRs As New ADODB.Recordset
        'Dim ocrd4 As New ADODB.Connection
        'Dim ocrd5 As New ADODB.Connection
        Dim nReportID As Integer
        Dim nScheduleID As Integer
        Dim DestID As Integer
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim nID As Integer
        Dim I As Integer
        Dim nCount As Integer

        _Status("Importing custom formulas...")
        Application.DoEvents()

        Try
            'ocrd4.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & crd4path & ";Persist Security Info=False")

            'ocrd5.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & crd5path & ";Persist Security Info=False")

            SQL = "SELECT * FROM Formula"

            sCols = "FormulaID,FormulaName,FormulaDef"

            oRs.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

            nCount = oRs.RecordCount

            Do While oRs.EOF = False
                I += 1

                pb.Value = (I / nCount) * 100

                o.Text = "Entry " & I & " of " & nCount

                sVals = _CreateDataID() & "," & _
                "'" & SQLPrepare(oRs("formulaname").Value) & "'," & _
                "'" & SQLPrepare(oRs("formuladefinition").Value) & "'"

                SQL = "insert into formulaattr (" & sCols & ") values (" & sVals & ")"

                ocrd5.Execute(SQL)

                oRs.MoveNext()
            Loop

            oRs.Close()

            'ocrd4.Close()
            'ocrd5.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message & vbCrLf & Err.Number & vbCrLf & ex.TargetSite.GetCurrentMethod.Name, _
            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Public Sub _MoveSettings(ByVal lsv As ListBox)
        On Error Resume Next
        Dim skey4 As String = "Software\ChristianSteven\Crystal Reports Distributor"
        Dim skey5 As String = "Software\ChristianSteven\CRD"
        Dim oReg As RegistryKey = Registry.LocalMachine
        Dim oSec As New clsMarsSecurity
        Dim MAPIPassword As String
        Dim SMTPPassword As String

        System.IO.File.Copy(crd4path.ToLower.Replace("reportdesklive.dat", "defmail.crd"), _
        crd5path.ToLower.Replace("crdlive.dat", "defmsg.crd"), True)

        lsv.Items.Clear()

        _Status("Importing application settings...")
        Application.DoEvents()

        _SaveRegistry(oReg, skey5, "ErrorHandle", _ReadRegistry(oReg, skey4, "AlertType", ""))
        _SaveRegistry(oReg, skey5, "AlertWho", _ReadRegistry(oReg, skey4, "AlertWho", ""))
        _SaveRegistry(oReg, skey5, "Archive", _ReadRegistry(oReg, skey4, "ArchiveOld", ""))
        _SaveRegistry(oReg, skey5, "ArchiveAfter", _ReadRegistry(oReg, skey4, "ArchiveAfter", ""))
        _SaveRegistry(oReg, skey5, "ErrorHandle", _ReadRegistry(oReg, skey4, "AlertType", ""))
        _SaveRegistry(oReg, skey5, "CRDAddress", _ReadRegistry(oReg, skey4, "CRDAddress", ""))
        _SaveRegistry(oReg, skey5, "CRDSender", _ReadRegistry(oReg, skey4, "CRDSender", ""))
        _SaveRegistry(oReg, skey5, "CRDService", _ReadRegistry(oReg, skey4, "CRDservice", ""))
        _SaveRegistry(oReg, skey5, "Crystal Version", _ReadRegistry(oReg, skey4, "Crystal Version", ""))
        _SaveRegistry(oReg, skey5, "CustNo", _ReadRegistry(oReg, skey4, "CustNo", ""))
        _SaveRegistry(oReg, skey5, "DefDatabase", _ReadRegistry(oReg, skey4, "DefDatabase", ""))
        _SaveRegistry(oReg, skey5, "DefPassword", _ReadRegistry(oReg, skey4, "DefPassword", ""))
        _SaveRegistry(oReg, skey5, "DefPath", _ReadRegistry(oReg, skey4, "DefPath", ""))
        _SaveRegistry(oReg, skey5, "DefServer", _ReadRegistry(oReg, skey4, "DefServer", ""))
        _SaveRegistry(oReg, skey5, "DefUse", _ReadRegistry(oReg, skey4, "DefUse", ""))
        _SaveRegistry(oReg, skey5, "DefUserName", _ReadRegistry(oReg, skey4, "DefUserName", ""))
        _SaveRegistry(oReg, skey5, "ErrorHandle", _ReadRegistry(oReg, skey4, "AlertType", ""))

        If _ReadRegistry(oReg, skey4, "AlertType", "") = "True" Then
            _SaveRegistry(oReg, skey5, "ErrorHandle", 1)
        Else
            _SaveRegistry(oReg, skey5, "ErrorHandle", 0)
        End If

        _SaveRegistry(oReg, skey5, "AgedValue", _ReadRegistry(oReg, skey4, "FMaint", ""))
        _SaveRegistry(oReg, skey5, "AgedUnit", _ReadRegistry(oReg, skey4, "FUnit", ""))
        _SaveRegistry(oReg, skey5, "KeepAuto", _ReadRegistry(oReg, skey4, "KeepAuto", ""))
        _SaveRegistry(oReg, skey5, "KeepMail", _ReadRegistry(oReg, skey4, "KeepMail", ""))
        _SaveRegistry(oReg, skey5, "KeepPackage", _ReadRegistry(oReg, skey4, "KeepPackage", ""))
        _SaveRegistry(oReg, skey5, "KeepSingle", _ReadRegistry(oReg, skey4, "KeepSingle", ""))
        _SaveRegistry(oReg, skey5, "KeepTrigger", _ReadRegistry(oReg, skey4, "KeepTrigger", ""))
        _SaveRegistry(oReg, skey5, "MailType", _ReadRegistry(oReg, skey4, "MailType", ""))

        MAPIPassword = Decrypt(_ReadRegistry(oReg, skey4, "MAPI_Password", ""), "chookoo")

        If Not MAPIPassword Is Nothing Then _SaveRegistry(oReg, skey5, "MAPIPassword", oSec._Encrypt(MAPIPassword))

        _SaveRegistry(oReg, skey5, "MAPIProfile", _ReadRegistry(oReg, skey4, "MAPI_Profile", ""))
        _SaveRegistry(oReg, skey5, "MAPIAlias", _ReadRegistry(oReg, skey4, "MAPIAlias", ""))
        _SaveRegistry(oReg, skey5, "MAPIType", _ReadRegistry(oReg, skey4, "MAPIType", ""))
        _SaveRegistry(oReg, skey5, "MAPIServer", _ReadRegistry(oReg, skey4, "MAPIServer", ""))
        _SaveRegistry(oReg, skey5, "MailType", _ReadRegistry(oReg, skey4, "MailType", ""))
        _SaveRegistry(oReg, skey5, "MAPIDomain", _ReadRegistry(oReg, skey4, "MAPIDomain", ""))
        _SaveRegistry(oReg, skey5, "MAPIUser", _ReadRegistry(oReg, skey4, "MAPIUser", ""))
        _SaveRegistry(oReg, skey5, "Poll", _ReadRegistry(oReg, skey4, "Poll", ""))
        _SaveRegistry(oReg, skey5, "PrintPage", _ReadRegistry(oReg, skey4, "PrintPage", ""))
        _SaveRegistry(oReg, skey5, "RegCo", _ReadRegistry(oReg, skey4, "RegCo", ""))
        _SaveRegistry(oReg, skey5, "RegUser", _ReadRegistry(oReg, skey4, "RegUser", ""))
        _SaveRegistry(oReg, skey5, "RegNo", _ReadRegistry(oReg, skey4, "Registration", ""))
        _SaveRegistry(oReg, skey5, "MailType", _ReadRegistry(oReg, skey4, "MailType", ""))
        _SaveRegistry(oReg, skey5, "SMTPSenderName", _ReadRegistry(oReg, skey4, "SenderName", ""))
        _SaveRegistry(oReg, skey5, "SMTPSenderAddress", _ReadRegistry(oReg, skey4, "SMTP_User", ""))

        SMTPPassword = Decrypt(_ReadRegistry(oReg, skey4, "SMTP_Password", ""), "chookoo")

        If Not SMTPPassword Is Nothing Then _SaveRegistry(oReg, skey5, "SMTPPassword", oSec._Encrypt(SMTPPassword))

        _SaveRegistry(oReg, skey5, "SMTPProfile", _ReadRegistry(oReg, skey4, "SMTP_Profile", ""))
        _SaveRegistry(oReg, skey5, "SMTPUserID", _ReadRegistry(oReg, skey4, "SMTP_Profile", ""))
        _SaveRegistry(oReg, skey5, "SMTPServer", _ReadRegistry(oReg, skey4, "SMTPServer", ""))
        _SaveRegistry(oReg, skey5, "SMTPTimeout", _ReadRegistry(oReg, skey4, "SMTPTimeout", ""))
        _SaveRegistry(oReg, skey5, "TempFolder", _ReadRegistry(oReg, skey4, "TempOutputFolder", ""))
        _SaveRegistry(oReg, skey5, "MailType", _ReadRegistry(oReg, skey4, "MailType", ""))


    End Sub
    Public Function Decrypt(ByVal EncText, ByVal WatchWord)

        'All the Dim things Follows
        Dim intKey
        Dim intX
        Dim strChar
        Dim DecText

        'The Few Lines that work for us.The Following three
        'Lines Create the key to Decrypt the Data.
        For intX = 1 To Len(WatchWord)
            intKey = intKey + Asc(Mid(WatchWord, intX)) / 20
        Next

        'Here is our Engine Room that Decrypts the Data by using the key.
        For intX = 1 To Len(EncText)
            strChar = Mid(EncText, intX, 1)
            DecText = DecText & Chr(Asc(strChar) Xor intKey)
        Next

        'Return the Data.
        Decrypt = DecText
    End Function
    Public Function _SaveRegistry(ByVal ParentKey As RegistryKey, ByVal SubKey As String, _
    ByVal ValueName As String, ByRef Value As Object) As Boolean

        Dim Key As RegistryKey
        Dim Temp

        If Value Is Nothing Then Value = ""

        Temp = Value

        Try
            'Open the registry key.
            Key = ParentKey.OpenSubKey(SubKey, True)

            If Key Is Nothing Then 'if the key doesn't exist

                Key = ParentKey

                Key = Key.CreateSubKey(SubKey)
            End If

            Key.SetValue(ValueName, Value)

            Key.Close()

            Return True
        Catch e As Exception
            MessageBox.Show(e.Message)
            Return False
        End Try

    End Function

    Public Function _ReadRegistry(ByVal ParentKey As RegistryKey, ByVal SubKey As String, _
    ByVal ValueName As String, ByRef Value As Object) As Object

        Dim Key As RegistryKey
        Dim Temp

        Temp = Value

        Try
            'Open the registry key.
            Key = ParentKey.OpenSubKey(SubKey, True)

            If Key Is Nothing Then 'if the key doesn't exist
                Return Temp
            End If

            'Get the value.
            Value = Key.GetValue(ValueName)

            Key.Close()

            If Value Is Nothing Then
                Return Temp
            Else
                Return Value
            End If
        Catch e As Exception
            Return Temp
        End Try

    End Function

    Public Sub _CheckVersion(ByVal spath As String)
        On Error Resume Next
        spath = spath.ToLower.Replace("reportdesklive.dat", "crd.exe")

        If System.IO.File.Exists(spath) = False Then
            MessageBox.Show("Could not obtain the version of the previous CRD installation." & vbCrLf & _
            "Please make sure that the path you have selected for 'reportdesklive.dat' also contains the 'crd.exe'", _
            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            End

        End If

        Dim nVer As Double
        Dim o As FileVersionInfo = FileVersionInfo.GetVersionInfo(spath)


        nVer = CType((o.ProductMajorPart & "." & o.ProductMinorPart), Double)

        If nVer < 4.5 Then
            MessageBox.Show("Please upgrade to the latest version of Crystal Reports Distributor 4 (4.5) before attempting to migrate to version 5", _
            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            End
        End If

    End Sub


    Public Sub _Action()

        _MoveFolders()

        If MessageBox.Show("Import single schedules?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            _MoveSingles()
        End If

        If MessageBox.Show("Import package schedules?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            _MovePackages(crd4path, crd5path, lsv)
        End If

        If MessageBox.Show("Import address book?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            _MoveAddressBook(crd4path, crd5path, lsv)
        End If

        If MessageBox.Show("Import custom calendars?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            _MoveCalendar(crd4path, crd5path, lsv)
        End If

        If MessageBox.Show("Import custom formulas?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            _MoveFormulas(crd4path, crd5path, lsv)
        End If

        If MessageBox.Show("Import applicaton settings?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            _MoveSettings(lsv)
        End If

        MessageBox.Show("Migration completed", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        o.cmdStart.Text = "Start"
    End Sub

    Public Sub _MoveTasks(ByVal ScheduleType As String, ByVal OldID As Integer, ByVal ScheduleID As Integer, ByVal lsv As ListBox)
        'Dim ocrd4 As New ADODB.Connection
        'Dim ocrd5 As New ADODB.Connection
        Dim SQL As String
        Dim oRS As New ADODB.Recordset
        Dim sVals As String
        Dim sCols As String
        Dim AfterType As String
        Dim sWhere As String = " AND AlertID =" & OldID

        _Status("Importing custom actions...")
        Application.DoEvents()

        Try
            ' ocrd4.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & crd4path & ";Persist Security Info=False")

            'ocrd5.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & crd5path & ";Persist Security Info=False")

            'lets copy send email
            SQL = "SELECT * FROM Tasks WHERE TaskType = 'Email' AND ReportType ='" & ScheduleType & "'" & _
            sWhere

            Try
                oRS.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                Do While oRS.EOF = False
                    AfterType = SQLPrepare(oRS("aftertype").Value)

                    If AfterType.ToLower = "delivered" Then
                        AfterType = "Delivery"
                    Else
                        AfterType = "Production"
                    End If

                    sCols = "TaskID,ScheduleID,TaskType,TaskName,Msg,SendTo,CC,Bcc,Subject,OrderID,RunWhen,AfterType," & _
                    "ProgramPath,FileList"

                    sVals = _CreateDataID() & "," & _
                    ScheduleID & "," & _
                    "'SendMail'," & _
                    "'" & SQLPrepare(oRS("taskdesc").Value) & "'," & _
                    "'" & SQLPrepare(oRS("body").Value) & "'," & _
                    "'" & SQLPrepare(oRS("sendto").Value) & "'," & _
                    "'" & SQLPrepare(oRS("sendcc").Value) & "'," & _
                    "'" & SQLPrepare(oRS("sendbcc").Value) & "'," & _
                    "'" & SQLPrepare(oRS("subject").Value) & "'," & _
                    oRS("orderid").Value & "," & _
                    "'" & SQLPrepare(oRS("executewhen").Value) & "'," & _
                    "'" & AfterType & "'," & _
                    "'TEXT'," & _
                    "'" & SQLPrepare(oRS("attach").Value) & "'"

                    SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

                    ocrd5.Execute(SQL)

                    oRS.MoveNext()
                Loop

                oRS.Close()
            Catch : End Try


            'copy execute program
            Try
                SQL = "SELECT * FROM Tasks WHERE TaskType = 'Program' AND ReportType ='" & ScheduleType & "'" & _
                sWhere

                oRS.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                Do While oRS.EOF = False
                    AfterType = SQLPrepare(oRS("aftertype").Value)

                    If AfterType.ToLower = "delivered" Then
                        AfterType = "Delivery"
                    Else
                        AfterType = "Production"
                    End If

                    sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramPath,ProgramParameters,WindowStyle," & _
                    "OrderID,RunWhen,AfterType"

                    sVals = _CreateDataID() & "," & _
                    ScheduleID & "," & _
                    "'Program'," & _
                    "'" & SQLPrepare(oRS("taskdesc").Value) & "'," & _
                    "'" & SQLPrepare(oRS("apppath").Value) & "'," & _
                    "'" & SQLPrepare(oRS("argument").Value) & "'," & _
                    "'Normal'," & _
                    oRS("orderid").Value & "," & _
                    "'" & oRS("executewhen").Value & "'," & _
                    "'" & AfterType & "'"

                    SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

                    ocrd5.Execute(SQL)

                    oRS.MoveNext()
                Loop

                oRS.Close()
            Catch : End Try

            'copy rename file
            Try
                SQL = "SELECT * FROM Tasks WHERE TaskType = 'Rename File' AND ReportType ='" & ScheduleType & "'" & _
                sWhere

                oRS.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                Do While oRS.EOF = False
                    AfterType = SQLPrepare(oRS("aftertype").Value)

                    If AfterType.ToLower = "delivered" Then
                        AfterType = "Delivery"
                    Else
                        AfterType = "Production"
                    End If

                    sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramPath,FileList," & _
                    "OrderID,RunWhen,AfterType"

                    sVals = _CreateDataID() & "," & _
                    ScheduleID & "," & _
                    "'RenameFile'," & _
                    "'" & SQLPrepare(oRS("taskdesc").Value) & "'," & _
                    "'" & SQLPrepare(oRS("attach").Value) & "'," & _
                    "'" & SQLPrepare(oRS("body").Value) & "'," & _
                    oRS("orderid").Value & "," & _
                    "'" & oRS("executewhen").Value & "'," & _
                    "'" & AfterType & "'"

                    SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

                    ocrd5.Execute(SQL)

                    oRS.MoveNext()
                Loop

                oRS.Close()
            Catch : End Try

            'copy pause

            Try
                SQL = "SELECT * FROM Tasks WHERE TaskType = 'Wait' AND ReportType ='" & ScheduleType & "'" & _
                sWhere

                oRS.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                Do While oRS.EOF = False
                    AfterType = SQLPrepare(oRS("aftertype").Value)

                    If AfterType.ToLower = "delivered" Then
                        AfterType = "Delivery"
                    Else
                        AfterType = "Production"
                    End If

                    sCols = "TaskID,ScheduleID,TaskType,TaskName,PauseInt," & _
                    "OrderID,RunWhen,AfterType"

                    sVals = _CreateDataID() & "," & _
                    ScheduleID & "," & _
                    "'Pause'," & _
                    "'" & SQLPrepare(oRS("taskdesc").Value) & "'," & _
                    oRS("taskdelay").Value & "," & _
                    oRS("orderid").Value & "," & _
                    "'" & oRS("executewhen").Value & "'," & _
                    "'" & AfterType & "'"

                    SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

                    ocrd5.Execute(SQL)

                    oRS.MoveNext()
                Loop

                oRS.Close()
            Catch : End Try

            'copy write to file

            Try
                SQL = "SELECT * FROM Tasks WHERE TaskType = 'File' AND ReportType ='" & ScheduleType & "'" & _
                sWhere

                oRS.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                Do While oRS.EOF = False
                    AfterType = SQLPrepare(oRS("aftertype").Value)

                    If AfterType.ToLower = "delivered" Then
                        AfterType = "Delivery"
                    Else
                        AfterType = "Production"
                    End If

                    Dim nReplace As Integer

                    If SQLPrepare(oRS("filetype").Value).ToLower = "create" Or _
                    SQLPrepare(oRS("filetype").Value).ToLower = "delete" Then
                        nReplace = 1
                    Else
                        nReplace = 0
                    End If

                    sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramPath,ReplaceFiles,Msg," & _
                    "OrderID,RunWhen,AfterType"

                    sVals = _CreateDataID() & "," & _
                    ScheduleID & "," & _
                    "'WriteFile'," & _
                    "'" & SQLPrepare(oRS("taskdesc").Value) & "'," & _
                    "'" & SQLPrepare(oRS("apppath").Value) & "'," & _
                    nReplace & "," & _
                    "'" & SQLPrepare(oRS("body").Value) & "'," & _
                    oRS("orderid").Value & "," & _
                    "'" & oRS("executewhen").Value & "'," & _
                    "'" & AfterType & "'"

                    SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

                    ocrd5.Execute(SQL)

                    oRS.MoveNext()
                Loop

                oRS.Close()
            Catch : End Try

            'copy update db

            Try
                SQL = "SELECT * FROM Tasks WHERE TaskType = 'SQL' AND ReportType ='" & ScheduleType & "'" & _
                sWhere

                oRS.Open(SQL, ocrd4, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                Do While oRS.EOF = False
                    AfterType = SQLPrepare(oRS("aftertype").Value)

                    If AfterType.ToLower = "delivered" Then
                        AfterType = "Delivery"
                    Else
                        AfterType = "Production"
                    End If

                    'sendto = update
                    'filelist = where

                    Dim UpdateTable As String
                    Dim sDsn As String
                    Dim sUser As String
                    Dim sPassword As String
                    Dim sCon As String

                    UpdateTable = oRS("body").Value
                    UpdateTable = UpdateTable.Split(" ")(1).Trim

                    sCon = oRS("updateodbc").Value

                    sDsn = sCon.Split(";")(1).Replace("DSN=", String.Empty)
                    sUser = sCon.Split(";")(2).Replace("UID=", String.Empty)
                    sPassword = sCon.Split(";")(3).Replace("PWD=", String.Empty)

                    sCon = sDsn & "|" & sUser & "|" & sPassword & "|"

                    sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramPath,ProgramParameters,FileList,Msg,SendTo," & _
                    "OrderID,RunWhen,AfterType"

                    sVals = _CreateDataID() & "," & _
                    ScheduleID & "," & _
                    "'DBUpdate'," & _
                    "'" & SQLPrepare(oRS("taskdesc").Value) & "'," & _
                    "'" & SQLPrepare(UpdateTable) & "'," & _
                    "'" & SQLPrepare(sCon) & "'," & _
                    "''," & _
                    "'" & SQLPrepare(oRS("body").Value) & "'," & _
                    "''," & _
                    oRS("orderid").Value & "," & _
                    "'" & oRS("executewhen").Value & "'," & _
                    "'" & AfterType & "'"

                    SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

                    Console.WriteLine(SQL)

                    ocrd5.Execute(SQL)

                    oRS.MoveNext()
                Loop

                oRS.Close()
            Catch ex As Exception
                Console.WriteLine(ex.Message)
            End Try

        Catch : End Try
    End Sub
    Public Function GetLineNumber(ByVal sIn As String) As Integer
        Try
            Dim st As String = sIn
            Dim LineNr As Int32 = CType(st.Substring(st.LastIndexOf(":line") + 5), Int32)

            Return LineNr
        Catch
            Return 0
        End Try

    End Function
    Public Sub _Connect()
        ocrd4.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & crd4path & ";Persist Security Info=False")

        ocrd5.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & crd5path & ";Persist Security Info=False")
    End Sub
    Public Sub New()
        ocrd4 = New ADODB.Connection
        ocrd5 = New ADODB.Connection
    End Sub
End Class
