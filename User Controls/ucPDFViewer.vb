Imports DevComponents.DotNetBar
Friend Class ucPDFViewer
    Inherits System.Windows.Forms.UserControl


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()


    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents ContextMenu1 As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents oPDF As AxPDFViewer.AxPDFView
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucPDFViewer))
        Me.ContextMenu1 = New System.Windows.Forms.ContextMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.oPDF = New AxPDFViewer.AxPDFView
        CType(Me.oPDF, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ContextMenu1
        '
        Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem4, Me.MenuItem5, Me.MenuItem6})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "25"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "50"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "75"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 3
        Me.MenuItem4.Text = "100"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 4
        Me.MenuItem5.Text = "150"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 5
        Me.MenuItem6.Text = "200"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        '
        'oPDF
        '
        Me.oPDF.Dock = System.Windows.Forms.DockStyle.Fill
        Me.oPDF.Enabled = True
        Me.oPDF.Location = New System.Drawing.Point(0, 0)
        Me.oPDF.Name = "oPDF"
        Me.oPDF.OcxState = CType(resources.GetObject("oPDF.OcxState"), System.Windows.Forms.AxHost.State)
        Me.oPDF.Size = New System.Drawing.Size(536, 496)
        Me.oPDF.TabIndex = 0
        '
        'ucPDFViewer
        '
        Me.Controls.Add(Me.oPDF)
        Me.Name = "ucPDFViewer"
        Me.Size = New System.Drawing.Size(536, 496)
        CType(Me.oPDF, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Public Function PrintFile(ByVal sFile As String, ByVal nCopies As Integer, _
   ByVal sPrinter As String, Optional ByVal nPageFrom As Integer = 0, Optional ByVal nPageTo As Integer = 0) As Boolean
        Dim pdfprinter As Gnostice.PDFOne.PDFPrinter.PDFPrinter = New Gnostice.PDFOne.PDFPrinter.PDFPrinter
        'Dim err As Gnostice.PDFOne.PDFPrinter.PrinterErrors
        '//load the pdf document
        pdfprinter.LoadDocument(sFile)

        If pdfprinter.PDFLoaded = False Then
            Throw New Exception("Failed to load PDF for printing")
        End If

        pdfprinter.ReversePageOrder = False

        Dim options As Printing.PrinterSettings = New Printing.PrinterSettings

        options.PrinterName = sPrinter

        If nPageFrom = 0 And nPageTo = 0 Then
            options.PrintRange = Printing.PrintRange.AllPages
        Else
            options.PrintRange = Printing.PrintRange.SomePages
            options.FromPage = nPageFrom
            options.ToPage = nPageTo
        End If

        options.Copies = nCopies

        options.DefaultPageSettings.Margins.Left = 0
        options.DefaultPageSettings.Margins.Right = 0
        options.DefaultPageSettings.Margins.Top = 0
        options.DefaultPageSettings.Margins.Bottom = 0

        pdfprinter.PrintOptions = options

        pdfprinter.Print(IO.Path.GetFileName(sFile))

        pdfprinter.CloseDocument()

        pdfprinter.Dispose()
    End Function
    Public Function _PrintFile(ByVal sFile As String, ByVal sPrinter As String, Optional ByVal nStart As Integer = 0, _
    Optional ByVal nEnd As Integer = 0) As Boolean
        Try
            Dim oPrint As iSED.QuickPDF = New iSED.QuickPDF
            Dim oPrinter As clsPrinters = New clsPrinters

#If DEBUG Then
            sFile = "c:\users\steven\desktop\latecharges.pdf"
#End If

            oPrint.UnlockKey("B34B74C95F359046BEFB789480980C03")

            oPrint.LoadFromFile(sFile)

            Dim nOptions As Integer = oPrint.PrintOptions(1, 1, ExtractFileName(sFile))

            If nStart = 0 And nEnd = 0 Then
                nStart = 1
                nEnd = oPrint.PageCount
            End If

            sPrinter = clsMarsParser.Parser.ParseString(sPrinter)

            If sPrinter = "" Then Return True

            If oPrint.Unlocked = 1 Then
                oPrint.PrintDocument(sPrinter, nStart, nEnd, nOptions)
            End If

        Catch ex As Exception
            gErrorDesc = ex.Message
            gErrorNumber = Err.Number
            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
            gErrorLine = _GetLineNumber(ex.StackTrace)

            Return False
        End Try

    End Function
    Public Function _View(ByVal sFile As String)
        Try
            With oPDF
                .OpenPDF(sFile)
            End With
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace), "Please make sure that Adobe Acrobat 7 is installed.")
        End Try
    End Function




End Class
