Public Class frmFTPBrowser2

    Dim UserCancel As Boolean = True

    Public Overloads Function GetFtpInfo(ByVal ftp As Rebex.Net.Sftp, Optional folder As String = "") As String()
        If ftp Is Nothing Then Return Nothing

        Dim sReturn(1) As String

        With Me.UcFTPBrowser21
            .GetFtpFolderOrFile(ftp, folder)
        End With

        Me.ShowDialog()

        If UserCancel = True Then
            Return Nothing
        Else
            sReturn(0) = UcFTPBrowser21.lblPath.Text
            sReturn(1) = UcFTPBrowser21.sFTPItem
        End If

        Return sReturn

finis:
    End Function
    'at this point ftp should be all connected...
    Public Overloads Function GetFtpInfo(ByRef ftp As Chilkat.Ftp2, Optional folder As String = "") As String()

        If ftp Is Nothing Then Return Nothing

        Dim sReturn(1) As String

        With Me.UcFTPBrowser21
            .GetFtpFolderOrFile(ftp, folder)
        End With

        Me.ShowDialog()

        If UserCancel = True Then
            Return Nothing
        Else
            sReturn(0) = UcFTPBrowser21.lblPath.Text
            sReturn(1) = UcFTPBrowser21.sFTPItem
        End If

        Return sReturn

finis:

    End Function

    Private Sub frmFTPBrowser2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Close()
    End Sub

    Private Sub cmdCancel_ClientSizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.ClientSizeChanged

    End Sub

    Private Sub cmdOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        UserCancel = False
        Close()
    End Sub

    Private Sub UcFTPBrowser21_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UcFTPBrowser21.Load

    End Sub
End Class