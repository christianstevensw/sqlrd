Imports DevComponents.DotNetBar
Imports DevComponents.DotNetBar.Controls
Public Class ucTasks
    Inherits System.Windows.Forms.UserControl
    Dim oTask As clsMarsTask = New clsMarsTask
    Public ScheduleID As Integer = 99999
    Public ShowAfterType As Boolean = True

    Public oAuto As Boolean = False
    Dim eventBased As Boolean = False
    Friend WithEvents mnuType As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents DefaultTaskToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ImportTaskListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Dim eventID As Integer = 99999
    Dim defaultTasks As Boolean = False
    Friend WithEvents mnuTest As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents TestToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents imgTasksTree As System.Windows.Forms.ImageList
    Friend WithEvents tvTaskTypes As DevComponents.AdvTree.AdvTree
    Friend WithEvents Node1 As DevComponents.AdvTree.Node
    Friend WithEvents NodeConnector1 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle1 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents Node6 As DevComponents.AdvTree.Node
    Friend WithEvents Node7 As DevComponents.AdvTree.Node
    Friend WithEvents Node8 As DevComponents.AdvTree.Node
    Friend WithEvents Node9 As DevComponents.AdvTree.Node
    Friend WithEvents Node10 As DevComponents.AdvTree.Node
    Friend WithEvents Node2 As DevComponents.AdvTree.Node
    Friend WithEvents Node11 As DevComponents.AdvTree.Node
    Friend WithEvents Node12 As DevComponents.AdvTree.Node
    Friend WithEvents Node13 As DevComponents.AdvTree.Node
    Friend WithEvents Node14 As DevComponents.AdvTree.Node
    Friend WithEvents Node15 As DevComponents.AdvTree.Node
    Friend WithEvents Node16 As DevComponents.AdvTree.Node
    Friend WithEvents Node18 As DevComponents.AdvTree.Node
    Friend WithEvents Node19 As DevComponents.AdvTree.Node
    Friend WithEvents Node20 As DevComponents.AdvTree.Node
    Friend WithEvents Node21 As DevComponents.AdvTree.Node
    Friend WithEvents Node22 As DevComponents.AdvTree.Node
    Friend WithEvents Node3 As DevComponents.AdvTree.Node
    Friend WithEvents Node23 As DevComponents.AdvTree.Node
    Friend WithEvents Node24 As DevComponents.AdvTree.Node
    Friend WithEvents Node25 As DevComponents.AdvTree.Node
    Friend WithEvents Node26 As DevComponents.AdvTree.Node
    Friend WithEvents Node27 As DevComponents.AdvTree.Node
    Friend WithEvents Node28 As DevComponents.AdvTree.Node
    Friend WithEvents Node29 As DevComponents.AdvTree.Node
    Friend WithEvents Node30 As DevComponents.AdvTree.Node
    Friend WithEvents Node4 As DevComponents.AdvTree.Node
    Friend WithEvents Node5 As DevComponents.AdvTree.Node
    Friend WithEvents Node31 As DevComponents.AdvTree.Node
    Friend WithEvents Node32 As DevComponents.AdvTree.Node
    Friend WithEvents Node33 As DevComponents.AdvTree.Node
    Friend WithEvents Node34 As DevComponents.AdvTree.Node
    Friend WithEvents Node35 As DevComponents.AdvTree.Node
    Friend WithEvents Node36 As DevComponents.AdvTree.Node
    Friend WithEvents Node37 As DevComponents.AdvTree.Node
    Friend WithEvents Node38 As DevComponents.AdvTree.Node
    Friend WithEvents Node39 As DevComponents.AdvTree.Node
    Friend WithEvents Node40 As DevComponents.AdvTree.Node
    Friend WithEvents Node41 As DevComponents.AdvTree.Node
    Friend WithEvents Node42 As DevComponents.AdvTree.Node
    Friend WithEvents Node43 As DevComponents.AdvTree.Node
    Friend WithEvents Node44 As DevComponents.AdvTree.Node
    Dim loaded As Boolean = False
    Friend WithEvents tvTasks As DevComponents.AdvTree.AdvTree
    Friend WithEvents NodeConnector2 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle2 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents ColumnHeader1 As DevComponents.AdvTree.ColumnHeader
    Friend WithEvents ColumnHeader2 As DevComponents.AdvTree.ColumnHeader
    Friend WithEvents ColumnHeader3 As DevComponents.AdvTree.ColumnHeader
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents Node17 As DevComponents.AdvTree.Node
    Friend WithEvents Node45 As DevComponents.AdvTree.Node
    Friend WithEvents Node46 As DevComponents.AdvTree.Node
    Friend WithEvents Node47 As DevComponents.AdvTree.Node
    Friend WithEvents Node48 As DevComponents.AdvTree.Node
    Friend WithEvents Node49 As DevComponents.AdvTree.Node
    Friend WithEvents Node50 As DevComponents.AdvTree.Node
    Friend WithEvents Node51 As DevComponents.AdvTree.Node

    Public Property m_showAfterType As Boolean
        Get
            Return ShowAfterType
        End Get
        Set(value As Boolean)
            ShowAfterType = value

            If value = False Then
                tvTasks.Columns(1).Visible = False
                tvTasks.Columns(2).Visible = False
            Else
                tvTasks.Columns(1).Visible = True
                tvTasks.Columns(2).Visible = True
            End If
        End Set
    End Property
    Public Property m_defaultTaks() As Boolean
        Get
            Return defaultTasks
        End Get
        Set(ByVal value As Boolean)
            defaultTasks = value
        End Set
    End Property
    Public Property m_eventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = value
        End Set
    End Property

    Public Property m_eventID() As Integer
        Get
            Return eventID
        End Get
        Set(ByVal value As Integer)
            eventID = value
        End Set
    End Property


    Private showExpanded As Boolean = True
    Public Property m_showExpanded() As Boolean
        Get
            Return showExpanded
        End Get
        Set(ByVal value As Boolean)
            value = True  '//dont what you put here, it will be true

            showExpanded = value

            If value = False Then
                For Each nd As DevComponents.AdvTree.Node In tvTaskTypes.Nodes
                    nd.Collapse(DevComponents.AdvTree.eTreeAction.Collapse)
                Next
            End If
        End Set
    End Property

    ''' <summary>
    ''' hide or show import and export buttons
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property m_ShowImportExport() As Boolean
        Set(ByVal value As Boolean)
            cmdLoad.Visible = value
            cmdSave.Visible = value
        End Set
    End Property

    ''' <summary>
    ''' returns number of selected items
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property m_SelTasks() As Integer
        Get
            Return Me.tvTasks.SelectedNodes.Count
        End Get
    End Property
    ''' <summary>
    ''' return the selected items collection
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property m_SelItems() As DevComponents.AdvTree.NodeCollection
        Get
            Return tvTasks.SelectedNodes
        End Get
    End Property

    Public Property m_forExceptionHandling As Boolean = False

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdAdd As ButtonX
    Friend WithEvents cmdEdit As ButtonX
    Friend WithEvents cmdDown As ButtonX
    Friend WithEvents cmdDelete As ButtonX
    Friend WithEvents cmdUp As ButtonX
    Friend WithEvents imgTasks As System.Windows.Forms.ImageList
    Friend WithEvents cmdSave As ButtonX
    Friend WithEvents cmdLoad As ButtonX
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucTasks))
        Me.mnuTest = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.TestToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.imgTasks = New System.Windows.Forms.ImageList(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdSave = New DevComponents.DotNetBar.ButtonX()
        Me.cmdLoad = New DevComponents.DotNetBar.ButtonX()
        Me.mnuType = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DefaultTaskToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ImportTaskListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmdDelete = New DevComponents.DotNetBar.ButtonX()
        Me.cmdDown = New DevComponents.DotNetBar.ButtonX()
        Me.cmdEdit = New DevComponents.DotNetBar.ButtonX()
        Me.cmdAdd = New DevComponents.DotNetBar.ButtonX()
        Me.cmdUp = New DevComponents.DotNetBar.ButtonX()
        Me.imgTasksTree = New System.Windows.Forms.ImageList(Me.components)
        Me.tvTaskTypes = New DevComponents.AdvTree.AdvTree()
        Me.Node1 = New DevComponents.AdvTree.Node()
        Me.Node6 = New DevComponents.AdvTree.Node()
        Me.Node7 = New DevComponents.AdvTree.Node()
        Me.Node8 = New DevComponents.AdvTree.Node()
        Me.Node9 = New DevComponents.AdvTree.Node()
        Me.Node10 = New DevComponents.AdvTree.Node()
        Me.Node2 = New DevComponents.AdvTree.Node()
        Me.Node11 = New DevComponents.AdvTree.Node()
        Me.Node12 = New DevComponents.AdvTree.Node()
        Me.Node13 = New DevComponents.AdvTree.Node()
        Me.Node14 = New DevComponents.AdvTree.Node()
        Me.Node48 = New DevComponents.AdvTree.Node()
        Me.Node15 = New DevComponents.AdvTree.Node()
        Me.Node16 = New DevComponents.AdvTree.Node()
        Me.Node18 = New DevComponents.AdvTree.Node()
        Me.Node19 = New DevComponents.AdvTree.Node()
        Me.Node20 = New DevComponents.AdvTree.Node()
        Me.Node46 = New DevComponents.AdvTree.Node()
        Me.Node21 = New DevComponents.AdvTree.Node()
        Me.Node22 = New DevComponents.AdvTree.Node()
        Me.Node47 = New DevComponents.AdvTree.Node()
        Me.Node17 = New DevComponents.AdvTree.Node()
        Me.Node3 = New DevComponents.AdvTree.Node()
        Me.Node23 = New DevComponents.AdvTree.Node()
        Me.Node24 = New DevComponents.AdvTree.Node()
        Me.Node25 = New DevComponents.AdvTree.Node()
        Me.Node26 = New DevComponents.AdvTree.Node()
        Me.Node27 = New DevComponents.AdvTree.Node()
        Me.Node28 = New DevComponents.AdvTree.Node()
        Me.Node29 = New DevComponents.AdvTree.Node()
        Me.Node30 = New DevComponents.AdvTree.Node()
        Me.Node4 = New DevComponents.AdvTree.Node()
        Me.Node31 = New DevComponents.AdvTree.Node()
        Me.Node45 = New DevComponents.AdvTree.Node()
        Me.Node32 = New DevComponents.AdvTree.Node()
        Me.Node33 = New DevComponents.AdvTree.Node()
        Me.Node34 = New DevComponents.AdvTree.Node()
        Me.Node35 = New DevComponents.AdvTree.Node()
        Me.Node36 = New DevComponents.AdvTree.Node()
        Me.Node49 = New DevComponents.AdvTree.Node()
        Me.Node50 = New DevComponents.AdvTree.Node()
        Me.Node37 = New DevComponents.AdvTree.Node()
        Me.Node38 = New DevComponents.AdvTree.Node()
        Me.Node39 = New DevComponents.AdvTree.Node()
        Me.Node40 = New DevComponents.AdvTree.Node()
        Me.Node41 = New DevComponents.AdvTree.Node()
        Me.Node5 = New DevComponents.AdvTree.Node()
        Me.Node42 = New DevComponents.AdvTree.Node()
        Me.Node43 = New DevComponents.AdvTree.Node()
        Me.Node44 = New DevComponents.AdvTree.Node()
        Me.NodeConnector1 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle1 = New DevComponents.DotNetBar.ElementStyle()
        Me.tvTasks = New DevComponents.AdvTree.AdvTree()
        Me.ColumnHeader1 = New DevComponents.AdvTree.ColumnHeader()
        Me.ColumnHeader2 = New DevComponents.AdvTree.ColumnHeader()
        Me.ColumnHeader3 = New DevComponents.AdvTree.ColumnHeader()
        Me.NodeConnector2 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle2 = New DevComponents.DotNetBar.ElementStyle()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.Node51 = New DevComponents.AdvTree.Node()
        Me.mnuTest.SuspendLayout()
        Me.mnuType.SuspendLayout()
        CType(Me.tvTaskTypes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tvTasks, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mnuTest
        '
        Me.mnuTest.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TestToolStripMenuItem})
        Me.mnuTest.Name = "mnuTest"
        Me.mnuTest.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuTest.Size = New System.Drawing.Size(97, 26)
        '
        'TestToolStripMenuItem
        '
        Me.TestToolStripMenuItem.Name = "TestToolStripMenuItem"
        Me.TestToolStripMenuItem.Size = New System.Drawing.Size(96, 22)
        Me.TestToolStripMenuItem.Text = "Test"
        '
        'imgTasks
        '
        Me.imgTasks.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.imgTasks.ImageSize = New System.Drawing.Size(16, 16)
        Me.imgTasks.TransparentColor = System.Drawing.Color.Transparent
        '
        'cmdSave
        '
        Me.cmdSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdSave.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSave.Location = New System.Drawing.Point(531, 331)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(24, 23)
        Me.cmdSave.TabIndex = 6
        Me.ToolTip1.SetToolTip(Me.cmdSave, "Export task list")
        '
        'cmdLoad
        '
        Me.cmdLoad.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdLoad.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdLoad.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdLoad.Image = CType(resources.GetObject("cmdLoad.Image"), System.Drawing.Image)
        Me.cmdLoad.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoad.Location = New System.Drawing.Point(499, 331)
        Me.cmdLoad.Name = "cmdLoad"
        Me.cmdLoad.Size = New System.Drawing.Size(24, 23)
        Me.cmdLoad.TabIndex = 6
        Me.ToolTip1.SetToolTip(Me.cmdLoad, "Import task list")
        '
        'mnuType
        '
        Me.mnuType.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DefaultTaskToolStripMenuItem, Me.ToolStripMenuItem1, Me.ImportTaskListToolStripMenuItem})
        Me.mnuType.Name = "mnuType"
        Me.mnuType.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuType.Size = New System.Drawing.Size(159, 54)
        '
        'DefaultTaskToolStripMenuItem
        '
        Me.DefaultTaskToolStripMenuItem.Name = "DefaultTaskToolStripMenuItem"
        Me.DefaultTaskToolStripMenuItem.Size = New System.Drawing.Size(158, 22)
        Me.DefaultTaskToolStripMenuItem.Text = "Default Task"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(155, 6)
        '
        'ImportTaskListToolStripMenuItem
        '
        Me.ImportTaskListToolStripMenuItem.Name = "ImportTaskListToolStripMenuItem"
        Me.ImportTaskListToolStripMenuItem.Size = New System.Drawing.Size(158, 22)
        Me.ImportTaskListToolStripMenuItem.Text = "Import Task List"
        '
        'cmdDelete
        '
        Me.cmdDelete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdDelete.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDelete.Location = New System.Drawing.Point(309, 331)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(64, 23)
        Me.cmdDelete.TabIndex = 7
        Me.cmdDelete.Text = "&Delete"
        '
        'cmdDown
        '
        Me.cmdDown.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdDown.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdDown.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdDown.Image = CType(resources.GetObject("cmdDown.Image"), System.Drawing.Image)
        Me.cmdDown.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDown.Location = New System.Drawing.Point(437, 331)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(24, 23)
        Me.cmdDown.TabIndex = 6
        '
        'cmdEdit
        '
        Me.cmdEdit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdEdit.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEdit.Location = New System.Drawing.Point(237, 331)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(64, 23)
        Me.cmdEdit.TabIndex = 5
        Me.cmdEdit.Text = "&Edit"
        '
        'cmdAdd
        '
        Me.cmdAdd.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdAdd.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAdd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAdd.Location = New System.Drawing.Point(74, 331)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(64, 23)
        Me.cmdAdd.TabIndex = 4
        Me.cmdAdd.Text = "&Add"
        Me.cmdAdd.Visible = False
        '
        'cmdUp
        '
        Me.cmdUp.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdUp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdUp.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdUp.Image = CType(resources.GetObject("cmdUp.Image"), System.Drawing.Image)
        Me.cmdUp.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdUp.Location = New System.Drawing.Point(469, 331)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(24, 23)
        Me.cmdUp.TabIndex = 6
        '
        'imgTasksTree
        '
        Me.imgTasksTree.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.imgTasksTree.ImageSize = New System.Drawing.Size(16, 16)
        Me.imgTasksTree.TransparentColor = System.Drawing.Color.Transparent
        '
        'tvTaskTypes
        '
        Me.tvTaskTypes.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.tvTaskTypes.AllowDrop = True
        Me.tvTaskTypes.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.tvTaskTypes.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.tvTaskTypes.BackgroundStyle.Class = "TreeBorderKey"
        Me.tvTaskTypes.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.tvTaskTypes.DragDropNodeCopyEnabled = False
        Me.tvTaskTypes.ExpandButtonType = DevComponents.AdvTree.eExpandButtonType.Triangle
        Me.tvTaskTypes.GridColumnLines = False
        Me.tvTaskTypes.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.tvTaskTypes.Location = New System.Drawing.Point(3, 8)
        Me.tvTaskTypes.Name = "tvTaskTypes"
        Me.tvTaskTypes.Nodes.AddRange(New DevComponents.AdvTree.Node() {Me.Node1, Me.Node2, Me.Node3, Me.Node4, Me.Node5})
        Me.tvTaskTypes.NodesConnector = Me.NodeConnector1
        Me.tvTaskTypes.NodeStyle = Me.ElementStyle1
        Me.tvTaskTypes.PathSeparator = ";"
        Me.tvTaskTypes.Size = New System.Drawing.Size(228, 315)
        Me.tvTaskTypes.Styles.Add(Me.ElementStyle1)
        Me.SuperTooltip1.SetSuperTooltip(Me.tvTaskTypes, New DevComponents.DotNetBar.SuperTooltipInfo("Tip", "", "You can drag from here and drop into the list view to add a custom task. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Or you" & _
            " can double-click on a custom task type to add it.", CType(resources.GetObject("tvTaskTypes.SuperTooltip"), System.Drawing.Image), Nothing, DevComponents.DotNetBar.eTooltipColor.Office2003))
        Me.tvTaskTypes.TabIndex = 8
        Me.tvTaskTypes.Text = "AdvTree1"
        '
        'Node1
        '
        Me.Node1.Expanded = True
        Me.Node1.Image = CType(resources.GetObject("Node1.Image"), System.Drawing.Image)
        Me.Node1.Name = "Node1"
        Me.Node1.Nodes.AddRange(New DevComponents.AdvTree.Node() {Me.Node6, Me.Node7, Me.Node8, Me.Node9, Me.Node10, Me.Node51})
        Me.Node1.Text = "General"
        '
        'Node6
        '
        Me.Node6.Image = CType(resources.GetObject("Node6.Image"), System.Drawing.Image)
        Me.Node6.Name = "Node6"
        Me.Node6.Text = "Run Program/Open Document"
        '
        'Node7
        '
        Me.Node7.Image = CType(resources.GetObject("Node7.Image"), System.Drawing.Image)
        Me.Node7.Name = "Node7"
        Me.Node7.Text = "Print Document"
        '
        'Node8
        '
        Me.Node8.Image = CType(resources.GetObject("Node8.Image"), System.Drawing.Image)
        Me.Node8.Name = "Node8"
        Me.Node8.Text = "Wait/Pause"
        '
        'Node9
        '
        Me.Node9.Image = CType(resources.GetObject("Node9.Image"), System.Drawing.Image)
        Me.Node9.Name = "Node9"
        Me.Node9.Text = "Execute Schedule"
        '
        'Node10
        '
        Me.Node10.Image = CType(resources.GetObject("Node10.Image"), System.Drawing.Image)
        Me.Node10.Name = "Node10"
        Me.Node10.Text = "Send SMS"
        '
        'Node2
        '
        Me.Node2.Expanded = True
        Me.Node2.Image = CType(resources.GetObject("Node2.Image"), System.Drawing.Image)
        Me.Node2.Name = "Node2"
        Me.Node2.Nodes.AddRange(New DevComponents.AdvTree.Node() {Me.Node11, Me.Node12, Me.Node13, Me.Node14, Me.Node48, Me.Node15, Me.Node16, Me.Node18, Me.Node19, Me.Node20, Me.Node46, Me.Node21, Me.Node22, Me.Node47, Me.Node17})
        Me.Node2.Text = "Files & Folders"
        '
        'Node11
        '
        Me.Node11.Image = CType(resources.GetObject("Node11.Image"), System.Drawing.Image)
        Me.Node11.Name = "Node11"
        Me.Node11.Text = "Copy File"
        '
        'Node12
        '
        Me.Node12.Image = CType(resources.GetObject("Node12.Image"), System.Drawing.Image)
        Me.Node12.Name = "Node12"
        Me.Node12.Text = "Rename/Move File"
        '
        'Node13
        '
        Me.Node13.Image = CType(resources.GetObject("Node13.Image"), System.Drawing.Image)
        Me.Node13.Name = "Node13"
        Me.Node13.Text = "Delete File"
        '
        'Node14
        '
        Me.Node14.Image = CType(resources.GetObject("Node14.Image"), System.Drawing.Image)
        Me.Node14.Name = "Node14"
        Me.Node14.Text = "Write Text File"
        '
        'Node48
        '
        Me.Node48.Expanded = True
        Me.Node48.Image = CType(resources.GetObject("Node48.Image"), System.Drawing.Image)
        Me.Node48.Name = "Node48"
        Me.Node48.Text = "Folder Housekeeping"
        '
        'Node15
        '
        Me.Node15.Image = CType(resources.GetObject("Node15.Image"), System.Drawing.Image)
        Me.Node15.Name = "Node15"
        Me.Node15.Text = "Create Folder"
        '
        'Node16
        '
        Me.Node16.Image = CType(resources.GetObject("Node16.Image"), System.Drawing.Image)
        Me.Node16.Name = "Node16"
        Me.Node16.Text = "Rename/Move Folder"
        '
        'Node18
        '
        Me.Node18.Image = CType(resources.GetObject("Node18.Image"), System.Drawing.Image)
        Me.Node18.Name = "Node18"
        Me.Node18.Text = "Zip Files"
        '
        'Node19
        '
        Me.Node19.Image = CType(resources.GetObject("Node19.Image"), System.Drawing.Image)
        Me.Node19.Name = "Node19"
        Me.Node19.Text = "Unzip File"
        '
        'Node20
        '
        Me.Node20.Expanded = True
        Me.Node20.Image = CType(resources.GetObject("Node20.Image"), System.Drawing.Image)
        Me.Node20.Name = "Node20"
        Me.Node20.Text = "Merge PDF Files"
        '
        'Node46
        '
        Me.Node46.Expanded = True
        Me.Node46.Image = CType(resources.GetObject("Node46.Image"), System.Drawing.Image)
        Me.Node46.Name = "Node46"
        Me.Node46.Text = "Manipulate PDF"
        '
        'Node21
        '
        Me.Node21.Image = CType(resources.GetObject("Node21.Image"), System.Drawing.Image)
        Me.Node21.Name = "Node21"
        Me.Node21.Text = "Build Excel Workbook"
        '
        'Node22
        '
        Me.Node22.Image = CType(resources.GetObject("Node22.Image"), System.Drawing.Image)
        Me.Node22.Name = "Node22"
        Me.Node22.Text = "Merge Excel Files"
        '
        'Node47
        '
        Me.Node47.Expanded = True
        Me.Node47.Image = CType(resources.GetObject("Node47.Image"), System.Drawing.Image)
        Me.Node47.Name = "Node47"
        Me.Node47.Text = "Manipulate Excel"
        '
        'Node17
        '
        Me.Node17.Expanded = True
        Me.Node17.Image = CType(resources.GetObject("Node17.Image"), System.Drawing.Image)
        Me.Node17.Name = "Node17"
        Me.Node17.Text = "Upload to SharePoint"
        '
        'Node3
        '
        Me.Node3.Image = CType(resources.GetObject("Node3.Image"), System.Drawing.Image)
        Me.Node3.Name = "Node3"
        Me.Node3.Nodes.AddRange(New DevComponents.AdvTree.Node() {Me.Node23, Me.Node24, Me.Node25})
        Me.Node3.Text = "Internet"
        '
        'Node23
        '
        Me.Node23.Image = CType(resources.GetObject("Node23.Image"), System.Drawing.Image)
        Me.Node23.Name = "Node23"
        Me.Node23.Text = "Send Email"
        '
        'Node24
        '
        Me.Node24.Image = CType(resources.GetObject("Node24.Image"), System.Drawing.Image)
        Me.Node24.Name = "Node24"
        Me.Node24.Text = "Open Web Address"
        '
        'Node25
        '
        Me.Node25.Expanded = True
        Me.Node25.Name = "Node25"
        Me.Node25.Nodes.AddRange(New DevComponents.AdvTree.Node() {Me.Node26, Me.Node27, Me.Node28, Me.Node29, Me.Node30})
        Me.Node25.Text = "FTP"
        '
        'Node26
        '
        Me.Node26.Image = CType(resources.GetObject("Node26.Image"), System.Drawing.Image)
        Me.Node26.Name = "Node26"
        Me.Node26.Text = "Upload File(s)"
        '
        'Node27
        '
        Me.Node27.Image = CType(resources.GetObject("Node27.Image"), System.Drawing.Image)
        Me.Node27.Name = "Node27"
        Me.Node27.Text = "Download File"
        '
        'Node28
        '
        Me.Node28.Image = CType(resources.GetObject("Node28.Image"), System.Drawing.Image)
        Me.Node28.Name = "Node28"
        Me.Node28.Text = "Delete FTP File"
        '
        'Node29
        '
        Me.Node29.Image = CType(resources.GetObject("Node29.Image"), System.Drawing.Image)
        Me.Node29.Name = "Node29"
        Me.Node29.Text = "Create Directory"
        '
        'Node30
        '
        Me.Node30.Image = CType(resources.GetObject("Node30.Image"), System.Drawing.Image)
        Me.Node30.Name = "Node30"
        Me.Node30.Text = "Delete Directory"
        '
        'Node4
        '
        Me.Node4.Expanded = True
        Me.Node4.Image = CType(resources.GetObject("Node4.Image"), System.Drawing.Image)
        Me.Node4.Name = "Node4"
        Me.Node4.Nodes.AddRange(New DevComponents.AdvTree.Node() {Me.Node31, Me.Node45, Me.Node32, Me.Node33, Me.Node34, Me.Node35, Me.Node36, Me.Node49, Me.Node50, Me.Node37})
        Me.Node4.Text = "Database"
        '
        'Node31
        '
        Me.Node31.Image = CType(resources.GetObject("Node31.Image"), System.Drawing.Image)
        Me.Node31.Name = "Node31"
        Me.Node31.Text = "Execute SQL Script (from file)"
        '
        'Node45
        '
        Me.Node45.Expanded = True
        Me.Node45.Image = CType(resources.GetObject("Node45.Image"), System.Drawing.Image)
        Me.Node45.Name = "Node45"
        Me.Node45.Text = "Execute an SSIS Package"
        '
        'Node32
        '
        Me.Node32.Image = CType(resources.GetObject("Node32.Image"), System.Drawing.Image)
        Me.Node32.Name = "Node32"
        Me.Node32.Text = "Update a record"
        '
        'Node33
        '
        Me.Node33.Image = CType(resources.GetObject("Node33.Image"), System.Drawing.Image)
        Me.Node33.Name = "Node33"
        Me.Node33.Text = "Insert a record"
        '
        'Node34
        '
        Me.Node34.Image = CType(resources.GetObject("Node34.Image"), System.Drawing.Image)
        Me.Node34.Name = "Node34"
        Me.Node34.Text = "Delete a record"
        '
        'Node35
        '
        Me.Node35.Image = CType(resources.GetObject("Node35.Image"), System.Drawing.Image)
        Me.Node35.Name = "Node35"
        Me.Node35.Text = "Run a stored procedure"
        '
        'Node36
        '
        Me.Node36.Image = CType(resources.GetObject("Node36.Image"), System.Drawing.Image)
        Me.Node36.Name = "Node36"
        Me.Node36.Text = "Export data to a report"
        '
        'Node49
        '
        Me.Node49.Expanded = True
        Me.Node49.Image = CType(resources.GetObject("Node49.Image"), System.Drawing.Image)
        Me.Node49.Name = "Node49"
        Me.Node49.Text = "Save BLOB to SQL Server"
        '
        'Node50
        '
        Me.Node50.Expanded = True
        Me.Node50.Image = CType(resources.GetObject("Node50.Image"), System.Drawing.Image)
        Me.Node50.Name = "Node50"
        Me.Node50.Text = "Get BLOB from SQL Server"
        '
        'Node37
        '
        Me.Node37.Image = CType(resources.GetObject("Node37.Image"), System.Drawing.Image)
        Me.Node37.Name = "Node37"
        Me.Node37.Nodes.AddRange(New DevComponents.AdvTree.Node() {Me.Node38, Me.Node39, Me.Node40, Me.Node41})
        Me.Node37.Text = "Table"
        '
        'Node38
        '
        Me.Node38.Image = CType(resources.GetObject("Node38.Image"), System.Drawing.Image)
        Me.Node38.Name = "Node38"
        Me.Node38.Text = "Create a table"
        '
        'Node39
        '
        Me.Node39.Image = CType(resources.GetObject("Node39.Image"), System.Drawing.Image)
        Me.Node39.Name = "Node39"
        Me.Node39.Text = "Delete a table"
        '
        'Node40
        '
        Me.Node40.Image = CType(resources.GetObject("Node40.Image"), System.Drawing.Image)
        Me.Node40.Name = "Node40"
        Me.Node40.Text = "Add column to table"
        '
        'Node41
        '
        Me.Node41.Image = CType(resources.GetObject("Node41.Image"), System.Drawing.Image)
        Me.Node41.Name = "Node41"
        Me.Node41.Text = "Delete column from table"
        '
        'Node5
        '
        Me.Node5.Expanded = True
        Me.Node5.Image = CType(resources.GetObject("Node5.Image"), System.Drawing.Image)
        Me.Node5.Name = "Node5"
        Me.Node5.Nodes.AddRange(New DevComponents.AdvTree.Node() {Me.Node42, Me.Node43, Me.Node44})
        Me.Node5.Text = "Registry"
        '
        'Node42
        '
        Me.Node42.Image = CType(resources.GetObject("Node42.Image"), System.Drawing.Image)
        Me.Node42.Name = "Node42"
        Me.Node42.Text = "Set Value"
        '
        'Node43
        '
        Me.Node43.Image = CType(resources.GetObject("Node43.Image"), System.Drawing.Image)
        Me.Node43.Name = "Node43"
        Me.Node43.Text = "Delete Key"
        '
        'Node44
        '
        Me.Node44.Image = CType(resources.GetObject("Node44.Image"), System.Drawing.Image)
        Me.Node44.Name = "Node44"
        Me.Node44.Text = "Create Key"
        '
        'NodeConnector1
        '
        Me.NodeConnector1.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid
        Me.NodeConnector1.LineColor = System.Drawing.Color.Transparent
        '
        'ElementStyle1
        '
        Me.ElementStyle1.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle1.Name = "ElementStyle1"
        Me.ElementStyle1.TextColor = System.Drawing.SystemColors.ControlText
        '
        'tvTasks
        '
        Me.tvTasks.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.tvTasks.AllowDrop = True
        Me.tvTasks.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tvTasks.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.tvTasks.BackgroundStyle.Class = "TreeBorderKey"
        Me.tvTasks.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.tvTasks.Columns.Add(Me.ColumnHeader1)
        Me.tvTasks.Columns.Add(Me.ColumnHeader2)
        Me.tvTasks.Columns.Add(Me.ColumnHeader3)
        Me.tvTasks.ContextMenuStrip = Me.mnuTest
        Me.tvTasks.DragDropEnabled = False
        Me.tvTasks.DragDropNodeCopyEnabled = False
        Me.tvTasks.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.tvTasks.Location = New System.Drawing.Point(237, 8)
        Me.tvTasks.Name = "tvTasks"
        Me.tvTasks.NodesConnector = Me.NodeConnector2
        Me.tvTasks.NodeStyle = Me.ElementStyle2
        Me.tvTasks.PathSeparator = ";"
        Me.tvTasks.Size = New System.Drawing.Size(318, 315)
        Me.tvTasks.Styles.Add(Me.ElementStyle2)
        Me.tvTasks.TabIndex = 9
        Me.tvTasks.Text = "AdvTree1"
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Name = "ColumnHeader1"
        Me.ColumnHeader1.Text = "Name"
        Me.ColumnHeader1.Width.Absolute = 150
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Name = "ColumnHeader2"
        Me.ColumnHeader2.Text = "Run"
        Me.ColumnHeader2.Width.Absolute = 75
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Name = "ColumnHeader3"
        Me.ColumnHeader3.Text = "After Type"
        Me.ColumnHeader3.Width.Absolute = 75
        '
        'NodeConnector2
        '
        Me.NodeConnector2.LineColor = System.Drawing.SystemColors.ControlText
        '
        'ElementStyle2
        '
        Me.ElementStyle2.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle2.Name = "ElementStyle2"
        Me.ElementStyle2.TextColor = System.Drawing.SystemColors.ControlText
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        '
        'Node51
        '
        Me.Node51.Expanded = True
        Me.Node51.Image = CType(resources.GetObject("Node51.Image"), System.Drawing.Image)
        Me.Node51.Name = "Node51"
        Me.Node51.Text = "Create Exchange Appointment"
        '
        'ucTasks
        '
        Me.BackColor = System.Drawing.Color.White
        Me.Controls.Add(Me.tvTasks)
        Me.Controls.Add(Me.tvTaskTypes)
        Me.Controls.Add(Me.cmdDelete)
        Me.Controls.Add(Me.cmdDown)
        Me.Controls.Add(Me.cmdUp)
        Me.Controls.Add(Me.cmdEdit)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.cmdLoad)
        Me.Controls.Add(Me.cmdAdd)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.Name = "ucTasks"
        Me.Size = New System.Drawing.Size(563, 363)
        Me.mnuTest.ResumeLayout(False)
        Me.mnuType.ResumeLayout(False)
        CType(Me.tvTaskTypes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tvTasks, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        'If gnEdition < gEdition.GOLD And Me.m_defaultTaks = True Then
        '    _NeedUpgrade(gEdition.GOLD)
        '    Return
        'End If

        Dim oTasks As frmTaskSelection = New frmTaskSelection

        oTasks.ShowAfter = ShowAfterType
        oTasks.ScheduleID = ScheduleID
        oTasks.oAuto = oAuto
        oTasks.m_eventBased = Me.m_eventBased
        oTasks.m_eventID = Me.m_eventID

        oTasks.ShowDialog()

        LoadTasks()
    End Sub

    Public Sub AddTasks()
        Dim oTasks As frmTaskSelection = New frmTaskSelection

        oTasks.ScheduleID = ScheduleID
        oTasks.oAuto = oAuto
        oTasks.ShowDialog()

        LoadTasks()
    End Sub
    Public Sub LoadTasks()
        Try
10:         Me.loaded = False

            Dim SQL As String
20:         Dim oData As clsMarsData = New clsMarsData
            Dim sWhere As String = " WHERE ScheduleID = " & ScheduleID & " AND ISNULL(forexception, 0) = " & Convert.ToInt32(m_forExceptionHandling)
            Dim sOrder As String = " ORDER BY OrderID"
            Dim tvItem As DevComponents.AdvTree.Node
            Dim sTaskType As String
            Dim nTaskID As Integer
            Dim selected As DevComponents.AdvTree.Node

            If gConType = "DAT" Then
                sWhere = " WHERE ScheduleID = " & ScheduleID & " AND forexception = " & Convert.ToInt32(m_forExceptionHandling)
            End If
            If tvTasks.SelectedNodes.Count > 0 Then selected = tvTasks.SelectedNode

30:         If tvTasks.Nodes.Count > 0 Then tvTasks.Nodes.Clear()

40:         SQL = "SELECT * FROM Tasks " & sWhere & sOrder

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

50:         If Not oRs Is Nothing Then
60:             Do While oRs.EOF = False
70:                 tvItem = New DevComponents.AdvTree.Node

80:                 sTaskType = IsNull(oRs("tasktype").Value)
90:                 nTaskID = oRs("taskid").Value

                    tvItem.DataKey = nTaskID
                    tvItem.Tag = sTaskType
100:                tvItem.Text = oRs("taskname").Value

                    tvItem.CheckBoxVisible = True

110:                Try
120:                    tvItem.Checked = IsNull(oRs("enabledstatus").Value, 1)
130:                Catch ex As Exception
140:                    tvItem.Checked = True
                    End Try

                    If IO.File.Exists(IO.Path.Combine(sAppPath, "Assets\Tasks\" & sTaskType & ".png")) Then
                        tvItem.Image = Image.FromFile(IO.Path.Combine(sAppPath, "Assets\Tasks\" & sTaskType & ".png"))
                    End If

770:                With tvItem.Cells
780:                    .Add(New DevComponents.AdvTree.Cell(IsNull(oRs("runwhen").Value)))
790:                    .Add(New DevComponents.AdvTree.Cell(IsNull(oRs("aftertype").Value)))
                    End With

800:                tvTasks.Nodes.Add(tvItem)

810:                oRs.MoveNext()
820:            Loop

830:            oRs.Close()

                If selected IsNot Nothing Then
                    For Each it As DevComponents.AdvTree.Node In Me.tvTasks.Nodes
                        If it.DataKey = selected.DataKey Then
                            tvTasks.SelectedNode = it
                            it.EnsureVisible()
                            Exit For
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        Finally
840:        Me.loaded = True
        End Try
    End Sub


    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles cmdEdit.Click
        If tvTasks.SelectedNodes.Count = 0 Then Exit Sub

        Dim sTaskType As String
        Dim nTaskID As Integer
        Dim lsv As DevComponents.AdvTree.Node

        lsv = tvTasks.SelectedNode

        sTaskType = lsv.Tag 'GetDelimitedWord(lsv.Tag, 1, ":")

        nTaskID = lsv.DataKey 'GetDelimitedWord(lsv.Tag, 2, ":")

        Select Case sTaskType.ToLower
            Case "createappointment"
                Dim form As frmTaskExchangeAppointment = New frmTaskExchangeAppointment
                hideAfterType(form)
                form.EditTask(nTaskID, ShowAfterType)
            Case "downloadblob"
                Dim oform As frmTaskDownloadBlobFromSQLServer = New frmTaskDownloadBlobFromSQLServer
                hideAfterType(oform)
                oform.editTask(nTaskID, ShowAfterType)
            Case "uploadblob"
                Dim oform As frmTaskSaveBlobToSQLServer = New frmTaskSaveBlobToSQLServer
                hideAfterType(oform)
                oform.editTask(nTaskID, ShowAfterType)
            Case "folderhousekeeping"
                Dim oform As frmTaskHouseKeeping = New frmTaskHouseKeeping
                hideAfterType(oform)
                oform.editTask(nTaskID, ShowAfterType)
            Case "editexcel"
                Dim oform As frmTaskExcelProperties = New frmTaskExcelProperties
                hideAfterType(oform)
                oform.EditTask(nTaskID, ShowAfterType)
            Case "editpdf"
                Dim oform As frmTaskPDFProperties = New frmTaskPDFProperties
                hideAfterType(oform)
                oform.EditTask(nTaskID, ShowAfterType)
            Case "executessis"
                Dim oform As frmTaskExecuteSSISPackage = New frmTaskExecuteSSISPackage
                hideAfterType(oform)
                oform.EditTask(nTaskID, ShowAfterType)
            Case "copyfile"
                Dim oForm As New frmTaskCopyFile
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "deletefile"
                Dim oForm As New frmTaskDeleteFile
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "sendmail"
                Dim oForm As New frmTaskEmail
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "createfolder"
                Dim oForm As New frmTaskFolderCreate
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "movefolder"
                Dim oForm As New frmTaskFolderMove
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "pause"
                Dim oForm As New frmTaskPause
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "print"
                Dim oForm As New frmTaskPrint
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "program"
                Dim oForm As New frmTaskProgram
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "renamefile"
                Dim oForm As New frmTaskRenameFile
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "webbrowse"
                Dim oForm As New frmTaskWebBrowse
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "writefile"
                Dim oForm As New frmTaskWriteFile
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "sqlscript"
                Dim oForm As New frmTaskSQLScript
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "sqlproc"
                Dim oForm As New frmTaskSQLProc
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "dbupdate"
                Dim oForm As New frmTaskDBUpdate
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "dbinsert"
                Dim oForm As New frmTaskDBInsert
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "dbdelete"
                Dim oForm As New frmTaskDBDelete
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "dbcreatetable"
                Dim oForm As New frmTaskTableCreate
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "dbdeletetable"
                Dim oForm As New frmTaskTableDelete
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "dbmodifytable_add", "dbmodifytable_drop"
                Dim oForm As New frmTaskTableModify
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "ftpupload"
                Dim oForm As New frmTaskFTPUpload
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "ftpdownload"
                Dim oForm As New frmTaskFTPDownload
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "ftpdeletefile"
                Dim oForm As New frmTaskFTPDeleteFile
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "ftpcreatedirectory"
                Dim oForm As New frmTaskFTPCreateDirectory
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "ftpdeletedirectory"
                Dim oForm As New frmTaskFTPDeleteDirectory
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "registrycreatekey"
                Dim oForm As New frmTaskAddKey
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "registrydeletekey"
                Dim oForm As New frmTaskDeleteKey
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "registrysetkey"
                Dim oForm As New frmTaskSetKey
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "runschedule"
                Dim oForm As New frmTaskRunSchedule
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "zipfiles"
                Dim oForm As New frmTaskZip
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "unzipfile"
                Dim oForm As New frmTaskUnzip
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "mergepdf"
                Dim oForm As New frmTaskPDFMerge
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "sendsms"
                Dim oForm As New frmTaskSMS
                hideAfterType(oForm)
                oForm.EditTask(nTaskID, ShowAfterType)
            Case "dbexport"
                Dim task As frmTaskSQLExport = New frmTaskSQLExport
                hideAfterType(task)
                task.EditTask(nTaskID, ShowAfterType)
            Case "buildworkbook"
                Dim task As frmTaskBuildWorkBook = New frmTaskBuildWorkBook
                hideAfterType(task)
                task.EditTask(nTaskID, ShowAfterType)
            Case "mergexls"
                Dim task As frmTaskExcelMerge = New frmTaskExcelMerge
                hideAfterType(task)
                task.EditTask(nTaskID, ShowAfterType)
            Case "sharepointupload"

                Dim task As frmTaskUploadToSharepoint = New frmTaskUploadToSharepoint
                hideAfterType(task)
                task.editTask(nTaskID, ShowAfterType)
        End Select

        LoadTasks()
    End Sub

    Private Sub ucTasks_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        For Each nd As DevComponents.AdvTree.Node In tvTaskTypes.Nodes
            nd.Expand()
        Next

    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim nTaskID As Integer
        Dim lsv As DevComponents.AdvTree.Node
        Dim arr As ArrayList = New ArrayList

        If tvTasks.SelectedNodes.Count = 0 Then Exit Sub

        If MessageBox.Show("Delete the selected task(s)?", _
        Application.ProductName, MessageBoxButtons.YesNo, _
        MessageBoxIcon.Question) = DialogResult.Yes Then

            For Each lsv In tvTasks.SelectedNodes

                nTaskID = lsv.datakey 'GetDelimitedWord(lsv.Tag, 2, ":")

                oTask.DeleteTask(nTaskID)

                clsMarsData.WriteData("DELETE FROM taskoptions WHERE taskid = " & nTaskID)

                arr.Add(lsv)
            Next

            For Each lsv In arr
                lsv.Remove()
            Next
        End If
    End Sub

    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        If tvTasks.SelectedNodes.Count = 0 Then Exit Sub

        Dim nTaskID As Integer
        Dim lsv As DevComponents.AdvTree.Node

        lsv = tvTasks.SelectedNode

        Dim selTag As String = lsv.Tag

        nTaskID = lsv.DataKey


        If lsv.Index = 0 Then Return

        'add everything from 0 to the prevnode
        Dim arr As ArrayList = New ArrayList

        For I As Integer = 0 To lsv.PrevNode.Index - 1
            arr.Add(tvTasks.Nodes(I))
        Next

        'add the node to be moved
        arr.Add(lsv)

        'add what was the prev node
        arr.Add(lsv.PrevNode)

        'add remaing
        If lsv.NextNode IsNot Nothing Then
            For I As Integer = lsv.NextNode.Index To tvTasks.Nodes.Count - 1
                arr.Add(tvTasks.Nodes(I))
            Next
        End If

        tvTasks.Nodes.Clear()

        Dim orderID As Integer = 0

        For Each nn As DevComponents.AdvTree.Node In arr
            tvTasks.Nodes.Add(nn)

            If nn.DataKey = nTaskID Then
                tvTasks.SelectedNode = nn
                nn.EnsureVisible()
            End If

            clsMarsData.WriteData(String.Format("UPDATE tasks SET orderid = {0} WHERE taskid = {1}", orderID, nn.DataKey))

            orderID += 1
        Next

    End Sub

    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        If tvTasks.SelectedNodes.Count = 0 Then Exit Sub

        If tvTasks.SelectedNode.Index = tvTasks.Nodes.Count - 1 Then
            Return
        End If

        Dim nTaskID As Integer
        Dim lsv As DevComponents.AdvTree.Node

        lsv = tvTasks.SelectedNodes(0)

        Dim selTag As String = lsv.Tag

        nTaskID = lsv.DataKey

        'add everything from 0 to the prevnode
        Dim arr As ArrayList = New ArrayList

        If lsv.PrevNode IsNot Nothing Then
            For I As Integer = 0 To lsv.PrevNode.Index
                arr.Add(tvTasks.Nodes(I))
            Next
        End If

        'add next node
        arr.Add(lsv.NextNode)

        'add us
        arr.Add(lsv)

        'add remaining
        If lsv.NextNode.Index + 1 <= tvTasks.Nodes.Count - 1 Then
            For I As Integer = lsv.NextNode.Index + 1 To tvTasks.Nodes.Count - 1
                arr.Add(tvTasks.Nodes(I))
            Next
        End If

        tvTasks.Nodes.Clear()

        Dim orderID As Integer = 0

        For Each nn As DevComponents.AdvTree.Node In arr
            tvTasks.Nodes.Add(nn)

            If nn.DataKey = nTaskID Then
                tvTasks.SelectedNode = nn
                nn.EnsureVisible()
            End If

            clsMarsData.WriteData(String.Format("UPDATE tasks SET orderid = {0} WHERE taskid = {1}", orderID, nn.DataKey))

            orderID += 1
        Next
    End Sub

   

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        SQL = "SELECT * FROM Tasks WHERE " & _
        "ScheduleID = " & ScheduleID & " ORDER BY OrderID"

        Dim oData As New clsMarsData

        oRs = clsMarsData.GetData(SQL)

        Try
            If oRs.EOF = False Then

                Dim sPath As String = sAppPath & "\Tasks\"

                If System.IO.Directory.Exists(sPath) = False Then
                    System.IO.Directory.CreateDirectory(sPath)
                End If

                Dim sName As String = InputBox("Please enter the name of the task list", Application.ProductName)

                If sName.Length = 0 Then Return

                sPath &= sName

                If sPath.EndsWith(".xml") = False Then
                    sPath &= ".xml"
                End If

                If System.IO.File.Exists(sPath) = True Then
                    If MessageBox.Show("A task list with that name already " & _
                    "exists. Ok to overwrite?", Application.ProductName, _
                    MessageBoxButtons.YesNo, _
                    MessageBoxIcon.Question) = DialogResult.No Then
                        Return
                    End If
                End If

                oData.CreateXML(oRs, sPath)

                MessageBox.Show("Task list saved successfully")
            End If
        Catch ex As Exception
            _ErrorHandle("Failed to export the task list (" & ex.Message & ")", _
            Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try

    End Sub

    Private Sub cmdLoad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoad.Click

    End Sub

    Private Sub ImportTaskListToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImportTaskListToolStripMenuItem.Click
        Dim oForm As New frmTaskManager
        Dim sReturn As String

        sReturn = oForm._GetTaskListPath

        If sReturn.Length = 0 Then Return

        Dim oData As New clsMarsData

        Dim oRs As ADODB.Recordset = oData.GetXML(sReturn)

        If oRs Is Nothing Then Return

        Dim sCols As String
        Dim sVals As String
        Dim I As Integer
        Dim oTask As New clsMarsTask
        Dim SQL As String
        Dim nPort As Integer = 0
        Dim nReplace As Integer = 0
        Dim nPause As Integer = 0
        Dim runWhen As String = "AFTER"
        Dim afterType As String = "Delivery"
        Dim status As Integer = 1

        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramPath," & _
        "ProgramParameters,WindowStyle,PrinterName,FileList,PauseInt," & _
        "ReplaceFiles,Msg,SendTo,Cc,Bcc,Subject,OrderID,FTPServer,FtpPort," & _
        "FTPUser,FTPPassword,FTPDirectory,RunWhen,AfterType,EnabledStatus,Forexception"

        Do While oRs.EOF = False
            Try
                nPort = oRs("ftpport").Value
            Catch ex As Exception
                nPort = 0
            End Try

            Try
                nReplace = oRs("replacefiles").Value
            Catch ex As Exception
                nReplace = 0
            End Try

            Try
                nPause = oRs("pauseint").Value
            Catch ex As Exception
                nPause = 0
            End Try

            Try
                status = oRs("enabledstatus").Value
            Catch ex As Exception
                status = 1
            End Try

            Try
                afterType = oRs("aftertype").Value
            Catch : End Try

            Try
                runWhen = oRs("runwhen").Value
            Catch : End Try

            sVals = clsMarsData.CreateDataID("tasks", "taskid") & "," & _
            ScheduleID & "," & _
            "'" & SQLPrepare(oRs("tasktype").Value) & "'," & _
            "'" & SQLPrepare(oRs("taskname").Value) & "'," & _
            "'" & SQLPrepare(oRs("programpath").Value) & "'," & _
            "'" & SQLPrepare(oRs("programparameters").Value) & "'," & _
            "'" & SQLPrepare(oRs("windowstyle").Value) & "'," & _
            "'" & SQLPrepare(oRs("printername").Value) & "'," & _
            "'" & SQLPrepare(oRs("filelist").Value) & "'," & _
            nPause & "," & _
            nReplace & "," & _
            "'" & SQLPrepare(oRs("msg").Value) & "'," & _
            "'" & SQLPrepare(oRs("sendto").Value) & "'," & _
            "'" & SQLPrepare(oRs("cc").Value) & "'," & _
            "'" & SQLPrepare(oRs("bcc").Value) & "'," & _
            "'" & SQLPrepare(oRs("subject").Value) & "'," & _
            oTask.GetOrderID(ScheduleID) & "," & _
            "'" & SQLPrepare(oRs("ftpserver").Value) & "'," & _
            nPort & "," & _
            "'" & SQLPrepare(oRs("ftpuser").Value) & "'," & _
            "'" & SQLPrepare(oRs("ftppassword").Value) & "'," & _
            "'" & SQLPrepare(oRs("ftpdirectory").Value) & "'," & _
            "'" & runWhen & "'," & _
            "'" & afterType & "'," & _
            status & "," & _
            Convert.ToInt32(m_forExceptionHandling)

            SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

            ''console.writeline(SQL)

            If clsMarsData.WriteData(SQL) = False Then
                Exit Sub
            End If

            oRs.MoveNext()
        Loop

        oRs.Close()

        LoadTasks()
    End Sub

    Private Sub cmdLoad_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdLoad.MouseUp
        mnuType.Show(cmdLoad, New Point(e.X, e.Y))
    End Sub

    Private Sub DefaultTaskToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DefaultTaskToolStripMenuItem.Click
        Dim odefault As New frmDefaultTasks

        Dim items As DevComponents.AdvTree.NodeCollection

        items = odefault.GetDefaultTask

        If items IsNot Nothing Then
            For Each item As DevComponents.AdvTree.Node In items
                Dim taskID As Integer = item.DataKey

                Dim overWriter As Hashtable

                If m_forExceptionHandling Then
                    overWriter = New Hashtable

                    overWriter.Add("forexception", 1)
                End If

                clsMarsData.DataItem.CopyRow("Tasks", taskID, " WHERE TaskID =" & taskID, "TaskID", _
                , , , , , , ScheduleID, , , , , , , overWriter)

            Next

            LoadTasks()
        End If
    End Sub


    Private Sub TestToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TestToolStripMenuItem.Click
        If tvTasks.SelectedNodes.Count > 0 Then
            Dim res As DialogResult = MessageBox.Show("Are you sure you would like to run the selected actions?", _
                                                       Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If res = DialogResult.No Then Return
        End If

        For Each it As DevComponents.AdvTree.Node In tvTasks.SelectedNodes
            Dim tag As String = it.Tag
            Dim taskID As Integer = it.DataKey
            Dim tsk As clsMarsTask = New clsMarsTask

            tsk.ProcessTasks(0, "", clsMarsTask.enAfterType.DELIVERY, taskID)
        Next

        MessageBox.Show("The selected actions have completed", Application.ProductName, _
                          MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub tvTaskTypes_DoubleClick(sender As Object, e As System.EventArgs) Handles tvTaskTypes.DoubleClick
        If tvTaskTypes.SelectedNode Is Nothing Then Return

        Dim inserter As frmInserter = New frmInserter(m_eventID)
        Dim newTaskID As Integer = 0

        inserter.m_EventBased = m_eventBased

        DoAddTask(tvTaskTypes.SelectedNode.Text.ToLower)

    End Sub

    Private Sub DoAddTask(type As String)

        If gRole.ToLower <> "administrator" Then
            Try

                Dim path As String = tvTaskTypes.SelectedNode.FullPath '//e.g general\execute
                Dim rootParent As String = path.Split(";")(0)
                Dim grp As usergroup = New usergroup(gRole)
                Dim allowedTasks As ArrayList = grp.getUserAllowedTaskTypes


                If allowedTasks.Contains(rootParent) = False Then
                    MessageBox.Show("The group '" & gRole & "' does not have access to '" & rootParent & "' Custom Tasks.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If

            Catch : End Try
        End If

        Dim newTaskID As Integer = 0

        Select Case type.ToLower
            Case "create exchange appointment"
                Dim form As frmTaskExchangeAppointment = New frmTaskExchangeAppointment
                form.m_eventBased = m_eventBased
                form.m_eventID = m_eventID

                hideAfterType(form)

                newTaskID = form.AddTask(ScheduleID, ShowAfterType)
            Case "get blob from sql server"
                If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.ca3_DatabaseCustomActions) = False Then
                    _NeedUpgrade(gEdition.ENTERPRISEPROPLUS, tvTaskTypes, "Database Custom Actions")
                    Return
                End If

                Dim form As frmTaskDownloadBlobFromSQLServer = New frmTaskDownloadBlobFromSQLServer
                form.m_eventBased = m_eventBased
                form.m_eventID = m_eventID

                hideAfterType(form)

                newTaskID = form.addTask(ScheduleID, ShowAfterType)
            Case "save blob to sql server"
                If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.ca3_DatabaseCustomActions) = False Then
                    _NeedUpgrade(gEdition.ENTERPRISEPROPLUS, tvTaskTypes, "Database Custom Actions")
                    Return
                End If

                Dim form As frmTaskSaveBlobToSQLServer = New frmTaskSaveBlobToSQLServer
                form.m_eventBased = m_eventBased
                form.m_eventID = m_eventID

                hideAfterType(form)

                newTaskID = form.addTask(ScheduleID, ShowAfterType)
            Case "folder housekeeping"
                If IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.sa7_FolderHousekeeping) = False Then
                    _NeedUpgrade(gEdition.ENTERPRISE, tvTaskTypes, "Folder Housekeeping")
                    Return
                End If

                Dim form As frmTaskHouseKeeping = New frmTaskHouseKeeping
                form.m_eventBased = m_eventBased
                form.m_eventID = m_eventID
                hideAfterType(form)

                newTaskID = form.AddTask(ScheduleID, ShowAfterType)
            Case "manipulate excel"
                If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.xl1_AdvancedXLPack) = False Then
                    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, tvTaskTypes, "Advanced Excel Pack")
                    Return
                End If

                Dim oForm As frmTaskExcelProperties = New frmTaskExcelProperties
                oForm.m_eventBased = m_eventBased
                oForm.m_eventID = m_eventID
                hideAfterType(oForm)

                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "manipulate pdf"
                If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.pd1_AdvancedPDFPack) = False Then
                    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, tvTaskTypes, "Advanced PDF Pack")
                    Return
                End If

                Dim oForm As frmTaskPDFProperties = New frmTaskPDFProperties
                oForm.m_eventBased = m_eventBased
                oForm.m_eventID = m_eventID
                hideAfterType(oForm)

                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "execute an ssis package"

                If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.ca3_DatabaseCustomActions) = False Then
                    _NeedUpgrade(gEdition.ENTERPRISEPROPLUS, tvTaskTypes, "Custom Actions - Database")
                    Return
                End If

                Dim oForm As frmTaskExecuteSSISPackage = New frmTaskExecuteSSISPackage
                oForm.m_eventBased = Me.m_eventBased
                oForm.m_eventID = Me.m_eventID
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "run program/open document"
                Dim oForm As frmTaskProgram = New frmTaskProgram
                oForm.m_eventBased = Me.m_eventBased
                oForm.m_eventID = Me.m_eventID
                hideAfterType(oForm)

                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)

            Case "print document"

                Dim oForm As frmTaskPrint = New frmTaskPrint
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "wait/pause"

                Dim oForm As frmTaskPause = New frmTaskPause
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "copy file"

                Dim oForm As frmTaskCopyFile = New frmTaskCopyFile
                oForm.m_eventID = Me.eventID
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "rename/move file"

                Dim oForm As frmTaskRenameFile = New frmTaskRenameFile
                oForm.m_eventID = Me.eventID
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "delete file"

                Dim oForm As frmTaskDeleteFile = New frmTaskDeleteFile
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "write text file"



                Dim oForm As frmTaskWriteFile = New frmTaskWriteFile
                oForm.m_eventBased = Me.m_eventBased
                oForm.m_eventID = Me.m_eventID
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "create folder"



                Dim oForm As frmTaskFolderCreate = New frmTaskFolderCreate
                oForm.m_EventID = Me.eventID
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "send email"

                Dim oForm As frmTaskEmail = New frmTaskEmail
                oForm.m_eventBased = Me.m_eventBased
                oForm.m_eventID = Me.m_eventID
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "open web address"

                Dim oForm As frmTaskWebBrowse = New frmTaskWebBrowse
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "execute sql script (from file)"
                If oAuto = False Then
                    If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, tvTaskTypes, "Custom Actions - Database")
                        Return
                    End If
                Else
                    If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, tvTaskTypes, "Custom Actions - Database")
                        Return
                    End If

                End If

                Dim oForm As frmTaskSQLScript = New frmTaskSQLScript
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "run a stored procedure"
                If oAuto = False Then
                    If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, tvTaskTypes, "Custom Actions - Database")
                        Return
                    End If
                Else
                    If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, tvTaskTypes, "Custom Actions - Database")
                        Return
                    End If

                End If
                Dim oForm As frmTaskSQLProc = New frmTaskSQLProc
                oForm.m_eventID = Me.eventID
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "update a record"
                If oAuto = False Then
                    If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, tvTaskTypes, "Custom Actions - Database")
                        Return
                    End If
                Else
                    If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, tvTaskTypes, "Custom Actions - Database")
                        Return
                    End If
                End If

                Dim oForm As frmTaskDBUpdate = New frmTaskDBUpdate
                oForm.m_eventBased = Me.m_eventBased
                oForm.m_eventID = Me.m_eventID
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "insert a record"
                If oAuto = False Then
                    If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, tvTaskTypes, "Custom Actions - Database")
                        Return
                    End If
                Else
                    If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, tvTaskTypes, "Custom Actions - Database")
                        Return
                    End If
                End If
                Dim oForm As frmTaskDBInsert = New frmTaskDBInsert
                oForm.m_eventBased = Me.m_eventBased
                oForm.m_eventID = Me.m_eventID
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "delete a record"
                If oAuto = False Then
                    If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, tvTaskTypes, "Custom Actions - Database")
                        Return
                    End If
                Else
                    If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, tvTaskTypes, "Custom Actions - Database")
                        Return
                    End If
                End If

                Dim oForm As frmTaskDBDelete = New frmTaskDBDelete
                oForm.m_eventBased = Me.m_eventBased
                oForm.m_eventID = Me.m_eventID
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "create a table"

                If oAuto = False Then
                    If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, tvTaskTypes, "Custom Actions - Database")
                        Return
                    End If
                Else
                    If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, tvTaskTypes, "Custom Actions - Database")
                        Return
                    End If
                End If

                Dim oForm As frmTaskTableCreate = New frmTaskTableCreate

                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "delete a table"

                If oAuto = False Then
                    If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, tvTaskTypes, "Custom Actions - Database")
                        Return
                    End If
                Else
                    If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, tvTaskTypes, "Custom Actions - Database")
                        Return
                    End If
                End If

                Dim oForm As frmTaskTableDelete = New frmTaskTableDelete
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "add column to table"
                If oAuto = False Then
                    If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, tvTaskTypes, "Custom Actions - Database")
                        Return
                    End If
                Else
                    If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, tvTaskTypes, "Custom Actions - Database")
                        Return
                    End If
                End If

                Dim oForm As frmTaskTableModify = New frmTaskTableModify

                oForm.cmbModifyType.Text = "ADD COLUMN"
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "delete column from table"
                If oAuto = False Then
                    If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, tvTaskTypes, "Custom Actions - Database")
                        Return
                    End If
                Else
                    If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, tvTaskTypes, "Custom Actions - Database")
                        Return
                    End If
                End If

                Dim oForm As frmTaskTableModify = New frmTaskTableModify

                oForm.cmbModifyType.Text = "DROP COLUMN"
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "upload file(s)"

                If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.f1_FTPDestination) = False And IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.ca4_FTPCustomAction) = False Then
                    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, tvTaskTypes, "Custom Action - FTP")
                    Return
                End If


                Dim oForm As New frmTaskFTPUpload
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "download file"

                If oAuto = False Then
                    If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.f1_FTPDestination) = False And IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.ca4_FTPCustomAction) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, tvTaskTypes, "Custom Actions - FTP")
                        Return
                    End If
                Else
                    If IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.f1_FTPDestination) = False And IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.ca4_FTPCustomAction) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE, tvTaskTypes, "Custom Actions - FTP")
                        Return
                    End If
                End If

                Dim oForm As New frmTaskFTPDownload
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "delete ftp file"

                If oAuto = False Then
                    If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.f1_FTPDestination) = False And IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.ca4_FTPCustomAction) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, tvTaskTypes, "Custom Actions - FTP")
                        Return
                    End If
                Else
                    If IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.f1_FTPDestination) = False And IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.ca4_FTPCustomAction) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE, tvTaskTypes, "Custom Actions - FTP")
                        Return
                    End If
                End If

                Dim oForm As New frmTaskFTPDeleteFile
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "create directory"

                If oAuto = False Then
                    If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.f1_FTPDestination) = False And IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.ca4_FTPCustomAction) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, tvTaskTypes, "Custom Actions - FTP")
                        Return
                    End If
                Else
                    If IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.f1_FTPDestination) = False And IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.ca4_FTPCustomAction) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE, tvTaskTypes, "Custom Actions - FTP")
                        Return
                    End If
                End If

                Dim oForm As New frmTaskFTPCreateDirectory
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "delete directory"

                If oAuto = False Then
                    If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.f1_FTPDestination) = False And IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.ca4_FTPCustomAction) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, tvTaskTypes, "Custom Actions - FTP")
                        Return
                    End If
                Else
                    If IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.f1_FTPDestination) = False And IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.ca4_FTPCustomAction) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE, tvTaskTypes, "Custom Actions - FTP")
                        Return
                    End If
                End If

                Dim oForm As New frmTaskFTPDeleteDirectory
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "create key"

                If oAuto = False Then
                    If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca5_RegistryCustomAction) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, tvTaskTypes, "Custom Actions - Registry")
                        Return
                    End If
                Else
                    If IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.ca5_RegistryCustomAction) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE, tvTaskTypes, "Custom Actions - Registry")
                        Return
                    End If
                End If

                Dim oForm As New frmTaskAddKey
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "delete key"

                If oAuto = False Then
                    If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca5_RegistryCustomAction) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, tvTaskTypes, "Custom Actions - Registry")
                        Return
                    End If
                Else
                    If IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.ca5_RegistryCustomAction) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE, tvTaskTypes, "Custom Actions - Registry")
                        Return
                    End If
                End If

                Dim oForm As New frmTaskDeleteKey
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "set value"

                If oAuto = False Then
                    If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca5_RegistryCustomAction) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, tvTaskTypes, "Custom Actions - Registry")
                        Return
                    End If
                Else
                    If IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.ca5_RegistryCustomAction) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE, tvTaskTypes, "Custom Actions - Registry")
                        Return
                    End If
                End If

                Dim oForm As New frmTaskSetKey
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "execute schedule"

                If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca2_SchedulesCustomAction) = False Then
                    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, tvTaskTypes, "Custom Actions - Schedules")
                    Return
                End If

                Dim oForm As New frmTaskRunSchedule
                hideAfterType(oForm)

                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "rename/move folder"


                Dim oForm As New frmTaskFolderMove
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "zip files"
                Dim oForm As New frmTaskZip
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "unzip file"
                Dim oForm As New frmTaskUnzip
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "merge pdf files"
                If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.pd1_AdvancedPDFPack) = False And IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.ca6_PDFCustomAction) = False Then
                    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, tvTaskTypes, "Advanced PDF Pack")
                    Return
                End If

                Dim oForm As New frmTaskPDFMerge
                oForm.m_eventBased = Me.m_eventBased
                oForm.m_eventID = Me.m_eventID
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "send sms"
                'If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.o1) = False Then
                '    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
                '    Return
                'End If

                Dim oForm As frmTaskSMS = New frmTaskSMS
                hideAfterType(oForm)
                newTaskID = oForm.AddTask(ScheduleID, ShowAfterType)
            Case "export data to a report"
                If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, tvTaskTypes, "Custom Actions - Database")
                    Return
                End If

                Dim task As frmTaskSQLExport = New frmTaskSQLExport
                hideAfterType(task)
                newTaskID = task.AddTask(ScheduleID, ShowAfterType)
            Case "build excel workbook"
                If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.xl1_AdvancedXLPack) = False Then
                    _NeedUpgrade(gEdition.ENTERPRISEPROPLUS, tvTaskTypes, "Advanced Excel Pack")
                    Return
                End If

                Dim task As frmTaskBuildWorkBook = New frmTaskBuildWorkBook
                hideAfterType(task)
                newTaskID = task.AddTask(ScheduleID, ShowAfterType)
            Case "merge excel files"
                If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.xl1_AdvancedXLPack) = False Then
                    _NeedUpgrade(gEdition.ENTERPRISEPROPLUS, tvTaskTypes, "Advanced Excel Pack")
                    Return
                End If

                Dim task As frmTaskExcelMerge = New frmTaskExcelMerge
                task.m_eventBased = m_eventBased
                task.m_eventID = m_eventID
                hideAfterType(task)
                newTaskID = task.AddTask(ScheduleID, ShowAfterType)
            Case "upload to sharepoint"
                If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.o4_SharePointDestination) = False Then
                    _NeedUpgrade(gEdition.ENTERPRISEPROPLUS, tvTaskTypes, "SharePoint Destination")
                    Return
                End If


                Dim task As frmTaskUploadToSharepoint = New frmTaskUploadToSharepoint
                task.m_eventBased = m_eventBased
                task.m_eventID = m_eventID
                hideAfterType(task)
                newTaskID = task.AddTask(ScheduleID, ShowAfterType)
        End Select

        If newTaskID <> 0 Then
            clsMarsData.WriteData("UPDATE tasks SET forexception = " & Convert.ToInt32(m_forExceptionHandling) & " WHERE taskid = " & newTaskID)
        End If

        LoadTasks()
    End Sub

    Private Sub tvTaskTypes_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tvTaskTypes.MouseMove
        If tvTaskTypes.SelectedNode IsNot Nothing Then
            If e.Button = MouseButtons.Left Then
                tvTaskTypes.DoDragDrop(tvTaskTypes.SelectedNode.Text, DragDropEffects.Copy)
            End If
        End If
    End Sub

    Private Sub ctrl_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles tvTasks.DragEnter
        If e.Data.GetDataPresent(DataFormats.StringFormat, True) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Sub hideAfterType(frm As Form)
        If ShowAfterType = False Then
            For Each ctrl As Control In frm.Controls
                If TypeOf ctrl Is DevComponents.DotNetBar.SuperTabControl Then
                    Dim sp As DevComponents.DotNetBar.SuperTabControl = CType(ctrl, DevComponents.DotNetBar.SuperTabControl)

                    For Each stab As DevComponents.DotNetBar.SuperTabItem In sp.Tabs
                        If stab.Text = "Options" Then
                            stab.Visible = False
                            Exit For
                        End If
                    Next

                    Exit For
                End If
            Next
        End If
    End Sub
    Private Sub ctrl_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles tvTasks.DragDrop
        e.Effect = DragDropEffects.None

        Dim val As String = CType(e.Data.GetData(DataFormats.StringFormat, True), String)

        DoAddTask(val)
    End Sub

    Private Sub tvTasks_AfterCheck(sender As Object, e As DevComponents.AdvTree.AdvTreeCellEventArgs) Handles tvTasks.AfterCheck


        Dim it As DevComponents.AdvTree.Cell = e.Cell

        Dim taskID As Integer = it.Parent.DataKey

        Dim SQL As String = "UPDATE Tasks SET EnabledStatus =" & Convert.ToInt32(it.Checked) & _
        " WHERE TaskID =" & taskID

        clsMarsData.WriteData(SQL)

    End Sub


    Private Sub tvTasks_DoubleClick(sender As Object, e As System.EventArgs) Handles tvTasks.DoubleClick
        If tvTasks.SelectedNodes.Count = 0 Then Return

        Dim lsv As DevComponents.AdvTree.AdvTree = CType(sender, DevComponents.AdvTree.AdvTree)
        Dim Item As DevComponents.AdvTree.Node

        If lsv IsNot Nothing Then

            Item = lsv.SelectedNodes(0)

            cmdEdit_Click(sender, e)
        End If
    End Sub

    Private Sub tvTasks_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles tvTasks.KeyDown
        If e.KeyCode = Keys.Delete Then
            Me.cmdDelete_Click(sender, e)
        End If
    End Sub

   
End Class
