Friend Class frmTaskRunWhen
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbRunWhen As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents optProduction As System.Windows.Forms.RadioButton
    Friend WithEvents optDelivery As System.Windows.Forms.RadioButton
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents Label1 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmTaskRunWhen))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.optProduction = New System.Windows.Forms.RadioButton
        Me.optDelivery = New System.Windows.Forms.RadioButton
        Me.cmbRunWhen = New System.Windows.Forms.ComboBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.ep = New System.Windows.Forms.ErrorProvider
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.cmbRunWhen)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(312, 136)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(136, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(168, 16)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "THE SCHEDULE"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.optProduction)
        Me.GroupBox2.Controls.Add(Me.optDelivery)
        Me.GroupBox2.Enabled = False
        Me.GroupBox2.Location = New System.Drawing.Point(8, 56)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(296, 72)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'optProduction
        '
        Me.optProduction.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optProduction.Location = New System.Drawing.Point(8, 40)
        Me.optProduction.Name = "optProduction"
        Me.optProduction.Size = New System.Drawing.Size(224, 24)
        Me.optProduction.TabIndex = 0
        Me.optProduction.Tag = "Production"
        Me.optProduction.Text = "After Successful Report Production"
        '
        'optDelivery
        '
        Me.optDelivery.Checked = True
        Me.optDelivery.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDelivery.Location = New System.Drawing.Point(8, 16)
        Me.optDelivery.Name = "optDelivery"
        Me.optDelivery.Size = New System.Drawing.Size(224, 24)
        Me.optDelivery.TabIndex = 0
        Me.optDelivery.TabStop = True
        Me.optDelivery.Tag = "Delivery"
        Me.optDelivery.Text = "After Successful Report Delivery"
        '
        'cmbRunWhen
        '
        Me.cmbRunWhen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbRunWhen.ItemHeight = 13
        Me.cmbRunWhen.Items.AddRange(New Object() {"BEFORE", "AFTER", "BOTH"})
        Me.cmbRunWhen.Location = New System.Drawing.Point(8, 24)
        Me.cmbRunWhen.Name = "cmbRunWhen"
        Me.cmbRunWhen.Size = New System.Drawing.Size(120, 21)
        Me.cmbRunWhen.TabIndex = 0
        '
        'cmdOK
        '
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(127, 152)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.TabIndex = 17
        Me.cmdOK.Text = "&OK"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'frmTaskRunWhen
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(330, 184)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.MaximizeBox = False
        Me.Name = "frmTaskRunWhen"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Execute the Task...."
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmTaskRunWhen_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub cmbRunWhen_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbRunWhen.SelectedIndexChanged
        If cmbRunWhen.Text = "BEFORE" Then
            GroupBox2.Enabled = False
        ElseIf cmbRunWhen.Text = "BOTH" Then
            GroupBox2.Enabled = True
        Else
            GroupBox2.Enabled = True
        End If

        ep.SetError(cmbRunWhen, "")
    End Sub

    Public Function SetTaskRunWhen(Optional ByVal nTaskID As Integer = 0) As String()
        Dim oData As New clsMarsData
        Dim SQL As String
        Dim oRs As ADODB.Recordset


        If nTaskID > 0 Then
            SQL = "SELECT RunWhen, AfterType FROM Tasks WHERE TaskID = " & nTaskID

            oRs = clsMarsData.GetData(SQL)

            If Not oRs Is Nothing And oRs.EOF = False Then
                cmbRunWhen.Text = IsNull(oRs("runwhen").Value)

                If cmbRunWhen.Text <> "BEFORE" Then
                    Select Case IsNull(oRs("aftertype").Value)
                        Case "Delivery"
                            optDelivery.Checked = True
                        Case "Production"
                            optProduction.Checked = True
                    End Select
                End If
            End If

            oRs.Close()
        Else
            SQL = "SELECT MAX(TaskID) FROM Tasks"

            oRs = clsMarsData.GetData(SQL)

            If Not oRs Is Nothing And oRs.EOF = False Then
                nTaskID = oRs(0).Value
            End If

            oRs.Close()
        End If

        oData.DisposeRecordset(oRs)

        Me.ShowDialog()

        Dim sVals(1) As String
        Dim sVerb As String

        If cmbRunWhen.Text = "BEFORE" Then
            sVals(0) = "BEFORE"
            sVals(1) = ""
        Else
            If optDelivery.Checked = True Then
                sVerb = optDelivery.Tag
            ElseIf optProduction.Checked = True Then
                sVerb = optProduction.Tag
            End If


            sVals(0) = cmbRunWhen.Text
            sVals(1) = sVerb

        End If

        SQL = "RunWhen = '" & sVals(0) & "'," & _
            "AfterType = '" & sVals(1) & "'"

        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

    End Function

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click

        If cmbRunWhen.Text.Length = 0 Then
            ep.SetError(cmbRunWhen, "Please set when the task should run")
            cmbRunWhen.Focus()
            Return
        End If

        Me.Close()
    End Sub
End Class
