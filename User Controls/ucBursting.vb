

Imports System.Xml

Friend Class ucBursting
    Inherits System.Windows.Forms.UserControl

    Public nReportID As Integer = 99999
    Public ExcelBurst As Boolean = False
    Public oCombo As ComboBox = cmbGroups
    Public IsBursting As Boolean = False
    Dim ep As ErrorProvider = New ErrorProvider

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdGet As System.Windows.Forms.Button
    Friend WithEvents lsvBursting As System.Windows.Forms.ListView
    Friend WithEvents GroupName As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmbGroups As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdRemove As System.Windows.Forms.Button
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents txtColumn As System.Windows.Forms.ComboBox
    Friend WithEvents cmdImport As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel



    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(ucBursting))
        Me.cmdGet = New System.Windows.Forms.Button
        Me.lsvBursting = New System.Windows.Forms.ListView
        Me.GroupName = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.cmdAdd = New System.Windows.Forms.Button
        Me.Label5 = New System.Windows.Forms.Label
        Me.cmbGroups = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtColumn = New System.Windows.Forms.ComboBox
        Me.cmdRemove = New System.Windows.Forms.Button
        Me.cmdImport = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel





        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdGet
        '
        Me.cmdGet.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdGet.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdGet.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdGet.Location = New System.Drawing.Point(320, 36)
        Me.cmdGet.Name = "cmdGet"
        Me.cmdGet.Size = New System.Drawing.Size(24, 21)
        Me.cmdGet.TabIndex = 16
        Me.cmdGet.Text = "..."
        Me.cmdGet.Visible = False
        '
        'lsvBursting
        '
        Me.lsvBursting.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lsvBursting.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.GroupName, Me.ColumnHeader1})
        Me.lsvBursting.FullRowSelect = True
        Me.lsvBursting.GridLines = True
        Me.lsvBursting.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvBursting.LargeImageList = Me.ImageList1
        Me.lsvBursting.Location = New System.Drawing.Point(16, 168)
        Me.lsvBursting.MultiSelect = False
        Me.lsvBursting.Name = "lsvBursting"
        Me.lsvBursting.Size = New System.Drawing.Size(344, 176)
        Me.lsvBursting.SmallImageList = Me.ImageList1
        Me.lsvBursting.TabIndex = 15
        Me.lsvBursting.View = System.Windows.Forms.View.Details
        '
        'GroupName
        '
        Me.GroupName.Text = "Group Value"
        Me.GroupName.Width = 125
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Destination"
        Me.ColumnHeader1.Width = 161
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'cmdAdd
        '
        Me.cmdAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAdd.Location = New System.Drawing.Point(328, 136)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(35, 23)
        Me.cmdAdd.TabIndex = 14
        '
        'Label5
        '
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(8, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(176, 16)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Select Report Group Value"
        '
        'cmbGroups
        '
        Me.cmbGroups.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbGroups.ItemHeight = 13
        Me.cmbGroups.Location = New System.Drawing.Point(8, 36)
        Me.cmbGroups.Name = "cmbGroups"
        Me.cmbGroups.Size = New System.Drawing.Size(296, 21)
        Me.cmbGroups.TabIndex = 10
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(336, 16)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Column/formula group is based on e.g. Orders.OrderID or @Year"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.cmdGet)
        Me.GroupBox1.Controls.Add(Me.cmbGroups)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtColumn)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(368, 120)
        Me.GroupBox1.TabIndex = 26
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Select Group Value"
        '
        'txtColumn
        '
        Me.txtColumn.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtColumn.ItemHeight = 13
        Me.txtColumn.Location = New System.Drawing.Point(8, 84)
        Me.txtColumn.Name = "txtColumn"
        Me.txtColumn.Size = New System.Drawing.Size(296, 21)
        Me.txtColumn.TabIndex = 10
        '
        'cmdRemove
        '
        Me.cmdRemove.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdRemove.Image = CType(resources.GetObject("cmdRemove.Image"), System.Drawing.Image)
        Me.cmdRemove.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemove.Location = New System.Drawing.Point(16, 136)
        Me.cmdRemove.Name = "cmdRemove"
        Me.cmdRemove.Size = New System.Drawing.Size(35, 23)
        Me.cmdRemove.TabIndex = 14
        '
        'cmdImport
        '
        Me.cmdImport.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdImport.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdImport.Image = CType(resources.GetObject("cmdImport.Image"), System.Drawing.Image)
        Me.cmdImport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdImport.Location = New System.Drawing.Point(328, 352)
        Me.cmdImport.Name = "cmdImport"
        Me.cmdImport.Size = New System.Drawing.Size(35, 23)
        Me.cmdImport.TabIndex = 28
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.lsvBursting)
        Me.Panel1.Controls.Add(Me.cmdRemove)
        Me.Panel1.Controls.Add(Me.cmdAdd)
        Me.Panel1.Controls.Add(Me.cmdImport)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(384, 384)
        Me.Panel1.TabIndex = 29

        '

        '
        '
        'ucBursting
        '
        Me.Controls.Add(Me.Panel1)

        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Name = "ucBursting"
        Me.Size = New System.Drawing.Size(392, 392)
        Me.GroupBox1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region





    Public Sub LoadAll()
        Dim oData As New clsMarsData
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim oItem As ListViewItem

        SQL = "SELECT * FROM BurstAttr INNER JOIN DestinationAttr ON " & _
        "BurstAttr.DestinationID = DestinationAttr.DestinationID WHERE " & _
        "BurstAttr.ReportID = " & nReportID

        oRs = clsMarsData.GetData(SQL)

        lsvBursting.Items.Clear()

        Try
            Do While oRs.EOF = False
                oItem = New ListViewItem

                oItem.Text = oRs("groupvalue").Value

                oItem.Tag = oRs("burstid").Value & ":" & oRs(2).Value

                oItem.SubItems.Add(oRs("destinationname").Value)

                Select Case Convert.ToString(oRs("destinationtype").Value.tolower)
                    Case "email"
                        oItem.ImageIndex = 0
                    Case "disk"
                        oItem.ImageIndex = 1
                    Case "ftp"
                        oItem.ImageIndex = 3
                    Case "printer"
                        oItem.ImageIndex = 2
                    Case "fax"
                        oItem.ImageIndex = 4
                End Select

                lsvBursting.Items.Add(oItem)

                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub
    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If cmbGroups.Text.Length = 0 Then
            ep.SetError(cmbGroups, "Please select the group value")
            cmbGroups.Focus()
            Return
        ElseIf txtColumn.Text.Length = 0 Then
            ep.SetError(txtColumn, "Please specify the column that the group is based on")
            txtColumn.Focus()
            Return
        Else
            For Each o As ListViewItem In lsvBursting.Items
                If o.Text = cmbGroups.Text Then
                    ep.SetError(cmbGroups, "A destination for this group value has been added already")
                    cmbGroups.Focus()
                    Return
                End If
            Next
        End If

        Dim oDest As New frmDestinationSelect
        Dim nDest As Integer

        nDest = oDest.AddDestination(0, nReportID, 0, , , , , , , , IsBursting)

        If nDest = 0 Then Return

        Dim oData As New clsMarsData
        Dim nBurstID As Integer = clsMarsData.CreateDataID("burstattr", "burstid")
        Dim sCols As String
        Dim sVals As String


        sCols = "BurstID,ReportID,DestinationID,GroupValue,GroupColumn"

        sVals = nBurstID & "," & _
        nReportID & "," & _
        nDest & "," & _
        "'" & SQLPrepare(cmbGroups.Text) & "'," & _
        "'" & SQLPrepare(txtColumn.Text) & "'"

        oData.InsertData("BurstAttr", sCols, sVals)

        LoadAll()

    End Sub

    Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
        If lsvBursting.SelectedItems.Count = 0 Then Return

        Dim oData As New clsMarsData

        If clsMarsData.WriteData("DELETE FROM DestinationAttr WHERE DestinationID =" & lsvBursting.SelectedItems(0).Tag.Split(":")(1)) = True Then
            clsMarsData.WriteData("DELETE FROM BurstAttr WHERE BurstID =" & lsvBursting.SelectedItems(0).Tag.Split(":")(0))

            lsvBursting.SelectedItems(0).Remove()
        End If

    End Sub



    Private Sub lsvBursting_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvBursting.DoubleClick
        If lsvBursting.SelectedItems.Count = 0 Then Return

        Dim oDest As New frmDestinationSelect

        oDest.EditDestinaton(lsvBursting.SelectedItems(0).Tag.split(":")(1), , , , , , , IsBursting)

        LoadAll()

    End Sub

    Private Sub txtColumn_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ep.SetError(sender, String.Empty)
    End Sub



    Private Sub cmbGroups_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbGroups.SelectedIndexChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub lsvBursting_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvBursting.SelectedIndexChanged
        If lsvBursting.SelectedItems.Count = 0 Then Return

        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim oData As New clsMarsData
        Dim nID As Integer

        nID = lsvBursting.SelectedItems(0).Tag.Split(":")(0)

        SQL = "SELECT GroupColumn FROM BurstAttr WHERE BurstID =" & nID

        oRs = clsMarsData.GetData(SQL)

        Try
            If oRs.EOF = False Then
                txtColumn.Text = oRs.Fields(0).Value
            End If

            oRs.Close()
        Catch : End Try

    End Sub


    Private Sub txtColumn_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtColumn.SelectedIndexChanged

    End Sub


    Private Sub cmdImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImport.Click
        Dim oDefault As New frmTestDestination
        Dim nID As Integer
        Dim oData As New clsMarsData

        nID = oDefault._ImportDestination

        If nID = 0 Then Return

        Dim sCols As String
        Dim sVals As String
        Dim SQL As String
        Dim nPackID As Integer = 0
        Dim nDest As Integer = clsMarsData.CreateDataID("destinationattr", "destinationid")

        sCols = "DestinationID,DestinationType,SendTo,CC,BCC,Subject,Message,Extras,OutputPath,PrinterName," & _
        "PrinterDriver,PrinterPort,Orientation,PageFrom,PageTo,Copies,[Collate],ReportID,PackID,FTPServer,FTPUserName," & _
        "FTPPassword,FTPPath,DestinationName,OutputFormat,CustomExt,AppendDateTime,DateTimeFormat,CustomName,IncludeAttach," & _
        "Compress,Embed,MailFormat,HTMLSplit,DeferDelivery,DeferBy"

        sVals = nDest & "," & _
        "DestinationType,SendTo,CC,BCC,Subject,Message,Extras,OutputPath,PrinterName," & _
        "PrinterDriver,PrinterPort,Orientation,PageFrom,PageTo,Copies,[Collate]," & nReportID & _
        "," & nPackID & ",FTPServer,FTPUserName," & _
        "FTPPassword,FTPPath,DestinationName,OutputFormat,CustomExt,AppendDateTime,DateTimeFormat,CustomName,IncludeAttach," & _
        "Compress,Embed,MailFormat,HTMLSplit,DeferDelivery,DeferBy"

        SQL = "INSERT INTO DestinationAttr (" & sCols & ") " & _
        "SELECT " & sVals & " FROM DestinationAttr WHERE DestinationID = " & nID

        clsMarsData.WriteData(SQL)

        Dim nBurstID As Integer = clsMarsData.CreateDataID("burstattr", "burstid")

        sCols = "BurstID,ReportID,DestinationID,GroupValue,GroupColumn"

        sVals = nBurstID & "," & _
        nReportID & "," & _
        nDest & "," & _
        "'" & SQLPrepare(cmbGroups.Text) & "'," & _
        "'" & SQLPrepare(txtColumn.Text) & "'"

        oData.InsertData("BurstAttr", sCols, sVals)

        LoadAll()
    End Sub

    Private Sub ucBursting_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        On Error Resume Next

        '#If CRYSTAL_VER = 8 Then
        '        crp = New AxCRVIEWERLib.AxCRViewer
        '#ElseIf CRYSTAL_VER = 9 Then
        '        crp = New AxCRVIEWER9Lib.AxCRViewer9
        '#ElseIf CRYSTAL_VER = 10 Then
        '        crp = new AxCrystalActiveXReportViewerLib10.AxCrystalActiveXReportViewer 
        '#ElseIf CRYSTAL_VER = 11 Then
        '        crp = New AxCrystalActiveXReportViewerLib11.AxCrystalActiveXReportViewer
        '#End If

        '        Me.Controls.Add(crp)

        '        crp.Top = 0
        '        crp.Left = 0
    End Sub
End Class
