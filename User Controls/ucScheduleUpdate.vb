Friend Class ucScheduleUpdate
    Inherits System.Windows.Forms.UserControl
    Public sFrequency As String = "Daily"
    Dim nStatus As Integer = 1
    Public ScheduleID As Integer
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbException As System.Windows.Forms.ComboBox
    Friend WithEvents chkException As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents pnlRepeat As System.Windows.Forms.Panel
    Public IsLoaded As Boolean = False
    Friend WithEvents cmbUnit As System.Windows.Forms.ComboBox
    Dim ep As New ErrorProvider
    Friend WithEvents cmdOther As DevComponents.DotNetBar.ButtonX
    Friend WithEvents optOther As System.Windows.Forms.RadioButton
    Public btnApply As Button
    Friend WithEvents cmbCollaboration As System.Windows.Forms.ComboBox
    Public m_fireOthers As Boolean = False

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents NextRunTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkNoEnd As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Label13 As DevComponents.DotNetBar.LabelX
    Friend WithEvents RepeatUntil As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkRepeat As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents NextRun As System.Windows.Forms.DateTimePicker
    Friend WithEvents EndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents RunAt As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdMonthly As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmbCustom As System.Windows.Forms.ComboBox
    Friend WithEvents optDaily As System.Windows.Forms.RadioButton
    Friend WithEvents optWeekDaily As System.Windows.Forms.RadioButton
    Friend WithEvents optWeekly As System.Windows.Forms.RadioButton
    Friend WithEvents optMonthly As System.Windows.Forms.RadioButton
    Friend WithEvents optYearly As System.Windows.Forms.RadioButton
    Friend WithEvents optNone As System.Windows.Forms.RadioButton
    Friend WithEvents optCustom As System.Windows.Forms.RadioButton
    Friend WithEvents cmdDaily As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdWeekly As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmbRpt As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkStatus As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents StartDate As System.Windows.Forms.DateTimePicker
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.chkRepeat = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Label7 = New DevComponents.DotNetBar.LabelX()
        Me.NextRunTime = New System.Windows.Forms.DateTimePicker()
        Me.chkNoEnd = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.NextRun = New System.Windows.Forms.DateTimePicker()
        Me.EndDate = New System.Windows.Forms.DateTimePicker()
        Me.Label11 = New DevComponents.DotNetBar.LabelX()
        Me.RunAt = New System.Windows.Forms.DateTimePicker()
        Me.Label12 = New DevComponents.DotNetBar.LabelX()
        Me.StartDate = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New DevComponents.DotNetBar.LabelX()
        Me.pnlRepeat = New System.Windows.Forms.Panel()
        Me.cmbUnit = New System.Windows.Forms.ComboBox()
        Me.RepeatUntil = New System.Windows.Forms.DateTimePicker()
        Me.cmbRpt = New System.Windows.Forms.NumericUpDown()
        Me.Label13 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdOther = New DevComponents.DotNetBar.ButtonX()
        Me.optOther = New System.Windows.Forms.RadioButton()
        Me.cmdMonthly = New DevComponents.DotNetBar.ButtonX()
        Me.cmbCustom = New System.Windows.Forms.ComboBox()
        Me.optDaily = New System.Windows.Forms.RadioButton()
        Me.optWeekDaily = New System.Windows.Forms.RadioButton()
        Me.optWeekly = New System.Windows.Forms.RadioButton()
        Me.optMonthly = New System.Windows.Forms.RadioButton()
        Me.optCustom = New System.Windows.Forms.RadioButton()
        Me.optYearly = New System.Windows.Forms.RadioButton()
        Me.optNone = New System.Windows.Forms.RadioButton()
        Me.cmdDaily = New DevComponents.DotNetBar.ButtonX()
        Me.cmdWeekly = New DevComponents.DotNetBar.ButtonX()
        Me.chkStatus = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmbCollaboration = New System.Windows.Forms.ComboBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.cmbException = New System.Windows.Forms.ComboBox()
        Me.chkException = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.GroupBox3.SuspendLayout()
        Me.pnlRepeat.SuspendLayout()
        CType(Me.cmbRpt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.chkRepeat)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.NextRunTime)
        Me.GroupBox3.Controls.Add(Me.chkNoEnd)
        Me.GroupBox3.Controls.Add(Me.NextRun)
        Me.GroupBox3.Controls.Add(Me.EndDate)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.RunAt)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.StartDate)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.pnlRepeat)
        Me.GroupBox3.Location = New System.Drawing.Point(0, 198)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(486, 101)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        '
        'chkRepeat
        '
        Me.chkRepeat.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkRepeat.BackgroundStyle.Class = ""
        Me.chkRepeat.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkRepeat.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkRepeat.Location = New System.Drawing.Point(11, 78)
        Me.chkRepeat.Name = "chkRepeat"
        Me.chkRepeat.Size = New System.Drawing.Size(96, 17)
        Me.chkRepeat.TabIndex = 6
        Me.chkRepeat.Text = "Repeat every"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        '
        '
        '
        Me.Label7.BackgroundStyle.Class = ""
        Me.Label7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(205, 47)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 15)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Next To Run @"
        '
        'NextRunTime
        '
        Me.NextRunTime.Enabled = False
        Me.NextRunTime.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.NextRunTime.Location = New System.Drawing.Point(382, 42)
        Me.NextRunTime.Name = "NextRunTime"
        Me.NextRunTime.ShowUpDown = True
        Me.NextRunTime.Size = New System.Drawing.Size(88, 21)
        Me.NextRunTime.TabIndex = 5
        '
        'chkNoEnd
        '
        '
        '
        '
        Me.chkNoEnd.BackgroundStyle.Class = ""
        Me.chkNoEnd.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkNoEnd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkNoEnd.Location = New System.Drawing.Point(382, 12)
        Me.chkNoEnd.Name = "chkNoEnd"
        Me.chkNoEnd.Size = New System.Drawing.Size(98, 24)
        Me.chkNoEnd.TabIndex = 2
        Me.chkNoEnd.Text = "No End Date"
        '
        'NextRun
        '
        Me.NextRun.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.NextRun.Location = New System.Drawing.Point(288, 42)
        Me.NextRun.Name = "NextRun"
        Me.NextRun.Size = New System.Drawing.Size(88, 21)
        Me.NextRun.TabIndex = 4
        '
        'EndDate
        '
        Me.EndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.EndDate.Location = New System.Drawing.Point(288, 13)
        Me.EndDate.Name = "EndDate"
        Me.EndDate.Size = New System.Drawing.Size(88, 21)
        Me.EndDate.TabIndex = 1
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        '
        '
        '
        Me.Label11.BackgroundStyle.Class = ""
        Me.Label11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(205, 16)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(48, 15)
        Me.Label11.TabIndex = 6
        Me.Label11.Text = "End Date"
        '
        'RunAt
        '
        Me.RunAt.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.RunAt.Location = New System.Drawing.Point(89, 42)
        Me.RunAt.Name = "RunAt"
        Me.RunAt.ShowUpDown = True
        Me.RunAt.Size = New System.Drawing.Size(88, 21)
        Me.RunAt.TabIndex = 3
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        '
        '
        '
        Me.Label12.BackgroundStyle.Class = ""
        Me.Label12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(8, 46)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(76, 15)
        Me.Label12.TabIndex = 6
        Me.Label12.Text = "Schedule Time"
        '
        'StartDate
        '
        Me.StartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.StartDate.Location = New System.Drawing.Point(89, 12)
        Me.StartDate.Name = "StartDate"
        Me.StartDate.Size = New System.Drawing.Size(88, 21)
        Me.StartDate.TabIndex = 0
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        '
        '
        '
        Me.Label10.BackgroundStyle.Class = ""
        Me.Label10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(8, 16)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(52, 15)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "Start Date"
        '
        'pnlRepeat
        '
        Me.pnlRepeat.Controls.Add(Me.cmbUnit)
        Me.pnlRepeat.Controls.Add(Me.RepeatUntil)
        Me.pnlRepeat.Controls.Add(Me.cmbRpt)
        Me.pnlRepeat.Controls.Add(Me.Label13)
        Me.pnlRepeat.Enabled = False
        Me.pnlRepeat.Location = New System.Drawing.Point(66, 69)
        Me.pnlRepeat.Name = "pnlRepeat"
        Me.pnlRepeat.Size = New System.Drawing.Size(414, 30)
        Me.pnlRepeat.TabIndex = 7
        '
        'cmbUnit
        '
        Me.cmbUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbUnit.FormattingEnabled = True
        Me.cmbUnit.Items.AddRange(New Object() {"hours", "minutes"})
        Me.cmbUnit.Location = New System.Drawing.Point(142, 5)
        Me.cmbUnit.Name = "cmbUnit"
        Me.cmbUnit.Size = New System.Drawing.Size(74, 21)
        Me.cmbUnit.TabIndex = 1
        '
        'RepeatUntil
        '
        Me.RepeatUntil.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.RepeatUntil.Location = New System.Drawing.Point(316, 6)
        Me.RepeatUntil.Name = "RepeatUntil"
        Me.RepeatUntil.ShowUpDown = True
        Me.RepeatUntil.Size = New System.Drawing.Size(88, 21)
        Me.RepeatUntil.TabIndex = 2
        '
        'cmbRpt
        '
        Me.cmbRpt.DecimalPlaces = 2
        Me.cmbRpt.Increment = New Decimal(New Integer() {25, 0, 0, 131072})
        Me.cmbRpt.Location = New System.Drawing.Point(56, 6)
        Me.cmbRpt.Minimum = New Decimal(New Integer() {25, 0, 0, 131072})
        Me.cmbRpt.Name = "cmbRpt"
        Me.cmbRpt.Size = New System.Drawing.Size(56, 21)
        Me.cmbRpt.TabIndex = 0
        Me.cmbRpt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.cmbRpt.Value = New Decimal(New Integer() {25, 0, 0, 131072})
        '
        'Label13
        '
        '
        '
        '
        Me.Label13.BackgroundStyle.Class = ""
        Me.Label13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label13.Enabled = False
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(277, 6)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(33, 21)
        Me.Label13.TabIndex = 6
        Me.Label13.Text = "until"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdOther)
        Me.GroupBox2.Controls.Add(Me.optOther)
        Me.GroupBox2.Controls.Add(Me.cmdMonthly)
        Me.GroupBox2.Controls.Add(Me.cmbCustom)
        Me.GroupBox2.Controls.Add(Me.optDaily)
        Me.GroupBox2.Controls.Add(Me.optWeekDaily)
        Me.GroupBox2.Controls.Add(Me.optWeekly)
        Me.GroupBox2.Controls.Add(Me.optMonthly)
        Me.GroupBox2.Controls.Add(Me.optCustom)
        Me.GroupBox2.Controls.Add(Me.optYearly)
        Me.GroupBox2.Controls.Add(Me.optNone)
        Me.GroupBox2.Controls.Add(Me.cmdDaily)
        Me.GroupBox2.Controls.Add(Me.cmdWeekly)
        Me.GroupBox2.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(486, 141)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        '
        'cmdOther
        '
        Me.cmdOther.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOther.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOther.Enabled = False
        Me.cmdOther.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOther.Location = New System.Drawing.Point(288, 84)
        Me.cmdOther.Name = "cmdOther"
        Me.cmdOther.Size = New System.Drawing.Size(40, 16)
        Me.cmdOther.TabIndex = 11
        Me.cmdOther.Text = "..."
        '
        'optOther
        '
        Me.optOther.AutoSize = True
        Me.optOther.Location = New System.Drawing.Point(208, 84)
        Me.optOther.Name = "optOther"
        Me.optOther.Size = New System.Drawing.Size(51, 17)
        Me.optOther.TabIndex = 10
        Me.optOther.TabStop = True
        Me.optOther.Text = "Other"
        Me.optOther.UseVisualStyleBackColor = True
        '
        'cmdMonthly
        '
        Me.cmdMonthly.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdMonthly.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdMonthly.Enabled = False
        Me.cmdMonthly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdMonthly.Location = New System.Drawing.Point(122, 115)
        Me.cmdMonthly.Name = "cmdMonthly"
        Me.cmdMonthly.Size = New System.Drawing.Size(40, 16)
        Me.cmdMonthly.TabIndex = 6
        Me.cmdMonthly.Text = "..."
        '
        'cmbCustom
        '
        Me.cmbCustom.Enabled = False
        Me.cmbCustom.ItemHeight = 13
        Me.cmbCustom.Location = New System.Drawing.Point(288, 49)
        Me.cmbCustom.Name = "cmbCustom"
        Me.cmbCustom.Size = New System.Drawing.Size(182, 21)
        Me.cmbCustom.TabIndex = 9
        '
        'optDaily
        '
        Me.optDaily.Checked = True
        Me.optDaily.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDaily.Location = New System.Drawing.Point(8, 16)
        Me.optDaily.Name = "optDaily"
        Me.optDaily.Size = New System.Drawing.Size(104, 24)
        Me.optDaily.TabIndex = 0
        Me.optDaily.TabStop = True
        Me.optDaily.Text = "Every Day"
        '
        'optWeekDaily
        '
        Me.optWeekDaily.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optWeekDaily.Location = New System.Drawing.Point(6, 46)
        Me.optWeekDaily.Name = "optWeekDaily"
        Me.optWeekDaily.Size = New System.Drawing.Size(112, 24)
        Me.optWeekDaily.TabIndex = 2
        Me.optWeekDaily.Text = "Every Week Day"
        '
        'optWeekly
        '
        Me.optWeekly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optWeekly.Location = New System.Drawing.Point(8, 80)
        Me.optWeekly.Name = "optWeekly"
        Me.optWeekly.Size = New System.Drawing.Size(104, 24)
        Me.optWeekly.TabIndex = 3
        Me.optWeekly.Text = "Every Week"
        '
        'optMonthly
        '
        Me.optMonthly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optMonthly.Location = New System.Drawing.Point(8, 111)
        Me.optMonthly.Name = "optMonthly"
        Me.optMonthly.Size = New System.Drawing.Size(104, 24)
        Me.optMonthly.TabIndex = 5
        Me.optMonthly.Text = "Every Month"
        '
        'optCustom
        '
        Me.optCustom.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optCustom.Location = New System.Drawing.Point(208, 46)
        Me.optCustom.Name = "optCustom"
        Me.optCustom.Size = New System.Drawing.Size(77, 24)
        Me.optCustom.TabIndex = 8
        Me.optCustom.Text = "Calendar"
        '
        'optYearly
        '
        Me.optYearly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optYearly.Location = New System.Drawing.Point(208, 16)
        Me.optYearly.Name = "optYearly"
        Me.optYearly.Size = New System.Drawing.Size(104, 24)
        Me.optYearly.TabIndex = 7
        Me.optYearly.Text = "Every Year"
        '
        'optNone
        '
        Me.optNone.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optNone.Location = New System.Drawing.Point(208, 111)
        Me.optNone.Name = "optNone"
        Me.optNone.Size = New System.Drawing.Size(104, 24)
        Me.optNone.TabIndex = 12
        Me.optNone.Text = "None"
        '
        'cmdDaily
        '
        Me.cmdDaily.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdDaily.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdDaily.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDaily.Location = New System.Drawing.Point(122, 20)
        Me.cmdDaily.Name = "cmdDaily"
        Me.cmdDaily.Size = New System.Drawing.Size(40, 16)
        Me.cmdDaily.TabIndex = 1
        Me.cmdDaily.Text = "..."
        '
        'cmdWeekly
        '
        Me.cmdWeekly.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdWeekly.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdWeekly.Enabled = False
        Me.cmdWeekly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdWeekly.Location = New System.Drawing.Point(122, 84)
        Me.cmdWeekly.Name = "cmdWeekly"
        Me.cmdWeekly.Size = New System.Drawing.Size(40, 16)
        Me.cmdWeekly.TabIndex = 4
        Me.cmdWeekly.Text = "..."
        '
        'chkStatus
        '
        '
        '
        '
        Me.chkStatus.BackgroundStyle.Class = ""
        Me.chkStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkStatus.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkStatus.Location = New System.Drawing.Point(8, 16)
        Me.chkStatus.Name = "chkStatus"
        Me.chkStatus.Size = New System.Drawing.Size(194, 24)
        Me.chkStatus.TabIndex = 0
        Me.chkStatus.Text = "Enabled schedule and run it on "
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmbCollaboration)
        Me.GroupBox1.Controls.Add(Me.chkStatus)
        Me.GroupBox1.Location = New System.Drawing.Point(0, 299)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(486, 48)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        '
        'cmbCollaboration
        '
        Me.cmbCollaboration.FormattingEnabled = True
        Me.cmbCollaboration.Location = New System.Drawing.Point(208, 16)
        Me.cmbCollaboration.Name = "cmbCollaboration"
        Me.cmbCollaboration.Size = New System.Drawing.Size(168, 21)
        Me.cmbCollaboration.TabIndex = 5
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.cmbException)
        Me.GroupBox4.Controls.Add(Me.chkException)
        Me.GroupBox4.Location = New System.Drawing.Point(0, 144)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(486, 52)
        Me.GroupBox4.TabIndex = 1
        Me.GroupBox4.TabStop = False
        '
        'cmbException
        '
        Me.cmbException.Enabled = False
        Me.cmbException.FormattingEnabled = True
        Me.cmbException.Location = New System.Drawing.Point(142, 18)
        Me.cmbException.Name = "cmbException"
        Me.cmbException.Size = New System.Drawing.Size(170, 21)
        Me.cmbException.TabIndex = 0
        '
        'chkException
        '
        Me.chkException.AutoSize = True
        '
        '
        '
        Me.chkException.BackgroundStyle.Class = ""
        Me.chkException.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkException.Location = New System.Drawing.Point(6, 20)
        Me.chkException.Name = "chkException"
        Me.chkException.Size = New System.Drawing.Size(137, 15)
        Me.chkException.TabIndex = 0
        Me.chkException.Text = "Use exception calendar"
        '
        'ucScheduleUpdate
        '
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Name = "ucScheduleUpdate"
        Me.Size = New System.Drawing.Size(493, 354)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.pnlRepeat.ResumeLayout(False)
        CType(Me.cmbRpt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public ReadOnly Property m_EnableRepeat(ByVal scheduleID As Integer) As Boolean
        Get
            Dim SQL As String
            Dim oRs As ADODB.Recordset

            SQL = "SELECT OtherUnit FROM ScheduleOptions WHERE ScheduleID =" & scheduleID

            oRs = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then
                Return True
            ElseIf oRs.EOF = True Then
                Return True
            Else
                Dim unit As String = IsNull(oRs("otherunit").Value, "")

                Select Case unit
                    Case "Seconds", "Minutes", "Hours"
                        Return False
                    Case Else
                        Return True
                End Select
            End If
        End Get
    End Property
    Public Property m_RepeatUnit() As String
        Get
            Return cmbUnit.Text
        End Get
        Set(ByVal value As String)
            cmbUnit.SelectedIndex = cmbUnit.Items.IndexOf(value)
        End Set
    End Property

    Public ReadOnly Property m_endDate() As String
        Get
            If chkNoEnd.Checked = True Then
                Dim d As Date

                'd = New Date(Now.Year + 1000, Now.Month, Now.Day)
                d = New Date(3004, Now.Month, Now.Day)

                EndDate.Value = d
            End If

            Return ConDate(CTimeZ(EndDate.Value, dateConvertType.WRITE))
        End Get
    End Property

    Public ReadOnly Property m_nextRun() As String
        Get
            Return ConDate(CTimeZ(NextRun.Value, dateConvertType.WRITE)) & " " & ConTime(CTimeZ(NextRunTime.Value, dateConvertType.WRITE))
        End Get
    End Property

    Public ReadOnly Property m_startTime() As String
        Get
            Return ConTime(CTimeZ(RunAt.Value, dateConvertType.WRITE))
        End Get
    End Property

    Public ReadOnly Property m_startDate() As String
        Get
            Return ConDateTime(CTimeZ(StartDate.Value, dateConvertType.WRITE))
        End Get
    End Property

    Public ReadOnly Property m_repeatUntil() As String
        Get

            Dim s As String = ConTime(CTimeZ(RepeatUntil.Value, dateConvertType.WRITE))

            Return s
        End Get
    End Property
    Private Sub optDaily_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optDaily.CheckedChanged
        If optDaily.Checked = True Then
            sFrequency = "Daily"
            GroupBox3.Visible = True
            nStatus = 1
            cmbCustom.Enabled = False
            cmdDaily.Enabled = True
            GroupBox1.Visible = True
        Else
            cmdDaily.Enabled = False
        End If

        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Private Sub optWeekDaily_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optWeekDaily.CheckedChanged
        If optWeekDaily.Checked = True Then
            sFrequency = "Week-Daily"
            GroupBox3.Visible = True
            nStatus = 1
            cmbCustom.Enabled = False
            GroupBox1.Visible = True
        End If

        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Private Sub optWeekly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optWeekly.CheckedChanged
        If optWeekly.Checked = True Then
            sFrequency = "Weekly"
            GroupBox3.Visible = True
            nStatus = 1
            cmbCustom.Enabled = False
            cmdWeekly.Enabled = True
            GroupBox1.Visible = True
        Else
            cmdWeekly.Enabled = False
        End If

        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Private Sub optCustom_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optCustom.CheckedChanged
        If optCustom.Checked = True Then

            sFrequency = "Custom"
            nStatus = 1
            GroupBox3.Visible = True
            cmbCustom.Enabled = True
            GroupBox1.Visible = True
        End If

        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Private Sub optMonthly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optMonthly.CheckedChanged
        If optMonthly.Checked = True Then
            sFrequency = "Monthly"
            GroupBox3.Visible = True
            nStatus = 1
            cmbCustom.Enabled = False
            cmdMonthly.Enabled = True
            GroupBox1.Visible = True
        Else
            cmdMonthly.Enabled = False
        End If

        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Private Sub optYearly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optYearly.CheckedChanged
        If optYearly.Checked = True Then
            sFrequency = "Yearly"
            GroupBox3.Visible = True
            nStatus = 1
            cmbCustom.Enabled = False
            GroupBox1.Visible = True
        End If

        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Private Sub optNone_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optNone.CheckedChanged
        If optNone.Checked = True Then
            sFrequency = "None"
            nStatus = 0
            GroupBox3.Visible = False
            GroupBox1.Visible = False
            cmbCustom.Enabled = False
            chkStatus.Checked = False
        End If

        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Private Sub cmbCustom_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCustom.SelectedIndexChanged
        If cmbCustom.Text = "[New...]" Then
            If clsMarsSecurity._HasGroupAccess("Custom Calendar") = True Then
                Dim oForm As New frmAddCalendar

                Dim temp As String = oForm.AddCalendar("")

                If temp <> "" Then
                    cmbCustom_DropDown(sender, e)
                    cmbCustom.SelectedIndex = cmbCustom.Items.IndexOf(temp)
                End If
            End If
        Else
            Dim oS As New clsMarsScheduler

            If ConTime(Me.RunAt.Value) > ConTime(Now) Then
                NextRun.Value = clsMarsScheduler.globalItem.GetCustomNextRunDate(cmbCustom.Text, Now.Date.AddDays(-1))
            Else
                NextRun.Value = clsMarsScheduler.globalItem.GetCustomNextRunDate(cmbCustom.Text, Now.Date)
            End If

            'NextRun.Value = oS.GetCustomNextRunDate(cmbCustom.Text, Now.Date.AddDays(-1))
            EndDate.Value = oS.GetCustomEndDate(cmbCustom.Text)
        End If

        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Private Sub cmbCustom_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCustom.DropDown
        cmbCustom.Items.Clear()
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData

        oRs = clsMarsData.GetData("SELECT DISTINCT CalendarName FROM CalendarAttr ORDER BY CalendarName")

        cmbCustom.Items.Add("[New...]")

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                cmbCustom.Items.Add(oRs.Fields(0).Value)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Private Sub chkNoEnd_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNoEnd.CheckedChanged
        If chkNoEnd.Checked = True Then
            EndDate.Enabled = False
            Dim d As Date = New Date(3004, Now.Month, Now.Day)
            EndDate.Value = d
        Else
            EndDate.Enabled = True
            Dim d As Date = EndDate.Value
            If (d.Year >= 3004) Then
                d = New Date(3003, d.Month, d.Day)
                EndDate.Value = d
            End If
        End If

        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Private Sub cmdDaily_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDaily.Click
        Dim oForm As New frmScheduleOptions

        oForm.DailyOptions(ScheduleID)

        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Private Sub cmdWeekly_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdWeekly.Click
        Dim oForm As New frmScheduleOptions

        oForm.WeeklyOptions(ScheduleID) ', NextRun.Value)

        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Private Sub cmdMonthly_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMonthly.Click

        Dim oForm As New frmScheduleOptions

        oForm.MonthlyOptions(ScheduleID) ' , NextRun.Value)

        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Private Sub chkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkStatus.CheckedChanged
        If clsMarsScheduler.isScheduleLocked(ScheduleID) And chkStatus.Checked Then
            showViolatorMessage()

            chkStatus.Checked = False
            Return
        End If

        If chkStatus.Checked = True Then
            nStatus = 1
            If ConDate(EndDate.Value) <= ConDate(Now.Date) Then
                EndDate.Value = Now.Date.AddYears(10)
            End If
        Else
            nStatus = 0
        End If

        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Private Sub chkRepeat_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRepeat.CheckedChanged
        Dim I As Double

        If chkRepeat.CheckState = CheckState.Checked Then
            pnlRepeat.Enabled = True
        Else
            pnlRepeat.Enabled = False
            NextRunTime.Value = RunAt.Value
        End If

        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub


    Private Sub cmbRpt_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbRpt.LostFocus, cmbUnit.LostFocus  ', cmbRpt.TextChanged, RunAt.ValueChanged, RunAt.TextChanged
        Try
            If IsLoaded = False Then Return

            Dim IncrementBy As Double = cmbRpt.Value

            NextRunTime.Value = RunAt.Value

            If chkRepeat.Checked = True Then
                Dim currentDateTime As String = ConDateTime(Now)
                Dim nextrunDateTime As String = ConDateTime(NextRunTime.Value)

                Do While currentDateTime > nextrunDateTime

                    If cmbUnit.Text = "hours" Then
                        nextrunDateTime = ConDateTime(NextRunTime.Value.AddHours(IncrementBy))
                    Else
                        nextrunDateTime = ConDateTime(NextRunTime.Value.AddMinutes(IncrementBy))
                    End If

                    If ConTime(nextrunDateTime) <= ConTime(RepeatUntil.Value) Then
                        If cmbUnit.Text = "hours" Then
                            NextRunTime.Value = NextRunTime.Value.AddHours(IncrementBy)
                            NextRun.Value = NextRunTime.Value.Date
                        Else
                            NextRunTime.Value = NextRunTime.Value.AddMinutes(IncrementBy)
                            NextRun.Value = NextRunTime.Value.Date
                        End If
                    Else
                        NextRunTime.Value = RunAt.Value
                        Exit Do
                    End If
                Loop
            End If
        Catch : End Try

        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Private Sub cmbException_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbException.DropDown
        Me.cmbException.Items.Clear()

        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData

        oRs = clsMarsData.GetData("SELECT DISTINCT CalendarName FROM CalendarAttr ORDER BY CalendarName")

        cmbException.Items.Add("[New...]")

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                cmbException.Items.Add(oRs.Fields(0).Value)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Private Sub cmbException_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbException.SelectedIndexChanged
        If cmbException.Text = "[New...]" Then
            If clsMarsSecurity._HasGroupAccess("Custom Calendar") = True Then
                Dim oForm As New frmAddCalendar
                Dim temp As String = oForm.AddCalendar("")

                If temp <> "" Then
                    cmbException_DropDown(sender, e)
                    cmbException.SelectedIndex = cmbException.Items.IndexOf(temp)
                End If
            End If
        End If

        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Private Sub chkException_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkException.CheckedChanged


        cmbException.Enabled = chkException.Checked

        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Public Function ValidateSchedule(ByVal tabProperties As DevComponents.DotNetBar.TabControl, Optional ByVal scheduleID As Integer = 99999) As Boolean
        If chkRepeat.Checked = True Then

            If cmbRpt.Text = "" Then
                tabProperties.SelectedTabIndex = 1
                ep.SetError(cmbRpt, "Please set the repeat interval")
                cmbRpt.Focus()

                Return False
            ElseIf ConTime(RepeatUntil.Value) < ConTime(RunAt.Value) Then
                tabProperties.SelectedTabIndex = 1
                ep.SetError(RepeatUntil, "The 'repeat until' time cannot be before the execution time")
                RepeatUntil.Focus()

                Return False
            End If
        ElseIf chkException.Checked And cmbException.Text.Length = 0 Then
            ep.SetError(cmbException, "Please select the exception calendar")
            tabProperties.SelectedTabIndex = 1

            cmbException.Focus()
            Return False
        ElseIf Me.optCustom.Checked = True And (cmbCustom.Text.Length = 0 Or cmbCustom.Text = "[New...]") Then
            ep.SetError(cmbCustom, "Please select a custom calendar")
            cmbCustom.Focus()
            Return False
        ElseIf Me.chkException.Checked = True And (cmbException.Text.Length = 0 Or cmbException.Text = "[New...]") Then
            ep.SetError(cmbException, "Please select an exception calendar")
            cmbException.Focus()
            Return False
        ElseIf cmbCustom.Text.Length > 0 And cmbCustom.Text = cmbException.Text Then
            ep.SetError(cmbException, "You cannot use the same calendar for exception calendar and exception calendar")
            cmbException.Focus()
            Return False
        ElseIf clsMarsData.IsDuplicate("ScheduleOptions", "ScheduleID", scheduleID, False) = False And optOther.Checked Then
            ep.SetError(cmdOther, "Please set up your schedule's frequency properties")
            cmdOther.Focus()
            Return False
        End If


        '//lets see if this group has black out times
        Dim SQL As String = "SELECT operationhrsid FROM groupattr g INNER JOIN groupblackoutattr b ON g.groupid = b.groupid WHERE g.groupname ='" & SQLPrepare(gRole) & "'"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Do While oRs.EOF = False
                Dim opID As Integer = oRs(0).Value
                Dim opHours As clsOperationalHours = New clsOperationalHours(opID)
                Dim checkDate As Date = New Date(NextRun.Value.Year, NextRun.Value.Month, NextRun.Value.Day, NextRunTime.Value.Hour, NextRunTime.Value.Minute, NextRunTime.Value.Second)

                '//check the next 7 run days
                For I As Integer = 0 To 6
                    If opHours.withinOperationHours(checkDate, opHours.operationHoursName) Then
                        ep.SetError(RunAt, "The specified execution time is not allowed for the Group '" & gRole & "' as its within the '" & opHours.operationHoursName & "' period")
                        RunAt.Focus()
                        oRs.Close()
                        Return False
                    End If

                    checkDate = clsMarsScheduler.globalItem.GetNextRun(scheduleID, sFrequency, checkDate, RunAt.Value, chkRepeat.Checked, cmbRpt.Value, _
                                                                       RepeatUntil.Value, cmbCustom.Text, True, True, , cmbUnit.Text, True)
                Next
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
        Return True

    End Function

    Private Sub EndDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EndDate.ValueChanged
        If ConDate(EndDate.Value) < ConDate(NextRun.Value) Then
            NextRun.Enabled = False
        Else
            NextRun.Enabled = True
        End If

        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Private Sub NextRun_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles NextRun.ValueChanged
        If ConDate(EndDate.Value) < ConDate(NextRun.Value) Then
            NextRun.Enabled = False
        Else
            NextRun.Enabled = True
        End If

        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Private Sub StartDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StartDate.ValueChanged
        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Private Sub RunAt_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RunAt.ValueChanged
        Try
            Me.NextRunTime.Value = RunAt.Value
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Private Sub NextRunTime_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NextRunTime.ValueChanged
        Try
            btnApply.Enabled = True
        Catch : End Try
    End Sub

    Private Sub RepeatUntil_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RepeatUntil.ValueChanged
        Try
            btnApply.Enabled = True
        Catch : End Try

    End Sub

    Private Sub optOther_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optOther.CheckedChanged
        If optOther.Checked = True Then
            sFrequency = "Other"
            GroupBox3.Visible = True
            nStatus = 1
            cmbCustom.Enabled = False
            cmdOther.Enabled = True
            chkStatus.Checked = True
            chkStatus.Enabled = True
            Dim selectedFreq As String = ""

            If Me.m_fireOthers = True Then
                Dim others As New frmScheduleOptions

                others.OtherOptions(ScheduleID, selectedFreq) ', StartDate.Value)
            End If

            If selectedFreq = "Seconds" Or selectedFreq = "Minutes" Or selectedFreq = "Hours" Then
                Me.chkRepeat.Checked = False
                Me.chkRepeat.Enabled = False
            Else
                Me.chkRepeat.Enabled = True
            End If
        Else
            cmdOther.Enabled = False
            Me.chkRepeat.Enabled = True
        End If
    End Sub

    Private Sub cmdOther_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdOther.Click

        Dim others As New frmScheduleOptions
        Dim selectedFreq As String = ""

        others.OtherOptions(ScheduleID, selectedFreq) ', StartDate.Value)

        If selectedFreq = "Seconds" Or selectedFreq = "Minutes" Or selectedFreq = "Hours" Then
            Me.chkRepeat.Checked = False
            Me.chkRepeat.Enabled = False
        Else
            Me.chkRepeat.Enabled = True
        End If

    End Sub

    Private Sub ucScheduleUpdate_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim iscollabo As Boolean

        Try
            iscollabo = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("collaboration", 0))
        Catch ex As Exception
            iscollabo = False
        End Try

        If iscollabo = True Then
            ' cmbCollaboration.Items.Clear()
            cmbCollaboration.Items.Add("DEFAULT")
            chkStatus.Text = "Enable schedule and run it on "
            cmbCollaboration.Enabled = True
            cmbCollaboration.Visible = True
            If cmbCollaboration.Text = "" Then cmbCollaboration.Text = "DEFAULT"
        Else
            chkStatus.Text = "Enable"
            cmbCollaboration.Text = "DEFAULT"
            cmbCollaboration.Enabled = False
            cmbCollaboration.Visible = False
        End If
    End Sub

    Public ReadOnly Property m_collaborationServer(ByVal l_scheduleID As Integer) As String
        Get
            Dim iscollabo As Boolean

            Try
                iscollabo = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("collaboration", 0))
            Catch ex As Exception
                iscollabo = False
            End Try


            If iscollabo = True Then
                Return clsCollaboration.getCollaborationForSchedule(l_scheduleID)
            Else
                Return "DEFAULT"
            End If
        End Get
    End Property

    Private collaborationServerID As Integer
    Public Property m_collaborationServerID() As Integer
        Get
            If cmbCollaboration.Text = "DEFAULT" Or cmbCollaboration.Text = "" Then
                Return 0
            Else
                Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT collaboID from collaboratorsAttr WHERE serverName ='" & SQLPrepare(cmbCollaboration.Text) & "'")

                If oRs Is Nothing Then
                    Return 0
                ElseIf oRs.EOF = True Then
                    Return 0
                Else
                    Return IsNull(oRs(0).Value, 0)
                End If
            End If
        End Get
        Set(ByVal value As Integer)
            If value = 0 Then
                cmbCollaboration.Text = "DEFAULT"
            Else
                Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT serverName from collaboratorsAttr WHERE collaboID =" & value)

                If oRs Is Nothing Then
                    cmbCollaboration.Text = "DEFAULT"
                ElseIf oRs.EOF = True Then
                    cmbCollaboration.Text = "DEFAULT"
                Else
                    cmbCollaboration.Text = IsNull(oRs(0).Value, "DEFAULT")
                End If
            End If
        End Set
    End Property

    Private Sub cmbCollaboration_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCollaboration.DropDown
        cmbCollaboration.Items.Clear()
        cmbCollaboration.Items.Add("DEFAULT")

        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT collaboID, serverName FROM collaboratorsAttr")

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Do While oRs.EOF = False
                cmbCollaboration.Items.Add(oRs("serverName").Value.ToString.ToUpper)
                oRs.MoveNext()
            Loop

            oRs.Close()
            oRs = Nothing
        End If
    End Sub

    Private Sub cmbCollaboration_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCollaboration.SelectedIndexChanged

    End Sub
End Class
