Public Class ucDSN
    Inherits System.Windows.Forms.UserControl
    Public sTemp As String = ""

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbDSN As System.Windows.Forms.ComboBox
    Friend WithEvents txtUserID As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents grpOracle As System.Windows.Forms.Panel
    Friend WithEvents txtOracleServer As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtOracleUserID As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtOraclePassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents grpODBC As System.Windows.Forms.Panel
    Friend WithEvents optOracle As System.Windows.Forms.RadioButton
    Friend WithEvents optODBC As System.Windows.Forms.RadioButton
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucDSN))
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.cmbDSN = New System.Windows.Forms.ComboBox()
        Me.txtUserID = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.txtPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.grpODBC = New System.Windows.Forms.Panel()
        Me.grpOracle = New System.Windows.Forms.Panel()
        Me.txtOracleServer = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtOracleUserID = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.txtOraclePassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.optODBC = New System.Windows.Forms.RadioButton()
        Me.optOracle = New System.Windows.Forms.RadioButton()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.grpODBC.SuspendLayout()
        Me.grpOracle.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(6, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "DSN Name"
        '
        'cmbDSN
        '
        Me.cmbDSN.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbDSN.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.cmbDSN.ItemHeight = 13
        Me.cmbDSN.Location = New System.Drawing.Point(89, 3)
        Me.cmbDSN.Name = "cmbDSN"
        Me.cmbDSN.Size = New System.Drawing.Size(260, 21)
        Me.cmbDSN.Sorted = True
        Me.cmbDSN.TabIndex = 1
        '
        'txtUserID
        '
        Me.txtUserID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtUserID.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtUserID.Border.Class = "TextBoxBorder"
        Me.txtUserID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUserID.DisabledBackColor = System.Drawing.Color.White
        Me.txtUserID.ForeColor = System.Drawing.Color.Blue
        Me.txtUserID.Location = New System.Drawing.Point(83, 7)
        Me.txtUserID.Name = "txtUserID"
        Me.txtUserID.Size = New System.Drawing.Size(260, 21)
        Me.txtUserID.TabIndex = 2
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(6, 7)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "UserID"
        '
        'txtPassword
        '
        Me.txtPassword.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPassword.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtPassword.Border.Class = "TextBoxBorder"
        Me.txtPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPassword.DisabledBackColor = System.Drawing.Color.White
        Me.txtPassword.ForeColor = System.Drawing.Color.Blue
        Me.txtPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtPassword.Location = New System.Drawing.Point(83, 35)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtPassword.Size = New System.Drawing.Size(260, 21)
        Me.txtPassword.TabIndex = 2
        '
        'Label3
        '
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(6, 33)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Password"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.txtUserID)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.txtPassword)
        Me.Panel1.Location = New System.Drawing.Point(6, 30)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(358, 60)
        Me.Panel1.TabIndex = 4
        '
        'grpODBC
        '
        Me.grpODBC.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpODBC.Controls.Add(Me.Label1)
        Me.grpODBC.Controls.Add(Me.Panel1)
        Me.grpODBC.Controls.Add(Me.cmbDSN)
        Me.grpODBC.Location = New System.Drawing.Point(3, 4)
        Me.grpODBC.Name = "grpODBC"
        Me.grpODBC.Size = New System.Drawing.Size(364, 104)
        Me.grpODBC.TabIndex = 5
        '
        'grpOracle
        '
        Me.grpOracle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpOracle.Controls.Add(Me.txtOracleServer)
        Me.grpOracle.Controls.Add(Me.txtOracleUserID)
        Me.grpOracle.Controls.Add(Me.LabelX3)
        Me.grpOracle.Controls.Add(Me.LabelX2)
        Me.grpOracle.Controls.Add(Me.LabelX1)
        Me.grpOracle.Controls.Add(Me.txtOraclePassword)
        Me.grpOracle.Location = New System.Drawing.Point(3, 27)
        Me.grpOracle.Name = "grpOracle"
        Me.grpOracle.Size = New System.Drawing.Size(364, 101)
        Me.grpOracle.TabIndex = 6
        Me.grpOracle.Visible = False
        '
        'txtOracleServer
        '
        Me.txtOracleServer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtOracleServer.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtOracleServer.Border.Class = "TextBoxBorder"
        Me.txtOracleServer.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtOracleServer.DisabledBackColor = System.Drawing.Color.White
        Me.txtOracleServer.ForeColor = System.Drawing.Color.Blue
        Me.txtOracleServer.Location = New System.Drawing.Point(89, 15)
        Me.txtOracleServer.Name = "txtOracleServer"
        Me.txtOracleServer.Size = New System.Drawing.Size(260, 21)
        Me.txtOracleServer.TabIndex = 2
        '
        'txtOracleUserID
        '
        Me.txtOracleUserID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtOracleUserID.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtOracleUserID.Border.Class = "TextBoxBorder"
        Me.txtOracleUserID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtOracleUserID.DisabledBackColor = System.Drawing.Color.White
        Me.txtOracleUserID.ForeColor = System.Drawing.Color.Blue
        Me.txtOracleUserID.Location = New System.Drawing.Point(89, 42)
        Me.txtOracleUserID.Name = "txtOracleUserID"
        Me.txtOracleUserID.Size = New System.Drawing.Size(260, 21)
        Me.txtOracleUserID.TabIndex = 2
        '
        'LabelX3
        '
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Location = New System.Drawing.Point(12, 70)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(60, 23)
        Me.LabelX3.TabIndex = 0
        Me.LabelX3.Text = "Password"
        '
        'LabelX2
        '
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Location = New System.Drawing.Point(12, 41)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(75, 23)
        Me.LabelX2.TabIndex = 0
        Me.LabelX2.Text = "User ID"
        '
        'LabelX1
        '
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(12, 12)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(64, 23)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "Server Name"
        '
        'txtOraclePassword
        '
        Me.txtOraclePassword.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtOraclePassword.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtOraclePassword.Border.Class = "TextBoxBorder"
        Me.txtOraclePassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtOraclePassword.DisabledBackColor = System.Drawing.Color.White
        Me.txtOraclePassword.ForeColor = System.Drawing.Color.Blue
        Me.txtOraclePassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtOraclePassword.Location = New System.Drawing.Point(89, 70)
        Me.txtOraclePassword.Name = "txtOraclePassword"
        Me.txtOraclePassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtOraclePassword.Size = New System.Drawing.Size(260, 21)
        Me.txtOraclePassword.TabIndex = 2
        '
        'optODBC
        '
        Me.optODBC.AutoSize = True
        Me.optODBC.Checked = True
        Me.optODBC.Location = New System.Drawing.Point(3, 4)
        Me.optODBC.Name = "optODBC"
        Me.optODBC.Size = New System.Drawing.Size(76, 17)
        Me.optODBC.TabIndex = 7
        Me.optODBC.TabStop = True
        Me.optODBC.Text = "ODBC DSN"
        Me.optODBC.UseVisualStyleBackColor = True
        Me.optODBC.Visible = False
        '
        'optOracle
        '
        Me.optOracle.AutoSize = True
        Me.optOracle.Location = New System.Drawing.Point(147, 4)
        Me.optOracle.Name = "optOracle"
        Me.optOracle.Size = New System.Drawing.Size(125, 17)
        Me.optOracle.TabIndex = 7
        Me.optOracle.Text = "Oracle .NET Provider"
        Me.optOracle.UseVisualStyleBackColor = True
        Me.optOracle.Visible = False
        '
        'ucDSN
        '
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me.grpODBC)
        Me.Controls.Add(Me.optOracle)
        Me.Controls.Add(Me.optODBC)
        Me.Controls.Add(Me.grpOracle)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.Name = "ucDSN"
        Me.Size = New System.Drawing.Size(375, 138)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.grpODBC.ResumeLayout(False)
        Me.grpOracle.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region
    ''' <summary>
    ''' returns a string in the format of DSN|username|encrypted password|
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property m_conString() As String
        Set(value As String)
            If value.Split("|").Length = 5 Then '//its oracle
                optOracle.Checked = True
                txtOracleServer.Text = value.Split("|")(0)
                txtOracleUserID.Text = value.Split("|")(1)

                Dim password As String = value.Split("|")(2)

                If password.StartsWith("$$") And password.EndsWith("$$") Then
                    password = _DecryptDBValue(password)
                End If

                txtOraclePassword.Text = password
            Else
                optODBC.Checked = True
                cmbDSN.Text = value.Split("|")(0)
                txtUserID.Text = value.Split("|")(1)
                Dim password As String = value.Split("|")(2)

                If password.StartsWith("$$") And password.EndsWith("$$") Then
                    password = _DecryptDBValue(password)
                End If

                txtPassword.Text = password
            End If
        End Set
        Get
            If optODBC.Checked Then
                Dim returnVal As String = Me.cmbDSN.Text & "|" & Me.txtUserID.Text & "|" & _EncryptDBValue(Me.txtPassword.Text) & "|"

                Return returnVal
            ElseIf optOracle.Checked Then
                Dim returnVal As String = txtOracleServer.Text & "|" & txtOracleUserID.Text & "|" & _EncryptDBValue(txtOraclePassword.Text) & "|Oracle.NET|"

                Return returnVal
            End If
        End Get
    End Property
    Private Sub ucDSN_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oData As clsMarsData = New clsMarsData

        oData.GetDsns(cmbDSN)

        setupForDragAndDrop(txtPassword)
        setupForDragAndDrop(txtOracleServer)
        setupForDragAndDrop(txtOracleUserID)
        setupForDragAndDrop(txtOraclePassword)

    End Sub


    Public Property m_showConnectionType() As Boolean
        Get
            Return optODBC.Visible
        End Get
        Set(ByVal value As Boolean)
            optODBC.Visible = value
            optOracle.Visible = value
        End Set
    End Property

    Public Function _Validate() As Boolean

        AppStatus(True)

        If optODBC.Checked Then
            Dim oCon As ADODB.Connection = New ADODB.Connection

            If cmbDSN.Text = "" Then
                setError(cmbDSN, "Please select the datasource name")
                Return False
            End If

            Try
                oCon.Open(cmbDSN.Text, txtUserID.Text, txtPassword.Text)

                Return True
            Catch ex As System.Exception
                setError(cmbDSN, ex.Message)
                Return False
            End Try
        Else
            If txtOracleServer.Text = "" Then
                setError(txtOracleServer, "Please enter the Oracle data source")
                Return False
            End If

            Try
                Dim orCon As System.Data.OracleClient.OracleConnection
                Dim connectionString As String

                If txtOracleUserID.Text = "" Then
                    connectionString = "Data Source=" & txtOracleServer.Text & ";Integrated Security=yes;"
                Else
                    connectionString = "Data Source=" & txtOracleServer.Text & ";User Id=" & txtOracleUserID.Text & ";Password=" & txtOraclePassword.Text & ";Integrated Security=no;"
                End If

                orCon = New System.Data.OracleClient.OracleConnection(connectionString)

                orCon.Open()
                orCon.Close()
                Return True
            Catch ex As Exception
                setError(txtOracleServer, ex.Message)
                Return False
            End Try
        End If

        AppStatus(False)
    End Function

    Private Sub cmbDSN_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDSN.SelectedIndexChanged
        setError(cmbDSN, "")

        If cmbDSN.Text = "<New>" Then
            Dim o As New Process
            Dim Check()
            Dim systemDrive As String = Environment.GetEnvironmentVariable("SystemDrive")
            Dim odbcPath As String

            If OSBit() = OSBitType.Bit32 Then
                odbcPath = IO.Path.Combine(systemDrive, "Windows\System32\odbcad32.exe")
            Else
                odbcPath = IO.Path.Combine(systemDrive, "Windows\SysWOW64\odbcad32.exe")
            End If

            o.StartInfo.FileName = odbcPath
            o.StartInfo.UseShellExecute = True

            o.Start()

            o.WaitForExit()


            Dim oData As New clsMarsData

            oData.GetDsns(cmbDSN)
        End If
    End Sub

    Private Sub optODBC_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles optODBC.CheckedChanged, optOracle.CheckedChanged
        If optODBC.Checked Then
            grpODBC.Visible = True
            grpOracle.Visible = False
        ElseIf optOracle.Checked Then
            grpODBC.Visible = False
            grpOracle.Visible = True
            grpOracle.Location = New Point(3, 27)
        End If
    End Sub
End Class
