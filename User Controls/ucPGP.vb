Public Class ucPGP
    Inherits System.Windows.Forms.UserControl
    Dim oData As New clsMarsData
    Public eventID As Integer = 0
    Public m_eventBased As Boolean
    Public IsStatic As Boolean
    Public Dynamic As Boolean

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdDbLoc As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKeyLocation As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents tip As System.Windows.Forms.ToolTip
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtPGPUserID As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdSec As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtSecring As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents chkPGPExt As DevComponents.DotNetBar.Controls.CheckBoxX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucPGP))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkPGPExt = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.cmdDbLoc = New DevComponents.DotNetBar.ButtonX()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.txtKeyLocation = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.txtPGPUserID = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.txtSecring = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdSec = New DevComponents.DotNetBar.ButtonX()
        Me.tip = New System.Windows.Forms.ToolTip(Me.components)
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.GroupBox1.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkPGPExt)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.cmdDbLoc)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtKeyLocation)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtPGPUserID)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtSecring)
        Me.GroupBox1.Controls.Add(Me.cmdSec)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(408, 256)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'chkPGPExt
        '
        '
        '
        '
        Me.chkPGPExt.BackgroundStyle.Class = ""
        Me.chkPGPExt.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkPGPExt.Location = New System.Drawing.Point(8, 176)
        Me.chkPGPExt.Name = "chkPGPExt"
        Me.chkPGPExt.Size = New System.Drawing.Size(328, 24)
        Me.chkPGPExt.TabIndex = 6
        Me.chkPGPExt.Text = "Append '.pgp' extension to resulting filename"
        '
        'Label3
        '
        '
        '
        '
        Me.Label3.BackgroundStyle.Class = ""
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label3.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label3.Location = New System.Drawing.Point(3, 237)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(402, 16)
        Me.Label3.TabIndex = 5
        Me.Label3.Tag = "red"
        Me.Label3.Text = "*PGP functionality powered by PGP (www.pgp.com)"
        '
        'cmdDbLoc
        '
        Me.cmdDbLoc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdDbLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDbLoc.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdDbLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDbLoc.Location = New System.Drawing.Point(360, 88)
        Me.cmdDbLoc.Name = "cmdDbLoc"
        Me.cmdDbLoc.Size = New System.Drawing.Size(40, 21)
        Me.cmdDbLoc.TabIndex = 2
        Me.cmdDbLoc.Text = "..."
        '
        'Label6
        '
        '
        '
        '
        Me.Label6.BackgroundStyle.Class = ""
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(8, 72)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(136, 16)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Public Keyring file"
        '
        'txtKeyLocation
        '
        Me.txtKeyLocation.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtKeyLocation.Border.Class = "TextBoxBorder"
        Me.txtKeyLocation.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKeyLocation.Location = New System.Drawing.Point(8, 88)
        Me.txtKeyLocation.Name = "txtKeyLocation"
        Me.txtKeyLocation.ReadOnly = True
        Me.txtKeyLocation.Size = New System.Drawing.Size(328, 21)
        Me.txtKeyLocation.TabIndex = 1
        Me.txtKeyLocation.Tag = "memo"
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "PGP UserID"
        '
        'txtPGPUserID
        '
        Me.txtPGPUserID.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtPGPUserID.Border.Class = "TextBoxBorder"
        Me.txtPGPUserID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPGPUserID.Location = New System.Drawing.Point(8, 40)
        Me.txtPGPUserID.Name = "txtPGPUserID"
        Me.txtPGPUserID.Size = New System.Drawing.Size(184, 21)
        Me.txtPGPUserID.TabIndex = 0
        Me.txtPGPUserID.Tag = "memo"
        Me.tip.SetToolTip(Me.txtPGPUserID, "Your PGP Key UserID")
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 120)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(136, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Private Keyring file"
        '
        'txtSecring
        '
        Me.txtSecring.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtSecring.Border.Class = "TextBoxBorder"
        Me.txtSecring.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSecring.Location = New System.Drawing.Point(8, 136)
        Me.txtSecring.Name = "txtSecring"
        Me.txtSecring.ReadOnly = True
        Me.txtSecring.Size = New System.Drawing.Size(328, 21)
        Me.txtSecring.TabIndex = 3
        Me.txtSecring.Tag = "memo"
        '
        'cmdSec
        '
        Me.cmdSec.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdSec.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSec.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdSec.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSec.Location = New System.Drawing.Point(360, 136)
        Me.cmdSec.Name = "cmdSec"
        Me.cmdSec.Size = New System.Drawing.Size(40, 21)
        Me.cmdSec.TabIndex = 4
        Me.cmdSec.Text = "..."
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'ucPGP
        '
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Name = "ucPGP"
        Me.Size = New System.Drawing.Size(424, 272)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Public Function _AddKey(Optional ByVal nDestinationID As Integer = 99999) As Boolean

        Dim SQL As String
        Dim sVals As String
        Dim sCols As String
        Dim sResult As String

        sCols = "KeyUserID,KeyPassword,KeyLocation,KeySize,KeyType,DestinationID,PGPID,PGPExt"

        sVals = "'" & clsMarsData.CreateDataID("pgpattr", "keyuserid") & "'," & _
        "''," & _
        "'" & SQLPrepare(txtKeyLocation.Text & "|" & txtSecring.Text) & "'," & _
        "''," & _
        "''," & _
        nDestinationID & "," & _
        "'" & SQLPrepare(txtPGPUserID.Text) & "'," & _
        Convert.ToInt32(chkPGPExt.Checked)

        SQL = "INSERT INTO PGPAttr (" & sCols & ") VALUES (" & sVals & ")"

        If clsMarsData.WriteData(SQL) = True Then
            Return True
        End If

    End Function

    Private Sub cmdDbLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDbLoc.Click
        Dim ofd As OpenFileDialog = New OpenFileDialog
        Dim oRes As DialogResult

        With ofd
            .Title = "Select the public keyring file..."
            .Filter = "Public keyring files (*.pkr)|*.pkr|All files (*.*)|*.*"

            oRes = .ShowDialog()

            If oRes = DialogResult.Cancel Then Return

            txtKeyLocation.Text = .FileName
        End With
    End Sub


    Private Sub txtKeyLocation_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKeyLocation.TextChanged
        SetError(sender, String.Empty)
    End Sub

    Private Sub cmdSec_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSec.Click
        Dim ofd As OpenFileDialog = New OpenFileDialog
        Dim oRes As DialogResult

        With ofd
            .Title = "Select the private keyring file..."
            .Filter = "Private keyring files (*.skr)|*.skr|All files (*.*)|*.*"

            oRes = .ShowDialog()

            If oRes = DialogResult.Cancel Then Return

            txtSecring.Text = .FileName
        End With
    End Sub

    Private Sub ucPGP_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtKeyLocation.ContextMenu = mnuInserter
        txtPGPUserID.ContextMenu = mnuInserter
        txtSecring.ContextMenu = mnuInserter
    End Sub


    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        Dim ctrl As Control = Me.ActiveControl

        If TypeOf ctrl Is TextBox Then
            Dim txt As TextBox = CType(ctrl, TextBox)

            txt.Undo()
        End If
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        Dim ctrl As Control = Me.ActiveControl

        If TypeOf ctrl Is TextBox Then
            Dim txt As TextBox = CType(ctrl, TextBox)

            txt.Cut()
        End If
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        Dim ctrl As Control = Me.ActiveControl

        If TypeOf ctrl Is TextBox Then
            Dim txt As TextBox = CType(ctrl, TextBox)

            txt.Copy()
        End If
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        Dim ctrl As Control = Me.ActiveControl

        If TypeOf ctrl Is TextBox Then
            Dim txt As TextBox = CType(ctrl, TextBox)

            txt.Paste()
        End If
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        Dim ctrl As Control = Me.ActiveControl

        If TypeOf ctrl Is TextBox Then
            Dim txt As TextBox = CType(ctrl, TextBox)

            txt.SelectedText = ""
        End If
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        Dim ctrl As Control = Me.ActiveControl

        If TypeOf ctrl Is TextBox Then
            Dim txt As TextBox = CType(ctrl, TextBox)

            txt.SelectAll()
        End If
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(eventID)
        oInsert.m_EventBased = Me.m_eventBased
        oInsert.m_EventID = Me.eventID
        'oField.SelectedText = oInsert.GetConstants(Me)

        oInsert.GetConstants(Me.ParentForm, Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        If Dynamic = False Or IsStatic = True Then
            Dim oItem As New frmDataItems
            Dim ctrl As Control = Me.ActiveControl

            If TypeOf ctrl Is TextBox Then
                Dim txt As TextBox = CType(ctrl, TextBox)

                txt.SelectedText = oItem._GetDataItem(Me.eventID)
            End If
        Else
            Dim oIntruder As New frmInserter(0)

            oIntruder.GetDatabaseField(gTables, gsCon, Me.ParentForm)
        End If
    End Sub
End Class
