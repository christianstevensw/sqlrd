<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucFTPBrowser2
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucFTPBrowser2))
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmbFTPType = New System.Windows.Forms.ComboBox()
        Me.lsvFTP = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.lblPath = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdConnect = New DevComponents.DotNetBar.ButtonX()
        Me.txtPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.txtUserName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.txtPort = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.txtServerAddress = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.chkPassive = New DevComponents.DotNetBar.Controls.CheckBoxX()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Date"
        Me.ColumnHeader2.Width = 122
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Name"
        Me.ColumnHeader1.Width = 132
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Size"
        '
        'cmbFTPType
        '
        Me.cmbFTPType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFTPType.FormattingEnabled = True
        Me.cmbFTPType.Items.AddRange(New Object() {"FTP", "FTP - SSL 3.1 (TLS)", "FTP - SSL 3.0"})
        Me.cmbFTPType.Location = New System.Drawing.Point(112, 80)
        Me.cmbFTPType.Name = "cmbFTPType"
        Me.cmbFTPType.Size = New System.Drawing.Size(144, 21)
        Me.cmbFTPType.TabIndex = 23
        Me.cmbFTPType.Visible = False
        '
        'lsvFTP
        '
        Me.lsvFTP.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.lsvFTP.Border.Class = "ListViewBorder"
        Me.lsvFTP.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvFTP.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader4})
        Me.lsvFTP.HideSelection = False
        Me.lsvFTP.Location = New System.Drawing.Point(0, 0)
        Me.lsvFTP.Name = "lsvFTP"
        Me.lsvFTP.Size = New System.Drawing.Size(349, 292)
        Me.lsvFTP.TabIndex = 17
        Me.lsvFTP.UseCompatibleStateImageBehavior = False
        Me.lsvFTP.View = System.Windows.Forms.View.Details
        '
        'lblPath
        '
        Me.lblPath.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.lblPath.Border.Class = "TextBoxBorder"
        Me.lblPath.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblPath.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lblPath.Location = New System.Drawing.Point(0, 310)
        Me.lblPath.Name = "lblPath"
        Me.lblPath.ReadOnly = True
        Me.lblPath.Size = New System.Drawing.Size(349, 21)
        Me.lblPath.TabIndex = 18
        '
        'cmdConnect
        '
        Me.cmdConnect.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdConnect.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdConnect.Location = New System.Drawing.Point(267, 82)
        Me.cmdConnect.Name = "cmdConnect"
        Me.cmdConnect.Size = New System.Drawing.Size(80, 21)
        Me.cmdConnect.TabIndex = 16
        Me.cmdConnect.Text = "Connect..."
        Me.cmdConnect.Visible = False
        '
        'txtPassword
        '
        '
        '
        '
        Me.txtPassword.Border.Class = ""
        Me.txtPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtPassword.Location = New System.Drawing.Point(112, 52)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtPassword.Size = New System.Drawing.Size(144, 14)
        Me.txtPassword.TabIndex = 13
        Me.txtPassword.Visible = False
        '
        'Label5
        '
        '
        '
        '
        Me.Label5.BackgroundStyle.Class = ""
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(8, 82)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(96, 16)
        Me.Label5.TabIndex = 20
        Me.Label5.Text = "FTP Type"
        Me.Label5.Visible = False
        '
        'Label3
        '
        '
        '
        '
        Me.Label3.BackgroundStyle.Class = ""
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 54)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(96, 16)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "Password"
        Me.Label3.Visible = False
        '
        'txtUserName
        '
        '
        '
        '
        Me.txtUserName.Border.Class = ""
        Me.txtUserName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUserName.Location = New System.Drawing.Point(112, 28)
        Me.txtUserName.Name = "txtUserName"
        Me.txtUserName.Size = New System.Drawing.Size(144, 14)
        Me.txtUserName.TabIndex = 12
        Me.txtUserName.Visible = False
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 16)
        Me.Label2.TabIndex = 21
        Me.Label2.Text = "User Name"
        Me.Label2.Visible = False
        '
        'txtPort
        '
        '
        '
        '
        Me.txtPort.Border.Class = ""
        Me.txtPort.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPort.Location = New System.Drawing.Point(344, 28)
        Me.txtPort.Name = "txtPort"
        Me.txtPort.Size = New System.Drawing.Size(32, 14)
        Me.txtPort.TabIndex = 15
        Me.txtPort.Text = "21"
        Me.txtPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPort.Visible = False
        '
        'Label4
        '
        '
        '
        '
        Me.Label4.BackgroundStyle.Class = ""
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(264, 30)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(88, 16)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "Port Number"
        Me.Label4.Visible = False
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 16)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "FTP Server Address"
        Me.Label1.Visible = False
        '
        'txtServerAddress
        '
        '
        '
        '
        Me.txtServerAddress.Border.Class = ""
        Me.txtServerAddress.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtServerAddress.Location = New System.Drawing.Point(112, 4)
        Me.txtServerAddress.Name = "txtServerAddress"
        Me.txtServerAddress.Size = New System.Drawing.Size(264, 14)
        Me.txtServerAddress.TabIndex = 11
        Me.txtServerAddress.Visible = False
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'Label6
        '
        '
        '
        '
        Me.Label6.BackgroundStyle.Class = ""
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(1, 295)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(96, 16)
        Me.Label6.TabIndex = 24
        Me.Label6.Text = "Selection"
        '
        'chkPassive
        '
        Me.chkPassive.AutoSize = True
        '
        '
        '
        Me.chkPassive.BackgroundStyle.Class = ""
        Me.chkPassive.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkPassive.Location = New System.Drawing.Point(112, 107)
        Me.chkPassive.Name = "chkPassive"
        Me.chkPassive.Size = New System.Drawing.Size(109, 16)
        Me.chkPassive.TabIndex = 31
        Me.chkPassive.Text = "Use Passive Mode"
        '
        'ucFTPBrowser2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me.lblPath)
        Me.Controls.Add(Me.lsvFTP)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.cmbFTPType)
        Me.Controls.Add(Me.cmdConnect)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtUserName)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtPort)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtServerAddress)
        Me.Controls.Add(Me.chkPassive)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "ucFTPBrowser2"
        Me.Size = New System.Drawing.Size(349, 331)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents cmbFTPType As System.Windows.Forms.ComboBox
    Friend WithEvents lsvFTP As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblPath As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdConnect As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtUserName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtPort As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtServerAddress As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents chkPassive As DevComponents.DotNetBar.Controls.CheckBoxX

End Class
