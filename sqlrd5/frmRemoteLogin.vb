Friend Class frmRemoteLogin
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim UserCancel As Boolean

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtUserID As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents ReflectionImage1 As DevComponents.DotNetBar.Controls.ReflectionImage
    Friend WithEvents chkRemember As DevComponents.DotNetBar.Controls.CheckBoxX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRemoteLogin))
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.txtUserID = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.chkRemember = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.ReflectionImage1 = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(321, 60)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Please enter an Administrator user name and password for the remote system"
        Me.Label2.WordWrap = True
        '
        'Label3
        '
        '
        '
        '
        Me.Label3.BackgroundStyle.Class = ""
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 69)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "User ID"
        '
        'txtUserID
        '
        '
        '
        '
        Me.txtUserID.Border.Class = "TextBoxBorder"
        Me.txtUserID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUserID.Location = New System.Drawing.Point(94, 67)
        Me.txtUserID.Name = "txtUserID"
        Me.txtUserID.Size = New System.Drawing.Size(226, 21)
        Me.txtUserID.TabIndex = 3
        '
        'txtPassword
        '
        '
        '
        '
        Me.txtPassword.Border.Class = "TextBoxBorder"
        Me.txtPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtPassword.Location = New System.Drawing.Point(94, 93)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtPassword.Size = New System.Drawing.Size(226, 21)
        Me.txtPassword.TabIndex = 4
        '
        'Label4
        '
        '
        '
        '
        Me.Label4.BackgroundStyle.Class = ""
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 95)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Password"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(407, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(56, 23)
        Me.cmdOK.TabIndex = 5
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(345, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(56, 23)
        Me.cmdCancel.TabIndex = 6
        '
        'chkRemember
        '
        '
        '
        '
        Me.chkRemember.BackgroundStyle.Class = ""
        Me.chkRemember.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkRemember.Location = New System.Drawing.Point(94, 120)
        Me.chkRemember.Name = "chkRemember"
        Me.chkRemember.Size = New System.Drawing.Size(208, 24)
        Me.chkRemember.TabIndex = 9
        Me.chkRemember.Text = "Remember Password"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 226)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(466, 30)
        Me.FlowLayoutPanel1.TabIndex = 10
        '
        'ReflectionImage1
        '
        '
        '
        '
        Me.ReflectionImage1.BackgroundStyle.Class = ""
        Me.ReflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ReflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.ReflectionImage1.Image = CType(resources.GetObject("ReflectionImage1.Image"), System.Drawing.Image)
        Me.ReflectionImage1.Location = New System.Drawing.Point(335, 12)
        Me.ReflectionImage1.Name = "ReflectionImage1"
        Me.ReflectionImage1.Size = New System.Drawing.Size(128, 212)
        Me.ReflectionImage1.TabIndex = 11
        '
        'frmRemoteLogin
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(466, 256)
        Me.ControlBox = False
        Me.Controls.Add(Me.ReflectionImage1)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.chkRemember)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.txtUserID)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label4)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmRemoteLogin"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Login to Remote System"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmRemoteLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Close()
    End Sub

    Public Function CheckLogin(ByVal sRemoteCon As String, ByVal RemoteID As Integer, _
    Optional ByVal sUser As String = "", Optional ByVal sPassword As String = "", Optional ByVal unifiedLogin As Boolean = False) As Boolean
        Dim SQL As String
        Dim oRs As New ADODB.Recordset
        Dim Valid As Boolean

        If sRemoteCon.Contains("sqlrdlive.dat") Then
            sRemoteCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sRemoteCon & ";Persist Security Info=False"
        End If

        If unifiedLogin = True Then

            SQL = "SELECT * FROM DomainAttr WHERE DomainName ='" & Environment.UserDomainName & "\" & Environment.UserName & "'"
            Try
                oRs.Open(SQL, sRemoteCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                If oRs.EOF = True Then
                    Valid = False
                Else
                    Valid = True
                End If

                oRs.Close()
            Catch ex As Exception
                Valid = False
            End Try

            If Valid = False Then
                If MessageBox.Show("The currently logged-in user does not have rights to access the remote system." & vbCrLf & _
                "Would you like to log-in using MARS authentication?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                    Return False
                End If
            Else
                Return True
            End If
        End If

        If sUser.Length > 0 Then
            txtUserID.Text = sUser
            txtPassword.Text = sPassword
            chkRemember.Checked = True
        End If

        Me.ShowDialog()

        If UserCancel = True Then Return False

        Dim StoredPassword As String

        SQL = "SELECT Password, UserRole FROM CRDUsers WHERE UserID = '" & SQLPrepare(txtUserID.Text) & "'"



        oRs.Open(SQL, sRemoteCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

        If oRs.EOF = False Then
            If txtUserID.Text.ToLower = "sqlrdadmin" Then
                StoredPassword = "SQLRDAdmin"
            Else
                StoredPassword = Decrypt(IsNull(oRs(0).Value), "qwer][po")
            End If

            If StoredPassword = txtPassword.Text Then
                Valid = True
            Else
                Valid = False
            End If
        Else
            Valid = False
        End If

        oRs.Close()

        If Valid = True And chkRemember.Checked = True Then
            SQL = "UPDATE RemoteSysAttr SET UserName ='" & SQLPrepare(Me.txtUserID.Text) & "'," & _
            "UserPassword = '" & SQLPrepare(_EncryptDBValue(txtPassword.Text)) & "' " & _
            "WHERE RemoteID =" & RemoteID

            clsMarsData.WriteData(SQL)
        ElseIf Valid = True And chkRemember.Checked = False Then
            SQL = "UPDATE RemoteSysAttr SET UserName =''," & _
            "UserPassword = '' " & _
            "WHERE RemoteID =" & RemoteID

            clsMarsData.WriteData(SQL)
        End If

        Return Valid
    End Function

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Close()
    End Sub

    Public Function AdminCheck() As Boolean
        Me.Text = "Aunthenticate user"
        ' Me.Label2.Text = "Confirmation"
        Label2.Text = "Please provide an  administrative password to confirm the  system change"
        Me.chkRemember.Visible = False

RETRY:
        Me.ShowDialog()

        If UserCancel = True Then Return False

        Dim oSec As clsMarsSecurity = New clsMarsSecurity

        If oSec._ValidatePassword(Me.txtUserID.Text, Me.txtPassword.Text) = False Then
            MessageBox.Show("The username and password you have provided are invalid. Please try again.", _
            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)

            GoTo Retry
        Else
            Return True
        End If

    End Function

    Public Sub getLoginDetails(ByRef userName As String, ByRef password As String, ByVal sUrl As String)
        Label2.Text = "The server at '" & sUrl & "' requires a username and password."
        Label3.Text = "Domain\User ID"
        Me.chkRemember.Visible = False

        Me.ShowDialog()

        If UserCancel = True Then
            userName = ""
            password = ""
            Return
        Else
            userName = Me.txtUserID.Text
            password = Me.txtPassword.Text
            Return
        End If

    End Sub
End Class
