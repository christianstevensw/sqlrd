Friend Class frmAutoScheduleWizard
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim nStep As Integer = 0
    Dim oData As New clsMarsData
    Const S1 As String = "Step 1: Schedule Name"
    Const S2 As String = "Step 2: Schedule Setup"
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem3 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Const S3 As String = "Step 3: Custom Tasks"

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdNext As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdBack As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Step1 As System.Windows.Forms.Panel
    Friend WithEvents cmdLoc As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtFolder As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Step2 As System.Windows.Forms.Panel
    Friend WithEvents Step3 As System.Windows.Forms.Panel
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents ucSet As sqlrd.ucScheduleSet
    Friend WithEvents cmdFinish As DevComponents.DotNetBar.ButtonX
    Friend WithEvents UcTasks As sqlrd.ucTasks
    Friend WithEvents txtDesc As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtKeyWord As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAutoScheduleWizard))
        Me.cmdNext = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmdBack = New DevComponents.DotNetBar.ButtonX()
        Me.Step1 = New System.Windows.Forms.Panel()
        Me.txtDesc = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtKeyWord = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdLoc = New DevComponents.DotNetBar.ButtonX()
        Me.txtFolder = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Step2 = New System.Windows.Forms.Panel()
        Me.ucSet = New sqlrd.ucScheduleSet()
        Me.Step3 = New System.Windows.Forms.Panel()
        Me.UcTasks = New sqlrd.ucTasks()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cmdFinish = New DevComponents.DotNetBar.ButtonX()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.DividerLabel1 = New sqlrd.DividerLabel()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem3 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.Step1.SuspendLayout()
        Me.Step2.SuspendLayout()
        Me.Step3.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdNext
        '
        Me.cmdNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(679, 459)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(75, 25)
        Me.cmdNext.TabIndex = 49
        Me.cmdNext.Text = "&Next"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(599, 459)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 25)
        Me.cmdCancel.TabIndex = 48
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdBack
        '
        Me.cmdBack.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdBack.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdBack.Enabled = False
        Me.cmdBack.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBack.Location = New System.Drawing.Point(519, 459)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(75, 25)
        Me.cmdBack.TabIndex = 47
        Me.cmdBack.Text = "&Back"
        '
        'Step1
        '
        Me.Step1.BackColor = System.Drawing.Color.White
        Me.Step1.Controls.Add(Me.txtDesc)
        Me.Step1.Controls.Add(Me.Label3)
        Me.Step1.Controls.Add(Me.Label7)
        Me.Step1.Controls.Add(Me.txtKeyWord)
        Me.Step1.Controls.Add(Me.cmdLoc)
        Me.Step1.Controls.Add(Me.txtFolder)
        Me.Step1.Controls.Add(Me.Label4)
        Me.Step1.Controls.Add(Me.txtName)
        Me.Step1.Controls.Add(Me.Label2)
        Me.Step1.Location = New System.Drawing.Point(3, 0)
        Me.Step1.Name = "Step1"
        Me.Step1.Size = New System.Drawing.Size(655, 447)
        Me.Step1.TabIndex = 14
        '
        'txtDesc
        '
        '
        '
        '
        Me.txtDesc.Border.Class = "TextBoxBorder"
        Me.txtDesc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDesc.Location = New System.Drawing.Point(120, 64)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDesc.Size = New System.Drawing.Size(360, 149)
        Me.txtDesc.TabIndex = 3
        Me.txtDesc.Tag = "memo"
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(121, 16)
        Me.Label3.TabIndex = 26
        Me.Label3.Text = "Description (optional)"
        '
        'Label7
        '
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(8, 225)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(106, 16)
        Me.Label7.TabIndex = 25
        Me.Label7.Text = "Keyword (optional)"
        '
        'txtKeyWord
        '
        Me.txtKeyWord.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtKeyWord.Border.Class = "TextBoxBorder"
        Me.txtKeyWord.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKeyWord.ForeColor = System.Drawing.Color.Blue
        Me.txtKeyWord.Location = New System.Drawing.Point(120, 219)
        Me.txtKeyWord.Name = "txtKeyWord"
        Me.txtKeyWord.Size = New System.Drawing.Size(360, 21)
        Me.txtKeyWord.TabIndex = 4
        Me.txtKeyWord.Tag = "memo"
        '
        'cmdLoc
        '
        Me.cmdLoc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoc.Location = New System.Drawing.Point(486, 37)
        Me.cmdLoc.Name = "cmdLoc"
        Me.cmdLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdLoc.TabIndex = 2
        Me.cmdLoc.Text = "...."
        Me.ToolTip1.SetToolTip(Me.cmdLoc, "Browse")
        '
        'txtFolder
        '
        Me.txtFolder.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtFolder.Border.Class = "TextBoxBorder"
        Me.txtFolder.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFolder.ForeColor = System.Drawing.Color.Blue
        Me.txtFolder.Location = New System.Drawing.Point(120, 37)
        Me.txtFolder.Name = "txtFolder"
        Me.txtFolder.ReadOnly = True
        Me.txtFolder.Size = New System.Drawing.Size(360, 21)
        Me.txtFolder.TabIndex = 1
        Me.txtFolder.Tag = "1"
        '
        'Label4
        '
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 43)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(74, 21)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Create In"
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(120, 11)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(360, 21)
        Me.txtName.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(208, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Schedule Name"
        '
        'Step2
        '
        Me.Step2.BackColor = System.Drawing.Color.White
        Me.Step2.Controls.Add(Me.ucSet)
        Me.Step2.Location = New System.Drawing.Point(0, 0)
        Me.Step2.Name = "Step2"
        Me.Step2.Size = New System.Drawing.Size(424, 341)
        Me.Step2.TabIndex = 15
        '
        'ucSet
        '
        Me.ucSet.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ucSet.Location = New System.Drawing.Point(8, 4)
        Me.ucSet.m_collaborationServerID = 0
        Me.ucSet.m_RepeatUnit = "hours"
        Me.ucSet.Name = "ucSet"
        Me.ucSet.Size = New System.Drawing.Size(424, 333)
        Me.ucSet.TabIndex = 0
        '
        'Step3
        '
        Me.Step3.BackColor = System.Drawing.Color.White
        Me.Step3.Controls.Add(Me.UcTasks)
        Me.Step3.Location = New System.Drawing.Point(1, 0)
        Me.Step3.Name = "Step3"
        Me.Step3.Size = New System.Drawing.Size(638, 447)
        Me.Step3.TabIndex = 16
        '
        'UcTasks
        '
        Me.UcTasks.BackColor = System.Drawing.Color.White
        Me.UcTasks.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcTasks.ForeColor = System.Drawing.Color.Navy
        Me.UcTasks.Location = New System.Drawing.Point(8, 8)
        Me.UcTasks.m_defaultTaks = False
        Me.UcTasks.m_eventBased = False
        Me.UcTasks.m_eventID = 99999
        Me.UcTasks.m_forExceptionHandling = False
        Me.UcTasks.m_showAfterType = True
        Me.UcTasks.m_showExpanded = True
        Me.UcTasks.Name = "UcTasks"
        Me.UcTasks.Size = New System.Drawing.Size(627, 436)
        Me.UcTasks.TabIndex = 0
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'cmdFinish
        '
        Me.cmdFinish.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdFinish.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdFinish.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdFinish.Location = New System.Drawing.Point(679, 459)
        Me.cmdFinish.Name = "cmdFinish"
        Me.cmdFinish.Size = New System.Drawing.Size(75, 25)
        Me.cmdFinish.TabIndex = 50
        Me.cmdFinish.Text = "&Finish"
        Me.cmdFinish.Visible = False
        '
        'DividerLabel1
        '
        Me.DividerLabel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(0, 0)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(788, 13)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 22
        '
        'SuperTabControl1
        '
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel3)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Location = New System.Drawing.Point(3, 3)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(751, 450)
        Me.SuperTabControl1.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTabControl1.TabIndex = 51
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem1, Me.SuperTabItem2, Me.SuperTabItem3})
        Me.SuperTabControl1.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        Me.SuperTabControl1.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.Step3)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(111, 0)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(640, 450)
        Me.SuperTabControlPanel3.TabIndex = 3
        Me.SuperTabControlPanel3.TabItem = Me.SuperTabItem3
        '
        'SuperTabItem3
        '
        Me.SuperTabItem3.AttachedControl = Me.SuperTabControlPanel3
        Me.SuperTabItem3.GlobalItem = False
        Me.SuperTabItem3.Image = CType(resources.GetObject("SuperTabItem3.Image"), System.Drawing.Image)
        Me.SuperTabItem3.Name = "SuperTabItem3"
        Me.SuperTabItem3.Text = "Custom Tasks"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.Step2)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(111, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(640, 450)
        Me.SuperTabControlPanel2.TabIndex = 2
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Image = CType(resources.GetObject("SuperTabItem2.Image"), System.Drawing.Image)
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Schedule"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.Step1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(111, 0)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(640, 450)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem1
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Image = CType(resources.GetObject("SuperTabItem1.Image"), System.Drawing.Image)
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "General"
        '
        'frmAutoScheduleWizard
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(788, 504)
        Me.ControlBox = False
        Me.Controls.Add(Me.SuperTabControl1)
        Me.Controls.Add(Me.DividerLabel1)
        Me.Controls.Add(Me.cmdFinish)
        Me.Controls.Add(Me.cmdNext)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdBack)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmAutoScheduleWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Automation Schedule Wizard"
        Me.Step1.ResumeLayout(False)
        Me.Step2.ResumeLayout(False)
        Me.Step3.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmAutoScheduleWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        FormatForWinXP(Me)
        Step1.Visible = True
        Step2.Visible = False
        Step3.Visible = False
        txtName.Focus()
        Step1.BringToFront()

        clsMarsData.DataItem.CleanDB()

        UcTasks.ShowAfterType = False
        UcTasks.oAuto = True

        If gParentID > 0 And gParent.Length > 0 Then
            txtFolder.Text = gParent
            txtFolder.Tag = gParentID
        End If
    End Sub

    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click
        Select Case nStep
            Case 0
                If txtName.Text.Length = 0 Then
                    ep.SetError(txtName, "Please set a name for this schedule")
                    txtName.Focus()
                    Return
                ElseIf txtFolder.Text.Length = 0 Then
                    ep.SetError(txtFolder, "Please select the parent folder for the schedule")
                    txtFolder.Focus()
                    Return
                ElseIf clsMarsUI.candoRename(txtName.Text, Me.txtFolder.Tag, clsMarsScheduler.enScheduleType.AUTOMATION) = False Then
                    ep.SetError(txtName, "An automation schedule " & _
                    "already exist with this name in this folder")
                    txtName.Focus()
                    Return
                End If

                cmdBack.Enabled = True
                Step1.Visible = False
                Step2.Visible = True
                Step2.BringToFront()
                ucSet.Focus()
            Case 1
                'If ucSet.StartDate.Value > ucSet.EndDate.Value Then
                '    ep.SetError(ucSet.EndDate, "Please select a valid date range")
                '    Return
                'ElseIf ucSet.chkRepeat.Checked = True Then
                '    If ucSet.cmbRpt.Text = 0 Then
                '        ep.SetError(ucSet.cmbRpt, "Please select the repeat interval")
                '        Return
                '    ElseIf ucSet.RunAt.Value > ucSet.RepeatUntil.Value Then
                '        ep.SetError(ucSet.RunAt, "The execution time cannot be after the repeat until time")
                '        Return
                '    End If
                'ElseIf ucSet.optCustom.Checked = True And _
                '    (ucSet.cmbCustom.Text.Length = 0 Or _
                '    ucSet.cmbCustom.Text = "[New]") Then
                '    ep.SetError(ucSet.cmbCustom, "Please select a valid calendar")
                '    ucSet.cmbCustom.Focus()
                '    Return
                'ElseIf ucSet.chkException.Checked And (ucSet.cmbException.Text.Length = 0 Or ucSet.cmbException.Text = "[New...]") Then
                '    ep.SetError(ucSet.cmbException, "Please select an exception calendar")
                '    ucSet.cmbException.Focus()
                '    Return
                'End If

                If ucSet.ValidateSchedule = False Then
                    Return
                End If
                Step2.Visible = False
                Step3.Visible = True
                Step3.BringToFront()
                cmdNext.Visible = False
                cmdFinish.Visible = True
                UcTasks.Focus()
            Case 2

        End Select

        nStep += 1
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub cmdLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoc.Click
        ep.SetError(txtFolder, "")
        Dim oFolders As frmFolders = New frmFolders
        Dim sFolder(1) As String

        sFolder = oFolders.GetFolder

        If sFolder(0) <> "" And sFolder(0) <> "Desktop" Then
            txtFolder.Text = sFolder(0)
            txtFolder.Tag = sFolder(1)
        End If
    End Sub

    Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBack.Click
        Select Case nStep
            Case 2
                Step3.Visible = False
                Step2.Visible = True
                Step2.BringToFront()
                cmdNext.Visible = True
                cmdFinish.Visible = False
            Case 1
                Step2.Visible = False
                Step1.Visible = True
                Step1.BringToFront()
                cmdBack.Enabled = False
        End Select

        nStep -= 1
    End Sub

    Private Sub cmdFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFinish.Click

        If UcTasks.tvTasks.Nodes.Count = 0 Then
            ep.SetError(UcTasks, "Please add a custom action")
            Return
        End If

        cmdFinish.Enabled = False

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim ExecuteOk As Boolean
        Dim nID As Integer
        Dim AutoID As Integer

        AutoID = clsMarsData.CreateDataID("automationattr", "autoid")

        sCols = "AutoID, AutoName, Parent,Owner"

        sVals = AutoID & ", '" & SQLPrepare(txtName.Text) & "'," & _
            txtFolder.Tag & "," & _
            "'" & gUser & "'"

        SQL = "INSERT INTO AutomationAttr (" & sCols & ") VALUES (" & sVals & ")"

        ExecuteOk = clsMarsData.WriteData(SQL)


        If ExecuteOk = False Then GoTo RollbackTrans

        Dim ScheduleID As Int64 = clsMarsData.CreateDataID("scheduleattr", "scheduleid")

        sCols = "ScheduleID,Frequency,StartDate,EndDate,NextRun," & _
                "StartTime,Repeat,RepeatInterval,RepeatUntil,Status,AutoID," & _
                "Description,KeyWord,CalendarName,UseException,ExceptionCalendar,RepeatUnit,collaborationServer"

        With ucSet
            If .chkNoEnd.Checked = True Then
                Dim d As Date

                'd = New Date(Now.Year + 1000, Now.Month, Now.Day)
                d = New Date(3004, Now.Month, Now.Day)

                .EndDate.Value = d
            End If

            sVals = ScheduleID & "," & _
                    "'" & .sFrequency & "'," & _
                    "'" & ConDateTime(CTimeZ(.StartDate.Value, dateConvertType.WRITE)) & "'," & _
                    "'" & ConDateTime(CTimeZ(.EndDate.Value, dateConvertType.WRITE)) & "'," & _
                    "'" & ConDate(CTimeZ(.StartDate.Value, dateConvertType.WRITE)) & " " & ConTime(CTimeZ(.RunAt.Value, dateConvertType.WRITE)) & "'," & _
                    "'" & CTimeZ(.RunAt.Value.ToShortTimeString, dateConvertType.WRITE) & "'," & _
                    Convert.ToInt32(.chkRepeat.Checked) & "," & _
                    "'" & Convert.ToString(.cmbRpt.Value).Replace(",", ".") & "'," & _
                    "'" & CTimeZ(.RepeatUntil.Value.ToShortTimeString, dateConvertType.WRITE) & "'," & _
                    Convert.ToInt32(.chkStatus.Checked) & "," & _
                    AutoID & "," & _
                    "'" & SQLPrepare(txtDesc.Text) & "'," & _
                    "'" & SQLPrepare(txtKeyWord.Text) & "'," & _
                    "'" & SQLPrepare(.cmbCustom.Text) & "'," & _
                    Convert.ToInt32(.chkException.Checked) & "," & _
                    "'" & SQLPrepare(.cmbException.Text) & "'," & _
                    "'" & ucSet.m_RepeatUnit & "'," & _
                    .m_collaborationServerID
        End With

        SQL = "INSERT INTO ScheduleAttr (" & sCols & ") VALUES (" & sVals & ")"

        ExecuteOk = clsMarsData.WriteData(SQL)

        clsMarsData.WriteData("UPDATE ScheduleOptions SET " & _
            "ScheduleID = " & ScheduleID & " WHERE ScheduleID = 99999")


        If ExecuteOk = False Then GoTo RollbackTrans

        SQL = "UPDATE Tasks SET ScheduleID = " & ScheduleID & " WHERE ScheduleID = 99999"

        clsMarsData.WriteData(SQL)

        If gUser.ToLower <> "admin" Then
            Dim oUser As New clsMarsUsers

            oUser.AssignView(nID, gUser, clsMarsUsers.enViewType.ViewAutomation)
        End If

        frmMainWin.itemToSelect = New genericExplorerObject
        frmMainWin.itemToSelect.folderID = txtFolder.Tag
        frmMainWin.itemToSelect.itemName = txtName.Text

        Close()

        clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.AUTOMATION, clsMarsAudit.AuditAction.CREATE)
        Return
RollbackTrans:
        clsMarsData.WriteData("DELETE FROM AutomationAttr WHERE AutoID =" & nID)
        clsMarsData.WriteData("DELETE FROM ScheduleAttr WHERE AutoID =" & nID)
        clsMarsData.WriteData("DELETE FROM Tasks WHERE ScheduleID =" & ScheduleID)
        clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & ScheduleID)
        cmdFinish.Enabled = True
    End Sub

    Dim closeFlag As Boolean

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click


        If closeFlag = False Then
            Dim sp As DevComponents.DotNetBar.SuperTooltip = New DevComponents.DotNetBar.SuperTooltip
            Dim spi As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo("Really Cancel?", "",
                                                                                                               "Are you sure you would like to close the wizard? All your progress will be lost. Click Cancel again to confirm.",
                                                                                                           Nothing,
                                                                                                               Nothing,
                                                                                                               DevComponents.DotNetBar.eTooltipColor.Office2003)
            sp.SetSuperTooltip(sender, spi)
            sp.ShowTooltip(sender)
            closeFlag = True
        Else
            Dim oCleaner As clsMarsData = New clsMarsData
            oCleaner.CleanDB()
            Me.Close()
        End If

        oData.CleanDB()

        Me.Close()
    End Sub



End Class
