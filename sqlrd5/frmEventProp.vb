Public Class frmEventProp
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim ep As New ErrorProvider
    Friend WithEvents UcConditions1 As sqlrd.ucConditions
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents optNew As System.Windows.Forms.RadioButton
    Friend WithEvents optExisting As System.Windows.Forms.RadioButton
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbAnyAll As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents chkStatus As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents UcReports1 As sqlrd.ucReports
    Friend WithEvents UcExisting As sqlrd.ucExistingReports
    Friend WithEvents ucFlow As sqlrd.ucExecutionPath
    Friend WithEvents optNone As System.Windows.Forms.RadioButton
    Dim NameChange As Boolean = False
    Friend WithEvents UcError As sqlrd.ucErrorHandler
    Friend WithEvents DividerLabel4 As sqlrd.DividerLabel
    Friend WithEvents DividerLabel3 As sqlrd.DividerLabel
    Friend WithEvents cmbOpHrs As System.Windows.Forms.ComboBox
    Friend WithEvents chkOpHrs As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents cmbPriority As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents DividerLabel5 As sqlrd.DividerLabel
    Dim packed As Boolean = False
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents tabProperties As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabGeneral As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel7 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabHistory As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel6 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabExecution As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel5 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabTasks As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabOptions As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabSchedules As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabCondition As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel4 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents scheduleHistory As sqlrd.ucScheduleHistory
    Friend WithEvents pnlMaxthreads As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents chkExecuteAsync As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtMaxthreads As DevComponents.Editors.IntegerInput
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Dim schedulePriority As String = "3 - Normal"

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdApply As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtID As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdLoc As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtFolder As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents UcTasks As sqlrd.ucTasks
    Friend WithEvents txtDesc As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKeyWord As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents mnuParameters As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuParameter As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEventProp))
        Me.cmdApply = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.txtID = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtDesc = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.Label7 = New DevComponents.DotNetBar.LabelX()
        Me.txtKeyWord = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdLoc = New DevComponents.DotNetBar.ButtonX()
        Me.txtFolder = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.mnuParameters = New System.Windows.Forms.ContextMenu()
        Me.mnuParameter = New System.Windows.Forms.MenuItem()
        Me.cmbPriority = New System.Windows.Forms.ComboBox()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.cmbOpHrs = New System.Windows.Forms.ComboBox()
        Me.chkOpHrs = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.optNew = New System.Windows.Forms.RadioButton()
        Me.optExisting = New System.Windows.Forms.RadioButton()
        Me.optNone = New System.Windows.Forms.RadioButton()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.chkStatus = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.cmbAnyAll = New System.Windows.Forms.ComboBox()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.tabProperties = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.UcReports1 = New sqlrd.ucReports()
        Me.UcExisting = New sqlrd.ucExistingReports()
        Me.tabSchedules = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabGeneral = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.UcConditions1 = New sqlrd.ucConditions()
        Me.tabCondition = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel7 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.scheduleHistory = New sqlrd.ucScheduleHistory()
        Me.tabHistory = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel6 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.ucFlow = New sqlrd.ucExecutionPath()
        Me.tabExecution = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel5 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.UcTasks = New sqlrd.ucTasks()
        Me.tabTasks = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.DividerLabel3 = New sqlrd.DividerLabel()
        Me.UcError = New sqlrd.ucErrorHandler()
        Me.DividerLabel5 = New sqlrd.DividerLabel()
        Me.DividerLabel4 = New sqlrd.DividerLabel()
        Me.tabOptions = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.pnlMaxthreads = New System.Windows.Forms.FlowLayoutPanel()
        Me.chkExecuteAsync = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtMaxthreads = New DevComponents.Editors.IntegerInput()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        CType(Me.tabProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabProperties.SuspendLayout()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuperTabControlPanel7.SuspendLayout()
        Me.SuperTabControlPanel6.SuspendLayout()
        Me.SuperTabControlPanel5.SuspendLayout()
        Me.SuperTabControlPanel4.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.pnlMaxthreads.SuspendLayout()
        CType(Me.txtMaxthreads, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdApply
        '
        Me.cmdApply.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdApply.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdApply.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdApply.Enabled = False
        Me.cmdApply.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdApply.Location = New System.Drawing.Point(587, 3)
        Me.cmdApply.Name = "cmdApply"
        Me.cmdApply.Size = New System.Drawing.Size(75, 23)
        Me.cmdApply.TabIndex = 49
        Me.cmdApply.Text = "&Apply"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(506, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 48
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(425, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        '
        'txtID
        '
        Me.txtID.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtID.Border.Class = "TextBoxBorder"
        Me.txtID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtID.ForeColor = System.Drawing.Color.Black
        Me.txtID.Location = New System.Drawing.Point(12, 357)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(100, 21)
        Me.txtID.TabIndex = 13
        Me.txtID.Visible = False
        '
        'txtDesc
        '
        Me.txtDesc.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDesc.Border.Class = "TextBoxBorder"
        Me.txtDesc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDesc.ForeColor = System.Drawing.Color.Black
        Me.txtDesc.Location = New System.Drawing.Point(124, 57)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDesc.Size = New System.Drawing.Size(341, 173)
        Me.txtDesc.TabIndex = 3
        Me.txtDesc.Tag = "memo"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(3, 57)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(115, 16)
        Me.Label3.TabIndex = 34
        Me.Label3.Text = "Description (optional)"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(3, 236)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(115, 16)
        Me.Label7.TabIndex = 33
        Me.Label7.Text = "Keywords (optional)"
        '
        'txtKeyWord
        '
        Me.txtKeyWord.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtKeyWord.Border.Class = "TextBoxBorder"
        Me.txtKeyWord.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKeyWord.ForeColor = System.Drawing.Color.Blue
        Me.txtKeyWord.Location = New System.Drawing.Point(124, 236)
        Me.txtKeyWord.Name = "txtKeyWord"
        Me.txtKeyWord.Size = New System.Drawing.Size(341, 21)
        Me.txtKeyWord.TabIndex = 4
        Me.txtKeyWord.Tag = "memo"
        '
        'cmdLoc
        '
        Me.cmdLoc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdLoc.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoc.Location = New System.Drawing.Point(471, 3)
        Me.cmdLoc.Name = "cmdLoc"
        Me.cmdLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdLoc.TabIndex = 2
        Me.cmdLoc.Text = "..."
        '
        'txtFolder
        '
        Me.txtFolder.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtFolder.Border.Class = "TextBoxBorder"
        Me.txtFolder.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFolder.ForeColor = System.Drawing.Color.Blue
        Me.txtFolder.Location = New System.Drawing.Point(124, 3)
        Me.txtFolder.Name = "txtFolder"
        Me.txtFolder.ReadOnly = True
        Me.txtFolder.Size = New System.Drawing.Size(341, 21)
        Me.txtFolder.TabIndex = 1
        Me.txtFolder.Tag = "1"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(3, 3)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(92, 16)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Location"
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(124, 30)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(341, 21)
        Me.txtName.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(3, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Schedule Name"
        '
        'mnuParameters
        '
        Me.mnuParameters.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuParameter})
        '
        'mnuParameter
        '
        Me.mnuParameter.Index = 0
        Me.mnuParameter.Text = "Parameter Substitution"
        '
        'cmbPriority
        '
        Me.cmbPriority.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPriority.FormattingEnabled = True
        Me.cmbPriority.Items.AddRange(New Object() {"1 - High", "2 - Above Nornal", "3 - Normal", "4 - Below Normal", "5 - Low "})
        Me.cmbPriority.Location = New System.Drawing.Point(205, 157)
        Me.cmbPriority.Name = "cmbPriority"
        Me.cmbPriority.Size = New System.Drawing.Size(218, 21)
        Me.cmbPriority.TabIndex = 10
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.Location = New System.Drawing.Point(3, 160)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(172, 16)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Set the priority of this schedules to"
        '
        'cmbOpHrs
        '
        Me.cmbOpHrs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOpHrs.Enabled = False
        Me.cmbOpHrs.FormattingEnabled = True
        Me.cmbOpHrs.Location = New System.Drawing.Point(206, 111)
        Me.cmbOpHrs.Name = "cmbOpHrs"
        Me.cmbOpHrs.Size = New System.Drawing.Size(218, 21)
        Me.cmbOpHrs.TabIndex = 6
        '
        'chkOpHrs
        '
        Me.chkOpHrs.AutoSize = True
        Me.chkOpHrs.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkOpHrs.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkOpHrs.Location = New System.Drawing.Point(3, 111)
        Me.chkOpHrs.Name = "chkOpHrs"
        Me.chkOpHrs.Size = New System.Drawing.Size(172, 16)
        Me.chkOpHrs.TabIndex = 5
        Me.chkOpHrs.Text = "Use custom hours of operation"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 347.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.pnlMaxthreads, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.optNew, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.optExisting, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.optNone, 2, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(537, 57)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'optNew
        '
        Me.optNew.AutoSize = True
        Me.optNew.BackColor = System.Drawing.Color.Transparent
        Me.optNew.Location = New System.Drawing.Point(3, 3)
        Me.optNew.Name = "optNew"
        Me.optNew.Size = New System.Drawing.Size(87, 17)
        Me.optNew.TabIndex = 0
        Me.optNew.Text = "New Reports"
        Me.optNew.UseVisualStyleBackColor = False
        '
        'optExisting
        '
        Me.optExisting.AutoSize = True
        Me.optExisting.BackColor = System.Drawing.Color.Transparent
        Me.optExisting.Location = New System.Drawing.Point(96, 3)
        Me.optExisting.Name = "optExisting"
        Me.optExisting.Size = New System.Drawing.Size(112, 17)
        Me.optExisting.TabIndex = 1
        Me.optExisting.Text = "Existing schedules"
        Me.optExisting.UseVisualStyleBackColor = False
        '
        'optNone
        '
        Me.optNone.AutoSize = True
        Me.optNone.BackColor = System.Drawing.Color.Transparent
        Me.optNone.Location = New System.Drawing.Point(214, 3)
        Me.optNone.Name = "optNone"
        Me.optNone.Size = New System.Drawing.Size(50, 17)
        Me.optNone.TabIndex = 2
        Me.optNone.Text = "None"
        Me.optNone.UseVisualStyleBackColor = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel1.Controls.Add(Me.chkStatus)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 371)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(537, 27)
        Me.FlowLayoutPanel1.TabIndex = 2
        '
        'chkStatus
        '
        Me.chkStatus.AutoSize = True
        '
        '
        '
        Me.chkStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkStatus.Checked = True
        Me.chkStatus.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkStatus.CheckValue = "Y"
        Me.chkStatus.Dock = System.Windows.Forms.DockStyle.Fill
        Me.chkStatus.Location = New System.Drawing.Point(3, 3)
        Me.chkStatus.Name = "chkStatus"
        Me.chkStatus.Size = New System.Drawing.Size(61, 16)
        Me.chkStatus.TabIndex = 0
        Me.chkStatus.Text = "Enabled"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel2.ColumnCount = 3
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel2.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.cmbAnyAll, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label5, 2, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(537, 26)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(37, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Fulfill"
        '
        'cmbAnyAll
        '
        Me.cmbAnyAll.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmbAnyAll.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbAnyAll.FormattingEnabled = True
        Me.cmbAnyAll.Items.AddRange(New Object() {"ANY", "ALL"})
        Me.cmbAnyAll.Location = New System.Drawing.Point(46, 3)
        Me.cmbAnyAll.Name = "cmbAnyAll"
        Me.cmbAnyAll.Size = New System.Drawing.Size(64, 21)
        Me.cmbAnyAll.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.Location = New System.Drawing.Point(116, 3)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(195, 20)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "of the following conditions"
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel3.ColumnCount = 3
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel3.Controls.Add(Me.txtKeyWord, 1, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.Label7, 0, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.txtDesc, 1, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Label4, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Label3, 0, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Label2, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.txtName, 1, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.txtFolder, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.cmdLoc, 2, 0)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 4
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(534, 260)
        Me.TableLayoutPanel3.TabIndex = 51
        '
        'tabProperties
        '
        Me.tabProperties.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.tabProperties.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.tabProperties.ControlBox.MenuBox.Name = ""
        Me.tabProperties.ControlBox.Name = ""
        Me.tabProperties.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabProperties.ControlBox.MenuBox, Me.tabProperties.ControlBox.CloseBox})
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel3)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel1)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel2)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel7)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel6)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel5)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel4)
        Me.tabProperties.Dock = System.Windows.Forms.DockStyle.Top
        Me.tabProperties.ForeColor = System.Drawing.Color.Black
        Me.tabProperties.Location = New System.Drawing.Point(0, 0)
        Me.tabProperties.Name = "tabProperties"
        Me.tabProperties.ReorderTabsEnabled = True
        Me.tabProperties.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.tabProperties.SelectedTabIndex = 0
        Me.tabProperties.Size = New System.Drawing.Size(665, 398)
        Me.tabProperties.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.tabProperties.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.tabProperties.TabIndex = 52
        Me.tabProperties.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabGeneral, Me.tabCondition, Me.tabSchedules, Me.tabOptions, Me.tabTasks, Me.tabExecution, Me.tabHistory})
        Me.tabProperties.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        Me.tabProperties.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.UcReports1)
        Me.SuperTabControlPanel3.Controls.Add(Me.UcExisting)
        Me.SuperTabControlPanel3.Controls.Add(Me.TableLayoutPanel1)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(128, 0)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(537, 398)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.tabSchedules
        '
        'UcReports1
        '
        Me.UcReports1.BackColor = System.Drawing.Color.Transparent
        Me.UcReports1.eventBased = True
        Me.UcReports1.Location = New System.Drawing.Point(6, 89)
        Me.UcReports1.m_MasterID = 99999
        Me.UcReports1.Name = "UcReports1"
        Me.UcReports1.Size = New System.Drawing.Size(179, 199)
        Me.UcReports1.TabIndex = 11
        '
        'UcExisting
        '
        Me.UcExisting.BackColor = System.Drawing.Color.Transparent
        Me.UcExisting.Location = New System.Drawing.Point(427, 95)
        Me.UcExisting.m_eventID = 99999
        Me.UcExisting.Name = "UcExisting"
        Me.UcExisting.Size = New System.Drawing.Size(237, 233)
        Me.UcExisting.TabIndex = 1
        '
        'tabSchedules
        '
        Me.tabSchedules.AttachedControl = Me.SuperTabControlPanel3
        Me.tabSchedules.GlobalItem = False
        Me.tabSchedules.Image = CType(resources.GetObject("tabSchedules.Image"), System.Drawing.Image)
        Me.tabSchedules.Name = "tabSchedules"
        Me.tabSchedules.Text = "Schedules"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.TableLayoutPanel3)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(128, 0)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(537, 398)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabGeneral
        '
        'tabGeneral
        '
        Me.tabGeneral.AttachedControl = Me.SuperTabControlPanel1
        Me.tabGeneral.GlobalItem = False
        Me.tabGeneral.Image = CType(resources.GetObject("tabGeneral.Image"), System.Drawing.Image)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Text = "General"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.UcConditions1)
        Me.SuperTabControlPanel2.Controls.Add(Me.FlowLayoutPanel1)
        Me.SuperTabControlPanel2.Controls.Add(Me.TableLayoutPanel2)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(128, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(537, 398)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.tabCondition
        '
        'UcConditions1
        '
        Me.UcConditions1.BackColor = System.Drawing.Color.Transparent
        Me.UcConditions1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UcConditions1.Location = New System.Drawing.Point(0, 26)
        Me.UcConditions1.m_EventID = 99999
        Me.UcConditions1.m_OrderID = 0
        Me.UcConditions1.Name = "UcConditions1"
        Me.UcConditions1.Size = New System.Drawing.Size(537, 315)
        Me.UcConditions1.TabIndex = 1
        '
        'tabCondition
        '
        Me.tabCondition.AttachedControl = Me.SuperTabControlPanel2
        Me.tabCondition.GlobalItem = False
        Me.tabCondition.Image = CType(resources.GetObject("tabCondition.Image"), System.Drawing.Image)
        Me.tabCondition.Name = "tabCondition"
        Me.tabCondition.Text = "Conditions"
        '
        'SuperTabControlPanel7
        '
        Me.SuperTabControlPanel7.Controls.Add(Me.scheduleHistory)
        Me.SuperTabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel7.Location = New System.Drawing.Point(128, 0)
        Me.SuperTabControlPanel7.Name = "SuperTabControlPanel7"
        Me.SuperTabControlPanel7.Size = New System.Drawing.Size(537, 398)
        Me.SuperTabControlPanel7.TabIndex = 0
        Me.SuperTabControlPanel7.TabItem = Me.tabHistory
        '
        'scheduleHistory
        '
        Me.scheduleHistory.BackColor = System.Drawing.Color.Transparent
        Me.scheduleHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.scheduleHistory.Location = New System.Drawing.Point(0, 0)
        Me.scheduleHistory.m_filter = False
        Me.scheduleHistory.m_historyLimit = 0
        Me.scheduleHistory.m_objectID = 0
        Me.scheduleHistory.m_scheduleType = sqlrd.clsMarsScheduler.enScheduleType.NONE
        Me.scheduleHistory.m_showControls = False
        Me.scheduleHistory.m_showSchedulesList = False
        Me.scheduleHistory.m_sortOrder = " ASC "
        Me.scheduleHistory.Name = "scheduleHistory"
        Me.scheduleHistory.Size = New System.Drawing.Size(537, 398)
        Me.scheduleHistory.TabIndex = 0
        '
        'tabHistory
        '
        Me.tabHistory.AttachedControl = Me.SuperTabControlPanel7
        Me.tabHistory.GlobalItem = False
        Me.tabHistory.Image = CType(resources.GetObject("tabHistory.Image"), System.Drawing.Image)
        Me.tabHistory.Name = "tabHistory"
        Me.tabHistory.Text = "History"
        '
        'SuperTabControlPanel6
        '
        Me.SuperTabControlPanel6.Controls.Add(Me.ucFlow)
        Me.SuperTabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel6.Location = New System.Drawing.Point(128, 0)
        Me.SuperTabControlPanel6.Name = "SuperTabControlPanel6"
        Me.SuperTabControlPanel6.Size = New System.Drawing.Size(537, 398)
        Me.SuperTabControlPanel6.TabIndex = 0
        Me.SuperTabControlPanel6.TabItem = Me.tabExecution
        '
        'ucFlow
        '
        Me.ucFlow.BackColor = System.Drawing.Color.Transparent
        Me.ucFlow.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ucFlow.Location = New System.Drawing.Point(5, 5)
        Me.ucFlow.m_ExecutionFlow = "AFTER"
        Me.ucFlow.Name = "ucFlow"
        Me.ucFlow.Size = New System.Drawing.Size(406, 171)
        Me.ucFlow.TabIndex = 0
        '
        'tabExecution
        '
        Me.tabExecution.AttachedControl = Me.SuperTabControlPanel6
        Me.tabExecution.GlobalItem = False
        Me.tabExecution.Image = CType(resources.GetObject("tabExecution.Image"), System.Drawing.Image)
        Me.tabExecution.Name = "tabExecution"
        Me.tabExecution.Text = "Execution Flow"
        '
        'SuperTabControlPanel5
        '
        Me.SuperTabControlPanel5.Controls.Add(Me.UcTasks)
        Me.SuperTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel5.Location = New System.Drawing.Point(128, 0)
        Me.SuperTabControlPanel5.Name = "SuperTabControlPanel5"
        Me.SuperTabControlPanel5.Size = New System.Drawing.Size(537, 398)
        Me.SuperTabControlPanel5.TabIndex = 0
        Me.SuperTabControlPanel5.TabItem = Me.tabTasks
        '
        'UcTasks
        '
        Me.UcTasks.BackColor = System.Drawing.Color.Transparent
        Me.UcTasks.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcTasks.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcTasks.ForeColor = System.Drawing.Color.Navy
        Me.UcTasks.Location = New System.Drawing.Point(0, 0)
        Me.UcTasks.m_defaultTaks = False
        Me.UcTasks.m_eventBased = False
        Me.UcTasks.m_eventID = 99999
        Me.UcTasks.m_forExceptionHandling = False
        Me.UcTasks.m_showAfterType = True
        Me.UcTasks.m_showExpanded = True
        Me.UcTasks.Name = "UcTasks"
        Me.UcTasks.Size = New System.Drawing.Size(537, 398)
        Me.UcTasks.TabIndex = 0
        '
        'tabTasks
        '
        Me.tabTasks.AttachedControl = Me.SuperTabControlPanel5
        Me.tabTasks.GlobalItem = False
        Me.tabTasks.Image = CType(resources.GetObject("tabTasks.Image"), System.Drawing.Image)
        Me.tabTasks.Name = "tabTasks"
        Me.tabTasks.Text = "Tasks"
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.cmbPriority)
        Me.SuperTabControlPanel4.Controls.Add(Me.Label6)
        Me.SuperTabControlPanel4.Controls.Add(Me.cmbOpHrs)
        Me.SuperTabControlPanel4.Controls.Add(Me.chkOpHrs)
        Me.SuperTabControlPanel4.Controls.Add(Me.DividerLabel3)
        Me.SuperTabControlPanel4.Controls.Add(Me.UcError)
        Me.SuperTabControlPanel4.Controls.Add(Me.DividerLabel5)
        Me.SuperTabControlPanel4.Controls.Add(Me.DividerLabel4)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(128, 0)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(537, 398)
        Me.SuperTabControlPanel4.TabIndex = 0
        Me.SuperTabControlPanel4.TabItem = Me.tabOptions
        '
        'DividerLabel3
        '
        Me.DividerLabel3.BackColor = System.Drawing.Color.Transparent
        Me.DividerLabel3.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel3.Location = New System.Drawing.Point(3, 8)
        Me.DividerLabel3.Name = "DividerLabel3"
        Me.DividerLabel3.Size = New System.Drawing.Size(435, 13)
        Me.DividerLabel3.Spacing = 0
        Me.DividerLabel3.TabIndex = 4
        Me.DividerLabel3.Text = "Exception Handling"
        '
        'UcError
        '
        Me.UcError.BackColor = System.Drawing.Color.Transparent
        Me.UcError.Location = New System.Drawing.Point(3, 24)
        Me.UcError.m_showFailOnOne = False
        Me.UcError.Name = "UcError"
        Me.UcError.Size = New System.Drawing.Size(440, 62)
        Me.UcError.TabIndex = 0
        '
        'DividerLabel5
        '
        Me.DividerLabel5.BackColor = System.Drawing.Color.Transparent
        Me.DividerLabel5.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel5.Location = New System.Drawing.Point(3, 141)
        Me.DividerLabel5.Name = "DividerLabel5"
        Me.DividerLabel5.Size = New System.Drawing.Size(429, 19)
        Me.DividerLabel5.Spacing = 0
        Me.DividerLabel5.TabIndex = 8
        Me.DividerLabel5.Text = "Schedule Priority"
        '
        'DividerLabel4
        '
        Me.DividerLabel4.BackColor = System.Drawing.Color.Transparent
        Me.DividerLabel4.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel4.Location = New System.Drawing.Point(3, 89)
        Me.DividerLabel4.Name = "DividerLabel4"
        Me.DividerLabel4.Size = New System.Drawing.Size(429, 19)
        Me.DividerLabel4.Spacing = 0
        Me.DividerLabel4.TabIndex = 3
        Me.DividerLabel4.Text = "Hours of Operation"
        '
        'tabOptions
        '
        Me.tabOptions.AttachedControl = Me.SuperTabControlPanel4
        Me.tabOptions.GlobalItem = False
        Me.tabOptions.Image = CType(resources.GetObject("tabOptions.Image"), System.Drawing.Image)
        Me.tabOptions.Name = "tabOptions"
        Me.tabOptions.Text = "Schedule Options"
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel4.Controls.Add(Me.cmdApply)
        Me.FlowLayoutPanel4.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel4.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(0, 401)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(665, 30)
        Me.FlowLayoutPanel4.TabIndex = 53
        '
        'pnlMaxthreads
        '
        Me.pnlMaxthreads.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.SetColumnSpan(Me.pnlMaxthreads, 2)
        Me.pnlMaxthreads.Controls.Add(Me.chkExecuteAsync)
        Me.pnlMaxthreads.Controls.Add(Me.txtMaxthreads)
        Me.pnlMaxthreads.Controls.Add(Me.Label8)
        Me.pnlMaxthreads.Enabled = False
        Me.pnlMaxthreads.Location = New System.Drawing.Point(96, 29)
        Me.pnlMaxthreads.Name = "pnlMaxthreads"
        Me.pnlMaxthreads.Size = New System.Drawing.Size(413, 25)
        Me.pnlMaxthreads.TabIndex = 12
        '
        'chkExecuteAsync
        '
        '
        '
        '
        Me.chkExecuteAsync.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkExecuteAsync.Location = New System.Drawing.Point(3, 3)
        Me.chkExecuteAsync.Name = "chkExecuteAsync"
        Me.chkExecuteAsync.Size = New System.Drawing.Size(247, 23)
        Me.chkExecuteAsync.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkExecuteAsync.TabIndex = 2
        Me.chkExecuteAsync.Text = "Execute schedules asyncronously. Use up to "
        '
        'txtMaxthreads
        '
        '
        '
        '
        Me.txtMaxthreads.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtMaxthreads.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtMaxthreads.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.txtMaxthreads.Enabled = False
        Me.txtMaxthreads.Location = New System.Drawing.Point(256, 3)
        Me.txtMaxthreads.MaxValue = 8
        Me.txtMaxthreads.MinValue = 2
        Me.txtMaxthreads.Name = "txtMaxthreads"
        Me.txtMaxthreads.ShowUpDown = True
        Me.txtMaxthreads.Size = New System.Drawing.Size(49, 21)
        Me.txtMaxthreads.TabIndex = 3
        Me.txtMaxthreads.Value = 2
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(311, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(56, 26)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "threads"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmEventProp
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(665, 431)
        Me.ControlBox = False
        Me.Controls.Add(Me.FlowLayoutPanel4)
        Me.Controls.Add(Me.tabProperties)
        Me.Controls.Add(Me.txtID)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmEventProp"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Event-Based Properties"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        CType(Me.tabProperties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabProperties.ResumeLayout(False)
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel7.ResumeLayout(False)
        Me.SuperTabControlPanel6.ResumeLayout(False)
        Me.SuperTabControlPanel5.ResumeLayout(False)
        Me.SuperTabControlPanel4.ResumeLayout(False)
        Me.SuperTabControlPanel4.PerformLayout()
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.pnlMaxthreads.ResumeLayout(False)
        CType(Me.txtMaxthreads, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private ReadOnly Property m_ScheduleType() As String
        Get
            If Me.optExisting.Checked Then
                Return "Existing"
            ElseIf optNone.Checked Then
                Return "None"
            Else
                Return "New"
            End If
        End Get
    End Property
    Public Property m_Packed() As Boolean
        Get
            Return packed
        End Get
        Set(ByVal value As Boolean)
            If value = True Then
                Me.txtFolder.Text = "\Package"
                Me.txtFolder.Visible = False
                Me.cmdLoc.Visible = False
                Me.Label4.Visible = False
            End If
        End Set
    End Property

    Private ReadOnly Property m_autoCalcValue() As Decimal
        Get
            Dim val As Decimal = clsMarsScheduler.globalItem.getScheduleDuration(0, 0, 0, 0, txtID.Text, clsMarsScheduler.enCalcType.MEDIAN, 3)
        End Get
    End Property

    Private Sub frmEventProp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        FormatForWinXP(Me)

        Dim oUI As New clsMarsUI

        oUI.BuildAdvTree(UcExisting.tvSchedules, True)

        UcExisting.tvSchedules.SelectedNode = UcExisting.tvSchedules.Nodes(0)

        UcExisting.tvSchedules.Nodes(0).Expand()

        UcTasks.ShowAfterType = False
        UcTasks.tvTasks.ColumnsVisible = Visible

        Me.cmbPriority.Text = schedulePriority
    End Sub

    Sub updateOrderIDForExistingSchedules(eventid As Integer)
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        SQL = "SELECT * FROM eventschedule WHERE eventid = " & eventid & " AND ordernumber IS NULL"

        oRs = clsMarsData.GetData(SQL)

        Dim count As Integer = 1
        Dim scheduleid As Integer

        Do While oRs.EOF = False
            scheduleid = oRs("scheduleid").Value
            clsMarsData.WriteData("UPDATE eventschedule SET ordernumber = " & count & " WHERE scheduleid = " & scheduleid)
            count += 1
            oRs.MoveNext()
        Loop

        oRs.Close()
        oRs = Nothing

    End Sub

    Public Function EditSchedule(ByVal nEventID As Integer) As Boolean
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim nImage As Integer
        Dim scheduleType As String
        Dim autoCalc As Boolean
        Dim packID As Integer = 0

        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.s3_EventBasedScheds) = False Then
            _NeedUpgrade(gEdition.ENTERPRISEPROPLUS, Me, "Event-Based Schedules")
            Return True
        End If

        txtID.Text = nEventID

        clsMarsEvent.currentEventID = nEventID

        updateOrderIDForExistingSchedules(nEventID)

        SQL = "SELECT * FROM EventAttr6 WHERE EventID =" & nEventID

        oRs = clsMarsData.GetData(SQL)

        Me.UcExisting.parentID = txtID.Text
        Me.UcExisting.m_eventID = nEventID
        UcReports1.eventBased = True
        UcReports1.m_MasterID = nEventID

        UcTasks.ScheduleID = nEventID
        UcTasks.m_eventID = nEventID
        UcTasks.m_eventBased = True
        UcTasks.LoadTasks()

        Me.UcConditions1.m_EventID = nEventID
        Me.UcConditions1.LoadConditions(nEventID)

        Me.UcReports1.m_MasterID = nEventID

        Try
            If oRs.EOF = True Then Return True

            txtName.Text = oRs("eventname").Value

            Me.Text = "Event-Based Schedule Properties - " & txtName.Text

            packID = IsNull(oRs("packid").Value, 0)

            autoCalc = IsNull(oRs("autocalc").Value, 0)
            Me.chkOpHrs.Checked = IsNull(oRs("useoperationhours").Value, 0)

            If Me.chkOpHrs.Checked = True Then
                Me.cmbOpHrs_DropDown(Nothing, Nothing)

                Me.cmbOpHrs.Text = IsNull(oRs("operationname").Value, "")
            End If

            If packID > 0 Then
                Me.DividerLabel4.Visible = False
                Me.chkOpHrs.Visible = False
                Me.cmbOpHrs.Visible = False

                Me.DividerLabel5.Visible = False
                Me.Label6.Visible = False
                Me.cmbPriority.Visible = False
            End If

            Dim rsP As ADODB.Recordset = clsMarsData.GetData("SELECT FolderName FROM Folders WHERE FolderID = " & oRs("Parent").Value)

            If rsP.EOF = False Then
                txtFolder.Text = rsP.Fields(0).Value

                txtFolder.Tag = oRs("Parent").Value

            Else
                txtFolder.Text = "\Package"
                txtFolder.Tag = oRs("packid").Value
            End If

            rsP.Close()
            'Dim sType As String = oRs("eventtype").Value

            If oRs("status").Value = 0 Then
                chkStatus.Checked = False
            Else
                chkStatus.Checked = True
            End If

            scheduleType = oRs("scheduletype").Value

            If scheduleType = "Existing" Then
                optExisting.Checked = True

            ElseIf scheduleType = "None" Then
                optNone.Checked = True
            Else
                optNew.Checked = True
            End If

            'schedule priority
            Try
                schedulePriority = IsNull(oRs("schedulepriority").Value, "2 - Normal")
            Catch ex As Exception
                schedulePriority = "3 - Normal"
            End Try

            ucFlow.m_ExecutionFlow = IsNull(oRs("executionflow").Value, "AFTER")

            txtDesc.Text = IsNull(oRs("description").Value)
            txtKeyWord.Text = IsNull(oRs("keyword").Value)

            cmbAnyAll.Text = oRs("anyall").Value

            Try
                UcError.m_autoFailAfter = IsNull(oRs("autofailafter").Value, 30)
                UcError.cmbRetry.Value = IsNull(oRs("retrycount").Value, 5)
                UcError.txtRetryInterval.Value = IsNull(oRs("retryinterval").Value, 0)
            Catch ex As Exception
                UcError.cmbRetry.Value = 5
                UcError.m_autoFailAfter = 30
            End Try

            UcError.chkAutoCalc.Checked = autoCalc

            If autoCalc = True Then
                Dim temp As Decimal = Me.m_autoCalcValue
                UcError.chkAutoCalc.Checked = True
                UcError.m_autoFailAfter = IIf(temp < 1, 1, temp)
                UcError.cmbAssumeFail.Enabled = False
            End If

            Try
                chkExecuteAsync.Checked = IsNull(oRs("existingasync").Value, 0)
            Catch : End Try

            Try
                txtMaxthreads.Value = IsNull(oRs("maxthreads").Value, 2)
            Catch : End Try

            oRs.Close()

            If UcTasks.tvTasks.Nodes.Count = 0 Then
                ucFlow.Enabled = False
            Else
                ucFlow.Enabled = True
            End If

            Me.ShowDialog()
        Catch ex As Exception
            ''console.writeline(ex.Message)
        End Try

        Return chkStatus.Checked
    End Function

    Private Sub cmdApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdApply.Click

        If txtName.Text.Length = 0 Then
            SetError(txtName, "Please enter the name of the schedule")
            tabProperties.SelectedTab = tabGeneral
            okayToClose = False
            txtName.Focus()
            Return
        ElseIf txtFolder.Text.Length = 0 Then
            SetError(txtFolder, "Please select the schedule's destination folder")
            tabProperties.SelectedTab = tabGeneral
            okayToClose = False
            txtFolder.Focus()
            Return
        ElseIf Me.chkOpHrs.Checked Then
            If Me.cmbOpHrs.Text = "" Or Me.cmbOpHrs.Text = "<New...>" Then
                SetError(Me.cmbOpHrs, "Please select a valid entry...")
                Me.cmbOpHrs.Focus()
                okayToClose = False
                Return
            End If
        ElseIf optExisting.Checked And Me.UcExisting.lsvSchedules.Nodes.Count = 0 Then
            setError(Me.optExisting, "Please add existing schedules to execute")
            UcExisting.btnAdd.Focus()
            okayToClose = False
            Return
        ElseIf clsMarsUI.candoRename(txtName.Text, txtFolder.Tag, clsMarsScheduler.enScheduleType.EVENTBASED, txtID.Text) = False Then
            SetError(Me.txtName, "An Event-Based schedule with this name already exists in this folder")
            txtName.Focus()
            okayToClose = False
            Return
        End If

        okayToClose = True

        Dim SQL As String
        Dim oData As New clsMarsData
        Dim sCols As String
        Dim sVals As String
        Dim nFire As Integer
        Dim OldName As String
        Dim newName As String = txtName.Text.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "")

        Try
            Dim oRename As ADODB.Recordset

            oRename = clsMarsData.GetData("SELECT EventName FROM EventAttr6 WHERE EventID =" & txtID.Text)

            If oRename.EOF = False Then
                OldName = oRename.Fields(0).Value

                OldName = OldName.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "")
            End If

            oRename.Close()
        Catch
            OldName = txtName.Text.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "")
        End Try

        sVals = "EventName ='" & SQLPrepare(newName) & "'," & _
        "Description ='" & SQLPrepare(txtDesc.Text) & "'," & _
        "Keyword ='" & SQLPrepare(txtKeyWord.Text) & "'," & _
        "Parent =" & txtFolder.Tag & "," & _
        "Status =" & Convert.ToInt32(chkStatus.Checked) & "," & _
        "ScheduleType='" & Me.m_ScheduleType & "', " & _
        "AnyAll = '" & cmbAnyAll.Text & "', " & _
        "ExecutionFlow = '" & ucFlow.m_ExecutionFlow & "', " & _
        "RetryCount = " & UcError.cmbRetry.Value & "," & _
        "AutoFailAfter =" & UcError.m_autoFailAfter(True) & "," & _
        "AutoCalc =" & Convert.ToInt32(UcError.chkAutoCalc.Checked) & "," & _
        "RetryInterval =" & UcError.txtRetryInterval.Value & "," & _
        "UseOperationHours =" & Convert.ToInt32(Me.chkOpHrs.Checked) & "," & _
        "OperationName = '" & SQLPrepare(Me.cmbOpHrs.Text) & "'," & _
        "SchedulePriority = '" & Me.cmbPriority.Text & "'," & _
        "existingasync = " & Convert.ToInt16(chkExecuteAsync.Checked) & "," & _
        "maxthreads = " & txtMaxthreads.Value

        SQL = "UPDATE EventAttr6 SET " & sVals & " WHERE EventID =" & txtID.Text

        If clsMarsData.WriteData(SQL) = True Then
            Dim nID As Integer
            Dim sType As String
            Dim I As Integer = 1

            sCols = "ID,EventID,ScheduleID,ScheduleType, OrderNumber"

            clsMarsData.WriteData("DELETE FROM EventSchedule WHERE EventID =" & txtID.Text)

            Dim ordernumber As Integer = 1

            For Each oItem As DevComponents.AdvTree.Node In UcExisting.lsvSchedules.Nodes
                nID = oItem.DataKey ' oItem.Tag.Split(":")(1)
                sType = oItem.Tag '.Split(":")(0)


                sVals = clsMarsData.CreateDataID("eventschedule", "id") & "," & _
                txtID.Text & "," & _
                nID & "," & _
                "'" & sType & "'," & _
                ordernumber

                SQL = "INSERT INTO EventSchedule(" & sCols & ") VALUES(" & sVals & ")"

                If clsMarsData.WriteData(SQL) = False Then
                    Exit For
                End If

                ordernumber += 1
            Next
        End If

        Try
            Dim sPath As String

            If OldName <> txtName.Text.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "") Then
                sPath = clsMarsEvent.m_CachedDataPath & OldName

                System.IO.Directory.Move(sPath, clsMarsEvent.m_CachedDataPath & txtName.Text.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", ""))
            End If

        Catch : End Try

        setAndShowMessageOnControl(cmdApply, "Saved!", "", "Your schedule has been updated successfully")
    End Sub

    Private Sub cmdLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoc.Click
        Dim oFolders As frmFolders = New frmFolders
        Dim sFolder(1) As String

        sFolder = oFolders.GetFolder

        If sFolder(0) <> "" And sFolder(0) <> "Desktop" Then
            txtFolder.Text = sFolder(0)
            txtFolder.Tag = sFolder(1)
        End If
    End Sub

    Dim okayToClose As Boolean = True

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        cmdApply_Click(sender, e)

        If okayToClose = False Then Return

        Close()
        Dim oUI As New clsMarsUI



        clsMarsEvent.currentEventID = 99999

        clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.EVENTS, clsMarsAudit.AuditAction.EDIT)
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        cmdApply.Enabled = True
        SetError(sender, String.Empty)
        NameChange = True
    End Sub

    Private Sub txtFolder_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFolder.TextChanged
        cmdApply.Enabled = True
        SetError(sender, String.Empty)
    End Sub

    Private Sub mnuParameter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuParameter.Click
        Dim sType As String
        Dim nID As Integer

        If UcExisting.lsvSchedules.SelectedNodes.Count = 0 Then Return

        Dim oItem As DevComponents.AdvTree.Node

        oItem = UcExisting.lsvSchedules.SelectedNodes(0)

        nID = oItem.DataKey ' oItem.Tag.Split(":")(1)
        sType = oItem.Tag '.Split(":")(0)

        'sType = oItem.Tag.Split(":")(0)
        'nID = oItem.Tag.Split(":")(1)

        If sType.ToLower <> "report" Then Return

        Dim oSub As frmEventSub = New frmEventSub
        oSub.m_eventID = txtID.Text
        oSub.SetSubstitution(nID, txtID.Text)
    End Sub

    Private Sub optNew_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optNew.CheckedChanged, optExisting.CheckedChanged, optNone.CheckedChanged
        If txtID.Text.Length = 0 Then Return

        Try

            pnlMaxthreads.Enabled = optExisting.Checked

            If optNew.Checked = True Then
                UcReports1.Visible = True
                UcReports1.BringToFront()
                UcReports1.Dock = DockStyle.Fill
                UcExisting.Visible = False
                UcExisting.Dock = DockStyle.None

                ' UcReports1.Size = New Size(516, 361)
                ' UcReports1.Location = New Point(13, 30)
                With UcReports1
                    .m_MasterID = txtID.Text
                    .LoadReports()
                End With

                tabSchedules.Text = "Reports"
            ElseIf optExisting.Checked Then
                UcExisting.Visible = True
                UcExisting.Dock = DockStyle.Fill
                UcExisting.BringToFront()
                UcReports1.Visible = False
                UcReports1.Dock = DockStyle.None
                With UcExisting
                    .m_eventID = txtID.Text
                    .LoadSelectedReports()
                    clsMarsUI.MainUI.BuildAdvTree(.tvSchedules, True)
                    .lsvSchedules.ContextMenu = .mnuParameters
                End With

                tabSchedules.Text = "Schedules"
            ElseIf optNone.Checked = True Then
                UcReports1.Visible = False
                UcExisting.Visible = False

            End If
        Catch : End Try
    End Sub

    Dim closeFlag As Boolean

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click


        If closeFlag = False Then
            Dim sp As DevComponents.DotNetBar.SuperTooltip = New DevComponents.DotNetBar.SuperTooltip
            Dim spi As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo("Really Cancel?", "",
                                                                                                               "Are you sure you would like to close the wizard? All your progress will be lost. Click Cancel again to confirm.",
                                                                                                           Nothing,
                                                                                                               Nothing,
                                                                                                               DevComponents.DotNetBar.eTooltipColor.Office2003)
            sp.SetSuperTooltip(sender, spi)
            sp.ShowTooltip(sender)
            closeFlag = True
        Else
            clsMarsEvent.currentEventID = 99999
            Dim oCleaner As clsMarsData = New clsMarsData
            oCleaner.CleanDB()
            Me.Close()
        End If
    End Sub
    Private Sub chkOpHrs_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkOpHrs.CheckedChanged
        Me.cmbOpHrs.Enabled = Me.chkOpHrs.Checked
    End Sub

    Private Sub cmbOpHrs_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbOpHrs.SelectedIndexChanged
        SetError(sender, "")

        If cmbOpHrs.Text = "<New...>" Then
            Dim opAdd As frmOperationalHours = New frmOperationalHours
            Dim sNew As String

            sNew = opAdd.AddOperationalHours

            If sNew <> "" Then
                Me.cmbOpHrs.Items.Add(sNew)

                Me.cmbOpHrs.Text = sNew
            End If
        End If
    End Sub

    Private Sub cmbOpHrs_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbOpHrs.DropDown
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        Me.cmbOpHrs.Items.Clear()

        SQL = "SELECT * FROM OperationAttr"

        oRs = clsMarsData.GetData(SQL)

        Me.cmbOpHrs.Items.Add("<New...>")

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                cmbOpHrs.Items.Add(oRs("operationname").Value)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub

    Private Sub chkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkStatus.CheckedChanged
        Try
            If chkStatus.Checked And clsMarsEvent.isEventLocked(txtID.Text) Then
                showViolatorMessage()
                chkStatus.Checked = False
                Return
            End If
        Catch : End Try
    End Sub

    Private Sub tabProperties_SelectedTabChanged(sender As System.Object, e As DevComponents.DotNetBar.SuperTabStripSelectedTabChangedEventArgs) Handles tabProperties.SelectedTabChanged
        If e.NewValue.Text = "History" Then
            With scheduleHistory
                .m_objectID = txtID.Text
                .m_scheduleType = clsMarsScheduler.enScheduleType.EVENTBASED
                .m_showControls = True
                .DrawEventHistory()
            End With
        End If
    End Sub

    

    Private Sub chkExecuteAsync_CheckedChanged(sender As Object, e As EventArgs) Handles chkExecuteAsync.CheckedChanged
        txtMaxthreads.Enabled = chkExecuteAsync.Checked
    End Sub
End Class
