Imports Microsoft.Win32
Imports DevComponents.AdvTree
Public Class frmUserManager
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim sPrev As String = String.Empty
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents chkNTAuth As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents lsvGroups As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents cmdDelete As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdAdd As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdEdit As DevComponents.DotNetBar.ButtonX
    Friend WithEvents tabUsers As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents stabSQLRD As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem3 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents stabNT As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel4 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel6 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents tvNTUsers As DevComponents.AdvTree.AdvTree
    Friend WithEvents Node2 As DevComponents.AdvTree.Node
    Friend WithEvents NodeConnector1 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle2 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents tvUsers As DevComponents.AdvTree.AdvTree
    Friend WithEvents Node1 As DevComponents.AdvTree.Node
    Friend WithEvents NodeConnector2 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle1 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents Node4 As DevComponents.AdvTree.Node
    Friend WithEvents Node3 As DevComponents.AdvTree.Node
    Friend WithEvents Bar1 As DevComponents.DotNetBar.Bar
    Friend WithEvents btnAddUser As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnEditUser As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents Bar2 As DevComponents.DotNetBar.Bar
    Friend WithEvents btnNTAddUser As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnNTEditUser As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnNTDeleteUser As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ContextMenuBar1 As DevComponents.DotNetBar.ContextMenuBar
    Friend WithEvents mnuUsers As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnuEnableWeb As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnuDisableWeb As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnDeleteUser As DevComponents.DotNetBar.ButtonItem



    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUserManager))
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.chkNTAuth = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.lsvGroups = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmdDelete = New DevComponents.DotNetBar.ButtonX()
        Me.cmdEdit = New DevComponents.DotNetBar.ButtonX()
        Me.cmdAdd = New DevComponents.DotNetBar.ButtonX()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.tabUsers = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.FlowLayoutPanel6 = New System.Windows.Forms.FlowLayoutPanel()
        Me.SuperTabItem3 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tvUsers = New DevComponents.AdvTree.AdvTree()
        Me.Node1 = New DevComponents.AdvTree.Node()
        Me.Node4 = New DevComponents.AdvTree.Node()
        Me.NodeConnector2 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle1 = New DevComponents.DotNetBar.ElementStyle()
        Me.Bar1 = New DevComponents.DotNetBar.Bar()
        Me.btnAddUser = New DevComponents.DotNetBar.ButtonItem()
        Me.btnEditUser = New DevComponents.DotNetBar.ButtonItem()
        Me.btnDeleteUser = New DevComponents.DotNetBar.ButtonItem()
        Me.stabSQLRD = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tvNTUsers = New DevComponents.AdvTree.AdvTree()
        Me.Node2 = New DevComponents.AdvTree.Node()
        Me.Node3 = New DevComponents.AdvTree.Node()
        Me.NodeConnector1 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle2 = New DevComponents.DotNetBar.ElementStyle()
        Me.Bar2 = New DevComponents.DotNetBar.Bar()
        Me.btnNTAddUser = New DevComponents.DotNetBar.ButtonItem()
        Me.btnNTEditUser = New DevComponents.DotNetBar.ButtonItem()
        Me.btnNTDeleteUser = New DevComponents.DotNetBar.ButtonItem()
        Me.stabNT = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.ContextMenuBar1 = New DevComponents.DotNetBar.ContextMenuBar()
        Me.mnuUsers = New DevComponents.DotNetBar.ButtonItem()
        Me.mnuEnableWeb = New DevComponents.DotNetBar.ButtonItem()
        Me.mnuDisableWeb = New DevComponents.DotNetBar.ButtonItem()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tabUsers, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabUsers.SuspendLayout()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.FlowLayoutPanel6.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        CType(Me.tvUsers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControlPanel2.SuspendLayout()
        CType(Me.tvNTUsers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Bar2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel4.SuspendLayout()
        CType(Me.ContextMenuBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'chkNTAuth
        '
        '
        '
        '
        Me.chkNTAuth.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkNTAuth.Location = New System.Drawing.Point(3, 3)
        Me.chkNTAuth.Name = "chkNTAuth"
        Me.chkNTAuth.Size = New System.Drawing.Size(440, 24)
        Me.chkNTAuth.TabIndex = 0
        Me.chkNTAuth.Text = "Enable Windows Integrated Authentication"
        '
        'lsvGroups
        '
        Me.lsvGroups.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvGroups.Border.Class = "ListViewBorder"
        Me.lsvGroups.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvGroups.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lsvGroups.DisabledBackColor = System.Drawing.Color.Empty
        Me.lsvGroups.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvGroups.ForeColor = System.Drawing.Color.Black
        Me.lsvGroups.FullRowSelect = True
        Me.lsvGroups.Location = New System.Drawing.Point(0, 0)
        Me.lsvGroups.Name = "lsvGroups"
        Me.lsvGroups.Size = New System.Drawing.Size(941, 482)
        Me.lsvGroups.TabIndex = 0
        Me.lsvGroups.UseCompatibleStateImageBehavior = False
        Me.lsvGroups.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Group Name"
        Me.ColumnHeader1.Width = 138
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Description"
        Me.ColumnHeader2.Width = 315
        '
        'cmdDelete
        '
        Me.cmdDelete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdDelete.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDelete.Location = New System.Drawing.Point(165, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(75, 23)
        Me.cmdDelete.TabIndex = 15
        Me.cmdDelete.Text = "&Delete"
        '
        'cmdEdit
        '
        Me.cmdEdit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdEdit.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEdit.Location = New System.Drawing.Point(84, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(75, 23)
        Me.cmdEdit.TabIndex = 16
        Me.cmdEdit.Text = "&Edit"
        '
        'cmdAdd
        '
        Me.cmdAdd.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdAdd.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAdd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAdd.Location = New System.Drawing.Point(3, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(75, 23)
        Me.cmdAdd.TabIndex = 14
        Me.cmdAdd.Text = "&Add"
        '
        'TabPage4
        '
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(456, 374)
        Me.TabPage4.TabIndex = 0
        Me.TabPage4.Text = "Groups"
        '
        'tabUsers
        '
        Me.tabUsers.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.tabUsers.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.tabUsers.ControlBox.MenuBox.Name = ""
        Me.tabUsers.ControlBox.Name = ""
        Me.tabUsers.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabUsers.ControlBox.MenuBox, Me.tabUsers.ControlBox.CloseBox})
        Me.tabUsers.Controls.Add(Me.SuperTabControlPanel3)
        Me.tabUsers.Controls.Add(Me.SuperTabControlPanel2)
        Me.tabUsers.Controls.Add(Me.SuperTabControlPanel1)
        Me.tabUsers.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabUsers.ForeColor = System.Drawing.Color.Black
        Me.tabUsers.Location = New System.Drawing.Point(0, 0)
        Me.tabUsers.Name = "tabUsers"
        Me.tabUsers.ReorderTabsEnabled = True
        Me.tabUsers.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.tabUsers.SelectedTabIndex = 0
        Me.tabUsers.Size = New System.Drawing.Size(941, 539)
        Me.tabUsers.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.tabUsers.TabIndex = 6
        Me.tabUsers.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabSQLRD, Me.stabNT, Me.SuperTabItem3})
        Me.tabUsers.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.lsvGroups)
        Me.SuperTabControlPanel3.Controls.Add(Me.FlowLayoutPanel6)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(941, 513)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.SuperTabItem3
        '
        'FlowLayoutPanel6
        '
        Me.FlowLayoutPanel6.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel6.Controls.Add(Me.cmdAdd)
        Me.FlowLayoutPanel6.Controls.Add(Me.cmdEdit)
        Me.FlowLayoutPanel6.Controls.Add(Me.cmdDelete)
        Me.FlowLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel6.Location = New System.Drawing.Point(0, 482)
        Me.FlowLayoutPanel6.Name = "FlowLayoutPanel6"
        Me.FlowLayoutPanel6.Size = New System.Drawing.Size(941, 31)
        Me.FlowLayoutPanel6.TabIndex = 0
        '
        'SuperTabItem3
        '
        Me.SuperTabItem3.AttachedControl = Me.SuperTabControlPanel3
        Me.SuperTabItem3.GlobalItem = False
        Me.SuperTabItem3.Name = "SuperTabItem3"
        Me.SuperTabItem3.Text = "Groups"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.tvUsers)
        Me.SuperTabControlPanel1.Controls.Add(Me.Bar1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(941, 513)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.stabSQLRD
        '
        'tvUsers
        '
        Me.tvUsers.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.tvUsers.AllowDrop = True
        Me.tvUsers.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.tvUsers.BackgroundStyle.Class = "TreeBorderKey"
        Me.tvUsers.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.tvUsers.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvUsers.ExpandButtonType = DevComponents.AdvTree.eExpandButtonType.Triangle
        Me.tvUsers.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.tvUsers.Location = New System.Drawing.Point(0, 25)
        Me.tvUsers.Name = "tvUsers"
        Me.tvUsers.Nodes.AddRange(New DevComponents.AdvTree.Node() {Me.Node1})
        Me.tvUsers.NodesConnector = Me.NodeConnector2
        Me.tvUsers.NodeStyle = Me.ElementStyle1
        Me.tvUsers.PathSeparator = ";"
        Me.tvUsers.Size = New System.Drawing.Size(941, 488)
        Me.tvUsers.Styles.Add(Me.ElementStyle1)
        Me.tvUsers.TabIndex = 1
        Me.tvUsers.Text = "AdvTree1"
        '
        'Node1
        '
        Me.Node1.Expanded = True
        Me.Node1.Name = "Node1"
        Me.Node1.Nodes.AddRange(New DevComponents.AdvTree.Node() {Me.Node4})
        Me.Node1.Text = "Node1"
        '
        'Node4
        '
        Me.Node4.Expanded = True
        Me.Node4.Name = "Node4"
        Me.Node4.Text = "Node4"
        '
        'NodeConnector2
        '
        Me.NodeConnector2.LineColor = System.Drawing.Color.Transparent
        '
        'ElementStyle1
        '
        Me.ElementStyle1.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle1.Name = "ElementStyle1"
        Me.ElementStyle1.TextColor = System.Drawing.SystemColors.ControlText
        '
        'Bar1
        '
        Me.Bar1.AntiAlias = True
        Me.Bar1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bar1.DockSide = DevComponents.DotNetBar.eDockSide.Document
        Me.Bar1.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Bar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnAddUser, Me.btnEditUser, Me.btnDeleteUser})
        Me.Bar1.Location = New System.Drawing.Point(0, 0)
        Me.Bar1.Name = "Bar1"
        Me.Bar1.Size = New System.Drawing.Size(941, 25)
        Me.Bar1.Stretch = True
        Me.Bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.Bar1.TabIndex = 7
        Me.Bar1.TabStop = False
        Me.Bar1.Text = "Bar1"
        '
        'btnAddUser
        '
        Me.btnAddUser.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnAddUser.Image = CType(resources.GetObject("btnAddUser.Image"), System.Drawing.Image)
        Me.btnAddUser.Name = "btnAddUser"
        Me.btnAddUser.Text = "Add User"
        '
        'btnEditUser
        '
        Me.btnEditUser.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnEditUser.Image = CType(resources.GetObject("btnEditUser.Image"), System.Drawing.Image)
        Me.btnEditUser.Name = "btnEditUser"
        Me.btnEditUser.Text = "Edit User"
        '
        'btnDeleteUser
        '
        Me.btnDeleteUser.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnDeleteUser.Image = CType(resources.GetObject("btnDeleteUser.Image"), System.Drawing.Image)
        Me.btnDeleteUser.Name = "btnDeleteUser"
        Me.btnDeleteUser.Text = "Delete User"
        '
        'stabSQLRD
        '
        Me.stabSQLRD.AttachedControl = Me.SuperTabControlPanel1
        Me.stabSQLRD.GlobalItem = False
        Me.stabSQLRD.Name = "stabSQLRD"
        Me.stabSQLRD.Text = "SQL-RD Authentication"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.tvNTUsers)
        Me.SuperTabControlPanel2.Controls.Add(Me.Bar2)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(941, 513)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.stabNT
        '
        'tvNTUsers
        '
        Me.tvNTUsers.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.tvNTUsers.AllowDrop = True
        Me.tvNTUsers.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.tvNTUsers.BackgroundStyle.Class = "TreeBorderKey"
        Me.tvNTUsers.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.tvNTUsers.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvNTUsers.ExpandButtonType = DevComponents.AdvTree.eExpandButtonType.Triangle
        Me.tvNTUsers.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.tvNTUsers.Location = New System.Drawing.Point(0, 25)
        Me.tvNTUsers.Name = "tvNTUsers"
        Me.tvNTUsers.Nodes.AddRange(New DevComponents.AdvTree.Node() {Me.Node2})
        Me.tvNTUsers.NodesConnector = Me.NodeConnector1
        Me.tvNTUsers.NodeStyle = Me.ElementStyle2
        Me.tvNTUsers.PathSeparator = ";"
        Me.tvNTUsers.Size = New System.Drawing.Size(941, 488)
        Me.tvNTUsers.Styles.Add(Me.ElementStyle2)
        Me.tvNTUsers.TabIndex = 1
        Me.tvNTUsers.Text = "AdvTree1"
        '
        'Node2
        '
        Me.Node2.Expanded = True
        Me.Node2.Name = "Node2"
        Me.Node2.Nodes.AddRange(New DevComponents.AdvTree.Node() {Me.Node3})
        Me.Node2.Text = "Node2"
        '
        'Node3
        '
        Me.Node3.Expanded = True
        Me.Node3.Name = "Node3"
        Me.Node3.Text = "Node3"
        '
        'NodeConnector1
        '
        Me.NodeConnector1.LineColor = System.Drawing.Color.Transparent
        '
        'ElementStyle2
        '
        Me.ElementStyle2.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle2.Name = "ElementStyle2"
        Me.ElementStyle2.TextColor = System.Drawing.SystemColors.ControlText
        '
        'Bar2
        '
        Me.Bar2.AntiAlias = True
        Me.Bar2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bar2.DockSide = DevComponents.DotNetBar.eDockSide.Document
        Me.Bar2.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Bar2.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnNTAddUser, Me.btnNTEditUser, Me.btnNTDeleteUser})
        Me.Bar2.Location = New System.Drawing.Point(0, 0)
        Me.Bar2.Name = "Bar2"
        Me.Bar2.Size = New System.Drawing.Size(941, 25)
        Me.Bar2.Stretch = True
        Me.Bar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.Bar2.TabIndex = 8
        Me.Bar2.TabStop = False
        Me.Bar2.Text = "Bar2"
        '
        'btnNTAddUser
        '
        Me.btnNTAddUser.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnNTAddUser.Image = CType(resources.GetObject("btnNTAddUser.Image"), System.Drawing.Image)
        Me.btnNTAddUser.Name = "btnNTAddUser"
        Me.btnNTAddUser.Text = "Add User"
        '
        'btnNTEditUser
        '
        Me.btnNTEditUser.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnNTEditUser.Image = CType(resources.GetObject("btnNTEditUser.Image"), System.Drawing.Image)
        Me.btnNTEditUser.Name = "btnNTEditUser"
        Me.btnNTEditUser.Text = "Edit User"
        '
        'btnNTDeleteUser
        '
        Me.btnNTDeleteUser.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnNTDeleteUser.Image = CType(resources.GetObject("btnNTDeleteUser.Image"), System.Drawing.Image)
        Me.btnNTDeleteUser.Name = "btnNTDeleteUser"
        Me.btnNTDeleteUser.Text = "Delete User"
        '
        'stabNT
        '
        Me.stabNT.AttachedControl = Me.SuperTabControlPanel2
        Me.stabNT.GlobalItem = False
        Me.stabNT.Name = "stabNT"
        Me.stabNT.Text = "Windows Authentication"
        Me.stabNT.Visible = False
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel4.Controls.Add(Me.chkNTAuth)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(0, 539)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(941, 32)
        Me.FlowLayoutPanel4.TabIndex = 2
        '
        'ContextMenuBar1
        '
        Me.ContextMenuBar1.AntiAlias = True
        Me.ContextMenuBar1.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.ContextMenuBar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.mnuUsers})
        Me.ContextMenuBar1.Location = New System.Drawing.Point(642, 0)
        Me.ContextMenuBar1.Name = "ContextMenuBar1"
        Me.ContextMenuBar1.Size = New System.Drawing.Size(96, 25)
        Me.ContextMenuBar1.Stretch = True
        Me.ContextMenuBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.ContextMenuBar1.TabIndex = 7
        Me.ContextMenuBar1.TabStop = False
        Me.ContextMenuBar1.Text = "ContextMenuBar1"
        '
        'mnuUsers
        '
        Me.mnuUsers.AutoExpandOnClick = True
        Me.mnuUsers.Name = "mnuUsers"
        Me.mnuUsers.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.mnuEnableWeb, Me.mnuDisableWeb})
        Me.mnuUsers.Text = "ButtonItem1"
        '
        'mnuEnableWeb
        '
        Me.mnuEnableWeb.Image = CType(resources.GetObject("mnuEnableWeb.Image"), System.Drawing.Image)
        Me.mnuEnableWeb.Name = "mnuEnableWeb"
        Me.mnuEnableWeb.Text = "Enable Web Access"
        '
        'mnuDisableWeb
        '
        Me.mnuDisableWeb.Image = CType(resources.GetObject("mnuDisableWeb.Image"), System.Drawing.Image)
        Me.mnuDisableWeb.Name = "mnuDisableWeb"
        Me.mnuDisableWeb.Text = "Disable Web Access"
        '
        'frmUserManager
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(941, 571)
        Me.Controls.Add(Me.ContextMenuBar1)
        Me.Controls.Add(Me.tabUsers)
        Me.Controls.Add(Me.FlowLayoutPanel4)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmUserManager"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "User Manager"
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tabUsers, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabUsers.ResumeLayout(False)
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel6.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        CType(Me.tvUsers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControlPanel2.ResumeLayout(False)
        CType(Me.tvNTUsers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Bar2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel4.ResumeLayout(False)
        CType(Me.ContextMenuBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub _LoadGroups()
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oItem As ListViewItem

        SQL = "SELECT * FROM GroupAttr"

        oRs = clsMarsData.GetData(SQL)

        lsvGroups.Items.Clear()

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                oItem = New ListViewItem

                For Each o As ListViewItem In lsvGroups.Items
                    If o.Text = oRs("groupname").Value Then
                        GoTo skip
                    End If
                Next

                oItem.Text = oRs("groupname").Value
                oItem.SubItems.Add(oRs("groupdesc").Value)
                oItem.Tag = oRs("groupid").Value
                oItem.ImageIndex = 0

                lsvGroups.Items.Add(oItem)

skip:
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If


    End Sub
    Private Sub frmUserManager_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        Dim oUser As New clsMarsUsers

        Me.ListUsers()

        Me.ListNTUsers()

        Me._LoadGroups()



        Dim oUI As New clsMarsUI

        chkNTAuth.Checked = oUI.ReadRegistry("UnifiedLogin", 0)

        If chkNTAuth.Checked Then
            ContextMenuBar1.SetContextMenuEx(tvNTUsers, mnuUsers)
        Else
            ContextMenuBar1.SetContextMenuEx(tvUsers, mnuUsers)
        End If
    End Sub

    'Private Sub tvUsers_AfterSelect(ByVal sender As Object, ByVal e As DevComponents.AdvTree.AdvTreeNodeEventArgs) Handles tvUsers.AfterNodeSelect

    '    If tvUsers.SelectedNode Is Nothing Then Return

    '    Dim oNode As Node
    '    Dim oData As New clsMarsData
    '    Dim oRs As ADODB.Recordset
    '    Dim SQL As String

    '    oNode = tvUsers.SelectedNode

    '    sPrev = oNode.Text

    '    Dim type As String = CType(oNode.Tag, String).Split(":")(0).ToLower

    '    Select Case type
    '        Case "userid"
    '            SQL = "SELECT * FROM CRDUsers WHERE UserID ='" & oNode.Text & "'"
    '            oRs = clsMarsData.GetData(SQL)

    '            If Not oRs Is Nothing Then
    '                If oRs.EOF = False Then
    '                    txtFirstName.Text = oRs("firstname").Value
    '                    txtLastName.Text = oRs("lastname").Value
    '                    txtUserID.Text = oRs("userid").Value
    '                    txtPassword.Text = _DecryptDBValue(IsNull(oRs("password").Value))

    '                    cmbRole.Text = oRs("userrole").Value

    '                    lblLastMod.Text = oRs("lastmodby").Value & " " & _
    '                    Convert.ToString(oRs("lastmoddate").Value)

    '                    Try
    '                        chkAutoLogin.Checked = Convert.ToBoolean(oRs("autologin").Value)
    '                    Catch
    '                        chkAutoLogin.Checked = False
    '                    End Try

    '                    Try
    '                        chkLoki.Checked = Convert.ToBoolean(oRs("lokienabled").Value)
    '                    Catch ex As Exception
    '                        chkLoki.Checked = False
    '                    End Try
    '                End If

    '                oRs.Close()
    '            End If

    '            If oNode.Text.ToLower = gUser.ToLower Then
    '                For Each txt As Control In GroupBox2.Controls
    '                    If TypeOf txt Is TextBox Then
    '                        Dim oT As TextBox

    '                        oT = CType(txt, TextBox)

    '                        oT.ReadOnly = True
    '                        cmdRemove.Enabled = False
    '                        cmbRole.Enabled = False
    '                        If cmbRole.Text <> "Administrator" Then cmdPermit.Enabled = False
    '                    End If
    '                Next

    '                txtPassword.ReadOnly = False
    '            Else
    '                For Each txt As Control In GroupBox2.Controls
    '                    If TypeOf txt Is TextBox Then
    '                        Dim oT As TextBox

    '                        oT = CType(txt, TextBox)

    '                        oT.ReadOnly = False
    '                        cmdRemove.Enabled = True
    '                        cmbRole.Enabled = True
    '                        If cmbRole.Text <> "Administrator" Then cmdPermit.Enabled = True
    '                    End If
    '                Next
    '            End If
    '    End Select

    '    cmdApply.Enabled = False
    'End Sub


    Private Sub cmdAddUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim oData As New clsMarsData

        SQL = "SELECT COUNT(*) FROM CRDUsers WHERE UserID <> 'SQLRDAdmin'"

        oRs = clsMarsData.GetData(SQL)

        Try
            If oRs.Fields(0).Value >= 1 Then

                If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.sa4_SecureUserLogon) = False Then
                    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, btnAddUser, "Secure User Logon")
                    Return
                End If

            End If
        Catch : End Try


    End Sub



    Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim oRes As DialogResult

        If tvUsers.SelectedNode Is Nothing Then Return

        If tvUsers.SelectedNode Is Nothing Or tvUsers.SelectedNode.Tag = "Role" Or _
        tvUsers.SelectedNode.Tag = "Parent" Then Return

        If gUser.ToLower = tvUsers.SelectedNode.Text.ToLower Then Return

        oRes = MessageBox.Show("Delete the user '" & tvUsers.SelectedNode.Text & "'?", _
        Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If oRes = DialogResult.Yes Then
            Dim oUser As New clsMarsUsers

            oUser.DeleteUser(tvUsers.SelectedNode.Text)

            _Delay(1)

            Me.ListUsers()
        End If

    End Sub


    Private Sub cmdPermit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If tvUsers.SelectedNode Is Nothing Then Return

        Dim oNode As Node = tvUsers.SelectedNode

        If oNode.Parent Is Nothing Then Return

        If oNode.Parent.Text.ToLower = "administrators" Then Return

        Dim oForm As New frmUserSchedules

        oForm.AssignSchedules(oNode.Text)
    End Sub

    Private Sub txtUserID_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If (Char.IsLetterOrDigit(e.KeyChar) = False) And e.KeyChar <> Chr(8) Then
            e.Handled = True
        End If
    End Sub



    Private Sub chkNTAuth_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNTAuth.CheckedChanged
        If chkNTAuth.Checked = True And IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.sa4_SecureUserLogon) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, chkNTAuth, "Secure User Logon")
            chkNTAuth.Checked = False
            Return
        End If

        Dim oUI As New clsMarsUI

        oUI.SaveRegistry("UnifiedLogin", Convert.ToInt32(chkNTAuth.Checked))

        Dim oRs As ADODB.Recordset
        Dim nCount As Integer

        oRs = clsMarsData.GetData("SELECT COUNT(*) FROM DomainAttr")

        If Not oRs Is Nothing Then
            nCount = oRs(0).Value

            oRs.Close()
        End If

        If chkNTAuth.Checked = True And nCount = 0 Then
            Dim SQL As String
            Dim sCols As String
            Dim sVals As String
            Dim nID As Int64
            Dim sUser As String

            sCols = "DomainID,DomainName,UserRole,UserNumber"

            nID = clsMarsData.CreateDataID("domainattr", "domainid")

            sUser = Environment.UserDomainName & "\" & Environment.UserName

            sVals = nID & "," & _
            "'" & SQLPrepare(sUser) & "','Administrator'," & clsMarsData.GetNewUserNumber("DomainAttr")

            SQL = "INSERT INTO DomainAttr (" & sCols & ") VALUES (" & sVals & ")"

            If clsMarsData.WriteData(SQL) = True Then
                Dim oUsers As New clsMarsUsers

                Me.ListNTUsers()
            End If
        End If

        stabSQLRD.Visible = Not (chkNTAuth.Checked)
        stabNT.Visible = chkNTAuth.Checked

        If chkNTAuth.Checked Then
            ContextMenuBar1.SetContextMenuEx(tvNTUsers, mnuUsers)
            ContextMenuBar1.SetContextMenuEx(tvUsers, Nothing)

            tabUsers.SelectedTab = stabNT
        Else
            ContextMenuBar1.SetContextMenuEx(tvUsers, mnuUsers)
            ContextMenuBar1.SetContextMenuEx(tvNTUsers, Nothing)
            tabUsers.SelectedTab = stabSQLRD
        End If
    End Sub

    'Private Sub cmdNTApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim sUser As String = cmbNTDomain.Text & "\" & cmbNTUser.Text
    '    Dim oEdit As Boolean

    '    If cmbNTDomain.Text.Length = 0 Then
    '        SetError(cmbNTDomain, "Please provide the user domain")
    '        cmbNTDomain.Focus()
    '        Return
    '    ElseIf cmbNTUser.Text.Length = 0 Then
    '        SetError(cmbNTUser, "Please provide the Windows User name")
    '        cmbNTDomain.Focus()
    '        Return
    '    ElseIf cmbNTRole.Text.Length = 0 Then
    '        SetError(cmbNTRole, "Please select the security role")
    '        cmbNTRole.Focus()
    '        Return
    '    ElseIf clsMarsData.IsDuplicate("DomainAttr", "DomainName", sUser, False) = True Then
    '        oEdit = True
    '    End If

    '    Dim SQL As String
    '    Dim sCols As String
    '    Dim sVals As String

    '    If oEdit = False Then
    '        sCols = "DomainID,DomainName,UserRole,LastModDate,LastModBy,lokiEnabled,UserNumber"

    '        sVals = clsMarsData.CreateDataID("domainattr", "domainid") & "," & _
    '        "'" & sUser & "'," & _
    '        "'" & SQLPrepare(cmbNTRole.Text) & "'," & _
    '        "'" & ConDateTime(Date.Now) & "'," & _
    '        "'" & gUser & "'," & _
    '        Convert.ToInt32(chkNTLoki.Checked) & "," & _
    '        clsMarsData.GetNewUserNumber("DomainAttr")

    '        SQL = "INSERT INTO DomainAttr (" & sCols & ") VALUES (" & sVals & ")"
    '    Else
    '        SQL = "UPDATE DomainAttr SET " & _
    '        "UserRole ='" & SQLPrepare(cmbNTRole.Text) & "'," & _
    '        "LastModDate='" & ConDateTime(Date.Now) & "'," & _
    '        "LastModBy ='" & gUser & "', " & _
    '        "LokiEnabled = " & Convert.ToInt32(chkNTLoki.Checked) & " " & _
    '        "WHERE DomainName ='" & SQLPrepare(sUser) & "'"
    '    End If

    '    If clsMarsData.WriteData(SQL) = True Then
    '        If chkAddEntireGroup.Checked And cmbNTGroup.Text <> "" Then
    '            If Not clsMarsData.IsDuplicate("groupattr", "groupname", cmbNTGroup.Text, False) Then
    '                Dim nGroupID As Integer = clsMarsData.CreateDataID("groupattr", "groupid")

    '                sCols = "GroupID,GroupName,GroupDesc"

    '                sVals = nGroupID & "," & _
    '                "'" & SQLPrepare(cmbNTGroup.Text) & "'," & _
    '                "''"

    '                SQL = "INSERT INTO GroupAttr (" & sCols & ") VALUES (" & sVals & ")"

    '                clsMarsData.WriteData(SQL)

    '                _LoadGroups()
    '            End If
    '        End If
    '        Dim oUser As New clsMarsUsers

    '        Me.ListNTUsers()

    '        cmdNTApply.Enabled = False
    '        cmdNTAbandon.Enabled = False
    '        tvNTUsers.Enabled = True
    '    End If
    'End Sub



    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.sa4_SecureUserLogon) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, cmdAdd, "Secure User Logon")
            Return
        End If

        Dim oGroup As frmGroupAttr = New frmGroupAttr

        oGroup.AddGroup()

        Me._LoadGroups()
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim SQL As String


        If MessageBox.Show("Delete the selected group(s)? This cannot be undone.", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            For Each oItem As ListViewItem In lsvGroups.SelectedItems
                Dim grp As usergroup = New usergroup(CType(oItem.Tag, Integer))

                If grp.memberCount > 0 Then
                    MessageBox.Show("Cannot delete the '" & oItem.Text & "' group as it has members. Remove all members from the group and try again", _
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Else
                    clsMarsData.WriteData("DELETE FROM GroupAttr WHERE GroupID =" & oItem.Tag)
                    clsMarsData.WriteData("DELETE FROM GroupPermissions WHERE GroupID =" & oItem.Tag)

                    oItem.Remove()
                End If
            Next
        End If

    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        Dim oGroup As New frmGroupAttr

        If lsvGroups.SelectedItems.Count = 0 Then Return

        Dim oItem As ListViewItem = lsvGroups.SelectedItems(0)

        oGroup.EditGroup(oItem.Tag)

        Me._LoadGroups()

        Dim oUser As New clsMarsUsers

        Me.ListUsers()

    End Sub

    Private Sub cmbRole_DropDown(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oCombo As ComboBox

        oCombo = CType(sender, ComboBox)

        oCombo.Items.Clear()

        SQL = "SELECT GroupName FROM GroupAttr"

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                oCombo.Items.Add(oRs(0).Value)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        Dim s As String() = New String() {"Administrator", "User"}

        oCombo.Items.AddRange(s)
    End Sub



    Private Sub cmdNTUserSchedules_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If tvNTUsers.SelectedNode Is Nothing Then Return

        Dim oNode As Node = tvNTUsers.SelectedNode

        If oNode.Parent Is Nothing Then Return

        If oNode.Parent.Text.ToLower = "administrators" Then Return

        Dim oForm As New frmUserSchedules

        oForm.AssignSchedules(oNode.Text)
    End Sub

    Private Sub lsvGroups_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvGroups.DoubleClick
        cmdEdit_Click(sender, e)
    End Sub


    Public Sub ListUsers()
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oRs1 As ADODB.Recordset
        'Dim oParent As Node

        SQL = "SELECT DISTINCT UserRole FROM CRDUsers WHERE UserID <> 'SQLRDAdmin'"

        oRs = clsMarsData.GetData(SQL)

        tvUsers.Nodes.Clear()

        Dim parentImage, roleImage, userImage As Image

        parentImage = resizeImage(Image.FromFile(getAssetLocation("root.png")), 16, 16)
        roleImage = resizeImage(Image.FromFile(getAssetLocation("group.png")), 16, 16)
        userImage = resizeImage(Image.FromFile(getAssetLocation("user.png")), 16, 16)

        ' oParent = New Node("SQL-RD Users")
        ' oParent.Tag = "Root"
        ' oParent.Image = parentImage

        'tvUsers.Nodes.Add(oParent)
        Dim columns() As String = New String() {"User ID", "Full Name", "Web Access", "Last Modified", "Last Modified By"}

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                Dim oNode As New Node(oRs("userrole").Value)

                oNode.Text = oRs("userrole").Value
                oNode.Image = roleImage
                oNode.Tag = "Role"

                tvUsers.Nodes.Add(oNode)

                '//add the columns
                For Each col As String In columns
                    Dim cHeader As ColumnHeader = New ColumnHeader(col)
                    cHeader.MinimumWidth = 64

                    cHeader.Width.Absolute = 150

                    oNode.NodesColumns.Add(cHeader)
                Next

                SQL = "SELECT * FROM CRDUsers WHERE UserRole ='" & oNode.Text & "' AND UserID <> 'SQLRDAdmin'"
                oRs1 = clsMarsData.GetData(SQL)

                If Not oRs1 Is Nothing Then
                    Do While oRs1.EOF = False
                        Dim oChild As New Node(oRs1("userid").Value)

                        oChild.Image = userImage
                        oChild.Tag = "UserID:" & oRs1("userid").Value
                        oChild.DataKey = oRs1("userid").Value
                        oNode.Nodes.Add(oChild)

                        '//add the other fields
                        For Each column As ColumnHeader In oNode.NodesColumns
                            Select Case column.Text
                                Case "User ID"
                                    'oChild.Cells.Add(New Cell(oRs1("userid").Value))
                                Case "Full Name"
                                    Dim fullname As String = oRs1("firstname").Value & " " & oRs1("lastname").Value
                                    oChild.Cells.Add(New Cell(fullname))
                                Case "Web Access"
                                    Dim lokienabled As Boolean = Convert.ToInt16(IsNull(oRs1("lokienabled").Value, 0))
                                    oChild.Cells.Add(New Cell(lokienabled))
                                Case "Last Modified"
                                    oChild.Cells.Add(New Cell(oRs1("lastmoddate").Value))
                                Case "Last Modified By"
                                    oChild.Cells.Add(New Cell(oRs1("lastmodby").Value))
                            End Select
                        Next

                        oRs1.MoveNext()
                    Loop

                    oRs1.Close()
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        For Each nn As DevComponents.AdvTree.Node In tvUsers.Nodes
            nn.Expanded = True
        Next

    End Sub

    Public Sub ListNTUsers()
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oRs1 As ADODB.Recordset
        Dim oParent As DevComponents.AdvTree.Node


        Dim parentImage, roleImage, userImage As Image

        parentImage = resizeImage(Image.FromFile(getAssetLocation("root.png")), 16, 16)
        roleImage = resizeImage(Image.FromFile(getAssetLocation("group.png")), 16, 16)
        userImage = resizeImage(Image.FromFile(getAssetLocation("user.png")), 16, 16)

        SQL = "SELECT DISTINCT UserRole FROM DomainAttr"

        oRs = clsMarsData.GetData(SQL)

        tvNTUsers.Nodes.Clear()

        Dim columns() As String = New String() {"User ID", "Web Access", "Last Modified", "Last Modified By"}

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                Dim oNode As New Node(oRs("userrole").Value)
                oNode.Image = roleImage
                oNode.Tag = "Role"


                tvNTUsers.Nodes.Add(oNode)

                '//add the columns
                For Each col As String In columns
                    Dim cHeader As ColumnHeader = New ColumnHeader(col)
                    cHeader.MinimumWidth = 64

                    cHeader.Width.Absolute = 150

                    oNode.NodesColumns.Add(cHeader)
                Next

                SQL = "SELECT * FROM DomainAttr WHERE UserRole ='" & oNode.Text & "'"

                oRs1 = clsMarsData.GetData(SQL)

                If Not oRs1 Is Nothing Then
                    Do While oRs1.EOF = False
                        Dim oChild As New Node(oRs1("domainname").Value)
                        oChild.Image = userImage
                        oChild.DataKey = oRs1("domainname").Value
                        oChild.Tag = "UserID"
                        oNode.Nodes.Add(oChild)

                        '//add the other fields
                        For Each column As ColumnHeader In oNode.NodesColumns
                            Select Case column.Text
                                Case "Web Access"
                                    Dim lokienabled As Boolean = Convert.ToInt16(IsNull(oRs1("lokienabled").Value, 0))
                                    oChild.Cells.Add(New Cell(lokienabled))
                                Case "Last Modified"
                                    oChild.Cells.Add(New Cell(IsNull(oRs1("lastmoddate").Value, Date.Now)))
                                Case "Last Modified By"
                                    oChild.Cells.Add(New Cell(IsNull(oRs1("lastmodby").Value, "SQLRDAdmin")))
                            End Select
                        Next

                        oRs1.MoveNext()
                    Loop

                    oRs1.Close()
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        For Each nn As DevComponents.AdvTree.Node In tvNTUsers.Nodes
            nn.Expanded = True
        Next

    End Sub


    Private Sub SuperTabControl1_SelectedTabChanged(sender As System.Object, e As DevComponents.DotNetBar.SuperTabStripSelectedTabChangedEventArgs) Handles tabUsers.SelectedTabChanged
        Select Case tabUsers.SelectedTab.Text
            Case "SQL-RD Authentication"
                'cmdNTAbandon.Visible = False
                'cmdNTApply.Visible = False

                'cmdAbandon.Visible = True
                'cmdApply.Visible = True
            Case Else
                'cmdNTAbandon.Visible = True
                'cmdNTApply.Visible = True

                'cmdAbandon.Visible = False
                'cmdApply.Visible = False
                ListNTUsers()
        End Select
    End Sub


    Private Sub btnAddUser_Click(sender As System.Object, e As System.EventArgs) Handles btnAddUser.Click, btnNTAddUser.Click
        Dim adder As frmAddUser = New frmAddUser
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim oData As New clsMarsData

        If chkNTAuth.Checked Then
            adder.userMode = frmAddUser.e_mode.NT

            adder.addNTUser()

            ListNTUsers()
        Else
            SQL = "SELECT COUNT(*) FROM CRDUsers WHERE UserID <> 'SQLRDAdmin'"

            oRs = clsMarsData.GetData(SQL)

            Try
                If oRs.Fields(0).Value >= 1 Then

                    If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.sa4_SecureUserLogon) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, btnAddUser, "Secure User Logon")
                        Return
                    End If
                End If
            Catch : End Try

            adder.userMode = frmAddUser.e_mode.NORMAL
            adder.addUser()

            ListUsers()
        End If

    End Sub

    Private Sub btnEditUser_Click(sender As System.Object, e As System.EventArgs) Handles btnEditUser.Click, btnNTEditUser.Click
        Dim adder As frmAddUser = New frmAddUser

        If chkNTAuth.Checked Then

            If tvNTUsers.SelectedNode Is Nothing Then
                Return
            ElseIf tvNTUsers.SelectedNode.Parent Is Nothing Then
                Return
            Else
                adder.userMode = frmAddUser.e_mode.NT

                adder.editNTUser(tvNTUsers.SelectedNode.DataKey)
            End If

            ListNTUsers()

        Else
            If tvUsers.SelectedNode Is Nothing Then
                Return
            ElseIf tvUsers.SelectedNode.Parent Is Nothing Then
                Return
            Else
                adder.userMode = frmAddUser.e_mode.NORMAL

                adder.editUser(tvUsers.SelectedNode.DataKey)
            End If

            ListUsers()
        End If


    End Sub

    Private Sub btnDeleteUser_Click(sender As System.Object, e As System.EventArgs) Handles btnDeleteUser.Click, btnNTDeleteUser.Click
        Dim oRes As DialogResult

        If chkNTAuth.Checked = False Then
            If tvUsers.SelectedNode Is Nothing Then Return

            If tvUsers.SelectedNode.Parent Is Nothing Then Return

            If gUser.ToLower = tvUsers.SelectedNode.Text.ToLower Then Return

            oRes = MessageBox.Show("Delete the user '" & tvUsers.SelectedNode.Text & "'?", _
            Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If oRes = DialogResult.Yes Then
                Dim oUser As New clsMarsUsers

                oUser.DeleteUser(tvUsers.SelectedNode.Text)

                _Delay(1)

                tvUsers.SelectedNode.Remove()
            End If
        Else

            If tvNTUsers.SelectedNode Is Nothing Then Return

            If tvNTUsers.SelectedNode Is Nothing Then Return

            oRes = MessageBox.Show("Remove the user '" & tvNTUsers.SelectedNode.Text & "'?", _
            Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If oRes = DialogResult.Yes Then
                Dim oUser As New clsMarsUsers

                oUser.DeleteNTUser(tvNTUsers.SelectedNode.Text)

                _Delay(1)

                tvNTUsers.SelectedNode.Remove()
            End If
        End If
    End Sub

    Private Sub tvUsers_Click(sender As System.Object, e As System.EventArgs) Handles tvUsers.DoubleClick, tvNTUsers.DoubleClick

        btnEditUser_Click(Nothing, Nothing)

    End Sub


    Private Sub btnNTDelete_Click(sender As System.Object, e As System.EventArgs) Handles btnNTDeleteUser.Click
        Dim users As clsMarsUsers = New clsMarsUsers
        Dim res As DialogResult = MessageBox.Show("Delete the selected user(s)?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If res = Windows.Forms.DialogResult.No Then Return

        If chkNTAuth.Checked Then
            For Each nn As Node In tvNTUsers.SelectedNodes
                users.DeleteNTUser(nn.DataKey)
            Next

            ListNTUsers()
        Else
            For Each nn As Node In tvUsers.SelectedNodes
                users.DeleteUser(nn.DataKey)
            Next

            ListUsers()
        End If
    End Sub

    

    Private Sub mnuUsers_PopupOpen(sender As Object, e As DevComponents.DotNetBar.PopupOpenEventArgs) Handles mnuUsers.PopupOpen
        If chkNTAuth.Checked Then
            If tvNTUsers.SelectedNode Is Nothing Then Return

            If tvNTUsers.SelectedNode.Parent Is Nothing Then
                mnuEnableWeb.Enabled = True
                mnuDisableWeb.Enabled = True
                Return
            End If

            If tvNTUsers.SelectedNodes.Count = 1 Then
                Dim user As cssusers.NTDomainUser = New cssusers.NTDomainUser(tvNTUsers.SelectedNode.DataKey)

                If user.webEnabled Then
                    mnuEnableWeb.Enabled = False
                    mnuDisableWeb.Enabled = True
                Else
                    mnuEnableWeb.Enabled = True
                    mnuDisableWeb.Enabled = False
                End If
            Else
                mnuEnableWeb.Enabled = True
                mnuDisableWeb.Enabled = True
            End If
        Else

            If tvUsers.SelectedNode Is Nothing Then Return

            If tvUsers.SelectedNode.Parent Is Nothing Then
                mnuEnableWeb.Enabled = True
                mnuDisableWeb.Enabled = True
                Return
            End If

            If tvUsers.SelectedNodes.Count = 1 Then
                Dim user As cssusers.normalUser = New cssusers.normalUser(tvUsers.SelectedNode.DataKey)

                If user.webEnabled Then
                    mnuEnableWeb.Enabled = False
                    mnuDisableWeb.Enabled = True
                Else
                    mnuEnableWeb.Enabled = True
                    mnuDisableWeb.Enabled = False
                End If
            Else
                mnuEnableWeb.Enabled = True
                mnuDisableWeb.Enabled = True
            End If
        End If
    End Sub

    Private Sub mnuEnableWeb_Click(sender As System.Object, e As System.EventArgs) Handles mnuEnableWeb.Click
        Dim message As String = ""

        If chkNTAuth.Checked Then
            If clsMarsUsers.canEnableUserForLoki(message, tvNTUsers) = False Then
                MessageBox.Show(message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
        Else
            If clsMarsUsers.canEnableUserForLoki(message, tvUsers) = False Then
                MessageBox.Show(message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
        End If


        If chkNTAuth.Checked Then
            If tvNTUsers.SelectedNode Is Nothing Then Return

            Dim nodes As NodeCollection

            If tvNTUsers.SelectedNode.Parent Is Nothing Then '//its the parent node
                nodes = tvNTUsers.SelectedNode.Nodes
            Else
                nodes = tvNTUsers.SelectedNodes
            End If

            For Each nn As Node In nodes
                If clsMarsUsers.canEnableUserForLoki(message, tvNTUsers) = False Then
                    MessageBox.Show(message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit For
                Else
                    clsMarsData.WriteData("UPDATE domainattr SET lokienabled = 1, lastModDate ='" & ConDateTime(Now) & "', lastModBy ='" & gUser & "' WHERE domainname ='" & SQLPrepare(nn.DataKey) & "'")

                    nn.Cells(1).Text = "True"
                    nn.Cells(2).Text = Date.Now
                    nn.Cells(3).Text = gUser

                End If
            Next
        Else
            If tvUsers.SelectedNode Is Nothing Then Return

            Dim nodes As NodeCollection

            If tvUsers.SelectedNode.Parent Is Nothing Then '//its the parent node
                nodes = tvUsers.SelectedNode.Nodes
            Else
                nodes = tvUsers.SelectedNodes
            End If

            For Each nn As Node In nodes
                If clsMarsUsers.canEnableUserForLoki(message, tvUsers) = False Then
                    MessageBox.Show(message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit For
                Else
                    clsMarsData.WriteData("UPDATE crdusers SET lokienabled = 1, lastModDate ='" & ConDateTime(Now) & "', lastModBy ='" & gUser & "' WHERE userid ='" & SQLPrepare(nn.DataKey) & "'")

                    nn.Cells(2).Text = "True"
                    nn.Cells(3).Text = Date.Now
                    nn.Cells(4).Text = gUser

                End If
            Next
        End If
    End Sub

    Private Sub mnuDisableWeb_Click(sender As System.Object, e As System.EventArgs) Handles mnuDisableWeb.Click
        Dim message As String = ""


        If chkNTAuth.Checked Then
            If tvNTUsers.SelectedNode Is Nothing Then Return

            Dim nodes As NodeCollection

            If tvNTUsers.SelectedNode.Parent Is Nothing Then '//its the parent node
                nodes = tvNTUsers.SelectedNode.Nodes
            Else
                nodes = tvNTUsers.SelectedNodes
            End If

            For Each nn As Node In nodes
                clsMarsData.WriteData("UPDATE domainattr SET lokienabled = 0, lastModDate ='" & ConDateTime(Now) & "', lastModBy ='" & gUser & "' WHERE domainname ='" & SQLPrepare(nn.DataKey) & "'")

                nn.Cells(1).Text = "False"
                nn.Cells(2).Text = Date.Now
                nn.Cells(3).Text = gUser
            Next
        Else
            If tvUsers.SelectedNode Is Nothing Then Return

            Dim nodes As NodeCollection

            If tvUsers.SelectedNode.Parent Is Nothing Then '//its the parent node
                nodes = tvUsers.SelectedNode.Nodes
            Else
                nodes = tvUsers.SelectedNodes
            End If

            For Each nn As Node In nodes
                clsMarsData.WriteData("UPDATE crdusers SET lokienabled = 0, lastModDate ='" & ConDateTime(Now) & "', lastModBy ='" & gUser & "' WHERE userid ='" & SQLPrepare(nn.DataKey) & "'")

                nn.Cells(2).Text = "False"
                nn.Cells(3).Text = Date.Now
                nn.Cells(4).Text = gUser
            Next
        End If
    End Sub
End Class
