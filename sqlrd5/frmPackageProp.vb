#If CRYSTAL_VER = 8 Then
imports My.Crystal85
#ElseIf CRYSTAL_VER = 9 Then
imports My.Crystal9 
#ElseIf CRYSTAL_VER = 10 Then
imports My.Crystal10 
#ElseIf CRYSTAL_VER = 11 Then
Imports My.Crystal11
Imports System.Windows.Forms
#End If

Friend Class frmPackageProp
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim oUI As clsMarsUI = New clsMarsUI
    Dim oData As clsMarsData = New clsMarsData
    Dim oMsg As clsMarsMessaging = New clsMarsMessaging
    Dim ScheduleID As Integer
    Dim m_ParametersRead As Boolean = False
    Dim IsDynamic As Boolean
    Dim owner As String = ""
#Region "Controls"
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents cmdView As DevComponents.DotNetBar.ButtonX
    Friend WithEvents chkSnapshots As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents cmdExecute As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdDelete As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtSnapshots As System.Windows.Forms.NumericUpDown
    Friend WithEvents lsvSnapshots As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ExpandableSplitter1 As DevComponents.DotNetBar.ExpandableSplitter
    Friend WithEvents tvPacks As System.Windows.Forms.TreeView
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtAdjustStamp As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label26 As DevComponents.DotNetBar.LabelX
    Friend WithEvents grpDynamicTasks As System.Windows.Forms.GroupBox
    Friend WithEvents optOnce As System.Windows.Forms.RadioButton
    Friend WithEvents optAll As System.Windows.Forms.RadioButton
    Friend WithEvents btnTest As DevComponents.DotNetBar.ButtonX
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
#End Region
    Dim OkayToClose As Boolean
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents grpSelectReport As System.Windows.Forms.GroupBox
    Friend WithEvents chkSelectAll As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents UcError As sqlrd.ucErrorHandler
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents grpAutoResume As System.Windows.Forms.GroupBox
    Friend WithEvents txtCacheExpiry As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents chkAutoResume As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Step3 As System.Windows.Forms.Panel
    Friend WithEvents chkGroupReports As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents btnConnect As DevComponents.DotNetBar.ButtonX
    Friend WithEvents grpQuery As System.Windows.Forms.GroupBox
    Friend WithEvents cmbDDKey As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents btnBuild As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtQuery As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents UcDSNDD As sqlrd.ucDSN
    Dim IsLoaded As Boolean = False
    Friend WithEvents btnClear As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtMergeAllXL As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtMergeAllPDF As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents chkMergeAllXL As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkMergeAllPDF As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkMultithreaded As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkMergeText As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Label13 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtMergeTextName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents tabProperties As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel6 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents UcHistory As sqlrd.ucScheduleHistory
    Friend WithEvents tabHistory As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel5 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents UcBlank As sqlrd.ucBlankReportX
    Friend WithEvents tabExceptionHandling As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabDestinations As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents tabReports As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents UcSched As sqlrd.ucSchedule
    Friend WithEvents tabSchedule As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabGeneral As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel10 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabDataDriver As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel9 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabSnapshots As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel8 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabLinking As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel7 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabTasks As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel6 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel5 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel4 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel7 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnXLSMerge As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnPDFMerge As DevComponents.DotNetBar.ButtonX

    Dim IsDataDriven As Boolean = False
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdLoc As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtFolder As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdAddReport As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lsvReports As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ReportName As System.Windows.Forms.ColumnHeader
    Friend WithEvents Format As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdRemoveReport As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdApply As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents oTask As sqlrd.ucTasks
    Friend WithEvents UcDest As sqlrd.ucDestination
    Friend WithEvents txtDesc As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKeyWord As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtMergePDF As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents chkMergeXL As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkMergePDF As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtMergeXL As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmbDateTime As System.Windows.Forms.ComboBox
    Friend WithEvents chkDTStamp As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents cmdUp As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdDown As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnView As DevComponents.DotNetBar.ButtonX
    Friend WithEvents chkStaticDest As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents cmdValidate As DevComponents.DotNetBar.ButtonX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label21 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbValue As System.Windows.Forms.ComboBox
    Friend WithEvents cmbColumn As System.Windows.Forms.ComboBox
    Friend WithEvents cmbTable As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents optStatic As System.Windows.Forms.RadioButton
    Friend WithEvents cmbKey As System.Windows.Forms.ComboBox
    Friend WithEvents Label23 As DevComponents.DotNetBar.LabelX
    Friend WithEvents optDynamic As System.Windows.Forms.RadioButton
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cmdEdit As DevComponents.DotNetBar.ButtonX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPackageProp))
        Me.txtDesc = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.Label7 = New DevComponents.DotNetBar.LabelX()
        Me.txtKeyWord = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdLoc = New DevComponents.DotNetBar.ButtonX()
        Me.txtFolder = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.cmdUp = New DevComponents.DotNetBar.ButtonX()
        Me.cmdDown = New DevComponents.DotNetBar.ButtonX()
        Me.cmbDateTime = New System.Windows.Forms.ComboBox()
        Me.chkDTStamp = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.cmdAddReport = New DevComponents.DotNetBar.ButtonX()
        Me.lsvReports = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ReportName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Format = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmdRemoveReport = New DevComponents.DotNetBar.ButtonX()
        Me.cmdEdit = New DevComponents.DotNetBar.ButtonX()
        Me.cmdApply = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.txtMergePDF = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.chkMergeXL = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkMergePDF = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtMergeXL = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.ofd = New System.Windows.Forms.OpenFileDialog()
        Me.btnView = New DevComponents.DotNetBar.ButtonX()
        Me.chkStaticDest = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.UcDSN = New sqlrd.ucDSN()
        Me.cmdValidate = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnClear = New DevComponents.DotNetBar.ButtonX()
        Me.btnTest = New DevComponents.DotNetBar.ButtonX()
        Me.Label21 = New DevComponents.DotNetBar.LabelX()
        Me.cmbValue = New System.Windows.Forms.ComboBox()
        Me.cmbColumn = New System.Windows.Forms.ComboBox()
        Me.cmbTable = New System.Windows.Forms.ComboBox()
        Me.Label8 = New DevComponents.DotNetBar.LabelX()
        Me.optStatic = New System.Windows.Forms.RadioButton()
        Me.cmbKey = New System.Windows.Forms.ComboBox()
        Me.Label23 = New DevComponents.DotNetBar.LabelX()
        Me.optDynamic = New System.Windows.Forms.RadioButton()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkAutoResume = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkMergeAllXL = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkMergeAllPDF = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkMergeText = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Label13 = New DevComponents.DotNetBar.LabelX()
        Me.txtMergeTextName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.chkMultithreaded = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.grpSelectReport = New System.Windows.Forms.GroupBox()
        Me.chkSelectAll = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ExpandableSplitter1 = New DevComponents.DotNetBar.ExpandableSplitter()
        Me.tvPacks = New System.Windows.Forms.TreeView()
        Me.lsvSnapshots = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmdView = New DevComponents.DotNetBar.ButtonX()
        Me.chkSnapshots = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.cmdExecute = New DevComponents.DotNetBar.ButtonX()
        Me.Label11 = New DevComponents.DotNetBar.LabelX()
        Me.cmdDelete = New DevComponents.DotNetBar.ButtonX()
        Me.txtSnapshots = New System.Windows.Forms.NumericUpDown()
        Me.Step3 = New System.Windows.Forms.Panel()
        Me.txtMergeAllXL = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtMergeAllPDF = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.UcDSNDD = New sqlrd.ucDSN()
        Me.chkGroupReports = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.btnConnect = New DevComponents.DotNetBar.ButtonX()
        Me.grpQuery = New System.Windows.Forms.GroupBox()
        Me.cmbDDKey = New System.Windows.Forms.ComboBox()
        Me.Label10 = New DevComponents.DotNetBar.LabelX()
        Me.btnBuild = New DevComponents.DotNetBar.ButtonX()
        Me.txtQuery = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label9 = New DevComponents.DotNetBar.LabelX()
        Me.grpDynamicTasks = New System.Windows.Forms.GroupBox()
        Me.optOnce = New System.Windows.Forms.RadioButton()
        Me.optAll = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label26 = New DevComponents.DotNetBar.LabelX()
        Me.txtAdjustStamp = New System.Windows.Forms.NumericUpDown()
        Me.grpAutoResume = New System.Windows.Forms.GroupBox()
        Me.txtCacheExpiry = New System.Windows.Forms.NumericUpDown()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.Label12 = New DevComponents.DotNetBar.LabelX()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider()
        Me.UcDest = New sqlrd.ucDestination()
        Me.oTask = New sqlrd.ucTasks()
        Me.tabProperties = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.btnXLSMerge = New DevComponents.DotNetBar.ButtonX()
        Me.btnPDFMerge = New DevComponents.DotNetBar.ButtonX()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.tabReports = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabDestinations = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabGeneral = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel10 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabDataDriver = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel5 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.UcBlank = New sqlrd.ucBlankReportX()
        Me.UcError = New sqlrd.ucErrorHandler()
        Me.tabExceptionHandling = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.UcSched = New sqlrd.ucSchedule()
        Me.tabSchedule = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel9 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.FlowLayoutPanel6 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
        Me.tabSnapshots = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel7 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabTasks = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel6 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.UcHistory = New sqlrd.ucScheduleHistory()
        Me.tabHistory = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel8 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.tabLinking = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel7 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.grpSelectReport.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.txtSnapshots, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step3.SuspendLayout()
        Me.grpQuery.SuspendLayout()
        Me.grpDynamicTasks.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtAdjustStamp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpAutoResume.SuspendLayout()
        CType(Me.txtCacheExpiry, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tabProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabProperties.SuspendLayout()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuperTabControlPanel4.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel10.SuspendLayout()
        Me.SuperTabControlPanel5.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuperTabControlPanel9.SuspendLayout()
        Me.FlowLayoutPanel6.SuspendLayout()
        Me.FlowLayoutPanel5.SuspendLayout()
        Me.SuperTabControlPanel7.SuspendLayout()
        Me.SuperTabControlPanel6.SuspendLayout()
        Me.SuperTabControlPanel8.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel7.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtDesc
        '
        Me.txtDesc.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDesc.Border.Class = "TextBoxBorder"
        Me.txtDesc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDesc.ForeColor = System.Drawing.Color.Black
        Me.HelpProvider1.SetHelpKeyword(Me.txtDesc, "packageGeneral.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtDesc, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtDesc.Location = New System.Drawing.Point(7, 119)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.HelpProvider1.SetShowHelp(Me.txtDesc, True)
        Me.txtDesc.Size = New System.Drawing.Size(376, 247)
        Me.txtDesc.TabIndex = 4
        Me.txtDesc.Tag = "memo"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label3, "packageGeneral.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label3, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(7, 103)
        Me.Label3.Name = "Label3"
        Me.HelpProvider1.SetShowHelp(Me.Label3, True)
        Me.Label3.Size = New System.Drawing.Size(208, 16)
        Me.Label3.TabIndex = 22
        Me.Label3.Text = "Description (optional)"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label7, "packageGeneral.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label7, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(7, 379)
        Me.Label7.Name = "Label7"
        Me.HelpProvider1.SetShowHelp(Me.Label7, True)
        Me.Label7.Size = New System.Drawing.Size(208, 16)
        Me.Label7.TabIndex = 21
        Me.Label7.Text = "Keyword (optional)"
        '
        'txtKeyWord
        '
        Me.txtKeyWord.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtKeyWord.Border.Class = "TextBoxBorder"
        Me.txtKeyWord.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKeyWord.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtKeyWord, "packageGeneral.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtKeyWord, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtKeyWord.Location = New System.Drawing.Point(7, 395)
        Me.txtKeyWord.Name = "txtKeyWord"
        Me.HelpProvider1.SetShowHelp(Me.txtKeyWord, True)
        Me.txtKeyWord.Size = New System.Drawing.Size(376, 21)
        Me.txtKeyWord.TabIndex = 5
        Me.txtKeyWord.Tag = "memo"
        '
        'cmdLoc
        '
        Me.cmdLoc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdLoc.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdLoc.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.HelpProvider1.SetHelpKeyword(Me.cmdLoc, "packageGeneral.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdLoc, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoc.Location = New System.Drawing.Point(327, 71)
        Me.cmdLoc.Name = "cmdLoc"
        Me.HelpProvider1.SetShowHelp(Me.cmdLoc, True)
        Me.cmdLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdLoc.TabIndex = 3
        Me.cmdLoc.Text = "..."
        '
        'txtFolder
        '
        Me.txtFolder.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtFolder.Border.Class = "TextBoxBorder"
        Me.txtFolder.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFolder.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtFolder.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtFolder, "packageGeneral.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtFolder, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtFolder.Location = New System.Drawing.Point(7, 71)
        Me.txtFolder.Name = "txtFolder"
        Me.txtFolder.ReadOnly = True
        Me.HelpProvider1.SetShowHelp(Me.txtFolder, True)
        Me.txtFolder.Size = New System.Drawing.Size(296, 21)
        Me.txtFolder.TabIndex = 2
        Me.txtFolder.Tag = "1"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Label4.ForeColor = System.Drawing.Color.Navy
        Me.HelpProvider1.SetHelpKeyword(Me.Label4, "packageGeneral.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label4, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(7, 55)
        Me.Label4.Name = "Label4"
        Me.HelpProvider1.SetShowHelp(Me.Label4, True)
        Me.Label4.Size = New System.Drawing.Size(208, 16)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Create In"
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtName, "packageGeneral.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtName, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtName.Location = New System.Drawing.Point(7, 23)
        Me.txtName.Name = "txtName"
        Me.HelpProvider1.SetShowHelp(Me.txtName, True)
        Me.txtName.Size = New System.Drawing.Size(376, 21)
        Me.txtName.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Label2.ForeColor = System.Drawing.Color.Navy
        Me.HelpProvider1.SetHelpKeyword(Me.Label2, "packageGeneral.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label2, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(7, 7)
        Me.Label2.Name = "Label2"
        Me.HelpProvider1.SetShowHelp(Me.Label2, True)
        Me.Label2.Size = New System.Drawing.Size(208, 16)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Package Name"
        '
        'cmdUp
        '
        Me.cmdUp.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdUp.BackColor = System.Drawing.Color.Transparent
        Me.cmdUp.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.HelpProvider1.SetHelpKeyword(Me.cmdUp, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdUp, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdUp.Image = CType(resources.GetObject("cmdUp.Image"), System.Drawing.Image)
        Me.cmdUp.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdUp.Location = New System.Drawing.Point(476, 265)
        Me.cmdUp.Name = "cmdUp"
        Me.HelpProvider1.SetShowHelp(Me.cmdUp, True)
        Me.cmdUp.Size = New System.Drawing.Size(75, 24)
        Me.cmdUp.TabIndex = 5
        '
        'cmdDown
        '
        Me.cmdDown.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdDown.BackColor = System.Drawing.Color.Transparent
        Me.cmdDown.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.HelpProvider1.SetHelpKeyword(Me.cmdDown, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdDown, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdDown.Image = CType(resources.GetObject("cmdDown.Image"), System.Drawing.Image)
        Me.cmdDown.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDown.Location = New System.Drawing.Point(476, 294)
        Me.cmdDown.Name = "cmdDown"
        Me.HelpProvider1.SetShowHelp(Me.cmdDown, True)
        Me.cmdDown.Size = New System.Drawing.Size(75, 24)
        Me.cmdDown.TabIndex = 6
        '
        'cmbDateTime
        '
        Me.cmbDateTime.Enabled = False
        Me.cmbDateTime.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.cmbDateTime, "packageOutput.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmbDateTime, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmbDateTime.ItemHeight = 13
        Me.cmbDateTime.Items.AddRange(New Object() {"ddhh", "ddMM", "ddMMyy", "ddMMyyhhmm", "ddMMyyhhmms", "ddMMyyyy", "ddMMyyyyhhmm", "ddMMyyyyhhmmss", "hhmm", "hhmmss", "MMddyy", "MMddyyhhmm", "MMddyyhhmmss", "MMddyyyy", "MMddyyyyhhmm", "MMddyyyyhhmmss", "yyMMdd", "yyMMddhhmm", "yyMMddhhmmss", "yyyyMMdd", "yyyyMMddhhmm", "yyyyMMddhhmmss", "MMMM"})
        Me.cmbDateTime.Location = New System.Drawing.Point(213, 12)
        Me.cmbDateTime.Name = "cmbDateTime"
        Me.HelpProvider1.SetShowHelp(Me.cmbDateTime, True)
        Me.cmbDateTime.Size = New System.Drawing.Size(178, 21)
        Me.cmbDateTime.TabIndex = 18
        '
        'chkDTStamp
        '
        Me.chkDTStamp.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkDTStamp.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.chkDTStamp, "packageOutput.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.chkDTStamp, System.Windows.Forms.HelpNavigator.Topic)
        Me.chkDTStamp.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkDTStamp.Location = New System.Drawing.Point(6, 9)
        Me.chkDTStamp.Name = "chkDTStamp"
        Me.HelpProvider1.SetShowHelp(Me.chkDTStamp, True)
        Me.chkDTStamp.Size = New System.Drawing.Size(160, 24)
        Me.chkDTStamp.TabIndex = 1
        Me.chkDTStamp.Text = "Append date/time stamp"
        '
        'cmdAddReport
        '
        Me.cmdAddReport.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAddReport.BackColor = System.Drawing.Color.Transparent
        Me.cmdAddReport.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.HelpProvider1.SetHelpKeyword(Me.cmdAddReport, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdAddReport, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdAddReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddReport.Location = New System.Drawing.Point(476, 42)
        Me.cmdAddReport.Name = "cmdAddReport"
        Me.HelpProvider1.SetShowHelp(Me.cmdAddReport, True)
        Me.cmdAddReport.Size = New System.Drawing.Size(75, 24)
        Me.cmdAddReport.TabIndex = 2
        Me.cmdAddReport.Text = "&Add"
        '
        'lsvReports
        '
        Me.lsvReports.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvReports.Border.Class = "ListViewBorder"
        Me.lsvReports.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvReports.CheckBoxes = True
        Me.lsvReports.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ReportName, Me.Format})
        Me.lsvReports.ForeColor = System.Drawing.Color.Blue
        Me.lsvReports.FullRowSelect = True
        Me.lsvReports.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.HelpProvider1.SetHelpKeyword(Me.lsvReports, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.lsvReports, System.Windows.Forms.HelpNavigator.Topic)
        Me.lsvReports.HideSelection = False
        Me.lsvReports.Location = New System.Drawing.Point(8, 43)
        Me.lsvReports.MultiSelect = False
        Me.lsvReports.Name = "lsvReports"
        Me.HelpProvider1.SetShowHelp(Me.lsvReports, True)
        Me.lsvReports.Size = New System.Drawing.Size(462, 276)
        Me.lsvReports.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.lsvReports, "Checked = Enabled, Unchecked = Disabled")
        Me.lsvReports.UseCompatibleStateImageBehavior = False
        Me.lsvReports.View = System.Windows.Forms.View.Details
        '
        'ReportName
        '
        Me.ReportName.Text = "Report Name"
        Me.ReportName.Width = 200
        '
        'Format
        '
        Me.Format.Text = "Format"
        Me.Format.Width = 125
        '
        'cmdRemoveReport
        '
        Me.cmdRemoveReport.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRemoveReport.BackColor = System.Drawing.Color.Transparent
        Me.cmdRemoveReport.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.HelpProvider1.SetHelpKeyword(Me.cmdRemoveReport, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdRemoveReport, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdRemoveReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveReport.Location = New System.Drawing.Point(476, 100)
        Me.cmdRemoveReport.Name = "cmdRemoveReport"
        Me.HelpProvider1.SetShowHelp(Me.cmdRemoveReport, True)
        Me.cmdRemoveReport.Size = New System.Drawing.Size(75, 24)
        Me.cmdRemoveReport.TabIndex = 4
        Me.cmdRemoveReport.Text = "&Remove"
        '
        'cmdEdit
        '
        Me.cmdEdit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdEdit.BackColor = System.Drawing.Color.Transparent
        Me.cmdEdit.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.HelpProvider1.SetHelpKeyword(Me.cmdEdit, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdEdit, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEdit.Location = New System.Drawing.Point(476, 71)
        Me.cmdEdit.Name = "cmdEdit"
        Me.HelpProvider1.SetShowHelp(Me.cmdEdit, True)
        Me.cmdEdit.Size = New System.Drawing.Size(75, 24)
        Me.cmdEdit.TabIndex = 3
        Me.cmdEdit.Text = "&Edit"
        '
        'cmdApply
        '
        Me.cmdApply.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdApply.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdApply.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdApply.Enabled = False
        Me.cmdApply.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdApply.Location = New System.Drawing.Point(642, 3)
        Me.cmdApply.Name = "cmdApply"
        Me.cmdApply.Size = New System.Drawing.Size(75, 23)
        Me.cmdApply.TabIndex = 49
        Me.cmdApply.Text = "&Apply"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(561, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 48
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(480, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        '
        'txtMergePDF
        '
        Me.txtMergePDF.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtMergePDF.Border.Class = "TextBoxBorder"
        Me.txtMergePDF.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtMergePDF.Enabled = False
        Me.txtMergePDF.ForeColor = System.Drawing.Color.Black
        Me.HelpProvider1.SetHelpKeyword(Me.txtMergePDF, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtMergePDF, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtMergePDF.Location = New System.Drawing.Point(286, 330)
        Me.txtMergePDF.Name = "txtMergePDF"
        Me.HelpProvider1.SetShowHelp(Me.txtMergePDF, True)
        Me.txtMergePDF.Size = New System.Drawing.Size(184, 21)
        Me.txtMergePDF.TabIndex = 8
        Me.txtMergePDF.Tag = "memo"
        '
        'chkMergeXL
        '
        Me.chkMergeXL.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkMergeXL.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.chkMergeXL, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.chkMergeXL, System.Windows.Forms.HelpNavigator.Topic)
        Me.chkMergeXL.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkMergeXL.Location = New System.Drawing.Point(8, 353)
        Me.chkMergeXL.Name = "chkMergeXL"
        Me.HelpProvider1.SetShowHelp(Me.chkMergeXL, True)
        Me.chkMergeXL.Size = New System.Drawing.Size(253, 32)
        Me.chkMergeXL.TabIndex = 9
        Me.chkMergeXL.Text = "Merge all Excel outputs into a single workbook"
        '
        'chkMergePDF
        '
        Me.chkMergePDF.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkMergePDF.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.chkMergePDF, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.chkMergePDF, System.Windows.Forms.HelpNavigator.Topic)
        Me.chkMergePDF.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkMergePDF.Location = New System.Drawing.Point(8, 325)
        Me.chkMergePDF.Name = "chkMergePDF"
        Me.HelpProvider1.SetShowHelp(Me.chkMergePDF, True)
        Me.chkMergePDF.Size = New System.Drawing.Size(262, 31)
        Me.chkMergePDF.TabIndex = 7
        Me.chkMergePDF.Text = "Merge all PDF outputs into a single  file"
        '
        'txtMergeXL
        '
        Me.txtMergeXL.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtMergeXL.Border.Class = "TextBoxBorder"
        Me.txtMergeXL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtMergeXL.Enabled = False
        Me.txtMergeXL.ForeColor = System.Drawing.Color.Black
        Me.HelpProvider1.SetHelpKeyword(Me.txtMergeXL, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtMergeXL, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtMergeXL.Location = New System.Drawing.Point(286, 359)
        Me.txtMergeXL.Name = "txtMergeXL"
        Me.HelpProvider1.SetShowHelp(Me.txtMergeXL, True)
        Me.txtMergeXL.Size = New System.Drawing.Size(184, 21)
        Me.txtMergeXL.TabIndex = 10
        Me.txtMergeXL.Tag = "memo"
        '
        'btnView
        '
        Me.btnView.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnView.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.HelpProvider1.SetHelpKeyword(Me.btnView, "Linking_-_Dynamic_Package_Properties.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.btnView, System.Windows.Forms.HelpNavigator.Topic)
        Me.btnView.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnView.Location = New System.Drawing.Point(377, 3)
        Me.btnView.Name = "btnView"
        Me.HelpProvider1.SetShowHelp(Me.btnView, True)
        Me.btnView.Size = New System.Drawing.Size(40, 22)
        Me.btnView.TabIndex = 4
        Me.btnView.Text = "..."
        '
        'chkStaticDest
        '
        Me.chkStaticDest.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkStaticDest.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.chkStaticDest, "Linking_-_Dynamic_Package_Properties.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.chkStaticDest, System.Windows.Forms.HelpNavigator.Topic)
        Me.chkStaticDest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkStaticDest.Location = New System.Drawing.Point(3, 3)
        Me.chkStaticDest.Name = "chkStaticDest"
        Me.HelpProvider1.SetShowHelp(Me.chkStaticDest, True)
        Me.chkStaticDest.Size = New System.Drawing.Size(376, 24)
        Me.chkStaticDest.TabIndex = 5
        Me.chkStaticDest.Text = "Use a static destination for this dynamic schedule"
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox6.Controls.Add(Me.UcDSN)
        Me.GroupBox6.Controls.Add(Me.cmdValidate)
        Me.GroupBox6.Controls.Add(Me.GroupBox2)
        Me.HelpProvider1.SetHelpKeyword(Me.GroupBox6, "Linking_-_Dynamic_Package_Properties.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.GroupBox6, System.Windows.Forms.HelpNavigator.Topic)
        Me.GroupBox6.Location = New System.Drawing.Point(3, 90)
        Me.GroupBox6.Name = "GroupBox6"
        Me.HelpProvider1.SetShowHelp(Me.GroupBox6, True)
        Me.GroupBox6.Size = New System.Drawing.Size(414, 288)
        Me.GroupBox6.TabIndex = 17
        Me.GroupBox6.TabStop = False
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.Color.Transparent
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN.Location = New System.Drawing.Point(8, 16)
        Me.UcDSN.m_conString = "|||"
        Me.UcDSN.m_showConnectionType = False
        Me.UcDSN.Name = "UcDSN"
        Me.UcDSN.Size = New System.Drawing.Size(384, 112)
        Me.UcDSN.TabIndex = 6
        '
        'cmdValidate
        '
        Me.cmdValidate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdValidate.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdValidate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdValidate.Location = New System.Drawing.Point(168, 136)
        Me.cmdValidate.Name = "cmdValidate"
        Me.cmdValidate.Size = New System.Drawing.Size(75, 23)
        Me.cmdValidate.TabIndex = 7
        Me.cmdValidate.Text = "Connect..."
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnClear)
        Me.GroupBox2.Controls.Add(Me.btnTest)
        Me.GroupBox2.Controls.Add(Me.Label21)
        Me.GroupBox2.Controls.Add(Me.cmbValue)
        Me.GroupBox2.Controls.Add(Me.cmbColumn)
        Me.GroupBox2.Controls.Add(Me.cmbTable)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 160)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(400, 112)
        Me.GroupBox2.TabIndex = 15
        Me.GroupBox2.TabStop = False
        '
        'btnClear
        '
        Me.btnClear.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnClear.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnClear.Location = New System.Drawing.Point(282, 79)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(70, 21)
        Me.btnClear.TabIndex = 12
        Me.btnClear.Text = "Clear"
        '
        'btnTest
        '
        Me.btnTest.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnTest.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnTest.Location = New System.Drawing.Point(184, 80)
        Me.btnTest.Name = "btnTest"
        Me.btnTest.Size = New System.Drawing.Size(83, 21)
        Me.btnTest.TabIndex = 11
        Me.btnTest.Text = "Test"
        '
        'Label21
        '
        '
        '
        '
        Me.Label21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label21.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label21.Location = New System.Drawing.Point(8, 64)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(352, 16)
        Me.Label21.TabIndex = 9
        Me.Label21.Text = "Please select the column that holds the required xxx"
        '
        'cmbValue
        '
        Me.cmbValue.Enabled = False
        Me.cmbValue.ItemHeight = 13
        Me.cmbValue.Location = New System.Drawing.Point(8, 80)
        Me.cmbValue.Name = "cmbValue"
        Me.cmbValue.Size = New System.Drawing.Size(168, 21)
        Me.cmbValue.TabIndex = 10
        '
        'cmbColumn
        '
        Me.cmbColumn.Enabled = False
        Me.cmbColumn.ItemHeight = 13
        Me.cmbColumn.Location = New System.Drawing.Point(184, 32)
        Me.cmbColumn.Name = "cmbColumn"
        Me.cmbColumn.Size = New System.Drawing.Size(168, 21)
        Me.cmbColumn.TabIndex = 9
        '
        'cmbTable
        '
        Me.cmbTable.Enabled = False
        Me.cmbTable.ItemHeight = 13
        Me.cmbTable.Location = New System.Drawing.Point(8, 32)
        Me.cmbTable.Name = "cmbTable"
        Me.cmbTable.Size = New System.Drawing.Size(168, 21)
        Me.cmbTable.TabIndex = 8
        '
        'Label8
        '
        '
        '
        '
        Me.Label8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(8, 16)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(384, 16)
        Me.Label8.TabIndex = 10
        Me.Label8.Text = "Please select the table and column that must equal the provided parameter"
        '
        'optStatic
        '
        Me.optStatic.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.optStatic, "Linking_-_Dynamic_Package_Properties.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.optStatic, System.Windows.Forms.HelpNavigator.Topic)
        Me.optStatic.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optStatic.Location = New System.Drawing.Point(3, 3)
        Me.optStatic.Name = "optStatic"
        Me.HelpProvider1.SetShowHelp(Me.optStatic, True)
        Me.optStatic.Size = New System.Drawing.Size(149, 22)
        Me.optStatic.TabIndex = 2
        Me.optStatic.TabStop = True
        Me.optStatic.Text = "Populate from static data"
        Me.optStatic.UseVisualStyleBackColor = False
        '
        'cmbKey
        '
        Me.HelpProvider1.SetHelpKeyword(Me.cmbKey, "Linking_-_Dynamic_Package_Properties.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmbKey, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmbKey.ItemHeight = 13
        Me.cmbKey.Location = New System.Drawing.Point(90, 3)
        Me.cmbKey.Name = "cmbKey"
        Me.HelpProvider1.SetShowHelp(Me.cmbKey, True)
        Me.cmbKey.Size = New System.Drawing.Size(327, 21)
        Me.cmbKey.TabIndex = 1
        '
        'Label23
        '
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label23, "Linking_-_Dynamic_Package_Properties.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label23, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label23.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label23.Location = New System.Drawing.Point(3, 3)
        Me.Label23.Name = "Label23"
        Me.HelpProvider1.SetShowHelp(Me.Label23, True)
        Me.Label23.Size = New System.Drawing.Size(81, 18)
        Me.Label23.TabIndex = 11
        Me.Label23.Text = "Key Parameter"
        '
        'optDynamic
        '
        Me.optDynamic.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.optDynamic, "Linking_-_Dynamic_Package_Properties.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.optDynamic, System.Windows.Forms.HelpNavigator.Topic)
        Me.optDynamic.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDynamic.Location = New System.Drawing.Point(158, 3)
        Me.optDynamic.Name = "optDynamic"
        Me.HelpProvider1.SetShowHelp(Me.optDynamic, True)
        Me.optDynamic.Size = New System.Drawing.Size(213, 22)
        Me.optDynamic.TabIndex = 3
        Me.optDynamic.TabStop = True
        Me.optDynamic.Text = "Populate with data from database"
        Me.optDynamic.UseVisualStyleBackColor = False
        '
        'chkAutoResume
        '
        Me.chkAutoResume.AutoSize = True
        '
        '
        '
        Me.chkAutoResume.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAutoResume.Location = New System.Drawing.Point(6, 20)
        Me.chkAutoResume.Name = "chkAutoResume"
        Me.chkAutoResume.Size = New System.Drawing.Size(262, 16)
        Me.chkAutoResume.TabIndex = 0
        Me.chkAutoResume.Text = "Resume failed/errored schedule with cached data"
        Me.ToolTip1.SetToolTip(Me.chkAutoResume, "If the schedule fails mid-flight, next time it runs, it will resume from where it" & _
        " left off instead of starting from the beginning")
        '
        'chkMergeAllXL
        '
        Me.chkMergeAllXL.AutoSize = True
        '
        '
        '
        Me.chkMergeAllXL.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkMergeAllXL.Location = New System.Drawing.Point(25, 426)
        Me.chkMergeAllXL.Name = "chkMergeAllXL"
        Me.chkMergeAllXL.Size = New System.Drawing.Size(250, 16)
        Me.chkMergeAllXL.TabIndex = 27
        Me.chkMergeAllXL.Text = "Merge the group into a single Excel Workbook:"
        Me.ToolTip1.SetToolTip(Me.chkMergeAllXL, "Selecting this option will over-ride the XL merging settings for the individual r" & _
        "eports (per record merging) .")
        '
        'chkMergeAllPDF
        '
        Me.chkMergeAllPDF.AutoSize = True
        '
        '
        '
        Me.chkMergeAllPDF.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkMergeAllPDF.Location = New System.Drawing.Point(25, 406)
        Me.chkMergeAllPDF.Name = "chkMergeAllPDF"
        Me.chkMergeAllPDF.Size = New System.Drawing.Size(210, 16)
        Me.chkMergeAllPDF.TabIndex = 28
        Me.chkMergeAllPDF.Text = "Merge the group into a single PDF file:"
        Me.ToolTip1.SetToolTip(Me.chkMergeAllPDF, "Selecting this option will over-ride the PDF merging settings for the individual " & _
        "reports (per record merging) .")
        '
        'chkMergeText
        '
        Me.chkMergeText.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkMergeText.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.chkMergeText, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.chkMergeText, System.Windows.Forms.HelpNavigator.Topic)
        Me.chkMergeText.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkMergeText.Location = New System.Drawing.Point(8, 381)
        Me.chkMergeText.Name = "chkMergeText"
        Me.HelpProvider1.SetShowHelp(Me.chkMergeText, True)
        Me.chkMergeText.Size = New System.Drawing.Size(253, 32)
        Me.chkMergeText.TabIndex = 13
        Me.chkMergeText.Text = "Merge all Text outputs into a single file"
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label13, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label13, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(476, 388)
        Me.Label13.Name = "Label13"
        Me.HelpProvider1.SetShowHelp(Me.Label13, True)
        Me.Label13.Size = New System.Drawing.Size(32, 16)
        Me.Label13.TabIndex = 12
        Me.Label13.Text = ".txt"
        '
        'txtMergeTextName
        '
        Me.txtMergeTextName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtMergeTextName.Border.Class = "TextBoxBorder"
        Me.txtMergeTextName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtMergeTextName.Enabled = False
        Me.txtMergeTextName.ForeColor = System.Drawing.Color.Black
        Me.HelpProvider1.SetHelpKeyword(Me.txtMergeTextName, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtMergeTextName, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtMergeTextName.Location = New System.Drawing.Point(286, 387)
        Me.txtMergeTextName.Name = "txtMergeTextName"
        Me.HelpProvider1.SetShowHelp(Me.txtMergeTextName, True)
        Me.txtMergeTextName.Size = New System.Drawing.Size(184, 21)
        Me.txtMergeTextName.TabIndex = 14
        Me.txtMergeTextName.Tag = "memo"
        '
        'chkMultithreaded
        '
        Me.chkMultithreaded.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkMultithreaded.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.chkMultithreaded, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.chkMultithreaded, System.Windows.Forms.HelpNavigator.Topic)
        Me.chkMultithreaded.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkMultithreaded.Location = New System.Drawing.Point(8, 406)
        Me.chkMultithreaded.Name = "chkMultithreaded"
        Me.HelpProvider1.SetShowHelp(Me.chkMultithreaded, True)
        Me.chkMultithreaded.Size = New System.Drawing.Size(354, 32)
        Me.chkMultithreaded.TabIndex = 11
        Me.chkMultithreaded.Text = "Run Package Multi-Threaded"
        '
        'grpSelectReport
        '
        Me.grpSelectReport.BackColor = System.Drawing.Color.Transparent
        Me.grpSelectReport.Controls.Add(Me.chkSelectAll)
        Me.grpSelectReport.Location = New System.Drawing.Point(3, 3)
        Me.grpSelectReport.Name = "grpSelectReport"
        Me.grpSelectReport.Size = New System.Drawing.Size(354, 31)
        Me.grpSelectReport.TabIndex = 0
        Me.grpSelectReport.TabStop = False
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        '
        '
        '
        Me.chkSelectAll.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkSelectAll.Checked = True
        Me.chkSelectAll.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkSelectAll.CheckValue = Nothing
        Me.chkSelectAll.Location = New System.Drawing.Point(5, 11)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(67, 16)
        Me.chkSelectAll.TabIndex = 0
        Me.chkSelectAll.Text = "Select All"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.ExpandableSplitter1)
        Me.Panel1.Controls.Add(Me.lsvSnapshots)
        Me.Panel1.Controls.Add(Me.tvPacks)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 31)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(584, 406)
        Me.Panel1.TabIndex = 16
        '
        'ExpandableSplitter1
        '
        Me.ExpandableSplitter1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ExpandableSplitter1.BackColor2 = System.Drawing.Color.Empty
        Me.ExpandableSplitter1.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.ExpandableSplitter1.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.ExpandableSplitter1.ExpandableControl = Me.tvPacks
        Me.ExpandableSplitter1.ExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(254, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.ExpandableSplitter1.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground
        Me.ExpandableSplitter1.ExpandLineColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ExpandableSplitter1.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.ForeColor = System.Drawing.Color.Black
        Me.ExpandableSplitter1.GripDarkColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ExpandableSplitter1.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.GripLightColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer))
        Me.ExpandableSplitter1.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.MenuBackground
        Me.ExpandableSplitter1.HotBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(213, Byte), Integer), CType(CType(140, Byte), Integer))
        Me.ExpandableSplitter1.HotBackColor2 = System.Drawing.Color.Empty
        Me.ExpandableSplitter1.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.ExpandableSplitter1.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemCheckedBackground
        Me.ExpandableSplitter1.HotExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(254, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.ExpandableSplitter1.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground
        Me.ExpandableSplitter1.HotExpandLineColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ExpandableSplitter1.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.HotGripDarkColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ExpandableSplitter1.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.HotGripLightColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer))
        Me.ExpandableSplitter1.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.MenuBackground
        Me.ExpandableSplitter1.Location = New System.Drawing.Point(121, 0)
        Me.ExpandableSplitter1.Name = "ExpandableSplitter1"
        Me.ExpandableSplitter1.Size = New System.Drawing.Size(3, 406)
        Me.ExpandableSplitter1.Style = DevComponents.DotNetBar.eSplitterStyle.Mozilla
        Me.ExpandableSplitter1.TabIndex = 16
        Me.ExpandableSplitter1.TabStop = False
        '
        'tvPacks
        '
        Me.tvPacks.Dock = System.Windows.Forms.DockStyle.Left
        Me.tvPacks.FullRowSelect = True
        Me.HelpProvider1.SetHelpKeyword(Me.tvPacks, "Snapshots_-_Packages.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.tvPacks, System.Windows.Forms.HelpNavigator.Topic)
        Me.tvPacks.HideSelection = False
        Me.tvPacks.Location = New System.Drawing.Point(0, 0)
        Me.tvPacks.Name = "tvPacks"
        Me.HelpProvider1.SetShowHelp(Me.tvPacks, True)
        Me.tvPacks.Size = New System.Drawing.Size(121, 406)
        Me.tvPacks.TabIndex = 3
        '
        'lsvSnapshots
        '
        Me.lsvSnapshots.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvSnapshots.Border.Class = "ListViewBorder"
        Me.lsvSnapshots.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvSnapshots.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader5, Me.ColumnHeader6})
        Me.lsvSnapshots.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvSnapshots.ForeColor = System.Drawing.Color.Black
        Me.lsvSnapshots.FullRowSelect = True
        Me.lsvSnapshots.GridLines = True
        Me.lsvSnapshots.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.HelpProvider1.SetHelpKeyword(Me.lsvSnapshots, "Snapshots_-_Packages.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.lsvSnapshots, System.Windows.Forms.HelpNavigator.Topic)
        Me.lsvSnapshots.HideSelection = False
        Me.lsvSnapshots.Location = New System.Drawing.Point(121, 0)
        Me.lsvSnapshots.Name = "lsvSnapshots"
        Me.HelpProvider1.SetShowHelp(Me.lsvSnapshots, True)
        Me.lsvSnapshots.Size = New System.Drawing.Size(463, 406)
        Me.lsvSnapshots.TabIndex = 4
        Me.lsvSnapshots.UseCompatibleStateImageBehavior = False
        Me.lsvSnapshots.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Entry Date"
        Me.ColumnHeader5.Width = 76
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Path"
        Me.ColumnHeader6.Width = 170
        '
        'cmdView
        '
        Me.cmdView.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdView.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdView.Enabled = False
        Me.HelpProvider1.SetHelpKeyword(Me.cmdView, "Snapshots_-_Packages.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdView, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdView.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdView.Location = New System.Drawing.Point(3, 3)
        Me.cmdView.Name = "cmdView"
        Me.HelpProvider1.SetShowHelp(Me.cmdView, True)
        Me.cmdView.Size = New System.Drawing.Size(75, 23)
        Me.cmdView.TabIndex = 5
        Me.cmdView.Text = "View"
        '
        'chkSnapshots
        '
        Me.chkSnapshots.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkSnapshots.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.chkSnapshots, "Snapshots_-_Packages.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.chkSnapshots, System.Windows.Forms.HelpNavigator.Topic)
        Me.chkSnapshots.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkSnapshots.Location = New System.Drawing.Point(3, 3)
        Me.chkSnapshots.Name = "chkSnapshots"
        Me.HelpProvider1.SetShowHelp(Me.chkSnapshots, True)
        Me.chkSnapshots.Size = New System.Drawing.Size(240, 24)
        Me.chkSnapshots.TabIndex = 1
        Me.chkSnapshots.Text = "Enable snapshots and keep snapshots for"
        '
        'cmdExecute
        '
        Me.cmdExecute.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdExecute.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdExecute.Enabled = False
        Me.HelpProvider1.SetHelpKeyword(Me.cmdExecute, "Snapshots_-_Packages.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdExecute, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdExecute.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdExecute.Location = New System.Drawing.Point(165, 3)
        Me.cmdExecute.Name = "cmdExecute"
        Me.HelpProvider1.SetShowHelp(Me.cmdExecute, True)
        Me.cmdExecute.Size = New System.Drawing.Size(75, 23)
        Me.cmdExecute.TabIndex = 6
        Me.cmdExecute.Text = "Execute"
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label11, "Snapshots_-_Packages.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label11, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(311, 3)
        Me.Label11.Name = "Label11"
        Me.HelpProvider1.SetShowHelp(Me.Label11, True)
        Me.Label11.Size = New System.Drawing.Size(80, 16)
        Me.Label11.TabIndex = 10
        Me.Label11.Text = "Days"
        '
        'cmdDelete
        '
        Me.cmdDelete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdDelete.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdDelete.Enabled = False
        Me.HelpProvider1.SetHelpKeyword(Me.cmdDelete, "Snapshots_-_Packages.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdDelete, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDelete.Location = New System.Drawing.Point(84, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.HelpProvider1.SetShowHelp(Me.cmdDelete, True)
        Me.cmdDelete.Size = New System.Drawing.Size(75, 23)
        Me.cmdDelete.TabIndex = 7
        Me.cmdDelete.Text = "Delete"
        '
        'txtSnapshots
        '
        Me.txtSnapshots.Enabled = False
        Me.HelpProvider1.SetHelpKeyword(Me.txtSnapshots, "Snapshots_-_Packages.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtSnapshots, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtSnapshots.Location = New System.Drawing.Point(249, 3)
        Me.txtSnapshots.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.txtSnapshots.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtSnapshots.Name = "txtSnapshots"
        Me.HelpProvider1.SetShowHelp(Me.txtSnapshots, True)
        Me.txtSnapshots.Size = New System.Drawing.Size(56, 21)
        Me.txtSnapshots.TabIndex = 2
        Me.txtSnapshots.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSnapshots.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Step3
        '
        Me.Step3.BackColor = System.Drawing.Color.Transparent
        Me.Step3.Controls.Add(Me.txtMergeAllXL)
        Me.Step3.Controls.Add(Me.txtMergeAllPDF)
        Me.Step3.Controls.Add(Me.chkMergeAllXL)
        Me.Step3.Controls.Add(Me.chkMergeAllPDF)
        Me.Step3.Controls.Add(Me.UcDSNDD)
        Me.Step3.Controls.Add(Me.chkGroupReports)
        Me.Step3.Controls.Add(Me.btnConnect)
        Me.Step3.Controls.Add(Me.grpQuery)
        Me.Step3.Controls.Add(Me.Label9)
        Me.Step3.Location = New System.Drawing.Point(3, 3)
        Me.Step3.Name = "Step3"
        Me.Step3.Size = New System.Drawing.Size(569, 460)
        Me.Step3.TabIndex = 22
        '
        'txtMergeAllXL
        '
        Me.txtMergeAllXL.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtMergeAllXL.Border.Class = "TextBoxBorder"
        Me.txtMergeAllXL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtMergeAllXL.Enabled = False
        Me.txtMergeAllXL.ForeColor = System.Drawing.Color.Black
        Me.txtMergeAllXL.Location = New System.Drawing.Point(277, 426)
        Me.txtMergeAllXL.Name = "txtMergeAllXL"
        Me.txtMergeAllXL.Size = New System.Drawing.Size(147, 21)
        Me.txtMergeAllXL.TabIndex = 29
        '
        'txtMergeAllPDF
        '
        Me.txtMergeAllPDF.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtMergeAllPDF.Border.Class = "TextBoxBorder"
        Me.txtMergeAllPDF.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtMergeAllPDF.Enabled = False
        Me.txtMergeAllPDF.ForeColor = System.Drawing.Color.Black
        Me.txtMergeAllPDF.Location = New System.Drawing.Point(277, 403)
        Me.txtMergeAllPDF.Name = "txtMergeAllPDF"
        Me.txtMergeAllPDF.Size = New System.Drawing.Size(147, 21)
        Me.txtMergeAllPDF.TabIndex = 30
        '
        'UcDSNDD
        '
        Me.UcDSNDD.BackColor = System.Drawing.Color.White
        Me.UcDSNDD.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSNDD.ForeColor = System.Drawing.Color.Navy
        Me.UcDSNDD.Location = New System.Drawing.Point(11, 27)
        Me.UcDSNDD.m_conString = "|||"
        Me.UcDSNDD.m_showConnectionType = False
        Me.UcDSNDD.Name = "UcDSNDD"
        Me.UcDSNDD.Size = New System.Drawing.Size(422, 128)
        Me.UcDSNDD.TabIndex = 6
        '
        'chkGroupReports
        '
        Me.chkGroupReports.AutoSize = True
        '
        '
        '
        Me.chkGroupReports.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkGroupReports.Location = New System.Drawing.Point(8, 383)
        Me.chkGroupReports.Name = "chkGroupReports"
        Me.chkGroupReports.Size = New System.Drawing.Size(407, 16)
        Me.chkGroupReports.TabIndex = 5
        Me.chkGroupReports.Text = "Group reports together by email address/directory (email/disk destination only)"
        '
        'btnConnect
        '
        Me.btnConnect.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnConnect.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnConnect.Location = New System.Drawing.Point(185, 161)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(75, 23)
        Me.btnConnect.TabIndex = 3
        Me.btnConnect.Text = "&Connect"
        '
        'grpQuery
        '
        Me.grpQuery.Controls.Add(Me.cmbDDKey)
        Me.grpQuery.Controls.Add(Me.Label10)
        Me.grpQuery.Controls.Add(Me.btnBuild)
        Me.grpQuery.Controls.Add(Me.txtQuery)
        Me.grpQuery.Enabled = False
        Me.grpQuery.Location = New System.Drawing.Point(8, 184)
        Me.grpQuery.Name = "grpQuery"
        Me.grpQuery.Size = New System.Drawing.Size(425, 189)
        Me.grpQuery.TabIndex = 2
        Me.grpQuery.TabStop = False
        Me.grpQuery.Text = "Data selection criteria"
        '
        'cmbDDKey
        '
        Me.cmbDDKey.FormattingEnabled = True
        Me.cmbDDKey.Location = New System.Drawing.Point(79, 161)
        Me.cmbDDKey.Name = "cmbDDKey"
        Me.cmbDDKey.Size = New System.Drawing.Size(171, 21)
        Me.cmbDDKey.TabIndex = 5
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        '
        '
        '
        Me.Label10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label10.Location = New System.Drawing.Point(7, 165)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(60, 16)
        Me.Label10.TabIndex = 4
        Me.Label10.Text = "Key Column"
        '
        'btnBuild
        '
        Me.btnBuild.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnBuild.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnBuild.Location = New System.Drawing.Point(177, 130)
        Me.btnBuild.Name = "btnBuild"
        Me.btnBuild.Size = New System.Drawing.Size(75, 23)
        Me.btnBuild.TabIndex = 1
        Me.btnBuild.Text = "&Build"
        '
        'txtQuery
        '
        Me.txtQuery.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtQuery.Border.Class = "TextBoxBorder"
        Me.txtQuery.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtQuery.ForeColor = System.Drawing.Color.Black
        Me.txtQuery.Location = New System.Drawing.Point(9, 19)
        Me.txtQuery.Multiline = True
        Me.txtQuery.Name = "txtQuery"
        Me.txtQuery.ReadOnly = True
        Me.txtQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtQuery.Size = New System.Drawing.Size(410, 103)
        Me.txtQuery.TabIndex = 0
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        '
        '
        '
        Me.Label9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label9.Location = New System.Drawing.Point(5, 11)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(136, 16)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "Database Connection Setup"
        '
        'grpDynamicTasks
        '
        Me.grpDynamicTasks.BackColor = System.Drawing.Color.Transparent
        Me.grpDynamicTasks.Controls.Add(Me.optOnce)
        Me.grpDynamicTasks.Controls.Add(Me.optAll)
        Me.grpDynamicTasks.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.grpDynamicTasks.Enabled = False
        Me.grpDynamicTasks.Location = New System.Drawing.Point(0, 396)
        Me.grpDynamicTasks.Name = "grpDynamicTasks"
        Me.grpDynamicTasks.Size = New System.Drawing.Size(584, 74)
        Me.grpDynamicTasks.TabIndex = 18
        Me.grpDynamicTasks.TabStop = False
        Me.grpDynamicTasks.Text = "Run custom tasks..."
        '
        'optOnce
        '
        Me.optOnce.AutoSize = True
        Me.HelpProvider1.SetHelpKeyword(Me.optOnce, "packageTasks.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.optOnce, System.Windows.Forms.HelpNavigator.Topic)
        Me.optOnce.Location = New System.Drawing.Point(6, 46)
        Me.optOnce.Name = "optOnce"
        Me.HelpProvider1.SetShowHelp(Me.optOnce, True)
        Me.optOnce.Size = New System.Drawing.Size(162, 17)
        Me.optOnce.TabIndex = 2
        Me.optOnce.TabStop = True
        Me.optOnce.Tag = "1"
        Me.optOnce.Text = "Once for the entire schedule"
        Me.optOnce.UseVisualStyleBackColor = True
        '
        'optAll
        '
        Me.optAll.AutoSize = True
        Me.optAll.Checked = True
        Me.HelpProvider1.SetHelpKeyword(Me.optAll, "packageTasks.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.optAll, System.Windows.Forms.HelpNavigator.Topic)
        Me.optAll.Location = New System.Drawing.Point(6, 20)
        Me.optAll.Name = "optAll"
        Me.HelpProvider1.SetShowHelp(Me.optAll, True)
        Me.optAll.Size = New System.Drawing.Size(179, 17)
        Me.optAll.TabIndex = 1
        Me.optAll.TabStop = True
        Me.optAll.Tag = "0"
        Me.optAll.Text = "Once for each generated report"
        Me.optAll.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.chkDTStamp)
        Me.GroupBox1.Controls.Add(Me.cmbDateTime)
        Me.GroupBox1.Controls.Add(Me.Label26)
        Me.GroupBox1.Controls.Add(Me.txtAdjustStamp)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 390)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(462, 73)
        Me.GroupBox1.TabIndex = 24
        Me.GroupBox1.TabStop = False
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label26, "packageOutput.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label26, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label26.Location = New System.Drawing.Point(3, 49)
        Me.Label26.Name = "Label26"
        Me.HelpProvider1.SetShowHelp(Me.Label26, True)
        Me.Label26.Size = New System.Drawing.Size(165, 16)
        Me.Label26.TabIndex = 21
        Me.Label26.Text = "Adjust date/time stamp by (days)"
        '
        'txtAdjustStamp
        '
        Me.txtAdjustStamp.Enabled = False
        Me.HelpProvider1.SetHelpKeyword(Me.txtAdjustStamp, "packageOutput.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtAdjustStamp, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtAdjustStamp.Location = New System.Drawing.Point(213, 43)
        Me.txtAdjustStamp.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.txtAdjustStamp.Minimum = New Decimal(New Integer() {365, 0, 0, -2147483648})
        Me.txtAdjustStamp.Name = "txtAdjustStamp"
        Me.HelpProvider1.SetShowHelp(Me.txtAdjustStamp, True)
        Me.txtAdjustStamp.Size = New System.Drawing.Size(49, 21)
        Me.txtAdjustStamp.TabIndex = 3
        Me.txtAdjustStamp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'grpAutoResume
        '
        Me.grpAutoResume.BackColor = System.Drawing.Color.Transparent
        Me.grpAutoResume.Controls.Add(Me.txtCacheExpiry)
        Me.grpAutoResume.Controls.Add(Me.Label1)
        Me.grpAutoResume.Controls.Add(Me.Label12)
        Me.grpAutoResume.Controls.Add(Me.chkAutoResume)
        Me.grpAutoResume.Enabled = False
        Me.grpAutoResume.Location = New System.Drawing.Point(6, 310)
        Me.grpAutoResume.Name = "grpAutoResume"
        Me.grpAutoResume.Size = New System.Drawing.Size(462, 74)
        Me.grpAutoResume.TabIndex = 23
        Me.grpAutoResume.TabStop = False
        Me.grpAutoResume.Text = "Resume with cached data"
        '
        'txtCacheExpiry
        '
        Me.txtCacheExpiry.Enabled = False
        Me.txtCacheExpiry.Location = New System.Drawing.Point(213, 41)
        Me.txtCacheExpiry.Maximum = New Decimal(New Integer() {10080, 0, 0, 0})
        Me.txtCacheExpiry.Name = "txtCacheExpiry"
        Me.txtCacheExpiry.Size = New System.Drawing.Size(38, 21)
        Me.txtCacheExpiry.TabIndex = 2
        Me.txtCacheExpiry.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.Location = New System.Drawing.Point(257, 45)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "minutes"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        '
        '
        '
        Me.Label12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label12.Location = New System.Drawing.Point(3, 45)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(164, 16)
        Me.Label12.TabIndex = 1
        Me.Label12.Text = "Expire dynamic cached data after"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'UcDest
        '
        Me.UcDest.BackColor = System.Drawing.Color.Transparent
        Me.UcDest.destinationsPicker = False
        Me.UcDest.Dock = System.Windows.Forms.DockStyle.Top
        Me.UcDest.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.HelpProvider1.SetHelpKeyword(Me.UcDest, "packageOutput.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.UcDest, System.Windows.Forms.HelpNavigator.Topic)
        Me.UcDest.Location = New System.Drawing.Point(0, 0)
        Me.UcDest.m_CanDisable = True
        Me.UcDest.m_DelayDelete = True
        Me.UcDest.m_eventBased = False
        Me.UcDest.m_ExcelBurst = False
        Me.UcDest.m_IsDynamic = False
        Me.UcDest.m_isPackage = False
        Me.UcDest.m_IsQuery = False
        Me.UcDest.m_StaticDest = False
        Me.UcDest.Name = "UcDest"
        Me.HelpProvider1.SetShowHelp(Me.UcDest, True)
        Me.UcDest.Size = New System.Drawing.Size(584, 304)
        Me.UcDest.TabIndex = 0
        '
        'oTask
        '
        Me.oTask.BackColor = System.Drawing.Color.Transparent
        Me.oTask.Dock = System.Windows.Forms.DockStyle.Fill
        Me.oTask.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.oTask.ForeColor = System.Drawing.Color.Navy
        Me.HelpProvider1.SetHelpKeyword(Me.oTask, "packageTasks.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.oTask, System.Windows.Forms.HelpNavigator.Topic)
        Me.oTask.Location = New System.Drawing.Point(0, 0)
        Me.oTask.m_defaultTaks = False
        Me.oTask.m_eventBased = False
        Me.oTask.m_eventID = 99999
        Me.oTask.m_forExceptionHandling = False
        Me.oTask.m_showAfterType = True
        Me.oTask.m_showExpanded = True
        Me.oTask.Name = "oTask"
        Me.HelpProvider1.SetShowHelp(Me.oTask, True)
        Me.oTask.Size = New System.Drawing.Size(584, 396)
        Me.oTask.TabIndex = 0
        '
        'tabProperties
        '
        Me.tabProperties.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.tabProperties.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.tabProperties.ControlBox.MenuBox.Name = ""
        Me.tabProperties.ControlBox.Name = ""
        Me.tabProperties.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabProperties.ControlBox.MenuBox, Me.tabProperties.ControlBox.CloseBox})
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel4)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel1)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel3)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel10)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel5)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel2)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel9)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel7)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel6)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel8)
        Me.tabProperties.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabProperties.ForeColor = System.Drawing.Color.Black
        Me.tabProperties.Location = New System.Drawing.Point(0, 0)
        Me.tabProperties.Name = "tabProperties"
        Me.tabProperties.ReorderTabsEnabled = True
        Me.tabProperties.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.tabProperties.SelectedTabIndex = 7
        Me.tabProperties.Size = New System.Drawing.Size(720, 470)
        Me.tabProperties.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.tabProperties.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.tabProperties.TabIndex = 51
        Me.tabProperties.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabGeneral, Me.tabSchedule, Me.tabReports, Me.tabDestinations, Me.tabExceptionHandling, Me.tabHistory, Me.tabTasks, Me.tabLinking, Me.tabSnapshots, Me.tabDataDriver})
        Me.tabProperties.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        Me.tabProperties.Text = "stabMain"
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.btnXLSMerge)
        Me.SuperTabControlPanel3.Controls.Add(Me.btnPDFMerge)
        Me.SuperTabControlPanel3.Controls.Add(Me.chkMergeText)
        Me.SuperTabControlPanel3.Controls.Add(Me.FlowLayoutPanel1)
        Me.SuperTabControlPanel3.Controls.Add(Me.Label13)
        Me.SuperTabControlPanel3.Controls.Add(Me.lsvReports)
        Me.SuperTabControlPanel3.Controls.Add(Me.txtMergeTextName)
        Me.SuperTabControlPanel3.Controls.Add(Me.cmdAddReport)
        Me.SuperTabControlPanel3.Controls.Add(Me.chkMultithreaded)
        Me.SuperTabControlPanel3.Controls.Add(Me.cmdRemoveReport)
        Me.SuperTabControlPanel3.Controls.Add(Me.cmdEdit)
        Me.SuperTabControlPanel3.Controls.Add(Me.txtMergePDF)
        Me.SuperTabControlPanel3.Controls.Add(Me.cmdUp)
        Me.SuperTabControlPanel3.Controls.Add(Me.chkMergePDF)
        Me.SuperTabControlPanel3.Controls.Add(Me.chkMergeXL)
        Me.SuperTabControlPanel3.Controls.Add(Me.cmdDown)
        Me.SuperTabControlPanel3.Controls.Add(Me.txtMergeXL)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(584, 470)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.tabReports
        '
        'btnXLSMerge
        '
        Me.btnXLSMerge.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnXLSMerge.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnXLSMerge.Enabled = False
        Me.btnXLSMerge.Location = New System.Drawing.Point(476, 359)
        Me.btnXLSMerge.Name = "btnXLSMerge"
        Me.btnXLSMerge.Size = New System.Drawing.Size(75, 21)
        Me.btnXLSMerge.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnXLSMerge.TabIndex = 15
        Me.btnXLSMerge.Text = ".xls"
        '
        'btnPDFMerge
        '
        Me.btnPDFMerge.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnPDFMerge.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnPDFMerge.Enabled = False
        Me.btnPDFMerge.Location = New System.Drawing.Point(476, 330)
        Me.btnPDFMerge.Name = "btnPDFMerge"
        Me.btnPDFMerge.Size = New System.Drawing.Size(75, 21)
        Me.btnPDFMerge.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnPDFMerge.TabIndex = 15
        Me.btnPDFMerge.Text = ".pdf"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel1.Controls.Add(Me.grpSelectReport)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(584, 37)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'tabReports
        '
        Me.tabReports.AttachedControl = Me.SuperTabControlPanel3
        Me.tabReports.GlobalItem = False
        Me.tabReports.Image = CType(resources.GetObject("tabReports.Image"), System.Drawing.Image)
        Me.tabReports.Name = "tabReports"
        Me.tabReports.Text = "Reports"
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel4.Controls.Add(Me.grpAutoResume)
        Me.SuperTabControlPanel4.Controls.Add(Me.UcDest)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(584, 470)
        Me.SuperTabControlPanel4.TabIndex = 0
        Me.SuperTabControlPanel4.TabItem = Me.tabDestinations
        '
        'tabDestinations
        '
        Me.tabDestinations.AttachedControl = Me.SuperTabControlPanel4
        Me.tabDestinations.GlobalItem = False
        Me.tabDestinations.Image = CType(resources.GetObject("tabDestinations.Image"), System.Drawing.Image)
        Me.tabDestinations.Name = "tabDestinations"
        Me.tabDestinations.Text = "Destinations"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.Label2)
        Me.SuperTabControlPanel1.Controls.Add(Me.txtDesc)
        Me.SuperTabControlPanel1.Controls.Add(Me.Label7)
        Me.SuperTabControlPanel1.Controls.Add(Me.Label4)
        Me.SuperTabControlPanel1.Controls.Add(Me.cmdLoc)
        Me.SuperTabControlPanel1.Controls.Add(Me.txtName)
        Me.SuperTabControlPanel1.Controls.Add(Me.Label3)
        Me.SuperTabControlPanel1.Controls.Add(Me.txtKeyWord)
        Me.SuperTabControlPanel1.Controls.Add(Me.txtFolder)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(584, 470)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabGeneral
        '
        'tabGeneral
        '
        Me.tabGeneral.AttachedControl = Me.SuperTabControlPanel1
        Me.tabGeneral.GlobalItem = False
        Me.tabGeneral.Image = CType(resources.GetObject("tabGeneral.Image"), System.Drawing.Image)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Text = "General"
        '
        'SuperTabControlPanel10
        '
        Me.SuperTabControlPanel10.Controls.Add(Me.Step3)
        Me.SuperTabControlPanel10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel10.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel10.Name = "SuperTabControlPanel10"
        Me.SuperTabControlPanel10.Size = New System.Drawing.Size(584, 470)
        Me.SuperTabControlPanel10.TabIndex = 0
        Me.SuperTabControlPanel10.TabItem = Me.tabDataDriver
        '
        'tabDataDriver
        '
        Me.tabDataDriver.AttachedControl = Me.SuperTabControlPanel10
        Me.tabDataDriver.GlobalItem = False
        Me.tabDataDriver.Image = CType(resources.GetObject("tabDataDriver.Image"), System.Drawing.Image)
        Me.tabDataDriver.Name = "tabDataDriver"
        Me.tabDataDriver.Text = "Data-Driver"
        Me.tabDataDriver.Visible = False
        '
        'SuperTabControlPanel5
        '
        Me.SuperTabControlPanel5.Controls.Add(Me.UcBlank)
        Me.SuperTabControlPanel5.Controls.Add(Me.UcError)
        Me.SuperTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel5.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel5.Name = "SuperTabControlPanel5"
        Me.SuperTabControlPanel5.Size = New System.Drawing.Size(584, 470)
        Me.SuperTabControlPanel5.TabIndex = 0
        Me.SuperTabControlPanel5.TabItem = Me.tabExceptionHandling
        '
        'UcBlank
        '
        Me.UcBlank.BackColor = System.Drawing.Color.Transparent
        Me.UcBlank.blankID = 0
        Me.UcBlank.blankType = "AlertandReport"
        Me.UcBlank.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcBlank.Location = New System.Drawing.Point(0, 86)
        Me.UcBlank.m_customDSN = ""
        Me.UcBlank.m_customPassword = ""
        Me.UcBlank.m_customUserID = ""
        Me.UcBlank.m_showAllReportsBlankForTasks = False
        Me.UcBlank.Name = "UcBlank"
        Me.UcBlank.Size = New System.Drawing.Size(584, 384)
        Me.UcBlank.TabIndex = 7
        '
        'UcError
        '
        Me.UcError.BackColor = System.Drawing.Color.Transparent
        Me.UcError.Dock = System.Windows.Forms.DockStyle.Top
        Me.UcError.Location = New System.Drawing.Point(0, 0)
        Me.UcError.m_showFailOnOne = False
        Me.UcError.Name = "UcError"
        Me.UcError.Size = New System.Drawing.Size(584, 86)
        Me.UcError.TabIndex = 6
        '
        'tabExceptionHandling
        '
        Me.tabExceptionHandling.AttachedControl = Me.SuperTabControlPanel5
        Me.tabExceptionHandling.GlobalItem = False
        Me.tabExceptionHandling.Image = CType(resources.GetObject("tabExceptionHandling.Image"), System.Drawing.Image)
        Me.tabExceptionHandling.Name = "tabExceptionHandling"
        Me.tabExceptionHandling.Text = "Exception Handling"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.UcSched)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(584, 470)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.tabSchedule
        '
        'UcSched
        '
        Me.UcSched.BackColor = System.Drawing.Color.Transparent
        Me.UcSched.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcSched.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UcSched.Location = New System.Drawing.Point(0, 0)
        Me.UcSched.m_collaborationServerID = 0
        Me.UcSched.m_nextRun = "2012-02-28 10:39:44"
        Me.UcSched.m_RepeatUnit = ""
        Me.UcSched.mode = sqlrd.ucSchedule.modeEnum.EDIT
        Me.UcSched.Name = "UcSched"
        Me.UcSched.scheduleID = 0
        Me.UcSched.scheduleStatus = True
        Me.UcSched.sFrequency = "Daily"
        Me.UcSched.Size = New System.Drawing.Size(584, 470)
        Me.UcSched.TabIndex = 0
        '
        'tabSchedule
        '
        Me.tabSchedule.AttachedControl = Me.SuperTabControlPanel2
        Me.tabSchedule.GlobalItem = False
        Me.tabSchedule.Image = CType(resources.GetObject("tabSchedule.Image"), System.Drawing.Image)
        Me.tabSchedule.Name = "tabSchedule"
        Me.tabSchedule.Text = "Schedule"
        '
        'SuperTabControlPanel9
        '
        Me.SuperTabControlPanel9.Controls.Add(Me.Panel1)
        Me.SuperTabControlPanel9.Controls.Add(Me.FlowLayoutPanel6)
        Me.SuperTabControlPanel9.Controls.Add(Me.FlowLayoutPanel5)
        Me.SuperTabControlPanel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel9.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel9.Name = "SuperTabControlPanel9"
        Me.SuperTabControlPanel9.Size = New System.Drawing.Size(584, 470)
        Me.SuperTabControlPanel9.TabIndex = 0
        Me.SuperTabControlPanel9.TabItem = Me.tabSnapshots
        '
        'FlowLayoutPanel6
        '
        Me.FlowLayoutPanel6.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel6.Controls.Add(Me.cmdView)
        Me.FlowLayoutPanel6.Controls.Add(Me.cmdDelete)
        Me.FlowLayoutPanel6.Controls.Add(Me.cmdExecute)
        Me.FlowLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel6.Location = New System.Drawing.Point(0, 437)
        Me.FlowLayoutPanel6.Name = "FlowLayoutPanel6"
        Me.FlowLayoutPanel6.Size = New System.Drawing.Size(584, 33)
        Me.FlowLayoutPanel6.TabIndex = 17
        '
        'FlowLayoutPanel5
        '
        Me.FlowLayoutPanel5.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel5.Controls.Add(Me.chkSnapshots)
        Me.FlowLayoutPanel5.Controls.Add(Me.txtSnapshots)
        Me.FlowLayoutPanel5.Controls.Add(Me.Label11)
        Me.FlowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel5.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
        Me.FlowLayoutPanel5.Size = New System.Drawing.Size(584, 31)
        Me.FlowLayoutPanel5.TabIndex = 0
        '
        'tabSnapshots
        '
        Me.tabSnapshots.AttachedControl = Me.SuperTabControlPanel9
        Me.tabSnapshots.GlobalItem = False
        Me.tabSnapshots.Image = CType(resources.GetObject("tabSnapshots.Image"), System.Drawing.Image)
        Me.tabSnapshots.Name = "tabSnapshots"
        Me.tabSnapshots.Text = "Snapshots"
        '
        'SuperTabControlPanel7
        '
        Me.SuperTabControlPanel7.Controls.Add(Me.oTask)
        Me.SuperTabControlPanel7.Controls.Add(Me.grpDynamicTasks)
        Me.SuperTabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel7.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel7.Name = "SuperTabControlPanel7"
        Me.SuperTabControlPanel7.Size = New System.Drawing.Size(584, 470)
        Me.SuperTabControlPanel7.TabIndex = 0
        Me.SuperTabControlPanel7.TabItem = Me.tabTasks
        '
        'tabTasks
        '
        Me.tabTasks.AttachedControl = Me.SuperTabControlPanel7
        Me.tabTasks.GlobalItem = False
        Me.tabTasks.Image = CType(resources.GetObject("tabTasks.Image"), System.Drawing.Image)
        Me.tabTasks.Name = "tabTasks"
        Me.tabTasks.Text = "Tasks"
        '
        'SuperTabControlPanel6
        '
        Me.SuperTabControlPanel6.Controls.Add(Me.UcHistory)
        Me.SuperTabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel6.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel6.Name = "SuperTabControlPanel6"
        Me.SuperTabControlPanel6.Size = New System.Drawing.Size(584, 470)
        Me.SuperTabControlPanel6.TabIndex = 0
        Me.SuperTabControlPanel6.TabItem = Me.tabHistory
        '
        'UcHistory
        '
        Me.UcHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcHistory.Location = New System.Drawing.Point(0, 0)
        Me.UcHistory.m_filter = False
        Me.UcHistory.m_historyLimit = 0
        Me.UcHistory.m_objectID = 0
        Me.UcHistory.m_scheduleType = sqlrd.clsMarsScheduler.enScheduleType.PACKAGE
        Me.UcHistory.m_showControls = False
        Me.UcHistory.m_showSchedulesList = False
        Me.UcHistory.m_sortOrder = " ASC "
        Me.UcHistory.Name = "UcHistory"
        Me.UcHistory.Size = New System.Drawing.Size(584, 470)
        Me.UcHistory.TabIndex = 0
        '
        'tabHistory
        '
        Me.tabHistory.AttachedControl = Me.SuperTabControlPanel6
        Me.tabHistory.GlobalItem = False
        Me.tabHistory.Image = CType(resources.GetObject("tabHistory.Image"), System.Drawing.Image)
        Me.tabHistory.Name = "tabHistory"
        Me.tabHistory.Text = "History"
        '
        'SuperTabControlPanel8
        '
        Me.SuperTabControlPanel8.Controls.Add(Me.GroupBox6)
        Me.SuperTabControlPanel8.Controls.Add(Me.FlowLayoutPanel4)
        Me.SuperTabControlPanel8.Controls.Add(Me.FlowLayoutPanel3)
        Me.SuperTabControlPanel8.Controls.Add(Me.FlowLayoutPanel2)
        Me.SuperTabControlPanel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel8.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel8.Name = "SuperTabControlPanel8"
        Me.SuperTabControlPanel8.Size = New System.Drawing.Size(584, 470)
        Me.SuperTabControlPanel8.TabIndex = 0
        Me.SuperTabControlPanel8.TabItem = Me.tabLinking
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel4.Controls.Add(Me.chkStaticDest)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(0, 57)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(584, 29)
        Me.FlowLayoutPanel4.TabIndex = 1
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel3.Controls.Add(Me.optStatic)
        Me.FlowLayoutPanel3.Controls.Add(Me.optDynamic)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnView)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(0, 28)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(584, 29)
        Me.FlowLayoutPanel3.TabIndex = 0
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel2.Controls.Add(Me.Label23)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmbKey)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(584, 28)
        Me.FlowLayoutPanel2.TabIndex = 0
        '
        'tabLinking
        '
        Me.tabLinking.AttachedControl = Me.SuperTabControlPanel8
        Me.tabLinking.GlobalItem = False
        Me.tabLinking.Image = CType(resources.GetObject("tabLinking.Image"), System.Drawing.Image)
        Me.tabLinking.Name = "tabLinking"
        Me.tabLinking.Text = "Linking"
        Me.tabLinking.Visible = False
        '
        'FlowLayoutPanel7
        '
        Me.FlowLayoutPanel7.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel7.Controls.Add(Me.cmdApply)
        Me.FlowLayoutPanel7.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel7.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel7.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel7.Location = New System.Drawing.Point(0, 470)
        Me.FlowLayoutPanel7.Name = "FlowLayoutPanel7"
        Me.FlowLayoutPanel7.Size = New System.Drawing.Size(720, 30)
        Me.FlowLayoutPanel7.TabIndex = 52
        '
        'frmPackageProp
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(720, 500)
        Me.Controls.Add(Me.tabProperties)
        Me.Controls.Add(Me.FlowLayoutPanel7)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.HelpButton = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPackageProp"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Package Properties"
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.grpSelectReport.ResumeLayout(False)
        Me.grpSelectReport.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.txtSnapshots, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step3.ResumeLayout(False)
        Me.Step3.PerformLayout()
        Me.grpQuery.ResumeLayout(False)
        Me.grpQuery.PerformLayout()
        Me.grpDynamicTasks.ResumeLayout(False)
        Me.grpDynamicTasks.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtAdjustStamp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpAutoResume.ResumeLayout(False)
        Me.grpAutoResume.PerformLayout()
        CType(Me.txtCacheExpiry, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tabProperties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabProperties.ResumeLayout(False)
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel4.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel10.ResumeLayout(False)
        Me.SuperTabControlPanel5.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel9.ResumeLayout(False)
        Me.FlowLayoutPanel6.ResumeLayout(False)
        Me.FlowLayoutPanel5.ResumeLayout(False)
        Me.SuperTabControlPanel7.ResumeLayout(False)
        Me.SuperTabControlPanel6.ResumeLayout(False)
        Me.SuperTabControlPanel8.ResumeLayout(False)
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel7.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private ReadOnly Property m_autoCalcValue() As Decimal
        Get
            Dim val As Decimal = clsMarsScheduler.globalItem.getScheduleDuration(0, Me.txtID.Text, 0, 0, 0, clsMarsScheduler.enCalcType.MEDIAN, 3)

            Return val
        End Get
    End Property
    Dim txtID As New TextBox

    Private Sub frmPackageProp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        On Error Resume Next
        HelpProvider1.HelpNamespace = gHelpPath

        UcDest.isPackage = True

        FormatForWinXP(Me)

        setupForDragAndDrop(txtMergePDF)
        setupForDragAndDrop(txtMergeXL)
        setupForDragAndDrop(txtMergeTextName)

        setupForDragAndDrop(txtMergeAllPDF)
        setupForDragAndDrop(txtMergePDF)

        UcHistory.DrawScheduleHistory(clsMarsScheduler.enScheduleType.PACKAGE, txtID.Text)

        tabProperties.SelectedTab = tabGeneral
        IsLoaded = True
    End Sub
    Public Function _GetParameters(ByVal cmb As ComboBox)

    End Function

    Private Sub ViewSnapshots()
        Try
            Dim SQL As String
            Dim parentNode As TreeNode
            Dim oRs As ADODB.Recordset

            SQL = "SELECT * FROM ReportSnapshots WHERE PackID = " & txtID.Text

            oRs = clsMarsData.GetData(SQL)

            If oRs.EOF = False Then
                chkSnapshots.Checked = True
                txtSnapshots.Value = oRs("keepsnap").Value
            End If

            oRs.Close()

            parentNode = tvPacks.Nodes.Add("Snapshots")
            parentNode.Tag = 0
            parentNode.ImageIndex = 6
            parentNode.SelectedImageIndex = 6

            SQL = "SELECT DISTINCT ExecID FROM SnapshotsAttr WHERE PackID = " & txtID.Text

            oRs = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then Return

            Do While oRs.EOF = False
                Dim nodeItem As TreeNode = parentNode.Nodes.Add("InstanceID: " & oRs("execid").Value)
                nodeItem.Tag = oRs("execid").Value
                nodeItem.ImageIndex = 0
                nodeItem.SelectedImageIndex = 0

                oRs.MoveNext()
            Loop

            oRs.Close()

            parentNode.Expand()
        Catch : End Try
    End Sub

    Public Function EditPackage(ByVal nPackID As Int32)
        'On Error Resume Next
        Dim SQL As String
        Dim sWhere As String
        Dim rs As ADODB.Recordset
        Dim I As Integer = 0
        Dim rsP As ADODB.Recordset
        Dim autoCalc As Boolean

        Try
            chkMultithreaded.Checked = False
            If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.sa1_MultiThreading) = False Then
                chkMultithreaded.Enabled = False
            End If

            sWhere = " WHERE PackID = " & nPackID

            SQL = "SELECT * FROM PackageAttr " & sWhere

            rs = clsMarsData.GetData(SQL)

            If rs.EOF = False Then

                txtID.Text = rs("packid").Value
                owner = rs("owner").Value

                UcDest.nReportID = 0
                UcDest.nSmartID = 0
                UcDest.nPackID = txtID.Text
                UcDest.LoadAll()
                autoCalc = IsNull(rs("autocalc").Value, 0)

                txtName.Text = rs("packagename").Value

                Me.Text = "Package Properties - " & txtName.Text

                txtFolder.Tag = rs("parent").Value

                rsP = clsMarsData.GetData("SELECT FolderName FROM Folders WHERE FolderID = " & rs("Parent").Value)

                txtFolder.Text = rsP("FolderName").Value

                rsP.Close()

                Dim sDestination

                rsP = clsMarsData.GetData("SELECT DestinationType FROM DestinationAttr WHERE " & _
                "PackID = " & nPackID)

                Try
                    If rsP.EOF = False Then
                        sDestination = rsP.Fields(0).Value
                    End If
                    rsP.Close()

                    Select Case sDestination.ToLower
                        Case "email"
                            Label21.Text = Label21.Text.Replace("xxx", "email address")
                        Case "disk"
                            Label21.Text = Label21.Text.Replace("xxx", "directory")
                        Case "printer"
                            Label21.Text = Label21.Text.Replace("xxx", "printer name")
                        Case "ftp"
                            Label21.Text = Label21.Text.Replace("xxx", "FTP Server")
                    End Select
                Catch : End Try

                Try
                    UcError.cmbRetry.Text = rs("retry").Value
                Catch
                    UcError.cmbRetry.Value = 3
                End Try

                Try
                    UcError.m_autoFailAfter = IIf(rs("assumefail").Value < 2, UcError.m_autoFailAfter = 30, rs("assumefail").Value)
                Catch
                    UcError.m_autoFailAfter = 30
                End Try

                UcError.chkAssumeFail.Checked = True

                UcError.chkAutoCalc.Checked = autoCalc

                If autoCalc = True Then
                    Dim temp As Decimal = Me.m_autoCalcValue

                    UcError.chkAutoCalc.Checked = True
                    UcError.m_autoFailAfter = IIf(temp < 1, 1, temp)
                    UcError.cmbAssumeFail.Enabled = False
                End If

                UcError.txtRetryInterval.Value = IsNull(rs("retryinterval").Value, 0)

                UcError.chkFailonOne.Checked = Convert.ToBoolean(rs("failonone").Value)

                Try
                    chkMergePDF.Checked = Convert.ToBoolean(rs("mergepdf").Value)
                    txtMergePDF.Text = rs("mergepdfname").Value
                Catch : End Try

                Try
                    chkMergeXL.Checked = rs("mergexl").Value
                    txtMergeXL.Text = rs("mergexlname").Value
                Catch : End Try

                Try
                    chkMergeText.Checked = rs("mergetext").Value
                    txtMergeTextName.Text = rs("mergetextname").Value
                Catch : End Try

                If IsNull(rs("dynamictasks").Value, 0) = 1 Then
                    optAll.Checked = True
                Else
                    optOnce.Checked = True
                End If

                UcBlank.chkCheckBlankReport.Checked = rs("checkblank").Value

                Try
                    chkDTStamp.Checked = Convert.ToBoolean(rs("datetimestamp").Value)
                Catch : chkDTStamp.Checked = False : End Try

                cmbDateTime.Text = IsNull(rs("stampformat").Value)

                Try
                    IsDynamic = Convert.ToBoolean(rs("dynamic").Value)
                Catch ex As Exception
                    IsDynamic = False
                End Try

                If IsNull(rs("isdatadriven").Value, "0") = "1" Then
                    IsDataDriven = True
                Else
                    IsDataDriven = False
                End If

                Try
                    Me.chkStaticDest.Checked = Convert.ToBoolean(rs("staticdest").Value)
                Catch ex As Exception
                    Me.chkStaticDest.Checked = False
                End Try

                txtAdjustStamp.Value = IsNull(rs("adjustpackagestamp").Value, 0)

                If (IsDynamic = False And IsDataDriven = False) Then
                    Try
                        If (chkMultithreaded.Enabled = True) Then
                            chkMultithreaded.Checked = rs("multithreaded").Value
                        End If
                    Catch : End Try
                Else
                    chkMultithreaded.Visible = False
                End If

            End If

            rs.Close()

            UcDest.isDynamic = IsDynamic

            If IsDynamic = True Then
                Me.Text = "Dynamic Package Properties"
                Me.grpAutoResume.Enabled = True
            ElseIf IsDataDriven Then
                Me.Text = "Data-Driven Package Properties"
                Me.grpAutoResume.Enabled = False
            Else
                Me.grpAutoResume.Enabled = False
            End If

            'get the blank report attributes
            UcBlank.getInfo(txtID.Text, clsMarsScheduler.enScheduleType.PACKAGE)

            UcSched.mode = ucSchedule.modeEnum.EDIT
            UcSched.loadScheduleInfo(txtID.Text, clsMarsScheduler.enScheduleType.PACKAGE, txtDesc, txtKeyWord, oTask)

            oTask.LoadTasks()

            _LoadReports()

            'dynamic package details
            If IsDynamic = True Then

                grpDynamicTasks.Enabled = True

                tabLinking.Visible = True
                tabDataDriver.Visible = False

                SQL = "SELECT * FROM DynamicLink WHERE PackID =" & nPackID

                rs = clsMarsData.GetData(SQL)

                If Not rs Is Nothing Then
                    If rs.EOF = False Then
                        With UcDSN
                            .cmbDSN.Text = Convert.ToString(rs("constring").Value).Split("|")(0)
                            .txtUserID.Text = Convert.ToString(rs("constring").Value).Split("|")(1)
                            .txtPassword.Text = Convert.ToString(rs("constring").Value).Split("|")(2)
                            gsCon = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & .txtPassword.Text
                        End With


                        cmbTable.Text = Convert.ToString(rs("keycolumn").Value).Split(".")(0)
                        cmbColumn.Text = Convert.ToString(rs("keycolumn").Value).Split(".")(1)

                        Dim valueColumn As String = IsNull(rs("valuecolumn").Value)
                        Dim valueTable As String = ""

                        Try
                            valueTable = valueColumn.Split(".")(0)
                        Catch ex As Exception
                            valueTable = cmbTable.Text
                        End Try

                        If valueTable = cmbTable.Text Then
                            Try
                                cmbValue.Text = Convert.ToString(rs("valuecolumn").Value).Split(".")(1)
                            Catch
                                cmbValue.Text = Convert.ToString(rs("valuecolumn").Value)
                            End Try
                        Else
                            cmbValue.Text = "<Advanced>"
                        End If

                        gTables(0) = cmbTable.Text

                        If IsNull(rs("table2").Value).Length > 0 Then _
                            gTables(1) = rs("table2").Value

                    End If

                    rs.Close()

                    rs = clsMarsData.GetData("SELECT KeyParameter, Query,AutoResume,ExpireCacheAfter FROM DynamicAttr WHERE " & _
                    "PackID = " & nPackID)

                    If Not rs Is Nothing Then
                        If rs.EOF = False Then
                            cmbKey.Text = rs("keyparameter").Value
                            Me.chkAutoResume.Checked = Convert.ToBoolean(Convert.ToInt32(IsNull(rs("autoresume").Value, 0)))
                            Me.txtCacheExpiry.Value = IsNull(rs("expirecacheafter").Value, 0)

                            Try
                                If rs("query").Value = "_" Then
                                    optStatic.Checked = True
                                Else
                                    optDynamic.Checked = True
                                End If
                            Catch ex As Exception
                                optStatic.Checked = True
                            End Try
                        End If
                    End If

                End If

            End If

            Try
                'add data-driven schedule data
3120:           If IsDataDriven = True Then
3130:               UcError.m_showFailOnOne = True
3140:               grpDynamicTasks.Enabled = True
3150:               Me.UcDest.isDataDriven = True
                    Me.tabSnapshots.Visible = False
                    Me.tabLinking.Visible = False
                    tabDataDriver.Visible = True

3160:               Text = "Data-Driven Package Properties"

3170:               rs = clsMarsData.GetData("SELECT * FROM DataDrivenAttr WHERE PackID =" & nPackID)

3180:               If rs IsNot Nothing Then
3190:                   If rs.EOF = False Then
3200:                       tabDataDriver.Visible = True
3210:                       tabSnapshots.Visible = False

                            Dim sQuery As String
                            Dim conString As String

3220:                       sQuery = rs("sqlquery").Value
3230:                       conString = rs("constring").Value

3240:                       txtQuery.Text = sQuery

3250:                       Try
3260:                           UcError.chkFailonOne.Checked = IsNull(rs("failonone").Value, 0)
3270:                       Catch ex As Exception
3280:                           UcError.chkFailonOne.Checked = False
                            End Try

3290:                       clsMarsReport.populateDataDrivenCache(sQuery, conString, True)

                            Dim DSN, sUser, sPassword As String

3300:                       DSN = conString.Split("|")(0)
3310:                       sUser = conString.Split("|")(1)
3320:                       sPassword = _DecryptDBValue(conString.Split("|")(2))

3330:                       With UcDSNDD
3340:                           .cmbDSN.Text = DSN
3350:                           .txtUserID.Text = sUser
3360:                           .txtPassword.Text = sPassword
                            End With

                            Try
                                Me.chkGroupReports.Checked = IsNull(rs("groupreports").Value, 1)
                            Catch
                                Me.chkGroupReports.Checked = True
                            End Try

                            Try
                                Me.chkMergeAllPDF.Checked = IsNull(rs("mergeallpdf").Value, 0)
                                Me.txtMergeAllPDF.Text = IsNull(rs("mergeallpdfname").Value)
                            Catch : End Try

                            Try
                                Me.chkMergeAllXL.Checked = IsNull(rs("mergeallxl").Value, 0)
                                Me.txtMergeAllXL.Text = IsNull(rs("mergeallxlname").Value)
                            Catch : End Try

3370:                       Dim rsDD As ADODB.Recordset = New ADODB.Recordset
3380:                       Dim ddCon As ADODB.Connection = New ADODB.Connection

3390:                       ddCon.Open(DSN, sUser, sPassword)

3400:                       rsDD.Open(sQuery, ddCon)

                            Dim z As Integer = 0

                            Me.cmbDDKey.Items.Clear()

3410:                       frmRDScheduleWizard.m_DataFields = New Hashtable

3420:                       For Each fld As ADODB.Field In rsDD.Fields
3430:                           frmRDScheduleWizard.m_DataFields.Add(z, fld.Name)
                                Me.cmbDDKey.Items.Add(fld.Name)
3440:                           z += 1
3450:                       Next

                            Try
                                Me.cmbDDKey.Text = IsNull(rs("keycolumn").Value)
                            Catch : End Try

3460:                       rsDD.Close()

3470:                       ddCon.Close()
3480:                       ddCon = Nothing
                        End If

3490:                   rs.Close()
                    End If
                End If
            Catch : End Try

            ViewSnapshots()

            Me.ShowDialog(oMain)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Function

    Private Function _LoadReports()
        Dim SQL As String
        Dim nPackID As Integer = txtID.Text
        Dim rs As ADODB.Recordset

        SQL = "SELECT r.ReportTitle, r.ReportID, p.OutputFormat, p.Status FROM " & _
            "ReportAttr r INNER JOIN " & _
            "PackagedReportAttr p ON r.ReportID = p.ReportID WHERE " & _
            "r.PackID =" & nPackID

        SQL &= " ORDER BY PackOrderID"

        rs = clsMarsData.GetData(SQL)

        Dim oListItem As ListViewItem

        If Not rs Is Nothing Then

            lsvReports.Items.Clear()

            Do While rs.EOF = False
                oListItem = lsvReports.Items.Add(rs("reporttitle").Value)

                oListItem.Tag = rs("reportid").Value

                oListItem.SubItems.Add(rs("outputformat").Value)

                If IsNull(rs("status").Value, "0") = "1" Then
                    oListItem.Checked = True
                Else
                    oListItem.Checked = False
                    oListItem.ForeColor = System.Drawing.Color.Gray
                End If

                rs.MoveNext()
            Loop
        End If

        rs.Close()
    End Function

    Private Sub lsvReports_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvReports.DoubleClick
        On Error Resume Next
        Dim olsv As ListViewItem
        Dim sNewName As String = ""
        Dim sVals As String()

        If lsvReports.SelectedItems.Count = 0 Then Exit Sub


        Dim frmPackProp As frmPackedReport = New frmPackedReport
        frmPackProp.IsDataDriven = Me.IsDataDriven

        olsv = lsvReports.SelectedItems(0)

        sVals = frmPackProp.EditReport(olsv.Tag)

        If Not sVals Is Nothing Then
            Dim oItem As ListViewItem = lsvReports.SelectedItems(0)

            oItem.Text = sVals(0)
            oItem.SubItems(1).Text = sVals(1)
        End If

        _LoadReports()

        For Each it As ListViewItem In lsvReports.Items
            If it.Tag = olsv.Tag Then
                it.Selected = True
                it.EnsureVisible()
            End If
        Next
    End Sub

    Private Sub cmdApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdApply.Click
        On Error Resume Next
        Dim sWhere As String
        Dim SQL As String
        Dim nRepeat As Double = 0
        Dim sPrinters As String = ""

        'validate user entry

        If txtName.Text = "" Then
            ep.SetError(txtName, "Please enter a name for the package")
            txtName.Focus()

            tabProperties.SelectedTabIndex = 0
            OkayToClose = False
            Exit Sub
        ElseIf clsMarsUI.candoRename(txtName.Text, Me.txtFolder.Tag, clsMarsScheduler.enScheduleType.PACKAGE, txtID.Text, IsDynamic, IsDataDriven) = False Then
            ep.SetError(txtName, "A package with this name already exists in this folder. Please use a different name.")
            txtName.Focus()
            tabProperties.SelectedTabIndex = 0
            OkayToClose = False
            Return
        ElseIf txtFolder.Text = "" Then

            ep.SetError(txtFolder, "Please select the parent folder")
            txtFolder.Focus()
            tabProperties.SelectedTabIndex = 0
            OkayToClose = False
            Exit Sub

        ElseIf UcError.chkAssumeFail.Checked = True And UcError.cmbAssumeFail.Text = "" Then
            tabProperties.SelectedTabIndex = 4
            ep.SetError(UcError.cmbAssumeFail, "Please select the amount before a schedule is assumed as failed")
            UcError.cmbAssumeFail.Focus()
            OkayToClose = False
            Exit Sub
        ElseIf UcDest.lsvDestination.Nodes.Count = 0 Then
            UcDest.ep.SetError(UcDest.lsvDestination, "Please add at least one destination")
            tabProperties.SelectedTabIndex = 3
            OkayToClose = False
            Return
        ElseIf chkMergePDF.Checked = True And txtMergePDF.Text.Length = 0 Then
            ep.SetError(txtMergePDF, "Please enter the name of the resulting PDF file")
            tabProperties.SelectedTabIndex = 2
            txtMergePDF.Focus()
            OkayToClose = False
            Return
        ElseIf chkMergeXL.Checked = True And txtMergeXL.Text.Length = 0 Then
            ep.SetError(txtMergeXL, "Please enter the name of the resulting Excel file")
            tabProperties.SelectedTabIndex = 2
            txtMergeXL.Focus()
            OkayToClose = False
            Return
        ElseIf UcError.chkAssumeFail.Checked = True And UcError.cmbAssumeFail.Value = 0 Then
            ep.SetError(UcError.cmbAssumeFail, "Please provide a value greater than zero")
            tabProperties.SelectedTabIndex = 4
            UcError.cmbAssumeFail.Focus()
            OkayToClose = False
            Return
        ElseIf Me.UcSched.isAllDataValid = False Then
            OkayToClose = False
            Return
        ElseIf Me.chkDTStamp.Checked And Me.cmbDateTime.Text = "" Then
            ep.SetError(Me.cmbDateTime, "Please specify the date/time format for the stamp")
            Me.cmbDateTime.Focus()
            tabProperties.SelectedTab = Me.tabDestinations
            Return
        ElseIf IsDynamic = True Then
            If Me.chkStaticDest.Checked = False Then
                If cmbTable.Text.Length = 0 Then
                    ep.SetError(cmbTable, "Please select the table that holds the value that must match the key parameter")
                    tabProperties.SelectedTab = Me.tabLinking
                    cmbTable.Focus()
                    Return
                ElseIf cmbColumn.Text.Length = 0 Then
                    ep.SetError(cmbColumn, "Please select the column that holds the value that must match the key parameter")
                    tabProperties.SelectedTab = Me.tabLinking
                    cmbColumn.Focus()
                    Return
                ElseIf cmbValue.Text.Length = 0 Then
                    ep.SetError(cmbValue, "Please select the column that will provide the report destination")
                    tabProperties.SelectedTab = Me.tabLinking
                    cmbValue.Focus()
                    Return
                ElseIf UcDSN._Validate = False Then
                    tabProperties.SelectedTab = Me.tabLinking
                    UcDSN.cmbDSN.Focus()
                    Return
                End If
            End If

            If cmbKey.Text.Length = 0 Then
                ep.SetError(cmbKey, "Please select the key parameter")
                cmbKey.Focus()
                Me.tabProperties.SelectedTab = Me.tabLinking
                Return
            ElseIf Me.optDynamic.Checked = False And Me.optStatic.Checked = False Then
                ep.SetError(optStatic, "Please select how the key parameter value is populated")
                optStatic.Focus()
                Return
            End If
        End If

        Dim enabledCount As Integer = 0

        For Each nn As DevComponents.AdvTree.Node In UcDest.lsvDestination.Nodes
            If nn.Checked = True Then
                enabledCount += 1
            End If
        Next

        If enabledCount = 0 Then
            ep.SetError(UcDest.lsvDestination, "Please enable at least one destination before saving")
            tabProperties.SelectedTab = tabDestinations
            OkayToClose = False
            Return
        End If

        OkayToClose = True

        sWhere = " WHERE PackID = " & txtID.Text

        Dim dynamicTasks As Integer

        If optAll.Checked Then
            dynamicTasks = 1
        Else
            dynamicTasks = 0
        End If

        SQL = "UPDATE PackageAttr SET " & _
        "PackageName = '" & txtName.Text.Replace("'", "''") & "'," & _
        "Parent = " & txtFolder.Tag & "," & _
        "Retry = " & UcError.cmbRetry.Text.Replace("'", "''") & "," & _
        "AssumeFail = " & UcError.m_autoFailAfter & "," & _
        "CheckBlank = " & Convert.ToInt32(UcBlank.chkCheckBlankReport.Checked) & "," & _
        "FailOnOne = " & Convert.ToInt32(UcError.chkFailonOne.Checked) & "," & _
        "MergePDF = " & Convert.ToInt32(chkMergePDF.Checked) & "," & _
        "MergeXL = " & Convert.ToInt32(chkMergeXL.Checked) & "," & _
        "MergePDFName = '" & SQLPrepare(txtMergePDF.Text) & "'," & _
        "MergeXLName = '" & SQLPrepare(txtMergeXL.Text) & "'," & _
        "DateTimeStamp =" & Convert.ToInt32(chkDTStamp.Checked) & "," & _
        "StampFormat ='" & SQLPrepare(cmbDateTime.Text) & "'," & _
        "StaticDest =" & Convert.ToInt32(Me.chkStaticDest.Checked) & ", " & _
        "AdjustPackageStamp =" & txtAdjustStamp.Value & ", " & _
        "DynamicTasks = " & dynamicTasks & "," & _
        "AutoCalc =" & Convert.ToInt32(UcError.chkAutoCalc.Checked) & "," & _
        "RetryInterval =" & UcError.txtRetryInterval.Value & "," & _
        "Multithreaded = " & Convert.ToInt32(chkMultithreaded.Checked) & "," & _
        "MergeText =" & Convert.ToInt32(chkMergeText.Checked) & "," & _
        "MergeTextName ='" & SQLPrepare(txtMergeTextName.Text) & "' "

        SQL &= sWhere

        clsMarsData.WriteData(SQL)


        UcSched.updateSchedule(txtID.Text, clsMarsScheduler.enScheduleType.PACKAGE, txtDesc.Text, txtKeyWord.Text)

        UcBlank.saveInfo(txtID.Text, clsMarsScheduler.enScheduleType.PACKAGE)        'save the blank report attributes

        'for dynamic
        'for dynamic data
        Dim ValueCol As String
        Dim LinkSQL As String

        If cmbValue.Text.ToLower = "<advanced>" Then
            ValueCol = "<Advanced>"
        Else
            ValueCol = cmbTable.Text & "." & cmbValue.Text
        End If

        SQL = "UPDATE DynamicAttr SET " & _
         "KeyParameter ='" & SQLPrepare(cmbKey.Text) & "', " & _
         "Autoresume = " & Convert.ToInt32(Me.chkAutoResume.Checked) & "," & _
         "ExpireCacheAfter = " & Me.txtCacheExpiry.Value & _
         " WHERE PackID =" & txtID.Text

        clsMarsData.WriteData(SQL)

        If ValueCol <> "<Advanced>" Then
            LinkSQL = "SELECT DISTINCT " & cmbTable.Text & "." & cmbValue.Text & " FROM " & cmbTable.Text

            SQL = "UPDATE DynamicLink SET " & _
            "KeyColumn ='" & SQLPrepare(cmbTable.Text & "." & cmbColumn.Text) & "'," & _
            "KeyParameter ='" & SQLPrepare(cmbKey.Text) & "'," & _
            "LinkSQL ='" & SQLPrepare(LinkSQL) & "'," & _
            "ConString ='" & SQLPrepare(UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & UcDSN.txtPassword.Text & "|") & "'," & _
            "ValueColumn ='" & SQLPrepare(ValueCol) & "' WHERE " & _
            "PackID =" & txtID.Text

            clsMarsData.WriteData(SQL)
        End If

        With UcDSN
            gsCon = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & .txtPassword.Text
        End With

        For Each oItem As ListViewItem In lsvReports.Items
            SQL = "UPDATE PackagedReportAttr SET Status =" & Convert.ToInt32(oItem.Checked) & " WHERE ReportID = " & oItem.Tag

            clsMarsData.WriteData(SQL)
        Next

        If IsDataDriven = True Then
            If Me.cmbDDKey.Text = "" Then
                ep.SetError(Me.cmbDDKey, "Please select the key column")
                Me.cmbDDKey.Focus()

                Me.tabProperties.SelectedTab = Me.tabDataDriver
                Return
            ElseIf Me.chkMergeAllPDF.Checked And Me.txtMergeAllPDF.Text = "" Then
                ep.SetError(Me.txtMergeAllPDF, "Please provide a name of the merged PDF file")
                Me.tabProperties.SelectedTab = Me.tabDataDriver
                Me.txtMergeAllPDF.Focus()
                Return
            ElseIf Me.chkMergeAllXL.Checked And Me.txtMergeAllXL.Text = "" Then
                ep.SetError(Me.txtMergeAllXL, "Please provide a name of the merged Excel file")
                Me.tabProperties.SelectedTab = Me.tabDataDriver
                Me.txtMergeAllXL.Focus()
                Return
            End If

            'check if the selected key column contains unique values only. Going to do this by reading from the data cache

            For Each row As DataRow In clsMarsReport.m_dataDrivenCache.Rows
                Dim dupKey As String = IsNull(row(cmbDDKey.Text), "")
                Dim rows() As DataRow

                'use select to get all the rows where the key column matches the dupKey
                rows = clsMarsReport.m_dataDrivenCache.Select(Me.cmbDDKey.Text & " = '" & dupKey & "'")

                If rows IsNot Nothing Then
                    If rows.Length > 1 Then
                        ep.SetError(cmbDDKey, "The selected key column contains duplicate values of '" & dupKey & "'. Please select a column that contains UNIQUE values only.")
                        cmbDDKey.Focus()
                        Me.tabProperties.SelectedTab = Me.tabDataDriver
                        Return
                    End If
                End If
            Next

            SQL = "UPDATE DataDrivenAttr SET " & _
            "SQLQuery = '" & SQLPrepare(txtQuery.Text) & "'," & _
            "ConString = '" & SQLPrepare(Me.UcDSNDD.m_conString) & "', " & _
            "FailOnOne = " & Convert.ToInt32(UcError.chkFailonOne.Checked) & ", " & _
            "GroupReports = " & Convert.ToInt32(Me.chkGroupReports.Checked) & ", " & _
            "KeyColumn = '" & SQLPrepare(Me.cmbDDKey.Text) & "', " & _
            "MergeAllPDF =" & Convert.ToInt32(Me.chkMergeAllPDF.Checked) & "," & _
            "MergeAllPDFName = '" & SQLPrepare(Me.txtMergeAllPDF.Text) & "'," & _
            "MergeAllXL = " & Convert.ToInt32(Me.chkMergeAllXL.Checked) & "," & _
            "MergeAllXLName = '" & SQLPrepare(Me.txtMergeAllXL.Text) & "' " & _
            "WHERE PackID =" & txtID.Text

            clsMarsData.WriteData(SQL)
        End If

        'snapshots
        If chkSnapshots.Checked = True Then
            clsMarsData.WriteData("DELETE FROM ReportSnapshots WHERE PackID =" & txtID.Text)

            SQL = "INSERT INTO ReportSnapshots (SnapID,PackID,KeepSnap) "

            SQL &= "VALUES (" & clsMarsData.CreateDataID("reportsnapshots", "snapid") & "," & _
            txtID.Text & "," & _
            txtSnapshots.Value & ")"

            clsMarsData.WriteData(SQL)
        Else
            clsMarsData.WriteData("DELETE FROM ReportSnapshots WHERE PackID =" & txtID.Text)

            Dim rsd As ADODB.Recordset

            rsd = clsMarsData.GetData("SELECT SnapPath FROM SnapshotsAttr WHERE PackID =" & txtID.Text)

            Do While rsd.EOF = False

                System.IO.File.Delete(rsd("snappath").Value)

                rsd.MoveNext()
            Loop

            rsd.Close()


            clsMarsData.WriteData("DELETE FROM SnapshotsAttr WHERE PackID =" & txtID.Text)
        End If

        setAndShowMessageOnControl(cmdApply, "Saved!", "", "Your schedule has been updated successfully")

        _LoadReports()

        OkayToClose = True
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        On Error Resume Next
        Dim lsv As ListViewItem
        Dim SQL As String
        Dim I As Integer = 1

        cmdApply_Click(sender, e)

        If OkayToClose = False Then Return

        If IsDataDriven Then
            frmRDScheduleWizard.m_DataFields = Nothing
        End If

        UcDest.doDeletions()

        Me.Close()

        Dim oUI As New clsMarsUI

        clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.PACKAGE, clsMarsAudit.AuditAction.EDIT)
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        On Error Resume Next
        oUI.ResetError(sender, ep)
        cmdApply.Enabled = True
    End Sub

    Private Sub cmdLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoc.Click
        On Error Resume Next
        Dim oFolders As frmFolders = New frmFolders
        Dim sFolder(1) As String

        sFolder = oFolders.GetFolder

        If sFolder(0) <> "" And sFolder(0) <> "Desktop" Then
            txtFolder.Text = sFolder(0)
            txtFolder.Tag = sFolder(1)
        End If

        oUI.ResetError(sender, ep)
    End Sub

    Private Sub cmdAddReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddReport.Click
        On Error Resume Next
        oUI.ResetError(lsvReports, ep)
        cmdApply.Enabled = True

        Dim oForm As frmPackedReport
        Dim sReturn() As String
        Dim rs As ADODB.Recordset
        Dim SQL As String
        Dim nCount As Integer

        SQL = "SELECT MAX(PackOrderID) FROM ReportAttr WHERE PackID =" & txtID.Text

        rs = clsMarsData.GetData(SQL)

        If rs Is Nothing Then
            nCount = 1
        Else
            If rs.EOF = False Then
                Dim sTemp As String = rs.Fields(0).Value

                nCount = sTemp

                nCount += 1
            Else
                nCount = 1
            End If
        End If

        rs.Close()

        oForm = New frmPackedReport
        oForm.IsDataDriven = Me.IsDataDriven
        sReturn = oForm.AddReport(nCount, txtID.Text)

        If sReturn(0).Length > 0 Then
            Dim lsv As ListViewItem = lsvReports.Items.Add(sReturn(0))

            lsv.Tag = sReturn(1)

            lsv.SubItems.Add(sReturn(2))
        End If

        Me._LoadReports()
    End Sub



    Private Sub txtFolder_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFolder.TextChanged
        oUI.ResetError(sender, ep)
        cmdApply.Enabled = True
    End Sub


    Private Sub cmdRemoveReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveReport.Click
        If lsvReports.SelectedItems.Count = 0 Then Exit Sub

        Dim SQL As String
        Dim sWhere As String
        Dim Response As DialogResult


        Response = MessageBox.Show("Delete the selected report from the package?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If Response = DialogResult.Yes Then

            oUI.DeleteReport(lsvReports.SelectedItems(0).Tag, txtID.Text)

            lsvReports.Items.Remove(lsvReports.SelectedItems(0))

        End If
    End Sub



    Private Sub chkMergePDF_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMergePDF.CheckedChanged
        txtMergePDF.Enabled = chkMergePDF.Checked

        If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.pd1_AdvancedPDFPack) Then
            If IsLoaded = True Then _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, sender, getFeatDesc(featureCodes.pd1_AdvancedPDFPack))
            chkMergePDF.Checked = False
            Return
        End If

        If chkMergePDF.Checked = True And IsLoaded = True Then
            Dim oOptions As New frmReportOptionsView

            oOptions.packageOptions("PDF", txtID.Text)
        End If

        btnPDFMerge.Enabled = chkMergePDF.Checked
    End Sub

    Private Sub chkMergeXL_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMergeXL.CheckedChanged
        If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.xl1_AdvancedXLPack) Then
            If IsLoaded = True Then _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, sender, getFeatDesc(featureCodes.xl1_AdvancedXLPack))
            chkMergeXL.Checked = False
            Return
        End If

        txtMergeXL.Enabled = chkMergeXL.Checked

        If chkMergeXL.Checked = True And IsLoaded = True Then
            Dim oOptions As New frmReportOptionsView

            oOptions.packageOptions("XLS", txtID.Text)
        End If

        btnXLSMerge.Enabled = chkMergeXL.Checked
    End Sub

    Private Sub txtMergePDF_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles txtMergePDF.TextChanged
        ep.SetError(sender, String.Empty)
    End Sub

    Private Sub txtMergeXL_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles txtMergeXL.TextChanged
        ep.SetError(sender, String.Empty)
    End Sub


    Private Sub chkDTStamp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDTStamp.CheckedChanged
        cmbDateTime.Enabled = chkDTStamp.Checked
        txtAdjustStamp.Enabled = chkDTStamp.Checked

        If chkDTStamp.Checked = False Then
            cmbDateTime.Text = String.Empty
            txtAdjustStamp.Value = 0
        Else
            Dim oUI As New clsMarsUI

            cmbDateTime.Text = oUI.ReadRegistry("DefDateTimeStamp", "")
        End If
    End Sub



    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        If lsvReports.SelectedItems.Count = 0 Then Return

        Dim olsv As ListViewItem = lsvReports.SelectedItems(0)
        Dim SelTag As Object = olsv.Tag

        If olsv.Index = 0 Then Return

        Dim olsv2 As ListViewItem = lsvReports.Items(olsv.Index - 1)

        Dim OldID, NewID As Integer

        NewID = olsv2.Index + 1 'Text.Split(":")(0)
        OldID = olsv.Index + 1  '.Text.Split(":")(0)

        Dim NewName As String

        Dim SQL As String
        Dim oData As New clsMarsData

        'update the item being moved up

        NewName = NewID & ":" & olsv.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "'," & _
        "PackOrderID = " & NewID & " WHERE ReportID =" & olsv.Tag

        clsMarsData.WriteData(SQL)

        olsv.Text = NewName

        'update the one being moved down
        NewName = OldID & ":" & olsv2.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID = " & OldID & " WHERE ReportID =" & olsv2.Tag

        clsMarsData.WriteData(SQL)

        olsv2.Text = NewName

        Me._LoadReports()

        For Each olsv In lsvReports.Items
            If olsv.Tag = SelTag Then
                olsv.Selected = True
                olsv.EnsureVisible()
                Exit For
            End If
        Next
    End Sub

    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        If lsvReports.SelectedItems.Count = 0 Then Return

        Dim olsv As ListViewItem = lsvReports.SelectedItems(0)
        Dim SelTag As Object = olsv.Tag

        If olsv.Index = lsvReports.Items.Count - 1 Then Return

        Dim olsv2 As ListViewItem = lsvReports.Items(olsv.Index + 1)

        Dim OldID, NewID As Integer

        NewID = olsv2.Index + 1  'Text.Split(":")(0)
        OldID = olsv.Index + 1 'Text.Split(":")(0)
        Dim NewName As String

        Dim SQL As String
        Dim oData As New clsMarsData

        'update the item being moved down

        NewName = NewID & ":" & olsv.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID = " & NewID & " WHERE ReportID =" & olsv.Tag

        clsMarsData.WriteData(SQL)

        olsv.Text = NewName

        'update the one being moved down
        NewName = OldID & ":" & olsv2.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID = " & OldID & " WHERE ReportID =" & olsv2.Tag

        clsMarsData.WriteData(SQL)

        olsv2.Text = NewName

        Me._LoadReports()

        For Each olsv In lsvReports.Items
            If olsv.Tag = SelTag Then
                olsv.Selected = True
                olsv.EnsureVisible()
                Exit For
            End If
        Next
    End Sub



    Private Sub optStatic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles optStatic.Click
        Dim sVal As String = ""

        If cmbKey.Text.Length = 0 Then
            ep.SetError(cmbKey, "Please select the key parameter")
            cmbKey.Focus()
            Return
        End If

        Dim oGet As New frmKeyParameter


        sVal = oGet._GetParameterValue(cmbKey.Text, sVal)

        If sVal.Length = 0 Then Return


        clsMarsData.WriteData("DELETE FROM DynamicAttr WHERE PackID=" & txtID.Text)

        Dim sCols As String
        Dim sVals As String
        Dim SQL As String

        sCols = "DynamicID,KeyParameter,KeyColumn,ConString," & _
        "Query,PackID"

        sVals = clsMarsData.CreateDataID("dynamicattr", "dynamicid") & "," & _
        "'" & SQLPrepare(cmbKey.Text) & "'," & _
        "'" & SQLPrepare(sVal) & "'," & _
        "'_'," & _
        "'_'," & _
        txtID.Text

        SQL = "INSERT INTO DynamicAttr(" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)
    End Sub

    Private Sub optDynamic_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optDynamic.CheckedChanged

    End Sub

    Private Sub optDynamic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles optDynamic.Click
        Dim oPar As New frmDynamicParameter
        Dim sParameter As String

        If cmbKey.Text.Length = 0 Then
            ep.SetError(cmbKey, "Please select the key parameter")
            cmbKey.Focus()
            Return
        End If

        sParameter = oPar.AddParameterDynamicQuery(cmbKey.Text, "package", , txtID.Text)

        If sParameter.Length = 0 Then Return


    End Sub

    Private Sub btnView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnView.Click
        If optStatic.Checked = True Then
            optStatic_Click(sender, e)
        ElseIf optDynamic.Checked = True Then
            optDynamic_Click(sender, e)
        End If
    End Sub

    Private Sub chkStaticDest_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkStaticDest.CheckedChanged
        GroupBox6.Enabled = Not chkStaticDest.Checked

        UcDest.StaticDest = chkStaticDest.Checked
    End Sub

    Private Sub cmdValidate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdValidate.Click
        If UcDSN._Validate = True Then
            cmbTable.Enabled = True
            cmbColumn.Enabled = True
            cmbValue.Enabled = True

            With UcDSN
                gsCon = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & .txtPassword.Text
            End With

            oData.GetTables(cmbTable, UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, _
                UcDSN.txtPassword.Text)

            oData.GetColumns(cmbColumn, UcDSN.cmbDSN.Text, cmbTable.Text, _
                UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

            oData.GetColumns(cmbValue, UcDSN.cmbDSN.Text, cmbTable.Text, _
                UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

            cmbValue.Items.Add("<Advanced>")
            cmbValue.Sorted = True
        Else
            _ErrorHandle("Could not connect to the selected data source", -2147636225, "ucDSN._Validate(frmDynamicSchedule.cmdValidate_Click)", 1912)
            cmbTable.Enabled = False
            cmbColumn.Enabled = False
            cmbValue.Enabled = False
        End If
    End Sub

    Private Sub cmbTable_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTable.SelectedIndexChanged
        oData.GetColumns(cmbColumn, UcDSN.cmbDSN.Text, cmbTable.Text, _
        UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)


        oData.GetColumns(cmbValue, UcDSN.cmbDSN.Text, cmbTable.Text, _
        UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

        cmbValue.Items.Add("<Advanced>")

        cmbValue.Sorted = True
    End Sub

    Private Sub cmbValue_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbValue.SelectedIndexChanged
        If cmbValue.Text.ToLower = "<advanced>" Then
            Dim oForm As New frmDynamicAdvanced

            oForm._AdvancedDynamicEdit(txtID.Text, UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, _
            UcDSN.txtPassword.Text, cmbTable.Text & "." & cmbColumn.Text, cmbKey.Text)
        End If
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        lsvReports_DoubleClick(sender, e)
    End Sub

    Private Sub tvPacks_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvPacks.AfterSelect
        Try
            Dim node As TreeNode = e.Node
            Dim execID As Integer = node.Tag
            Dim SQL As String
            Dim oRs As ADODB.Recordset

            lsvSnapshots.Items.Clear()

            SQL = "SELECT * FROM SnapshotsAttr WHERE ExecID = " & execID

            oRs = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then Return

            Do While oRs.EOF = False
                Dim item As ListViewItem = lsvSnapshots.Items.Add(oRs("datecreated").Value)
                Dim file As String = oRs("snappath").Value

                If IO.File.Exists(file) = True Then
                    item.SubItems.Add(file)
                Else
                    clsMarsData.WriteData("DELETE FROM SnapshotsAttr WHERE AttrID =" & oRs("attrid").Value)
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch : End Try
    End Sub

    Private Sub cmdView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdView.Click
        If lsvSnapshots.SelectedItems.Count = 0 Then Return

        For Each item As ListViewItem In lsvSnapshots.SelectedItems
            Process.Start(item.SubItems(1).Text)
        Next
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If tvPacks.SelectedNode Is Nothing Then Return

        If MessageBox.Show("Delete the selected package snapshot?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) _
        = Windows.Forms.DialogResult.Yes Then
            Dim SQL As String = "DELETE FROM SnapshotsAttr WHERE ExecID =" & tvPacks.SelectedNode.Tag

            clsMarsData.WriteData(SQL)
            tvPacks.SelectedNode.Remove()
            lsvSnapshots.Items.Clear()
        End If
    End Sub

    Private Sub chkSnapshots_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSnapshots.CheckedChanged
        If Not (IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.sa8_Snapshots)) And chkSnapshots.Checked = True Then
            _NeedUpgrade(gEdition.ENTERPRISEPRO, sender, getFeatDesc(featureCodes.sa8_Snapshots))
            chkSnapshots.Checked = False
            Return
        End If

        txtSnapshots.Enabled = chkSnapshots.Checked
        cmdExecute.Enabled = chkSnapshots.Checked
        cmdDelete.Enabled = chkSnapshots.Checked
        cmdView.Enabled = chkSnapshots.Checked
    End Sub

    Private Sub cmdExecute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdExecute.Click
        Try
            If tvPacks.SelectedNode Is Nothing Then Return

            Dim SQL As String = "SELECT * FROM SnapshotsAttr WHERE ExecID =" & tvPacks.SelectedNode.Tag
            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
            Dim files() As String
            Dim packid As Integer
            Dim i As Integer = 0

            If oRs Is Nothing Then Return

            ReDim files(0)

            Do While oRs.EOF = False
                packid = oRs("packid").Value

                ReDim Preserve files(i)

                files(i) = oRs("snappath").Value

                i += 1

                oRs.MoveNext()
            Loop

            oRs.Close()

            Dim osnap As New clsMarsSnapshots

            osnap.ExecuteSnapshot(packid, files)
        Catch ex As Exception
            _ErrorHandle("The snapshot is either missing or corrupt - " & vbCrLf & ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), "")
        End Try
    End Sub

    Private Sub btnTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTest.Click
        Dim SQL As String
        Dim ParValue As String
        Dim LinkSQL As String
        Dim LinkCon As String
        Dim oRs As ADODB.Recordset
        Dim sDSN As String
        Dim sUser As String
        Dim sPassword As String

        ParValue = InputBox("Please provide a sample value for the parameter", Application.ProductName)

        If ParValue.Length = 0 Then Return

        Try
            Dim oLinkCon As New ADODB.Connection

            If cmbValue.Text = "<Advanced>" Then
                SQL = "SELECT * FROM DynamicLink WHERE PackID= " & txtID.Text

                oRs = clsMarsData.GetData(SQL)

                If oRs.EOF = False Then
                    LinkSQL = oRs("linksql").Value
                    LinkCon = oRs("constring").Value
                End If

                sDSN = LinkCon.Split("|")(0)
                sUser = LinkCon.Split("|")(1)
                sPassword = LinkCon.Split("|")(2)

                oLinkCon.Open(sDSN, sUser, sPassword)

                ParValue = clsMarsData.ConvertToFieldType(LinkSQL, oLinkCon, oRs("keycolumn").Value, ParValue)

                If LinkSQL.ToLower.IndexOf("where") > -1 Then
                    LinkSQL &= " AND " & oRs("keycolumn").Value & " = " & ParValue
                Else
                    LinkSQL &= " WHERE " & oRs("keycolumn").Value & " = " & ParValue
                End If

                oRs.Close()
            Else
                sDSN = UcDSN.cmbDSN.Text
                sUser = UcDSN.txtUserID.Text
                sPassword = UcDSN.txtPassword.Text

                oLinkCon.Open(sDSN, sUser, sPassword)

                LinkSQL = "SELECT " & cmbValue.Text & " FROM " & cmbTable.Text


                ParValue = clsMarsData.ConvertToFieldType(LinkSQL, oLinkCon, cmbColumn.Text, ParValue)

                LinkSQL &= " WHERE " & cmbColumn.Text & " = " & ParValue

            End If

            oRs = New ADODB.Recordset

            oRs.Open(LinkSQL, oLinkCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

            Dim oResults As New frmDBResults

            oResults._ShowResults(oRs)

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace), LinkSQL)
        End Try

    End Sub
    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        On Error Resume Next
        Dim ctrl As TextBox

        ctrl = CType(Me.ActiveControl, TextBox)

        ctrl.Undo()

    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next
        Dim ctrl As TextBox

        ctrl = CType(Me.ActiveControl, TextBox)

        ctrl.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next
        Dim ctrl As TextBox

        ctrl = CType(Me.ActiveControl, TextBox)

        ctrl.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next
        Dim ctrl As TextBox

        ctrl = CType(Me.ActiveControl, TextBox)

        ctrl.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next
        Dim ctrl As TextBox

        ctrl = CType(Me.ActiveControl, TextBox)

        ctrl.SelectedText = ""
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next
        Dim ctrl As TextBox

        ctrl = CType(Me.ActiveControl, TextBox)

        ctrl.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        'On Error Resume Next
        'Dim intruder As frmInserter = New frmInserter(99999)
        On Error Resume Next
        Dim oInsert As New frmInserter(0)
        oInsert.m_EventBased = False
        oInsert.m_EventID = 0
        oInsert.m_parameterList = Nothing
        'oField.SelectedText = oInsert.GetConstants(Me)
        oInsert.GetConstants(Me)

        'intruder.GetConstants(Me, Me.ActiveControl)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next
        Dim getData As frmDataItems = New frmDataItems

        Dim ctrl As TextBox = CType(Me.ActiveControl, TextBox)

        ctrl.Text = getData._GetDataItem(99999)
    End Sub

    Private Sub cmbKey_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbKey.DropDown
        If Me.m_ParametersRead = False Then

            Dim saveCursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                Me._GetParameters(Me.cmbKey)
            Finally
                Cursor.Current = saveCursor
            End Try

            Me.m_ParametersRead = True
        End If
    End Sub




    Private Sub TabControlPanel9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub tvHistory_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs)

    End Sub

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        If chkSelectAll.Checked = True And chkSelectAll.CheckState = CheckState.Checked Then
            Dim olsv As ListViewItem
            For Each olsv In lsvReports.Items
                olsv.Checked = True
            Next
        ElseIf chkSelectAll.Checked = False And chkSelectAll.CheckState = CheckState.Unchecked Then
            Dim olsv As ListViewItem
            For Each olsv In lsvReports.Items
                olsv.Checked = False
            Next
        End If
    End Sub
    Private Sub chkAutoResume_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAutoResume.CheckedChanged
        txtCacheExpiry.Enabled = Me.chkAutoResume.Checked
    End Sub

    Private Sub btnConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnect.Click
        grpQuery.Enabled = UcDSNDD._Validate
    End Sub

    Private Sub btnBuild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuild.Click
        Dim queryBuilder As frmDynamicParameter = New frmDynamicParameter

        Dim sVals(1) As String
        Dim sCon As String = ""
        Dim temp As String = txtQuery.Text
        Dim tmpKey As String = cmbDDKey.Text

        If UcDSNDD.cmbDSN.Text.Length > 0 Then
            sCon = UcDSNDD.cmbDSN.Text & "|" & UcDSNDD.txtUserID.Text & "|" & UcDSNDD.txtPassword.Text & "|"
        End If

        queryBuilder.m_showStar = True
        queryBuilder.Label1.Text = "Please select the table that holds the required data"

        sVals = queryBuilder.ReturnSQLQuery(sCon, txtQuery.Text)

        If sVals Is Nothing Then Return

        sCon = sVals(0)
        txtQuery.Text = sVals(1)

        UcDSNDD.cmbDSN.Text = sCon.Split("|")(0)
        UcDSNDD.txtUserID.Text = sCon.Split("|")(1)
        UcDSNDD.txtPassword.Text = sCon.Split("|")(2)

        Dim oRs As ADODB.Recordset = New ADODB.Recordset
        Dim oCon As ADODB.Connection = New ADODB.Connection

        oCon.Open(UcDSNDD.cmbDSN.Text, UcDSNDD.txtUserID.Text, UcDSNDD.txtPassword.Text)

        oRs.Open(clsMarsParser.Parser.ParseString(txtQuery.Text), oCon, ADODB.CursorTypeEnum.adOpenStatic)

        Dim I As Integer = 0

        Me.cmbDDKey.Items.Clear()

        frmRDScheduleWizard.m_DataFields = New Hashtable

        For Each fld As ADODB.Field In oRs.Fields
            frmRDScheduleWizard.m_DataFields.Add(I, fld.Name)
            Me.cmbDDKey.Items.Add(fld.Name)
            I += 1
        Next

        If cmbDDKey.Items.Contains(tmpKey) Then cmbDDKey.Text = tmpKey

        Dim tmpCon As String = ""

        With UcDSNDD
            tmpCon = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & _EncryptDBValue(.txtPassword.Text)
        End With

        If txtQuery.Text.ToLower <> temp.ToLower Then
            clsMarsReport.populateDataDrivenCache(txtQuery.Text, tmpCon, True)
        End If

        oRs.Close()
        oCon.Close()

        oRs = Nothing
        oCon = Nothing
    End Sub

    Private Sub cmbDDKey_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDDKey.SelectedIndexChanged
        ep.SetError(Me.cmbDDKey, "")
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Dim res As DialogResult = MessageBox.Show("Are you sure you would like to clear all the Dynamic linking information for this schedule?", _
       Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If res = Windows.Forms.DialogResult.Yes Then
            clsMarsData.WriteData("DELETE FROM DynamicLink WHERE PackID=" & Me.txtID.Text)

            With UcDSN
                .cmbDSN.Text = ""
                .txtPassword.Text = ""
                .txtUserID.Text = ""
            End With

            Me.cmbTable.Text = ""
            cmbValue.Text = ""
            cmbColumn.Text = ""
        End If
    End Sub
    Private Sub chkGroupReports_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkGroupReports.CheckedChanged
        Me.chkMergeAllPDF.Enabled = chkGroupReports.Checked
        Me.chkMergeAllXL.Enabled = chkGroupReports.Checked

        Me.txtMergeAllPDF.Enabled = chkGroupReports.Checked
        Me.txtMergeAllXL.Enabled = Me.chkGroupReports.Checked
    End Sub

    Private Sub chkMergeAllPDF_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMergeAllPDF.CheckedChanged
        Me.txtMergeAllPDF.Enabled = Me.chkMergeAllPDF.Checked
        Me.chkMergePDF.Enabled = Not Me.chkMergeAllPDF.Checked

        If chkMergeAllPDF.Checked Then
            If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.pd1_AdvancedPDFPack) = False Then
                If IsLoaded = True Then _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, sender, getFeatDesc(featureCodes.pd1_AdvancedPDFPack))
                chkMergeAllPDF.Checked = False
                Return
            End If

            Me.chkMergePDF.Checked = False

            If IsLoaded = True Then
                Dim oOptions As New frmRptOptions

                oOptions.PackageOptions("PDF", Me.txtID.Text)
            End If
            'Else
            '    clsMarsData.WriteData("DELETE FROM PackageOptions WHERE PackID = " & Me.m_PackID, False)
        End If
    End Sub

    Private Sub chkMergeAllXL_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMergeAllXL.CheckedChanged
        Me.txtMergeAllXL.Enabled = Me.chkMergeAllXL.Checked
        Me.chkMergeXL.Enabled = Not chkMergeAllXL.Checked

        If chkMergeAllXL.Checked = True Then
            If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.xl1_AdvancedXLPack) = False Then
                If IsLoaded = True Then _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, sender, getFeatDesc(featureCodes.xl1_AdvancedXLPack))
                chkMergeAllXL.Checked = False
                Return
            End If

            Me.chkMergeXL.Checked = False

            If IsLoaded = True Then
                Dim oOptions As New frmRptOptions

                oOptions.PackageOptions("XLS", Me.txtID.Text)
            End If
            'Else
            '    clsMarsData.WriteData("DELETE FROM PackageOptions WHERE PackID = " & Me.m_PackID, False)
        End If
    End Sub

    Private Sub txtMergeAllPDF_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMergeAllPDF.Validated, txtMergeAllXL.Validated
        Dim txt As TextBox = CType(sender, TextBox)

        If txt.Text.Contains("<[r]") Then
            MessageBox.Show("Using a Data-Driven Constant in this field is not advisable as the schedule will always use the value from last record in the Data-Driver", _
            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txt.Focus()
            ep.SetError(txt, "Using a Data-Driven Constant in this field is not advisable as the schedule will always use the value from last record in the Data-Driver")
        End If
    End Sub

    Private Sub txtMergeAllPDF_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMergeAllPDF.TextChanged, txtMergeAllXL.TextChanged
        ep.SetError(CType(sender, TextBox), "")
    End Sub

    Private Sub chkMultithreaded_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.chkMultithreaded.Checked And IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.sa1_MultiThreading) = False Then
            _NeedUpgrade(gEdition.ENTERPRISEPROPLUS, sender, getFeatDesc(featureCodes.sa1_MultiThreading))
            Me.chkMultithreaded.Checked = False
        End If
    End Sub

    Private Sub chkMergeText_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkMergeText.CheckedChanged
        txtMergeTextName.Enabled = chkMergeText.Checked
    End Sub

    Dim inserter As frmInserter = New frmInserter(99999)

    Private Sub tabProperties_SelectedTabChanged1(sender As Object, e As DevComponents.DotNetBar.SuperTabStripSelectedTabChangedEventArgs) Handles tabProperties.SelectedTabChanged
        If e.NewValue Is Nothing Then Return

        If e.NewValue.Text = "Reports" Then
            If inserter IsNot Nothing AndAlso inserter.IsDisposed = False Then
                inserter.GetConstants(Me)
            Else
                inserter = New frmInserter(99999)

                inserter.GetConstants(Me)
            End If
        Else
            inserter.Hide()
            If e.NewValue.Text = "History" Then
                With UcHistory
                    .m_objectID = txtID.Text
                    .m_scheduleType = clsMarsScheduler.enScheduleType.PACKAGE
                    .m_showControls = True
                    .DrawScheduleHistory()
                End With
            ElseIf e.NewValue.Text = "Snapshots" Then
                ViewSnapshots()
            ElseIf e.NewValue.Text = "Exception Handling" Then
                UcBlank.UcBlankReportTasks.ShowAfterType = False
                UcBlank.UcBlankReportTasks.ScheduleID = txtID.Text
                UcBlank.UcBlankReportTasks.LoadTasks()
            End If
        End If
    End Sub

   

    Private Sub btnXLSMerge_Click(sender As Object, e As EventArgs) Handles btnXLSMerge.Click
        chkMergeXL_CheckedChanged(Nothing, Nothing)
    End Sub

    Private Sub btnPDFMerge_Click1(sender As Object, e As EventArgs) Handles btnPDFMerge.Click
        chkMergePDF_CheckedChanged(Nothing, Nothing)
    End Sub
End Class
