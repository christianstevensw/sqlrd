Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("SQL-RD")> 


<Assembly: AssemblyDescription("Build 20080219")> 


<Assembly: AssemblyCompany("ChristianSteven Software Ltd")> 
<Assembly: AssemblyProduct("SQL-RD")> 
<Assembly: AssemblyCopyright("Copyright ChristianSteven Software Ltd 2008")> 
<Assembly: AssemblyTrademark("")> 
'<Assembly: AssemblyFileVersion("5.0.0.5")> 
<Assembly: CLSCompliant(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("7EC12044-FB92-45AE-B548-9EFDD2A54DDF")> 

'change build nnumber
'add reference
'change viewer
'change CRYSTAL_VER in clsMarsReport and clsSubreport classes

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("6.6.*")> 
