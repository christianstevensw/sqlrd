CREATE TABLE HouseKeepingAttr (
HouseID			INTEGER,
DestinationID	INTEGER,
AgeValue		INTEGER,
AgeUnit			VARCHAR(50)
);
ALTER TABLE ReportAttr	ALTER COLUMN ReportName		TEXT;
ALTER TABLE ReportAttr	ALTER COLUMN  ReportTitle		TEXT;
ALTER TABLE PackageAttr ALTER COLUMN  PackageName		TEXT;
ALTER TABLE Folders		ALTER FolderName		TEXT;
ALTER TABLE PrinterAttr	ALTER COLUMN  PrinterName		TEXT;
ALTER TABLE PackageAttr ADD	  DateTimeStamp		INTEGER;
ALTER TABLE PackageAttr ADD	  StampFormat		VARCHAR(50);
ALTER TABLE SendWait	ADD	  MailFormat		VARCHAR(50);
CREATE TABLE RemoteSysAttr (
RemoteID		INTEGER,
PCName			VARCHAR(55),
LivePath		TEXT);
ALTER TABLE PrinterAttr ALTER COLUMN  PrinterDriver TEXT;
ALTER TABLE PrinterAttr ALTER COLUMN  PrinterPort TEXT;
ALTER TABLE BlankReportAlert ALTER COLUMN  [Type] TEXT;
ALTER TABLE BlankReportAlert ALTER COLUMN  [To] TEXT;
ALTER TABLE ReportOptions ADD HTMLNavigator INTEGER;
ALTER TABLE SendWait ALTER COLUMN  Subject TEXT;
ALTER TABLE SendWait ALTER COLUMN  Attach TEXT;
ALTER TABLE SendWait ALTER COLUMN  Extras TEXT;
ALTER TABLE SendWait ALTER COLUMN  SendCC TEXT;
ALTER TABLE SendWait ALTER COLUMN  SendBCC TEXT;
ALTER TABLE SendWait ALTER COLUMN  sName TEXT;
CREATE TABLE Slaves (
SlaveID			INTEGER,
SlaveName		VARCHAR(255), 
SlavePath		TEXT);
CREATE TABLE PackageOptions (
OptionID		INTEGER,
PackID			INTEGER,
PDFPassword		VARCHAR(50),
UserPassword	VARCHAR(50),
PDFWatermark	VARCHAR(50),
CanPrint		INTEGER,
CanCopy			INTEGER,
CanEdit			INTEGER,
CanNotes		INTEGER,
CanFill			INTEGER,
CanAccess		INTEGER,
CanAssemble		INTEGER,
CanPrintFull		INTEGER,
PDFSecurity		INTEGER);
ALTER TABLE ReportOptions ADD WorkSheetName		VARCHAR(50);
ALTER TABLE DestinationAttr ALTER COLUMN  CustomName TEXT;
UPDATE ReportParameter SET ParValue = REPLACE(ParValue,';','|') WHERE RIGHT(ParValue,1) = ';'
ALTER TABLE BlankReportAlert ADD Subject TEXT;
CREATE TABLE RefresherAttr (
RefreshID		INTEGER,
ObjectType		VARCHAR(50),
Frequency		VARCHAR(50),
ObjectName		VARCHAR(50),
ObjectID		INTEGER,
LastRun			DATETIME,
NextRun			DATETIME,
RepeatEvery		INTEGER,
StartTime		VARCHAR(50));
ALTER TABLE RemoteSysAttr ADD AutoConnect INTEGER;
ALTER TABLE ReportOptions ADD InfoTitle		VARCHAR(50);
ALTER TABLE ReportOptions ADD InfoAuthor	VARCHAR(50);
ALTER TABLE ReportOptions ADD InfoSubject	VARCHAR(50);
ALTER TABLE ReportOptions ADD InfoProducer	VARCHAR(50);
ALTER TABLE ReportOptions ADD InfoKeywords	TEXT;
ALTER TABLE ReportOptions ADD InfoCreated	DATETIME;
CREATE TABLE CustomSections (
ReportID		INTEGER,
SectionNo		INTEGER,
Suppress		INTEGER);
ALTER TABLE SubReportParameters ALTER COLUMN  SubName	TEXT;
ALTER TABLE SubReportParameters ALTER COLUMN  ParName	TEXT;
ALTER TABLE SubReportParameters ALTER COLUMN  ParValue	TEXT;
ALTER TABLE PackageOptions 	ADD InfoTitle		VARCHAR(50);
ALTER TABLE PackageOptions 	ADD InfoAuthor	VARCHAR(50);
ALTER TABLE PackageOptions 	ADD InfoSubject	VARCHAR(50);
ALTER TABLE PackageOptions 	ADD InfoProducer	VARCHAR(50);
ALTER TABLE PackageOptions 	ADD InfoKeywords	TEXT;
ALTER TABLE PackageOptions 	ADD InfoCreated	DATETIME;
ALTER TABLE SubReportLogin	ALTER COLUMN 	SubName		TEXT;
ALTER TABLE SubReportLogin	ALTER COLUMN 	RptServer	TEXT;
ALTER TABLE SubReportLogin	ALTER COLUMN 	RptDatabase	TEXT;
ALTER TABLE SubReportLogin	ALTER COLUMN 	RptUserID	TEXT;
ALTER TABLE SubReportLogin	ALTER COLUMN 	RptPassword	TEXT;
CREATE TABLE UserColumns(
ColumnID		INTEGER,
Caption			VARCHAR(50),
DataColumn		VARCHAR(50),
Owner			VARCHAR(50));
CREATE TABLE SMTPServers(
SMTPID				INTEGER,
SMTPName			VARCHAR(50),
SMTPServer			VARCHAR(50),
SMTPUser			VARCHAR(50),
SMTPPassword			VARCHAR(50),
SMTPSenderName			VARCHAR(50),
SMTPSenderAddress		VARCHAR(50),
SMTPTimeout			INTEGER,
IsBackup			INTEGER);
ALTER TABLE DestinationAttr 	ADD		SMTPServer			VARCHAR(50);
ALTER TABLE DestinationAttr		ADD		UseBursting			INTEGER;
ALTER TABLE ReportAttr			ADD		AutoRefresh			INTEGER;
ALTER TABLE SendWait			ADD		IncludeAttach		INTEGER;
ALTER TABLE SendWait			ADD		SMTPName			VARCHAR(50);
DROP TABLE BurstingAttr;
CREATE TABLE BurstAttr (
BurstID			INTEGER,
ReportID		INTEGER,
DestinationID	INTEGER,
GroupValue		VARCHAR(50),
GroupColumn		VARCHAR(50)
);
ALTER TABLE ReportAttr ADD Bursting				INTEGER;
UPDATE ScheduleAttr SET ReportID = 0 WHERE ReportID IS NULL;
UPDATE ScheduleAttr SET PackID = 0 WHERE PackID IS NULL;
ALTER TABLE ReportOptions ADD		ExcelBursting			INTEGER;
ALTER TABLE ReportOptions ADD		ExcelBurstColumn	VARCHAR(50);
ALTER TABLE ReportAttr DROP ExcelBurst;
ALTER TABLE ReportAttr DROP BurstGroup;
ALTER TABLE ReportAttr ADD StaticDest INTEGER;
ALTER TABLE DestinationAttr ADD EncryptZip			INTEGER;
ALTER TABLE DestinationAttr ADD EncryptZipLevel		INTEGER;
ALTER TABLE DestinationAttr ADD EncryptZipCode		VARCHAR(255);
CREATE TABLE ThreadManager (
ReportID		INTEGER,
PackID			INTEGER,
AutoID			INTEGER);
CREATE TABLE SystemFolders (
FolderID		INTEGER,
FolderName		VARCHAR(50),
FolderSQL		TEXT);
ALTER TABLE CRDUsers ADD AutoLogin				INTEGER;
CREATE TABLE PGPAttr (
KeyUserID			VARCHAR(255)	PRIMARY KEY,
KeyPassword		VARCHAR(255),
KeyLocation		TEXT,
KeySize			VARCHAR(55),
KeyType			VARCHAR(55),
DestinationID	INTEGER);
ALTER TABLE ReportOptions		ADD				ProtectExcel		INTEGER;
ALTER TABLE ReportOptions		ADD				ExcelPassword		VARCHAR(255);
ALTER TABLE DestinationAttr		ADD				UseDUN				INTEGER;
ALTER TABLE DestinationAttr		ADD				DUNName				VARCHAR(55);
ALTER TABLE ReportOptions		ADD				KeepNoFormats		INTEGER;
ALTER TABLE ReportOptions		ADD				KeepDateFormats		INTEGER;
ALTER TABLE PackageOptions		ADD				ProtectExcel		INTEGER;
ALTER TABLE PackageOptions		ADD				ExcelPassword		VARCHAR(255);
ALTER TABLE PGPAttr				ADD				PGPID				VARCHAR(255);
ALTER TABLE ReportAttr			ADD				PackOrderID			INTEGER;
ALTER TABLE ScheduleOptions		ADD				UseOptions			INTEGER;
ALTER TABLE DataItems			ADD				AllowMultiple		INTEGER;
ALTER TABLE DataItems			ADD				MultipleSep			VARCHAR(55);
ALTER TABLE PackagedReportAttr	ADD				Status				INTEGER;
ALTER TABLE PackagedReportAttr	ADD				DisabledDate		DATETIME;
UPDATE PackagedReportAttr SET Status = 1 WHERE Status IS NULL;
CREATE TABLE TaskOptions (
OptionID		INTEGER		PRIMARY KEY,
TaskID			INTEGER,
PDFPassword		VARCHAR(50),
UserPassword	VARCHAR(50),
CanPrint		INTEGER,
CanCopy			INTEGER,
CanEdit			INTEGER,
CanNotes		INTEGER,
CanFill			INTEGER,
CanAccess		INTEGER,
CanAssemble		INTEGER,
CanPrintFull	INTEGER,
PDFSecurity		INTEGER,
InfoTitle		VARCHAR(50),
InfoAuthor		VARCHAR(50),
InfoSubject		VARCHAR(50),
InfoProducer	VARCHAR(50),
InfoKeywords	TEXT,
InfoCreated		DATETIME,
PDFWatermark	VARCHAR(50));
CREATE TABLE ReportTable (
RptTableID			INTEGER PRIMARY KEY,
ReportID		INTEGER,
TableName		VARCHAR(55),
RptServer		VARCHAR(55),
RptDatabase		VARCHAR(55),
RptUserID		VARCHAR(55),
RptPassword		VARCHAR(55));
CREATE TABLE SubReportTable (
RptTableID		INTEGER PRIMARY KEY,
ReportID		INTEGER,
SubReportName	VARCHAR(55),
TableName		VARCHAR(55),
RptServer		VARCHAR(55),
RptDatabase		VARCHAR(55),
RptUserID		VARCHAR(55),
RptPassword		VARCHAR(55));
CREATE TABLE DomainAttr (
DomainID		INTEGER PRIMARY KEY,
DomainName		VARCHAR(55),
UserRole		VARCHAR(55),
LastModDate		DATETIME,
LastModBy		VARCHAR(55)
);
ALTER TABLE ScheduleAttr	ADD		SmartID		INTEGER;
ALTER TABLE DestinationAttr	ADD		SmartID		INTEGER;
ALTER TABLE ScheduleHistory	ADD		SmartID		INTEGER;
CREATE TABLE EventSubAttr (
EventSubID		INTEGER PRIMARY KEY,
EventID			INTEGER,
ReportID		INTEGER,
ParName			VARCHAR(255),
ParValue		VARCHAR(255)
);
CREATE TABLE GroupAttr (
GroupID			INTEGER PRIMARY KEY,
GroupName		VARCHAR(55),
GroupDesc		TEXT
);
CREATE TABLE GroupPermissions (
PermID			INTEGER PRIMARY KEY,
GroupID			INTEGER,
PermDesc		VARCHAR(55),
PermValue		INTEGER
);
ALTER TABLE ReportOptions ADD		PDFBookmarks	INTEGER;
CREATE TABLE HouseKeepingPaths (
EntryID			INTEGER PRIMARY KEY,
FolderPath		VARCHAR(255),
KeepFor			INTEGER,
KeepUnit		VARCHAR(55)
);
ALTER TABLE DynamicAttr		ADD PackID			INTEGER;
ALTER TABLE DynamicLink		ADD PackID			INTEGER;
ALTER TABLE PackageAttr		ADD Dynamic			INTEGER;
ALTER TABLE PackageAttr		ADD	StaticDest		INTEGER;
ALTER TABLE RemoteSysAttr	ADD UserName		VARCHAR(55);
ALTER TABLE RemoteSysAttr	ADD UserPassword	VARCHAR(55);
UPDATE DestinationAttr		SET SmartID = 2777 WHERE ReportID = 2777 AND PackID = 2777;
UPDATE DestinationAttr		SET SmartID = 0 WHERE SmartID IS NULL;
ALTER TABLE DynamicAttr		ALTER COLUMN KeyParameter		VARCHAR(255);
ALTER TABLE DynamicAttr		ALTER COLUMN KeyColumn			VARCHAR(255);
ALTER TABLE DynamicLink		ALTER COLUMN ConString			VARCHAR(255);
ALTER TABLE DynamicLink		ALTER COLUMN KeyColumn			VARCHAR(255);
ALTER TABLE DynamicLink		ALTER COLUMN KeyParameter		VARCHAR(255);
ALTER TABLE PGPAttr			ADD PGPExt						INTEGER;
CREATE TABLE CodeGen (
CodeValue		INTEGER PRIMARY KEY
);
ALTER TABLE SnapshotsAttr	ADD PackID			INTEGER;
ALTER TABLE SnapshotsAttr	ADD ExecID			INTEGER;
ALTER TABLE ReportSnapshots ADD	PackID			INTEGER;
ALTER TABLE Slaves			ADD	IPAddress		VARCHAR(15);
CREATE TABLE ReadReceiptsAttr (
ReceiptID					INTEGER PRIMARY KEY,
DestinationID				INTEGER,
WaitLength					INTEGER,
Repeat						INTEGER,
RepeatEvery					INTEGER
);
ALTER TABLE DestinationAttr	ADD	ReadReceipts	INTEGER;
CREATE TABLE ReadReceiptsLog (
LogID           INTEGER,
DestinationID   INTEGER,
EmailSubject    TEXT,
SentTo          TEXT,
EntryDate       DATETIME,
NextCheck       DATETIME,
LastCheck       DATETIME,
RRStatus        VARCHAR(25)
);
ALTER TABLE DestinationAttr     ADD FTPType                 VARCHAR(50);
ALTER TABLE DestinationAttr     ADD AdjustStamp             INTEGER;
ALTER TABLE PackagedReportAttr  ADD AdjustStamp             INTEGER;
ALTER TABLE PackageAttr         ADD AdjustPackageStamp      INTEGER;
ALTER TABLE SendWait            ADD ReadReceipt             INTEGER;
ALTER TABLE SendWait            ADD DestinationID           INTEGER;
ALTER TABLE ReportAttr          ADD DynamicTasks            INTEGER;
ALTER TABLE PackageAttr         ADD DynamicTasks            INTEGER;
CREATE TABLE EventAttr6 (
EventID			INTEGER PRIMARY KEY,
EventName		VARCHAR(50),
Description		TEXT,
Keyword			TEXT,
Parent			INTEGER,
Status			INTEGER,
Owner			VARCHAR(50),
DisabledDate	DATETIME,
ScheduleType	VARCHAR(50),
AnyAll          VARCHAR(50)
);
CREATE TABLE EventConditions (
ConditionID			INTEGER,
EventID				INTEGER,
ConditionName		VARCHAR(50),
ConditionType		VARCHAR(50),
ReturnValue			INTEGER,
ConnectionString	TEXT,
KeyColumn			VARCHAR(50),
DetectInserts		INTEGER,
FilePath			VARCHAR(255),
SearchCriteria		TEXT,
PopServer			VARCHAR(50),
PopUser				VARCHAR(50),
PopPassword			VARCHAR(50),
PopRemove			INTEGER,
AnyAll              VARCHAR(50),
PollingInterval     INTEGER,
LastPolled          DATETIME,
OrderID				INTEGER
);
ALTER TABLE ScheduleAttr    ADD     UseException        INTEGER;
ALTER TABLE ScheduleAttr    ADD     ExceptionCalendar    VARCHAR(55);
ALTER TABLE ReportOptions   ADD     CharPerInch         INTEGER;
ALTER TABLE EventAttr6      ADD     ExecutionFlow       VARCHAR(10);
ALTER TABLE ScheduleAttr    ALTER COLUMN RepeatUntil    VARCHAR(25);
ALTER TABLE ReportOptions   ADD     CrystalFieldType    VARCHAR(25);
ALTER TABLE TaskManager     ADD     AutoID              INTEGER;
ALTER TABLE PrinterAttr     ADD     PaperSource         VARCHAR(25);
ALTER TABLE ThreadManager   ADD     SmartID             INTEGER;
ALTER TABLE ThreadManager   ADD     EventID             INTEGER;
ALTER TABLE ThreadManager   ADD     EntryDate           DATETIME;
ALTER TABLE EventHistory    ADD     StartDate           DATETIME;
ALTER TABLE Tasks           ADD     FTPType             VARCHAR(25);
ALTER TABLE DestinationAttr ADD     EnabledStatus       INTEGER;
ALTER TABLE DestinationAttr ADD     DestOrderID         INTEGER;
UPDATE DestinationAttr  SET EnabledStatus = 1 WHERE EnabledStatus IS NULL;
UPDATE DestinationAttr  SET DestOrderID = 0 WHERE DestOrderID IS NULL;
CREATE Table StampAttr (
StampID         INTEGER PRIMARY KEY,
ForeignID        INTEGER,
StampType       VARCHAR(25),
FontDetails     TEXT,
OverlayUnderlay VARCHAR(25),
VerticalPos     VARCHAR(55),
HorizontalPos   VARCHAR(55),
ImageFile       TEXT,
StampAngle      INTEGER
);
ALTER TABLE ReportOptions       ALTER   COLUMN      PDFWatermark        TEXT;
ALTER TABLE PackageOptions      ALTER   COLUMN      PDFWatermark        TEXT;
ALTER TABLE ReportOptions       ADD             ExcelShowGrid       INTEGER;
ALTER TABLE ReportOptions       ADD             ExcelPHPF           VARCHAR(55);
ALTER TABLE ReportParameter     ADD             ParNull             INTEGER;
ALTER TABLE SubReportParameters ADD             ParNull             INTEGER;
ALTER TABLE ReportAttr          ADD             RptDatabaseType     VARCHAR(25);
ALTER TABLE ReportTable         ADD             RptDatabaseType     VARCHAR(25);
ALTER TABLE SubReportTable      ADD             RptDatabaseType     VARCHAR(25);
ALTER TABLE SendWait            ADD             EmbedFile           TEXT;
ALTER TABLE EventConditions     ADD             RedirectMail        INTEGER;
ALTER TABLE EventConditions     ADD             RedirectTo          TEXT;
ALTER TABLE EventConditions     ADD             ForwardMail         INTEGER;
CREATE TABLE ForwardMailAttr (
MailID              INTEGER PRIMARY KEY,
ConditionID         INTEGER,
SendTo              TEXT,
SendCC              TEXT,
SendBCC             TEXT,
MailSubject         TEXT,
MailBody            TEXT,
Attachments         TEXT,
MailFormat          VARCHAR(25),
MailServer          VARCHAR(25)
);
ALTER TABLE SendWait ADD                    EntryDate           DATETIME;
UPDATE SendWait SET EntryDate = LastAttempt WHERE EntryDate IS NULL;
ALTER TABLE EventConditions ALTER COLUMN PopUser VARCHAR(100);
ALTER TABLE EventAttr6 ADD RetryCount       INTEGER;
ALTER TABLE EventAttr6 ADD AutoFailAfter    INTEGER;
CREATE TABLE EventPackageAttr(
EventPackID         INTEGER PRIMARY KEY,
Parent              INTEGER,
PackageName         VARCHAR(255),
ExecutionFlow       VARCHAR(55),
Owner               VARCHAR(255)
);
ALTER TABLE     ScheduleAttr    ADD     EventPackID    INTEGER;
ALTER TABLE     EventAttr6      ADD     PackID     INTEGER;
ALTER TABLE     UserView        ADD     EventPackID     INTEGER;
UPDATE ScheduleAttr SET EventPackID =0 WHERE EventPackID IS NULL;
ALTER TABLE     EventAttr6      ADD     PackOrderID     INTEGER;
ALTER TABLE     Tasks           ALTER COLUMN ProgramParameters  TEXT;
ALTER TABLE     Tasks           ALTER COLUMN ProgramPath        TEXT;
ALTER TABLE     ScheduleHistory ADD     EventPackID     INTEGER;
ALTER TABLE     ThreadManager   ADD     EventPackID     INTEGER;
ALTER TABLE     TaskManager     ADD     EventPackID     INTEGER;
ALTER TABLE     ScheduleAttr    ADD     RepeatUnit      VARCHAR(25);
ALTER TABLE 	DataItems	    ALTER 	COLUMN	ConString TEXT;
ALTER TABLE     CRDUsers        ADD     UserNumber      INTEGER;
ALTER TABLE     DomainAttr        ADD     UserNumber      INTEGER;
ALTER TABLE    PackagedReportAttr     ALTER COLUMN  CustomName    TEXT;
ALTER TABLE    Tasks     ALTER COLUMN  FTPServer    TEXT;
ALTER TABLE    Tasks     ALTER COLUMN  FTPUser    TEXT;
ALTER TABLE    Tasks     ALTER COLUMN  FTPPassword    TEXT;
ALTER TABLE    Tasks     ALTER COLUMN  FTPDirectory    TEXT;
ALTER TABLE     PrinterAttr     ADD PrintDuplex     VARCHAR(55);
ALTER TABLE     EventConditions ADD serverPort          INTEGER;
ALTER TABLE     EventConditions ADD UseSSL              INTEGER;
CREATE TABLE    DataDrivenAttr (
DDID            INTEGER     PRIMARY KEY,
ReportID        INTEGER,
ConString       TEXT,
SQLQuery        TEXT,
FailOnOne       INTEGER
);
ALTER TABLE     ReportAttr  ADD IsDataDriven    INTEGER;
ALTER TABLE     EventConditions ADD MailServerType  VARCHAR(25);
ALTER TABLE     EventConditions ADD IMAPBox         TEXT;
ALTER TABLE     ReportAttr ADD AutoCalc         INTEGER;
ALTER TABLE     PackageAttr ADD AutoCalc        INTEGER;
ALTER TABLE     EventAttr6  ADD AutoCalc        INTEGER;
ALTER TABLE		ScheduleAttr ADD BZipName		TEXT;
ALTER TABLE		ScheduleAttr ADD BPath			TEXT;
ALTER TABLE     ReportOptions       ADD AppendToFile    INTEGER;
ALTER TABLE     EventConditions     ADD UseConstraint   INTEGER;
ALTER TABLE     EventConditions     ADD ConstraintValue INTEGER;
ALTER TABLE     EventConditions     ADD RunOnce         INTEGER;
ALTER TABLE     EventConditions     ADD IgnoreNow          INTEGER;
ALTER TABLE     EventConditions     ADD DataLastModified          DATETIME;
ALTER TABLE     PackagAttr	ALTER COLUMN    MergePDFName    TEXT;
ALTER TABLE     PackagAttr	ALTER COLUMN    MergeXLName    TEXT;
ALTER TABLE    ReportOptions    ADD     TCompression      VARCHAR(50);
ALTER TABLE    ReportOptions    ADD     TColour      VARCHAR(50);
ALTER TABLE    ReportOptions    ADD     TCompression      VARCHAR(50);
ALTER TABLE    ReportOptions    ADD     TGrey      INTEGER;
ALTER TABLE    ReportOptions    ADD     TCompression      VARCHAR(50);
ALTER TABLE    ReportOptions    ADD     TPage      INTEGER;
ALTER TABLE    ReportOptions    ADD     XDataOnly      INTEGER;
ALTER TABLE    ReportOptions    ADD     XDataType      VARCHAR(50);
ALTER TABLE    CalendarAttr     ADD     IsHoliday       INTEGER;
ALTER TABLE    CalendarAttr     ADD     HolidayForm     TEXT;
ALTER TABLE    ReportAttr       ADD     RetryInterval        INTEGER;
ALTER TABLE    PackageAttr      ADD     RetryInterval        INTEGER;
ALTER TABLE    EventAttr6       ADD     RetryInterval        INTEGER;
CREATE TABLE RetryTracker (
RetryID     INTEGER     PRIMARY KEY,
ScheduleID  INTEGER,
EntryDate   DATETIME,
RetryNumber INTEGER
);
ALTER TABLE ScheduleOptions     ADD     OtherFrequency      FLOAT;
ALTER TABLE ScheduleOptions     ADD     OtherUnit           VARCHAR(55);
CREATE TABLE OperationAttr (
OperationID                     INTEGER                     PRIMARY KEY,
OperationName                   TEXT
);
CREATE TABLE OperationDays (
DayID                           INTEGER                     PRIMARY KEY,
OperationID                     INTEGER,
DayName                         VARCHAR(25),
DayIndex                        INTEGER,
OpenAt                          VARCHAR(15),
CloseAt                         VARCHAR(15)              
);
ALTER TABLE EventAttr6          ADD     UseOperationHours   INTEGER;
ALTER TABLE EventAttr6          ADD     OperationName       VARCHAR(255);
CREATE TABLE SystemTimeZone (
TimeZone                        VARCHAR(255),
UTCOffset                       FLOAT
);
ALTER TABLE     SMTPServers  ADD             SMTPPort     INTEGER;
ALTER TABLE     EventConditions ADD          SaveAttachments    INTEGER;
ALTER TABLE     EventConditions ADD          AttachmentsPath    TEXT;
ALTER TABLE     Tasks           ADD          EnabledStatus      INTEGER;
UPDATE Tasks    SET EnabledStatus = 1        WHERE EnabledStatus IS NULL;
ALTER TABLE     ReportOptions   ADD         UseDefaultCSVType   INTEGER;
UPDATE ReportOptions    SET UseDefaultCSVType = 1 WHERE UseDefaultCSVType IS NULL;
CREATE TABLE ReportDurationTracker (
RunID       INTEGER,
ReportID    INTEGER,
StartDate   DATETIME,
EndDate     DATETIME
);
CREATE TABLE ReportDuration (
RunID       INTEGER PRIMARY KEY,
ReportID    INTEGER,
EntryDate   DATETIME
);
ALTER TABLE ThreadManager       ADD ProcessID           INTEGER;
ALTER TABLE ThreadManager       ADD DataCount           INTEGER;
ALTER TABLE PackageAttr         ALTER COLUMN MergePDFName TEXT;
ALTER TABLE PackageAttr         ALTER COLUMN MergeXLName  TEXT;
ALTER TABLE BlankReportAlert ADD    CustomQueryDetails  TEXT;
ALTER TABLE TaskManager         ADD EventID             INTEGER;
UPDATE TaskManager SET EventID = 0 WHERE EventID IS NULL;
ALTER TABLE EmailLog            ADD MessageFile         TEXT;
CREATE Table HistoryDetailAttr (
DetailID    INTEGER     PRIMARY KEY,
HistoryID   INTEGER,
ReportID    INTEGER,
EntryDate   DATETIME,
Success     INTEGER,
ErrMsg      TEXT,
StartDate   DATETIME
);
ALTER TABLE EventHistory ADD MasterHistoryID    INTEGER;
ALTER TABLE ReportAttr ADD ParseReportFields INTEGER;
ALTER TABLE SendWait    ADD MessageFile        TEXT;
ALTER TABLE SubReportLogin  ADD RptDatabaseType VARCHAR(25);
ALTER TABLE RetryTracker    ADD EventID         INTEGER;
DELETE FROM PrinterAttr WHERE DestinationID NOT IN (SELECT DestinationID FROM DestinationAttr);
ALTER TABLE EmailLog    ALTER COLUMN   ScheduleName TEXT;
ALTER TABLE DynamicAttr ADD     AutoResume      INTEGER;
ALTER TABLE DynamicAttr ADD     ExpireCacheAfter    INTEGER;
ALTER TABLE SMTPServers ADD     SMTPPOPLogin        INTEGER;
ALTER TABLE SMTPServers ADD     SMTPPOPServer       VARCHAR(55);
ALTER TABLE SMTPServers ADD     SMTPPOPUser         VARCHAR(55);
ALTER TABLE SMTPServers ADD     SMTPPOPPassword     VARCHAR(55);
ALTER TABLE SMTPServers ADD     SMTPPOPPort         INTEGER;
ALTER TABLE SMTPServers ADD     SMTPPOPSSL          INTEGER;
ALTER TABLE DataItems   ADD     ReplaceNull         INTEGER;
ALTER TABLE DataItems   ADD     ReplaceNullValue    VARCHAR(255);
ALTER TABLE DataItems   ADD     ReplaceEOF          INTEGER;
ALTER TABLE DataItems   ADD     ReplaceEOFValue     VARCHAR(255);
ALTER TABLE DataDrivenAttr  ADD GroupReports        INTEGER;
ALTER TABLE EventAttr6  ADD     SchedulePriority    VARCHAR(25);
UPDATE EventAttr6 SET SchedulePriority = '3 - Normal' WHERE (SchedulePriority IS NULL OR SchedulePriority ='');
;
ALTER TABLE     ReportParameter ALTER   COLUMN ParType  VARCHAR(25);
CREATE Table ReportDatasource (
DatasourceID     INTEGER PRIMARY KEY,
ReportID         INTEGER,
DatasourceName   VARCHAR(255),
RptUserID        VARCHAR(55),
RptPassword      VARCHAR(55)
);
UPDATE EventAttr6 SET PackID = 0 WHERE PackID IS NULL;
ALTER TABLE    ReportParameter  ADD     MultiValue  VARCHAR(25);
ALTER TABLE    PackageAttr      ALTER COLUMN MergePDFName   TEXT;
ALTER TABLE    PackageAttr      ALTER COLUMN MergeXLName   TEXT;
ALTER TABLE     DataItems	ALTER COLUMN    ConString    TEXT;
ALTER TABLE    DataItems    ADD     NullReturn      INTEGER;
ALTER TABLE DestinationAttr ADD FTPPassive          VARCHAR(255);
ALTER TABLE Tasks           ADD FTPPassive          INTEGER;
ALTER TABLE DestinationAttr ADD FTPOptions         TEXT;
ALTER TABLE Tasks           ADD FTPOptions         TEXT;
DELETE FROM HistoryDetailAttr WHERE HistoryID NOT IN (SELECT HistoryID FROM ScheduleHistory) AND HistoryID NOT IN (SELECT HistoryID FROM EventHistory);
ALTER TABLE PackageAttr		ADD	IsDataDriven		INTEGER;
ALTER TABLE DataDrivenAttr	ADD PackID				INTEGER;
ALTER TABLE SMTPServers     ADD SmtpSSL             INTEGER;
ALTER TABLE SMTPServers     ADD SmtpStartTLS        INTEGER;
ALTER TABLE SMTPServers     ADD SMTPAuthMode        VARCHAR(25);
ALTER TABLE DataDrivenAttr  ADD KeyColumn           VARCHAR(255);
ALTER TABLE EventConditions ADD CacheFolderName     VARCHAR(255);
ALTER TABLE DestinationAttr ADD SenderName			TEXT;
ALTER TABLE DestinationAttr ADD SenderAddress		TEXT;
ALTER TABLE Tasks ADD SenderName			TEXT;
ALTER TABLE Tasks ADD SenderAddress			TEXT;
ALTER TABLE TaskManager		ADD ProcessID			INTEGER;
CREATE TABLE TaskStatusTracker (
ProcessID       INTEGER PRIMARY KEY,
Status          TEXT,
EntryDate       DATETIME
);
ALTER TABLE DataDrivenAttr ADD MergeAllPDF INTEGER;
ALTER TABLE DataDrivenAttr ADD MergeAllPDFName VARCHAR(255);
ALTER TABLE DataDrivenAttr ADD MergeAllXL INTEGER;
ALTER TABLE DataDrivenAttr ADD MergeAllXLName VARCHAR(255);
CREATE TABLE FaxQueue (
TrackerID	INTEGER	PRIMARY KEY,
ProcessID	INTEGER,
EntryDate	DATETIME
);
CREATE TABLE SchedulerAttr (
WorkstationID	VARCHAR(65)	PRIMARY KEY
);
ALTER TABLE PackageAttr ADD Multithreaded        INTEGER;
ALTER TABLE ReportOptions ADD XMLWriteHierachy  INTEGER;
ALTER TABLE ReportOptions ADD XMLMode           INTEGER;
ALTER TABLE ReportOptions ADD XMLTableName		VARCHAR(255);
CREATE TABLE ThreadManagerDetail (
ProcessID   INTEGER,
ReportID    INTEGER,
IsThread	INTEGER
);
ALTER TABLE ReportOptions ADD rendername VARCHAR(255);
ALTER TABLE DestinationAttr ALTER COLUMN FTPServer     TEXT;
ALTER TABLE DestinationAttr ALTER COLUMN FTPUserName   TEXT;
ALTER TABLE DestinationAttr ALTER COLUMN FTPPassword   TEXT;
ALTER TABLE DestinationAttr ALTER COLUMN FTPPath       TEXT;
CREATE TABLE EventLogAttr (
EventID			INTEGER PRIMARY KEY,
PCName			VARCHAR(255),
ScheduleName	VARCHAR(255),
EntryDate		DATETIME,
ErrorDesc		TEXT,
ErrorNumber		VARCHAR(25),
ErrorSource		VARCHAR(255),
ErrorLine		INTEGER,
ErrorSuggestion	TEXT,
ErrorSeverity	INTEGER
);
ALTER TABLE ReportOptions ADD csvengine VARCHAR(25);
ALTER TABLE GroupPermissions ALTER COLUMN PermDesc VARCHAR(255);
ALTER TABLE ReadReceiptsAttr ADD useimap INTEGER;
ALTER TABLE ReadReceiptsAttr ADD imapport INTEGER;
ALTER TABLE ReadReceiptsAttr ADD imapssl  INTEGER;
ALTER TABLE ReportOptions	ADD textengine				VARCHAR(25);
ALTER TABLE ReportOptions	ADD	textdpi					INTEGER;
ALTER TABLE ReportOptions	ADD	usetextdriverdefault	INTEGER;
ALTER TABLE ReportOptions	ADD	formattingstyle			VARCHAR(25);
ALTER TABLE ReportOptions	ADD	characterset			INTEGER;
ALTER TABLE ReportOptions	ADD	eolstyle				INTEGER;
ALTER TABLE ReportOptions	ADD	whitespacesize			INTEGER;
ALTER TABLE ReportOptions	ADD	insertbreaks			INTEGER;  
ALTER TABLE EventConditions ADD runforeachfile			INTEGER;
ALTER TABLE Tasks ADD CPUPriority				VARCHAR(25);
ALTER TABLE Tasks ADD WaitForProcExit			INTEGER;
ALTER TABLE Tasks ADD ProcWaitLimit				INTEGER;
ALTER TABLE EventConditions	ADD Status			INTEGER;
UPDATE EventConditions SET Status = 1 WHERE Status IS NULL;
ALTER TABLE ReportOptions ADD csvIgnoreHeaders INTEGER;
ALTER TABLE ReportOptions ADD	TimePerPage INTEGER;
ALTER TABLE CRDUsers ADD lokiEnabled INTEGER;
ALTER TABLE DomainAttr ADD lokiEnabled INTEGER;	
ALTER TABLE scheduleattr ADD locked	INTEGER;
ALTER TABLE eventattr6	ADD	locked INTEGER;
ALTER TABLE eventattr6 ADD doprocess INTEGER;
ALTER TABLE packageattr ADD mergetext INTEGER;
ALTER TABLE packageattr ADD mergetextname VARCHAR(255);
CREATE TABLE ExecuteNowQue (
    ScheduleID	INTEGER  PRIMARY KEY,
    EntryDate	DATETIME,
    ScheduleType VARCHAR(255)
);
ALTER TABLE EventConditions ADD MatchExactCriteria		INTEGER;
ALTER TABLE executeNowQue ADD orderid INTEGER;
ALTER TABLE executenowque ADD userid VARCHAR(55);
ALTER TABLE executenowque ADD schedulename VARCHAR(55);
CREATE TABLE DefaultParameterValues (
	parametername	VARCHAR(255) PRIMARY KEY,
	parametervalue	TEXT
);
ALTER TABLE eventpackageattr ADD autofailafter INTEGER;
CREATE TABLE collaboratorsattr (
collaboid	INTEGER PRIMARY KEY,
servername	VARCHAR(255),
serverpath	TEXT,
username	VARCHAR(255),
userpassword	VARCHAR(255),
domain		VARCHAR(255)
);
ALTER TABLE scheduleattr ADD collaborationserver INTEGER;
ALTER TABLE groupattr ADD destinationaccesstype	VARCHAR(15);
CREATE TABLE groupdefaultdestinationsattr (
	attrid			INTEGER,
	groupid			INTEGER,
	destinationid	INTEGER
);
CREATE TABLE groupdestinationtypesattr (
	attrid		INTEGER,
	groupid		INTEGER,
	destinationtype	VARCHAR (25)
);
ALTER TABLE destinationattr ADD defaultlinkid INTEGER;
CREATE TABLE userdefaultsattr (
	defaultid		INTEGER PRIMARY KEY,
	emailsubject	TEXT,
	emailattachment	TEXT,
	emailmessage	TEXT,
	emailsignature	TEXT,
	dbUserid		VARCHAR(55),
	dbpassword		VARCHAR(55)
);
CREATE TABLE groupBlackoutAttr (
blackoutid			INTEGER PRIMARY KEY,
groupid				INTEGER,
operationhrsid		INTEGER
);
CREATE TABLE groupTaskTypesAttr (
attrid		INTEGER PRIMARY KEY,
groupid		INTEGER,
tasktype	VARCHAR(255)
);
ALTER TABLE groupattr ADD customtaskaccesstype	VARCHAR(15);
ALTER TABLE scheduleattr ADD collaborationServer INTEGER;
CREATE TABLE collaboratorsAttr (
collaboID	INTEGER PRIMARY KEY,
serverName	VARCHAR(255),
serverPath	TEXT,
username	VARCHAR(255),
userpassword	VARCHAR(255),
domain		VARCHAR(255)
);
CREATE TABLE deferdeliveryDetailAttr (
    detailID    INTEGER PRIMARY KEY,
    deferID     INTEGER,
    sendTo      TEXT,
    sendCC      TEXT,
    sendBcc     TEXT,
    subject     VARCHAR(255),
    body        TEXT,
    senderName  VARCHAR(255),
    senderAddress   VARCHAR(255),
    deliveryDevice    TEXT,
    ftpServer   VARCHAR(255),
    ftpUsername VARCHAR(255),
    ftpPassword VARCHAR(255),
    ftpPath     VARCHAR(255)
);
ALTER TABLE blankReportAlert ADD blankReportFormat VARCHAR(55);
ALTER TABLE packageattr ADD mergetext INTEGER;
ALTER TABLE packageattr ADD mergetextname VARCHAR(255);
ALTER TABLE reportattr ADD groupSelectionFormula	TEXT;
ALTER TABLE reportoptions ADD crystalreadonly INTEGER;
ALTER TABLE CRDUSers	ADD byteKey INTEGER;
CREATE TABLE messageQAttr (
	qid		INTEGER	PRIMARY KEY,
	qname	VARCHAR(255)
);
CREATE TABLE messageAttr (
	messageID	VARCHAR(255) PRIMARY KEY,
	qid			INTEGER,
	messageBody	TEXT,
	Unread		INTEGER,
	entryDate	DATETIME
);
ALTER TABLE tasks ALTER COLUMN printername VARCHAR(500);
ALTER TABLE tasks ALTER COLUMN [Msg] TEXT;
CREATE TABLE portListnerDataAttr (
	entryid		VARCHAR(85) PRIMARY KEY,
	entrydate	DATETIME,
	conditionid	INTEGER,
	dataRecvd	TEXT
);
ALTER TABLE schedulerattr ADD	lastpingtime DATETIME;
ALTER TABLE reportoptions ADD	expirepdf INTEGER;
ALTER TABLE reportoptions ADD	pdfexpirydate DATETIME;
ALTER TABLE packageoptions ADD	expirepdf INTEGER;
ALTER TABLE packageoptions ADD	pdfexpirydate DATETIME;
ALTER TABLE tasks			ADD forexception INTEGER;
ALTER TABLE portListnerDataAttr ADD serverPort INTEGER;
ALTER TABLE portListnerDataAttr ADD serverIP VARCHAR(55);
ALTER TABLE taskoptions ADD expirepdf INTEGER;
ALTER TABLE taskoptions ADD pdfexpirydate	DATETIME;
ALTER TABLE taskoptions ADD makereadonly	INTEGER;
ALTER TABLE blankReportAlert ADD allReportsBlankOnly INTEGER;
ALTER TABLE folders ADD owner VARCHAR(55);
UPDATE folders SET owner ='SQLRDAdmin' WHERE owner IS NULL;
ALTER TABLE DataItems			ADD				FieldDelimiter			VARCHAR(55);
ALTER TABLE DataItems			ADD				UseForParameters		INTEGER;
ALTER TABLE BlankReportAlert	ADD				UseFileSizeCheck		INTEGER;
ALTER TABLE BlankReportAlert	ADD				FileSizeMinimum			INTEGER;
ALTER TABLE messageAttr			ADD				processID				INTEGER;
ALTER TABLE reportparameter		ADD				parIndex				INTEGER;
ALTER TABLE destinationattr		ADD				instantDelivery			INTEGER;
ALTER TABLE reportAttr			ADD				rptFormsAuth			INTEGER;
UPDATE reportAttr SET rptFormsAuth = 0 WHERE rptFormsAuth IS NULL;
CREATE TABLE outputTracker (
	column1		VARCHAR(255),
	column2		VARCHAR(255)
	);
ALTER TABLE packageattr ADD mergeXLIntoWorksheet INTEGER;
ALTER TABLE reportoptions ADD freezepanes VARCHAR(55);
ALTER TABLE packageoptions ADD freezepanes VARCHAR(55);
CREATE TABLE groupFolderAttr (
	accessid	VARCHAR (55) PRIMARY KEY,
	groupid		INTEGER,
	folderid	INTEGER,
	accesstype	VARCHAR (55)
);
ALTER TABLE groupattr ADD folderaccesstype	INTEGER;
ALTER TABLE schedulehistory ADD serverName VARCHAR(255);
CREATE TABLE datasetAttr (
datasetID	INTEGER PRIMARY KEY,
taskID		INTEGER,
datasetName	VARCHAR(255),
conString	VARCHAR(255),
sqlText		TEXT,
ordernumber	INTEGER
);
ALTER TABLE FormulaAttr ADD EvaluateFunctions INTEGER;
ALTER TABLE eventattr6 ADD existingasync INTEGER;
ALTER TABLE eventschedule ADD ordernumber INTEGER;
ALTER TABLE eventattr6 ADD maxthreads INTEGER;
CREATE TABLE dropBoxAttr (
accountName		VARCHAR(255) PRIMARY KEY,
tokenValue		TEXT,
secretValue		TEXT
);
ALTER TABLE tasks ADD starttime VARCHAR(55);
ALTER TABLE tasks ADD endtime VARCHAR(55);
ALTER TABLE reportparameter ADD parvaluelabel VARCHAR(8000);
ALTER TABLE packageoptions ADD worksheetname VARCHAR(255);
ALTER TABLE ForwardMailAttr ADD AttachMsg		INTEGER;
ALTER TABLE ForwardMailAttr ADD SenderName		VARCHAR(255);
ALTER TABLE ForwardMailAttr ADD SenderAddress	VARCHAR(255);
ALTER TABLE executenowque ALTER COLUMN schedulename VARCHAR(255);
ALTER TABLE eventconditions ADD downloademails INTEGER;
ALTER TABLE eventconditions ADD ignoreoriginaltext INTEGER;
ALTER TABLE reportparameter ADD parameterprompt VARCHAR(500);
