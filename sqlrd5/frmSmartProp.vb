Public Class frmSmartProp
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim SmartFolderID As Integer
    Friend WithEvents stabSmartFolder As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabGeneral As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel5 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabHistory As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabOutput As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabSchedule As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabDefinition As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents StyleManager1 As DevComponents.DotNetBar.StyleManager
    Friend WithEvents UcScheduleUpdate As sqlrd.ucSchedule
    Friend WithEvents scheduleHistory As sqlrd.ucScheduleHistory
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Dim ScheduleID As Integer

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdApply As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDesc As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lsvCriteria As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents cmdAddWhere As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdRemoveWhere As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmbColumn As System.Windows.Forms.ComboBox
    Friend WithEvents cmbOperator As System.Windows.Forms.ComboBox
    Friend WithEvents cmbValue As System.Windows.Forms.ComboBox
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmbAnyAll As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents UcDest As sqlrd.ucDestination
    Friend WithEvents imgSingle As System.Windows.Forms.ImageList
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSmartProp))
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lsvCriteria = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmdAddWhere = New DevComponents.DotNetBar.ButtonX()
        Me.cmdRemoveWhere = New DevComponents.DotNetBar.ButtonX()
        Me.cmbColumn = New System.Windows.Forms.ComboBox()
        Me.cmbOperator = New System.Windows.Forms.ComboBox()
        Me.cmbValue = New System.Windows.Forms.ComboBox()
        Me.cmbAnyAll = New System.Windows.Forms.ComboBox()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.txtDesc = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.cmdApply = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.UcDest = New sqlrd.ucDestination()
        Me.imgSingle = New System.Windows.Forms.ImageList(Me.components)
        Me.StyleManager1 = New DevComponents.DotNetBar.StyleManager(Me.components)
        Me.stabSmartFolder = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.UcScheduleUpdate = New sqlrd.ucSchedule()
        Me.tabSchedule = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabGeneral = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabOutput = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabDefinition = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel5 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.scheduleHistory = New sqlrd.ucScheduleHistory()
        Me.tabHistory = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GroupBox2.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.stabSmartFolder, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabSmartFolder.SuspendLayout()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel4.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuperTabControlPanel5.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.lsvCriteria)
        Me.GroupBox2.Controls.Add(Me.cmdAddWhere)
        Me.GroupBox2.Controls.Add(Me.cmdRemoveWhere)
        Me.GroupBox2.Controls.Add(Me.cmbColumn)
        Me.GroupBox2.Controls.Add(Me.cmbOperator)
        Me.GroupBox2.Controls.Add(Me.cmbValue)
        Me.GroupBox2.Location = New System.Drawing.Point(15, 36)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(416, 320)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        '
        'lsvCriteria
        '
        '
        '
        '
        Me.lsvCriteria.Border.Class = "ListViewBorder"
        Me.lsvCriteria.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvCriteria.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lsvCriteria.FullRowSelect = True
        Me.lsvCriteria.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvCriteria.Location = New System.Drawing.Point(8, 72)
        Me.lsvCriteria.Name = "lsvCriteria"
        Me.lsvCriteria.Size = New System.Drawing.Size(400, 240)
        Me.lsvCriteria.TabIndex = 15
        Me.lsvCriteria.UseCompatibleStateImageBehavior = False
        Me.lsvCriteria.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Criteria"
        Me.ColumnHeader1.Width = 226
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = ""
        Me.ColumnHeader2.Width = 168
        '
        'cmdAddWhere
        '
        Me.cmdAddWhere.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAddWhere.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAddWhere.Image = CType(resources.GetObject("cmdAddWhere.Image"), System.Drawing.Image)
        Me.cmdAddWhere.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddWhere.Location = New System.Drawing.Point(368, 40)
        Me.cmdAddWhere.Name = "cmdAddWhere"
        Me.cmdAddWhere.Size = New System.Drawing.Size(40, 24)
        Me.cmdAddWhere.TabIndex = 14
        '
        'cmdRemoveWhere
        '
        Me.cmdRemoveWhere.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRemoveWhere.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRemoveWhere.Image = CType(resources.GetObject("cmdRemoveWhere.Image"), System.Drawing.Image)
        Me.cmdRemoveWhere.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveWhere.Location = New System.Drawing.Point(256, 40)
        Me.cmdRemoveWhere.Name = "cmdRemoveWhere"
        Me.cmdRemoveWhere.Size = New System.Drawing.Size(40, 24)
        Me.cmdRemoveWhere.TabIndex = 13
        '
        'cmbColumn
        '
        Me.cmbColumn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbColumn.ItemHeight = 13
        Me.cmbColumn.Items.AddRange(New Object() {"Bcc", "Cc", "Description", "EndDate", "ErrMsg", "FolderName", "Frequency", "Keyword", "Message", "NextRun", "ReportTitle", "ScheduleName", "ScheduleType", "SendTo", "StartDate", "StartTime", "Status", "Subject"})
        Me.cmbColumn.Location = New System.Drawing.Point(8, 16)
        Me.cmbColumn.Name = "cmbColumn"
        Me.cmbColumn.Size = New System.Drawing.Size(112, 21)
        Me.cmbColumn.Sorted = True
        Me.cmbColumn.TabIndex = 8
        '
        'cmbOperator
        '
        Me.cmbOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOperator.ItemHeight = 13
        Me.cmbOperator.Items.AddRange(New Object() {"=", "<>", ">=", "<=", ">", "<", "starts with", "ends with", "contains", "does not contain"})
        Me.cmbOperator.Location = New System.Drawing.Point(136, 16)
        Me.cmbOperator.Name = "cmbOperator"
        Me.cmbOperator.Size = New System.Drawing.Size(104, 21)
        Me.cmbOperator.TabIndex = 8
        '
        'cmbValue
        '
        Me.cmbValue.ItemHeight = 13
        Me.cmbValue.Items.AddRange(New Object() {"Any", "All"})
        Me.cmbValue.Location = New System.Drawing.Point(256, 16)
        Me.cmbValue.Name = "cmbValue"
        Me.cmbValue.Size = New System.Drawing.Size(152, 21)
        Me.cmbValue.TabIndex = 8
        '
        'cmbAnyAll
        '
        Me.cmbAnyAll.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbAnyAll.ItemHeight = 13
        Me.cmbAnyAll.Items.AddRange(New Object() {"Any", "All"})
        Me.cmbAnyAll.Location = New System.Drawing.Point(52, 9)
        Me.cmbAnyAll.Name = "cmbAnyAll"
        Me.cmbAnyAll.Size = New System.Drawing.Size(80, 21)
        Me.cmbAnyAll.TabIndex = 12
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label4.BackgroundStyle.Class = ""
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(12, 11)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 16)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Match"
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label5.BackgroundStyle.Class = ""
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(140, 11)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(288, 16)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "of these conditions:"
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.Location = New System.Drawing.Point(26, 22)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(400, 21)
        Me.txtName.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(26, 2)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 23)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Name"
        '
        'txtDesc
        '
        '
        '
        '
        Me.txtDesc.Border.Class = "TextBoxBorder"
        Me.txtDesc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDesc.Location = New System.Drawing.Point(26, 64)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.Size = New System.Drawing.Size(400, 224)
        Me.txtDesc.TabIndex = 5
        Me.txtDesc.Tag = "memo"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label3.BackgroundStyle.Class = ""
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(26, 46)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(184, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Description (optional)"
        '
        'cmdApply
        '
        Me.cmdApply.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdApply.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdApply.Enabled = False
        Me.cmdApply.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdApply.Location = New System.Drawing.Point(599, 3)
        Me.cmdApply.Name = "cmdApply"
        Me.cmdApply.Size = New System.Drawing.Size(75, 23)
        Me.cmdApply.TabIndex = 12
        Me.cmdApply.Text = "&Apply"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(518, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 11
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(437, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 10
        Me.cmdOK.Text = "&OK"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'UcDest
        '
        Me.UcDest.BackColor = System.Drawing.Color.Transparent
        Me.UcDest.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcDest.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDest.Location = New System.Drawing.Point(0, 0)
        Me.UcDest.m_CanDisable = True
        Me.UcDest.m_DelayDelete = False
        Me.UcDest.m_eventBased = False
        Me.UcDest.m_ExcelBurst = False
        Me.UcDest.m_IsDynamic = False
        Me.UcDest.m_isPackage = False
        Me.UcDest.m_IsQuery = False
        Me.UcDest.m_StaticDest = False
        Me.UcDest.Name = "UcDest"
        Me.UcDest.Size = New System.Drawing.Size(583, 362)
        Me.UcDest.TabIndex = 0
        '
        'imgSingle
        '
        Me.imgSingle.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.imgSingle.ImageSize = New System.Drawing.Size(16, 16)
        Me.imgSingle.TransparentColor = System.Drawing.Color.Transparent
        '
        'StyleManager1
        '
        Me.StyleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2010Blue
        '
        'stabSmartFolder
        '
        '
        '
        '
        '
        '
        '
        Me.stabSmartFolder.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabSmartFolder.ControlBox.MenuBox.Name = ""
        Me.stabSmartFolder.ControlBox.Name = ""
        Me.stabSmartFolder.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabSmartFolder.ControlBox.MenuBox, Me.stabSmartFolder.ControlBox.CloseBox})
        Me.stabSmartFolder.Controls.Add(Me.SuperTabControlPanel3)
        Me.stabSmartFolder.Controls.Add(Me.SuperTabControlPanel1)
        Me.stabSmartFolder.Controls.Add(Me.SuperTabControlPanel4)
        Me.stabSmartFolder.Controls.Add(Me.SuperTabControlPanel2)
        Me.stabSmartFolder.Controls.Add(Me.SuperTabControlPanel5)
        Me.stabSmartFolder.Dock = System.Windows.Forms.DockStyle.Top
        Me.stabSmartFolder.Location = New System.Drawing.Point(0, 0)
        Me.stabSmartFolder.Name = "stabSmartFolder"
        Me.stabSmartFolder.ReorderTabsEnabled = True
        Me.stabSmartFolder.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabSmartFolder.SelectedTabIndex = 0
        Me.stabSmartFolder.Size = New System.Drawing.Size(677, 362)
        Me.stabSmartFolder.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.stabSmartFolder.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.stabSmartFolder.TabIndex = 17
        Me.stabSmartFolder.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabGeneral, Me.tabDefinition, Me.tabSchedule, Me.tabOutput, Me.tabHistory})
        Me.stabSmartFolder.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.UcScheduleUpdate)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(94, 0)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(583, 362)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.tabSchedule
        '
        'UcScheduleUpdate
        '
        Me.UcScheduleUpdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UcScheduleUpdate.Location = New System.Drawing.Point(0, 0)
        Me.UcScheduleUpdate.m_collaborationServerID = 0
        Me.UcScheduleUpdate.m_nextRun = "2011-05-12 16:49:16"
        Me.UcScheduleUpdate.m_RepeatUnit = ""
        Me.UcScheduleUpdate.mode = sqlrd.ucSchedule.modeEnum.EDIT
        Me.UcScheduleUpdate.Name = "UcScheduleUpdate"
        Me.UcScheduleUpdate.scheduleID = 0
        Me.UcScheduleUpdate.scheduleStatus = True
        Me.UcScheduleUpdate.sFrequency = "Daily"
        Me.UcScheduleUpdate.Size = New System.Drawing.Size(583, 425)
        Me.UcScheduleUpdate.TabIndex = 0
        '
        'tabSchedule
        '
        Me.tabSchedule.AttachedControl = Me.SuperTabControlPanel3
        Me.tabSchedule.GlobalItem = False
        Me.tabSchedule.Image = CType(resources.GetObject("tabSchedule.Image"), System.Drawing.Image)
        Me.tabSchedule.Name = "tabSchedule"
        Me.tabSchedule.Text = "Schedule"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.txtDesc)
        Me.SuperTabControlPanel1.Controls.Add(Me.Label3)
        Me.SuperTabControlPanel1.Controls.Add(Me.txtName)
        Me.SuperTabControlPanel1.Controls.Add(Me.Label2)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(90, 0)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(587, 362)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabGeneral
        '
        'tabGeneral
        '
        Me.tabGeneral.AttachedControl = Me.SuperTabControlPanel1
        Me.tabGeneral.GlobalItem = False
        Me.tabGeneral.Image = CType(resources.GetObject("tabGeneral.Image"), System.Drawing.Image)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Text = "General"
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.UcDest)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(94, 0)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(583, 362)
        Me.SuperTabControlPanel4.TabIndex = 0
        Me.SuperTabControlPanel4.TabItem = Me.tabOutput
        '
        'tabOutput
        '
        Me.tabOutput.AttachedControl = Me.SuperTabControlPanel4
        Me.tabOutput.GlobalItem = False
        Me.tabOutput.Image = CType(resources.GetObject("tabOutput.Image"), System.Drawing.Image)
        Me.tabOutput.Name = "tabOutput"
        Me.tabOutput.Text = "Output"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.GroupBox2)
        Me.SuperTabControlPanel2.Controls.Add(Me.cmbAnyAll)
        Me.SuperTabControlPanel2.Controls.Add(Me.Label5)
        Me.SuperTabControlPanel2.Controls.Add(Me.Label4)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(94, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(583, 362)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.tabDefinition
        '
        'tabDefinition
        '
        Me.tabDefinition.AttachedControl = Me.SuperTabControlPanel2
        Me.tabDefinition.GlobalItem = False
        Me.tabDefinition.Image = CType(resources.GetObject("tabDefinition.Image"), System.Drawing.Image)
        Me.tabDefinition.Name = "tabDefinition"
        Me.tabDefinition.Text = "Definition"
        '
        'SuperTabControlPanel5
        '
        Me.SuperTabControlPanel5.Controls.Add(Me.scheduleHistory)
        Me.SuperTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel5.Location = New System.Drawing.Point(94, 0)
        Me.SuperTabControlPanel5.Name = "SuperTabControlPanel5"
        Me.SuperTabControlPanel5.Size = New System.Drawing.Size(583, 362)
        Me.SuperTabControlPanel5.TabIndex = 0
        Me.SuperTabControlPanel5.TabItem = Me.tabHistory
        '
        'scheduleHistory
        '
        Me.scheduleHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.scheduleHistory.Location = New System.Drawing.Point(0, 0)
        Me.scheduleHistory.m_filter = False
        Me.scheduleHistory.m_objectID = 0
        Me.scheduleHistory.m_scheduleType = sqlrd.clsMarsScheduler.enScheduleType.SMARTFOLDER
        Me.scheduleHistory.m_showControls = False
        Me.scheduleHistory.m_sortOrder = " ASC "
        Me.scheduleHistory.Name = "scheduleHistory"
        Me.scheduleHistory.Size = New System.Drawing.Size(583, 362)
        Me.scheduleHistory.TabIndex = 0
        '
        'tabHistory
        '
        Me.tabHistory.AttachedControl = Me.SuperTabControlPanel5
        Me.tabHistory.GlobalItem = False
        Me.tabHistory.Image = CType(resources.GetObject("tabHistory.Image"), System.Drawing.Image)
        Me.tabHistory.Name = "tabHistory"
        Me.tabHistory.Text = "History"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdApply)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 364)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(677, 30)
        Me.FlowLayoutPanel1.TabIndex = 18
        '
        'frmSmartProp
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(677, 394)
        Me.ControlBox = False
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.stabSmartFolder)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmSmartProp"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Smart Folder Properties"
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.stabSmartFolder, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabSmartFolder.ResumeLayout(False)
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel4.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel5.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region



    Private Sub frmSmartProp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub cmbColumn_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbColumn.SelectedIndexChanged
        Dim oData As clsMarsData = New clsMarsData
        Dim SQL(3) As String
        Dim oRs As ADODB.Recordset

        cmbValue.Items.Clear()
        cmbOperator.Items.Clear()

        Select Case cmbColumn.Text
            Case "Status", "ScheduleType"
                With cmbOperator.Items
                    .Add("=")
                End With
            Case "Message", "Description", "Keyword", "SendTo", "ReportTitle", "ScheduleName", _
            "Cc", "Bcc", "Subject", "ErrMsg"
                With cmbOperator.Items
                    .Add("STARTS WITH")
                    .Add("ENDS WITH")
                    .Add("CONTAINS")
                    .Add("DOES NOT CONTAIN")
                    .Add("IS EMPTY")
                    .Add("IS NOT EMPTY")
                End With
            Case "ScheduleType", "Frequency", "DestinationType", "PrinterName", _
                 "OutputFormat", "FTPServer", "FTPUserName", "FolderName"

                With cmbOperator.Items
                    .Add("=")
                    .Add("<>")
                    .Add("STARTS WITH")
                    .Add("ENDS WITH")
                    .Add("CONTAINS")
                    .Add("DOES NOT CONTAIN")
                End With
            Case "StartDate", "EndDate", "NextRun"

                With cmbOperator.Items
                    .Add("=")
                    .Add("<>")
                    .Add(">=")
                    .Add("<=")
                    .Add(">")
                    .Add("<")
                End With
            Case Else
                With cmbOperator.Items
                    .Add("=")
                    .Add("<>")
                    .Add(">=")
                    .Add("<=")
                    .Add(">")
                    .Add("<")
                    .Add("STARTS WITH")
                    .Add("ENDS WITH")
                    .Add("CONTAINS")
                    .Add("DOES NOT CONTAIN")
                End With
        End Select

        Select Case cmbColumn.Text.ToLower
            Case "frequency"
                With cmbValue.Items
                    .Add("Daily")
                    .Add("Weekly")
                    .Add("Monthly")
                    .Add("Yearly")
                    .Add("Custom")
                    .Add("None")
                End With
            Case "status"
                With cmbValue.Items
                    .Add("0")
                    .Add("1")
                End With
            Case "destinationtype"
                With cmbValue.Items
                    .Add("Email")
                    .Add("Disk")
                    .Add("Printer")
                    .Add("FTP")
                End With
            Case "printername"
                Dim strItem As String
                Dim oPrint As New System.Drawing.Printing.PrinterSettings

                For Each strItem In oPrint.InstalledPrinters
                    cmbValue.Items.Add(strItem)
                Next
            Case "outputformat"
                With cmbValue.Items
                    .Add("Acrobat Format (*.pdf)")
                    .Add("Crystal Reports (*.rpt)")
                    .Add("CSV (*.csv)")
                    .Add("Data Interchange Format (*.dif)")
                    .Add("dBase II (*.dbf)")
                    .Add("dBase III (*.dbf)")
                    .Add("dBase IV (*.dbf)")
                    .Add("HTML (*.htm)")
                    .Add("Lotus 1-2-3 (*.wk1)")
                    .Add("Lotus 1-2-3 (*.wk3)")
                    .Add("Lotus 1-2-3 (*.wk4)")
                    .Add("Lotus 1-2-3 (*.wks)")
                    .Add("MS Excel - Data Only (*.xls)")
                    .Add("MS Excel 7 (*.xls)")
                    .Add("MS Excel 8 (*.xls)")
                    .Add("MS Excel 97-2000 (*.xls)")
                    .Add("MS Word (*.doc)")
                    .Add("ODBC (*.odbc)")
                    .Add("Rich Text Format (*.rtf)")
                    .Add("Tab Seperated (*.txt)")
                    .Add("Text (*.txt)")
                    .Add("TIFF (*.tif)")
                    .Add("XML (*.xml)")
                End With
            Case "schedulename"
                SQL(0) = "SELECT DISTINCT ReportTitle FROM ReportAttr"
                SQL(1) = "SELECT DISTINCT PackageName FROM PackageAttr"
                SQL(2) = "SELECT DISTINCT AutoName FROM AutomationAttr"
                SQL(3) = "SELECT DISTINCT EventName FROM EventAttr"

                For Each s As String In SQL
                    oRs = clsMarsData.GetData(s)

                    If Not oRs Is Nothing Then
                        Do While oRs.EOF = False
                            cmbValue.Items.Add(oRs.Fields(0).Value)
                            oRs.MoveNext()
                        Loop

                        oRs.Close()
                    End If
                Next
            Case "foldername"
                SQL(0) = "SELECT DISTINCT FolderName FROM Folders"

                oRs = clsMarsData.GetData(SQL(0))

                If Not oRs Is Nothing Then
                    Do While oRs.EOF = False
                        cmbValue.Items.Add(oRs(0).Value)
                        oRs.MoveNext()
                    Loop

                    oRs.Close()

                End If
            Case "scheduletype"
                With cmbValue.Items
                    .Add("Single Schedule")
                    .Add("Package Schedule")
                    .Add("Automation Schedule")
                    .Add("Event-Based Schedule")
                    .Add("Event-Based Packages")
                    .Add("Dynamic Schedules")
                    .Add("Dynamic Packages")
                    .Add("Data-Driven Schedules")
                    .Add("Data-Driven Packages")
                    .Add("Bursting Schedules")
                End With
        End Select
    End Sub



    Public Sub EditSmartFolder(ByVal nID As Integer)
        SmartFolderID = nID

        UcDest.nSmartID = nID
        UcDest.nPackID = 0
        UcDest.nReportID = 0

        With scheduleHistory
            .m_objectID = nID
            .m_scheduleType = clsMarsScheduler.enScheduleType.SMARTFOLDER
        End With

        UcScheduleUpdate.ScheduleID = 99999

        Dim SQL As String
        Dim oData As New clsMarsData
        Dim oRs As ADODB.Recordset

        SQL = "SELECT * FROM SmartFolders WHERE SmartID =" & nID

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("smartname").Value
            txtDesc.Text = oRs("smartdesc").Value
            cmbAnyAll.Text = oRs("smarttype").Value
        End If

        UcDest.sReportTitle = txtName.Text

        oRs.Close()

        SQL = "SELECT * FROM SmartFolderAttr WHERE SmartID =" & nID

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                Dim olsv As ListViewItem = New ListViewItem

                olsv.Text = oRs("smartcolumn").Value

                olsv.SubItems.Add(oRs("smartcriteria").Value)

                lsvCriteria.Items.Add(olsv)

                oRs.MoveNext()
            Loop
            oRs.Close()
        End If

        Try
            UcScheduleUpdate.chkstatus.Checked = False
            UcScheduleUpdate.cmbCollaboration.Enabled = False

            UcScheduleUpdate.loadScheduleInfo(SmartFolderID, clsMarsScheduler.enScheduleType.SMARTFOLDER, txtDesc, Nothing, Nothing)

            UcDest.LoadAll()
        Catch : End Try

        '   oRs.Close()
        Me.ShowDialog()
    End Sub

    Private Sub cmdApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdApply.Click
        Dim SQL As String
        Dim oData As New clsMarsData
        Dim sCol As String
        Dim sVal As String
        Dim I As Integer = 1

        sCol = "SmartName = '" & SQLPrepare(txtName.Text) & "'," & _
            "SmartDesc = '" & SQLPrepare(txtDesc.Text) & "'," & _
            "SmartType = '" & cmbAnyAll.Text & "'"

        SQL = "UPDATE SmartFolders SET " & sCol & _
            " WHERE SmartID = " & SmartFolderID

        clsMarsData.WriteData(SQL)

        clsMarsData.WriteData("DELETE FROM SmartFolderAttr WHERE SmartID = " & SmartFolderID)

        sCol = "SmartDefID,SmartID,SmartColumn,SmartCriteria"

        For Each olsv As ListViewItem In lsvCriteria.Items
            sVal = clsMarsData.CreateDataID("smartfolderattr", "smartdefid") & "," & _
            SmartFolderID & "," & _
            "'" & SQLPrepare(olsv.Text) & "'," & _
            "'" & SQLPrepare(olsv.SubItems(1).Text) & "'"

            SQL = "INSERT INTO SmartFolderAttr (" & sCol & ") VALUES (" & sVal & ")"

            clsMarsData.WriteData(SQL)

            I += 1
        Next

        '_Delay(1)

        With UcScheduleUpdate
            If .chkEndDate.Checked = False Then
                Dim d As Date

                'd = New Date(Now.Year + 1000, Now.Month, Now.Day)
                d = New Date(3004, Now.Month, Now.Day)

                .dtEndDate.Value = d
            End If

            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM ScheduleAttr WHERE SmartID = " & SmartFolderID)
            Dim ScheduleID As Integer = clsMarsData.CreateDataID("scheduleattr", "scheduleid")

            If Not oRs Is Nothing Then
                If oRs.EOF = True Then
                    With UcScheduleUpdate
                        .saveSchedule(SmartFolderID, clsMarsScheduler.enScheduleType.SMARTFOLDER, txtDesc.Text, "")
                    End With
                Else

                    UcScheduleUpdate.updateSchedule(SmartFolderID, clsMarsScheduler.enScheduleType.SMARTFOLDER, txtDesc.Text, "")

                End If

                clsMarsData.WriteData("UPDATE DestinationAttr SET ReportID=0,PackID=0 WHERE SmartID =" & SmartFolderID)

            End If

        End With

        setAndShowMessageOnControl(cmdApply, "Saved!", "", "Your smart folder has been updated successfully")

    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Dim sName As String = txtName.Text

        If txtName.Text.Length = 0 Then
            setError(txtName, "Please enter a name for this smart folder")
            stabSmartFolder.SelectedTab = tabGeneral
            txtName.Focus()
            Return
        ElseIf lsvCriteria.Items.Count = 0 Then
            stabSmartFolder.SelectedTab = tabDefinition
            setError(lsvCriteria, "Please build the selection criteria")
            Return
        ElseIf UcScheduleUpdate.chkstatus.Checked = True And UcDest.lsvDestination.Nodes.Count = 0 Then
            stabSmartFolder.SelectedTab = tabOutput
            setError(UcDest.lsvDestination, "Please add a destination for the schedule")
            Return
        ElseIf UcScheduleUpdate.chkstatus.Checked = True And UcScheduleUpdate.isAllDataValid Then
            Return
        End If

        cmdApply_Click(sender, e)

        Me.Close()

        Dim oUi As New clsMarsUI




        Try
            Dim oWin As Object = oWindow(nWindowCurrent)
            Dim tv As TreeView = oWin.tvSmartFolders

            For Each o As TreeNode In tv.Nodes
                If o.Text = sName Then
                    tv.SelectedNode = o
                End If
            Next
        Catch : End Try
    End Sub

    Private Sub cmdAddWhere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddWhere.Click
        Dim olsv As ListViewItem
        Dim sCriteria As String

        If cmbColumn.Text.Length = 0 Then
            SetError(cmbColumn, "Please select the column for the criteria")
            cmbColumn.Focus()
            Return
        ElseIf cmbOperator.Text.Length = 0 Then
            SetError(cmbOperator, "Please select the operator the criteria")
            cmbOperator.Focus()
            Return
        End If

        olsv = New ListViewItem
        olsv.Text = cmbColumn.Text

        Select Case cmbOperator.Text.ToLower
            Case "starts with"
                sCriteria = "LIKE '" & cmbValue.Text & "%'"
            Case "ends with"
                sCriteria = "LIKE '%" & cmbValue.Text & "'"
            Case "contains"
                sCriteria = "LIKE '%" & cmbValue.Text & "%'"
            Case "does not contain"
                sCriteria = " NOT LIKE '%" & cmbValue.Text & "%'"
            Case "is empty"
                sCriteria = " IS EMPTY"
            Case "is not empty"
                sCriteria = " IS NOT EMPTY"
            Case Else
                sCriteria = cmbOperator.Text & "'" & cmbValue.Text & "'"
        End Select

        olsv.SubItems.Add(sCriteria)

        lsvCriteria.Items.Add(olsv)

        cmdApply.Enabled = True
        SetError(lsvCriteria, String.Empty)
    End Sub

    Private Sub cmdRemoveWhere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveWhere.Click
        If lsvCriteria.SelectedItems.Count = 0 Then Return

        lsvCriteria.SelectedItems(0).Remove()
        cmdApply.Enabled = True
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Close()
    End Sub

    Private Sub cmbAnyAll_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAnyAll.SelectedIndexChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        SetError(sender, String.Empty)
        cmdApply.Enabled = True
    End Sub

    Private Sub txtDesc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDesc.TextChanged
        cmdApply.Enabled = True
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub cmbOperator_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbOperator.SelectedIndexChanged
        If cmbOperator.Text.ToLower.IndexOf("empty") > -1 Then
            cmbValue.Enabled = False
        Else
            cmbValue.Enabled = True
        End If
    End Sub

    Private Sub stabSmartFolder_SelectedTabChanged(ByVal sender As Object, ByVal e As DevComponents.DotNetBar.SuperTabStripSelectedTabChangedEventArgs) Handles stabSmartFolder.SelectedTabChanged
        If e.NewValue.Text = "History" Then
            scheduleHistory.DrawScheduleHistory()
        End If

    End Sub
End Class
