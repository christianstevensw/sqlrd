Imports Microsoft.Win32
Friend Class frmPackWizard
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim nStep As Int32
    Dim sFrequency As String
    Dim oErr As clsMarsUI = New clsMarsUI
    Dim oUI As New clsMarsUI
    Dim oField As TextBox

    Const S1 As String = "Step 1: Package Setup"
    Const S2 As String = "Step 2: Schedule Setup"
    Const S3 As String = "Step 3: Destination Setup"
    Const S4 As String = "Step 4: Add Reports"
    Const S5 As String = "Step 5: Exception Handling"
    Dim HasCancelled As Boolean = True
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Friend WithEvents chkSnapshot As System.Windows.Forms.CheckBox
    Friend WithEvents txtSnapshots As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtAdjustStamp As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDefSubject As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDefMsg As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSignature As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAttachment As System.Windows.Forms.MenuItem
    Friend WithEvents UcError As sqlrd.ucErrorHandler
    Const S6 As String = "Step 6: Custom Tasks"
    Friend WithEvents chkMultithreaded As System.Windows.Forms.CheckBox
    Friend WithEvents txtMergeText As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents chkMergeText As System.Windows.Forms.CheckBox
    Friend WithEvents stabMain As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel5 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabException As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabReports As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabDestinations As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabSchedule As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabGeneral As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel6 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabCustomTasks As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents ucSet As sqlrd.ucSchedule
    Friend WithEvents UcBlankReportX1 As sqlrd.ucBlankReportX
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents ReflectionImage1 As DevComponents.DotNetBar.Controls.ReflectionImage
    Friend WithEvents btnXLSMerge As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnPDFMerge As DevComponents.DotNetBar.ButtonX
    Dim serverUrl As String = ""
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdFinish As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents fbg As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ErrProv As System.Windows.Forms.ErrorProvider
    Friend WithEvents cmdLoc As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtFolder As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Step1 As System.Windows.Forms.Panel
    Friend WithEvents Step2 As System.Windows.Forms.Panel
    Friend WithEvents Step3 As System.Windows.Forms.Panel
    Friend WithEvents Step4 As System.Windows.Forms.Panel
    Friend WithEvents Step5 As System.Windows.Forms.Panel
    Friend WithEvents cmdNext As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lsvReports As System.Windows.Forms.ListView
    Friend WithEvents ReportName As System.Windows.Forms.ColumnHeader
    Friend WithEvents Format As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdAddReport As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdRemoveReport As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Step6 As System.Windows.Forms.Panel
    Friend WithEvents oTask As sqlrd.ucTasks
    Friend WithEvents UcDest As sqlrd.ucDestination
    Friend WithEvents txtDesc As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtKeyWord As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents mnuContacts As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuMAPI As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMARS As System.Windows.Forms.MenuItem
    Friend WithEvents cmdEditReport As DevComponents.DotNetBar.ButtonX
    Friend WithEvents chkMergePDF As System.Windows.Forms.CheckBox
    Friend WithEvents chkMergeXL As System.Windows.Forms.CheckBox
    Friend WithEvents txtMergePDF As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtMergeXL As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbDateTime As System.Windows.Forms.ComboBox
    Friend WithEvents chkDTStamp As System.Windows.Forms.CheckBox
    Friend WithEvents cmdUp As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdDown As DevComponents.DotNetBar.ButtonX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPackWizard))
        Me.cmdFinish = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.fbg = New System.Windows.Forms.FolderBrowserDialog()
        Me.ofd = New System.Windows.Forms.OpenFileDialog()
        Me.ErrProv = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cmdLoc = New DevComponents.DotNetBar.ButtonX()
        Me.txtFolder = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Step1 = New System.Windows.Forms.Panel()
        Me.ReflectionImage1 = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.txtDesc = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtKeyWord = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Step2 = New System.Windows.Forms.Panel()
        Me.ucSet = New sqlrd.ucSchedule()
        Me.Step3 = New System.Windows.Forms.Panel()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.txtAdjustStamp = New System.Windows.Forms.NumericUpDown()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.cmbDateTime = New System.Windows.Forms.ComboBox()
        Me.chkDTStamp = New System.Windows.Forms.CheckBox()
        Me.UcDest = New sqlrd.ucDestination()
        Me.Step4 = New System.Windows.Forms.Panel()
        Me.btnXLSMerge = New DevComponents.DotNetBar.ButtonX()
        Me.btnPDFMerge = New DevComponents.DotNetBar.ButtonX()
        Me.txtMergeText = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.chkMergeText = New System.Windows.Forms.CheckBox()
        Me.chkMultithreaded = New System.Windows.Forms.CheckBox()
        Me.chkSnapshot = New System.Windows.Forms.CheckBox()
        Me.txtSnapshots = New System.Windows.Forms.NumericUpDown()
        Me.chkMergeXL = New System.Windows.Forms.CheckBox()
        Me.txtMergeXL = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdUp = New DevComponents.DotNetBar.ButtonX()
        Me.txtMergePDF = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdDown = New DevComponents.DotNetBar.ButtonX()
        Me.chkMergePDF = New System.Windows.Forms.CheckBox()
        Me.cmdAddReport = New DevComponents.DotNetBar.ButtonX()
        Me.lsvReports = New System.Windows.Forms.ListView()
        Me.ReportName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Format = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmdRemoveReport = New DevComponents.DotNetBar.ButtonX()
        Me.cmdEditReport = New DevComponents.DotNetBar.ButtonX()
        Me.Step5 = New System.Windows.Forms.Panel()
        Me.UcBlankReportX1 = New sqlrd.ucBlankReportX()
        Me.UcError = New sqlrd.ucErrorHandler()
        Me.cmdNext = New DevComponents.DotNetBar.ButtonX()
        Me.Step6 = New System.Windows.Forms.Panel()
        Me.oTask = New sqlrd.ucTasks()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.mnuContacts = New System.Windows.Forms.ContextMenu()
        Me.mnuMAPI = New System.Windows.Forms.MenuItem()
        Me.mnuMARS = New System.Windows.Forms.MenuItem()
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.MenuItem5 = New System.Windows.Forms.MenuItem()
        Me.mnuDefSubject = New System.Windows.Forms.MenuItem()
        Me.mnuDefMsg = New System.Windows.Forms.MenuItem()
        Me.mnuSignature = New System.Windows.Forms.MenuItem()
        Me.mnuAttachment = New System.Windows.Forms.MenuItem()
        Me.stabMain = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabReports = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabDestinations = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabSchedule = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabGeneral = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel5 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabException = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel6 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabCustomTasks = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.DividerLabel1 = New sqlrd.DividerLabel()
        CType(Me.ErrProv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step1.SuspendLayout()
        Me.Step2.SuspendLayout()
        Me.Step3.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.txtAdjustStamp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step4.SuspendLayout()
        CType(Me.txtSnapshots, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step5.SuspendLayout()
        Me.Step6.SuspendLayout()
        CType(Me.stabMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabMain.SuspendLayout()
        Me.SuperTabControlPanel4.SuspendLayout()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel5.SuspendLayout()
        Me.SuperTabControlPanel6.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdFinish
        '
        Me.cmdFinish.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdFinish.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdFinish.Enabled = False
        Me.cmdFinish.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdFinish.Location = New System.Drawing.Point(689, 3)
        Me.cmdFinish.Name = "cmdFinish"
        Me.cmdFinish.Size = New System.Drawing.Size(73, 25)
        Me.cmdFinish.TabIndex = 50
        Me.cmdFinish.Text = "&Finish"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(531, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(73, 25)
        Me.cmdCancel.TabIndex = 48
        Me.cmdCancel.Text = "&Cancel"
        '
        'ofd
        '
        Me.ofd.DefaultExt = "*.mdb"
        Me.ofd.Filter = "MS Access Database|*.mdb|All Files|*.*"
        '
        'ErrProv
        '
        Me.ErrProv.ContainerControl = Me
        Me.ErrProv.Icon = CType(resources.GetObject("ErrProv.Icon"), System.Drawing.Icon)
        '
        'cmdLoc
        '
        Me.cmdLoc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdLoc.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdLoc.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.cmdLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoc.Location = New System.Drawing.Point(500, 46)
        Me.cmdLoc.Name = "cmdLoc"
        Me.cmdLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdLoc.TabIndex = 2
        Me.cmdLoc.Text = "..."
        Me.ToolTip1.SetToolTip(Me.cmdLoc, "Browse")
        '
        'txtFolder
        '
        Me.txtFolder.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtFolder.Border.Class = "TextBoxBorder"
        Me.txtFolder.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFolder.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtFolder.ForeColor = System.Drawing.Color.Blue
        Me.txtFolder.Location = New System.Drawing.Point(118, 46)
        Me.txtFolder.Name = "txtFolder"
        Me.txtFolder.ReadOnly = True
        Me.txtFolder.Size = New System.Drawing.Size(376, 21)
        Me.txtFolder.TabIndex = 1
        Me.txtFolder.Tag = "1"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Label4.ForeColor = System.Drawing.Color.Navy
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 49)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(85, 21)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Create In"
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(118, 19)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(376, 21)
        Me.txtName.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Label2.ForeColor = System.Drawing.Color.Navy
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(208, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Package Name"
        '
        'Step1
        '
        Me.Step1.BackColor = System.Drawing.Color.White
        Me.Step1.Controls.Add(Me.ReflectionImage1)
        Me.Step1.Controls.Add(Me.txtDesc)
        Me.Step1.Controls.Add(Me.Label1)
        Me.Step1.Controls.Add(Me.Label7)
        Me.Step1.Controls.Add(Me.txtKeyWord)
        Me.Step1.Controls.Add(Me.cmdLoc)
        Me.Step1.Controls.Add(Me.txtFolder)
        Me.Step1.Controls.Add(Me.Label4)
        Me.Step1.Controls.Add(Me.txtName)
        Me.Step1.Controls.Add(Me.Label2)
        Me.Step1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Step1.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Step1.ForeColor = System.Drawing.Color.Navy
        Me.HelpProvider1.SetHelpKeyword(Me.Step1, "Packaged_Reports_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.Step1, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step1.Location = New System.Drawing.Point(0, 0)
        Me.Step1.Name = "Step1"
        Me.HelpProvider1.SetShowHelp(Me.Step1, True)
        Me.Step1.Size = New System.Drawing.Size(629, 527)
        Me.Step1.TabIndex = 3
        '
        'ReflectionImage1
        '
        '
        '
        '
        Me.ReflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ReflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.ReflectionImage1.Image = CType(resources.GetObject("ReflectionImage1.Image"), System.Drawing.Image)
        Me.ReflectionImage1.Location = New System.Drawing.Point(501, 78)
        Me.ReflectionImage1.Name = "ReflectionImage1"
        Me.ReflectionImage1.Size = New System.Drawing.Size(128, 213)
        Me.ReflectionImage1.TabIndex = 19
        '
        'txtDesc
        '
        Me.txtDesc.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDesc.Border.Class = "TextBoxBorder"
        Me.txtDesc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDesc.ForeColor = System.Drawing.Color.Black
        Me.txtDesc.Location = New System.Drawing.Point(118, 73)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDesc.Size = New System.Drawing.Size(376, 167)
        Me.txtDesc.TabIndex = 3
        Me.txtDesc.Tag = "memo"
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 74)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(117, 16)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Description (optional)"
        '
        'Label7
        '
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(8, 248)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(104, 21)
        Me.Label7.TabIndex = 17
        Me.Label7.Text = "Keyword (optional)"
        '
        'txtKeyWord
        '
        Me.txtKeyWord.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtKeyWord.Border.Class = "TextBoxBorder"
        Me.txtKeyWord.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKeyWord.ForeColor = System.Drawing.Color.Blue
        Me.txtKeyWord.Location = New System.Drawing.Point(118, 248)
        Me.txtKeyWord.Name = "txtKeyWord"
        Me.txtKeyWord.Size = New System.Drawing.Size(376, 21)
        Me.txtKeyWord.TabIndex = 4
        Me.txtKeyWord.Tag = "memo"
        '
        'Step2
        '
        Me.Step2.BackColor = System.Drawing.Color.White
        Me.Step2.Controls.Add(Me.ucSet)
        Me.Step2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.HelpProvider1.SetHelpKeyword(Me.Step2, "Packaged_Reports_Schedule.htm#Step2")
        Me.HelpProvider1.SetHelpNavigator(Me.Step2, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step2.Location = New System.Drawing.Point(0, 0)
        Me.Step2.Name = "Step2"
        Me.HelpProvider1.SetShowHelp(Me.Step2, True)
        Me.Step2.Size = New System.Drawing.Size(629, 527)
        Me.Step2.TabIndex = 14
        Me.Step2.Visible = False
        '
        'ucSet
        '
        Me.ucSet.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ucSet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ucSet.Location = New System.Drawing.Point(0, 0)
        Me.ucSet.m_collaborationServerID = 0
        Me.ucSet.m_nextRun = "2013-11-26 11:05:17"
        Me.ucSet.m_RepeatUnit = ""
        Me.ucSet.mode = sqlrd.ucSchedule.modeEnum.SETUP
        Me.ucSet.Name = "ucSet"
        Me.ucSet.scheduleID = 0
        Me.ucSet.scheduleStatus = True
        Me.ucSet.sFrequency = "Daily"
        Me.ucSet.Size = New System.Drawing.Size(629, 527)
        Me.ucSet.TabIndex = 0
        '
        'Step3
        '
        Me.Step3.BackColor = System.Drawing.Color.White
        Me.Step3.Controls.Add(Me.GroupBox5)
        Me.Step3.Controls.Add(Me.UcDest)
        Me.Step3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.HelpProvider1.SetHelpKeyword(Me.Step3, "Packaged_Reports_Schedule.htm#Step3")
        Me.HelpProvider1.SetHelpNavigator(Me.Step3, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step3.Location = New System.Drawing.Point(0, 0)
        Me.Step3.Name = "Step3"
        Me.HelpProvider1.SetShowHelp(Me.Step3, True)
        Me.Step3.Size = New System.Drawing.Size(629, 527)
        Me.Step3.TabIndex = 15
        Me.Step3.Visible = False
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox5.Controls.Add(Me.txtAdjustStamp)
        Me.GroupBox5.Controls.Add(Me.Label26)
        Me.GroupBox5.Controls.Add(Me.cmbDateTime)
        Me.GroupBox5.Controls.Add(Me.chkDTStamp)
        Me.GroupBox5.Location = New System.Drawing.Point(8, 443)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(618, 78)
        Me.GroupBox5.TabIndex = 1
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Date/Time Stamp"
        '
        'txtAdjustStamp
        '
        Me.txtAdjustStamp.Enabled = False
        Me.txtAdjustStamp.Location = New System.Drawing.Point(232, 48)
        Me.txtAdjustStamp.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.txtAdjustStamp.Minimum = New Decimal(New Integer() {365, 0, 0, -2147483648})
        Me.txtAdjustStamp.Name = "txtAdjustStamp"
        Me.txtAdjustStamp.Size = New System.Drawing.Size(49, 21)
        Me.txtAdjustStamp.TabIndex = 2
        Me.txtAdjustStamp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(15, 52)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(168, 13)
        Me.Label26.TabIndex = 21
        Me.Label26.Text = "Adjust date/time stamp by (days)"
        '
        'cmbDateTime
        '
        Me.cmbDateTime.Enabled = False
        Me.cmbDateTime.ForeColor = System.Drawing.Color.Blue
        Me.cmbDateTime.ItemHeight = 13
        Me.cmbDateTime.Items.AddRange(New Object() {"ddhh", "ddMM", "ddMMyy", "ddMMyyhhmm", "ddMMyyhhmms", "ddMMyyyy", "ddMMyyyyhhmm", "ddMMyyyyhhmmss", "hhmm", "hhmmss", "MMddyy", "MMddyyhhmm", "MMddyyhhmmss", "MMddyyyy", "MMddyyyyhhmm", "MMddyyyyhhmmss", "yyMMdd", "yyMMddhhmm", "yyMMddhhmmss", "yyyyMMdd", "yyyyMMddhhmm", "yyyyMMddhhmmss", "MMMM"})
        Me.cmbDateTime.Location = New System.Drawing.Point(232, 16)
        Me.cmbDateTime.Name = "cmbDateTime"
        Me.cmbDateTime.Size = New System.Drawing.Size(152, 21)
        Me.cmbDateTime.TabIndex = 1
        '
        'chkDTStamp
        '
        Me.chkDTStamp.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkDTStamp.Location = New System.Drawing.Point(16, 16)
        Me.chkDTStamp.Name = "chkDTStamp"
        Me.chkDTStamp.Size = New System.Drawing.Size(160, 24)
        Me.chkDTStamp.TabIndex = 0
        Me.chkDTStamp.Text = "Append date/time stamp"
        '
        'UcDest
        '
        Me.UcDest.BackColor = System.Drawing.Color.Transparent
        Me.UcDest.destinationsPicker = False
        Me.UcDest.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDest.Location = New System.Drawing.Point(8, 8)
        Me.UcDest.m_CanDisable = True
        Me.UcDest.m_DelayDelete = False
        Me.UcDest.m_eventBased = False
        Me.UcDest.m_ExcelBurst = False
        Me.UcDest.m_IsDynamic = False
        Me.UcDest.m_isPackage = False
        Me.UcDest.m_IsQuery = False
        Me.UcDest.m_StaticDest = False
        Me.UcDest.Name = "UcDest"
        Me.UcDest.Size = New System.Drawing.Size(605, 429)
        Me.UcDest.TabIndex = 0
        '
        'Step4
        '
        Me.Step4.BackColor = System.Drawing.Color.White
        Me.Step4.Controls.Add(Me.btnXLSMerge)
        Me.Step4.Controls.Add(Me.btnPDFMerge)
        Me.Step4.Controls.Add(Me.txtMergeText)
        Me.Step4.Controls.Add(Me.chkMergeText)
        Me.Step4.Controls.Add(Me.chkMultithreaded)
        Me.Step4.Controls.Add(Me.chkSnapshot)
        Me.Step4.Controls.Add(Me.txtSnapshots)
        Me.Step4.Controls.Add(Me.chkMergeXL)
        Me.Step4.Controls.Add(Me.txtMergeXL)
        Me.Step4.Controls.Add(Me.cmdUp)
        Me.Step4.Controls.Add(Me.txtMergePDF)
        Me.Step4.Controls.Add(Me.cmdDown)
        Me.Step4.Controls.Add(Me.chkMergePDF)
        Me.Step4.Controls.Add(Me.cmdAddReport)
        Me.Step4.Controls.Add(Me.lsvReports)
        Me.Step4.Controls.Add(Me.cmdRemoveReport)
        Me.Step4.Controls.Add(Me.cmdEditReport)
        Me.Step4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.HelpProvider1.SetHelpKeyword(Me.Step4, "Packaged_Reports_Schedule.htm#Step4")
        Me.HelpProvider1.SetHelpNavigator(Me.Step4, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step4.Location = New System.Drawing.Point(0, 0)
        Me.Step4.Name = "Step4"
        Me.HelpProvider1.SetShowHelp(Me.Step4, True)
        Me.Step4.Size = New System.Drawing.Size(629, 527)
        Me.Step4.TabIndex = 16
        Me.Step4.Visible = False
        '
        'btnXLSMerge
        '
        Me.btnXLSMerge.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnXLSMerge.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnXLSMerge.Enabled = False
        Me.btnXLSMerge.Location = New System.Drawing.Point(542, 389)
        Me.btnXLSMerge.Name = "btnXLSMerge"
        Me.btnXLSMerge.Size = New System.Drawing.Size(75, 21)
        Me.btnXLSMerge.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnXLSMerge.TabIndex = 19
        Me.btnXLSMerge.Text = ".xls"
        '
        'btnPDFMerge
        '
        Me.btnPDFMerge.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnPDFMerge.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnPDFMerge.Enabled = False
        Me.btnPDFMerge.Location = New System.Drawing.Point(542, 416)
        Me.btnPDFMerge.Name = "btnPDFMerge"
        Me.btnPDFMerge.Size = New System.Drawing.Size(75, 21)
        Me.btnPDFMerge.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnPDFMerge.TabIndex = 20
        Me.btnPDFMerge.Text = ".pdf"
        '
        'txtMergeText
        '
        Me.txtMergeText.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtMergeText.Border.Class = "TextBoxBorder"
        Me.txtMergeText.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtMergeText.Enabled = False
        Me.txtMergeText.ForeColor = System.Drawing.Color.Black
        Me.txtMergeText.Location = New System.Drawing.Point(177, 446)
        Me.txtMergeText.Name = "txtMergeText"
        Me.txtMergeText.Size = New System.Drawing.Size(356, 21)
        Me.txtMergeText.TabIndex = 18
        Me.txtMergeText.Tag = "memo"
        Me.ToolTip1.SetToolTip(Me.txtMergeText, "Enter the resulting PDF file name")
        '
        'chkMergeText
        '
        Me.chkMergeText.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkMergeText.Location = New System.Drawing.Point(8, 440)
        Me.chkMergeText.Name = "chkMergeText"
        Me.chkMergeText.Size = New System.Drawing.Size(163, 32)
        Me.chkMergeText.TabIndex = 17
        Me.chkMergeText.Text = "Merge all TEXT files"
        Me.ToolTip1.SetToolTip(Me.chkMergeText, "All reports in TEXT format will be merged into a single file")
        '
        'chkMultithreaded
        '
        Me.chkMultithreaded.AutoSize = True
        Me.chkMultithreaded.Location = New System.Drawing.Point(8, 478)
        Me.chkMultithreaded.Name = "chkMultithreaded"
        Me.chkMultithreaded.Size = New System.Drawing.Size(195, 17)
        Me.chkMultithreaded.TabIndex = 14
        Me.chkMultithreaded.Text = "Run package using multiple threads"
        Me.chkMultithreaded.UseVisualStyleBackColor = True
        '
        'chkSnapshot
        '
        Me.chkSnapshot.AutoSize = True
        Me.chkSnapshot.Location = New System.Drawing.Point(8, 504)
        Me.chkSnapshot.Name = "chkSnapshot"
        Me.chkSnapshot.Size = New System.Drawing.Size(208, 17)
        Me.chkSnapshot.TabIndex = 10
        Me.chkSnapshot.Text = "Enable snapshots and keep for (days)"
        Me.chkSnapshot.UseVisualStyleBackColor = True
        '
        'txtSnapshots
        '
        Me.txtSnapshots.Enabled = False
        Me.txtSnapshots.Location = New System.Drawing.Point(222, 500)
        Me.txtSnapshots.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtSnapshots.Name = "txtSnapshots"
        Me.txtSnapshots.Size = New System.Drawing.Size(85, 21)
        Me.txtSnapshots.TabIndex = 11
        Me.txtSnapshots.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSnapshots.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'chkMergeXL
        '
        Me.chkMergeXL.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkMergeXL.Location = New System.Drawing.Point(8, 383)
        Me.chkMergeXL.Name = "chkMergeXL"
        Me.chkMergeXL.Size = New System.Drawing.Size(163, 32)
        Me.chkMergeXL.TabIndex = 6
        Me.chkMergeXL.Text = "Merge all Excel files"
        Me.ToolTip1.SetToolTip(Me.chkMergeXL, "All reports in Excel format will be merged into a single Excel workbook")
        '
        'txtMergeXL
        '
        Me.txtMergeXL.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtMergeXL.Border.Class = "TextBoxBorder"
        Me.txtMergeXL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtMergeXL.Enabled = False
        Me.txtMergeXL.ForeColor = System.Drawing.Color.Black
        Me.txtMergeXL.Location = New System.Drawing.Point(177, 389)
        Me.txtMergeXL.Name = "txtMergeXL"
        Me.txtMergeXL.Size = New System.Drawing.Size(356, 21)
        Me.txtMergeXL.TabIndex = 7
        Me.txtMergeXL.Tag = "memo"
        Me.ToolTip1.SetToolTip(Me.txtMergeXL, "Enter the resulting PDF file name")
        '
        'cmdUp
        '
        Me.cmdUp.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdUp.BackColor = System.Drawing.SystemColors.Control
        Me.cmdUp.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdUp.Image = CType(resources.GetObject("cmdUp.Image"), System.Drawing.Image)
        Me.cmdUp.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdUp.Location = New System.Drawing.Point(539, 323)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(75, 24)
        Me.cmdUp.TabIndex = 4
        '
        'txtMergePDF
        '
        Me.txtMergePDF.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtMergePDF.Border.Class = "TextBoxBorder"
        Me.txtMergePDF.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtMergePDF.Enabled = False
        Me.txtMergePDF.ForeColor = System.Drawing.Color.Black
        Me.txtMergePDF.Location = New System.Drawing.Point(177, 416)
        Me.txtMergePDF.Name = "txtMergePDF"
        Me.txtMergePDF.Size = New System.Drawing.Size(356, 21)
        Me.txtMergePDF.TabIndex = 9
        Me.txtMergePDF.Tag = "memo"
        Me.ToolTip1.SetToolTip(Me.txtMergePDF, "Enter the resulting PDF file name")
        '
        'cmdDown
        '
        Me.cmdDown.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdDown.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDown.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdDown.Image = CType(resources.GetObject("cmdDown.Image"), System.Drawing.Image)
        Me.cmdDown.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDown.Location = New System.Drawing.Point(539, 353)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(75, 24)
        Me.cmdDown.TabIndex = 5
        '
        'chkMergePDF
        '
        Me.chkMergePDF.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkMergePDF.Location = New System.Drawing.Point(8, 410)
        Me.chkMergePDF.Name = "chkMergePDF"
        Me.chkMergePDF.Size = New System.Drawing.Size(163, 32)
        Me.chkMergePDF.TabIndex = 8
        Me.chkMergePDF.Text = "Merge all PDF files"
        Me.ToolTip1.SetToolTip(Me.chkMergePDF, "All reports in PDF format will be merged into a single PDF file")
        '
        'cmdAddReport
        '
        Me.cmdAddReport.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAddReport.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAddReport.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAddReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddReport.Location = New System.Drawing.Point(539, 8)
        Me.cmdAddReport.Name = "cmdAddReport"
        Me.cmdAddReport.Size = New System.Drawing.Size(75, 24)
        Me.cmdAddReport.TabIndex = 0
        Me.cmdAddReport.Text = "&Add"
        '
        'lsvReports
        '
        Me.lsvReports.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ReportName, Me.Format})
        Me.lsvReports.ForeColor = System.Drawing.Color.Blue
        Me.lsvReports.FullRowSelect = True
        Me.lsvReports.HideSelection = False
        Me.lsvReports.Location = New System.Drawing.Point(8, 8)
        Me.lsvReports.MultiSelect = False
        Me.lsvReports.Name = "lsvReports"
        Me.lsvReports.Size = New System.Drawing.Size(525, 369)
        Me.lsvReports.TabIndex = 1
        Me.lsvReports.UseCompatibleStateImageBehavior = False
        Me.lsvReports.View = System.Windows.Forms.View.Details
        '
        'ReportName
        '
        Me.ReportName.Text = "Report Name"
        Me.ReportName.Width = 200
        '
        'Format
        '
        Me.Format.Text = "Format"
        Me.Format.Width = 150
        '
        'cmdRemoveReport
        '
        Me.cmdRemoveReport.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRemoveReport.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRemoveReport.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRemoveReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveReport.Location = New System.Drawing.Point(539, 72)
        Me.cmdRemoveReport.Name = "cmdRemoveReport"
        Me.cmdRemoveReport.Size = New System.Drawing.Size(75, 24)
        Me.cmdRemoveReport.TabIndex = 3
        Me.cmdRemoveReport.Text = "&Remove"
        '
        'cmdEditReport
        '
        Me.cmdEditReport.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdEditReport.BackColor = System.Drawing.SystemColors.Control
        Me.cmdEditReport.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdEditReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEditReport.Location = New System.Drawing.Point(539, 40)
        Me.cmdEditReport.Name = "cmdEditReport"
        Me.cmdEditReport.Size = New System.Drawing.Size(75, 24)
        Me.cmdEditReport.TabIndex = 2
        Me.cmdEditReport.Text = "&Edit"
        '
        'Step5
        '
        Me.Step5.BackColor = System.Drawing.Color.White
        Me.Step5.Controls.Add(Me.UcBlankReportX1)
        Me.Step5.Controls.Add(Me.UcError)
        Me.Step5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.HelpProvider1.SetHelpKeyword(Me.Step5, "Packaged_Reports_Schedule.htm#Step5")
        Me.HelpProvider1.SetHelpNavigator(Me.Step5, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step5.Location = New System.Drawing.Point(0, 0)
        Me.Step5.Name = "Step5"
        Me.HelpProvider1.SetShowHelp(Me.Step5, True)
        Me.Step5.Size = New System.Drawing.Size(629, 527)
        Me.Step5.TabIndex = 17
        Me.Step5.Visible = False
        '
        'UcBlankReportX1
        '
        Me.UcBlankReportX1.BackColor = System.Drawing.Color.Transparent
        Me.UcBlankReportX1.blankID = 0
        Me.UcBlankReportX1.blankType = "AlertandReport"
        Me.UcBlankReportX1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.UcBlankReportX1.Location = New System.Drawing.Point(0, 98)
        Me.UcBlankReportX1.m_customDSN = ""
        Me.UcBlankReportX1.m_customPassword = ""
        Me.UcBlankReportX1.m_customUserID = ""
        Me.UcBlankReportX1.m_showAllReportsBlankForTasks = False
        Me.UcBlankReportX1.Name = "UcBlankReportX1"
        Me.UcBlankReportX1.Size = New System.Drawing.Size(629, 429)
        Me.UcBlankReportX1.TabIndex = 6
        '
        'UcError
        '
        Me.UcError.BackColor = System.Drawing.Color.Transparent
        Me.UcError.Location = New System.Drawing.Point(8, 6)
        Me.UcError.m_showFailOnOne = True
        Me.UcError.Name = "UcError"
        Me.UcError.Size = New System.Drawing.Size(594, 86)
        Me.UcError.TabIndex = 5
        '
        'cmdNext
        '
        Me.cmdNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdNext.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(610, 3)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(73, 25)
        Me.cmdNext.TabIndex = 49
        Me.cmdNext.Text = "&Next"
        '
        'Step6
        '
        Me.Step6.BackColor = System.Drawing.Color.White
        Me.Step6.Controls.Add(Me.oTask)
        Me.Step6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.HelpProvider1.SetHelpKeyword(Me.Step6, "Packaged_Reports_Schedule.htm#Step6")
        Me.HelpProvider1.SetHelpNavigator(Me.Step6, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step6.Location = New System.Drawing.Point(0, 0)
        Me.Step6.Name = "Step6"
        Me.HelpProvider1.SetShowHelp(Me.Step6, True)
        Me.Step6.Size = New System.Drawing.Size(629, 527)
        Me.Step6.TabIndex = 20
        '
        'oTask
        '
        Me.oTask.BackColor = System.Drawing.Color.White
        Me.oTask.Dock = System.Windows.Forms.DockStyle.Fill
        Me.oTask.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.oTask.ForeColor = System.Drawing.Color.Navy
        Me.oTask.Location = New System.Drawing.Point(0, 0)
        Me.oTask.m_defaultTaks = False
        Me.oTask.m_eventBased = False
        Me.oTask.m_eventID = 99999
        Me.oTask.m_forExceptionHandling = False
        Me.oTask.m_showAfterType = True
        Me.oTask.m_showExpanded = True
        Me.oTask.Name = "oTask"
        Me.oTask.Size = New System.Drawing.Size(629, 527)
        Me.oTask.TabIndex = 0
        '
        'mnuContacts
        '
        Me.mnuContacts.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuMAPI, Me.mnuMARS})
        '
        'mnuMAPI
        '
        Me.mnuMAPI.Index = 0
        Me.mnuMAPI.Text = "MAPI Address Book"
        '
        'mnuMARS
        '
        Me.mnuMARS.Index = 1
        Me.mnuMARS.Text = "SQL-RD Address Book"
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase, Me.MenuItem5, Me.mnuDefSubject, Me.mnuDefMsg, Me.mnuSignature, Me.mnuAttachment})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 3
        Me.MenuItem5.Text = "-"
        '
        'mnuDefSubject
        '
        Me.mnuDefSubject.Index = 4
        Me.mnuDefSubject.Text = "Default Subject"
        '
        'mnuDefMsg
        '
        Me.mnuDefMsg.Index = 5
        Me.mnuDefMsg.Text = "Default Message"
        '
        'mnuSignature
        '
        Me.mnuSignature.Index = 6
        Me.mnuSignature.Text = "Default Signature"
        '
        'mnuAttachment
        '
        Me.mnuAttachment.Index = 7
        Me.mnuAttachment.Text = "Default Attachment"
        '
        'stabMain
        '
        Me.stabMain.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.stabMain.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabMain.ControlBox.MenuBox.Name = ""
        Me.stabMain.ControlBox.Name = ""
        Me.stabMain.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabMain.ControlBox.MenuBox, Me.stabMain.ControlBox.CloseBox})
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel4)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel1)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel3)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel2)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel5)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel6)
        Me.stabMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.stabMain.ForeColor = System.Drawing.Color.Black
        Me.stabMain.Location = New System.Drawing.Point(0, 0)
        Me.stabMain.Name = "stabMain"
        Me.stabMain.ReorderTabsEnabled = True
        Me.stabMain.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabMain.SelectedTabIndex = 1
        Me.stabMain.Size = New System.Drawing.Size(765, 527)
        Me.stabMain.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.stabMain.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.stabMain.TabIndex = 51
        Me.stabMain.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabGeneral, Me.tabSchedule, Me.tabDestinations, Me.tabReports, Me.tabException, Me.tabCustomTasks})
        Me.stabMain.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        Me.stabMain.Text = "Custom Tasks"
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.Step4)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(629, 527)
        Me.SuperTabControlPanel4.TabIndex = 0
        Me.SuperTabControlPanel4.TabItem = Me.tabReports
        '
        'tabReports
        '
        Me.tabReports.AttachedControl = Me.SuperTabControlPanel4
        Me.tabReports.GlobalItem = False
        Me.tabReports.Image = CType(resources.GetObject("tabReports.Image"), System.Drawing.Image)
        Me.tabReports.Name = "tabReports"
        Me.tabReports.Text = "Reports"
        Me.tabReports.Visible = False
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.Step3)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(629, 527)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.tabDestinations
        '
        'tabDestinations
        '
        Me.tabDestinations.AttachedControl = Me.SuperTabControlPanel3
        Me.tabDestinations.GlobalItem = False
        Me.tabDestinations.Image = CType(resources.GetObject("tabDestinations.Image"), System.Drawing.Image)
        Me.tabDestinations.Name = "tabDestinations"
        Me.tabDestinations.Text = "Destinations"
        Me.tabDestinations.Visible = False
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.Step2)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(629, 527)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.tabSchedule
        Me.SuperTabControlPanel2.Visible = False
        '
        'tabSchedule
        '
        Me.tabSchedule.AttachedControl = Me.SuperTabControlPanel2
        Me.tabSchedule.GlobalItem = False
        Me.tabSchedule.Image = CType(resources.GetObject("tabSchedule.Image"), System.Drawing.Image)
        Me.tabSchedule.Name = "tabSchedule"
        Me.tabSchedule.Text = "Schedule"
        Me.tabSchedule.Visible = False
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.Step1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(629, 527)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabGeneral
        '
        'tabGeneral
        '
        Me.tabGeneral.AttachedControl = Me.SuperTabControlPanel1
        Me.tabGeneral.GlobalItem = False
        Me.tabGeneral.Image = CType(resources.GetObject("tabGeneral.Image"), System.Drawing.Image)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Text = "General"
        '
        'SuperTabControlPanel5
        '
        Me.SuperTabControlPanel5.Controls.Add(Me.Step5)
        Me.SuperTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel5.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel5.Name = "SuperTabControlPanel5"
        Me.SuperTabControlPanel5.Size = New System.Drawing.Size(629, 527)
        Me.SuperTabControlPanel5.TabIndex = 0
        Me.SuperTabControlPanel5.TabItem = Me.tabException
        '
        'tabException
        '
        Me.tabException.AttachedControl = Me.SuperTabControlPanel5
        Me.tabException.GlobalItem = False
        Me.tabException.Image = CType(resources.GetObject("tabException.Image"), System.Drawing.Image)
        Me.tabException.Name = "tabException"
        Me.tabException.Text = "Exception Handling"
        Me.tabException.Visible = False
        '
        'SuperTabControlPanel6
        '
        Me.SuperTabControlPanel6.Controls.Add(Me.Step6)
        Me.SuperTabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel6.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel6.Name = "SuperTabControlPanel6"
        Me.SuperTabControlPanel6.Size = New System.Drawing.Size(629, 527)
        Me.SuperTabControlPanel6.TabIndex = 0
        Me.SuperTabControlPanel6.TabItem = Me.tabCustomTasks
        '
        'tabCustomTasks
        '
        Me.tabCustomTasks.AttachedControl = Me.SuperTabControlPanel6
        Me.tabCustomTasks.GlobalItem = False
        Me.tabCustomTasks.Image = CType(resources.GetObject("tabCustomTasks.Image"), System.Drawing.Image)
        Me.tabCustomTasks.Name = "tabCustomTasks"
        Me.tabCustomTasks.Text = "Custom Tasks"
        Me.tabCustomTasks.Visible = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdFinish)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdNext)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 527)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(765, 31)
        Me.FlowLayoutPanel1.TabIndex = 53
        '
        'DividerLabel1
        '
        Me.DividerLabel1.BackColor = System.Drawing.Color.Transparent
        Me.DividerLabel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(0, 0)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(762, 13)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 25
        '
        'frmPackWizard
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(765, 558)
        Me.Controls.Add(Me.stabMain)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.HelpButton = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPackWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Packaged Schedule Wizard"
        CType(Me.ErrProv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step1.ResumeLayout(False)
        Me.Step2.ResumeLayout(False)
        Me.Step3.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.txtAdjustStamp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step4.ResumeLayout(False)
        Me.Step4.PerformLayout()
        CType(Me.txtSnapshots, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step5.ResumeLayout(False)
        Me.Step6.ResumeLayout(False)
        CType(Me.stabMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabMain.ResumeLayout(False)
        Me.SuperTabControlPanel4.ResumeLayout(False)
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel5.ResumeLayout(False)
        Me.SuperTabControlPanel6.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW As Object = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property

    Private Sub cmdLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoc.Click
        Dim oFolders As frmFolders = New frmFolders
        Dim sFolder(1) As String

        sFolder = oFolders.GetFolder

        If sFolder(0) <> "" And sFolder(0) <> "Desktop" Then
            txtFolder.Text = sFolder(0)
            txtFolder.Tag = sFolder(1)
        End If
    End Sub

    Private Sub frmPackWizard_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            If HasCancelled = True Then
                cmdCancel_Click(Nothing, Nothing)
            End If
        Catch: End Try
    End Sub



    Private Sub frmPackWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        stabMain.SelectedTab = tabGeneral

        HelpProvider1.HelpNamespace = gHelpPath
        If _CheckScheduleCount() = False Then Return

        Dim I As Int32 = 0

        nStep = 1

        'ucSet.cmbRpt.Text = 0

        UcDest.isPackage = True

        'ucSet.EndDate.Value = Now.AddYears(100)

        FormatForWinXP(Me)
        txtName.Focus()


        If MailType <> MarsGlobal.gMailType.MAPI Then
            mnuMAPI.Enabled = False
        End If

        If gParentID > 0 And gParent.Length > 0 Then
            Dim fld As folder = New folder(gParentID)

            txtFolder.Text = fld.getFolderPath
            txtFolder.Tag = gParentID
        End If

        Dim oData As New clsMarsData

        oData.CleanDB()

        setupForDragAndDrop(txtMergePDF)
        setupForDragAndDrop(txtMergeXL)
        setupForDragAndDrop(txtMergeText)

        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.sa1_MultiThreading) = False Then
            chkMultithreaded.Enabled = False
        End If

    End Sub


    Private Sub txtFolder_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFolder.TextChanged
        oErr.ResetError(sender, ErrProv)
    End Sub


    Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Select Case nStep
            Case 6
                Step6.Visible = False
                Step5.Visible = True
                Step5.BringToFront()
                cmdNext.Visible = True
                cmdFinish.Visible = False
                Me.AcceptButton = cmdNext
            Case 5
                Step5.Visible = False
                Step4.Visible = True
                Step4.BringToFront()

            Case 4
                Step4.Visible = False
                Step3.Visible = True
                Step3.BringToFront()
            Case 3
                Step3.Visible = False
                Step2.Visible = True
                Step2.BringToFront()
            Case 2
                Step2.Visible = False
                Step1.Visible = True
                Step1.BringToFront()
                cmdBack.Enabled = False
                txtName.Focus()
            Case Else
                Exit Sub
        End Select

        nStep -= 1
    End Sub

    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click

        Dim oData As New clsMarsData

        Select Case stabMain.SelectedTab.Text
            Case "General"
                If txtName.Text = "" Then
                    ErrProv.SetError(txtName, "Please provide a name for this schedule")
                    txtName.Focus()
                    Exit Sub
                ElseIf txtFolder.Text = "" Then
                    ErrProv.SetError(cmdLoc, "Please select the destination folder")
                    cmdLoc.Focus()
                    Exit Sub
                ElseIf clsMarsUI.candoRename(txtName.Text, Me.txtFolder.Tag, clsMarsScheduler.enScheduleType.PACKAGE) = False Then
                    ErrProv.SetError(txtName, "A package schedule " & _
                    "already exist with this name")
                    txtName.Focus()
                    Return
                End If

                tabSchedule.Visible = True
                stabMain.SelectedTab = tabSchedule
                Step2.Visible = True
            Case "Schedule"
                If ucSet.isAllDataValid = False Then
                    Return
                End If

                tabDestinations.Visible = True
                stabMain.SelectedTab = tabDestinations

                Step3.Visible = True
            Case "Destinations"
                If UcDest.lsvDestination.Nodes.Count = 0 Then
                    ErrProv.SetError(UcDest.lsvDestination, "Please add a destination")
                    Return
                ElseIf Me.chkDTStamp.Checked And Me.cmbDateTime.Text = "" Then
                    ErrProv.SetError(Me.cmbDateTime, "Please specify the date/time format for the stamp")
                    Me.cmbDateTime.Focus()
                    Return
                End If

                tabReports.Visible = True
                stabMain.SelectedTab = tabReports
                cmdAddReport.Focus()
                Step4.Visible = True
            Case "Reports"
                If chkMergePDF.Checked = True And txtMergePDF.Text.Length = 0 Then
                    ErrProv.SetError(txtMergePDF, "Please enter the name of the resulting PDF file")
                    txtMergePDF.Focus()
                    Return
                ElseIf chkMergeXL.Checked = True And txtMergeXL.Text.Length = 0 Then
                    ErrProv.SetError(txtMergeXL, "Please enter the name of the resulting Excel file")
                    txtMergeXL.Focus()
                    Return
                ElseIf chkMergeText.Checked And txtMergeText.Text = "" Then
                    ErrProv.SetError(txtMergeText, "Please enter the name of the resulting Text file")
                    txtMergeText.Focus()
                    Return
                ElseIf chkDTStamp.Checked = True And cmbDateTime.Text.Length = 0 Then
                    ErrProv.SetError(cmbDateTime, "Please select the datetime stamp")
                    cmbDateTime.Focus()
                    Return
                ElseIf lsvReports.Items.Count = 0 Then
                    setError(lsvReports, "Please add a report to the package", ErrorIconAlignment.MiddleRight)
                    cmdAddReport.Focus()
                    Return
                End If

                tabException.Visible = True
                stabMain.SelectedTab = tabException
                UcError.cmbRetry.Focus()
                Step5.Visible = True
            Case "Exception Handling"
                'If UcBlank.chkBlankReport.Checked = True Then
                '    If (UcBlank.optAlert.Checked = True Or UcBlank.optAlertandReport.Checked = True) And _
                '    UcBlank.txtAlertTo.Text.Length = 0 Then
                '        UcBlank.ep.SetError(UcBlank.txtAlertTo, "Please enter the alert recipient")
                '        UcBlank.txtAlertTo.Focus()
                '        Return
                '    End If
                'End If
                'If UcBlank.ValidateEntries = False Then Return

                tabCustomTasks.Visible = True
                stabMain.SelectedTab = tabCustomTasks
                cmdNext.Enabled = False
                cmdFinish.Enabled = True
                Me.AcceptButton = cmdFinish
                Step6.Visible = True
            Case Else
                Exit Sub
        End Select
        nStep += 1
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        oErr.ResetError(sender, ErrProv)
    End Sub

    Private Sub cmdAddReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddReport.Click

        removeError(ErrProv, lsvReports)

        Dim sReturn(2) As String
        Dim oNewReport As New frmPackedReport

        oNewReport = New frmPackedReport

        If serverUrl <> "" Then oNewReport.m_serverUrl = serverUrl

        sReturn = oNewReport.AddReport(lsvReports.Items.Count + 1)

        Me.serverUrl = oNewReport.m_serverUrl

        If sReturn(0) = "" Then Exit Sub

        Dim lsvItem As ListViewItem = New ListViewItem
        Dim lsvSub As ListViewItem.ListViewSubItem = New ListViewItem.ListViewSubItem

        lsvItem.Text = sReturn(0)
        lsvItem.Tag = sReturn(1)
        lsvSub.Text = sReturn(2)

        lsvItem.SubItems.Add(lsvSub)

        lsvReports.Items.Add(lsvItem)
    End Sub

    Private Sub cmdFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdFinish.Click
        Dim oData As clsMarsData = New clsMarsData
        Dim SQL As String
        Dim nPackID As Integer
        Dim WriteSuccess As Boolean
        Dim sPrinters As String = ""
        Dim lsv As ListViewItem
        Dim I As Integer = 1
        'save the package

        oErr.BusyProgress(10, "Saving package data...")

        cmdFinish.Enabled = False

        nPackID = clsMarsData.CreateDataID("PackageAttr", "PackID")

        HasCancelled = False

        SQL = "INSERT INTO PackageAttr(PackID,PackageName,Parent,Retry,AssumeFail," & _
            "CheckBlank,Owner,FailOnOne,MergePDF,MergeXL,MergePDFName,MergeXLName,DateTimeStamp,StampFormat,Dynamic," & _
            "AdjustPackageStamp,AutoCalc,RetryInterval,Multithreaded,MergeText,MergeTextName) VALUES(" & _
            nPackID & "," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            txtFolder.Tag & "," & _
            UcError.cmbRetry.Value & "," & _
            UcError.m_autoFailAfter & "," & _
            Convert.ToInt32(UcBlankReportX1.chkCheckBlankReport.Checked) & "," & _
            "'" & gUser & "'," & _
            Convert.ToInt32(UcError.chkFailonOne.Checked) & "," & _
            Convert.ToInt32(chkMergePDF.Checked) & "," & _
            Convert.ToInt32(chkMergeXL.Checked) & "," & _
            "'" & SQLPrepare(txtMergePDF.Text) & "'," & _
            "'" & SQLPrepare(txtMergeXL.Text) & "'," & _
            Convert.ToInt32(chkDTStamp.Checked) & "," & _
            "'" & SQLPrepare(cmbDateTime.Text) & "',0," & _
            txtAdjustStamp.Value & "," & _
            Convert.ToInt32(UcError.chkAutoCalc.Checked) & "," & _
            UcError.txtRetryInterval.Value & "," & _
            Convert.ToInt32(chkMultithreaded.Checked) & "," & _
            Convert.ToInt32(chkMergeText.Checked) & "," & _
            "'" & SQLPrepare(txtMergeText.Text) & "')"


        If clsMarsData.WriteData(SQL) = False Then Exit Sub

        oErr.BusyProgress(30, "Saving schedule data...")

        Dim ScheduleID As Integer = ucSet.saveSchedule(nPackID, clsMarsScheduler.enScheduleType.PACKAGE, txtDesc.Text, txtKeyWord.Text)

        'save the destination

        oErr.BusyProgress(50, "Saving destination data...")

        SQL = "UPDATE DestinationAttr SET ReportID =0, SmartID = 0, PackID = " & nPackID & " WHERE PackID = 99999"

        WriteSuccess = clsMarsData.WriteData(SQL)

        clsMarsData.WriteData("UPDATE PackageOptions SET PackID =" & nPackID & " WHERE PackID = 99999")

        If WriteSuccess = False Then
            clsMarsData.WriteData("DELETE FROM PackageAttr WHERE PackID = " & nPackID)
            clsMarsData.WriteData("DELETE FROM ScheduleAttr WHERE PackID = " & nPackID)
            Exit Sub
        End If

        oErr.BusyProgress(50, "Saving printers...")

        'save the tasks

        SQL = "UPDATE Tasks SET ScheduleID = " & ScheduleID & " WHERE ScheduleID = 99999"

        clsMarsData.WriteData(SQL)

        UcBlankReportX1.saveInfo(nPackID, clsMarsScheduler.enScheduleType.PACKAGE)

        oErr.BusyProgress(90, "Saving reports data...")

        'update the report for this package
        SQL = "UPDATE ReportAttr SET PackID = " & nPackID & " WHERE PackID = 99999"

        If clsMarsData.WriteData(SQL) = False Then GoTo RollBackTransaction

        SQL = "UPDATE PackagedReportAttr SET PackID = " & nPackID & " WHERE PackID = 99999"

        clsMarsData.WriteData(SQL)

        clsMarsData.WriteData("UPDATE ReportOptions SET DestinationID =0 WHERE DestinationID =99999")

        'sort out snapshots for the package
        If chkSnapshot.Checked = True Then
            Dim sCols, sVals As String

            sCols = "SnapID,PackID,KeepSnap"

            sVals = clsMarsData.CreateDataID("reportsnapshots", "snapid") & "," & _
            nPackID & "," & _
            txtSnapshots.Value

            SQL = "INSERT INTO ReportSnapshots (" & sCols & ") VALUES " & _
            "(" & sVals & ")"

            clsMarsData.WriteData(SQL)
        End If
        If gRole.ToLower <> "administrator" Then
            Dim oUser As New clsMarsUsers

            oUser.AssignView(nPackID, gUser, clsMarsUsers.enViewType.ViewPackage)
        End If

        frmMainWin.itemToSelect = New genericExplorerObject
        frmMainWin.itemToSelect.folderID = txtFolder.Tag
        frmMainWin.itemToSelect.itemName = txtName.Text
        oErr.BusyProgress(, , True)


        clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.PACKAGE, clsMarsAudit.AuditAction.CREATE)

        Me.Close()
        Exit Sub
RollBackTransaction:
        oErr.BusyProgress(, , True)
        clsMarsData.WriteData("DELETE FROM PackageAttr WHERE PackID = " & nPackID)
        clsMarsData.WriteData("DELETE FROM ScheduleAttr WHERE PackID = " & nPackID)
        clsMarsData.WriteData("DELETE FROM DestinationAttr WHERE PackID = " & nPackID)
        clsMarsData.WriteData("DELETE FROM BlankReportAlert WHERE PackID =" & nPackID)
        cmdFinish.Enabled = True
    End Sub

    Dim closeFlag As Boolean
    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        If closeFlag = False Then
            Dim sp As DevComponents.DotNetBar.SuperTooltip = New DevComponents.DotNetBar.SuperTooltip
            Dim spi As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo("Really Cancel?", "",
                                                                                                               "Are you sure you would like to close the wizard? All your progress will be lost. Click Cancel again to confirm.",
                                                                                                           Nothing,
                                                                                                               Nothing,
                                                                                                               DevComponents.DotNetBar.eTooltipColor.Office2003)
            sp.SetSuperTooltip(sender, spi)
            sp.ShowTooltip(sender)
            closeFlag = True
        Else
            clsMarsEvent.currentEventID = 99999
            Dim oCleaner As clsMarsData = New clsMarsData
            oCleaner.CleanDB()

            HasCancelled = False

            Dim SQL As String
            Try
                For Each oitem As ListViewItem In lsvReports.Items
                    SQL = "DELETE FROM ReportParameter WHERE ReportID =" & oitem.Tag

                    clsMarsData.WriteData(SQL)
                Next
            Catch
            End Try

            Me.Close()
        End If


    End Sub

    Private Sub cmdRemoveReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveReport.Click
        If lsvReports.SelectedItems.Count = 0 Then Exit Sub

        Dim oData As clsMarsData = New clsMarsData

        Dim nReportID As Integer

        Dim lsv As ListViewItem = lsvReports.SelectedItems(0)

        nReportID = lsv.Tag

        Dim SQL As String = "DELETE FROM ReportAttr WHERE PackID = 99999 AND " & _
                "ReportID =" & nReportID

        clsMarsData.WriteData(SQL)

        SQL = "DELETE FROM ReportParameter WHERE ReportID=" & nReportID

        clsMarsData.WriteData(SQL)

        lsv.Remove()
    End Sub



    Private Sub cmdEditReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEditReport.Click
        If lsvReports.SelectedItems.Count = 0 Then Return

        Dim oEdit As New frmPackedReport
        Dim sVals() As String

        sVals = oEdit.EditReport(lsvReports.SelectedItems(0).Tag)

        If Not sVals Is Nothing Then
            Dim oItem As ListViewItem = lsvReports.SelectedItems(0)

            oItem.Text = sVals(0)

            oItem.SubItems(1).Text = sVals(1)

            lsvReports.Refresh()
        End If


    End Sub

    Private Sub lsvReports_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvReports.SelectedIndexChanged

    End Sub

    Private Sub lsvReports_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvReports.DoubleClick
        cmdEditReport_Click(sender, e)
    End Sub

    Private Sub chkMergePDF_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMergePDF.CheckedChanged
        If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.pd1_AdvancedPDFPack) Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, sender, getFeatDesc(featureCodes.pd1_AdvancedPDFPack))
            chkMergePDF.Checked = False
            Return
        End If

        txtMergePDF.Enabled = chkMergePDF.Checked

        If chkMergePDF.Checked = True Then
            Dim oOptions As New frmReportOptionsView

            oOptions.packageOptions("PDF")
        End If

        btnPDFMerge.Enabled = chkMergePDF.Checked
    End Sub

    Private Sub chkMergeXL_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMergeXL.CheckedChanged
        If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.xl1_AdvancedXLPack) Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, sender, getFeatDesc(featureCodes.xl1_AdvancedXLPack))
            chkMergeXL.Checked = False
            Return
        End If

        txtMergeXL.Enabled = chkMergeXL.Checked

        If chkMergeXL.Checked = True Then
            Dim oOptions As New frmReportOptionsView

            oOptions.packageOptions("XLS")
        End If

        btnXLSMerge.Enabled = chkMergeXL.Checked
    End Sub

    Private Sub txtMergePDF_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMergePDF.TextChanged
        ErrProv.SetError(sender, String.Empty)
    End Sub

    Private Sub txtMergeXL_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMergeXL.GotFocus, txtMergePDF.GotFocus
        Try
            oField = CType(sender, TextBox)
        Catch : End Try
    End Sub

    Private Sub txtMergeXL_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMergeXL.TextChanged
        ErrProv.SetError(sender, String.Empty)
    End Sub

    Private Sub chkDTStamp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDTStamp.CheckedChanged
        cmbDateTime.Enabled = chkDTStamp.Checked
        txtAdjustStamp.Enabled = chkDTStamp.Checked

        If chkDTStamp.Checked = False Then
            cmbDateTime.Text = String.Empty
            txtAdjustStamp.Value = 0
        Else
            Dim oUI As New clsMarsUI

            cmbDateTime.Text = oUI.ReadRegistry("DefDateTimeStamp", "")

        End If
    End Sub


    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        If lsvReports.SelectedItems.Count = 0 Then Return

        Dim olsv As ListViewItem = lsvReports.SelectedItems(0)
        Dim SelTag As Object = olsv.Tag

        If olsv.Index = 0 Then Return

        Dim olsv2 As ListViewItem = lsvReports.Items(olsv.Index - 1)

        Dim OldID, NewID As Integer

        NewID = olsv2.Text.Split(":")(0)
        OldID = olsv.Text.Split(":")(0)
        Dim NewName As String

        Dim SQL As String
        Dim oData As New clsMarsData

        'update the item being moved up
        NewName = NewID & ":" & olsv.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID = " & NewID & " WHERE ReportID = " & olsv.Tag

        clsMarsData.WriteData(SQL)

        olsv.Text = NewName

        'update the one being moved down
        NewName = OldID & ":" & olsv2.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID =" & OldID & " WHERE ReportID =" & olsv2.Tag

        clsMarsData.WriteData(SQL)

        olsv2.Text = NewName

        SQL = "SELECT * FROM ReportAttr INNER JOIN PackagedReportAttr ON " & _
        "ReportAttr.ReportID = PackagedReportAttr.ReportID WHERE ReportAttr.PackID = 99999 ORDER BY PackOrderID"


        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        lsvReports.Items.Clear()

        Do While oRs.EOF = False
            olsv = New ListViewItem

            olsv.Text = oRs("reporttitle").Value
            olsv.Tag = oRs(0).Value

            olsv.SubItems.Add(oRs("outputformat").Value)

            lsvReports.Items.Add(olsv)
            oRs.MoveNext()
        Loop

        oRs.Close()

        For Each olsv In lsvReports.Items
            If olsv.Tag = SelTag Then
                olsv.Selected = True
                Exit For
            End If
        Next

    End Sub

    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        If lsvReports.SelectedItems.Count = 0 Then Return

        Dim olsv As ListViewItem = lsvReports.SelectedItems(0)
        Dim SelTag As Object = olsv.Tag

        If olsv.Index = lsvReports.Items.Count - 1 Then Return

        Dim olsv2 As ListViewItem = lsvReports.Items(olsv.Index + 1)

        Dim OldID, NewID As Integer

        NewID = olsv2.Text.Split(":")(0)
        OldID = olsv.Text.Split(":")(0)
        Dim NewName As String

        Dim SQL As String
        Dim oData As New clsMarsData

        'update the item being moved down

        NewName = NewID & ":" & olsv.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID =" & NewID & " WHERE ReportID =" & olsv.Tag

        clsMarsData.WriteData(SQL)

        olsv.Text = NewName

        'update the one being moved down
        NewName = OldID & ":" & olsv2.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID = " & OldID & " WHERE ReportID =" & olsv2.Tag

        clsMarsData.WriteData(SQL)

        olsv2.Text = NewName

        SQL = "SELECT * FROM ReportAttr INNER JOIN PackagedReportAttr ON " & _
        "ReportAttr.ReportID = PackagedReportAttr.ReportID WHERE ReportAttr.PackID = 99999 ORDER BY PackOrderID"



        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        lsvReports.Items.Clear()

        Do While oRs.EOF = False
            olsv = New ListViewItem

            olsv.Text = oRs("reporttitle").Value
            olsv.Tag = oRs(0).Value

            olsv.SubItems.Add(oRs("outputformat").Value)

            lsvReports.Items.Add(olsv)
            oRs.MoveNext()
        Loop

        oRs.Close()

        For Each olsv In lsvReports.Items
            If olsv.Tag = SelTag Then
                olsv.Selected = True
                Exit For
            End If
        Next
    End Sub

    Private Sub chkSnapshot_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSnapshot.CheckedChanged
        If Not (IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.sa8_Snapshots)) And chkSnapshot.Checked = True Then
            _NeedUpgrade(gEdition.ENTERPRISEPRO, sender, getFeatDesc(featureCodes.sa8_Snapshots))
            chkSnapshot.Checked = False
            Return
        End If

        txtSnapshots.Enabled = chkSnapshot.Checked
    End Sub
    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        Try
            oField.Undo()
        Catch
        End Try
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next
        oField.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next
        oField.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next
        oField.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next
        oField.SelectedText = String.Empty
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next
        oField.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(0)
        oInsert.m_EventBased = False
        oInsert.m_EventID = 0
        oInsert.m_ParameterList = Nothing
        'oField.SelectedText = oInsert.GetConstants(Me)
        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next
        Dim oItem As New frmDataItems

        'oItem.m_ParameterList = Nothing
        oField.SelectedText = oItem._GetDataItem(0)
    End Sub

    Private Sub chkMultithreaded_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMultithreaded.CheckedChanged
        If Me.chkMultithreaded.Checked And IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.sa1_MultiThreading) = False Then
            _NeedUpgrade(gEdition.ENTERPRISEPROPLUS, sender, getFeatDesc(featureCodes.sa1_MultiThreading))
            Me.chkMultithreaded.Checked = False
        End If
    End Sub

    Private Sub chkMergeText_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkMergeText.CheckedChanged
        txtMergeText.Enabled = chkMergeText.Checked
    End Sub

    Private Function cmdBack() As Object
        Throw New NotImplementedException
    End Function

    Dim inserter As frmInserter = New frmInserter(99999)


    Private Sub stabMain_SelectedTabChanged(sender As Object, e As DevComponents.DotNetBar.SuperTabStripSelectedTabChangedEventArgs) Handles stabMain.SelectedTabChanged
        If stabMain.SelectedTab.Text = "Reports" Then
            If inserter IsNot Nothing AndAlso inserter.IsDisposed = False Then
                inserter.GetConstants(Me)
            Else
                inserter = New frmInserter(99999)

                inserter.GetConstants(Me)
            End If
        Else
            inserter.Hide()
        End If
    End Sub

    Private Sub btnPDFMerge_Click(sender As Object, e As EventArgs) Handles btnPDFMerge.Click
        chkMergePDF_CheckedChanged(Nothing, Nothing)
    End Sub

    Private Sub btnXLSMerge_Click(sender As Object, e As EventArgs) Handles btnXLSMerge.Click
        chkMergeXL_CheckedChanged(Nothing, Nothing)
    End Sub
End Class
