Imports Microsoft.Win32
Friend Class frmBgApp
    Inherits System.Windows.Forms.Form
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents bgTimer As System.Timers.Timer
    Friend WithEvents bgMenu As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents bgIcon As System.Windows.Forms.NotifyIcon
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBgApp))
        Me.bgTimer = New System.Timers.Timer
        Me.bgIcon = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.bgMenu = New System.Windows.Forms.ContextMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        CType(Me.bgTimer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bgTimer
        '
        Me.bgTimer.SynchronizingObject = Me
        '
        'bgIcon
        '
        Me.bgIcon.ContextMenu = Me.bgMenu
        Me.bgIcon.Icon = CType(resources.GetObject("bgIcon.Icon"), System.Drawing.Icon)
        Me.bgIcon.Text = "SQL-RD"
        Me.bgIcon.Visible = True
        '
        'bgMenu
        '
        Me.bgMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Load Editor"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "-"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Exit"
        '
        'frmBgApp
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(635, 498)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmBgApp"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Background Scheduler"
        Me.TransparencyKey = System.Drawing.Color.White
        CType(Me.bgTimer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim sKey As String = "Software\ChristianSteven\" & Application.ProductName
    Private Sub frmBgApp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oUI As clsMarsUI = New clsMarsUI

        bgTimer.Enabled = False

        Me.Visible = False
        Me.Opacity = 0

        Dim nInterval As Integer = oUI.ReadRegistry("Poll", "30")

        nInterval *= 1000

        bgTimer.Interval = nInterval

        RunSchedule()

        bgTimer.Enabled = True
    End Sub

    Private Sub bgTimer_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles bgTimer.Elapsed
        bgTimer.Enabled = False

        RunSchedule()

        bgTimer.Enabled = True
    End Sub

    Private Sub RunSchedule()
        Dim oScheduler As clsMarsScheduler = New clsMarsScheduler

        oScheduler.Schedule()

    End Sub
    Private Sub MenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem3.Click
        bgIcon.Visible = False
        Application.Exit()
    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click
        Dim oProc As Process = New Process

        With oProc
            .StartInfo.FileName = sAppPath & assemblyName
            .Start()
        End With
    End Sub
End Class
