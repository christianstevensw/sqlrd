Imports crd5.clsMarsData

Public Class frmExportWizard
    Inherits DevComponents.DotNetBar.Office2007Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Step1 As System.Windows.Forms.Panel
    Friend WithEvents cmdFinish As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdNext As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Step2 As System.Windows.Forms.Panel
    Friend WithEvents cmdRemove As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdAdd As DevComponents.DotNetBar.ButtonX
    Friend WithEvents grpODBC As System.Windows.Forms.GroupBox
    Friend WithEvents grpDat As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDatPath As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdTempFolder As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ofd As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents cmdTest As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Step3 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents pnProgress As System.Windows.Forms.Panel
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents optDat As System.Windows.Forms.RadioButton
    Friend WithEvents optODBC As System.Windows.Forms.RadioButton
    Friend WithEvents cmbFolders As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents stabExportWiz As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabGeneral As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabDestination As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabConfirmation As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents tvFolders As DevComponents.AdvTree.AdvTree
    Friend WithEvents Node1 As DevComponents.AdvTree.Node
    Friend WithEvents NodeConnector1 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle1 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents ColumnHeader2 As DevComponents.AdvTree.ColumnHeader
    Friend WithEvents lsvSchedules As DevComponents.AdvTree.AdvTree
    Friend WithEvents ColumnHeader1 As DevComponents.AdvTree.ColumnHeader
    Friend WithEvents NodeConnector2 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle2 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents pgb As System.Windows.Forms.ProgressBar
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExportWizard))
        Me.Step1 = New System.Windows.Forms.Panel()
        Me.lsvSchedules = New DevComponents.AdvTree.AdvTree()
        Me.NodeConnector2 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle2 = New DevComponents.DotNetBar.ElementStyle()
        Me.tvFolders = New DevComponents.AdvTree.AdvTree()
        Me.Node1 = New DevComponents.AdvTree.Node()
        Me.NodeConnector1 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle1 = New DevComponents.DotNetBar.ElementStyle()
        Me.cmdAdd = New DevComponents.DotNetBar.ButtonX()
        Me.cmdRemove = New DevComponents.DotNetBar.ButtonX()
        Me.cmdFinish = New DevComponents.DotNetBar.ButtonX()
        Me.cmdNext = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.Step2 = New System.Windows.Forms.Panel()
        Me.grpODBC = New System.Windows.Forms.GroupBox()
        Me.UcDSN = New sqlrd.ucDSN()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.cmbFolders = New System.Windows.Forms.ComboBox()
        Me.grpDat = New System.Windows.Forms.GroupBox()
        Me.cmdTempFolder = New DevComponents.DotNetBar.ButtonX()
        Me.txtDatPath = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.cmdTest = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.optDat = New System.Windows.Forms.RadioButton()
        Me.optODBC = New System.Windows.Forms.RadioButton()
        Me.ofd = New System.Windows.Forms.FolderBrowserDialog()
        Me.Step3 = New System.Windows.Forms.Panel()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.pnProgress = New System.Windows.Forms.Panel()
        Me.pgb = New System.Windows.Forms.ProgressBar()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.stabExportWiz = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabDestination = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabGeneral = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabConfirmation = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.ColumnHeader2 = New DevComponents.AdvTree.ColumnHeader()
        Me.ColumnHeader1 = New DevComponents.AdvTree.ColumnHeader()
        Me.Step1.SuspendLayout()
        CType(Me.lsvSchedules, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tvFolders, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step2.SuspendLayout()
        Me.grpODBC.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.grpDat.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.Step3.SuspendLayout()
        Me.pnProgress.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.stabExportWiz, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabExportWiz.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Step1
        '
        Me.Step1.BackColor = System.Drawing.Color.Transparent
        Me.Step1.Controls.Add(Me.lsvSchedules)
        Me.Step1.Controls.Add(Me.tvFolders)
        Me.Step1.Controls.Add(Me.cmdAdd)
        Me.Step1.Controls.Add(Me.cmdRemove)
        Me.Step1.Location = New System.Drawing.Point(3, 3)
        Me.Step1.Name = "Step1"
        Me.Step1.Size = New System.Drawing.Size(521, 449)
        Me.Step1.TabIndex = 0
        '
        'lsvSchedules
        '
        Me.lsvSchedules.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.lsvSchedules.AllowDrop = True
        Me.lsvSchedules.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.lsvSchedules.BackgroundStyle.Class = "TreeBorderKey"
        Me.lsvSchedules.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvSchedules.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.lsvSchedules.Location = New System.Drawing.Point(289, 3)
        Me.lsvSchedules.MultiSelect = True
        Me.lsvSchedules.Name = "lsvSchedules"
        Me.lsvSchedules.NodesConnector = Me.NodeConnector2
        Me.lsvSchedules.NodeStyle = Me.ElementStyle2
        Me.lsvSchedules.PathSeparator = ";"
        Me.lsvSchedules.Size = New System.Drawing.Size(229, 443)
        Me.lsvSchedules.Styles.Add(Me.ElementStyle2)
        Me.lsvSchedules.TabIndex = 5
        Me.lsvSchedules.Text = "AdvTree1"
        '
        'NodeConnector2
        '
        Me.NodeConnector2.LineColor = System.Drawing.Color.Transparent
        '
        'ElementStyle2
        '
        Me.ElementStyle2.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle2.Name = "ElementStyle2"
        Me.ElementStyle2.TextColor = System.Drawing.SystemColors.ControlText
        '
        'tvFolders
        '
        Me.tvFolders.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.tvFolders.AllowDrop = True
        Me.tvFolders.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.tvFolders.BackgroundStyle.Class = "TreeBorderKey"
        Me.tvFolders.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.tvFolders.DragDropNodeCopyEnabled = False
        Me.tvFolders.ExpandButtonType = DevComponents.AdvTree.eExpandButtonType.Triangle
        Me.tvFolders.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.tvFolders.Location = New System.Drawing.Point(3, 9)
        Me.tvFolders.MultiSelect = True
        Me.tvFolders.Name = "tvFolders"
        Me.tvFolders.Nodes.AddRange(New DevComponents.AdvTree.Node() {Me.Node1})
        Me.tvFolders.NodesConnector = Me.NodeConnector1
        Me.tvFolders.NodeStyle = Me.ElementStyle1
        Me.tvFolders.PathSeparator = ";"
        Me.tvFolders.Size = New System.Drawing.Size(225, 437)
        Me.tvFolders.Styles.Add(Me.ElementStyle1)
        Me.tvFolders.TabIndex = 4
        Me.tvFolders.Text = "tvfolders"
        '
        'Node1
        '
        Me.Node1.Expanded = True
        Me.Node1.Name = "Node1"
        Me.Node1.Text = "Node1"
        '
        'NodeConnector1
        '
        Me.NodeConnector1.LineColor = System.Drawing.Color.Transparent
        '
        'ElementStyle1
        '
        Me.ElementStyle1.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle1.Name = "ElementStyle1"
        Me.ElementStyle1.TextColor = System.Drawing.SystemColors.ControlText
        '
        'cmdAdd
        '
        Me.cmdAdd.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAdd.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAdd.Location = New System.Drawing.Point(239, 160)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(40, 24)
        Me.cmdAdd.TabIndex = 1
        '
        'cmdRemove
        '
        Me.cmdRemove.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRemove.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRemove.Image = CType(resources.GetObject("cmdRemove.Image"), System.Drawing.Image)
        Me.cmdRemove.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemove.Location = New System.Drawing.Point(239, 216)
        Me.cmdRemove.Name = "cmdRemove"
        Me.cmdRemove.Size = New System.Drawing.Size(40, 24)
        Me.cmdRemove.TabIndex = 3
        '
        'cmdFinish
        '
        Me.cmdFinish.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdFinish.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdFinish.Enabled = False
        Me.cmdFinish.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdFinish.Location = New System.Drawing.Point(586, 3)
        Me.cmdFinish.Name = "cmdFinish"
        Me.cmdFinish.Size = New System.Drawing.Size(75, 25)
        Me.cmdFinish.TabIndex = 50
        Me.cmdFinish.Text = "&Finish"
        '
        'cmdNext
        '
        Me.cmdNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdNext.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(505, 3)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(75, 25)
        Me.cmdNext.TabIndex = 49
        Me.cmdNext.Text = "&Next"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(424, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 25)
        Me.cmdCancel.TabIndex = 48
        Me.cmdCancel.Text = "&Cancel"
        '
        'Step2
        '
        Me.Step2.BackColor = System.Drawing.Color.Transparent
        Me.Step2.Controls.Add(Me.grpDat)
        Me.Step2.Controls.Add(Me.grpODBC)
        Me.Step2.Controls.Add(Me.GroupBox3)
        Me.Step2.Controls.Add(Me.cmdTest)
        Me.Step2.Controls.Add(Me.GroupBox5)
        Me.Step2.Location = New System.Drawing.Point(3, 3)
        Me.Step2.Name = "Step2"
        Me.Step2.Size = New System.Drawing.Size(440, 328)
        Me.Step2.TabIndex = 0
        '
        'grpODBC
        '
        Me.grpODBC.Controls.Add(Me.UcDSN)
        Me.grpODBC.Location = New System.Drawing.Point(8, 64)
        Me.grpODBC.Name = "grpODBC"
        Me.grpODBC.Size = New System.Drawing.Size(424, 152)
        Me.grpODBC.TabIndex = 2
        Me.grpODBC.TabStop = False
        Me.grpODBC.Text = "DSN"
        Me.grpODBC.Visible = False
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.Color.Transparent
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN.Location = New System.Drawing.Point(8, 24)
        Me.UcDSN.m_conString = "|||"
        Me.UcDSN.m_showConnectionType = False
        Me.UcDSN.Name = "UcDSN"
        Me.UcDSN.Size = New System.Drawing.Size(400, 122)
        Me.UcDSN.TabIndex = 0
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cmbFolders)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 256)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(424, 56)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Destination Folder Name"
        Me.GroupBox3.Visible = False
        '
        'cmbFolders
        '
        Me.cmbFolders.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFolders.Location = New System.Drawing.Point(8, 24)
        Me.cmbFolders.Name = "cmbFolders"
        Me.cmbFolders.Size = New System.Drawing.Size(168, 21)
        Me.cmbFolders.TabIndex = 0
        '
        'grpDat
        '
        Me.grpDat.Controls.Add(Me.cmdTempFolder)
        Me.grpDat.Controls.Add(Me.txtDatPath)
        Me.grpDat.Controls.Add(Me.Label1)
        Me.grpDat.Location = New System.Drawing.Point(8, 64)
        Me.grpDat.Name = "grpDat"
        Me.grpDat.Size = New System.Drawing.Size(424, 152)
        Me.grpDat.TabIndex = 1
        Me.grpDat.TabStop = False
        Me.grpDat.Text = "SQL-RD Location"
        Me.grpDat.Visible = False
        '
        'cmdTempFolder
        '
        Me.cmdTempFolder.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdTempFolder.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdTempFolder.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTempFolder.Location = New System.Drawing.Point(368, 40)
        Me.cmdTempFolder.Name = "cmdTempFolder"
        Me.cmdTempFolder.Size = New System.Drawing.Size(48, 21)
        Me.cmdTempFolder.TabIndex = 1
        Me.cmdTempFolder.Text = "..."
        '
        'txtDatPath
        '
        '
        '
        '
        Me.txtDatPath.Border.Class = "TextBoxBorder"
        Me.txtDatPath.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDatPath.Location = New System.Drawing.Point(8, 40)
        Me.txtDatPath.Name = "txtDatPath"
        Me.txtDatPath.Size = New System.Drawing.Size(336, 21)
        Me.txtDatPath.TabIndex = 0
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(336, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Please select the SQL-RD assembly (.exe) on the remote machine"
        '
        'cmdTest
        '
        Me.cmdTest.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdTest.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdTest.Location = New System.Drawing.Point(183, 224)
        Me.cmdTest.Name = "cmdTest"
        Me.cmdTest.Size = New System.Drawing.Size(75, 23)
        Me.cmdTest.TabIndex = 2
        Me.cmdTest.Text = "Connect"
        Me.cmdTest.Visible = False
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.optDat)
        Me.GroupBox5.Controls.Add(Me.optODBC)
        Me.GroupBox5.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(424, 48)
        Me.GroupBox5.TabIndex = 0
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Select the destination system's connection type"
        '
        'optDat
        '
        Me.optDat.Location = New System.Drawing.Point(16, 16)
        Me.optDat.Name = "optDat"
        Me.optDat.Size = New System.Drawing.Size(104, 24)
        Me.optDat.TabIndex = 0
        Me.optDat.Text = "File System"
        '
        'optODBC
        '
        Me.optODBC.Location = New System.Drawing.Point(184, 16)
        Me.optODBC.Name = "optODBC"
        Me.optODBC.Size = New System.Drawing.Size(184, 24)
        Me.optODBC.TabIndex = 1
        Me.optODBC.Text = "ODBC/SQL Server"
        '
        'Step3
        '
        Me.Step3.BackColor = System.Drawing.Color.Transparent
        Me.Step3.Controls.Add(Me.Label2)
        Me.Step3.Controls.Add(Me.pnProgress)
        Me.Step3.Location = New System.Drawing.Point(3, 3)
        Me.Step3.Name = "Step3"
        Me.Step3.Size = New System.Drawing.Size(521, 449)
        Me.Step3.TabIndex = 29
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.Location = New System.Drawing.Point(8, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(424, 32)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "You are now ready to export the selected schedules. Press 'Finish' to initiate th" & _
    "e exporting process..."
        Me.Label2.WordWrap = True
        '
        'pnProgress
        '
        Me.pnProgress.Controls.Add(Me.pgb)
        Me.pnProgress.Controls.Add(Me.Label3)
        Me.pnProgress.Controls.Add(Me.PictureBox2)
        Me.pnProgress.Controls.Add(Me.PictureBox3)
        Me.pnProgress.Location = New System.Drawing.Point(8, 177)
        Me.pnProgress.Name = "pnProgress"
        Me.pnProgress.Size = New System.Drawing.Size(504, 88)
        Me.pnProgress.TabIndex = 1
        Me.pnProgress.Visible = False
        '
        'pgb
        '
        Me.pgb.Location = New System.Drawing.Point(81, 40)
        Me.pgb.Name = "pgb"
        Me.pgb.Size = New System.Drawing.Size(343, 16)
        Me.pgb.TabIndex = 3
        '
        'Label3
        '
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.Location = New System.Drawing.Point(81, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(271, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Exporting schedules, please wait..."
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(8, 16)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(67, 69)
        Me.PictureBox2.TabIndex = 0
        Me.PictureBox2.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(433, 16)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(66, 69)
        Me.PictureBox3.TabIndex = 0
        Me.PictureBox3.TabStop = False
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        '
        'stabExportWiz
        '
        '
        '
        '
        '
        '
        '
        Me.stabExportWiz.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabExportWiz.ControlBox.MenuBox.Name = ""
        Me.stabExportWiz.ControlBox.Name = ""
        Me.stabExportWiz.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabExportWiz.ControlBox.MenuBox, Me.stabExportWiz.ControlBox.CloseBox})
        Me.stabExportWiz.Controls.Add(Me.SuperTabControlPanel2)
        Me.stabExportWiz.Controls.Add(Me.SuperTabControlPanel1)
        Me.stabExportWiz.Controls.Add(Me.SuperTabControlPanel3)
        Me.stabExportWiz.Dock = System.Windows.Forms.DockStyle.Top
        Me.stabExportWiz.Location = New System.Drawing.Point(0, 0)
        Me.stabExportWiz.Name = "stabExportWiz"
        Me.stabExportWiz.ReorderTabsEnabled = True
        Me.stabExportWiz.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabExportWiz.SelectedTabIndex = 0
        Me.stabExportWiz.Size = New System.Drawing.Size(664, 455)
        Me.stabExportWiz.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.stabExportWiz.TabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.stabExportWiz.TabIndex = 51
        Me.stabExportWiz.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabGeneral, Me.tabDestination, Me.tabConfirmation})
        Me.stabExportWiz.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        Me.stabExportWiz.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.Step2)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(151, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(513, 455)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.tabDestination
        '
        'tabDestination
        '
        Me.tabDestination.AttachedControl = Me.SuperTabControlPanel2
        Me.tabDestination.GlobalItem = False
        Me.tabDestination.Image = CType(resources.GetObject("tabDestination.Image"), System.Drawing.Image)
        Me.tabDestination.Name = "tabDestination"
        Me.tabDestination.Text = "Destination System"
        Me.tabDestination.Visible = False
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.Step1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(137, 0)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(527, 455)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabGeneral
        '
        'tabGeneral
        '
        Me.tabGeneral.AttachedControl = Me.SuperTabControlPanel1
        Me.tabGeneral.GlobalItem = False
        Me.tabGeneral.Image = CType(resources.GetObject("tabGeneral.Image"), System.Drawing.Image)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Text = "Select Schedules"
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.Step3)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(151, 0)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(513, 455)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.tabConfirmation
        '
        'tabConfirmation
        '
        Me.tabConfirmation.AttachedControl = Me.SuperTabControlPanel3
        Me.tabConfirmation.GlobalItem = False
        Me.tabConfirmation.Image = CType(resources.GetObject("tabConfirmation.Image"), System.Drawing.Image)
        Me.tabConfirmation.Name = "tabConfirmation"
        Me.tabConfirmation.Text = "Do Export"
        Me.tabConfirmation.Visible = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdFinish)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdNext)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 458)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(664, 33)
        Me.FlowLayoutPanel1.TabIndex = 52
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Name = "ColumnHeader2"
        Me.ColumnHeader2.Text = "Name"
        Me.ColumnHeader2.Width.Absolute = 150
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Name = "ColumnHeader1"
        Me.ColumnHeader1.Text = "Name"
        Me.ColumnHeader1.Width.Absolute = 180
        '
        'frmExportWizard
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(664, 491)
        Me.ControlBox = False
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.stabExportWiz)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmExportWizard"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Schedule Export Wizard"
        Me.Step1.ResumeLayout(False)
        CType(Me.lsvSchedules, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tvFolders, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step2.ResumeLayout(False)
        Me.grpODBC.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.grpDat.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.Step3.ResumeLayout(False)
        Me.pnProgress.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.stabExportWiz, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabExportWiz.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim oUI As New clsMarsUI
    Const S1 As String = "Step 1: Select schedules"
    Const S2 As String = "Step 2: Configure Destination System"
    Const S3 As String = "Step 3: Export Schedules"
    Dim nStep As Integer = 1
    Dim RemoteCon As String
    Dim oFolderIDs As ComboBox
    Dim nParent As Integer

    Private Sub frmExportWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        oUI.BuildAdvTree(tvFolders, True)

        tvFolders.Focus()


    End Sub

    Private Sub addFolderContents(fld As folder)
        '//add reports

        Dim oItem As DevComponents.AdvTree.Node

        For Each rpt As cssreport In fld.getReports
            If lsvSchedules.FindNodeByDataKey(rpt.ID) Is Nothing Then
                oItem = New DevComponents.AdvTree.Node
                oItem.Text = rpt.reportName
                oItem.Tag = clsMarsScheduler.enScheduleType.REPORT
                oItem.Image = resizeImage(rpt.reportImage, 16, 16)
                oItem.DataKey = rpt.ID
                lsvSchedules.Nodes.Add(oItem)
            End If
        Next

        '//add packages
        For Each pck As Package In fld.getPackages
            If lsvSchedules.FindNodeByDataKey(pck.ID) Is Nothing Then
                oItem = New DevComponents.AdvTree.Node
                oItem.Text = pck.packageName
                oItem.Tag = clsMarsScheduler.enScheduleType.PACKAGE
                oItem.Image = resizeImage(pck.packageImage, 16, 16)
                oItem.DataKey = pck.ID
                lsvSchedules.Nodes.Add(oItem)
            End If
        Next

        '//add subfolders
        For Each childfld As folder In fld.getSubFolders
            addFolderContents(childfld)
        Next
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If tvFolders.SelectedNode Is Nothing Then Return

        Dim oNode As DevComponents.AdvTree.Node
        Dim oItem As DevComponents.AdvTree.Node
        Dim sType As clsMarsScheduler.enScheduleType

        For Each oNode In tvFolders.SelectedNodes

            sType = oNode.Tag

            If sType = clsMarsScheduler.enScheduleType.SMARTFOLDER Then Return

            If sType = clsMarsScheduler.enScheduleType.FOLDER Then
                Dim res As DialogResult = MessageBox.Show("This will add all Report and Package Schedules in this folder and its subfolders for the export, proceed?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

                If res = Windows.Forms.DialogResult.Yes Then
                    Dim fld As folder = New folder(oNode.DataKey)

                    addFolderContents(fld)
                End If
            Else
                For Each oItem In lsvSchedules.Nodes
                    If oItem.Tag = oNode.Tag And oItem.DataKey = oNode.DataKey Then
                        GoTo skip
                    End If
                Next

                oItem = New DevComponents.AdvTree.Node

                oItem.Text = oNode.Text
                oItem.Tag = oNode.Tag
                oItem.Image = oNode.Image
                oItem.DataKey = oNode.DataKey

                lsvSchedules.Nodes.Add(oItem)
            End If

skip:
        Next
    End Sub

    Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
        If lsvSchedules.SelectedNodes.Count = 0 Then Exit Sub

        Dim nodes As ArrayList = New ArrayList

        For Each n As DevComponents.AdvTree.Node In lsvSchedules.SelectedNodes
            nodes.Add(n)
        Next

        For Each n As DevComponents.AdvTree.Node In nodes
            lsvSchedules.Nodes.Remove(n, DevComponents.AdvTree.eTreeAction.Code)
        Next
    End Sub

    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click
        Select Case stabExportWiz.SelectedTab.Text
            Case "Select Schedules"
                If lsvSchedules.Nodes.Count = 0 Then
                    setError(lsvSchedules, "Please select at least one schedule to export")
                    Return
                End If

                tabDestination.Visible = True
                stabExportWiz.SelectedTab = tabDestination
            Case "Destination System"
                If cmbFolders.Text.Length = 0 Then
                    setError(cmbFolders, "Please select the destination folder")
                    cmbFolders.Focus()
                    Return
                End If

                tabConfirmation.Visible = True
                stabExportWiz.SelectedTab = tabConfirmation

                cmdNext.Enabled = False
                cmdFinish.Enabled = True
        End Select
    End Sub


    Private Sub optDat_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optDat.CheckedChanged
        If optDat.Checked = True Then
            grpDat.Visible = True
            cmdTest.Visible = True
            grpODBC.Visible = False

        End If
    End Sub

    Private Sub optODBC_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optODBC.CheckedChanged
        If optODBC.Checked = True Then
            grpODBC.Visible = True
            cmdTest.Visible = True
            grpDat.Visible = False

        End If
    End Sub

    Private Sub cmdTempFolder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTempFolder.Click
        Dim oRes As DialogResult
        Dim openfd As OpenFileDialog = New OpenFileDialog
        openfd.Filter = "sqlrd|sqlrd.exe|All Files|*.*"
        openfd.Title = "Select SQL-RD on the remote machine"

        If openfd.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
            txtDatPath.Text = IO.Path.GetDirectoryName(openfd.FileName)

            If txtDatPath.Text.EndsWith("\") = False Then
                txtDatPath.Text = txtDatPath.Text & "\"
            End If
        End If

        'With ofd
        '    .Description = "Please select the destination SQL-RD folder (containing the 'crdlive.dat' file)..."
        '    .ShowNewFolderButton = True
        '    oRes = .ShowDialog()

        '    If oRes = Windows.Forms.DialogResult.Cancel Then
        '        Exit Sub
        '    Else
        '        txtDatPath.Text = .SelectedPath

        '        If txtDatPath.Text.EndsWith("\") = False Then
        '            txtDatPath.Text = txtDatPath.Text & "\"
        '        End If
        '    End If
        'End With
    End Sub

    Private Sub cmdTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTest.Click


        If optDat.Checked = True Then
            Dim remotePC As String = txtDatPath.Text.Split("\")(2)

            Dim config As String = "\\" & remotePC & "\sqlrdconfigshare\sqlrdlive.config" 'txtDatPath.Text & "crdlive.config"
            Dim contype As String = clsMarsUI.MainUI.ReadRegistry("ConType", "DAT", , config, True)

            If contype = "DAT" Then
                Dim sPath As String = txtDatPath.Text & "sqlrdlive.dat"

                RemoteCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sPath & ";Persist Security Info=False"
            ElseIf contype = "LOCAL" Then
                RemoteCon = clsMarsUI.MainUI.ReadRegistry("ConString", "", True, config, True)
                RemoteCon = RemoteCon.Replace(".\", remotePC & "\")
            End If
        Else
            With UcDSN
                RemoteCon = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & .txtPassword.Text
            End With
        End If

        Dim TestCon As ADODB.Connection = New ADODB.Connection

        Try
            If optDat.Checked = True Then
                TestCon.Open(RemoteCon)
            Else
                With UcDSN
                    TestCon.Open(.cmbDSN.Text, .txtUserID.Text, .txtPassword.Text)
                End With
            End If

            Dim oRs As ADODB.Recordset = New ADODB.Recordset
            Dim fieldOne As String
            Dim OrderBy As String
            Dim SQL As String = "FolderID FROM Folders "

            If optDat.Checked = False Then
                fieldOne = "CONVERT(VARCHAR(255),FolderName)"
                OrderBy = " ORDER BY CONVERT(VARCHAR(255),FolderName)"
            Else
                fieldOne = "FolderName"
                OrderBy = " ORDER BY FolderName"
            End If

            SQL = "SELECT " & fieldOne & "," & SQL & OrderBy

            oRs.Open(clsMarsData.prepforbinaryCollation(SQL), TestCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

            oFolderIDs = New ComboBox

            cmbFolders.Items.Clear()

            Do While oRs.EOF = False
                Dim obj As ComboBox.ObjectCollection = cmbFolders.Items
                Dim inList As Boolean

                inList = False

                For I As Integer = 0 To obj.Count - 1
                    If obj(I) = oRs(0).Value Then
                        inList = True
                        Exit For
                    End If
                Next

                If inList = True Then GoTo SKIP

                cmbFolders.Items.Add(oRs(0).Value)
                oFolderIDs.Items.Add(oRs(1).Value)
SKIP:
                oRs.MoveNext()
            Loop

            oRs.Close()

            TestCon.Close()

            cmdNext.Enabled = True
            GroupBox3.Visible = True
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))

            cmdNext.Enabled = False

        End Try
    End Sub

    Private Function isRemoteRecordDuplicate(table As String, key As String, value As Object) As Boolean
        Try

            Dim oRemote As ADODB.Connection = New ADODB.Connection

            If optDat.Checked = True Then
                oRemote.Open(RemoteCon)
            Else
                oRemote.Open(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
            End If

            Dim oRs As ADODB.Recordset = New ADODB.Recordset

            If TypeOf value Is System.String Then
                value = "'" & SQLPrepare(value) & "'"
            End If

            Dim SQL As String = "SELECT * FROM " & table & " WHERE " & key & " = " & value

            oRs.Open(SQL, oRemote)

            If oRs.EOF = False Then
                '//duplicate exists
                oRs.Close()
                oRemote.Close()
                oRemote = Nothing
                oRs = Nothing
                Return True
            Else
                oRs.Close()
                oRemote.Close()
                oRemote = Nothing
                oRs = Nothing
                Return False
            End If
        Catch ex As Exception
            Return True
        End Try
    End Function

    Private Sub cmdFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFinish.Click

        pnProgress.Visible = True

        cmdFinish.Enabled = False
        cmdCancel.Enabled = False

        Application.DoEvents()

        Dim oItem As DevComponents.AdvTree.Node
        Dim sType As clsMarsScheduler.enScheduleType
        Dim nID As Integer
        Dim oData As New clsMarsData
        Dim oTables As New ComboBox
        Dim oColumns As New ComboBox
        Dim KeyColumn As String
        Dim ExportTables() As String
        Dim I, x As Integer
        Dim HasFailed As Boolean
        Dim Failures() As String
        Dim errCount As Integer = 0
        Dim exceptList As ArrayList

        x = 0

        If optDat.Checked = True Then
            oData.UpgradeCRDDb(RemoteCon)
            oData.GetTables(oTables, RemoteCon)
        Else
            oData.UpgradeCRDDb(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
            oData.GetTables(oTables, UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
        End If

        Dim oItems As ComboBox.ObjectCollection = oTables.Items

        For Each oItem In lsvSchedules.Nodes
            x = 0
            sType = oItem.Tag
            nID = oItem.DataKey

            If sType = clsMarsScheduler.enScheduleType.PACKAGE Then
                Dim SQL As String = "SELECT ReportID FROM ReportAttr WHERE PackID =" & nID
                Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

                If oRs IsNot Nothing Then
                    Do While oRs.EOF = False
                        Me.prepareScheduleExport(clsMarsScheduler.enScheduleType.REPORT, oRs(0).Value, oItem, oItems, HasFailed, errCount)
                        oRs.MoveNext()
                    Loop

                    oRs.Close()
                End If

                exceptList = New ArrayList
                exceptList.Add("reportattr")
                exceptList.Add("packagedreportattr")
                exceptList.Add("reportoptions")

                Me.prepareScheduleExport(clsMarsScheduler.enScheduleType.PACKAGE, nID, oItem, oItems, HasFailed, errCount, exceptList)

                '//custom calendars
                exportCustomCalendars(nID, clsMarsScheduler.enScheduleType.PACKAGE)
            ElseIf sType = clsMarsScheduler.enScheduleType.REPORT Then
                Dim SQL As String = "SELECT DestinationID FROM DestinationAttr WHERE ReportID =" & nID
                Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

                If oRs IsNot Nothing Then
                    Do While oRs.EOF = False
                        Me.prepareScheduleExport(clsMarsScheduler.enScheduleType.DESTINATION, oRs(0).Value, oItem, oItems, HasFailed, errCount)


                        '//check address entries
                        exportReferencedAddressBookEntries(oRs(0).Value)
                        '//check data items
                        exportReferencedDataItems(oRs(0).Value)

                        oRs.MoveNext()
                    Loop

                    oRs.Close()
                End If

                exceptList = New ArrayList

                exceptList.Add("destinationattr")
                prepareScheduleExport(clsMarsScheduler.enScheduleType.REPORT, nID, oItem, oItems, HasFailed, errCount, exceptList)
                '//custom calendars
                exportCustomCalendars(nID, clsMarsScheduler.enScheduleType.REPORT)
            ElseIf sType = clsMarsScheduler.enScheduleType.AUTOMATION Then
                prepareScheduleExport(clsMarsScheduler.enScheduleType.AUTOMATION, nID, oItem, oItems, HasFailed, errCount, exceptList)
                '//custom calendars
                exportCustomCalendars(nID, clsMarsScheduler.enScheduleType.REPORT)
            End If
        Next

        If HasFailed = False Then
            Dim sFinito As String

            sFinito = "The schedule has been exported successfully.  However you must now 'Refresh' the schedule on " & _
            "the remote system so that the rpt file and other data are successfully amended to suit " & _
            "the remote system's setup and folder structure."

            MessageBox.Show(sFinito, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

            Close()

            Return
        Else
            cmdFinish.Enabled = True
            cmdCancel.Enabled = True
            pnProgress.Visible = False
            pgb.Value = 0

            Dim sMsg As String

            sMsg = "Schedule export completed. Failed to export the following schedules: " & vbCrLf

            If Failures IsNot Nothing Then
                For Each s As String In Failures
                    sMsg &= s & vbCrLf
                Next
            End If

            MessageBox.Show(sMsg, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        End If
    End Sub

    Private Sub prepareScheduleExport(ByVal stype As clsMarsScheduler.enScheduleType, ByVal nID As Integer, ByVal oItem As DevComponents.AdvTree.Node, _
    ByVal oItems As ComboBox.ObjectCollection, ByRef HasFailed As Boolean, ByRef errCount As Integer, Optional ByVal exceptList As ArrayList = Nothing)
        Dim keyColumn As String
        Dim I, x As Integer
        Dim oData As New clsMarsData
        Dim oColumns As New ComboBox
        Dim ExportTables() As String
        Dim failures() As String
        Dim scheduleID As Integer = 0

        Select Case stype
            Case clsMarsScheduler.enScheduleType.REPORT
                keyColumn = "reportid"
                scheduleID = clsMarsScheduler.GetScheduleID(nID)
            Case clsMarsScheduler.enScheduleType.PACKAGE
                keyColumn = "packid"
                scheduleID = clsMarsScheduler.GetScheduleID(, nID)
            Case clsMarsScheduler.enScheduleType.AUTOMATION
                keyColumn = "autoid"
                scheduleID = clsMarsScheduler.GetScheduleID(, , nID)
            Case clsMarsScheduler.enScheduleType.EVENTBASED
                keyColumn = "eventid"
                scheduleID = nID
            Case clsMarsScheduler.enScheduleType.DESTINATION
                keyColumn = "destinationid"
        End Select

        For I = 0 To oItems.Count - 1

            If optDat.Checked = True Then
                oData.GetColumns(oColumns, RemoteCon, oItems(I))
            Else
                oData.GetColumns(oColumns, UcDSN.cmbDSN.Text, oItems(I), UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
            End If

            Dim oItems2 As ComboBox.ObjectCollection = oColumns.Items

            For n As Integer = 0 To oItems2.Count - 1
                Dim sTemp As String = Convert.ToString(oItems2(n))

                If sTemp.ToLower = keyColumn Then
                    ReDim Preserve ExportTables(x)
                    ExportTables(x) = oItems(I)
                    x += 1
                    Exit For
                End If
            Next
        Next

        Application.DoEvents()
        '_Delay(1)

        For n As Integer = 0 To ExportTables.GetUpperBound(0)

            If exceptList IsNot Nothing Then
                If exceptList.Contains(ExportTables(n).ToLower) Then Continue For
            End If


            Label3.Text = "Exporting schedule " & oItem.Text & ": " & ExportTables(n)

            pgb.Value = (n / ExportTables.GetUpperBound(0)) * 100
            Application.DoEvents()


            If _ExportRecord(nID, ExportTables(n), keyColumn) = False Then

                _DeleteRecords(ExportTables, nID, keyColumn)

                HasFailed = True
                ReDim Preserve failures(errCount)

                failures(errCount) = oItem.Text
                Exit For
            End If

        Next

        'export tasks
        If errCount = 0 Then
            Me._ExportRecord(scheduleID, "Tasks", "ScheduleID")
        End If

    End Sub

    Private Sub exportReferencedDataItems(destinationID As Integer)
        Dim SQL As String = "SELECT * FROM destinationattr WHERE destinationid = " & destinationID
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim foundItems As ArrayList = New ArrayList

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            For Each fld As ADODB.Field In oRs.Fields
                Dim temp As ArrayList = findReferencedDataItemInField(IsNull(oRs(fld.Name).Value, ""))

                If temp IsNot Nothing Then
                    For Each item As String In temp
                        If foundItems.Contains(item) = False Then
                            foundItems.Add(item)
                        End If
                    Next
                End If
            Next
        End If

        oRs = clsMarsData.GetData("SELECT * FROM reportoptions WHERE destinationid=" & destinationID)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            For Each fld As ADODB.Field In oRs.Fields
                Dim temp As ArrayList = findReferencedDataItemInField(IsNull(oRs(fld.Name).Value, ""))

                If temp IsNot Nothing Then
                    For Each item As String In temp
                        If foundItems.Contains(item) = False Then
                            foundItems.Add(item)
                        End If
                    Next
                End If
            Next

            oRs.Close()
            oRs = Nothing
        End If

        For Each item As String In foundItems
            exportDataItem(item)
        Next
    End Sub

    Private Sub exportDataItem(dataItemName As String)
        Dim oRemote As ADODB.Connection = New ADODB.Connection

        dataItemName = dataItemName.Replace("<[d]", "").Replace(">", "")

        If optDat.Checked = True Then
            oRemote.Open(RemoteCon)
        Else
            oRemote.Open(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
        End If

        If isRemoteRecordDuplicate("dataitems", "itemname", dataItemName) Then Return


        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM dataitems WHERE itemName ='" & SQLPrepare(dataItemName) & "'")

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Dim cols, vals As String

            cols = "[ItemID],[ItemName],[constring],[SQLQuery],[AllowMultiple],[MultipleSep],[replacenull],[replacenullvalue],[replaceeof],[replaceeofvalue],[fielddelimiter],[useforparameters]"

            vals = oRs("itemid").Value & "," & _
                "'" & SQLPrepare(oRs("itemname").Value) & "'," & _
                "'" & SQLPrepare(oRs("constring").Value) & "'," & _
                "'" & SQLPrepare(oRs("sqlquery").Value) & "'," & _
               IsNull(oRs("allowmultiple").Value, 0) & "," & _
                "'" & SQLPrepare(oRs("multiplesep").Value) & "'," & _
                IsNull(oRs("replacenull").Value, 0) & "," & _
                "'" & SQLPrepare(oRs("replacenullvalue").Value) & "'," & _
                IsNull(oRs("replaceeof").Value, 0) & "," & _
                "'" & SQLPrepare(oRs("replaceeofvalue").Value) & "'," & _
                "'" & SQLPrepare(oRs("fielddelimiter").Value) & "'," & _
                IsNull(oRs("useforparameters").Value, 0)

            Dim SQL As String = "INSERT INTO dataitems (" & cols & ") VALUES (" & vals & ")"

            SQL = clsMarsData.prepforbinaryCollation(SQL)

            oRemote.Execute(SQL)

            oRemote.Close()

            oRs.Close()
            oRs = Nothing
        End If
    End Sub

    Private Sub exportCustomCalendars(nID As Integer, type As clsMarsScheduler.enScheduleType)
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        If type = clsMarsScheduler.enScheduleType.PACKAGE Then
            SQL = "SELECT calendarname, exceptioncalendar FROM scheduleattr WHERE packid =" & nID
        ElseIf type = clsMarsScheduler.enScheduleType.REPORT Then
            SQL = "SELECT calendarname, exceptioncalendar FROM scheduleattr WHERE reportid =" & nID
        End If

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Dim customName, exceptionName As String
            customName = IsNull(oRs("calendarName").Value)
            exceptionName = IsNull(oRs("exceptioncalendar").Value)

            If customName <> "" Then
                exportCalendar(customName)
            ElseIf exceptionName <> "" And exceptionName <> customName Then
                exportCalendar(exceptionName)
            End If
        End If
    End Sub

    Private Sub exportCalendar(calendarName As String)
        Dim oRs As New ADODB.Recordset
        Dim oRemote As ADODB.Connection = New ADODB.Connection
        Dim SQL As String

        Try
            If isRemoteRecordDuplicate("calendarattr", "calendarname", calendarName) Then Return

            If optDat.Checked = True Then
                oRemote.Open(RemoteCon)
            Else
                oRemote.Open(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
            End If

            Dim cols, vals As String
            SQL = "SELECT * FROM calendarattr WHERE calendarname ='" & SQLPrepare(calendarName) & "'"

            oRs = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing AndAlso oRs.EOF = False Then

                cols = "calendarname,calendardate,calendarid,isholiday,holidayform"

                Do While oRs.EOF = False
                    vals = "'" & SQLPrepare(calendarName) & "'," & _
                        "'" & oRs("calendardate").Value & "'," & _
                        oRs("calendarid").Value & "," & _
                        IsNull(oRs("isholiday").Value, 0) & "," & _
                        "'" & oRs("holidayform").Value & "'"

                    SQL = "INSERT INTO calendarattr (" & cols & ") VALUES (" & vals & ")"

                    SQL = clsMarsData.prepforbinaryCollation(SQL)

                    oRemote.Execute(SQL)
                    oRs.MoveNext()
                Loop

                oRemote.Close()
            End If
        Catch : End Try
    End Sub

    Private Function findReferencedDataItemInField(sIn As String) As ArrayList
        Dim result As ArrayList = New ArrayList
        Dim nStart, nEnd As Integer
        Dim sFunction As String
        Dim sValue As String

        Do While sIn.IndexOf("<[d]") > -1

            nStart = sIn.IndexOf("<[d]")

            nEnd = sIn.IndexOf(">", nStart) + 1

            sFunction = sIn.Substring(nStart, nEnd - nStart)

            Dim oItem As New frmDataItems

            sValue = "XXXXXX" '//just some crap to remove the data item

            sIn = sIn.Replace(sFunction, sValue)

            result.Add(sFunction)
        Loop

        Return result
    End Function
    Private Sub exportReferencedAddressBookEntries(destinationID As Integer)
        Try
            Dim SQL As String = "SELECT * FROM destinationattr WHERE destinationid = " & destinationID & " AND destinationtype = 'email'"
            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                Dim sendTo, sendCC, sendBcc As String
                Dim uniqueAddresses As ArrayList = New ArrayList

                sendTo = oRs("sendto").Value
                sendCC = oRs("cc").Value
                sendBcc = oRs("bcc").Value

                For Each address As String In sendTo.Split(";")
                    If address.Trim.StartsWith("<[a]") And uniqueAddresses.Contains(address) = False Then
                        uniqueAddresses.Add(address)
                    End If
                Next

                For Each address As String In sendCC.Split(";")
                    If address.Trim.StartsWith("<[a]") And uniqueAddresses.Contains(address) = False Then
                        uniqueAddresses.Add(address)
                    End If
                Next

                For Each address As String In sendBcc.Split(";")
                    If address.Trim.StartsWith("<[a]") And uniqueAddresses.Contains(address) = False Then
                        uniqueAddresses.Add(address)
                    End If
                Next

                Dim uniqueContacts As ArrayList = New ArrayList

                For Each address As String In uniqueAddresses
                    exportContact(address)
                Next
            End If
        Catch : End Try
    End Sub

    Private Function getContactID(addressKey As String) As Integer
        addressKey = addressKey.Replace("<[a]", "").Replace(">", "")

        Dim SQL As String = "SELECT contactid FROM contactattr WHERE contactName = '" & SQLPrepare(addressKey) & "'"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim contactID As Integer

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            contactID = oRs("contactid").Value

            oRs.Close()
            oRs = Nothing
        End If

        Return contactID
    End Function

    Private Function exportContact(ByVal Contact As String)
        Dim oRs As New ADODB.Recordset
        Dim oRemote As ADODB.Connection = New ADODB.Connection
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim contactID As Integer = getContactID(Contact)
        Dim sTable As String = "contactattr"

        If contactID = 0 Then Return False

        Contact = Contact.Replace("<[a]", "").Replace(">", "")

        If isRemoteRecordDuplicate("contactattr", "contactname", Contact) Then Return False

        Try
            If optDat.Checked = True Then
                oRemote.Open(RemoteCon)
            Else
                oRemote.Open(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
            End If

            '//copy the contactattr
            SQL = "SELECT * FROM contactattr  WHERE contactid = " & contactID

            oRs = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                sCols = "contactid,contactname,contacttype"
                sVals = oRs("contactid").Value & "," & _
                    "'" & SQLPrepare(oRs("contactname").Value) & "'," & _
                    "'" & oRs("contacttype").Value & "'"

                SQL = "INSERT INTO " & sTable & "(" & sCols & ") VALUES (" & sVals & ")"

                SQL = clsMarsData.prepforbinaryCollation(SQL)

                oRemote.Execute(SQL)

                oRs.Close()

                sTable = "contactdetail"

                oRs = clsMarsData.GetData("SELECT * FROM contactdetail WHERE contactid =" & contactID)

                sCols = "detailid,contactid,emailaddress,phonenumber,faxnumber"
                sVals = oRs("detailid").Value & "," & _
                    contactID & "," & _
                    "'" & SQLPrepare(oRs("emailaddress").Value) & "'," & _
                    "'" & SQLPrepare(oRs("phonenumber").Value) & "'," & _
                    "'" & SQLPrepare(oRs("faxnumber").Value) & "'"

                SQL = "INSERT INTO " & sTable & "(" & sCols & ") VALUES (" & sVals & ")"

                SQL = clsMarsData.prepforbinaryCollation(SQL)

                oRemote.Execute(SQL)

                oRemote.Close()
            End If

            Return True
        Catch : End Try

    End Function



    Private Sub _DeleteRecords(ByVal sTables() As String, ByVal nID As Integer, ByVal KeyColumn As String)
        On Error Resume Next

        Dim o As ADODB.Connection = New ADODB.Connection

        If optDat.Checked = True Then
            o.Open(RemoteCon)
        Else
            o.Open(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
        End If

        For Each s As String In sTables
            o.Execute("DELETE FROM " & s & " WHERE " & KeyColumn & " = " & nID)
        Next

    End Sub
    Private Function _ExportRecord(ByVal nID As Integer, ByVal sTable As String, _
    ByVal KeyColumn As String) As Boolean
        Dim oRs As New ADODB.Recordset
        Dim oRemote As ADODB.Connection = New ADODB.Connection
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String

        Try
            If optDat.Checked = True Then
                oRemote.Open(RemoteCon)
            Else
                oRemote.Open(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
            End If


            SQL = "SELECT * FROM " & sTable & " WHERE " & KeyColumn & " = " & nID

            oRs = clsMarsData.GetData(SQL)

            Dim oField As ADODB.Field

            If Not oRs Is Nothing Then

                Do While oRs.EOF = False

                    sCols = ""
                    sVals = ""

                    For Each oField In oRs.Fields
                        sCols = sCols & "[" & oField.Name & "],"

                        Select Case oField.Type
                            Case ADODB.DataTypeEnum.adInteger
                                If oField.Name.ToLower = "parent" Then
                                    sVals = sVals & nParent & ","
                                Else
                                    sVals = sVals & IsNull(oField.Value, 0) & ","
                                End If
                            Case ADODB.DataTypeEnum.adDate, ADODB.DataTypeEnum.adDBTimeStamp
                                sVals = sVals & "'" & ConDateTime(IsNull(oField.Value, Now)) & "',"
                            Case Else
                                If sTable.ToLower = "scheduleattr" And oField.Name.ToLower = "repeatinterval" Then
                                    sVals = sVals & IsNull(oField.Value, "0").ToString.Replace(",", ".") & ","
                                Else
                                    sVals = sVals & "'" & SQLPrepare(IsNull(oField.Value, "")) & "',"
                                End If
                        End Select

                    Next

                    sCols = sCols.Substring(0, sCols.Length - 1)
                    sVals = sVals.Substring(0, sVals.Length - 1)

                    SQL = "INSERT INTO " & sTable & "(" & sCols & ") VALUES (" & sVals & ")"

                    SQL = clsMarsData.prepforbinaryCollation(SQL)

                    Try
                        oRemote.Execute(SQL)
                    Catch ex1 As Exception
                        If ex1.Message.ToLower.Contains("primary key") = False Then
                            Throw ex1
                        Else
                            If sTable.ToLower <> "reportattr" And sTable.ToLower <> "packageattr" Then
                                Dim newCode As Integer = clsMarsData.CreateDataID
                                Dim newVals As String = ""
                                Dim tempArr As String() = sVals.Split(",")

                                tempArr(0) = newCode

                                For Each s As String In tempArr
                                    newVals &= s & ","
                                Next

                                newVals = newVals.Remove(newVals.Length - 1, 1)

                                SQL = "INSERT INTO " & sTable & "(" & sCols & ") VALUES (" & newVals & ")"

                                SQL = clsMarsData.prepforbinaryCollation(SQL)

                                Try
                                    oRemote.Execute(SQL)
                                Catch : End Try
                            End If
                        End If
                    End Try

                    oRs.MoveNext()
                Loop

                oRs.Close()

                oRemote.Close()
            End If

            Return True
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))

            Return False
        End Try

    End Function

    Private Sub cmbFolders_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFolders.SelectedIndexChanged
        Dim oRemote As New ADODB.Connection
        Dim oRs As ADODB.Recordset = New ADODB.Recordset

        If optDat.Checked = True Then
            oRemote.Open(RemoteCon)
        Else
            oRemote.Open(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
        End If

        oRs.Open(clsMarsData.DataItem.prepforbinaryCollation("SELECT FolderID FROM Folders WHERE FolderName = '" & SQLPrepare(cmbFolders.Text) & "'"), _
        oRemote, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

        If oRs.EOF = False Then
            nParent = oRs(0).Value
        Else
            nParent = 0
        End If

        oRs.Close()
        oRemote.Close()
    End Sub



    Private Sub tvFolders_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tvFolders.MouseMove
        If tvFolders.SelectedNode IsNot Nothing Then
            If e.Button = MouseButtons.Left Then
                tvFolders.DoDragDrop(tvFolders.SelectedNodes, DragDropEffects.Copy)
            End If
        End If
    End Sub

    Private Sub lsvSchedules_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles lsvSchedules.DragDrop
        e.Effect = DragDropEffects.None
        Dim oItem As DevComponents.AdvTree.Node
        Dim sType As clsMarsScheduler.enScheduleType

        Dim nodes As DevComponents.AdvTree.NodeCollection = CType(e.Data.GetData(GetType(System.Windows.Forms.DataObject).ToString, True), DevComponents.AdvTree.NodeCollection)

        For Each oNode As DevComponents.AdvTree.Node In nodes

            sType = oNode.Tag

            If sType = clsMarsScheduler.enScheduleType.FOLDER Or sType = clsMarsScheduler.enScheduleType.SMARTFOLDER Then Return

            For Each oItem In lsvSchedules.Nodes
                If oItem.Tag = oNode.Tag And oItem.DataKey = oNode.DataKey Then
                    GoTo skip
                End If
            Next

            oItem = New DevComponents.AdvTree.Node

            oItem.Text = oNode.Text
            oItem.Tag = oNode.Tag
            oItem.Image = oNode.Image
            oItem.DataKey = oNode.DataKey

            lsvSchedules.Nodes.Add(oItem)
skip:
        Next
    End Sub

    Private Sub lsvSchedules_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles lsvSchedules.DragEnter

        e.Effect = DragDropEffects.Copy

    End Sub
End Class
