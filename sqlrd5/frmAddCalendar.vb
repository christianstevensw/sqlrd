Public Class frmAddCalendar
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim UserCancel As Boolean = True
    Friend WithEvents cmbName As System.Windows.Forms.TextBox
    Friend WithEvents ExpandableSplitter1 As DevComponents.DotNetBar.ExpandableSplitter
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Dim oData As New clsMarsData
    Friend WithEvents NavBarControl1 As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents NavBarGroup1 As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents NavBarGroupControlContainer2 As DevExpress.XtraNavBar.NavBarGroupControlContainer
    Friend WithEvents NavBarGroupControlContainer1 As DevExpress.XtraNavBar.NavBarGroupControlContainer
    Friend WithEvents NavBarGroup2 As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents cmbCountry As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lsvHolidays As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Dim formLoaded As Boolean = False

    Private ReadOnly Property m_CountryCode() As String
        Get
            Dim countryCode As String

            Select Case cmbCountry.Text
                Case "USA"
                    countryCode = "US"
                Case "UK"
                    countryCode = "GBEAW"
                Case "Scotland"
                    countryCode = "GBNIR"
                Case "Northern Ireland"
                    countryCode = "GBSCT"
                Case "Canada"
                    countryCode = "CA"
                Case "Australia"
                    countryCode = "AUS"
                Case "New Zealand"
                    countryCode = "NZ"
                Case Else
                    Return ""
            End Select


            Return countryCode
        End Get
    End Property

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents oCal As System.Windows.Forms.MonthCalendar
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lsvDates As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbImport As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdExport As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdShare As DevComponents.DotNetBar.ButtonX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdDelete As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents sfd As System.Windows.Forms.SaveFileDialog
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents cmdUpdate As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdAdd As DevComponents.DotNetBar.ButtonX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAddCalendar))
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lsvDates = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ExpandableSplitter1 = New DevComponents.DotNetBar.ExpandableSplitter()
        Me.oCal = New System.Windows.Forms.MonthCalendar()
        Me.NavBarControl1 = New DevExpress.XtraNavBar.NavBarControl()
        Me.NavBarGroup1 = New DevExpress.XtraNavBar.NavBarGroup()
        Me.NavBarGroupControlContainer2 = New DevExpress.XtraNavBar.NavBarGroupControlContainer()
        Me.NavBarGroupControlContainer1 = New DevExpress.XtraNavBar.NavBarGroupControlContainer()
        Me.lsvHolidays = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmbCountry = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.NavBarGroup2 = New DevExpress.XtraNavBar.NavBarGroup()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbImport = New DevComponents.DotNetBar.ButtonX()
        Me.cmdExport = New DevComponents.DotNetBar.ButtonX()
        Me.cmdShare = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmbName = New System.Windows.Forms.TextBox()
        Me.cmdUpdate = New DevComponents.DotNetBar.ButtonX()
        Me.cmdAdd = New DevComponents.DotNetBar.ButtonX()
        Me.cmdDelete = New DevComponents.DotNetBar.ButtonX()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.sfd = New System.Windows.Forms.SaveFileDialog()
        Me.ofd = New System.Windows.Forms.OpenFileDialog()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.Panel2.SuspendLayout()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.NavBarControl1.SuspendLayout()
        Me.NavBarGroupControlContainer2.SuspendLayout()
        Me.NavBarGroupControlContainer1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lsvDates)
        Me.Panel2.Controls.Add(Me.ExpandableSplitter1)
        Me.Panel2.Controls.Add(Me.NavBarControl1)
        Me.Panel2.Location = New System.Drawing.Point(8, 64)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(472, 499)
        Me.Panel2.TabIndex = 2
        '
        'lsvDates
        '
        '
        '
        '
        Me.lsvDates.Border.Class = "ListViewBorder"
        Me.lsvDates.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvDates.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvDates.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvDates.FullRowSelect = True
        Me.lsvDates.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvDates.Location = New System.Drawing.Point(244, 0)
        Me.lsvDates.Name = "lsvDates"
        Me.lsvDates.Size = New System.Drawing.Size(228, 499)
        Me.lsvDates.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.SuperTooltip1.SetSuperTooltip(Me.lsvDates, New DevComponents.DotNetBar.SuperTooltipInfo("Custom Calendar", "", "Click on a date to remove it from the custom calendar.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, True, False, New System.Drawing.Size(0, 0)))
        Me.lsvDates.TabIndex = 1
        Me.lsvDates.UseCompatibleStateImageBehavior = False
        Me.lsvDates.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Selected Dates"
        Me.ColumnHeader1.Width = 142
        '
        'ExpandableSplitter1
        '
        Me.ExpandableSplitter1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ExpandableSplitter1.BackColor2 = System.Drawing.Color.Empty
        Me.ExpandableSplitter1.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.ExpandableSplitter1.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.ExpandableSplitter1.ExpandableControl = Me.oCal
        Me.ExpandableSplitter1.ExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(254, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.ExpandableSplitter1.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground
        Me.ExpandableSplitter1.ExpandLineColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ExpandableSplitter1.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.GripDarkColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ExpandableSplitter1.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.GripLightColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer))
        Me.ExpandableSplitter1.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.MenuBackground
        Me.ExpandableSplitter1.HotBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(213, Byte), Integer), CType(CType(140, Byte), Integer))
        Me.ExpandableSplitter1.HotBackColor2 = System.Drawing.Color.Empty
        Me.ExpandableSplitter1.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.ExpandableSplitter1.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemCheckedBackground
        Me.ExpandableSplitter1.HotExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(254, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.ExpandableSplitter1.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground
        Me.ExpandableSplitter1.HotExpandLineColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ExpandableSplitter1.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.HotGripDarkColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ExpandableSplitter1.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.HotGripLightColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer))
        Me.ExpandableSplitter1.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.MenuBackground
        Me.ExpandableSplitter1.Location = New System.Drawing.Point(241, 0)
        Me.ExpandableSplitter1.Name = "ExpandableSplitter1"
        Me.ExpandableSplitter1.Size = New System.Drawing.Size(3, 499)
        Me.ExpandableSplitter1.Style = DevComponents.DotNetBar.eSplitterStyle.Mozilla
        Me.ExpandableSplitter1.TabIndex = 1
        Me.ExpandableSplitter1.TabStop = False
        '
        'oCal
        '
        Me.oCal.BackColor = System.Drawing.SystemColors.Control
        Me.oCal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.oCal.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.oCal.Location = New System.Drawing.Point(7, 0)
        Me.oCal.MaxSelectionCount = 10000
        Me.oCal.Name = "oCal"
        Me.oCal.ShowToday = False
        Me.oCal.ShowTodayCircle = False
        Me.SuperTooltip1.SetSuperTooltip(Me.oCal, New DevComponents.DotNetBar.SuperTooltipInfo("Custom Calendar", "", "Click on a date to add it to your custom calendar", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, False, New System.Drawing.Size(0, 0)))
        Me.oCal.TabIndex = 1
        Me.oCal.TitleBackColor = System.Drawing.Color.Navy
        Me.oCal.TitleForeColor = System.Drawing.Color.White
        Me.oCal.TodayDate = New Date(2006, 3, 15, 0, 0, 0, 0)
        '
        'NavBarControl1
        '
        Me.NavBarControl1.ActiveGroup = Me.NavBarGroup1
        Me.NavBarControl1.Controls.Add(Me.NavBarGroupControlContainer1)
        Me.NavBarControl1.Controls.Add(Me.NavBarGroupControlContainer2)
        Me.NavBarControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.NavBarControl1.Groups.AddRange(New DevExpress.XtraNavBar.NavBarGroup() {Me.NavBarGroup1, Me.NavBarGroup2})
        Me.NavBarControl1.Location = New System.Drawing.Point(0, 0)
        Me.NavBarControl1.Name = "NavBarControl1"
        Me.NavBarControl1.Size = New System.Drawing.Size(241, 499)
        Me.NavBarControl1.TabIndex = 9
        Me.NavBarControl1.Text = "NavBarControl1"
        Me.NavBarControl1.View = New DevExpress.XtraNavBar.ViewInfo.StandardSkinExplorerBarViewInfoRegistrator("iMaginary")
        '
        'NavBarGroup1
        '
        Me.NavBarGroup1.Caption = "Select Dates"
        Me.NavBarGroup1.ControlContainer = Me.NavBarGroupControlContainer2
        Me.NavBarGroup1.Expanded = True
        Me.NavBarGroup1.GroupClientHeight = 162
        Me.NavBarGroup1.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.ControlContainer
        Me.NavBarGroup1.Name = "NavBarGroup1"
        '
        'NavBarGroupControlContainer2
        '
        Me.NavBarGroupControlContainer2.Controls.Add(Me.oCal)
        Me.NavBarGroupControlContainer2.Name = "NavBarGroupControlContainer2"
        Me.NavBarGroupControlContainer2.Size = New System.Drawing.Size(240, 159)
        Me.NavBarGroupControlContainer2.TabIndex = 1
        '
        'NavBarGroupControlContainer1
        '
        Me.NavBarGroupControlContainer1.Controls.Add(Me.lsvHolidays)
        Me.NavBarGroupControlContainer1.Controls.Add(Me.cmbCountry)
        Me.NavBarGroupControlContainer1.Controls.Add(Me.Label2)
        Me.NavBarGroupControlContainer1.Name = "NavBarGroupControlContainer1"
        Me.NavBarGroupControlContainer1.Size = New System.Drawing.Size(240, 280)
        Me.NavBarGroupControlContainer1.TabIndex = 0
        '
        'lsvHolidays
        '
        '
        '
        '
        Me.lsvHolidays.Border.Class = "ListViewBorder"
        Me.lsvHolidays.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvHolidays.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader2})
        Me.lsvHolidays.FullRowSelect = True
        Me.lsvHolidays.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lsvHolidays.HideSelection = False
        Me.lsvHolidays.Location = New System.Drawing.Point(7, 43)
        Me.lsvHolidays.Name = "lsvHolidays"
        Me.lsvHolidays.Size = New System.Drawing.Size(227, 228)
        Me.lsvHolidays.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lsvHolidays.TabIndex = 9
        Me.lsvHolidays.UseCompatibleStateImageBehavior = False
        Me.lsvHolidays.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Width = 190
        '
        'cmbCountry
        '
        Me.cmbCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCountry.FormattingEnabled = True
        Me.cmbCountry.Items.AddRange(New Object() {"Australia", "Canada", "New Zealand", "Northern Ireland", "Scotland", "UK", "USA"})
        Me.cmbCountry.Location = New System.Drawing.Point(7, 16)
        Me.cmbCountry.Name = "cmbCountry"
        Me.cmbCountry.Size = New System.Drawing.Size(227, 21)
        Me.cmbCountry.Sorted = True
        Me.cmbCountry.TabIndex = 10
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Location = New System.Drawing.Point(4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Select Country"
        '
        'NavBarGroup2
        '
        Me.NavBarGroup2.Caption = "Holidays"
        Me.NavBarGroup2.ControlContainer = Me.NavBarGroupControlContainer1
        Me.NavBarGroup2.Expanded = True
        Me.NavBarGroup2.GroupClientHeight = 283
        Me.NavBarGroup2.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.ControlContainer
        Me.NavBarGroup2.Name = "NavBarGroup2"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.BackColor = System.Drawing.Color.Transparent
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.Enabled = False
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(511, 24)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 1
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.BackColor = System.Drawing.Color.Transparent
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(511, 56)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 2
        Me.cmdCancel.Text = "&Cancel"
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(6, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 16)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Calendar Name"
        '
        'cmbImport
        '
        Me.cmbImport.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmbImport.BackColor = System.Drawing.Color.Transparent
        Me.cmbImport.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmbImport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmbImport.Location = New System.Drawing.Point(511, 96)
        Me.cmbImport.Name = "cmbImport"
        Me.cmbImport.Size = New System.Drawing.Size(75, 23)
        Me.cmbImport.TabIndex = 3
        Me.cmbImport.Text = "&Import"
        Me.cmbImport.Visible = False
        '
        'cmdExport
        '
        Me.cmdExport.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdExport.BackColor = System.Drawing.Color.Transparent
        Me.cmdExport.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdExport.Enabled = False
        Me.cmdExport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdExport.Location = New System.Drawing.Point(511, 128)
        Me.cmdExport.Name = "cmdExport"
        Me.cmdExport.Size = New System.Drawing.Size(75, 23)
        Me.cmdExport.TabIndex = 4
        Me.cmdExport.Text = "&Export"
        Me.cmdExport.Visible = False
        '
        'cmdShare
        '
        Me.cmdShare.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdShare.BackColor = System.Drawing.Color.Transparent
        Me.cmdShare.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdShare.Enabled = False
        Me.cmdShare.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdShare.Location = New System.Drawing.Point(511, 160)
        Me.cmdShare.Name = "cmdShare"
        Me.cmdShare.Size = New System.Drawing.Size(75, 23)
        Me.cmdShare.TabIndex = 5
        Me.cmdShare.Text = "&Share"
        Me.cmdShare.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmbName)
        Me.GroupBox1.Controls.Add(Me.Panel2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cmdUpdate)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(488, 569)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'cmbName
        '
        Me.cmbName.Location = New System.Drawing.Point(8, 32)
        Me.cmbName.Name = "cmbName"
        Me.cmbName.Size = New System.Drawing.Size(223, 21)
        Me.cmbName.TabIndex = 0
        '
        'cmdUpdate
        '
        Me.cmdUpdate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdUpdate.BackColor = System.Drawing.Color.Transparent
        Me.cmdUpdate.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdUpdate.Enabled = False
        Me.cmdUpdate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdUpdate.Location = New System.Drawing.Point(248, 30)
        Me.cmdUpdate.Name = "cmdUpdate"
        Me.cmdUpdate.Size = New System.Drawing.Size(120, 21)
        Me.cmdUpdate.TabIndex = 4
        Me.cmdUpdate.Text = "&Update Schedules"
        '
        'cmdAdd
        '
        Me.cmdAdd.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAdd.Location = New System.Drawing.Point(511, 221)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(56, 21)
        Me.cmdAdd.TabIndex = 8
        Me.cmdAdd.Text = "&New"
        Me.cmdAdd.Visible = False
        '
        'cmdDelete
        '
        Me.cmdDelete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdDelete.BackColor = System.Drawing.Color.Transparent
        Me.cmdDelete.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdDelete.Enabled = False
        Me.cmdDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDelete.Location = New System.Drawing.Point(511, 192)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(75, 23)
        Me.cmdDelete.TabIndex = 6
        Me.cmdDelete.Text = "&Delete"
        Me.cmdDelete.Visible = False
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'sfd
        '
        Me.sfd.Filter = "Calendar File|*.cal"
        '
        'ofd
        '
        Me.ofd.Filter = "Calendar File|*.cal"
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'frmAddCalendar
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(588, 580)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cmdAdd)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmbImport)
        Me.Controls.Add(Me.cmdExport)
        Me.Controls.Add(Me.cmdShare)
        Me.Controls.Add(Me.cmdDelete)
        Me.DoubleBuffered = True
        Me.EnableGlass = False
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmAddCalendar"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add/Edit Custom Calendar"
        Me.Panel2.ResumeLayout(False)
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.NavBarControl1.ResumeLayout(False)
        Me.NavBarGroupControlContainer2.ResumeLayout(False)
        Me.NavBarGroupControlContainer1.ResumeLayout(False)
        Me.NavBarGroupControlContainer1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    'Private Sub oCal_DateChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DateRangeEventArgs) Handles oCal.DateChanged

    '    If formLoaded = False Then Return

    '    Dim oDate As Date
    '    Dim AlreadyBold As Boolean = False

    '    If oCal.BoldedDates.GetLength(0) > 0 Then
    '        For Each d As Date In oCal.BoldedDates
    '            If e.Start = d Then
    '                oCal.RemoveBoldedDate(d)
    '                oCal.UpdateBoldedDates()

    '                AlreadyBold = True

    '                For Each oitem As ListViewItem In lsvDates.Items
    '                    If oitem.Text = ConDate(d) Then
    '                        oitem.Remove()
    '                    End If
    '                Next

    '                Exit For
    '            Else
    '                AlreadyBold = False
    '            End If
    '        Next
    '    End If

    '    If AlreadyBold = False Then
    '        oDate = New Date(e.Start.Year, e.Start.Month, e.Start.Day)

    '        oCal.AddBoldedDate(e.Start)
    '        oCal.UpdateBoldedDates()
    '        lsvDates.Items.Add(ConDate(oDate))
    '    End If

    'End Sub

    Public Shared Sub UpdateSchedules(ByVal calendarName As String)
        Dim SQL As String
        Dim toEnable As ArrayList = New ArrayList
        Dim toEnableName As ArrayList = New ArrayList

        SQL = "UPDATE ScheduleAttr SET CalendarName ='" & SQLPrepare(calendarName) & "' WHERE CalendarName ='" & SQLPrepare(calendarName) & "'"
        clsMarsData.WriteData(SQL)

        SQL = "UPDATE ScheduleAttr SET ExceptionCalendar ='" & SQLPrepare(calendarName) & "' WHERE ExceptionCalendar ='" & SQLPrepare(calendarName) & "'"
        clsMarsData.WriteData(SQL)

        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM ScheduleAttr WHERE CalendarName ='" & SQLPrepare(calendarName) & "'")

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim scheduleID As Integer = oRs("scheduleid").Value
                Dim nextRun As Date = oRs("nextrun").Value
                Dim endDate As Date = oRs("enddate").Value
                Dim status As Boolean = oRs("status").Value
                Dim nextRunDate As Date = nextRun.Date
                Dim nextRunTime As String = ConTime(nextRun)

                If nextRunTime > ConTime(Now) Then
                    nextRun = clsMarsScheduler.globalItem.GetCustomNextRunDate(calendarName, Now.Date.AddDays(-1))
                Else
                    nextRun = clsMarsScheduler.globalItem.GetCustomNextRunDate(calendarName, Now.Date)
                End If

                SQL = "UPDATE ScheduleAttr SET " & _
                "NextRun ='" & ConDateTime(nextRun & " " & nextRunTime) & "'," & _
                "EndDate ='" & ConDate(clsMarsScheduler.globalItem.GetCustomEndDate(calendarName)) & "' " & _
                "WHERE ScheduleID =" & scheduleID

                clsMarsData.WriteData(SQL)

                'see if there are any schedules that we need 
                If ConDate(endDate) < ConDate(Now) And status = False Then
                    toEnable.Add(scheduleID)
                    toEnableName.Add(clsMarsScheduler.globalItem.GetScheduleName(scheduleID))
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()

            Dim values As String = ""

            If toEnable.Count > 0 Then
                Dim strMsg As String = "Would you like SQL-RD to enable all the schedules that use this Calendar in order to take advantage to the added dates?"

                For Each s As String In toEnableName
                    strMsg &= vbCrLf & "- " & s
                Next

                Dim doUpdate As DialogResult = MessageBox.Show(strMsg, _
                Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                Dim scheduleName As String

                If doUpdate = Windows.Forms.DialogResult.Yes Then
                    For Each s As String In toEnable
                        values &= s & ","
                    Next

                    values = "(" & values.Remove(values.Length - 1, 1) & ")"

                    SQL = "UPDATE ScheduleAttr SET status = 1 WHERE ScheduleID IN " & values

                    clsMarsData.WriteData(SQL)
                End If
            End If
        End If
    End Sub
    Public Function AddCalendar(ByVal calendarName As String) As String
        cmbName.Text = calendarName
        cmdOK.Enabled = True
        Me.cmdUpdate.Enabled = True

        _ViewCalendar()

        Me.ShowDialog()

        If UserCancel = True Then
            Return String.Empty
        End If

        Dim SQL As String
        Dim sVals As String
        Dim sCols As String
        Dim I As Integer = 1
        Dim OK As Boolean
        Dim oUI As New clsMarsUI

        clsMarsData.WriteData("DELETE FROM CalendarAttr WHERE " & _
        "CalendarName = '" & SQLPrepare(calendarName) & "'")

        sCols = "CalendarID,CalendarName,CalendarDate,IsHoliday,HolidayForm"

        For Each oItem As ListViewItem In lsvDates.Items
            Dim isHoliday As Boolean = False
            Dim holidayForm As String = ""

            If oItem.Tag Is Nothing Then oItem.Tag = ""

            If oItem.Tag.ToString <> "" Then
                isHoliday = True
                holidayForm = oItem.Tag.ToString
            End If

            sVals = clsMarsData.CreateDataID("calendarattr", "calendarid") & "," & _
                "'" & SQLPrepare(cmbName.Text) & "'," & _
                "'" & oItem.Text & "'," & _
                Convert.ToInt32(isHoliday) & "," & _
                "'" & SQLPrepare(holidayForm) & "'"


            SQL = "INSERT INTO CalendarAttr(" & sCols & ") " & _
            "VALUES (" & sVals & ")"

            OK = clsMarsData.WriteData(SQL)

            If OK = False Then Exit For

            oUI.BusyProgress((I / lsvDates.Items.Count) * 100, "Saving calendar...")

            I += 1
        Next

        UpdateSchedules(cmbName.Text)

        If OK = True Then
            MessageBox.Show("Calendar saved successfully. All schedules using this calendar have also been updated.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

        oUI.BusyProgress(, , True)

        Return cmbName.Text
    End Function

    Private Sub frmAddCalendar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        'FormatForWinXP(Me)

        lsvDates.Width = oCal.Width

        lsvDates.Columns(0).Width = lsvDates.Width - 5

        Dim range As SelectionRange = New SelectionRange(Now.AddDays(-7), Now.AddDays(-7))

        oCal.SelectionRange = range

        formLoaded = True
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click

        If cmbName.Text.Length = 0 Then
            setError(cmbName, "Please enter the name for the calendar")
            cmbName.Focus()
            Return
        ElseIf lsvDates.Items.Count = 0 Then
            setError(lsvDates, "Please add dates to your calendar")
            oCal.Focus()
            Return
        End If

        UserCancel = False

        Close()
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim Res As DialogResult

        Res = MessageBox.Show("Delete the '" & cmbName.Text & "' calendar?", _
        Application.ProductName, _
        MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If Res = DialogResult.Yes Then
            clsMarsData.WriteData("DELETE FROM CalendarAttr WHERE " & _
            "CalendarName = '" & SQLPrepare(cmbName.Text) & "'")

            cmbName.Text = String.Empty
        End If

    End Sub

    Private Sub cmbName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbName.TextChanged
        If cmbName.Text.Length = 0 Then
            cmdDelete.Enabled = False
            cmdExport.Enabled = False
            cmdOK.Enabled = False
            cmdShare.Enabled = False
            cmdUpdate.Enabled = False

            oCal.RemoveAllBoldedDates()
            oCal.UpdateBoldedDates()
            lsvDates.Items.Clear()
        Else
            cmdDelete.Enabled = True
            cmdExport.Enabled = True
            cmdOK.Enabled = True
            cmdShare.Enabled = True
            cmdUpdate.Enabled = True
        End If
    End Sub
    Private Sub _ViewCalendar()
        Dim oRs As ADODB.Recordset

        oRs = clsMarsData.GetData("SELECT DISTINCT CalendarDate FROM CalendarAttr " & _
        "WHERE CalendarName = '" & SQLPrepare(cmbName.Text) & "'")

        lsvDates.Items.Clear()

        oCal.RemoveAllBoldedDates()
        oCal.UpdateBoldedDates()

        If Not oRs Is Nothing AndAlso oRs.EOF = False Then
            Do While oRs.EOF = False
                lsvDates.Items.Add(oRs.Fields(0).Value)

                Dim d As Date

                d = oRs.Fields(0).Value

                oCal.AddBoldedDate(d)

                oRs.MoveNext()
            Loop
            oRs.Close()
        Else
            oCal.SelectionStart = Date.Now.Date
            oCal.SelectionEnd = Date.Now.Date
        End If

        oCal.UpdateBoldedDates()
    End Sub


    Private Sub cmbImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbImport.Click
        Dim oRs As ADODB.Recordset
        Dim rsTest As New ADODB.Recordset
        Dim SQL As String
        Dim FileName As String
        Dim NewFilename As String
        Dim NewName As String
        Dim I As Integer = 1
        Dim oUI As New clsMarsUI
        Dim Append As Boolean = False
        Dim nCount As Integer
        Try
            ofd.Title = "Select the calendar file to import..."

            ofd.ShowDialog()

            If ofd.FileName.Length = 0 Then Return

            FileName = ofd.FileName

            NewFilename = FileName.Replace(".cal", ".xml")

            System.IO.File.Copy(FileName, NewFilename, True)

            oUI.BusyProgress(20, "Reading calendar file")

            oRs = oData.GetXML(NewFilename)

            If oRs Is Nothing Then Return

            If oRs.EOF = False Then

                Do While oRs.EOF = False
                    nCount += 1
                    oRs.MoveNext()
                Loop

                oRs.MoveFirst()

                Dim Count

                Count = FindOccurence(NewFilename, "\") + 1

                NewName = GetDelimitedWord(NewFilename, Count, "\")

                NewName = Replace(NewName, ".xml", "")

                oUI.BusyProgress(50, "Validating file contents...")

                SQL = "SELECT * FROM CalendarAttr WHERE CalendarName = '" & NewName & "'"

                rsTest = clsMarsData.GetData(SQL, ADODB.CursorTypeEnum.adOpenStatic)

                If Not rsTest Is Nothing Then
                    If rsTest.RecordCount > 0 Then
                        Dim Res As DialogResult

                        Res = MessageBox.Show("A Calendar with this name " & _
                        "already exists in the system, add this calendar " & _
                        "to the existing one?", _
                        Application.ProductName, _
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question)

                        If Res = DialogResult.No Then
                            NewName = InputBox("Please provide a new name " & _
                            "for the imported calendar", Application.ProductName)
                        Else
                            NewName = NewName
                            Append = True
                        End If
                    End If

                    rsTest.Close()
                End If

                If NewName.Length = 0 Then
                    oUI.BusyProgress(, , True)
                    Exit Sub
                End If

                oUI.BusyProgress(75, "Importing calendar data...")

                Do While oRs.EOF = False
                    SQL = "INSERT INTO CalendarAttr(CalendarID, CalendarName, " & _
                    "CalendarDate) Values(" & _
                    clsMarsData.CreateDataID("calendarattr", "calendarid") & "," & _
                    "'" & SQLPrepare(NewName) & "'," & _
                    "'" & oRs("calendardate").Value & "')"

                    Dim Ok As Boolean = clsMarsData.WriteData(SQL)

                    If Ok = False Then Exit Do

                    oRs.MoveNext()

                    I += 1

                    oUI.BusyProgress((I / nCount) * 100, "Importing calendar data...")
                Loop

                oRs.Close()
            End If

            If Append = True Then
                cmbName.Text = NewName
                _ViewCalendar()
            Else
                'cmbName.Items.Add(NewName)
                'cmbName.SelectedItem = cmbName.Text
                cmbName.Text = NewName
            End If

            oUI.BusyProgress(95, "Cleaning up...")

            System.IO.File.Delete(NewFilename)

            oUI.BusyProgress(, , True)

            MessageBox.Show("Calendar imported successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub cmdExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdExport.Click
        Dim FileName As String
        Dim Path As String
        Dim SQL As String
        Dim oUI As New clsMarsUI
        Dim oRs As ADODB.Recordset

        sfd.OverwritePrompt = True
        sfd.Title = "Save calendar as..."

        sfd.ShowDialog()

        If sfd.FileName.Length = 0 Then Return

        Path = sfd.FileName.ToLower

        If Path.Substring(Path.Length - 4, 4) <> ".cal" Then
            Path &= ".cal"
        End If

        oUI.BusyProgress(25, "Opening calendar...")

        Path = Path.Replace(".cal", ".xml")

        SQL = "SELECT * FROM CalendarAttr WHERE CalendarName = '" & cmbName.Text & "'"

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then

            oUI.BusyProgress(60, "Configurating calendar for export...")

            If oRs.EOF = False Then
                oData.CreateXML(oRs, Path)
            Else
                MessageBox.Show("Please save the calendar before attempting to export it", Application.ProductName, _
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                oRs.Close()

                oUI.BusyProgress(, , True)
                Return
            End If

        End If

        oUI.BusyProgress(95, "Finalizing export...")

        System.IO.File.Copy(Path, Path.Replace(".xml", ".cal"), True)

        System.IO.File.Delete(Path)


        oUI.BusyProgress(, , True)

        MessageBox.Show("Calendar Exported Successfully", _
        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub lsvDates_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvDates.SelectedIndexChanged
        If lsvDates.SelectedItems.Count = 0 Then Return

        Dim oDate As Date = lsvDates.SelectedItems(0).Text

        oCal.SetSelectionRange(oDate, oDate)

        oCal.Update()
    End Sub

    Private Sub cmdShare_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdShare.Click
        Dim oUI As New clsMarsUI

        Try
            Dim FileName As String
            Dim Path As String
            Dim NewPath As String
            Dim oRs As New ADODB.Recordset
            Dim SQL As String


            FileName = cmbName.Text


            If FileName.EndsWith(".cal") = False Then FileName &= ".cal"

            Path = sAppPath & "Output\"

            Path = Path & FileName

            oUI.BusyProgress(25, "Opening calendar...")

            Path = Replace(Path, ".cal", ".xml")

            SQL = "SELECT * FROM CalendarAttr WHERE " & _
            "CalendarName = '" & SQLPrepare(cmbName.Text) & "'"

            oRs = clsMarsData.GetData(SQL)

            oUI.BusyProgress(60, "Configurating calendar for sharing...")

            If Not oRs Is Nothing Then
                If oRs.EOF = False Then
                    oData.CreateXML(oRs, Path)
                End If
            End If


            oUI.BusyProgress(75, "Finalizing export...")

            NewPath = Path.Replace(".xml", ".cal")

            System.IO.File.Copy(Path, NewPath)

            oUI.BusyProgress(85, "Sending calendar...")

            Dim oMsg As New clsMarsMessaging

            If MailType = MarsGlobal.gMailType.SMTP Or _
            MailType = MarsGlobal.gMailType.SQLRDMAIL Then
                oMsg.SendSMTP("support@christiansteven.com", CustID, _
                    "Sharing: " & cmbName.Text, "Single", NewPath)
            ElseIf MailType = gMailType.MAPI Then
                oMsg.SendMAPI("support@christiansteven.com", CustID, _
                    "Sharing: " & cmbName.Text, "Single", NewPath)
            ElseIf MailType = gMailType.GROUPWISE Then
                oMsg.SendGROUPWISE("support@christiansteven.com", "", "", CustID, "Sharing: " & cmbName.Text, NewPath, "Single")
            End If

            Try
                System.IO.File.Delete(Path)
                System.IO.File.Delete(NewPath)
            Catch : End Try

            MessageBox.Show("Calendar shared successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), _
            "Please save the calendar before attempting to share it.")
        Finally
            oUI.BusyProgress(100, "", True)
        End Try
    End Sub

    Private Sub cmdUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        Try
            UpdateSchedules(cmbName.Text)
            MessageBox.Show("All schedules updated successfully!", _
            Application.ProductName, _
            MessageBoxButtons.OK, _
            MessageBoxIcon.Information)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub


    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        cmbName.Text = String.Empty
        lsvDates.Items.Clear()
        oCal.RemoveAllBoldedDates()
        oCal.UpdateBoldedDates()
    End Sub

    Private Sub cmbCountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCountry.SelectedIndexChanged
        Dim hols As Dls.Holidays.HolidayService = New Dls.Holidays.HolidayService
        Dim csshols As cssholidays.holiday = New cssholidays.holiday
        Dim countryCode As String
        Dim year As Integer = Now.Year

        lsvHolidays.Items.Clear()

        countryCode = Me.m_CountryCode


        Dim ds As DataSet


        If countryCode = "AUS" Or countryCode = "NZ" Or countryCode = "CA" Then
            ds = csshols.GetHolidaysForYear(countryCode, year)
        Else
            ds = hols.GetHolidaysForYear(countryCode, year)
        End If

        Dim dt As DataTable = ds.Tables(0)

        For Each row As DataRow In dt.Rows
            Dim it As ListViewItem = New ListViewItem

            it.Text = row("Name")

            lsvHolidays.Items.Add(it)
        Next

        For Each it As ListViewItem In lsvHolidays.Items

            Dim dateVal As String = ""

            year = Now.Year

            Do
                If countryCode = "AUS" Or countryCode = "NZ" Or countryCode = "CA" Then
                    ds = csshols.GetHolidaysForYear(countryCode, year)
                Else
                    ds = hols.GetHolidaysForYear(countryCode, year)
                End If

                dt = ds.Tables(0)

                Dim rows() As DataRow = dt.Select("Name ='" & SQLPrepare(it.Text) & "'")

                If rows.Length > 0 Then
                    Dim row As DataRow = rows(0)
                    dateVal = ConDate(row("Date"))
                End If

                year += 1
            Loop Until dateVal > ConDate(Now)

            it.Tag = dateVal
        Next

    End Sub

    Private Sub lsvHolidays_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvHolidays.DoubleClick
        If lsvHolidays.SelectedItems.Count = 0 Then Return

        For Each hol As ListViewItem In lsvHolidays.SelectedItems
            Dim d As Date = hol.Tag

            For Each it As ListViewItem In lsvDates.Items
                If ConDate(d) = it.Text Then
                    Return
                End If
            Next

            Dim itx As ListViewItem = New ListViewItem

            itx.Text = ConDate(d)
            itx.Tag = Me.m_CountryCode & "|" & hol.Text

            lsvDates.Items.Add(itx)

            oCal.AddBoldedDate(d)
            oCal.UpdateBoldedDates()

            oCal.SetDate(d)
        Next
    End Sub





    Private Sub oCal_DateSelected(ByVal sender As Object, ByVal e As System.Windows.Forms.DateRangeEventArgs) Handles oCal.DateSelected
        If formLoaded = False Then Return

        Dim oDate As Date
        Dim AlreadyBold As Boolean = False

        If oCal.BoldedDates.GetLength(0) > 0 Then
            For Each d As Date In oCal.BoldedDates
                If e.Start = d Then
                    oCal.RemoveBoldedDate(d)
                    oCal.UpdateBoldedDates()

                    AlreadyBold = True

                    For Each oitem As ListViewItem In lsvDates.Items
                        If oitem.Text = ConDate(d) Then
                            oitem.Remove()
                        End If
                    Next

                    Exit For
                Else
                    AlreadyBold = False
                End If
            Next
        End If

        If AlreadyBold = False Then
            oDate = New Date(e.Start.Year, e.Start.Month, e.Start.Day)

            oCal.AddBoldedDate(e.Start)
            oCal.UpdateBoldedDates()
            lsvDates.Items.Add(ConDate(oDate))
        End If
    End Sub

    Private Sub lsvHolidays_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lsvHolidays.KeyUp
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Return Then
            Me.lsvHolidays_DoubleClick(sender, e)
        End If
    End Sub
End Class
