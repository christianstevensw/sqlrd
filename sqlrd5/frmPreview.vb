

Friend Class frmPreview
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim oDialog As Boolean = False
    Friend WithEvents UcPDFViewer1 As sqlrd.ucPDFViewer
    Dim sTitle As String


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.


    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPreview))
        Me.UcPDFViewer1 = New sqlrd.ucPDFViewer
        Me.SuspendLayout()
        '
        'UcPDFViewer1
        '
        Me.UcPDFViewer1.Location = New System.Drawing.Point(0, 427)
        Me.UcPDFViewer1.Name = "UcPDFViewer1"
        Me.UcPDFViewer1.Size = New System.Drawing.Size(518, 84)
        Me.UcPDFViewer1.TabIndex = 0
        Me.UcPDFViewer1.Visible = False
        '
        'frmPreview
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(704, 760)
        Me.Controls.Add(Me.UcPDFViewer1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmPreview"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Report Preview"
        Me.ResumeLayout(False)

    End Sub

#End Region


    Public Overloads Function PreviewReport(ByVal arv As Microsoft.Reporting.WinForms.ReportViewer, ByVal sTitle As String, Optional ByVal MeShowDialog As Boolean = False)
        Me.UcPDFViewer1.Visible = False

        Me.Controls.Add(arv)
        arv.Visible = True
        arv.Dock = DockStyle.Fill
        arv.Refresh()
        arv.ZoomMode = Microsoft.Reporting.WinForms.ZoomMode.PageWidth
        arv.RefreshReport()

        oDialog = MeShowDialog

        Me.sTitle = sTitle

        If Me.oDialog = False Then
            Me.MdiParent = oMain
            Me.ShowInTaskbar = False
            Me.Show()
        Else
            Me.ShowDialog()
        End If
    End Function
    Public Overloads Function PreviewReport(ByVal sFile As String, ByVal sTitle As String, Optional ByVal MeShowDialog As Boolean = False)

        oDialog = MeShowDialog

        Me.sTitle = sTitle
        Me.UcPDFViewer1.Visible = True
        Me.UcPDFViewer1.Dock = DockStyle.Fill

        Me.UcPDFViewer1._View(sFile)

        If oDialog = False Then
            Me.Show()
        Else
            Me.ShowDialog()
        End If
    End Function

    Private Sub frmPreview_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        If oDialog = False Then Me.MdiParent = oMain

        Me.Text = "Report Preview - " & sTitle
        'Me.rv.RefreshReport()
    End Sub


End Class
