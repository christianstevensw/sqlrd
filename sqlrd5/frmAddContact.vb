Public Class frmAddContact
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim UserCancel As Boolean
    Dim sType As String
    Dim oData As New clsMarsData
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents ReflectionImage1 As DevComponents.DotNetBar.Controls.ReflectionImage
    Public isEdit As Boolean = False
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents TabItem1 As DevComponents.DotNetBar.TabItem
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtAddress As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtPhone As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtFax As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents TabItem2 As DevComponents.DotNetBar.TabItem
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtGroupName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents lsEmails As System.Windows.Forms.ListBox
    Friend WithEvents pnGroup As System.Windows.Forms.Panel
    Friend WithEvents pnContact As System.Windows.Forms.Panel
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents cmdAddEmail As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdRemoveEmail As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmbAddress As AutoCompleteCombo.CompletionCombo
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAddContact))
        Me.cmdAddEmail = New DevComponents.DotNetBar.ButtonX()
        Me.lsEmails = New System.Windows.Forms.ListBox()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.txtGroupName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.cmdRemoveEmail = New DevComponents.DotNetBar.ButtonX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.TabItem2 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.txtAddress = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.txtPhone = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtFax = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.TabItem1 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.pnGroup = New System.Windows.Forms.Panel()
        Me.cmbAddress = New AutoCompleteCombo.CompletionCombo()
        Me.pnContact = New System.Windows.Forms.Panel()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ReflectionImage1 = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.pnGroup.SuspendLayout()
        Me.pnContact.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdAddEmail
        '
        Me.cmdAddEmail.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAddEmail.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAddEmail.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAddEmail.Image = CType(resources.GetObject("cmdAddEmail.Image"), System.Drawing.Image)
        Me.cmdAddEmail.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddEmail.Location = New System.Drawing.Point(336, 104)
        Me.cmdAddEmail.Name = "cmdAddEmail"
        Me.cmdAddEmail.Size = New System.Drawing.Size(40, 23)
        Me.cmdAddEmail.TabIndex = 3
        '
        'lsEmails
        '
        Me.lsEmails.Location = New System.Drawing.Point(8, 136)
        Me.lsEmails.Name = "lsEmails"
        Me.lsEmails.Size = New System.Drawing.Size(368, 147)
        Me.lsEmails.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label5.BackgroundStyle.Class = ""
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(8, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(100, 16)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Group Name"
        '
        'txtGroupName
        '
        '
        '
        '
        Me.txtGroupName.Border.Class = "TextBoxBorder"
        Me.txtGroupName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtGroupName.Location = New System.Drawing.Point(8, 24)
        Me.txtGroupName.Name = "txtGroupName"
        Me.txtGroupName.Size = New System.Drawing.Size(368, 21)
        Me.txtGroupName.TabIndex = 0
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label6.BackgroundStyle.Class = ""
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(8, 56)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(100, 16)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Add Contact"
        '
        'cmdRemoveEmail
        '
        Me.cmdRemoveEmail.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRemoveEmail.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRemoveEmail.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRemoveEmail.Image = CType(resources.GetObject("cmdRemoveEmail.Image"), System.Drawing.Image)
        Me.cmdRemoveEmail.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveEmail.Location = New System.Drawing.Point(8, 104)
        Me.cmdRemoveEmail.Name = "cmdRemoveEmail"
        Me.cmdRemoveEmail.Size = New System.Drawing.Size(40, 23)
        Me.cmdRemoveEmail.TabIndex = 5
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(393, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(474, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 49
        Me.cmdCancel.Text = "&Cancel"
        '
        'TabItem2
        '
        Me.TabItem2.Name = "TabItem2"
        Me.TabItem2.Text = "Group Details"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Contact Name"
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.Location = New System.Drawing.Point(112, 16)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(240, 21)
        Me.txtName.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Email Address"
        '
        'txtAddress
        '
        '
        '
        '
        Me.txtAddress.Border.Class = "TextBoxBorder"
        Me.txtAddress.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtAddress.Location = New System.Drawing.Point(112, 56)
        Me.txtAddress.Multiline = True
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtAddress.Size = New System.Drawing.Size(240, 131)
        Me.txtAddress.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label3.BackgroundStyle.Class = ""
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 210)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 16)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Phone Number"
        '
        'txtPhone
        '
        '
        '
        '
        Me.txtPhone.Border.Class = "TextBoxBorder"
        Me.txtPhone.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPhone.Location = New System.Drawing.Point(112, 208)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(240, 21)
        Me.txtPhone.TabIndex = 2
        '
        'txtFax
        '
        '
        '
        '
        Me.txtFax.Border.Class = "TextBoxBorder"
        Me.txtFax.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFax.Location = New System.Drawing.Point(112, 256)
        Me.txtFax.Name = "txtFax"
        Me.txtFax.Size = New System.Drawing.Size(240, 21)
        Me.txtFax.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label4.BackgroundStyle.Class = ""
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 258)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(100, 16)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Fax Number"
        '
        'TabItem1
        '
        Me.TabItem1.Name = "TabItem1"
        Me.TabItem1.Text = "Contact Details"
        '
        'pnGroup
        '
        Me.pnGroup.Controls.Add(Me.cmbAddress)
        Me.pnGroup.Controls.Add(Me.cmdRemoveEmail)
        Me.pnGroup.Controls.Add(Me.Label6)
        Me.pnGroup.Controls.Add(Me.txtGroupName)
        Me.pnGroup.Controls.Add(Me.Label5)
        Me.pnGroup.Controls.Add(Me.lsEmails)
        Me.pnGroup.Controls.Add(Me.cmdAddEmail)
        Me.pnGroup.Location = New System.Drawing.Point(1, 1)
        Me.pnGroup.Name = "pnGroup"
        Me.pnGroup.Size = New System.Drawing.Size(408, 296)
        Me.pnGroup.TabIndex = 0
        '
        'cmbAddress
        '
        Me.cmbAddress.ItemHeight = 13
        Me.cmbAddress.Location = New System.Drawing.Point(8, 72)
        Me.cmbAddress.Name = "cmbAddress"
        Me.cmbAddress.Size = New System.Drawing.Size(368, 21)
        Me.cmbAddress.TabIndex = 2
        '
        'pnContact
        '
        Me.pnContact.BackColor = System.Drawing.Color.Transparent
        Me.pnContact.Controls.Add(Me.txtFax)
        Me.pnContact.Controls.Add(Me.txtPhone)
        Me.pnContact.Controls.Add(Me.Label3)
        Me.pnContact.Controls.Add(Me.txtAddress)
        Me.pnContact.Controls.Add(Me.Label2)
        Me.pnContact.Controls.Add(Me.Label4)
        Me.pnContact.Controls.Add(Me.txtName)
        Me.pnContact.Controls.Add(Me.Label1)
        Me.pnContact.Location = New System.Drawing.Point(1, 1)
        Me.pnContact.Name = "pnContact"
        Me.pnContact.Size = New System.Drawing.Size(408, 296)
        Me.pnContact.TabIndex = 9
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'ReflectionImage1
        '
        '
        '
        '
        Me.ReflectionImage1.BackgroundStyle.Class = ""
        Me.ReflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ReflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.ReflectionImage1.Image = CType(resources.GetObject("ReflectionImage1.Image"), System.Drawing.Image)
        Me.ReflectionImage1.Location = New System.Drawing.Point(415, -2)
        Me.ReflectionImage1.Name = "ReflectionImage1"
        Me.ReflectionImage1.Size = New System.Drawing.Size(137, 299)
        Me.ReflectionImage1.TabIndex = 51
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 300)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(552, 32)
        Me.FlowLayoutPanel1.TabIndex = 52
        '
        'frmAddContact
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(552, 332)
        Me.ControlBox = False
        Me.Controls.Add(Me.pnContact)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.ReflectionImage1)
        Me.Controls.Add(Me.pnGroup)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmAddContact"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add Contact"
        Me.pnGroup.ResumeLayout(False)
        Me.pnContact.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmAddContact_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub cmdAddFiles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddEmail.Click
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData

        If cmbAddress.Text.Length = 0 Then Return

        If lsEmails.Items.IndexOf(cmbAddress.Text) > -1 Then Return

        If cmbAddress.Text.IndexOf("@") < 0 Then
            oRs = clsMarsData.GetData("SELECT EmailAddress FROM ContactAttr c " & _
            "INNER JOIN ContactDetail x ON c.ContactID = x.ContactID WHERE " & _
            "ContactName = '" & SQLPrepare(cmbAddress.Text) & "'")

            If Not oRs Is Nothing Then
                If oRs.EOF = False Then
                    lsEmails.Items.Add(cmbAddress.Text)
                    cmbAddress.Items.Remove(cmbAddress.Text)
                Else
                    SetError(cmbAddress, "Could not find entry in the address book")
                    cmbAddress.FindForm()
                    Return
                End If

                oRs.Close()
            End If
        Else
            lsEmails.Items.Add(cmbAddress.Text)
        End If

        cmbAddress.Text = String.Empty
        cmbAddress.Focus()
    End Sub



    Private Sub cmdRemoveEmail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveEmail.Click
        If lsEmails.SelectedItems.Count = 0 Then Return

        Dim s As String = lsEmails.SelectedItem

        If s.IndexOf("@") < 0 Then
            If cmbAddress.Items.IndexOf(s) < 0 Then
                cmbAddress.Items.Add(s)
            End If
        End If

        lsEmails.Items.Remove(lsEmails.SelectedItems(0))
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub
    Public Sub EditGroup(ByVal nID As Integer)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim OldName As String

        Try
            SQL = "SELECT * FROM ContactAttr c LEFT OUTER JOIN ContactDetail x ON " & _
            "c.ContactID = x.ContactID WHERE c.ContactID = " & nID

            oRs = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then Return

            Do While oRs.EOF = False
                txtGroupName.Text = oRs("contactname").Value
                sType = oRs("contacttype").Value
                lsEmails.Items.Add(oRs("emailaddress").Value)
                oRs.MoveNext()
            Loop

            OldName = txtGroupName.Text

            oRs.Close()

            oRs = clsMarsData.GetData("SELECT ContactName FROM ContactAttr WHERE ContactType = 'contact'")

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    cmbAddress.Items.Add(oRs.Fields(0).Value)
                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            pnGroup.BringToFront()

            Me.Text = "Edit Group"

        Catch : End Try

        Me.ShowDialog()

        Try

            If UserCancel = True Then Return

            Dim sVals As String
            Dim sWhere As String = " WHERE ContactID =" & nID

            sVals = "ContactName = '" & SQLPrepare(txtGroupName.Text) & "'"

            SQL = "UPDATE ContactAttr SET " & sVals & sWhere

            If clsMarsData.WriteData(SQL) = True Then

                clsMarsData.WriteData("DELETE FROM ContactDetail" & sWhere)

                Dim nCount As Integer
                Dim sEmail As String
                Dim oList As ListBox.ObjectCollection
                Dim sCols

                nCount = lsEmails.Items.Count

                sCols = "DetailID,ContactID,EmailAddress"

                For I As Integer = 0 To nCount - 1
                    Try
                        oList = lsEmails.Items
                        sEmail = oList.Item(I)
                    Catch ex As Exception
                        sEmail = String.Empty
                    End Try

                    sVals = clsMarsData.CreateDataID("contactdetail", "detailid") & "," & _
                        nID & "," & _
                        "'" & SQLPrepare(sEmail) & "'"

                    SQL = "INSERT INTO ContactDetail (" & sCols & ") " & _
                    "VALUES(" & sVals & ")"

                    clsMarsData.WriteData(SQL)
                Next

                If gConType = "DAT" Then
                    Dim nDestID As Integer

                    SQL = "SELECT DestinationID,SendTo,Cc,Bcc FROM DestinationAttr"

                    oRs = clsMarsData.GetData(SQL)

                    If Not oRs Is Nothing Then
                        Do While oRs.EOF = False
                            nDestID = oRs("destinationid").Value

                            SQL = "UPDATE DestinationAttr SET " & _
                            "SendTo = '" & SQLPrepare(oRs("sendto").Value).Replace(OldName & ">", SQLPrepare(txtGroupName.Text) & ">") & "'," & _
                            "Cc = '" & SQLPrepare(oRs("cc").Value).Replace(OldName & ">", SQLPrepare(txtGroupName.Text) & ">") & "'," & _
                            "Bcc = '" & SQLPrepare(oRs("bcc").Value).Replace(OldName & ">", SQLPrepare(txtGroupName.Text) & ">") & "' " & _
                            "WHERE DestinationID =" & nDestID

                            clsMarsData.WriteData(SQL)

                            oRs.MoveNext()
                        Loop

                        oRs.Close()
                    End If
                Else
                    SQL = "UPDATE DestinationAttr SET SendTo = REPLACE(CAST(SendTo AS VARCHAR(1000)),'" & SQLPrepare(OldName) & ">','" & SQLPrepare(txtGroupName.Text) & ">')"

                    clsMarsData.WriteData(SQL)

                    SQL = "UPDATE DestinationAttr SET Cc = REPLACE(CAST(Cc AS VARCHAR(1000)),'" & SQLPrepare(OldName) & ">','" & SQLPrepare(txtGroupName.Text) & ">')"

                    clsMarsData.WriteData(SQL)

                    SQL = "UPDATE DestinationAttr SET Bcc = REPLACE(CAST(Bcc AS VARCHAR(1000)),'" & SQLPrepare(OldName) & ">','" & SQLPrepare(txtGroupName.Text) & ">')"

                    clsMarsData.WriteData(SQL)
                End If
            End If

            MessageBox.Show("Group details updated successfully. All schedules using this group have been updated.", _
            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch : End Try
    End Sub
    Public Sub EditContact(ByVal nID As Integer)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim OldName As String
        Dim nDestID As Integer

        SQL = "SELECT * FROM ContactAttr c INNER JOIN ContactDetail x ON " & _
        "c.ContactID = x.ContactID WHERE c.ContactID = " & nID

        oRs = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then Return

        If oRs.EOF = False Then
            txtName.Text = oRs("contactname").Value
            sType = oRs("contacttype").Value
            txtAddress.Text = oRs("emailaddress").Value
            txtPhone.Text = IsNull(oRs("phonenumber").Value)
            txtFax.Text = IsNull(oRs("faxnumber").Value)
        End If

        OldName = txtName.Text

        oRs.Close()

        pnContact.BringToFront()

        Me.Text = "Edit Contact"

        Me.ShowDialog()

        If UserCancel = True Then Return

        Dim sVals As String
        Dim sWhere As String = " WHERE ContactID =" & nID

        sVals = "ContactName = '" & SQLPrepare(txtName.Text) & "'"

        SQL = "UPDATE ContactAttr SET " & sVals & sWhere

        clsMarsData.WriteData(SQL)

        sVals = "EmailAddress ='" & SQLPrepare(txtAddress.Text) & "'," & _
        "PhoneNumber = '" & SQLPrepare(txtPhone.Text) & "'," & _
        "FaxNumber = '" & SQLPrepare(txtFax.Text) & "'"

        SQL = "UPDATE ContactDetail SET " & sVals & sWhere

        clsMarsData.WriteData(SQL)

        If gConType = "DAT" Then
            SQL = "SELECT DestinationID,SendTo,Cc,Bcc FROM DestinationAttr"

            oRs = clsMarsData.GetData(SQL)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    nDestID = oRs("destinationid").Value

                    Dim Cc As String = ""
                    Dim SendTo As String = ""
                    Dim Bcc As String = ""

                    SendTo = IsNull(oRs("sendto").Value)
                    Cc = IsNull(oRs("cc").Value)
                    Bcc = IsNull(oRs("bcc").Value)

                    SQL = "UPDATE DestinationAttr SET " & _
                    "SendTo = '" & SQLPrepare(SendTo).Replace(OldName & ">", SQLPrepare(txtName.Text) & ">") & "'," & _
                    "Cc = '" & SQLPrepare(Cc).Replace(OldName & ">", SQLPrepare(txtName.Text) & ">") & "'," & _
                    "Bcc = '" & SQLPrepare(Bcc).Replace(OldName & ">", SQLPrepare(txtName.Text) & ">") & "' " & _
                    "WHERE DestinationID =" & nDestID

                    clsMarsData.WriteData(SQL)

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If
        Else
            SQL = "UPDATE DestinationAttr SET SendTo = REPLACE(CAST(SendTo AS VARCHAR(1000)),'" & SQLPrepare(OldName) & ">','" & SQLPrepare(txtName.Text) & ">')"

            clsMarsData.WriteData(SQL)

            SQL = "UPDATE DestinationAttr SET Cc = REPLACE(CAST(Cc AS VARCHAR(1000)),'" & SQLPrepare(OldName) & ">','" & SQLPrepare(txtName.Text) & ">')"

            clsMarsData.WriteData(SQL)

            SQL = "UPDATE DestinationAttr SET Bcc = REPLACE(CAST(Bcc AS VARCHAR(1000)),'" & SQLPrepare(OldName) & ">','" & SQLPrepare(txtName.Text) & ">')"

            clsMarsData.WriteData(SQL)
        End If

        clsMarsData.WriteData("UPDATE ContactDetail SET EmailAddress ='" & SQLPrepare(txtName.Text) & "' WHERE " & _
        "EmailAddress LIKE '" & SQLPrepare(OldName) & "'")

        MessageBox.Show("Contact details updated successfully. All schedules using this contact have been updated.", _
        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
    Public Function AddContact() As String
        pnContact.BringToFront()
        Me.Text = "Add Contact"
        sType = "contact"

        Me.ShowDialog()

        If UserCancel = True Then Return String.Empty

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim nID As Int64
        Dim oData As New clsMarsData

        nID = clsMarsData.CreateDataID("contactattr", "contactid")

        sCols = "ContactID,ContactName,ContactType"

        sVals = nID & "," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            "'" & sType & "'"

        SQL = "INSERT INTO COntactAttr (" & sCols & ") VALUES (" & sVals & ")"

        Dim OK As Boolean = clsMarsData.WriteData(SQL)

        If OK = True Then
            sCols = "DetailID,ContactID,EmailAddress,PhoneNumber,FaxNumber"

            sVals = clsMarsData.CreateDataID("contactdetail", "detailid") & "," & _
            nID & "," & _
            "'" & SQLPrepare(txtAddress.Text) & "'," & _
            "'" & SQLPrepare(txtPhone.Text) & "'," & _
            "'" & SQLPrepare(txtFax.Text) & "'"

            SQL = "INSERT INTO ContactDetail (" & sCols & ") VALUES (" & sVals & ")"

            OK = clsMarsData.WriteData(SQL)

            If OK = False Then
                clsMarsData.WriteData("DELETE FROM ContactAttr WHERE ContactID =" & nID)
            End If

            Return txtName.Text
        Else
            Return String.Empty
        End If

    End Function

    Public Function AddGroup() As String
        Dim oData As New clsMarsData
        pnGroup.BringToFront()
        Me.Text = "Add Group"

        sType = "group"

        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT ContactName FROM ContactAttr WHERE ContactType = 'contact'")

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                cmbAddress.Items.Add(oRs.Fields(0).Value)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        Me.ShowDialog()

        If UserCancel = True Then Return String.Empty

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim nID As Int64


        nID = clsMarsData.CreateDataID("contactattr", "contactid")

        sCols = "ContactID,ContactName,ContactType"

        sVals = nID & "," & _
            "'" & SQLPrepare(txtGroupName.Text) & "'," & _
            "'" & sType & "'"

        SQL = "INSERT INTO COntactAttr (" & sCols & ") VALUES (" & sVals & ")"

        Dim OK As Boolean = clsMarsData.WriteData(SQL)

        If OK = True Then
            Dim nCount As Integer
            Dim sEmail As String
            Dim oList As ListBox.ObjectCollection

            nCount = lsEmails.Items.Count

            sCols = "DetailID,ContactID,EmailAddress"


            For I As Integer = 0 To nCount - 1
                Try
                    oList = lsEmails.Items
                    sEmail = oList.Item(I)
                Catch ex As Exception
                    sEmail = String.Empty
                End Try

                sVals = clsMarsData.CreateDataID("contactdetail", "detailid") & "," & _
                    nID & "," & _
                    "'" & SQLPrepare(sEmail) & "'"

                SQL = "INSERT INTO ContactDetail (" & sCols & ") VALUES(" & sVals & ")"

                OK = clsMarsData.WriteData(SQL)

                If OK = False Then
                    clsMarsData.WriteData("DELETE FROM ContactAttr WHERE ContactID =" & nID)
                    clsMarsData.WriteData("DELETE FROM ContactDetail WHERE ContactID =" & nID)
                    Exit For
                End If
            Next

            Return txtGroupName.Text
        Else
            Return String.Empty
        End If
    End Function

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Select Case sType
            Case "contact"
                If txtName.Text.Length = 0 Then
                    SetError(txtName, "Please enter the name of the contact")
                    txtName.Focus()
                    Return
                ElseIf txtAddress.Text.Length = 0 Then
                    SetError(txtAddress, "Please enter an email address")
                    txtAddress.Focus()
                    Return
                ElseIf clsMarsData.IsDuplicate("ContactAttr", "ContactName", _
                txtName.Text, False) = True And isEdit = False Then
                    SetError(txtName, "A contact " & _
                    "already exist with this name")
                    txtName.Focus()
                    Return
                ElseIf txtAddress.Text.IndexOf("@") < 0 Then
                    SetError(txtAddress, "Please enter a valid email address e.g. billtester@mycompany.com")
                    txtAddress.Focus()
                    Return
                End If
            Case "group"
                If txtGroupName.Text.Length = 0 Then
                    SetError(txtGroupName, "Please enter the group name")
                    txtGroupName.Focus()
                    Return
                ElseIf lsEmails.Items.Count = 0 Then
                    SetError(lsEmails, "Please add an email address")
                    lsEmails.Focus()
                    Return
                ElseIf clsMarsData.IsDuplicate("ContactAttr", "ContactName", _
                txtGroupName.Text, False) = True And isEdit = False Then
                    SetError(txtGroupName, "A group " & _
                    "already exist with this name")
                    txtName.Focus()
                    Return
                End If
        End Select

        Close()
    End Sub



    Private Sub cmbAddress_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim oUI As New clsMarsUI

        oUI.AutoCompleteLeave(cmbAddress)
    End Sub


    Private Sub cmbAddress_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbAddress.KeyUp
        If e.KeyCode = Keys.Enter Then
            cmdAddFiles_Click(sender, e)
        End If
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        SetError(sender, "")
    End Sub

    Private Sub txtAddress_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAddress.TextChanged
        SetError(sender, "")
    End Sub

    Private Sub txtGroupName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtGroupName.TextChanged
        SetError(sender, "")
    End Sub



    Private Sub txtAddress_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtAddress.KeyDown

    End Sub

    Private Sub txtAddress_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtAddress.KeyUp
        'If e.KeyCode = Keys.Space Then
        '    txtAddress.Text = txtAddress.Text.Replace(" ", String.Empty)
        '    SendKeys.Send("{END}")
        'ElseIf e.KeyCode = Keys.Enter Then
        '    txtAddress.Text = txtAddress.Text.Replace(vbCrLf, String.Empty)
        '    SendKeys.Send("{END}")
        'End If
    End Sub

    Private Sub txtAddress_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAddress.KeyPress
        If Char.IsWhiteSpace(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub
End Class
