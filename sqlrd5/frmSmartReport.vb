Imports System.Data
Imports System.Data.OleDb
Imports System.Data.Odbc
Imports Microsoft.Win32
Imports DevComponents.DotNetBar
Imports Xceed.Grid
Imports Xceed.Grid.Editors
Imports Xceed.Grid.Viewers
Imports Xceed.Grid.Collections
Imports Xceed.Editors
Imports Xceed.Grid.Reporting
Public Class frmSmartReport
    Inherits System.Windows.Forms.Form
    Dim DataGridPrinter1 As DataGridPrinter


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents PrintDialog1 As System.Windows.Forms.PrintDialog
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintPreviewDialog1 As System.Windows.Forms.PrintPreviewDialog
    Friend WithEvents TabItem1 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabItem2 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel1 As DevComponents.DotNetBar.TabControlPanel
    Private WithEvents myGrid As Xceed.Grid.GridControl
    Private WithEvents dataRowTemplate1 As Xceed.Grid.DataRow
    Private WithEvents GroupByRow1 As Xceed.Grid.GroupByRow
    Private WithEvents ColumnManagerRow1 As Xceed.Grid.ColumnManagerRow
    Friend WithEvents dtMan As DevComponents.DotNetBar.DotNetBarManager
    Friend WithEvents barBottomDockSite As DevComponents.DotNetBar.DockSite
    Friend WithEvents barLeftDockSite As DevComponents.DotNetBar.DockSite
    Friend WithEvents barRightDockSite As DevComponents.DotNetBar.DockSite
    Friend WithEvents barTopDockSite As DevComponents.DotNetBar.DockSite
    Friend WithEvents DockSite1 As DevComponents.DotNetBar.DockSite
    Friend WithEvents DockSite2 As DevComponents.DotNetBar.DockSite
    Friend WithEvents DockSite3 As DevComponents.DotNetBar.DockSite
    Friend WithEvents bar1 As DevComponents.DotNetBar.Bar
    Friend WithEvents btnPreview As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnPrint As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSave As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnReport As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnClose As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents DockSite4 As DevComponents.DotNetBar.DockSite
    Friend WithEvents statusBar2 As DevComponents.DotNetBar.Bar
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents tbReport As DevComponents.DotNetBar.TabControl
    '<System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSmartReport))
        Me.PrintDialog1 = New System.Windows.Forms.PrintDialog()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.PrintPreviewDialog1 = New System.Windows.Forms.PrintPreviewDialog()
        Me.TabItem1 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.tbReport = New DevComponents.DotNetBar.TabControl()
        Me.TabControlPanel1 = New DevComponents.DotNetBar.TabControlPanel()
        Me.myGrid = New Xceed.Grid.GridControl()
        Me.dataRowTemplate1 = New Xceed.Grid.DataRow()
        Me.GroupByRow1 = New Xceed.Grid.GroupByRow()
        Me.ColumnManagerRow1 = New Xceed.Grid.ColumnManagerRow()
        Me.TabItem2 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.dtMan = New DevComponents.DotNetBar.DotNetBarManager(Me.components)
        Me.barBottomDockSite = New DevComponents.DotNetBar.DockSite()
        Me.barLeftDockSite = New DevComponents.DotNetBar.DockSite()
        Me.barRightDockSite = New DevComponents.DotNetBar.DockSite()
        Me.barTopDockSite = New DevComponents.DotNetBar.DockSite()
        Me.DockSite1 = New DevComponents.DotNetBar.DockSite()
        Me.DockSite2 = New DevComponents.DotNetBar.DockSite()
        Me.DockSite3 = New DevComponents.DotNetBar.DockSite()
        Me.DockSite4 = New DevComponents.DotNetBar.DockSite()
        Me.bar1 = New DevComponents.DotNetBar.Bar()
        Me.btnPreview = New DevComponents.DotNetBar.ButtonItem()
        Me.btnPrint = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSave = New DevComponents.DotNetBar.ButtonItem()
        Me.btnReport = New DevComponents.DotNetBar.ButtonItem()
        Me.btnClose = New DevComponents.DotNetBar.ButtonItem()
        Me.statusBar2 = New DevComponents.DotNetBar.Bar()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        CType(Me.tbReport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbReport.SuspendLayout()
        Me.TabControlPanel1.SuspendLayout()
        CType(Me.myGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dataRowTemplate1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ColumnManagerRow1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DockSite3.SuspendLayout()
        Me.DockSite4.SuspendLayout()
        CType(Me.bar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.statusBar2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PrintDocument1
        '
        '
        'PrintPreviewDialog1
        '
        Me.PrintPreviewDialog1.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPreviewDialog1.Document = Me.PrintDocument1
        Me.PrintPreviewDialog1.Enabled = True
        Me.PrintPreviewDialog1.Icon = CType(resources.GetObject("PrintPreviewDialog1.Icon"), System.Drawing.Icon)
        Me.PrintPreviewDialog1.Name = "PrintPreviewDialog1"
        Me.PrintPreviewDialog1.UseAntiAlias = True
        Me.PrintPreviewDialog1.Visible = False
        '
        'TabItem1
        '
        Me.TabItem1.Name = "TabItem1"
        Me.TabItem1.Text = "TabItem1"
        '
        'tbReport
        '
        Me.tbReport.CanReorderTabs = True
        Me.tbReport.Controls.Add(Me.TabControlPanel1)
        Me.tbReport.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbReport.Location = New System.Drawing.Point(0, 25)
        Me.tbReport.Name = "tbReport"
        Me.tbReport.SelectedTabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.tbReport.SelectedTabIndex = 0
        Me.tbReport.Size = New System.Drawing.Size(728, 556)
        Me.tbReport.Style = DevComponents.DotNetBar.eTabStripStyle.VS2005Document
        Me.tbReport.TabIndex = 9
        Me.tbReport.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.tbReport.Tabs.Add(Me.TabItem2)
        '
        'TabControlPanel1
        '
        Me.TabControlPanel1.Controls.Add(Me.myGrid)
        Me.TabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel1.Location = New System.Drawing.Point(0, 26)
        Me.TabControlPanel1.Name = "TabControlPanel1"
        Me.TabControlPanel1.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel1.Size = New System.Drawing.Size(728, 530)
        Me.TabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.TabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.TabControlPanel1.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel1.Style.GradientAngle = 90
        Me.TabControlPanel1.TabIndex = 1
        Me.TabControlPanel1.TabItem = Me.TabItem2
        '
        'myGrid
        '
        Me.myGrid.DataRowTemplate = Me.dataRowTemplate1
        Me.myGrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.myGrid.FixedHeaderRows.Add(Me.GroupByRow1)
        Me.myGrid.FixedHeaderRows.Add(Me.ColumnManagerRow1)
        Me.myGrid.Location = New System.Drawing.Point(1, 1)
        Me.myGrid.Name = "myGrid"
        Me.myGrid.Size = New System.Drawing.Size(726, 528)
        Me.myGrid.TabIndex = 0
        '
        'TabItem2
        '
        Me.TabItem2.AttachedControl = Me.TabControlPanel1
        Me.TabItem2.Name = "TabItem2"
        Me.TabItem2.Text = "TabItem2"
        '
        'dtMan
        '
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1)
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC)
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA)
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV)
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX)
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ)
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlY)
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del)
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins)
        Me.dtMan.BottomDockSite = Me.barBottomDockSite
        Me.dtMan.LeftDockSite = Me.barLeftDockSite
        Me.dtMan.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.dtMan.ParentForm = Me
        Me.dtMan.RightDockSite = Me.barRightDockSite
        Me.dtMan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.dtMan.ToolbarBottomDockSite = Me.DockSite4
        Me.dtMan.ToolbarLeftDockSite = Me.DockSite1
        Me.dtMan.ToolbarRightDockSite = Me.DockSite2
        Me.dtMan.ToolbarTopDockSite = Me.DockSite3
        Me.dtMan.TopDockSite = Me.barTopDockSite
        '
        'barBottomDockSite
        '
        Me.barBottomDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.barBottomDockSite.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barBottomDockSite.DocumentDockContainer = New DevComponents.DotNetBar.DocumentDockContainer()
        Me.barBottomDockSite.Location = New System.Drawing.Point(0, 581)
        Me.barBottomDockSite.Name = "barBottomDockSite"
        Me.barBottomDockSite.Size = New System.Drawing.Size(728, 0)
        Me.barBottomDockSite.TabIndex = 13
        Me.barBottomDockSite.TabStop = False
        '
        'barLeftDockSite
        '
        Me.barLeftDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.barLeftDockSite.Dock = System.Windows.Forms.DockStyle.Left
        Me.barLeftDockSite.DocumentDockContainer = New DevComponents.DotNetBar.DocumentDockContainer()
        Me.barLeftDockSite.Location = New System.Drawing.Point(0, 25)
        Me.barLeftDockSite.Name = "barLeftDockSite"
        Me.barLeftDockSite.Size = New System.Drawing.Size(0, 556)
        Me.barLeftDockSite.TabIndex = 10
        Me.barLeftDockSite.TabStop = False
        '
        'barRightDockSite
        '
        Me.barRightDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.barRightDockSite.Dock = System.Windows.Forms.DockStyle.Right
        Me.barRightDockSite.DocumentDockContainer = New DevComponents.DotNetBar.DocumentDockContainer()
        Me.barRightDockSite.Location = New System.Drawing.Point(728, 25)
        Me.barRightDockSite.Name = "barRightDockSite"
        Me.barRightDockSite.Size = New System.Drawing.Size(0, 556)
        Me.barRightDockSite.TabIndex = 11
        Me.barRightDockSite.TabStop = False
        '
        'barTopDockSite
        '
        Me.barTopDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.barTopDockSite.Dock = System.Windows.Forms.DockStyle.Top
        Me.barTopDockSite.DocumentDockContainer = New DevComponents.DotNetBar.DocumentDockContainer()
        Me.barTopDockSite.Location = New System.Drawing.Point(0, 25)
        Me.barTopDockSite.Name = "barTopDockSite"
        Me.barTopDockSite.Size = New System.Drawing.Size(728, 0)
        Me.barTopDockSite.TabIndex = 12
        Me.barTopDockSite.TabStop = False
        '
        'DockSite1
        '
        Me.DockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite1.Dock = System.Windows.Forms.DockStyle.Left
        Me.DockSite1.Location = New System.Drawing.Point(0, 25)
        Me.DockSite1.Name = "DockSite1"
        Me.DockSite1.Size = New System.Drawing.Size(0, 556)
        Me.DockSite1.TabIndex = 14
        Me.DockSite1.TabStop = False
        '
        'DockSite2
        '
        Me.DockSite2.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite2.Dock = System.Windows.Forms.DockStyle.Right
        Me.DockSite2.Location = New System.Drawing.Point(728, 25)
        Me.DockSite2.Name = "DockSite2"
        Me.DockSite2.Size = New System.Drawing.Size(0, 556)
        Me.DockSite2.TabIndex = 15
        Me.DockSite2.TabStop = False
        '
        'DockSite3
        '
        Me.DockSite3.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite3.Controls.Add(Me.bar1)
        Me.DockSite3.Dock = System.Windows.Forms.DockStyle.Top
        Me.DockSite3.Location = New System.Drawing.Point(0, 0)
        Me.DockSite3.Name = "DockSite3"
        Me.DockSite3.Size = New System.Drawing.Size(728, 25)
        Me.DockSite3.TabIndex = 16
        Me.DockSite3.TabStop = False
        '
        'DockSite4
        '
        Me.DockSite4.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite4.Controls.Add(Me.statusBar2)
        Me.DockSite4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.DockSite4.Location = New System.Drawing.Point(0, 581)
        Me.DockSite4.Name = "DockSite4"
        Me.DockSite4.Size = New System.Drawing.Size(728, 20)
        Me.DockSite4.TabIndex = 17
        Me.DockSite4.TabStop = False
        '
        'bar1
        '
        Me.bar1.AccessibleDescription = "My Bar (bar1)"
        Me.bar1.AccessibleName = "My Bar"
        Me.bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar
        Me.bar1.DockSide = DevComponents.DotNetBar.eDockSide.Top
        Me.bar1.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003
        Me.bar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnPreview, Me.btnPrint, Me.btnSave, Me.btnReport, Me.btnClose})
        Me.bar1.Location = New System.Drawing.Point(0, 0)
        Me.bar1.Name = "bar1"
        Me.bar1.Size = New System.Drawing.Size(378, 25)
        Me.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bar1.TabIndex = 0
        Me.bar1.TabStop = False
        Me.bar1.Text = "My Bar"
        '
        'btnPreview
        '
        Me.btnPreview.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnPreview.GlobalName = "btnPreview"
        Me.btnPreview.Image = CType(resources.GetObject("btnPreview.Image"), System.Drawing.Image)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Text = "Print Preview"
        '
        'btnPrint
        '
        Me.btnPrint.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnPrint.GlobalName = "btnPrint"
        Me.btnPrint.Image = CType(resources.GetObject("btnPrint.Image"), System.Drawing.Image)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Text = "Print"
        '
        'btnSave
        '
        Me.btnSave.BeginGroup = True
        Me.btnSave.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnSave.GlobalName = "btnSave"
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Text = "Save to Disk"
        '
        'btnReport
        '
        Me.btnReport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnReport.GlobalName = "btnReport"
        Me.btnReport.Image = CType(resources.GetObject("btnReport.Image"), System.Drawing.Image)
        Me.btnReport.Name = "btnReport"
        Me.btnReport.Text = "Custom Report"
        '
        'btnClose
        '
        Me.btnClose.BeginGroup = True
        Me.btnClose.GlobalName = "btnClose"
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Far
        Me.btnClose.Name = "btnClose"
        '
        'statusBar2
        '
        Me.statusBar2.AccessibleDescription = "Status (statusBar2)"
        Me.statusBar2.AccessibleName = "Status"
        Me.statusBar2.AccessibleRole = System.Windows.Forms.AccessibleRole.StatusBar
        Me.statusBar2.DockSide = DevComponents.DotNetBar.eDockSide.Bottom
        Me.statusBar2.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.ResizeHandle
        Me.statusBar2.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem1})
        Me.statusBar2.ItemSpacing = 2
        Me.statusBar2.Location = New System.Drawing.Point(0, 0)
        Me.statusBar2.Name = "statusBar2"
        Me.statusBar2.Size = New System.Drawing.Size(728, 19)
        Me.statusBar2.Stretch = True
        Me.statusBar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.statusBar2.TabIndex = 0
        Me.statusBar2.TabStop = False
        Me.statusBar2.Text = "Status"
        '
        'LabelItem1
        '
        Me.LabelItem1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem1.GlobalName = "LabelItem1"
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Width = 136
        '
        'frmSmartReport
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(728, 601)
        Me.Controls.Add(Me.tbReport)
        Me.Controls.Add(Me.barLeftDockSite)
        Me.Controls.Add(Me.barRightDockSite)
        Me.Controls.Add(Me.barTopDockSite)
        Me.Controls.Add(Me.barBottomDockSite)
        Me.Controls.Add(Me.DockSite1)
        Me.Controls.Add(Me.DockSite2)
        Me.Controls.Add(Me.DockSite3)
        Me.Controls.Add(Me.DockSite4)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmSmartReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Smart Folder Report"
        CType(Me.tbReport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbReport.ResumeLayout(False)
        Me.TabControlPanel1.ResumeLayout(False)
        CType(Me.myGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dataRowTemplate1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ColumnManagerRow1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DockSite3.ResumeLayout(False)
        Me.DockSite4.ResumeLayout(False)
        CType(Me.bar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.statusBar2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Private Sub grid_QueryGroupKeys(ByVal sender As Object, ByVal e As Xceed.Grid.QueryGroupKeysEventArgs) Handles myGrid.QueryGroupKeys
        Dim i As Integer

        Try
            For i = 0 To e.GroupKeys.Count - 1
                If TypeOf e.GroupKeys(i) Is String Then
                    e.GroupKeys(i) = (CType(e.GroupKeys(i), String)).ToUpper()
                End If
            Next
        Catch exception As Exception
            MessageBox.Show(exception.ToString())
        End Try
    End Sub
    Public Function _RenderReport(ByVal sQuery() As String, ByVal sName As String, _
    Optional ByVal ShowReport As Boolean = True, Optional ByVal sOwner As String = "", _
    Optional ByVal IsSystem As Boolean = False) As DataSet
        Dim oSet As DataSet
        Dim oTable As DataTable
        Dim oData As New clsMarsData
        Dim oCol As DataColumn
        Dim nReportID, nPackID, nAutoID, nEventID As Integer
        Dim oUI As New clsMarsUI
        Dim oRs As ADODB.Recordset
        Dim I As Integer = 0
        Dim sDataCols() As String
        Dim oValues() As Object
        Dim z As Integer = 0

        Try

            If sOwner.Length = 0 Then
                sOwner = gUser
            End If

            oRs = clsMarsData.GetData("SELECT * FROM UserColumns  WHERE Owner ='" & sOwner & "' ORDER BY ColumnID")

            oTable = New DataTable("Schedules List")

            myGrid.Clear()

            myGrid.FixedHeaderRows.Add(New Xceed.Grid.GroupByRow)
            myGrid.FixedHeaderRows.Add(New Xceed.Grid.ColumnManagerRow)

            myGrid.ApplyStyleSheet(Xceed.Grid.StyleSheet.Document)


            With oTable.Columns
                .Add("Schedule Type")
                .Add("Name")
                .Add("Owner")
            End With


            Do While oRs.EOF = False
                oCol = New DataColumn(oRs("caption").Value)

                oTable.Columns.Add(oCol)

                ReDim Preserve sDataCols(I)

                sDataCols(I) = oRs("datacolumn").Value

                oRs.MoveNext()

                I += 1
            Loop

            oRs.Close()

            Dim columns() As String = New String() {"StartTime", "Frequency", "DestinationType", _
          "EndDate", "NextRun"} ', "Output Formats", "Repeat", "Repeat Interval", _
            ' "Repeat Until", "Start Date"}

            For Each colx As String In columns
                If oTable.Columns.Contains(colx) Then Continue For

                oTable.Columns.Add(colx)
                If sDataCols IsNot Nothing Then
                    If sDataCols.IndexOf(sDataCols, colx) = -1 Then
                        ReDim Preserve sDataCols(I)

                        sDataCols(I) = colx
                        I += 1
                    End If
                Else
                    ReDim Preserve sDataCols(I)

                    sDataCols(I) = colx
                    I += 1
                End If
            Next

            ReDim oValues(I + 2)

            I = 0

            'oRs.Close()

            Dim s As String

            'single schedules-------------------------------------------------------------------

            s = sQuery(0)

            oRs = clsMarsData.GetData(s)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    nReportID = oRs(clsMarsData.GetColumnIndex("ReportID", oRs)).Value

                    Dim oType As ADODB.Recordset

                    oType = clsMarsData.GetData("SELECT Dynamic,Bursting,IsDataDriven FROM ReportAttr WHERE ReportID = " & nReportID)

                    If oType.EOF = False Then
                        If IsNull(oType(0).Value, "0") = "1" Then
                            oValues(0) = "Dynamic Schedule"
                        ElseIf IsNull(oType(1).Value, "0") = "1" Then
                            oValues(0) = "Bursting Schedule"
                        ElseIf IsNull(oType(2).Value, "0") = "1" Then
                            oValues(0) = "Data-Driven Schedule"
                        Else
                            oValues(0) = "Single Schedule"
                        End If
                    End If

                    oType.Close()

                    oValues(1) = oRs("reporttitle").Value
                    oValues(2) = oRs("owner").Value

                    I = 3

                    If Not sDataCols Is Nothing Then
                        For Each x As String In sDataCols
                            If x.Length > 0 Then
                                Select Case x.ToLower
                                    Case "nextrun"
                                        If oRs("status").Value = 1 Then
                                            Dim val As Date = IsNull(oRs(x).Value)
                                            oValues(I) = CTimeZ(val, dateConvertType.READ)
                                        Else
                                            oValues(I) = "Disabled"
                                        End If
                                    Case "outputformat", "destinationtype"
                                        oValues(I) = oUI.GetDestinationValues(x, nReportID)
                                    Case "entrydate"
                                        oValues(I) = oUI.GetLastRun(nReportID)
                                    Case "success"
                                        oValues(I) = oUI.GetLastResult(nReportID)
                                    Case "starttime"
                                        Dim val As Date = ConDate(Now) & " " & ConTime(IsNull(oRs(x).Value, Now))
                                        oValues(I) = CTimeZ(val, dateConvertType.READ)
                                    Case Else
                                        oValues(I) = IsNull(oRs(x).Value)
                                End Select
                            End If
                            I += 1
                        Next
                    End If

                    I = 0

                    oTable.Rows.Add(oValues)

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            'packages-----------------------------------------------------------------------------

            s = sQuery(1)

            I = 0

            oRs = clsMarsData.GetData(s)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    nPackID = oRs(clsMarsData.GetColumnIndex("PackID", oRs)).Value

                    oValues(0) = "Package Schedule"
                    oValues(1) = oRs("packagename").Value
                    oValues(2) = oRs("owner").Value

                    I = 3

                    If Not sDataCols Is Nothing Then
                        For Each x As String In sDataCols
                            If x.Length > 0 Then
                                Select Case x.ToLower
                                    Case "nextrun"
                                        If oRs("status").Value = 1 Then
                                            Dim val As Date = IsNull(oRs(x).Value)
                                            oValues(I) = CTimeZ(val, dateConvertType.READ)
                                        Else
                                            oValues(I) = "Disabled"
                                        End If
                                    Case "repeatuntil"
                                        oValues(I) = ConTime(IsNull(oRs(x).Value))
                                    Case "databasepath", "outputformat", "lastrefreshed"
                                        oValues(I) = "Various"
                                    Case "destinationtype"
                                        oValues(I) = oUI.GetDestinationValues("destinationtype", , nPackID)
                                    Case "entrydate"
                                        oValues(I) = oUI.GetLastRun(, nPackID)
                                    Case "success"
                                        oValues(I) = oUI.GetLastResult(, nPackID)
                                    Case "starttime"
                                        Dim val As Date = ConDate(Now) & " " & ConTime(IsNull(oRs(x).Value, Now))
                                        oValues(I) = CTimeZ(val, dateConvertType.READ)
                                    Case Else
                                        oValues(I) = IsNull(oRs(x).Value)
                                End Select
                            End If
                            I += 1
                        Next
                    End If

                    I = 0

                    oTable.Rows.Add(oValues)

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            'automation --------------------------------------------------------------------------
            s = sQuery(2)

            I = 0

            oRs = clsMarsData.GetData(s)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    nAutoID = oRs(clsMarsData.GetColumnIndex("AutoID", oRs)).Value

                    oValues(0) = "Automation Schedule"
                    oValues(1) = oRs("autoname").Value
                    oValues(2) = oRs("owner").Value

                    I = 3
                    If Not sDataCols Is Nothing Then
                        For Each x As String In sDataCols
                            If x.Length > 0 Then
                                Select Case x.ToLower
                                    Case "nextrun"
                                        If oRs("status").Value = 1 Then
                                            Dim val As Date = IsNull(oRs(x).Value)
                                            oValues(I) = CTimeZ(val, dateConvertType.READ)
                                        Else
                                            oValues(I) = "Disabled"
                                        End If
                                    Case "repeatuntil"
                                        oValues(I) = ConTime(oRs(x).Value)
                                    Case "outputformat", "destinationtype", "databasepath", "lastrefreshed"
                                        oValues(I) = "N/A"
                                    Case "entrydate"
                                        oValues(I) = oUI.GetLastRun(, , nAutoID)
                                    Case "success"
                                        oValues(I) = oUI.GetLastResult(, , nAutoID)
                                    Case "starttime"
                                        Dim val As Date = ConDate(Now) & " " & ConTime(IsNull(oRs(x).Value, Now))
                                        oValues(I) = CTimeZ(val, dateConvertType.READ)
                                    Case Else
                                        oValues(I) = oRs(x).Value
                                End Select
                            End If
                            I += 1
                        Next
                    End If

                    I = 0

                    oTable.Rows.Add(oValues)

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If
            'event based-----------------------------------------------------------------------
            s = sQuery(3)

            I = 0

            oRs = clsMarsData.GetData(s)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    nEventID = oRs(0).Value

                    oValues(0) = "Event-Based Schedule"
                    oValues(1) = oRs("eventname").Value
                    oValues(2) = oRs("owner").Value

                    I = 3
                    If Not sDataCols Is Nothing Then
                        For Each x As String In sDataCols
                            If x.Length > 0 Then
                                Select Case x.ToLower
                                    Case "nextrun"
                                        If oRs("status").Value = 1 Then
                                            oValues(I) = "On-Event"
                                        Else
                                            oValues(I) = "Disabled"
                                        End If
                                    Case "description"
                                        oValues(I) = oRs(x).Value
                                    Case Else
                                        oValues(I) = "Event-Based"
                                End Select
                            End If
                            I += 1
                        Next
                    End If

                    I = 0

                    oTable.Rows.Add(oValues)

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            'event-based packages----------------------------------------------------------------------
            s = sQuery(4)

            I = 0

            oRs = clsMarsData.GetData(s)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    nPackID = oRs(clsMarsData.GetColumnIndex("EventPackID", oRs)).Value

                    oValues(0) = "Event-Based Package"
                    oValues(1) = oRs("packagename").Value
                    oValues(2) = oRs("owner").Value

                    I = 3

                    If Not sDataCols Is Nothing Then
                        For Each x As String In sDataCols
                            If x.Length > 0 Then
                                Select Case x.ToLower
                                    Case "nextrun"
                                        If oRs("status").Value = 1 Then
                                            Dim val As Date = IsNull(oRs(x).Value)
                                            oValues(I) = CTimeZ(val, dateConvertType.READ)
                                        Else
                                            oValues(I) = "Disabled"
                                        End If
                                    Case "repeatuntil"
                                        oValues(I) = ConTime(IsNull(oRs(x).Value))
                                    Case "databasepath", "outputformat", "lastrefreshed"
                                        oValues(I) = "N/A"
                                    Case "destinationtype"
                                        oValues(I) = "N/A"
                                    Case "entrydate"
                                        oValues(I) = oUI.GetLastRun(, nPackID)
                                    Case "success"
                                        oValues(I) = oUI.GetLastResult(, nPackID)
                                    Case "starttime"
                                        Dim val As Date = ConDate(Now) & " " & ConTime(IsNull(oRs(x).Value, Now))
                                        oValues(I) = CTimeZ(val, dateConvertType.READ)
                                    Case Else
                                        Try
                                            oValues(I) = IsNull(oRs(x).Value)
                                        Catch
                                            oValues(I) = "N/A"
                                        End Try
                                End Select
                            End If
                            I += 1
                        Next
                    End If

                    I = 0

                    oTable.Rows.Add(oValues)

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            oSet = New DataSet(sName)

            oSet.Tables.Add(oTable)

            myGrid.DataSource = oSet
            myGrid.DataMember = "Schedules List"
            myGrid.ReadOnly = True
            myGrid.EndInit()

            'add grouping
            Dim grp As Xceed.Grid.Group = New Xceed.Grid.Group
            grp.GroupBy = "Schedule Type"
            grp.HeaderRows.Add(New Xceed.Grid.GroupManagerRow)

            AddHandler myGrid.QueryGroupKeys, AddressOf grid_QueryGroupKeys

            myGrid.GroupTemplates.Add(grp)
            myGrid.UpdateGrouping()

            myGrid.Columns("Schedule Type").Visible = False

            'Try
            '    DataGridPrinter1 = New DataGridPrinter(myGrid, PrintDocument1, oSet.Tables(0))
            'Catch : End Try

            If ShowReport = False Then
                Return oSet
            End If



            If IsSystem = False Then
                Me.Text = "Smart Folder Report"
            Else
                Me.Text = "System Folder Report"
            End If

            tbReport.Tabs(0).Text = sName

            Me.ShowDialog()
        Catch ex As Exception

            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try

    End Function
    Public Function _ViewReport(ByVal sQuery() As String, ByVal sName As String)
        Dim sCon As String
        Dim oleCon As OleDbConnection
        Dim odbcCon As OdbcConnection

        If gConType = "DAT" Then
            sCon = "Jet OLEDB:Global Partial Bulk Ops=2;Jet OLEDB:Registry Path=;" & _
            "Jet OLEDB:Database Locking Mode=1;Jet OLEDB:Database Password=;" & _
            "Data Source='" & sAppPath & "sqlrdlive.dat" & "';" & _
            "Password=;Jet OLEDB:Engine Type=5;Jet OLEDB:Global Bulk Transactions=1;" & _
            "Provider='Microsoft.Jet.OLEDB.4.0';Jet OLEDB:System database=;Jet OLEDB:SFP=False;" & _
            "Extended Properties=;Mode=Share Deny None;Jet OLEDB:New Database Password=;" & _
            "Jet OLEDB:Create System Database=False;Jet OLEDB:Don't Copy Locale on Compact=False;" & _
            "Jet OLEDB:Compact Without Replica Repair=False;User ID=Admin;Jet OLEDB:Encrypt Database=False"

            oleCon = New OleDbConnection(sCon)
        ElseIf gConType = "ODBC" Then
            Dim sTemp As String
            Dim oUI As New clsMarsUI

            sTemp = oUI.ReadRegistry("ConString", "", True)

            Dim sUser As String
            Dim sPass As String
            Dim sDSN As String

            sUser = sTemp.Split(";")(3).Split("=")(1)
            sPass = sTemp.Split(";")(1).Split("=")(1)
            sDSN = sTemp.Split(";")(4).Split("=")(1)

            If sUser.Length > 0 Then
                sCon = "DATABASE=" & gCon.DefaultDatabase & ";PWD=" & sPass & ";DSN=" & sDSN & ";APP=Microsoft� Visual Studio .NET;AnsiNPW=No;AutoTranslate=No;QuotedId=No;WSID=" & Environment.MachineName & ";UID=" & sUser & ";Trusted_Connection=No"
            Else
                sCon = "DATABASE=" & gCon.DefaultDatabase & ";PWD=" & sPass & ";DSN=" & sDSN & ";APP=Microsoft� Visual Studio .NET;AnsiNPW=No;AutoTranslate=No;QuotedId=No;WSID=" & Environment.MachineName & ";UID=" & sUser & ";Trusted_Connection=Yes"
            End If

            odbcCon = New OdbcConnection(sCon)
        End If

        Try

            Dim SQL As String
            Dim I As Integer = 0
            Dim sDataCols() As String
            Dim sCaptions() As String

            Dim oRs As ADODB.Recordset
            Dim oData As New clsMarsData
            Dim oUI As New clsMarsUI

            SQL = "SELECT * FROM UserColumns WHERE Owner ='" & gUser & "' ORDER BY ColumnID"

            oRs = clsMarsData.GetData(SQL)

            'If oRs.EOF = True Then Return Nothing


            ReDim sDataCols(0)
            ReDim sCaptions(0)

            Do While oRs.EOF = False

                ReDim Preserve sDataCols(I)
                ReDim Preserve sCaptions(I)

                sDataCols(I) = oRs("datacolumn").Value
                sCaptions(I) = oRs("caption").Value

                I += 1
                oRs.MoveNext()
            Loop



            oRs.Close()

            I = 0

            SQL = ""

            For Each s As String In sQuery
                If s.Length > 0 Then
                    Dim sFirst As String
                    Dim sLast As String
                    Dim nSplit As Integer

                    nSplit = s.ToLower.IndexOf("from")

                    sFirst = s.Substring(0, nSplit)

                    sLast = s.Replace(sFirst, String.Empty)

                    sFirst = String.Empty

                    Select Case I
                        Case 0 'single schedules
                            Try
                                For Each x As String In sDataCols
                                    Select Case x.ToLower
                                        Case "destinationtype", "outputformat", "entrydate", "success"
                                            Application.DoEvents()
                                        Case "description", "keyword"
                                            If gConType = "DAT" Then
                                                sFirst &= x & ","
                                            Else
                                                sFirst &= "CAST(" & x & " AS VARCHAR(55)),"
                                            End If
                                        Case Else
                                            sFirst &= x & ","
                                    End Select
                                Next
                            Catch
                                sFirst = String.Empty
                            End Try

                            If sFirst.Length > 0 Then
                                If gConType = "DAT" Then
                                    sFirst = "SELECT 'Single Schedule' As [Schedule Type],ReportTitle," & sFirst & "'" & gUser & "' As Owner "
                                Else
                                    sFirst = "SELECT 'Single Schedule' As [Schedule Type],CAST(ReportTitle AS VARCHAR(255))," & sFirst & "'" & gUser & "' As Owner "
                                End If
                            Else
                                If gConType = "DAT" Then
                                    sFirst = "SELECT 'Single Schedule' As [Schedule Type],ReportTitle," & "'" & gUser & "' As Owner "
                                Else
                                    sFirst = "SELECT 'Single Schedule' As [Schedule Type],CAST(ReportTitle AS VARCHAR(255))," & "'" & gUser & "' As Owner "
                                End If
                            End If

                        Case 1 'package schedules
                            Try
                                For Each x As String In sDataCols
                                    Select Case x.ToLower
                                        Case "destinationtype", "outputformat", "entrydate", "success"
                                            Application.DoEvents()
                                        Case "databasepath", "lastrefreshed"
                                            sFirst &= "'Various',"
                                        Case "description", "keyword"
                                            If gConType = "DAT" Then
                                                sFirst &= x & ","
                                            Else
                                                sFirst &= "CAST(" & x & " AS VARCHAR(55)),"
                                            End If
                                        Case Else
                                            sFirst &= x & ","
                                    End Select
                                Next
                            Catch
                                sFirst = String.Empty
                            End Try

                            If sFirst.Length > 0 Then
                                If gConType = "DAT" Then
                                    sFirst = "SELECT 'Package Schedule',PackageName," & sFirst & "'" & gUser & "' "
                                Else
                                    sFirst = "SELECT 'Package Schedule',CAST(PackageName AS VARCHAR(255))," & sFirst & "'" & gUser & "' "
                                End If
                            Else
                                If gConType = "DAT" Then
                                    sFirst = "SELECT 'Package Schedule',PackageName," & "'" & gUser & "' "
                                Else
                                    sFirst = "SELECT 'Package Schedule',CAST(PackageName AS VARCHAR(255))," & "'" & gUser & "' "
                                End If
                            End If

                        Case 2 'automation schedules
                            Try
                                For Each x As String In sDataCols
                                    Select Case x.ToLower
                                        Case "destinationtype", "outputformat", "entrydate", "success"
                                            Application.DoEvents()
                                        Case "databasepath", "lastrefreshed"
                                            sFirst &= "'N/A',"
                                        Case "description", "keyword"
                                            If gConType = "DAT" Then
                                                sFirst &= x & ","
                                            Else
                                                sFirst &= "CAST(" & x & " AS VARCHAR(55)),"
                                            End If
                                        Case Else
                                            sFirst &= x & ","
                                    End Select
                                Next
                            Catch
                                sFirst = String.Empty
                            End Try

                            If sFirst.Length > 0 Then
                                If gConType = "DAT" Then
                                    sFirst = "SELECT 'Automation Schedule',AutoName," & sFirst & "'" & gUser & "' "
                                Else
                                    sFirst = "SELECT 'Automation Schedule',CAST(AutoName AS VARCHAR(255))," & sFirst & "'" & gUser & "' "
                                End If
                            Else
                                If gConType = "DAT" Then
                                    sFirst = "SELECT 'Automation Schedule',AutoName," & "'" & gUser & "' "
                                Else
                                    sFirst = "SELECT 'Automation Schedule',CAST(AutoName AS VARCHAR(255))," & "'" & gUser & "' "
                                End If
                            End If

                        Case 3 'event based schedules
                            Try
                                For Each x As String In sDataCols
                                    Select Case x.ToLower
                                        Case "destinationtype", "outputformat", "entrydate", "success"
                                            Application.DoEvents()
                                        Case "databasepath", "frequency", "outputformat", "destinationtype", _
                                       "repeatinterval", "repeatuntil", "lastrefreshed"
                                            sFirst &= "'N/A',"
                                        Case "startdate", "enddate", "nextrun", "repeatuntil", "starttime"
                                            sFirst &= "'1900-01-01',"
                                        Case "description", "keyword"
                                            If gConType = "DAT" Then
                                                sFirst &= x & ","
                                            Else
                                                sFirst &= "CAST(" & x & " AS VARCHAR(55)),"
                                            End If
                                        Case Else
                                            sFirst &= x & ","
                                    End Select
                                Next
                            Catch
                                sFirst = String.Empty
                            End Try

                            If sFirst.Length > 0 Then
                                If gConType = "DAT" Then
                                    sFirst = "SELECT 'Event-Based Schedule',EventName," & sFirst & "'" & gUser & "' "
                                Else
                                    sFirst = "SELECT 'Event-Based Schedule',CAST(EventName AS VARCHAR(255))," & sFirst & "'" & gUser & "' "
                                End If
                            Else
                                If gConType = "DAT" Then
                                    sFirst = "SELECT 'Event-Based Schedule',EventName," & "'" & gUser & "' "
                                Else
                                    sFirst = "SELECT 'Event-Based Schedule',CAST(EventName AS VARCHAR(255))," & "'" & gUser & "' "
                                End If
                            End If
                    End Select

                    s = sFirst & sLast

                    SQL &= s & Environment.NewLine & " UNION " & Environment.NewLine

                    I += 1
                End If
            Next

            SQL = SQL.Substring(0, SQL.Length - 8)

            '''console.writeline(SQL)
            Dim oleAdapt As OleDbDataAdapter
            Dim odbcAdapt As OdbcDataAdapter
            Dim oSet As New DataSet

            ''console.writeline(SQL)

            If gConType = "DAT" Then
                oleCon.Open()
                oleAdapt = New OleDbDataAdapter(SQL, oleCon)
                oleAdapt.Fill(oSet)
            Else
                odbcCon.Open()
                odbcAdapt = New OdbcDataAdapter(SQL, odbcCon)
                odbcAdapt.Fill(oSet)
            End If

            oSet.Tables(0).TableName = sName & " Contents"

            I = 0

            oSet.Tables(0).Columns(0).ColumnName = "Schedule Type"
            oSet.Tables(0).Columns(1).ColumnName = "Name"

            For I = 0 To oSet.Tables(0).Columns.Count - 1
                Dim s As String

                s = oSet.Tables(0).Columns(I).ColumnName

                Select Case s.ToLower
                    Case "description"
                        oSet.Tables(0).Columns(I).ColumnName = "Schedule Description"
                    Case "starttime"
                        oSet.Tables(0).Columns(I).ColumnName = "Execution Time"
                    Case "databasepath"
                        oSet.Tables(0).Columns(I).ColumnName = "Report Path"
                    Case "repeatinterval"
                        oSet.Tables(0).Columns(I).ColumnName = "Repeat Interval"
                    Case "repeatuntil"
                        oSet.Tables(0).Columns(I).ColumnName = "Repeat Until"
                    Case "lastrefreshed"
                        oSet.Tables(0).Columns(I).ColumnName = "Last Refreshed"
                End Select
            Next


            oSet.DataSetName = sName

            myGrid.BeginInit()
            myGrid.DataSource = oSet
            myGrid.ReadOnly = True
            myGrid.EndInit()

            'myGrid.Expand(-1)

            'myGrid.ColumnHeadersVisible = True

            'oSet.WriteXml("c:\test.xml")

            'myGrid.ParentRowsVisible = False

            'DataGridPrinter1 = New DataGridPrinter(myGrid, PrintDocument1, oSet.Tables(0))



            Me.Text = "Smart Folder Report [" & sName & "]"

            tbReport.Tabs(0).Text = sName

            Me.Show()
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Function

    Private Sub frmSmartReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)


    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        DataGridPrinter1.PageNumber = 1
        DataGridPrinter1.RowCount = 0

        PrintDialog1.Document = PrintDocument1

        Dim oRes As DialogResult = PrintDialog1.ShowDialog()

        If oRes = DialogResult.OK Then
            PrintDocument1.Print()
        End If
    End Sub
    Private Sub DrawTopLabel(ByVal g As Graphics)
        Dim TopMargin As Integer = PrintDocument1.DefaultPageSettings.Margins.Top

        'g.FillRectangle(New SolidBrush(Label1.BackColor), Label1.Location.X, Label1.Location.Y + TopMargin, Label1.Size.Width, Label1.Size.Height)
        'g.DrawString(Label1.Text, Label1.Font, New SolidBrush(Label1.ForeColor), Label1.Location.X + 50, Label1.Location.Y + TopMargin, New StringFormat)
    End Sub
    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim g As Graphics = e.Graphics

        DrawTopLabel(g)

        Dim more As Boolean = DataGridPrinter1.DrawDataGrid(g)

        If more = True Then
            e.HasMorePages = True
            DataGridPrinter1.PageNumber += 1
        End If

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        DataGridPrinter1.PageNumber = 1
        DataGridPrinter1.RowCount = 0

        Me.PrintPreviewDialog1.ShowDialog()

    End Sub
    Private Function LongestField(ByVal ds As DataSet, ByVal TableName As String, ByVal ColumnName As String) As Integer

        Dim maxlength As Integer = 0
        Dim g As Graphics = myGrid.CreateGraphics()

        ' Take width of one blank space and add to the new width of the Column.
        Dim offset As Integer = Convert.ToInt32(Math.Ceiling(g.MeasureString(" ", myGrid.Font).Width))

        Dim i As Integer = 0
        Dim intaux As Integer
        Dim straux As String
        Dim tot As Integer = ds.Tables(TableName).Rows.Count

        For i = 0 To (tot - 1)
            straux = ds.Tables(TableName).Rows(i)(ColumnName).ToString()
            ' Get the width of Current Field String according to the Font.
            intaux = Convert.ToInt32(Math.Ceiling(g.MeasureString(straux, myGrid.Font).Width))
            If (intaux > maxlength) Then
                maxlength = intaux
            End If
        Next

        Return maxlength + offset

    End Function

    Private Sub cmdExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim sfd As New SaveFileDialog

        Try
            With sfd
                .CheckFileExists = False
                .DefaultExt = "xml"
                .Filter = "XML File|*.xml"
                .Title = "Export Smart Folder Report..."
                .ShowDialog()

                Dim sXML As String = .FileName

                If sXML.Length = 0 Then
                    Return
                Else
                    Dim oSet As DataSet
                    oSet = myGrid.DataSource

                    oSet.WriteXml(sXML)

                    MessageBox.Show("Report exported successfully!", Application.ProductName, _
                    MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End With
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub dtMan_ItemClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtMan.ItemClick
        Dim oItem As DevComponents.DotNetBar.BaseItem

        oItem = CType(sender, BaseItem)

        Select Case oItem.Name.ToLower
            Case "btnpreview"
                Dim report As New Report(myGrid)
                report.PrintPreview()

            Case "btnprint"
                Dim report As New Report(myGrid)

                report.Print(True)
            Case "btnreport"
                Dim reportForm As New GenerateReportForm(myGrid)

                reportForm.ShowDialog()
            Case "btnsave"
                Dim sfd As New SaveFileDialog

                Try
                    With sfd
                        .CheckFileExists = False
                        .DefaultExt = "xml"
                        .Filter = "XML File|*.xml"
                        .Title = "Export Smart Folder Report..."
                        .ShowDialog()

                        Dim sXML As String = .FileName

                        If sXML.Length = 0 Then
                            Return
                        Else
                            Dim oSet As DataSet
                            oSet = myGrid.DataSource

                            oSet.WriteXml(sXML)

                            MessageBox.Show("Report exported successfully!", Application.ProductName, _
                            MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If
                    End With
                Catch ex As Exception
                    _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
                End Try
            Case "btnclose"
                Close()
        End Select
    End Sub

    Public Function _GenerateReport(ByVal nID As Integer) As Boolean
        Dim Ok As Boolean

        Try
            Dim oUI As New clsMarsUI
            Dim sName As String = ""
            Dim oRs As ADODB.Recordset
            Dim SmartSQL() As String
            Dim sOwner As String = ""

            oRs = clsMarsData.GetData("SELECT SmartName,Owner FROM SmartFolders WHERE SmartID=" & nID)

            If Not oRs Is Nothing Then
                If oRs.EOF = False Then
                    sName = oRs(0).Value
                    sOwner = oRs(1).Value
                End If

                oRs.Close()
            End If

            SmartSQL = oUI.SmartQueryBuilder(nID)

            Dim oSet As DataSet

            oSet = _RenderReport(SmartSQL, sName, False, sOwner)

            If oSet Is Nothing Then
                Throw New Exception("Error creating dataset from smart querries")
            End If

            Dim sPath As String = clsMarsReport.m_OutputFolder

            sName = sName.Replace(":", "").Replace("\", "").Replace("/", "").Replace("|", "").Replace("?", ""). _
                        Replace("""", "").Replace("<", "").Replace(">", "").Replace("*", "")

            'sPath = sPath & sName & ".xml"

            'If IO.File.Exists(sPath) Then
            '    IO.File.Delete(sPath)
            'End If
            ''generate XML File
            'oSet.WriteXml(sPath)

            'process destination

            Dim SQL As String = "SELECT * FROM DestinationAttr WHERE SmartID =" & nID

            oRs = clsMarsData.GetData(SQL)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    Dim nDestinationID As Integer
                    Dim sFormat As String
                    Dim oParse As New clsMarsParser
                    Dim sFileName As String
                    Dim sDateStamp As String
                    Dim sExt As String

                    nDestinationID = oRs("destinationid").Value

                    sFormat = IsNull(oRs("outputformat").Value, "")

                    If IsNull(oRs("CustomName").Value) <> "" Then

                        sFileName = oParse.ParseString(oRs("CustomName").Value)

                        sFileName = sFileName.Replace(":", "").Replace("\", "").Replace("/", "").Replace("|", "").Replace("?", ""). _
                        Replace("""", "").Replace("<", "").Replace(">", "").Replace("*", "")
                    Else
                        sFileName = sName

                        sFileName = sFileName.Replace(":", "").Replace("\", "").Replace("/", "").Replace("|", "").Replace("?", ""). _
                        Replace("""", "").Replace("<", "").Replace(">", "").Replace("*", "")
                    End If

                    If Convert.ToBoolean(oRs("AppendDateTime").Value) = True Then
                        sDateStamp = IsNull(oRs("DateTimeFormat").Value)

                        sDateStamp = Date.Now.ToString(sDateStamp)
                    Else
                        sDateStamp = ""
                    End If

                    If IsNull(oRs("customext").Value, "") <> "" Then
                        sExt = oRs("customext").Value
                    Else
                        Select Case sFormat.ToLower.Split("(")(0).Trim
                            Case "acrobat format"
                                sExt = "pdf"
                            Case "html"
                                sExt = "html"
                            Case "jpeg"
                                sExt = "jpg"
                            Case "tiff"
                                sExt = "tif"
                            Case "ms excel"
                                sExt = "xls"
                            Case "xml (*.xml)"
                                sExt = "xml"
                        End Select
                    End If

                    If sExt Is Nothing Then sExt = ""

                    If sExt.StartsWith(".") = False Then sExt = "." & sExt

                    Dim rpt As Report = New Report(myGrid)
                    Dim outputFile As String = clsMarsReport.m_OutputFolder & sName & sExt

                    Select Case sFormat.ToLower
                        Case "acrobat format (*.pdf)"
                            rpt.ReportStyleSheet = Reporting.ReportStyleSheet.SteelBlue
                            rpt.Export(outputFile, ExportFormat.Pdf, True, RunEditor)

                            Dim oPdf As New clsMarsPDF
                            Dim oRs1 As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM ReportOptions WHERE DestinationID =" & nDestinationID)

                            Dim PDFsecurity As Boolean = False

                            If oRs1.EOF = False Then
                                Dim oPerm As New clsMarsPDF
                                Dim InfoTitle, InfoAuthor, InfoSubject, InfoKeywords, InfoProducer As String
                                Dim InfoCreated As Date
                                Dim CanPrint, CanCopy, CanEdit, CanNotes, CanFill, CanAccess, CanAssemble, _
                                CanPrintFull As Integer
                                Dim OwnerPassword, UserPassword, PDFWatermark As String

                                Dim pdfExpires As Boolean
                                Dim pdfExpiryDate As Date
                                Try
                                    pdfExpires = Convert.ToBoolean(IsNull(oRs("expirepdf").Value, 0))
                                    pdfExpiryDate = IsNull(oRs("pdfexpirydate").Value, Date.Now)
                                Catch : End Try

                                Try
                                    If oRs1("pdfsecurity").Value = 1 Or oRs1("pdfsecurity").Value = -1 Then
                                        PDFsecurity = True
                                    End If
                                Catch
                                    PDFsecurity = False
                                End Try

                                InfoTitle = IsNull(oRs1("infotitle").Value)
                                InfoAuthor = IsNull(oRs1("infoauthor").Value)
                                InfoSubject = IsNull(oRs1("infosubject").Value)
                                InfoKeywords = IsNull(oRs1("infokeywords").Value)
                                InfoProducer = IsNull(oRs1("infoproducer").Value)
                                InfoCreated = IsNull(oRs1("infocreated").Value, Now)

                                CanPrint = IsNull(oRs1("canprint").Value, 1)
                                CanCopy = IsNull(oRs1("cancopy").Value, 1)
                                CanEdit = IsNull(oRs1("canedit").Value, 1)
                                CanNotes = IsNull(oRs1("cannotes").Value, 1)
                                CanFill = IsNull(oRs1("canfill").Value, 1)
                                CanAccess = IsNull(oRs1("canaccess").Value, 1)
                                CanAssemble = IsNull(oRs1("canassemble").Value, 1)
                                CanPrintFull = IsNull(oRs1("canprintfull").Value, 1)

                                OwnerPassword = IsNull(oRs1("pdfpassword").Value)
                                UserPassword = IsNull(oRs1("userpassword").Value)
                                PDFWatermark = IsNull(oRs1("pdfwatermark").Value)

                                Dim oItem As New clsMarsReport

                                outputFile = oItem._PostProcessPDF(outputFile, InfoTitle, _
                                InfoAuthor, InfoSubject, InfoKeywords, InfoProducer, _
                                InfoCreated, False, 0, Nothing, PDFsecurity, False, _
                                CanPrint, CanCopy, CanEdit, _
                                CanNotes, CanFill, CanAccess, CanAssemble, _
                                CanPrintFull, OwnerPassword, UserPassword, _
                                PDFWatermark, nDestinationID, pdfExpires, pdfExpiryDate)
                            End If
                        Case "html (*.html)"
                            rpt.ReportStyleSheet = Reporting.ReportStyleSheet.SteelBlue
                            rpt.Export(outputFile, ExportFormat.Html, True, RunEditor)
                        Case "jpeg (*.jpg)"
                            rpt.ReportStyleSheet = Reporting.ReportStyleSheet.SteelBlue
                            rpt.Export(outputFile, ExportFormat.Jpeg, True, RunEditor)
                        Case "tiff (*.tif)"
                            rpt.ReportStyleSheet = Reporting.ReportStyleSheet.SteelBlue
                            rpt.Export(outputFile, ExportFormat.Tiff, True, RunEditor)
                        Case "ms excel (*.xls)"
                            If IO.File.Exists(outputFile) Then
                                IO.File.Delete(outputFile)
                            End If

                            oSet.WriteXml(outputFile)
                        Case "xml (*.xml)"
                            If IO.File.Exists(outputFile) Then
                                IO.File.Delete(outputFile)
                            End If

                            oSet.WriteXml(outputFile)
                    End Select
                    
                    sFileName = sFileName & sDateStamp & sExt

                    'rename the file
                    If outputFile <> GetDirectory(outputFile) & sFileName Then
                        If IO.File.Exists(GetDirectory(outputFile) & sFileName) = True Then
                            IO.File.Delete(GetDirectory(outputFile) & sFileName)
                        End If

                        IO.File.Move(outputFile, GetDirectory(outputFile) & sFileName)
                    End If

                    Dim sSendTo As String = ""
                    Dim sSubject As String
                    Dim sCc As String
                    Dim sBcc As String
                    Dim sExtras As String
                    Dim sMsg As String
                    Dim Embed As Boolean
                    Dim MailFormat As String
                    Dim ScheduleType As String = "Single"
                    Dim SMTPServer As String
                    Dim sExport As String = sPath
                    Dim oReport As clsMarsReport = New clsMarsReport
                    Dim oPGP As New clsMarsPGP
                    Dim oPGPResult()
                    Dim sTempPath As String = oUI.ReadRegistry("TempFolder", sAppPath & "Output\")


                    sExport = GetDirectory(sPath) & sFileName

                    oPGPResult = oPGP.EncryptFile(sExport, nDestinationID)

                    If oPGPResult(0) = False Then
                        Return False
                    End If

                    sExport &= oPGPResult(1)

                    Try
                        If oRs("compress").Value = 1 And sExport.Length > 0 Then
                            Dim Encrypt As Boolean
                            Dim ZipCode As String = ""

                            oUI.BusyProgress(60, "Zipping report...")

                            Try
                                Encrypt = oRs("encryptzip").Value
                                ZipCode = IsNull(oRs("encryptzipcode").Value, "")
                            Catch : Encrypt = False : End Try

                            sExport = oReport.ZipFiles(ScheduleType, sTempPath, sExport, Encrypt, ZipCode)

                        End If
                    Catch : End Try

                    Dim oMsg As New clsMarsMessaging


                    Select Case oRs("DestinationType").Value.Tolower

                        Case "email"
                            sSendTo = clsMarsMessaging.ResolveEmailAddress(oRs("sendto").Value)
                            sCc = clsMarsMessaging.ResolveEmailAddress(oRs("cc").Value)
                            sBcc = clsMarsMessaging.ResolveEmailAddress(oRs("bcc").Value)
                            sExtras = IsNull(oRs("extras").Value, "")
                            sSubject = oParse.ParseString(oRs("subject").Value)

                            MailFormat = oRs("mailformat").Value
                            SMTPServer = IsNull(oRs("smtpserver").Value, "Default")

                            sMsg = oParse.ParseString(oRs("message").Value)

                            oUI.BusyProgress(75, "Emailing report...")

                            Try
                                Embed = Convert.ToBoolean(oRs("embed").Value)
                            Catch ex As Exception
                                Embed = False
                            End Try

                            If MailType = MarsGlobal.gMailType.MAPI Then
                                Ok = clsMarsMessaging.SendMAPI(sSendTo, _
                                sSubject, sMsg, ScheduleType, sExport, 1, sExtras, _
                                    sCc, sBcc, Embed, "", sFileName)
                            ElseIf MailType = MarsGlobal.gMailType.SMTP Or MailType = MarsGlobal.gMailType.SQLRDMAIL Then
                                Dim objSender As clsMarsMessaging = New clsMarsMessaging

                                Ok = objSender.SendSMTP(sSendTo, sSubject, _
                               sMsg, ScheduleType, sExport, 1, sExtras, sCc, _
                               sBcc, sFileName, Embed, "", , , _
                               MailFormat, SMTPServer, )
                            ElseIf MailType = gMailType.GROUPWISE Then
                                Ok = clsMarsMessaging.SendGROUPWISE(sSendTo, sCc, sBcc, sSubject, sMsg, sExport, ScheduleType, _
                                sExtras, True, Embed, , , , , MailFormat)
                            Else
                                Throw New Exception("No messaging configuration found. Please set up your messaging options in 'Options'")
                            End If

                        Case "disk"


                            Dim UseDUN As Boolean
                            Dim sDUN As String
                            Dim oNet As clsNetworking
                            'if DUN is used...
                            Try
                                UseDUN = Convert.ToBoolean(oRs("usedun").Value)
                                sDUN = IsNull(oRs("dunname").Value)
                            Catch ex As Exception
                                UseDUN = False
                            End Try

                            If UseDUN = True Then
                                oNet = New clsNetworking

                                If oNet._DialConnection(sDUN, "Smart Folder Report Error: " & sName & ": ") = False Then
                                    Return False
                                End If
                            End If

                            oUI.BusyProgress(75, "Copying reports...")

                            Dim sFile As String = ExtractFileName(sExport)

                            sPath = oParse.ParseString(oRs("outputpath").Value)

                            Dim oSys As New clsSystemTools

                            For Each s As String In sPath.Split("|")
                                If s.Length > 0 Then

                                    If s.EndsWith("\") = False And s.Length > 0 Then s &= "\"

                                    s = _CreateUNC(s)

                                    Ok = oParse.ParseDirectory(s)

                                    Try
                                        System.IO.File.Copy(sExport, s & sFile, True)
                                    Catch ex As Exception
                                        If Err.Number = 57 Then
                                            For Each o As Process In Process.GetProcessesByName("excel")
                                                o.Kill()
                                            Next

                                            System.IO.File.Copy(sExport, s & sFile, True)
                                        End If
                                    End Try

                                    oSys._HouseKeeping(nDestinationID, s, sFileName)
                                End If
                            Next

                            Try
                                If UseDUN = True Then
                                    oNet._Disconnect()
                                End If
                            Catch : End Try
                        Case "ftp"
                            Dim oFtp As New clsMarsTask
                            Dim sFtp As String = ""
                            Dim FTPServer As String
                            Dim FTPUser As String
                            Dim FTPPassword As String
                            Dim FTPPath As String
                            Dim FTPType As String
                            Dim ftpCount As Integer = 0

                            FTPServer = oParse.ParseString(IsNull(oRs("ftpserver").Value))
                            FTPUser = oParse.ParseString(oRs("ftpusername").Value)
                            FTPPassword = oParse.ParseString(oRs("ftppassword").Value)
                            FTPPath = oParse.ParseString(oRs("ftppath").Value)
                            FTPType = IsNull(oRs("ftptype").Value, "FTP")

                            oUI.BusyProgress(75, "Uploading report...")

                            ftpCount = FTPServer.Split("|").GetUpperBound(0)

                            If ftpCount > 0 Then
                                ftpCount -= 1
                            Else
                                ftpCount = 0
                            End If

                            For I As Integer = 0 To ftpCount
                                Dim l_FTPServer As String = FTPServer.Split("|")(I)
                                Dim l_FTPUser As String = FTPUser.Split("|")(I)
                                Dim l_FTPPassword As String = _DecryptDBValue(FTPPassword.Split("|")(I))
                                Dim l_FTPPath As String = FTPPath.Split("|")(I)
                                Dim l_FTPType As String = FTPType.Split("|")(I)


                                oFtp.FTPUpload(l_FTPServer, 21, l_FTPUser, _
                                l_FTPPassword, l_FTPPath, _
                                sExport, l_FTPType)
                            Next

                            Ok = True
                        Case "fax"

                            Ok = oMsg.SendFax(oRs("sendto").Value, oRs("subject").Value, "Single", _
                            oRs("cc").Value, oRs("bcc").Value, oRs("message").Value, sExport, False)
                        Case "sms"
                            Ok = clsMarsMessaging.SendSMS(oRs("sendto").Value, oRs("message").Value, sExport)

                    End Select

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            Return Ok
        Catch ex As Exception
            _ErrorHandle("Smart Folder Report Error: " & ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.stacktrace))

            Return False
        End Try
    End Function
End Class
