Imports DevComponents.DotNetBar
Public Class frmRemoteAdmin
    Inherits frmTaskMaster
    Dim oData As New clsMarsData
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Dim oUI As New clsMarsUI

    Dim imgList As ImageList
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lsvRemotes As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents cmdAdd As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdDelete As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdConnect As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents mnuRemote As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRemoteAdmin))
        Me.lsvRemotes = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.mnuRemote = New System.Windows.Forms.ContextMenu()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.cmdAdd = New DevComponents.DotNetBar.ButtonX()
        Me.cmdDelete = New DevComponents.DotNetBar.ButtonX()
        Me.cmdConnect = New DevComponents.DotNetBar.ButtonX()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lsvRemotes
        '
        '
        '
        '
        Me.lsvRemotes.Border.Class = "ListViewBorder"
        Me.lsvRemotes.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvRemotes.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3})
        Me.lsvRemotes.ContextMenu = Me.mnuRemote
        Me.lsvRemotes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvRemotes.FullRowSelect = True
        Me.lsvRemotes.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvRemotes.Location = New System.Drawing.Point(0, 0)
        Me.lsvRemotes.Name = "lsvRemotes"
        Me.lsvRemotes.Size = New System.Drawing.Size(452, 241)
        Me.lsvRemotes.TabIndex = 1
        Me.lsvRemotes.UseCompatibleStateImageBehavior = False
        Me.lsvRemotes.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "PC Name"
        Me.ColumnHeader1.Width = 106
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "System Path"
        Me.ColumnHeader2.Width = 200
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Auto Connect"
        Me.ColumnHeader3.Width = 97
        '
        'mnuRemote
        '
        Me.mnuRemote.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Auto Connect on Startup"
        '
        'cmdAdd
        '
        Me.cmdAdd.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAdd.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAdd.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAdd.Dock = System.Windows.Forms.DockStyle.Right
        Me.cmdAdd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAdd.Location = New System.Drawing.Point(289, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(75, 23)
        Me.cmdAdd.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdAdd.TabIndex = 8
        Me.cmdAdd.Text = "&Add"
        '
        'cmdDelete
        '
        Me.cmdDelete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdDelete.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDelete.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdDelete.Dock = System.Windows.Forms.DockStyle.Right
        Me.cmdDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDelete.Location = New System.Drawing.Point(370, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(75, 23)
        Me.cmdDelete.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdDelete.TabIndex = 7
        Me.cmdDelete.Text = "&Remove"
        '
        'cmdConnect
        '
        Me.cmdConnect.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdConnect.BackColor = System.Drawing.SystemColors.Control
        Me.cmdConnect.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdConnect.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdConnect.Location = New System.Drawing.Point(3, 3)
        Me.cmdConnect.Name = "cmdConnect"
        Me.cmdConnect.Size = New System.Drawing.Size(75, 22)
        Me.cmdConnect.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdConnect.TabIndex = 7
        Me.cmdConnect.Text = "&Connect"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdConnect)
        Me.FlowLayoutPanel1.Controls.Add(Me.LabelX1)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdAdd)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdDelete)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 241)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(452, 33)
        Me.FlowLayoutPanel1.TabIndex = 10
        '
        'LabelX1
        '
        '
        '
        '
        Me.LabelX1.BackgroundStyle.Class = ""
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(84, 3)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(199, 23)
        Me.LabelX1.TabIndex = 9
        '
        'frmRemoteAdmin
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(452, 274)
        Me.ControlBox = True
        Me.Controls.Add(Me.lsvRemotes)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmRemoteAdmin"
        Me.Text = "Select Remote System"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmRemoteAdmin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        imgList = New ImageList

        imgList.Images.Add(Image.FromFile(getAssetLocation("workstation.png")))

        lsvRemotes.LargeImageList = imgList
        lsvRemotes.SmallImageList = imgList

        LoadAll()
    End Sub

    Private Sub LoadAll()
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim oItem As ListViewItem

        SQL = "SELECT * FROM RemoteSysAttr"

        oRs = clsMarsData.GetData(SQL)

        lsvRemotes.Items.Clear()

        Try
            Do While oRs.EOF = False
                oItem = New ListViewItem

                oItem.ImageIndex = 0

                oItem.Text = oRs("pcname").Value
                oItem.SubItems.Add(oRs("livepath").Value)

                If IsNull(oRs("autoconnect").Value) = "1" Then
                    oItem.SubItems.Add("1")
                Else
                    oItem.SubItems.Add("0")
                End If

                oItem.Tag = oRs("remoteid").Value

                lsvRemotes.Items.Add(oItem)

                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch : End Try
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim nID As Integer

        If lsvRemotes.SelectedItems.Count = 0 Then Return

        If MessageBox.Show("Delete the selected remote system?", Application.ProductName, _
        MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then

            nID = lsvRemotes.SelectedItems(0).Tag

            Dim SQL As String = "DELETE FROM RemoteSysAttr WHERE RemoteID =" & nID

            clsMarsData.WriteData(SQL)

            lsvRemotes.SelectedItems(0).Remove()

        End If

    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        Dim fbd As New FolderBrowserDialog

        fbd.Description = "Please select the SQL-RD folder on the remote system"

        fbd.ShowNewFolderButton = False
        fbd.ShowDialog()

        Dim Temp As String
        Dim SQL As String
        Dim rs As New ADODB.Recordset
        Dim I As Integer
        Dim NewItem As Integer
        Dim bExists As Boolean
        Dim sRemote As String

        Temp = fbd.SelectedPath

        If Temp.Length = 0 Then Exit Sub

        If Temp.StartsWith("\\") = False Then Temp = _CreateUNC(Temp)

        If Temp.StartsWith("\\") = False Then
            MessageBox.Show("Remote Administration requires the full UNC path ofthe remote system " & vbCrLf & _
            "e.g \\MyServer\C\Program Files\ChristianSteven\SQL-RD", Application.ProductName, MessageBoxButtons.OK, _
            MessageBoxIcon.Exclamation)
            Return
        End If

        sRemote = ConnectRemote(Temp)

        If sRemote.Length = 0 Then Return


        Dim sCols As String
        Dim sVals As String
        Dim nID As Integer = clsMarsData.CreateDataID("remotesysattr", "remoteid")

        sCols = "RemoteID,PCName,LivePath"

        sVals = nID & "," & _
        "'" & SQLPrepare(sRemote) & "'," & _
        "'" & SQLPrepare(Temp) & "'"

        SQL = "INSERT INTO RemoteSysAttr (" & sCols & ") VALUES (" & sVals & ")"

        If clsMarsData.WriteData(SQL) = True Then
            Dim oItem As New ListViewItem

            oItem.Text = sRemote

            oItem.SubItems.Add(Temp)

            oItem.Tag = nID

            lsvRemotes.Items.Add(oItem)
        End If

    End Sub
    Public Function ConnectRemote(ByVal sRemotePath As String, Optional ByVal checkNTUser As Boolean = False, Optional ByVal remoteconType As String = "DAT") As String
        Dim rs As New ADODB.Recordset
        Dim SQL As String
        Dim Temp As String
        Dim MachineName As String
        Dim cn As New ADODB.Connection
        Dim sHelp As String
        Dim sRemoteDSN As String
        Dim oRemoteReg As New RemoteRegistry.clsRegistry
        Dim remoteConfig As String

        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.sa3_RemoteAdmin) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, cmdConnect, "Remote Administration")
            Exit Function
        End If

        Try

            MachineName = GetDelimitedWord(sRemotePath, 3, "\")

            remoteConfig = "\\" & MachineName & "\sqlrdconfigshare\sqlrdlive.config"

            If IO.File.Exists(remoteConfig) = False Then
                Throw New Exception("Could not connect to the remote server as the  share named 'SQLRDCONFIGSHARE' does not exist. Please upgrade server to the latest version and try again.")
            End If

            If remoteconType = "DAT" Then
                sRemoteDSN = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sRemotePath & "\sqlrdlive.dat;Persist Security Info=False"
            Else
                sRemoteDSN = clsMarsUI.MainUI.ReadRegistry("ConString", "", True, remoteConfig, True)

                If sRemoteDSN = "" Then sRemoteDSN = "Provider=SQLOLEDB.1;Password=cssAdmin12345$;Persist Security Info=True;User ID=sa;Initial Catalog=SQL-RD;Data Source=" & MachineName & "\SQLRD"
            End If

            oUI.BusyProgress(25, "Connecting to remote system...")

            cn.Mode = ADODB.ConnectModeEnum.adModeShareDenyNone

            cn.Open(sRemoteDSN)

            oUI.BusyProgress(75, "Validating system catalog...")

            SQL = "SELECT TOP 1 * FROM ReportAttr"

            rs.Open(SQL, cn, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

            rs.Close()

            rs = Nothing


            oUI.BusyProgress(100, "", True)

            rs = Nothing
            cn.Close()
            cn = Nothing

            Return MachineName
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            oUI.BusyProgress(, , True)
            Return String.Empty
        End Try
    End Function

    Private Sub cmdConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdConnect.Click
        If lsvRemotes.SelectedItems.Count = 0 Then Return

        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim nID As Integer = lsvRemotes.SelectedItems(0).Tag
        Dim PCName As String
        Dim sPath As String
        Dim sUser As String
        Dim sPassword As String

        SQL = "SELECT * FROM RemoteSysAttr WHERE RemoteID =" & nID

        oRs = clsMarsData.GetData(SQL)

        Try
            If oRs.EOF = False Then
                PCName = oRs("pcname").Value
                sPath = oRs("livepath").Value
                sUser = IsNull(oRs("username").Value)
                sPassword = _DecryptDBValue(IsNull(oRs("userpassword").Value))
            End If

            oRs.Close()
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            Exit Sub
        End Try

        Dim oLogin As New frmRemoteLogin
        Dim remoteConfigfile As String = "\\" & PCName & "\sqlrdconfigshare" & sConfigFileName
        Dim unified As Boolean = clsMarsUI.MainUI.ReadRegistry("UnifiedLogin", 0, , remoteConfigfile, True)
        Dim conType As String = clsMarsUI.MainUI.ReadRegistry("ConType", "DAT", , remoteConfigfile, True)
        Dim remoteCon As String

        If conType = "DAT" Then
            remoteCon = sPath & "\sqlrdlive.dat"
        Else
            remoteCon = clsMarsUI.MainUI.ReadRegistry("ConString", "", True, remoteConfigfile, True)

            '//just try opening the connection
            Dim con As ADODB.Connection = New ADODB.Connection

            Try
                con.Open(remoteCon)
                con.Close()
            Catch ex As Exception
                '//get the SQL Server Dynamic port on the remote machine
                Dim port As String = clsMarsUI.MainUI.ReadRegistry("SQLTCPPORT", "0", , remoteConfigfile, True)
                Dim suggestion As String = ""

                If port <> "0" Then
                    suggestion = "SQL-RD was unable to connect to the remote SQL-RD's database. Please make sure that the following ports are open in on the remote machine's Firewall" & ControlChars.CrLf & _
                    "UDP Port: 1434" & vbCrLf & _
                    "TCP/IP Port: " & port
                Else
                    suggestion = "SQL-RD was unable to connect to the remote SQL-RD's database. Please make sure that the ports used by SQL Server  are open on the remote machine's Firewall."
                End If

                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, suggestion)
                Return
            Finally
                con = Nothing
            End Try
        End If


        If oLogin.CheckLogin(remoteCon, nID, sUser, sPassword, unified) = False Then
            MessageBox.Show("Login to remote system failed. Please check the provided username and password.", _
            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        End If

        If ConnectRemote(sPath, , conType).Length = 0 Then
            Exit Sub
        End If



        nWindowCount = 0

        'close data connect
        oData.CloseMainDataConnection()

        gIsRemoteSystem = True

        'reset values 
        gPCName = PCName
        gConfigFile = remoteConfigfile

        If conType = "DAT" Then
            sCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sPath & "\sqlrdlive.dat;Persist Security Info=False"
            gConType = "DAT"
        Else
            sCon = remoteCon
            gConType = conType
        End If

        oData.OpenMainDataConnection()

        oData.UpgradeCRDDb()

        clsSettingsManager.Appconfig.populateConfigTable(gConfigFile)

        sAppPath = sPath & "\"

        Try
            oWindow = Nothing

            executingForm.Text = "SQL-RD " & gsEdition & " Edition - on Machine:\\" & PCName
            '  oMain.Text = "SQL-RD " & gsEdition & " Edition - on Machine:\\" & PCName
        Catch ex As Exception
            ''console.writeline(ex.ToString)
        End Try

        Try
            executingForm.btnRemoteConnect.Enabled = False
            executingForm.btnRemoteDisconnect.Enabled = True

            For Each it As DevComponents.DotNetBar.Office2007RibbonForm In gwindows
                If it.Tag <> executingForm.Tag Then
                    it.Close()
                End If
            Next

            executingForm.loadData()
            executingForm.loadFolders()
        Catch : End Try

        Select Case clsMarsUI.MainUI.ReadRegistry("MailType", "NONE", False)
            Case "MAPI"
                MailType = gMailType.MAPI
            Case "SMTP"
                MailType = gMailType.SMTP
            Case "GROUPWISE"
                MailType = gMailType.GROUPWISE
            Case "SQLRDMAIL"
                MailType = gMailType.SQLRDMAIL
            Case Else
                MailType = gMailType.NONE
        End Select

        Me.Close()

        gRole = "Administrator"
    End Sub

    Public Sub Disconnect()
        Dim sPath As String

        gPCName = String.Empty



        nWindowCount = 0

        'save all the data to the remote machine
        clsSettingsManager.Appconfig.savetoConfigFile(gConfigFile)

        'close data connect
        oData.CloseMainDataConnection()

        'reset values 
        sPath = Application.ExecutablePath.ToLower.Replace(assemblyName, "sqlrdlive.dat")


        sAppPath = Application.ExecutablePath.ToLower.Replace(assemblyName, "")

        If sAppPath.Substring(sAppPath.Length - 1, 1) <> "\" Then _
            sAppPath += "\"

        gConfigFile = sAppDataPath & "sqlrdlive.config"

        clsSettingsManager.Appconfig.populateConfigTable(gConfigFile)

        Dim conType As String = clsMarsUI.MainUI.ReadRegistry("ConType", "DAT")

        If conType = "DAT" Then
            sCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sPath & ";Persist Security Info=False"
            gConType = "DAT"
        Else
            sCon = clsMarsUI.MainUI.ReadRegistry("ConString", "", True)

            If sCon = "" Then sCon = "Provider=SQLOLEDB.1;Password=cssAdmin12345$;Persist Security Info=True;User ID=sa;Initial Catalog=SQL-RD;Data Source=" & gPCName & "\SQLRD"
            gConType = conType
        End If

        oData.OpenMainDataConnection()

        gIsRemoteSystem = False

        System.Threading.Thread.Sleep(5000)

        Try
            executingForm.Text = "SQL-RD " & gsEdition & " Edition"
        Catch : End Try

        Try

            executingForm.btnRemoteConnect.Enabled = True
            executingForm.btnRemoteDisconnect.Enabled = False

        Catch : End Try


    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click
        Dim nID As Integer
        Dim nValue As Int16
        Dim SQL As String
        Dim SQL2 As String = ""

        If lsvRemotes.SelectedItems.Count = 0 Then Return

        nValue = lsvRemotes.SelectedItems(0).SubItems(2).Text
        nID = lsvRemotes.SelectedItems(0).Tag

        If nValue = 1 Then
            SQL = "UPDATE RemoteSysAttr SET AutoConnect =0 WHERE RemoteID =" & nID
        Else
            SQL = "UPDATE RemoteSysAttr SET AutoConnect =1 WHERE RemoteID =" & nID
            SQL2 = "UPDATE RemoteSysAttr SET AutoConnect =0  WHERE RemoteID <>" & nID
        End If

        clsMarsData.WriteData(SQL)

        If SQL2.Length > 0 Then clsMarsData.WriteData(SQL2)

        LoadAll()
    End Sub
End Class
