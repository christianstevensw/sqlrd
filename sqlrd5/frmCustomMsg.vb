Public Class frmCustomMsg
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim m_link As String = ""
    Dim iscancel As Boolean = False

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtMsg As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnLink As DevComponents.DotNetBar.ButtonX
    Friend WithEvents tmFlash As System.Windows.Forms.Timer
    Friend WithEvents btnCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCopy As DevComponents.DotNetBar.ButtonX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCustomMsg))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtMsg = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCopy = New DevComponents.DotNetBar.ButtonX()
        Me.btnLink = New DevComponents.DotNetBar.ButtonX()
        Me.tmFlash = New System.Windows.Forms.Timer(Me.components)
        Me.btnCancel = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtMsg)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(680, 496)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'txtMsg
        '
        Me.txtMsg.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        '
        '
        '
        Me.txtMsg.Border.Class = "TextBoxBorder"
        Me.txtMsg.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtMsg.Location = New System.Drawing.Point(8, 16)
        Me.txtMsg.Multiline = True
        Me.txtMsg.Name = "txtMsg"
        Me.txtMsg.ReadOnly = True
        Me.txtMsg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtMsg.Size = New System.Drawing.Size(664, 472)
        Me.txtMsg.TabIndex = 0
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(530, 512)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 1
        Me.cmdOK.Text = "&OK"
        '
        'cmdCopy
        '
        Me.cmdCopy.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCopy.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCopy.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCopy.Location = New System.Drawing.Point(8, 512)
        Me.cmdCopy.Name = "cmdCopy"
        Me.cmdCopy.Size = New System.Drawing.Size(120, 23)
        Me.cmdCopy.TabIndex = 2
        Me.cmdCopy.Text = "Copy to clipboard"
        '
        'btnLink
        '
        Me.btnLink.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnLink.BackColor = System.Drawing.SystemColors.Control
        Me.btnLink.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnLink.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnLink.Location = New System.Drawing.Point(134, 512)
        Me.btnLink.Name = "btnLink"
        Me.btnLink.Size = New System.Drawing.Size(120, 23)
        Me.btnLink.TabIndex = 2
        Me.btnLink.Text = "More info"
        Me.btnLink.Visible = False
        '
        'tmFlash
        '
        Me.tmFlash.Interval = 500
        '
        'btnCancel
        '
        Me.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnCancel.Location = New System.Drawing.Point(611, 512)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "&Cancel"
        '
        'frmCustomMsg
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(698, 544)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnLink)
        Me.Controls.Add(Me.cmdCopy)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCustomMsg"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SQL-RD Message"
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Close()
    End Sub

    Public Function ShowMsg(ByVal sMsg As String, ByVal sCaption As String, _
    Optional ByVal ShowDialog As Boolean = False, Optional ByVal link As String = "") As DialogResult
        Me.CheckForIllegalCrossThreadCalls = False
        txtMsg.CheckForIllegalCrossThreadCalls = False

        Me.Text = sCaption

        txtMsg.Text = sMsg

        txtMsg.SelectionLength = 0

        If link <> "" Then
            Me.m_link = link
            Me.btnLink.Visible = True
            tmFlash.Enabled = True
        End If

        If ShowDialog = True Then
            Me.ShowDialog()
        Else
            'Me.MdiParent = oMain
            Me.ShowDialog()
        End If

        If iscancel Then
            Return Windows.Forms.DialogResult.Cancel
        Else
            Return Windows.Forms.DialogResult.OK
        End If
    End Function

    Private Sub frmCustomMsg_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub cmdCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCopy.Click
        txtMsg.SelectAll()
        txtMsg.Copy()
    End Sub

    Private Sub btnLink_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLink.Click
        Try
            Process.Start(m_link)
        Catch : End Try
    End Sub

    Private Sub tmFlash_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmFlash.Tick
        On Error Resume Next
        If btnLink.BackColor = Color.FromKnownColor(KnownColor.Control) Then
            btnLink.BackColor = Color.FromKnownColor(KnownColor.ControlDark)
        Else
            btnLink.BackColor = Color.FromKnownColor(KnownColor.Control)
        End If
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        iscancel = True
        Close()
    End Sub
End Class
