Public Class frmFolders
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim isCancel As Boolean
    Friend WithEvents imgFolders As System.Windows.Forms.ImageList
    Friend WithEvents btnNewFolder As DevComponents.DotNetBar.ButtonX
    Public SelectDeskTop As Boolean = False
    Public m_eventOnly As Boolean = False
    Public m_packageOnly As Boolean = False
    Friend WithEvents tvFolders As DevComponents.AdvTree.AdvTree
    Friend WithEvents Node1 As DevComponents.AdvTree.Node
    Friend WithEvents Node3 As DevComponents.AdvTree.Node
    Friend WithEvents Node4 As DevComponents.AdvTree.Node
    Friend WithEvents Node5 As DevComponents.AdvTree.Node
    Friend WithEvents NodeConnector1 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle1 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents ElementStyle3 As DevComponents.DotNetBar.ElementStyle
    Public m_eventPackage As Boolean = False
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Footer1 As WizardFooter.Footer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.imgFolders = New System.Windows.Forms.ImageList(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Footer1 = New WizardFooter.Footer()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.btnNewFolder = New DevComponents.DotNetBar.ButtonX()
        Me.tvFolders = New DevComponents.AdvTree.AdvTree()
        Me.Node1 = New DevComponents.AdvTree.Node()
        Me.Node3 = New DevComponents.AdvTree.Node()
        Me.Node4 = New DevComponents.AdvTree.Node()
        Me.Node5 = New DevComponents.AdvTree.Node()
        Me.NodeConnector1 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle1 = New DevComponents.DotNetBar.ElementStyle()
        Me.ElementStyle3 = New DevComponents.DotNetBar.ElementStyle()
        CType(Me.tvFolders, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'imgFolders
        '
        Me.imgFolders.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.imgFolders.ImageSize = New System.Drawing.Size(16, 16)
        Me.imgFolders.TransparentColor = System.Drawing.Color.Transparent
        '
        'Label1
        '
        Me.Label1.ForeColor = System.Drawing.Color.Navy
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(296, 16)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Please select the folder..."
        '
        'Footer1
        '
        Me.Footer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Footer1.Location = New System.Drawing.Point(0, 328)
        Me.Footer1.Name = "Footer1"
        Me.Footer1.Size = New System.Drawing.Size(336, 16)
        Me.Footer1.TabIndex = 4
        Me.Footer1.TabStop = False
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.Enabled = False
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(152, 344)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 24)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(232, 344)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 24)
        Me.cmdCancel.TabIndex = 49
        Me.cmdCancel.Text = "&Cancel"
        '
        'btnNewFolder
        '
        Me.btnNewFolder.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnNewFolder.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnNewFolder.Location = New System.Drawing.Point(8, 294)
        Me.btnNewFolder.Name = "btnNewFolder"
        Me.btnNewFolder.Size = New System.Drawing.Size(87, 25)
        Me.btnNewFolder.TabIndex = 1
        Me.btnNewFolder.Text = "New Folder"
        '
        'tvFolders
        '
        Me.tvFolders.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.tvFolders.AllowDrop = True
        Me.tvFolders.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.tvFolders.BackgroundStyle.BackColor = System.Drawing.Color.WhiteSmoke
        Me.tvFolders.BackgroundStyle.BackColor2 = System.Drawing.Color.AliceBlue
        Me.tvFolders.BackgroundStyle.BackColorGradientAngle = 45
        Me.tvFolders.BackgroundStyle.Class = "TreeBorderKey"
        Me.tvFolders.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.tvFolders.ExpandButtonType = DevComponents.AdvTree.eExpandButtonType.Triangle
        Me.tvFolders.ExpandLineColor = System.Drawing.Color.White
        Me.tvFolders.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tvFolders.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.tvFolders.Location = New System.Drawing.Point(11, 27)
        Me.tvFolders.Name = "tvFolders"
        Me.tvFolders.Nodes.AddRange(New DevComponents.AdvTree.Node() {Me.Node1, Me.Node3, Me.Node4, Me.Node5})
        Me.tvFolders.NodesConnector = Me.NodeConnector1
        Me.tvFolders.NodeStyle = Me.ElementStyle1
        Me.tvFolders.PathSeparator = ";"
        Me.tvFolders.Size = New System.Drawing.Size(296, 261)
        Me.tvFolders.Styles.Add(Me.ElementStyle1)
        Me.tvFolders.Styles.Add(Me.ElementStyle3)
        Me.tvFolders.TabIndex = 51
        Me.tvFolders.Text = "AdvTree1"
        '
        'Node1
        '
        Me.Node1.Expanded = True
        Me.Node1.ImageIndex = 1
        Me.Node1.Name = "Node1"
        Me.Node1.Text = "Daily"
        '
        'Node3
        '
        Me.Node3.Expanded = True
        Me.Node3.ImageIndex = 1
        Me.Node3.Name = "Node3"
        Me.Node3.Text = "Monthly"
        '
        'Node4
        '
        Me.Node4.Expanded = True
        Me.Node4.ImageIndex = 1
        Me.Node4.Name = "Node4"
        Me.Node4.Text = "Weekly"
        '
        'Node5
        '
        Me.Node5.Expanded = True
        Me.Node5.ImageIndex = 1
        Me.Node5.Name = "Node5"
        Me.Node5.Text = "Others"
        '
        'NodeConnector1
        '
        Me.NodeConnector1.LineColor = System.Drawing.Color.Transparent
        '
        'ElementStyle1
        '
        Me.ElementStyle1.Class = ""
        Me.ElementStyle1.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle1.Name = "ElementStyle1"
        Me.ElementStyle1.TextColor = System.Drawing.SystemColors.ControlText
        '
        'ElementStyle3
        '
        Me.ElementStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(236, Byte), Integer))
        Me.ElementStyle3.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(164, Byte), Integer), CType(CType(187, Byte), Integer))
        Me.ElementStyle3.BackColorGradientAngle = 90
        Me.ElementStyle3.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ElementStyle3.BorderBottomWidth = 1
        Me.ElementStyle3.BorderColor = System.Drawing.Color.DarkGray
        Me.ElementStyle3.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ElementStyle3.BorderLeftWidth = 1
        Me.ElementStyle3.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ElementStyle3.BorderRightWidth = 1
        Me.ElementStyle3.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ElementStyle3.BorderTopWidth = 1
        Me.ElementStyle3.Class = ""
        Me.ElementStyle3.CornerDiameter = 4
        Me.ElementStyle3.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle3.Description = "Magenta"
        Me.ElementStyle3.Name = "ElementStyle3"
        Me.ElementStyle3.PaddingBottom = 1
        Me.ElementStyle3.PaddingLeft = 1
        Me.ElementStyle3.PaddingRight = 1
        Me.ElementStyle3.PaddingTop = 1
        Me.ElementStyle3.TextColor = System.Drawing.Color.Black
        '
        'frmFolders
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(314, 378)
        Me.ControlBox = False
        Me.Controls.Add(Me.tvFolders)
        Me.Controls.Add(Me.btnNewFolder)
        Me.Controls.Add(Me.Footer1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmFolders"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Folders"
        CType(Me.tvFolders, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        isCancel = True
        Me.Close()
    End Sub

    Private Sub frmFolders_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oUI As clsMarsUI = New clsMarsUI
        FormatForWinXP(Me)
        tvFolders.PathSeparator = "\"
        loadFolders()
    End Sub
    Public Function GetEventBasedSchedule() As Hashtable

        Me.ShowDialog()

        If isCancel = True Then Return Nothing

        Dim values As Hashtable = New Hashtable

        values.Add("Name", tvFolders.SelectedNode.Text)
        values.Add("ID", Convert.ToString(tvFolders.SelectedNode.Tag).Split(":")(1))

        Return values
    End Function

    Public Overloads Function getFolder(ByVal title As String, ByRef folderPath As String, ByRef folderid As Integer) As Boolean
        Label1.Text = title

        Me.ShowDialog()

        If isCancel = False Then
            folderPath = tvFolders.SelectedNode.FullPath
            folderid = tvFolders.SelectedNode.Tag.ToString.Split(":")(1)
            Return True
        Else
            Return False
        End If
    End Function

    Public Overloads Function GetFolder(Optional ByVal sTitle As String = "Please select the folder") As String()
        Dim sFolder(1) As String

        Label1.Text = sTitle

        Me.ShowDialog()

        If isCancel = True Then
            sFolder(0) = ""
            sFolder(1) = 0
            Me.Close()
            isCancel = False
            Return sFolder
        End If

        If gRole <> "Administrator" Then
            Dim grp As usergroup = New usergroup(gRole)
            Dim selectedFolderID As Integer = GetDelimitedWord(tvFolders.SelectedNode.Tag, 2, ":")

            If grp.isFolderAccessRestricted Then
                Dim allowedFolders As System.Collections.Generic.List(Of Integer) = grp.userAllowedFolders

                If allowedFolders.Contains(selectedFolderID) = False Then
                    MessageBox.Show("Sorry, you do not have access rights to create schedules in the selected folder", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)

                    sFolder(0) = ""
                    sFolder(1) = 0
                    Me.Close()
                    isCancel = False
                    Return sFolder
                End If
            End If

        End If

        sFolder(0) = tvFolders.SelectedNode.FullPath
        sFolder(1) = GetDelimitedWord(tvFolders.SelectedNode.Tag, 2, ":")

        Return sFolder
    End Function
    Public Function GetPackage() As Integer
        Me.ShowDialog()

        If isCancel = True Then Return 0

        Return tvFolders.SelectedNode.Tag.ToString.Split(":")(1)
    End Function

    Private Sub tvFolders_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs)
        Dim sType As String

        sType = GetDelimitedWord(tvFolders.SelectedNode.Tag, 1, ":")

        If Me.m_packageOnly = True Then

            If sType = "Package" Then
                cmdOK.Enabled = True
            Else
                cmdOK.Enabled = False
            End If

            btnNewFolder.Enabled = False
        ElseIf m_eventPackage = True Then
            If sType = "Event-Package" Then
                cmdOK.Enabled = True
            Else
                cmdOK.Enabled = False
            End If
        ElseIf m_eventOnly = False Then
            If sType = "Folder" Then
                cmdOK.Enabled = True
            ElseIf SelectDeskTop = True Then
                If sType = "Desktop" Then
                    cmdOK.Enabled = True
                End If
            Else
                cmdOK.Enabled = False
            End If

            If sType = "Folder" Or sType = "Desktop" Then
                btnNewFolder.Enabled = True
            Else
                btnNewFolder.Enabled = False
            End If
        Else
            btnNewFolder.Enabled = False

            If sType = "Event" Then
                cmdOK.Enabled = True
            Else
                cmdOK.Enabled = False
            End If
        End If
    End Sub

    Private Sub tvFolders_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        On Error Resume Next

        If GetDelimitedWord(tvFolders.SelectedNode.Tag, 1, ":") = "Folder" Then
            cmdOK.Enabled = True
        Else
            cmdOK.Enabled = False
        End If
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click

        Me.Close()
    End Sub

    Private Sub tvFolders_AfterNodeSelect(ByVal sender As Object, ByVal e As DevComponents.AdvTree.AdvTreeNodeEventArgs) Handles tvFolders.AfterNodeSelect
        'If tvFolders.SelectedNode IsNot Nothing Then
        '    cmdOK.Enabled = True
        '    btnNewFolder.Enabled = True
        'Else
        '    cmdOK.Enabled = False
        '    btnNewFolder.Enabled = False
        'End If
        If tvFolders.SelectedNode Is Nothing Then Return

        Dim sType As String

        sType = GetDelimitedWord(tvFolders.SelectedNode.Tag, 1, ":")

        sType = sType.ToLower

        If Me.m_packageOnly = True Then

            If sType = "package" Then
                cmdOK.Enabled = True
            Else
                cmdOK.Enabled = False
            End If

            btnNewFolder.Enabled = False
        ElseIf m_eventPackage = True Then
            If sType = "event-package" Then
                cmdOK.Enabled = True
            Else
                cmdOK.Enabled = False
            End If
        ElseIf m_eventOnly = False Then
            If sType = "folder" Then
                cmdOK.Enabled = True
            ElseIf SelectDeskTop = True Then
                If sType = "desktop" Then
                    cmdOK.Enabled = True
                End If
            Else
                cmdOK.Enabled = False
            End If

            If sType = "folder" Or sType = "desktop" Then
                btnNewFolder.Enabled = True
            Else
                btnNewFolder.Enabled = False
            End If
        Else
            btnNewFolder.Enabled = False

            If sType = "event" Then
                cmdOK.Enabled = True
            Else
                cmdOK.Enabled = False
            End If
        End If
    End Sub

    Private Sub tvFolders_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvFolders.NodeDoubleClick
        
    End Sub

    Private Sub btnNewFolder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewFolder.Click
        Try
            Dim parent As Integer
            Dim parentNode As DevComponents.AdvTree.Node

            If tvFolders.SelectedNode IsNot Nothing Then
                parent = Me.tvFolders.SelectedNode.Tag.split(":")(1)
                parentNode = tvFolders.SelectedNode
            Else
                parent = 0
                parentNode = Nothing
            End If

            Dim pFolder As folder = New folder(parent)

            Dim sName As String = InputBox("Please enter the new folder name", Application.ProductName, "Untitled Folder")

            If sName = "" Then Return

            Dim newfolderid As Integer = pFolder.addFolder(sName, Nothing)

            '  Dim newFolder As Hashtable = clsMarsUI.MainUI.NewFolder(parent)

            'If newFolder Is Nothing Then Return

            loadFolders()

            Dim newFolderNode As DevComponents.AdvTree.Node = tvFolders.FindNodeByDataKey(newfolderid)

            If newFolderNode IsNot Nothing Then
                tvFolders.SelectedNode = newFolderNode
                newFolderNode.EnsureVisible()
            End If

            'Dim newFolderNode As DevComponents.AdvTree.Node = New DevComponents.AdvTree.Node(newFolder.Item("Name"))
            'newFolderNode.Tag = newFolder.Item("Tag")
            'newFolderNode.Image = resizeImage(My.Resources.Folder_256x2561, 16, 16)

            'If parentNode IsNot Nothing Then
            '    parentNode.Nodes.Add(newFolderNode)

            '    parentNode.Expand()
            'Else
            '    tvFolders.Nodes.Add(newFolderNode)
            'End If

            btnNewFolder.Enabled = True

            If selectedForm IsNot Nothing AndAlso selectedForm.tvExplorer.SelectedNode IsNot Nothing Then
                selectedForm.loadData(, True)
                Try
                    selectedForm.buildCrumbBar(parent, selectedForm.crumb.SelectedItem)
                Catch : End Try
            End If
        Catch : End Try
    End Sub

    Sub loadFolders()
        tvFolders.BeginUpdate()
        tvFolders.Nodes.Clear()

        Dim topLevel As folder = New folder(0)

        For Each fld As folder In topLevel.getSubFolders
            Dim folderName As String = fld.folderName
            Dim folderID As Integer = fld.folderID

            Using node As DevComponents.AdvTree.Node = New DevComponents.AdvTree.Node(folderName)
                node.Tag = "folder:" & folderID
                node.DataKey = folderID
                node.Image = resizeImage(My.Resources.Folder_256x2561, 16, 16)

                tvFolders.Nodes.Add(node)

                populatefolderChildren(folderID, node)
            End Using
        Next

        tvFolders.EndUpdate()
        tvFolders.Refresh()
    End Sub

    Sub populatefolderChildren(ByVal folderID As Integer, ByVal parentnode As DevComponents.AdvTree.Node)
        Dim parentFolder As folder = New folder(folderID)

        For Each fld As folder In parentFolder.getSubFolders
            Using node As DevComponents.AdvTree.Node = New DevComponents.AdvTree.Node(fld.folderName)
                node.Tag = "folder:" & fld.folderID
                node.Image = resizeImage(My.Resources.Folder_256x2561, 16, 16)
                node.DataKey = fld.folderID
                parentnode.Nodes.Add(node)


                populatefolderChildren(fld.folderID, node)
            End Using
        Next

        If selectAny Then
            For Each rpt As cssreport In parentFolder.getReports
                Using node As DevComponents.AdvTree.Node = New DevComponents.AdvTree.Node(rpt.reportName)
                    node.Tag = "report:" & rpt.ID
                    node.Image = resizeImage(rpt.reportImage, 16, 16)
                    node.DataKey = rpt.ID
                    parentnode.Nodes.Add(node)
                End Using
            Next
        End If

        If selectAny Then
            For Each pack As Package In parentFolder.getPackages
                Using node As DevComponents.AdvTree.Node = New DevComponents.AdvTree.Node(pack.packageName)
                    node.Tag = "package:" & pack.ID
                    node.Image = resizeImage(pack.packageImage, 16, 16)
                    node.DataKey = pack.ID
                    parentnode.Nodes.Add(node)
                End Using
            Next
        End If

        If selectAny Then
            For Each auto As Automation In parentFolder.getAutomations
                Using node As DevComponents.AdvTree.Node = New DevComponents.AdvTree.Node(auto.AutoName)
                    node.Tag = "automation:" & auto.ID
                    node.Image = resizeImage(auto.automationImage, 16, 16)
                    node.DataKey = auto.ID
                    parentnode.Nodes.Add(node)
                End Using
            Next
        End If

        If selectAny Or m_eventOnly Then
            For Each evt As EventBased In parentFolder.getEventBased
                Using node As DevComponents.AdvTree.Node = New DevComponents.AdvTree.Node(evt.eventName)
                    node.Tag = "event:" & evt.ID
                    node.Image = resizeImage(evt.eventImage, 16, 16)
                    node.DataKey = evt.ID
                    parentnode.Nodes.Add(node)
                End Using
            Next
        End If

        If selectAny Then
            For Each evtpack As EventBasedPackage In parentFolder.getEventBasedPackages
                Using node As DevComponents.AdvTree.Node = New DevComponents.AdvTree.Node(evtpack.eventPackageName)
                    node.Tag = "event-package:" & evtpack.ID
                    node.Image = resizeImage(evtpack.packageImage, 16, 16)
                    node.DataKey = evtpack.ID
                    parentnode.Nodes.Add(node)
                End Using
            Next
        End If
    End Sub

    Private Sub tvFolders_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tvFolders.Click

    End Sub

    Private Sub tvFolders_DoubleClick1(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvFolders.DoubleClick
        cmdOK_Click(Nothing, Nothing)
    End Sub

    Dim selectAny As Boolean

    Public Function getAnyObject() As sqlrdAPI.sqlrdObject
        selectAny = True
        m_eventOnly = True

        Me.ShowDialog()

        If isCancel Then Return Nothing

        Dim obj As sqlrdAPI.sqlrdObject

        obj.objectID = Convert.ToString(tvFolders.SelectedNode.Tag).Split(":")(1)

        Dim path As String = ""
        Dim pathSplit() As String = tvFolders.SelectedNode.FullPath.Split("\")

        For I As Integer = 1 To pathSplit.GetUpperBound(0) - 1
            path &= pathSplit(I) & "\"
        Next

        obj.objectLocation = path
        obj.objectName = tvFolders.SelectedNode.Text

        Dim type As String = Convert.ToString(tvFolders.SelectedNode.Tag).Split(":")(0)

        Select Case type.ToLower
            Case "folder"
                obj.objectType = clsMarsScheduler.enScheduleType.FOLDER
            Case "package"
                obj.objectType = clsMarsScheduler.enScheduleType.PACKAGE
            Case "event-package"
                obj.objectType = clsMarsScheduler.enScheduleType.EVENTPACKAGE
            Case "report"
                obj.objectType = clsMarsScheduler.enScheduleType.REPORT
            Case "automation"
                obj.objectType = clsMarsScheduler.enScheduleType.AUTOMATION
            Case "event"
                obj.objectType = clsMarsScheduler.enScheduleType.EVENTBASED
            Case Else
                obj.objectType = clsMarsScheduler.enScheduleType.NONE
        End Select

        Return obj
    End Function
End Class
