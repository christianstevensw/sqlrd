Imports Microsoft.Win32
Public Class frmMAPIType
    Inherits DevComponents.DotNetBar.Office2007Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbMapiType As System.Windows.Forms.ComboBox
    Friend WithEvents grpExchange As System.Windows.Forms.GroupBox
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtAlias As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtServer As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdVerify As DevComponents.DotNetBar.ButtonX
    Friend WithEvents grpExchangeWDS As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtServerUrl As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtEWSPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtEWSUserID As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtSenderAddress As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtSenderName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents btnAutoDiscover As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtAutodiscover As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents chkAutodiscover As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.cmbMapiType = New System.Windows.Forms.ComboBox()
        Me.grpExchange = New System.Windows.Forms.GroupBox()
        Me.cmdVerify = New DevComponents.DotNetBar.ButtonX()
        Me.txtAlias = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.txtServer = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.grpExchangeWDS = New System.Windows.Forms.GroupBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtEWSPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtEWSUserID = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label7 = New DevComponents.DotNetBar.LabelX()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.txtSenderAddress = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtSenderName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtServerUrl = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label9 = New DevComponents.DotNetBar.LabelX()
        Me.Label8 = New DevComponents.DotNetBar.LabelX()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.txtAutodiscover = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btnAutoDiscover = New DevComponents.DotNetBar.ButtonX()
        Me.chkAutodiscover = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.grpExchange.SuspendLayout()
        Me.grpExchangeWDS.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(200, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "MAPI Type"
        '
        'cmbMapiType
        '
        Me.cmbMapiType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMapiType.ItemHeight = 13
        Me.cmbMapiType.Items.AddRange(New Object() {"Stand-Alone Outlook", "Exchange Server", "Exchange Web Service (2007 or later)"})
        Me.cmbMapiType.Location = New System.Drawing.Point(8, 24)
        Me.cmbMapiType.Name = "cmbMapiType"
        Me.cmbMapiType.Size = New System.Drawing.Size(389, 21)
        Me.cmbMapiType.TabIndex = 0
        '
        'grpExchange
        '
        Me.grpExchange.Controls.Add(Me.cmdVerify)
        Me.grpExchange.Controls.Add(Me.txtAlias)
        Me.grpExchange.Controls.Add(Me.Label2)
        Me.grpExchange.Controls.Add(Me.Label3)
        Me.grpExchange.Controls.Add(Me.Label4)
        Me.grpExchange.Controls.Add(Me.txtServer)
        Me.grpExchange.Location = New System.Drawing.Point(8, 48)
        Me.grpExchange.Name = "grpExchange"
        Me.grpExchange.Size = New System.Drawing.Size(389, 146)
        Me.grpExchange.TabIndex = 1
        Me.grpExchange.TabStop = False
        Me.grpExchange.Visible = False
        '
        'cmdVerify
        '
        Me.cmdVerify.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdVerify.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdVerify.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdVerify.Location = New System.Drawing.Point(163, 115)
        Me.cmdVerify.Name = "cmdVerify"
        Me.cmdVerify.Size = New System.Drawing.Size(75, 23)
        Me.cmdVerify.TabIndex = 2
        Me.cmdVerify.Text = "Verify"
        '
        'txtAlias
        '
        '
        '
        '
        Me.txtAlias.Border.Class = "TextBoxBorder"
        Me.txtAlias.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtAlias.Location = New System.Drawing.Point(8, 88)
        Me.txtAlias.Name = "txtAlias"
        Me.txtAlias.Size = New System.Drawing.Size(190, 21)
        Me.txtAlias.TabIndex = 0
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 11)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(256, 48)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "To use MAPI with the NT service scheduler, you must provide your exchange server " & _
    "name and MAPI alias"
        Me.Label2.WordWrap = True
        '
        'Label3
        '
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 72)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(104, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "MAPI Alias"
        '
        'Label4
        '
        '
        '
        '
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(208, 72)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(151, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Exchange Server Name"
        '
        'txtServer
        '
        '
        '
        '
        Me.txtServer.Border.Class = "TextBoxBorder"
        Me.txtServer.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtServer.Location = New System.Drawing.Point(208, 88)
        Me.txtServer.Name = "txtServer"
        Me.txtServer.Size = New System.Drawing.Size(170, 21)
        Me.txtServer.TabIndex = 1
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.Enabled = False
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(412, 24)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 2
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(412, 56)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 3
        Me.cmdCancel.Text = "&Cancel"
        '
        'grpExchangeWDS
        '
        Me.grpExchangeWDS.Controls.Add(Me.btnAutoDiscover)
        Me.grpExchangeWDS.Controls.Add(Me.GroupBox1)
        Me.grpExchangeWDS.Controls.Add(Me.txtSenderAddress)
        Me.grpExchangeWDS.Controls.Add(Me.txtSenderName)
        Me.grpExchangeWDS.Controls.Add(Me.txtServerUrl)
        Me.grpExchangeWDS.Controls.Add(Me.txtAutodiscover)
        Me.grpExchangeWDS.Controls.Add(Me.Label9)
        Me.grpExchangeWDS.Controls.Add(Me.Label8)
        Me.grpExchangeWDS.Controls.Add(Me.Label5)
        Me.grpExchangeWDS.Controls.Add(Me.chkAutodiscover)
        Me.grpExchangeWDS.Location = New System.Drawing.Point(8, 48)
        Me.grpExchangeWDS.Name = "grpExchangeWDS"
        Me.grpExchangeWDS.Size = New System.Drawing.Size(389, 300)
        Me.grpExchangeWDS.TabIndex = 1
        Me.grpExchangeWDS.TabStop = False
        Me.grpExchangeWDS.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtEWSPassword)
        Me.GroupBox1.Controls.Add(Me.txtEWSUserID)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 20)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(309, 86)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Authentication"
        '
        'txtEWSPassword
        '
        '
        '
        '
        Me.txtEWSPassword.Border.Class = "TextBoxBorder"
        Me.txtEWSPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtEWSPassword.Location = New System.Drawing.Point(90, 56)
        Me.txtEWSPassword.Name = "txtEWSPassword"
        Me.txtEWSPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtEWSPassword.Size = New System.Drawing.Size(203, 21)
        Me.txtEWSPassword.TabIndex = 1
        '
        'txtEWSUserID
        '
        '
        '
        '
        Me.txtEWSUserID.Border.Class = "TextBoxBorder"
        Me.txtEWSUserID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtEWSUserID.Location = New System.Drawing.Point(90, 23)
        Me.txtEWSUserID.Name = "txtEWSUserID"
        Me.txtEWSUserID.Size = New System.Drawing.Size(203, 21)
        Me.txtEWSUserID.TabIndex = 0
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        '
        '
        '
        Me.Label7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label7.Location = New System.Drawing.Point(6, 59)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(48, 16)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Password"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        '
        '
        '
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.Location = New System.Drawing.Point(6, 26)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(77, 16)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Domain\UserID"
        '
        'txtSenderAddress
        '
        '
        '
        '
        Me.txtSenderAddress.Border.Class = "TextBoxBorder"
        Me.txtSenderAddress.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSenderAddress.Location = New System.Drawing.Point(6, 269)
        Me.txtSenderAddress.Name = "txtSenderAddress"
        Me.txtSenderAddress.Size = New System.Drawing.Size(307, 21)
        Me.txtSenderAddress.TabIndex = 4
        '
        'txtSenderName
        '
        '
        '
        '
        Me.txtSenderName.Border.Class = "TextBoxBorder"
        Me.txtSenderName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSenderName.Location = New System.Drawing.Point(6, 229)
        Me.txtSenderName.Name = "txtSenderName"
        Me.txtSenderName.Size = New System.Drawing.Size(307, 21)
        Me.txtSenderName.TabIndex = 3
        '
        'txtServerUrl
        '
        '
        '
        '
        Me.txtServerUrl.Border.Class = "TextBoxBorder"
        Me.txtServerUrl.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtServerUrl.Location = New System.Drawing.Point(6, 187)
        Me.txtServerUrl.Name = "txtServerUrl"
        Me.txtServerUrl.Size = New System.Drawing.Size(307, 21)
        Me.txtServerUrl.TabIndex = 2
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        '
        '
        '
        Me.Label9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label9.Location = New System.Drawing.Point(6, 253)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(78, 16)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Sender Address"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        '
        '
        '
        Me.Label8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label8.Location = New System.Drawing.Point(6, 213)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(68, 16)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Sender Name"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        '
        '
        '
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.Location = New System.Drawing.Point(6, 171)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(127, 16)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Server Name/Service URL"
        '
        'txtAutodiscover
        '
        '
        '
        '
        Me.txtAutodiscover.Border.Class = "TextBoxBorder"
        Me.txtAutodiscover.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtAutodiscover.Enabled = False
        Me.txtAutodiscover.Location = New System.Drawing.Point(6, 136)
        Me.txtAutodiscover.Name = "txtAutodiscover"
        Me.txtAutodiscover.Size = New System.Drawing.Size(307, 21)
        Me.txtAutodiscover.TabIndex = 0
        '
        'btnAutoDiscover
        '
        Me.btnAutoDiscover.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnAutoDiscover.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnAutoDiscover.Enabled = False
        Me.btnAutoDiscover.Location = New System.Drawing.Point(319, 136)
        Me.btnAutoDiscover.Name = "btnAutoDiscover"
        Me.btnAutoDiscover.Size = New System.Drawing.Size(59, 21)
        Me.btnAutoDiscover.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnAutoDiscover.TabIndex = 1
        Me.btnAutoDiscover.Text = "Go"
        '
        'chkAutodiscover
        '
        '
        '
        '
        Me.chkAutodiscover.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAutodiscover.Location = New System.Drawing.Point(8, 116)
        Me.chkAutodiscover.Name = "chkAutodiscover"
        Me.chkAutodiscover.Size = New System.Drawing.Size(305, 22)
        Me.chkAutodiscover.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkAutodiscover.TabIndex = 1
        Me.chkAutodiscover.Text = "Use Exchange Auto-Discovery"
        '
        'frmMAPIType
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(500, 351)
        Me.ControlBox = False
        Me.Controls.Add(Me.grpExchangeWDS)
        Me.Controls.Add(Me.grpExchange)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmbMapiType)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmdCancel)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmMAPIType"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Advanced MAPI Settings"
        Me.grpExchange.ResumeLayout(False)
        Me.grpExchangeWDS.ResumeLayout(False)
        Me.grpExchangeWDS.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim isCancel As Boolean
    Dim oUI As clsMarsUI = New clsMarsUI
    Dim oReg As RegistryKey = Registry.LocalMachine
    Dim sKey As String = "Software\ChristianSteven\" & Application.ProductName
    Dim MAPIType As String
    Private Sub changeHeight(ByVal newHeight As Integer)
        Dim current As Integer = Me.Height

        If newHeight > current Then
            For I As Integer = current To newHeight
                Me.Height = I
                _Delay(0.00125)
            Next
        Else
            Dim I As Integer = Me.Height

            Do While Me.Height > newHeight
                Me.Height = I
                I -= 1
                _Delay(0.00125)
            Loop
        End If
    End Sub
    Private Sub frmMAPIType_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim sMsg As String
        Dim CrLf = Environment.NewLine

        FormatForWinXP(Me)

        sMsg = "To obtain your MAPI alias:" & CrLf & _
        "- Go into into control panels" & CrLf & _
        "- Double-click 'Mail' to open it" & CrLf & _
        "- Click the 'Email Accounts' button" & CrLf & _
        "- Select 'View or change existing email accounts'" & CrLf & _
        "- Click  'Next' button" & CrLf & _
        "- Select your Microsoft Exchange Server account" & CrLf & _
        "- Click  'Change' button" & CrLf & _
        "- Your MAPI alias is in the field called 'User Name'"

        ToolTip1.SetToolTip(txtAlias, sMsg)

        sMsg = "To obtain your Microsoft Exchange Server name:" & CrLf & _
        "- Go into into control panels" & CrLf & _
        "- Double-click 'Mail' to open it" & CrLf & _
        "- Click the 'Email Accounts' button" & CrLf & _
        "- Select 'View or change existing email accounts'" & CrLf & _
        "- Click 'Next' button" & CrLf & _
        "- Select your Microsoft Exchange Server account" & CrLf & _
        "- Click 'Change' button" & CrLf & _
        "- Your MS Exchange server name is in the field called 'Microsoft Exchange Server'"

        ToolTip1.SetToolTip(txtServer, sMsg)

        txtAlias.Text = oUI.ReadRegistry("MAPIAlias", "")
        txtServer.Text = oUI.ReadRegistry("MAPIServer", "")
        txtServerUrl.Text = oUI.ReadRegistry("ExchangeWDSUrl", "")
        txtEWSUserID.Text = oUI.ReadRegistry("ExchangeWDSUserID", "")
        txtEWSPassword.Text = oUI.ReadRegistry("ExchangeWDSPassword", "", True, )
        txtSenderName.Text = oUI.ReadRegistry("ExchangeWDSSender", "")
        txtSenderAddress.Text = oUI.ReadRegistry("exchangeWDSSenderAddress", "")
        txtAutodiscover.Text = oUI.ReadRegistry("ExchangeAutoDiscoverEmailAddress", "")
        chkAutodiscover.Checked = Convert.ToInt16(oUI.ReadRegistry("ExchangeAutoDiscover", 0))

    End Sub

    Private Sub cmbMapiType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbMapiType.SelectedIndexChanged
        If cmbMapiType.Text = "Stand-Alone Outlook" Then
            grpExchange.Visible = False
            grpExchangeWDS.Visible = False
            Me.Height = 112
            MAPIType = "Single"
            cmdOK.Enabled = True
        ElseIf cmbMapiType.Text = "Exchange Server" Then
            grpExchange.Visible = True
            grpExchangeWDS.Visible = False
            Me.Height = 224
            MAPIType = "Server"
            cmdOK.Enabled = False
        ElseIf cmbMapiType.Text = "Exchange Web Service (2007 or later)" Then
            grpExchangeWDS.Visible = True
            grpExchange.Visible = False
            Me.Height = 390
            MAPIType = "ExchangeWDS"
            cmdOK.Enabled = True
        End If
    End Sub

    Private Sub cmdVerify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdVerify.Click
        Try
            RedemptionL.RedemptionLoader.DllLocation32Bit = IO.Path.Combine(sAppPath, "Redemption.dll")

            Dim cdoSession As Object = RedemptionL.RedemptionLoader.new_RDOSession()

            cdoSession.LogonExchangeMailbox(txtAlias.Text, txtServer.Text)
            'nomail:=True, _
            'profileinfo:=txtServer.Text & vbLf & txtAlias.Text)


            MessageBox.Show("MAPI test completed successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmdOK.Enabled = True
        Catch ex As Exception
            MessageBox.Show("MAPI test failed with the following reason:" & vbCrLf & _
                    "Error Message: " & ex.Message & vbCrLf & _
                    "Error Number: " & Err.Number, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmdOK.Enabled = False
        End Try
    End Sub

    Public Function MAPISetup() As Boolean
        txtAlias.Text = oUI.ReadRegistry("MAPIAlias", "")
        txtServer.Text = oUI.ReadRegistry("MAPIServer", "")
        MAPIType = oUI.ReadRegistry("MAPIType", "Single")

        Select Case MAPIType
            Case "Single"
                cmbMapiType.Text = "Stand-Alone Outlook"
                grpExchange.Visible = False
                Me.Height = 112
                cmdOK.Enabled = True
            Case "Server"
                cmbMapiType.Text = "Exchange Server"
                grpExchange.Visible = True
                Me.Height = 224
                cmdOK.Enabled = False
            Case "ExchangeWDS"
                cmbMapiType.Text = "Exchange Web Service (2007 or later)"
                grpExchangeWDS.Visible = True
                Me.Height = 390
                cmdOK.Enabled = True
        End Select

        Me.ShowDialog()

        If isCancel = True Then
            isCancel = False
            Return False
            Exit Function
        End If

        oUI.SaveRegistry("MAPIAlias", txtAlias.Text, , , True)
        oUI.SaveRegistry("MAPIServer", txtServer.Text, , , True)
        oUI.SaveRegistry("MAPIType", MAPIType, , , True)
        oUI.SaveRegistry("ExchangeWDSUrl", txtServerUrl.Text, , , True)
        oUI.SaveRegistry("ExchangeWDSUserID", txtEWSUserID.Text, , , True)
        oUI.SaveRegistry("ExchangeWDSPassword", txtEWSPassword.Text, True, , True)
        oUI.SaveRegistry("ExchangeWDSSender", txtSenderName.Text, , , True)
        oUI.SaveRegistry("exchangeWDSSenderAddress", txtSenderAddress.Text, , , True)
        oUI.SaveRegistry("ExchangeAutoDiscoverEmailAddress", txtAutodiscover.Text, , , True)
        oUI.SaveRegistry("ExchangeAutoDiscover", Convert.ToInt16(chkAutodiscover.Checked))

        Return True

        Me.Close()
    End Function

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        isCancel = True
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Me.Close()
    End Sub

    Private Sub txtServerUrl_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtServerUrl.LostFocus, txtSenderName.LostFocus, txtSenderAddress.LostFocus, txtAutodiscover.LostFocus
        If txtServerUrl.Text.Contains("http") = False And txtServerUrl.Text <> "" Then
            txtServerUrl.Text = "http://" & txtServerUrl.Text & "/ews/exchange.asmx"
        End If

        If txtSenderName.Text <> "" And txtSenderAddress.Text <> "" And txtServerUrl.Text <> "" Then
            cmdOK.Enabled = True
        Else
            cmdOK.Enabled = False
        End If
    End Sub

    Private Sub txtServerUrl_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtServerUrl.TextChanged, txtSenderName.TextChanged, txtSenderAddress.TextChanged, txtAutodiscover.TextChanged
        If txtSenderName.Text <> "" And txtSenderAddress.Text <> "" And txtServerUrl.Text <> "" Then
            cmdOK.Enabled = True
        Else
            cmdOK.Enabled = False
        End If
    End Sub
    Private Function validateAutoDiscoverRedirection()
        Return True
    End Function
    Private Sub btnAutoDiscover_Click(sender As Object, e As EventArgs) Handles btnAutoDiscover.Click
        AppStatus(True)
        Dim service As Microsoft.Exchange.WebServices.Data.ExchangeService = New Microsoft.Exchange.WebServices.Data.ExchangeService(Microsoft.Exchange.WebServices.Data.ExchangeVersion.Exchange2007_SP1)

        Dim username, domain As String
        username = txtEWSUserID.Text


        If username.Contains("\") Then
            domain = txtEWSUserID.Text.Split("\")(0)
            username = username.Split("\")(1)
        End If

        If domain = "" Then domain = Environment.UserDomainName

        service.Credentials = New System.Net.NetworkCredential(username, txtEWSPassword.Text, domain)

        Try
            service.AutodiscoverUrl(txtAutodiscover.Text, AddressOf validateAutoDiscoverRedirection)

            txtServerUrl.Text = service.Url.ToString
            txtServerUrl.SelectAll()
            txtServerUrl.Focus()
            txtSenderAddress.Text = txtAutodiscover.Text
        Catch ex As Exception
            MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        AppStatus(False)
    End Sub

    Private Sub chkAutodiscover_CheckedChanged(sender As Object, e As EventArgs) Handles chkAutodiscover.CheckedChanged
        txtAutodiscover.Enabled = chkAutodiscover.Checked
        btnAutoDiscover.Enabled = chkAutodiscover.Checked
    End Sub
End Class
