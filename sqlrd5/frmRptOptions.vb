#If CRYSTAL_VER = 8 Then
imports My.Crystal85
#ElseIf CRYSTAL_VER = 9 Then
imports My.Crystal9 
#ElseIf CRYSTAL_VER = 10 Then
imports My.Crystal10 
#ElseIf CRYSTAL_VER = 11 Or crystal_ver = 11.5 Or CRYSTAL_VER >= 11.6 Then
Imports My.Crystal11
#End If

Public Class frmRptOptions
    Inherits System.Windows.Forms.UserControl
    Dim oData As New clsMarsData
    Dim UserCancel As Boolean = False
    Dim oField As TextBox = txtOwnerPassword
    Dim xFormat As String = ""
    Dim ep As New ErrorProvider
    Public Dynamic As Boolean = False
    Dim eventBased As Boolean = False
    Dim fields As Hashtable
    Public m_ReportID As Integer = 0
    Public m_DestinationID As Integer = 0
    Dim formLoaded As Boolean = False

#Region "declarations"
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents btnPDF As DevComponents.DotNetBar.ButtonX
    Friend WithEvents grpRecordStyle As System.Windows.Forms.Panel
    Friend WithEvents chkrcDateformat As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkrcNoFormats As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents grpAppendFile As System.Windows.Forms.GroupBox
    Friend WithEvents chkAppendFile As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents grpTiff As System.Windows.Forms.Panel
    Friend WithEvents chkNative As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label20 As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblFrom As DevComponents.DotNetBar.LabelX
    Friend WithEvents numTo As System.Windows.Forms.NumericUpDown
    Friend WithEvents numFrom As System.Windows.Forms.NumericUpDown
    Friend WithEvents rbPageRange As System.Windows.Forms.RadioButton
    Friend WithEvents rbAllPages As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents chkGrayScale As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents cmbColor As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox11 As System.Windows.Forms.GroupBox
    Friend WithEvents rbCLASSF196 As System.Windows.Forms.RadioButton
    Friend WithEvents rbCLASSF As System.Windows.Forms.RadioButton
    Friend WithEvents rbMAC As System.Windows.Forms.RadioButton
    Friend WithEvents rbLZW As System.Windows.Forms.RadioButton
    Friend WithEvents rbFAX4 As System.Windows.Forms.RadioButton
    Friend WithEvents rbFAX3 As System.Windows.Forms.RadioButton
    Friend WithEvents rbRLE As System.Windows.Forms.RadioButton
    Friend WithEvents rbNone As System.Windows.Forms.RadioButton
    Friend WithEvents grpXML As System.Windows.Forms.Panel
    Friend WithEvents chkXMLWriteHierachy As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents GroupBox13 As System.Windows.Forms.GroupBox
    Friend WithEvents optXMLIncludeSchema As System.Windows.Forms.RadioButton
    Friend WithEvents optXMLDiffGram As System.Windows.Forms.RadioButton
    Friend WithEvents optXMLIgnoreSchema As System.Windows.Forms.RadioButton
    Friend WithEvents Label24 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtXMLTableName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents grpWord As System.Windows.Forms.Panel
    Friend WithEvents chkInsertPageBreaksWord As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents SuperTabControl2 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem4 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents stabExcel As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents ExcelGeneralSuperTab As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem6 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents stabOptions As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents formatOptionsPanel As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents fileSummaryPanel As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents stabFileSummary As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents pagerangeTabPanel As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
#End Region
    Public eventID As Integer = 99999

    ' Public m_Format As String
    Friend WithEvents cmbFormat As System.Windows.Forms.ComboBox
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents grpCrystal As System.Windows.Forms.Panel
    Friend WithEvents chkMakeCrystalReadOnly As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents dtPDFExpiry As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkPDFExpiry As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents grpCustom As System.Windows.Forms.Panel
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtrenderextension As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents optAdvancedHTML As System.Windows.Forms.RadioButton
    Friend WithEvents optBasicHTML As System.Windows.Forms.RadioButton
    Friend WithEvents cmbCSVEngine As System.Windows.Forms.ComboBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents chkIgnoreHeaders As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents tbMiraplacid As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtdpi As System.Windows.Forms.NumericUpDown
    Friend WithEvents grpMiraplacid As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtwhitespacesize As System.Windows.Forms.NumericUpDown
    Friend WithEvents cmbformattingstyle As System.Windows.Forms.ComboBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents cmbcharacterset As System.Windows.Forms.ComboBox
    Friend WithEvents cmbendoflinestyle As System.Windows.Forms.ComboBox
    Friend WithEvents chkInsertBreaks As System.Windows.Forms.CheckBox
    Friend WithEvents chkUseDriverDefaults As System.Windows.Forms.CheckBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txtTimePerPage As System.Windows.Forms.NumericUpDown
    Friend WithEvents cmbTextEngine As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Public m_ReportType As String
    Public is_Smart As Boolean = False
    Public Property m_isQuery As Boolean

    Public ReadOnly Property m_format As String
        Get
            Return cmbFormat.Text.Split("(")(0).Trim
        End Get
    End Property

    Public Property m_eventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = value
        End Set
    End Property

    Public Property selectedFormat As String
        Get
            Try
                Return cmbFormat.Text
            Catch
                cmbFormat.Items.Add("")
                cmbFormat.Text = ""
                Return ""
            End Try
        End Get
        Set(ByVal value As String)

            If value = "" Then Return


            For Each it As String In cmbFormat.Items
                If String.Compare(it, value, True) = 0 Then
                    cmbFormat.SelectedItem = it
                End If
            Next

            If cmbFormat.Text = "" Then cmbFormat.Text = value
        End Set
    End Property
    Private ReadOnly Property m_CSVChar() As String
        Get
            If Me.optCharacter.Checked = True Then
                Return Me.txtCharacter.Text
            Else
                Return "tab"
            End If
        End Get
    End Property
    Private ReadOnly Property m_preserveLinks() As Boolean
        Get
            If Me.optAdvancedHTML.Checked = True Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    Public Property m_destinationType As String
    '//text options


    Private Property m_text_formattingstyle() As String
        Get
            Select Case cmbformattingstyle.Text.ToLower
                Case "formatted text"
                    Return "Formatted"
                Case "text with layout"
                    Return "TWL"
                Case "xml"
                    Return "XML"
                Case "rss"
                    Return "RSS"
                Case "plain text"
                    Return "Plain"
                Case Else
                    Return "Formatted"
            End Select
        End Get
        Set(ByVal value As String)
            Select Case value
                Case "Formatted"
                    cmbformattingstyle.Text = "Formatted Text"
                Case "TWL"
                    cmbformattingstyle.Text = "Text with Layout"
                Case "Plain"
                    cmbformattingstyle.Text = "Plain Text"
                Case Else
                    cmbformattingstyle.Text = value
            End Select
        End Set
    End Property

    Private Property m_text_characterset() As Integer
        Get
            Select Case cmbcharacterset.Text.ToLower
                Case "unicode"
                    Return 5
                Case "default ansi"
                    Return 1250
                Case "default oem"
                    Return 437
                Case "default mac"
                    Return 850
            End Select
        End Get
        Set(ByVal value As Integer)
            Select Case value
                Case 5
                    cmbcharacterset.Text = "UNICODE"
                Case 1250
                    cmbcharacterset.Text = "DEFAULT ANSI"
                Case 437
                    cmbcharacterset.Text = "DEFAULT OEM"
                Case 850
                    cmbcharacterset.Text = "DEFAULT MAC"
            End Select
        End Set
    End Property

    Private Property m_text_eolstyle() As Integer
        Get
            If cmbendoflinestyle.Text.ToLower = "windows (\r\n)" Then
                Return 0
            Else
                Return 1
            End If
        End Get
        Set(ByVal value As Integer)
            If value = 0 Then
                cmbendoflinestyle.Text = "Windows (\r\n)"
            Else
                cmbendoflinestyle.Text = "Unix (\n)"
            End If
        End Set
    End Property
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents grpPDF As System.Windows.Forms.Panel
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txtWatermark As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents grpHTML As System.Windows.Forms.Panel
    Friend WithEvents txtOwnerPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtUserPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkPrint As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkCopy As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkEdit As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkNotes As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkFill As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkAccess As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents tip As System.Windows.Forms.ToolTip
    Friend WithEvents chkAssemble As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkFullRes As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSpell As System.Windows.Forms.MenuItem
    Friend WithEvents Speller As Keyoti.RapidSpell.RapidSpellDialog
    Friend WithEvents grpPageRange As System.Windows.Forms.GroupBox
    Friend WithEvents optAllPages As System.Windows.Forms.RadioButton
    Friend WithEvents optPageRange As System.Windows.Forms.RadioButton
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtPageFrom As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtPageTo As System.Windows.Forms.NumericUpDown
    Friend WithEvents grpExcel As System.Windows.Forms.Panel
    Friend WithEvents grpCSV As System.Windows.Forms.Panel
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents optCharacter As System.Windows.Forms.RadioButton
    Friend WithEvents txtCharacter As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents optTab As System.Windows.Forms.RadioButton
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDelimiter As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents grpODBC As System.Windows.Forms.Panel
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents cmdConnect As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtTableName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents grpText As System.Windows.Forms.Panel
    Friend WithEvents chkPDFSecurity As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Label10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtWorksheetName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtInfoCreated As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtInfoTitle As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtInfoAuthor As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtInfoSubject As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtInfoKeywords As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label13 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label14 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label15 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtInfoProducer As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label16 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
    Friend WithEvents chkExcelPass As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtExcelPass As DevComponents.DotNetBar.Controls.TextBoxX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.grpPDF = New System.Windows.Forms.Panel()
        Me.SuperTabControl2 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.btnPDF = New DevComponents.DotNetBar.ButtonX()
        Me.chkPDFSecurity = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.txtWatermark = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkPrint = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkCopy = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkEdit = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkNotes = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkFill = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkAccess = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkAssemble = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkFullRes = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtOwnerPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtUserPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.SuperTabItem4 = New DevComponents.DotNetBar.SuperTabItem()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.MenuItem13 = New System.Windows.Forms.MenuItem()
        Me.mnuSpell = New System.Windows.Forms.MenuItem()
        Me.grpRecordStyle = New System.Windows.Forms.Panel()
        Me.chkrcDateformat = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkrcNoFormats = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.ofd = New System.Windows.Forms.OpenFileDialog()
        Me.grpHTML = New System.Windows.Forms.Panel()
        Me.optAdvancedHTML = New System.Windows.Forms.RadioButton()
        Me.optBasicHTML = New System.Windows.Forms.RadioButton()
        Me.tip = New System.Windows.Forms.ToolTip(Me.components)
        Me.grpExcel = New System.Windows.Forms.Panel()
        Me.stabExcel = New DevComponents.DotNetBar.SuperTabControl()
        Me.ExcelGeneralSuperTab = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.GroupBox10 = New System.Windows.Forms.GroupBox()
        Me.chkExcelPass = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtExcelPass = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtWorksheetName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label10 = New DevComponents.DotNetBar.LabelX()
        Me.SuperTabItem6 = New DevComponents.DotNetBar.SuperTabItem()
        Me.grpCSV = New System.Windows.Forms.Panel()
        Me.chkIgnoreHeaders = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.cmbCSVEngine = New System.Windows.Forms.ComboBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.txtCharacter = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.optCharacter = New System.Windows.Forms.RadioButton()
        Me.optTab = New System.Windows.Forms.RadioButton()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.txtDelimiter = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.grpText = New System.Windows.Forms.Panel()
        Me.tbMiraplacid = New System.Windows.Forms.TableLayoutPanel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtdpi = New System.Windows.Forms.NumericUpDown()
        Me.grpMiraplacid = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtwhitespacesize = New System.Windows.Forms.NumericUpDown()
        Me.cmbformattingstyle = New System.Windows.Forms.ComboBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.cmbcharacterset = New System.Windows.Forms.ComboBox()
        Me.cmbendoflinestyle = New System.Windows.Forms.ComboBox()
        Me.chkInsertBreaks = New System.Windows.Forms.CheckBox()
        Me.chkUseDriverDefaults = New System.Windows.Forms.CheckBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtTimePerPage = New System.Windows.Forms.NumericUpDown()
        Me.cmbTextEngine = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.grpWord = New System.Windows.Forms.Panel()
        Me.chkInsertPageBreaksWord = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.grpXML = New System.Windows.Forms.Panel()
        Me.txtXMLTableName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label24 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox13 = New System.Windows.Forms.GroupBox()
        Me.optXMLDiffGram = New System.Windows.Forms.RadioButton()
        Me.optXMLIgnoreSchema = New System.Windows.Forms.RadioButton()
        Me.optXMLIncludeSchema = New System.Windows.Forms.RadioButton()
        Me.chkXMLWriteHierachy = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.grpODBC = New System.Windows.Forms.Panel()
        Me.txtTableName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label7 = New DevComponents.DotNetBar.LabelX()
        Me.cmdConnect = New DevComponents.DotNetBar.ButtonX()
        Me.UcDSN = New sqlrd.ucDSN()
        Me.grpTiff = New System.Windows.Forms.Panel()
        Me.chkNative = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label20 = New DevComponents.DotNetBar.LabelX()
        Me.lblFrom = New DevComponents.DotNetBar.LabelX()
        Me.numTo = New System.Windows.Forms.NumericUpDown()
        Me.numFrom = New System.Windows.Forms.NumericUpDown()
        Me.rbPageRange = New System.Windows.Forms.RadioButton()
        Me.rbAllPages = New System.Windows.Forms.RadioButton()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.chkGrayScale = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.cmbColor = New System.Windows.Forms.ComboBox()
        Me.GroupBox11 = New System.Windows.Forms.GroupBox()
        Me.rbCLASSF196 = New System.Windows.Forms.RadioButton()
        Me.rbCLASSF = New System.Windows.Forms.RadioButton()
        Me.rbMAC = New System.Windows.Forms.RadioButton()
        Me.rbLZW = New System.Windows.Forms.RadioButton()
        Me.rbFAX4 = New System.Windows.Forms.RadioButton()
        Me.rbFAX3 = New System.Windows.Forms.RadioButton()
        Me.rbRLE = New System.Windows.Forms.RadioButton()
        Me.rbNone = New System.Windows.Forms.RadioButton()
        Me.grpAppendFile = New System.Windows.Forms.GroupBox()
        Me.chkAppendFile = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.grpPageRange = New System.Windows.Forms.GroupBox()
        Me.txtPageFrom = New System.Windows.Forms.NumericUpDown()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.optAllPages = New System.Windows.Forms.RadioButton()
        Me.optPageRange = New System.Windows.Forms.RadioButton()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.txtPageTo = New System.Windows.Forms.NumericUpDown()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.dtPDFExpiry = New System.Windows.Forms.DateTimePicker()
        Me.chkPDFExpiry = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtInfoCreated = New System.Windows.Forms.DateTimePicker()
        Me.Label11 = New DevComponents.DotNetBar.LabelX()
        Me.txtInfoTitle = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtInfoAuthor = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtInfoSubject = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtInfoKeywords = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label12 = New DevComponents.DotNetBar.LabelX()
        Me.Label13 = New DevComponents.DotNetBar.LabelX()
        Me.Label14 = New DevComponents.DotNetBar.LabelX()
        Me.Label15 = New DevComponents.DotNetBar.LabelX()
        Me.txtInfoProducer = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label16 = New DevComponents.DotNetBar.LabelX()
        Me.Speller = New Keyoti.RapidSpell.RapidSpellDialog(Me.components)
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.stabOptions = New DevComponents.DotNetBar.SuperTabControl()
        Me.formatOptionsPanel = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.grpCustom = New System.Windows.Forms.Panel()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtrenderextension = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.grpCrystal = New System.Windows.Forms.Panel()
        Me.chkMakeCrystalReadOnly = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.fileSummaryPanel = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.stabFileSummary = New DevComponents.DotNetBar.SuperTabItem()
        Me.pagerangeTabPanel = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.cmbFormat = New System.Windows.Forms.ComboBox()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.grpPDF.SuspendLayout()
        CType(Me.SuperTabControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl2.SuspendLayout()
        Me.SuperTabControlPanel4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.grpRecordStyle.SuspendLayout()
        Me.grpHTML.SuspendLayout()
        Me.grpExcel.SuspendLayout()
        CType(Me.stabExcel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabExcel.SuspendLayout()
        Me.ExcelGeneralSuperTab.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        Me.grpCSV.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.grpText.SuspendLayout()
        Me.tbMiraplacid.SuspendLayout()
        CType(Me.txtdpi, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpMiraplacid.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.txtwhitespacesize, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTimePerPage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpWord.SuspendLayout()
        Me.grpXML.SuspendLayout()
        Me.GroupBox13.SuspendLayout()
        Me.grpODBC.SuspendLayout()
        Me.grpTiff.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.numTo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numFrom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.grpAppendFile.SuspendLayout()
        Me.grpPageRange.SuspendLayout()
        CType(Me.txtPageFrom, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPageTo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.stabOptions, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabOptions.SuspendLayout()
        Me.formatOptionsPanel.SuspendLayout()
        Me.grpCustom.SuspendLayout()
        Me.grpCrystal.SuspendLayout()
        Me.fileSummaryPanel.SuspendLayout()
        Me.pagerangeTabPanel.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpPDF
        '
        Me.grpPDF.BackColor = System.Drawing.Color.Transparent
        Me.grpPDF.Controls.Add(Me.SuperTabControl2)
        Me.grpPDF.Location = New System.Drawing.Point(11, 44)
        Me.grpPDF.Name = "grpPDF"
        Me.grpPDF.Size = New System.Drawing.Size(456, 320)
        Me.grpPDF.TabIndex = 0
        Me.grpPDF.Text = "PDF Options"
        Me.grpPDF.Visible = False
        '
        'SuperTabControl2
        '
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl2.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl2.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl2.ControlBox.Name = ""
        Me.SuperTabControl2.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl2.ControlBox.MenuBox, Me.SuperTabControl2.ControlBox.CloseBox})
        Me.SuperTabControl2.Controls.Add(Me.SuperTabControlPanel4)
        Me.SuperTabControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControl2.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControl2.Name = "SuperTabControl2"
        Me.SuperTabControl2.ReorderTabsEnabled = True
        Me.SuperTabControl2.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl2.SelectedTabIndex = 0
        Me.SuperTabControl2.Size = New System.Drawing.Size(456, 320)
        Me.SuperTabControl2.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTabControl2.TabIndex = 9
        Me.SuperTabControl2.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem4})
        Me.SuperTabControl2.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.WinMediaPlayer12
        Me.SuperTabControl2.Text = "stabPDF"
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.btnPDF)
        Me.SuperTabControlPanel4.Controls.Add(Me.chkPDFSecurity)
        Me.SuperTabControlPanel4.Controls.Add(Me.Label2)
        Me.SuperTabControlPanel4.Controls.Add(Me.txtWatermark)
        Me.SuperTabControlPanel4.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(0, 24)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(456, 296)
        Me.SuperTabControlPanel4.TabIndex = 1
        Me.SuperTabControlPanel4.TabItem = Me.SuperTabItem4
        '
        'btnPDF
        '
        Me.btnPDF.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnPDF.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnPDF.Location = New System.Drawing.Point(312, 227)
        Me.btnPDF.Name = "btnPDF"
        Me.btnPDF.Size = New System.Drawing.Size(43, 23)
        Me.btnPDF.TabIndex = 17
        Me.btnPDF.Text = "..."
        '
        'chkPDFSecurity
        '
        '
        '
        '
        Me.chkPDFSecurity.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkPDFSecurity.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkPDFSecurity.Location = New System.Drawing.Point(3, 4)
        Me.chkPDFSecurity.Name = "chkPDFSecurity"
        Me.chkPDFSecurity.Size = New System.Drawing.Size(336, 24)
        Me.chkPDFSecurity.TabIndex = 0
        Me.chkPDFSecurity.Text = "Enable PDF Options"
        Me.tip.SetToolTip(Me.chkPDFSecurity, "Output can only be read using Adobe Acrobat 5 and above")
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(13, 213)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(256, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Include the following watermark/stamp"
        '
        'txtWatermark
        '
        '
        '
        '
        Me.txtWatermark.Border.Class = "TextBoxBorder"
        Me.txtWatermark.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtWatermark.Location = New System.Drawing.Point(13, 229)
        Me.txtWatermark.Name = "txtWatermark"
        Me.txtWatermark.Size = New System.Drawing.Size(293, 21)
        Me.txtWatermark.TabIndex = 16
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkPrint)
        Me.GroupBox1.Controls.Add(Me.chkCopy)
        Me.GroupBox1.Controls.Add(Me.chkEdit)
        Me.GroupBox1.Controls.Add(Me.chkNotes)
        Me.GroupBox1.Controls.Add(Me.chkFill)
        Me.GroupBox1.Controls.Add(Me.chkAccess)
        Me.GroupBox1.Controls.Add(Me.chkAssemble)
        Me.GroupBox1.Controls.Add(Me.chkFullRes)
        Me.GroupBox1.Controls.Add(Me.txtOwnerPassword)
        Me.GroupBox1.Controls.Add(Me.txtUserPassword)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.GroupBox9)
        Me.GroupBox1.Enabled = False
        Me.GroupBox1.Location = New System.Drawing.Point(3, 34)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(352, 176)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "File Permissions"
        '
        'chkPrint
        '
        '
        '
        '
        Me.chkPrint.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkPrint.Checked = True
        Me.chkPrint.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPrint.CheckValue = "Y"
        Me.chkPrint.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkPrint.Location = New System.Drawing.Point(8, 72)
        Me.chkPrint.Name = "chkPrint"
        Me.chkPrint.Size = New System.Drawing.Size(88, 24)
        Me.chkPrint.TabIndex = 2
        Me.chkPrint.Text = "Can Print"
        Me.tip.SetToolTip(Me.chkPrint, "Allow the user to print the document")
        '
        'chkCopy
        '
        '
        '
        '
        Me.chkCopy.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkCopy.Checked = True
        Me.chkCopy.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkCopy.CheckValue = "Y"
        Me.chkCopy.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkCopy.Location = New System.Drawing.Point(8, 96)
        Me.chkCopy.Name = "chkCopy"
        Me.chkCopy.Size = New System.Drawing.Size(88, 24)
        Me.chkCopy.TabIndex = 3
        Me.chkCopy.Text = "Can Copy"
        Me.tip.SetToolTip(Me.chkCopy, "Allows the user to copy text and images from the document")
        '
        'chkEdit
        '
        '
        '
        '
        Me.chkEdit.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkEdit.Checked = True
        Me.chkEdit.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkEdit.CheckValue = "Y"
        Me.chkEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkEdit.Location = New System.Drawing.Point(8, 120)
        Me.chkEdit.Name = "chkEdit"
        Me.chkEdit.Size = New System.Drawing.Size(80, 24)
        Me.chkEdit.TabIndex = 4
        Me.chkEdit.Text = "Can Edit"
        Me.tip.SetToolTip(Me.chkEdit, "Allows the user to edit the document")
        '
        'chkNotes
        '
        '
        '
        '
        Me.chkNotes.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkNotes.Checked = True
        Me.chkNotes.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkNotes.CheckValue = "Y"
        Me.chkNotes.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkNotes.Location = New System.Drawing.Point(168, 120)
        Me.chkNotes.Name = "chkNotes"
        Me.chkNotes.Size = New System.Drawing.Size(104, 24)
        Me.chkNotes.TabIndex = 8
        Me.chkNotes.Text = "Can Add Notes"
        Me.tip.SetToolTip(Me.chkNotes, "Allows the user to add annotations")
        '
        'chkFill
        '
        '
        '
        '
        Me.chkFill.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkFill.Checked = True
        Me.chkFill.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFill.CheckValue = "Y"
        Me.chkFill.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkFill.Location = New System.Drawing.Point(8, 144)
        Me.chkFill.Name = "chkFill"
        Me.chkFill.Size = New System.Drawing.Size(96, 24)
        Me.chkFill.TabIndex = 5
        Me.chkFill.Text = "Can Fill Fields"
        Me.tip.SetToolTip(Me.chkFill, "Allows the user to fill in form fields")
        '
        'chkAccess
        '
        '
        '
        '
        Me.chkAccess.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAccess.Checked = True
        Me.chkAccess.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAccess.CheckValue = "Y"
        Me.chkAccess.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAccess.Location = New System.Drawing.Point(168, 144)
        Me.chkAccess.Name = "chkAccess"
        Me.chkAccess.Size = New System.Drawing.Size(176, 24)
        Me.chkAccess.TabIndex = 9
        Me.chkAccess.Text = "Can copy accessibility options"
        Me.tip.SetToolTip(Me.chkAccess, "Allow user copying for use with accessibility features")
        '
        'chkAssemble
        '
        '
        '
        '
        Me.chkAssemble.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAssemble.Checked = True
        Me.chkAssemble.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAssemble.CheckValue = "Y"
        Me.chkAssemble.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAssemble.Location = New System.Drawing.Point(168, 72)
        Me.chkAssemble.Name = "chkAssemble"
        Me.chkAssemble.Size = New System.Drawing.Size(96, 24)
        Me.chkAssemble.TabIndex = 6
        Me.chkAssemble.Text = "Can Assemble"
        Me.tip.SetToolTip(Me.chkAssemble, "Allow the user to assemble the document")
        '
        'chkFullRes
        '
        '
        '
        '
        Me.chkFullRes.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkFullRes.Checked = True
        Me.chkFullRes.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFullRes.CheckValue = "Y"
        Me.chkFullRes.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkFullRes.Location = New System.Drawing.Point(168, 96)
        Me.chkFullRes.Name = "chkFullRes"
        Me.chkFullRes.Size = New System.Drawing.Size(152, 24)
        Me.chkFullRes.TabIndex = 7
        Me.chkFullRes.Text = "Can print in full resolution"
        Me.tip.SetToolTip(Me.chkFullRes, "Allow the user print in full resolution")
        '
        'txtOwnerPassword
        '
        '
        '
        '
        Me.txtOwnerPassword.Border.Class = "TextBoxBorder"
        Me.txtOwnerPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtOwnerPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtOwnerPassword.Location = New System.Drawing.Point(8, 40)
        Me.txtOwnerPassword.Name = "txtOwnerPassword"
        Me.txtOwnerPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtOwnerPassword.Size = New System.Drawing.Size(152, 21)
        Me.txtOwnerPassword.TabIndex = 0
        '
        'txtUserPassword
        '
        '
        '
        '
        Me.txtUserPassword.Border.Class = "TextBoxBorder"
        Me.txtUserPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUserPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtUserPassword.Location = New System.Drawing.Point(168, 40)
        Me.txtUserPassword.Name = "txtUserPassword"
        Me.txtUserPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtUserPassword.Size = New System.Drawing.Size(152, 21)
        Me.txtUserPassword.TabIndex = 1
        '
        'Label3
        '
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(168, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "User Password"
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(112, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Owner Password"
        '
        'GroupBox9
        '
        Me.GroupBox9.Location = New System.Drawing.Point(8, 64)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(336, 8)
        Me.GroupBox9.TabIndex = 2
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Tag = "3dline"
        '
        'SuperTabItem4
        '
        Me.SuperTabItem4.AttachedControl = Me.SuperTabControlPanel4
        Me.SuperTabItem4.GlobalItem = False
        Me.SuperTabItem4.Name = "SuperTabItem4"
        Me.SuperTabItem4.Text = "PDF Security"
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1, Me.MenuItem13, Me.mnuSpell})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 10
        Me.MenuItem13.Text = "-"
        '
        'mnuSpell
        '
        Me.mnuSpell.Index = 11
        Me.mnuSpell.Text = "Spell Check"
        '
        'grpRecordStyle
        '
        Me.grpRecordStyle.Controls.Add(Me.chkrcDateformat)
        Me.grpRecordStyle.Controls.Add(Me.chkrcNoFormats)
        Me.grpRecordStyle.Location = New System.Drawing.Point(11, 44)
        Me.grpRecordStyle.Name = "grpRecordStyle"
        Me.grpRecordStyle.Size = New System.Drawing.Size(456, 70)
        Me.grpRecordStyle.TabIndex = 3
        Me.grpRecordStyle.Text = "Record Style Options"
        Me.grpRecordStyle.Visible = False
        '
        'chkrcDateformat
        '
        Me.chkrcDateformat.AutoSize = True
        '
        '
        '
        Me.chkrcDateformat.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkrcDateformat.Location = New System.Drawing.Point(20, 43)
        Me.chkrcDateformat.Name = "chkrcDateformat"
        Me.chkrcDateformat.Size = New System.Drawing.Size(174, 16)
        Me.chkrcDateformat.TabIndex = 1
        Me.chkrcDateformat.Text = "Same date formats as in report"
        '
        'chkrcNoFormats
        '
        Me.chkrcNoFormats.AutoSize = True
        '
        '
        '
        Me.chkrcNoFormats.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkrcNoFormats.Location = New System.Drawing.Point(20, 20)
        Me.chkrcNoFormats.Name = "chkrcNoFormats"
        Me.chkrcNoFormats.Size = New System.Drawing.Size(190, 16)
        Me.chkrcNoFormats.TabIndex = 0
        Me.chkrcNoFormats.Text = "Same number formats as in report"
        '
        'ofd
        '
        Me.ofd.Filter = "JPEG Files|*.jpg|GIF Files|*.gif|Windows Bitmap|*.bmp|All Files|*.*"
        Me.ofd.Title = "Select image file"
        '
        'grpHTML
        '
        Me.grpHTML.BackColor = System.Drawing.Color.Transparent
        Me.grpHTML.Controls.Add(Me.optAdvancedHTML)
        Me.grpHTML.Controls.Add(Me.optBasicHTML)
        Me.grpHTML.Location = New System.Drawing.Point(11, 44)
        Me.grpHTML.Name = "grpHTML"
        Me.grpHTML.Size = New System.Drawing.Size(456, 320)
        Me.grpHTML.TabIndex = 1
        Me.grpHTML.Text = "HTML Options"
        Me.grpHTML.Visible = False
        '
        'optAdvancedHTML
        '
        Me.optAdvancedHTML.AutoSize = True
        Me.optAdvancedHTML.Checked = True
        Me.optAdvancedHTML.Location = New System.Drawing.Point(11, 7)
        Me.optAdvancedHTML.Name = "optAdvancedHTML"
        Me.optAdvancedHTML.Size = New System.Drawing.Size(171, 17)
        Me.SuperTooltip1.SetSuperTooltip(Me.optAdvancedHTML, New DevComponents.DotNetBar.SuperTooltipInfo("Advanced HTML Rendering", "", "This HTML rendering method produces outputs that are closer in representation to " & _
            "the original report and also preserves the links that you have in the report ", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, True, True, New System.Drawing.Size(250, 124)))
        Me.optAdvancedHTML.TabIndex = 2
        Me.optAdvancedHTML.TabStop = True
        Me.optAdvancedHTML.Text = "Use advanced HTML rendering"
        Me.optAdvancedHTML.UseVisualStyleBackColor = True
        '
        'optBasicHTML
        '
        Me.optBasicHTML.AutoSize = True
        Me.optBasicHTML.Location = New System.Drawing.Point(11, 30)
        Me.optBasicHTML.Name = "optBasicHTML"
        Me.optBasicHTML.Size = New System.Drawing.Size(148, 17)
        Me.optBasicHTML.TabIndex = 1
        Me.optBasicHTML.Text = "Use basic HTML rendering"
        Me.optBasicHTML.UseVisualStyleBackColor = True
        '
        'grpExcel
        '
        Me.grpExcel.BackColor = System.Drawing.Color.Transparent
        Me.grpExcel.Controls.Add(Me.stabExcel)
        Me.grpExcel.Location = New System.Drawing.Point(11, 44)
        Me.grpExcel.Name = "grpExcel"
        Me.grpExcel.Size = New System.Drawing.Size(454, 314)
        Me.grpExcel.TabIndex = 2
        Me.grpExcel.Text = "Excel Options (requires MS Office)"
        Me.grpExcel.Visible = False
        '
        'stabExcel
        '
        '
        '
        '
        '
        '
        '
        Me.stabExcel.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabExcel.ControlBox.MenuBox.Name = ""
        Me.stabExcel.ControlBox.Name = ""
        Me.stabExcel.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabExcel.ControlBox.MenuBox, Me.stabExcel.ControlBox.CloseBox})
        Me.stabExcel.Controls.Add(Me.ExcelGeneralSuperTab)
        Me.stabExcel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.stabExcel.Location = New System.Drawing.Point(0, 0)
        Me.stabExcel.Name = "stabExcel"
        Me.stabExcel.ReorderTabsEnabled = True
        Me.stabExcel.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabExcel.SelectedTabIndex = 0
        Me.stabExcel.Size = New System.Drawing.Size(454, 314)
        Me.stabExcel.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.stabExcel.TabIndex = 9
        Me.stabExcel.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem6})
        Me.stabExcel.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.WinMediaPlayer12
        Me.stabExcel.Text = "stabExcel"
        '
        'ExcelGeneralSuperTab
        '
        Me.ExcelGeneralSuperTab.Controls.Add(Me.GroupBox10)
        Me.ExcelGeneralSuperTab.Controls.Add(Me.txtWorksheetName)
        Me.ExcelGeneralSuperTab.Controls.Add(Me.Label10)
        Me.ExcelGeneralSuperTab.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ExcelGeneralSuperTab.Location = New System.Drawing.Point(0, 24)
        Me.ExcelGeneralSuperTab.Name = "ExcelGeneralSuperTab"
        Me.ExcelGeneralSuperTab.Size = New System.Drawing.Size(454, 290)
        Me.ExcelGeneralSuperTab.TabIndex = 1
        Me.ExcelGeneralSuperTab.TabItem = Me.SuperTabItem6
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.chkExcelPass)
        Me.GroupBox10.Controls.Add(Me.txtExcelPass)
        Me.GroupBox10.Location = New System.Drawing.Point(5, 57)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(332, 80)
        Me.GroupBox10.TabIndex = 5
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "Workbook Password"
        '
        'chkExcelPass
        '
        '
        '
        '
        Me.chkExcelPass.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkExcelPass.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkExcelPass.Location = New System.Drawing.Point(8, 24)
        Me.chkExcelPass.Name = "chkExcelPass"
        Me.chkExcelPass.Size = New System.Drawing.Size(184, 24)
        Me.chkExcelPass.TabIndex = 0
        Me.chkExcelPass.Text = "Password protect  the workbook"
        '
        'txtExcelPass
        '
        '
        '
        '
        Me.txtExcelPass.Border.Class = "TextBoxBorder"
        Me.txtExcelPass.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtExcelPass.Enabled = False
        Me.txtExcelPass.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtExcelPass.Location = New System.Drawing.Point(8, 48)
        Me.txtExcelPass.Name = "txtExcelPass"
        Me.txtExcelPass.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtExcelPass.Size = New System.Drawing.Size(311, 21)
        Me.txtExcelPass.TabIndex = 3
        '
        'txtWorksheetName
        '
        '
        '
        '
        Me.txtWorksheetName.Border.Class = "TextBoxBorder"
        Me.txtWorksheetName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtWorksheetName.Location = New System.Drawing.Point(4, 25)
        Me.txtWorksheetName.MaxLength = 25
        Me.txtWorksheetName.Name = "txtWorksheetName"
        Me.txtWorksheetName.Size = New System.Drawing.Size(333, 21)
        Me.txtWorksheetName.TabIndex = 3
        Me.txtWorksheetName.Tag = "memo"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        '
        '
        '
        Me.Label10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(4, 8)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(265, 16)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "Worksheet Name (leave blank for default i.e.  Sheet1)"
        '
        'SuperTabItem6
        '
        Me.SuperTabItem6.AttachedControl = Me.ExcelGeneralSuperTab
        Me.SuperTabItem6.GlobalItem = False
        Me.SuperTabItem6.Name = "SuperTabItem6"
        Me.SuperTabItem6.Text = "General"
        '
        'grpCSV
        '
        Me.grpCSV.BackColor = System.Drawing.Color.Transparent
        Me.grpCSV.Controls.Add(Me.chkIgnoreHeaders)
        Me.grpCSV.Controls.Add(Me.cmbCSVEngine)
        Me.grpCSV.Controls.Add(Me.Label21)
        Me.grpCSV.Controls.Add(Me.GroupBox7)
        Me.grpCSV.Location = New System.Drawing.Point(11, 44)
        Me.grpCSV.Name = "grpCSV"
        Me.grpCSV.Size = New System.Drawing.Size(456, 320)
        Me.grpCSV.TabIndex = 3
        Me.grpCSV.Text = "CSV Options"
        Me.grpCSV.Visible = False
        '
        'chkIgnoreHeaders
        '
        '
        '
        '
        Me.chkIgnoreHeaders.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkIgnoreHeaders.Location = New System.Drawing.Point(16, 136)
        Me.chkIgnoreHeaders.Name = "chkIgnoreHeaders"
        Me.chkIgnoreHeaders.Size = New System.Drawing.Size(119, 23)
        Me.chkIgnoreHeaders.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.chkIgnoreHeaders.TabIndex = 9
        Me.chkIgnoreHeaders.Text = "Ignore Header"
        '
        'cmbCSVEngine
        '
        Me.cmbCSVEngine.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCSVEngine.FormattingEnabled = True
        Me.cmbCSVEngine.Items.AddRange(New Object() {"Standard", "Excel"})
        Me.cmbCSVEngine.Location = New System.Drawing.Point(144, 9)
        Me.cmbCSVEngine.Name = "cmbCSVEngine"
        Me.cmbCSVEngine.Size = New System.Drawing.Size(118, 21)
        Me.cmbCSVEngine.TabIndex = 8
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(12, 15)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(74, 13)
        Me.Label21.TabIndex = 7
        Me.Label21.Text = "Export Engine"
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.txtCharacter)
        Me.GroupBox7.Controls.Add(Me.optCharacter)
        Me.GroupBox7.Controls.Add(Me.optTab)
        Me.GroupBox7.Controls.Add(Me.Label6)
        Me.GroupBox7.Controls.Add(Me.txtDelimiter)
        Me.GroupBox7.Location = New System.Drawing.Point(8, 44)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(348, 80)
        Me.GroupBox7.TabIndex = 0
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Separator"
        '
        'txtCharacter
        '
        '
        '
        '
        Me.txtCharacter.Border.Class = "TextBoxBorder"
        Me.txtCharacter.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCharacter.Location = New System.Drawing.Point(136, 16)
        Me.txtCharacter.Name = "txtCharacter"
        Me.txtCharacter.Size = New System.Drawing.Size(72, 21)
        Me.txtCharacter.TabIndex = 1
        Me.txtCharacter.Text = ","
        '
        'optCharacter
        '
        Me.optCharacter.Checked = True
        Me.optCharacter.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optCharacter.Location = New System.Drawing.Point(8, 16)
        Me.optCharacter.Name = "optCharacter"
        Me.optCharacter.Size = New System.Drawing.Size(104, 24)
        Me.optCharacter.TabIndex = 0
        Me.optCharacter.TabStop = True
        Me.optCharacter.Text = "Character"
        '
        'optTab
        '
        Me.optTab.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optTab.Location = New System.Drawing.Point(212, 15)
        Me.optTab.Name = "optTab"
        Me.optTab.Size = New System.Drawing.Size(104, 24)
        Me.optTab.TabIndex = 0
        Me.optTab.Text = "Tab"
        '
        'Label6
        '
        '
        '
        '
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(7, 49)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 16)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Delimiter"
        '
        'txtDelimiter
        '
        '
        '
        '
        Me.txtDelimiter.Border.Class = "TextBoxBorder"
        Me.txtDelimiter.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDelimiter.Location = New System.Drawing.Point(136, 47)
        Me.txtDelimiter.Name = "txtDelimiter"
        Me.txtDelimiter.Size = New System.Drawing.Size(72, 21)
        Me.txtDelimiter.TabIndex = 1
        Me.txtDelimiter.Text = """"
        '
        'grpText
        '
        Me.grpText.BackColor = System.Drawing.Color.Transparent
        Me.grpText.Controls.Add(Me.tbMiraplacid)
        Me.grpText.Controls.Add(Me.cmbTextEngine)
        Me.grpText.Controls.Add(Me.Label8)
        Me.grpText.Location = New System.Drawing.Point(11, 44)
        Me.grpText.Name = "grpText"
        Me.grpText.Size = New System.Drawing.Size(456, 320)
        Me.grpText.TabIndex = 5
        Me.grpText.Text = "Text Options"
        Me.grpText.Visible = False
        '
        'tbMiraplacid
        '
        Me.tbMiraplacid.ColumnCount = 2
        Me.tbMiraplacid.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.49462!))
        Me.tbMiraplacid.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.50538!))
        Me.tbMiraplacid.Controls.Add(Me.Label9, 0, 0)
        Me.tbMiraplacid.Controls.Add(Me.txtdpi, 1, 0)
        Me.tbMiraplacid.Controls.Add(Me.grpMiraplacid, 0, 3)
        Me.tbMiraplacid.Controls.Add(Me.chkUseDriverDefaults, 1, 2)
        Me.tbMiraplacid.Controls.Add(Me.Label25, 0, 1)
        Me.tbMiraplacid.Controls.Add(Me.txtTimePerPage, 1, 1)
        Me.tbMiraplacid.Location = New System.Drawing.Point(18, 38)
        Me.tbMiraplacid.Name = "tbMiraplacid"
        Me.tbMiraplacid.RowCount = 4
        Me.tbMiraplacid.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tbMiraplacid.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tbMiraplacid.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tbMiraplacid.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tbMiraplacid.Size = New System.Drawing.Size(372, 261)
        Me.tbMiraplacid.TabIndex = 4
        Me.tbMiraplacid.Visible = False
        '
        'Label9
        '
        Me.Label9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label9.Location = New System.Drawing.Point(3, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(99, 27)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Dots per inch (dpi)"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtdpi
        '
        Me.txtdpi.Location = New System.Drawing.Point(108, 3)
        Me.txtdpi.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.txtdpi.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtdpi.Name = "txtdpi"
        Me.txtdpi.Size = New System.Drawing.Size(71, 21)
        Me.txtdpi.TabIndex = 1
        Me.txtdpi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtdpi.Value = New Decimal(New Integer() {96, 0, 0, 0})
        '
        'grpMiraplacid
        '
        Me.tbMiraplacid.SetColumnSpan(Me.grpMiraplacid, 2)
        Me.grpMiraplacid.Controls.Add(Me.TableLayoutPanel1)
        Me.grpMiraplacid.Enabled = False
        Me.grpMiraplacid.Location = New System.Drawing.Point(3, 80)
        Me.grpMiraplacid.Name = "grpMiraplacid"
        Me.grpMiraplacid.Size = New System.Drawing.Size(366, 161)
        Me.grpMiraplacid.TabIndex = 3
        Me.grpMiraplacid.TabStop = False
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.83616!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.16384!))
        Me.TableLayoutPanel1.Controls.Add(Me.LinkLabel1, 1, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.Label18, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtwhitespacesize, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.cmbformattingstyle, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label19, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label22, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label23, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.cmbcharacterset, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.cmbendoflinestyle, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.chkInsertBreaks, 1, 4)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(5, 14)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 6
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.21053!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.21053!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.1579!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.1579!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.78947!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(354, 141)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'LinkLabel1
        '
        Me.LinkLabel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LinkLabel1.Location = New System.Drawing.Point(98, 123)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(253, 14)
        Me.LinkLabel1.TabIndex = 4
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Powered by Miraplacid"
        Me.LinkLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label18
        '
        Me.Label18.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label18.Location = New System.Drawing.Point(3, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(89, 24)
        Me.Label18.TabIndex = 0
        Me.Label18.Text = "Formatting style"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtwhitespacesize
        '
        Me.txtwhitespacesize.Increment = New Decimal(New Integer() {10, 0, 0, 0})
        Me.txtwhitespacesize.Location = New System.Drawing.Point(98, 73)
        Me.txtwhitespacesize.Minimum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.txtwhitespacesize.Name = "txtwhitespacesize"
        Me.txtwhitespacesize.Size = New System.Drawing.Size(154, 21)
        Me.txtwhitespacesize.TabIndex = 1
        Me.txtwhitespacesize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtwhitespacesize.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        'cmbformattingstyle
        '
        Me.cmbformattingstyle.FormattingEnabled = True
        Me.cmbformattingstyle.Items.AddRange(New Object() {"Formatted", "Plain", "RSS", "Text with Layout", "XML"})
        Me.cmbformattingstyle.Location = New System.Drawing.Point(98, 3)
        Me.cmbformattingstyle.Name = "cmbformattingstyle"
        Me.cmbformattingstyle.Size = New System.Drawing.Size(154, 21)
        Me.cmbformattingstyle.TabIndex = 1
        Me.cmbformattingstyle.Text = "Formatted"
        '
        'Label19
        '
        Me.Label19.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label19.Location = New System.Drawing.Point(3, 24)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(89, 24)
        Me.Label19.TabIndex = 0
        Me.Label19.Text = "Character Set"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label22
        '
        Me.Label22.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label22.Location = New System.Drawing.Point(3, 48)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(89, 22)
        Me.Label22.TabIndex = 0
        Me.Label22.Text = "End-of-line style"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label23
        '
        Me.Label23.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label23.Location = New System.Drawing.Point(3, 70)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(89, 22)
        Me.Label23.TabIndex = 0
        Me.Label23.Text = "Whitespace size"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbcharacterset
        '
        Me.cmbcharacterset.FormattingEnabled = True
        Me.cmbcharacterset.Items.AddRange(New Object() {"UNICODE", "Default ANSI", "Default OEM", "Default MAC"})
        Me.cmbcharacterset.Location = New System.Drawing.Point(98, 27)
        Me.cmbcharacterset.Name = "cmbcharacterset"
        Me.cmbcharacterset.Size = New System.Drawing.Size(154, 21)
        Me.cmbcharacterset.TabIndex = 1
        Me.cmbcharacterset.Text = "Default ANSI"
        '
        'cmbendoflinestyle
        '
        Me.cmbendoflinestyle.FormattingEnabled = True
        Me.cmbendoflinestyle.Items.AddRange(New Object() {"Windows (\r\n)", "Unix (\n)"})
        Me.cmbendoflinestyle.Location = New System.Drawing.Point(98, 51)
        Me.cmbendoflinestyle.Name = "cmbendoflinestyle"
        Me.cmbendoflinestyle.Size = New System.Drawing.Size(154, 21)
        Me.cmbendoflinestyle.TabIndex = 1
        Me.cmbendoflinestyle.Text = "Windows (\r\n)"
        '
        'chkInsertBreaks
        '
        Me.chkInsertBreaks.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkInsertBreaks.AutoSize = True
        Me.chkInsertBreaks.Location = New System.Drawing.Point(98, 95)
        Me.chkInsertBreaks.Name = "chkInsertBreaks"
        Me.chkInsertBreaks.Size = New System.Drawing.Size(117, 21)
        Me.chkInsertBreaks.TabIndex = 2
        Me.chkInsertBreaks.Text = "Insert page breaks"
        Me.chkInsertBreaks.UseVisualStyleBackColor = True
        '
        'chkUseDriverDefaults
        '
        Me.chkUseDriverDefaults.AutoSize = True
        Me.chkUseDriverDefaults.Checked = True
        Me.chkUseDriverDefaults.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkUseDriverDefaults.Location = New System.Drawing.Point(108, 57)
        Me.chkUseDriverDefaults.Name = "chkUseDriverDefaults"
        Me.chkUseDriverDefaults.Size = New System.Drawing.Size(169, 17)
        Me.chkUseDriverDefaults.TabIndex = 2
        Me.chkUseDriverDefaults.Text = "Use Miraplacid Driver Defaults"
        Me.chkUseDriverDefaults.UseVisualStyleBackColor = True
        '
        'Label25
        '
        Me.Label25.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label25.Location = New System.Drawing.Point(3, 27)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(99, 27)
        Me.Label25.TabIndex = 0
        Me.Label25.Text = "Seconds/Page"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTimePerPage
        '
        Me.txtTimePerPage.Location = New System.Drawing.Point(108, 30)
        Me.txtTimePerPage.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.txtTimePerPage.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtTimePerPage.Name = "txtTimePerPage"
        Me.txtTimePerPage.Size = New System.Drawing.Size(71, 21)
        Me.txtTimePerPage.TabIndex = 1
        Me.txtTimePerPage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTimePerPage.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'cmbTextEngine
        '
        Me.cmbTextEngine.FormattingEnabled = True
        Me.cmbTextEngine.Items.AddRange(New Object() {"Default", "Miraplacid"})
        Me.cmbTextEngine.Location = New System.Drawing.Point(121, 6)
        Me.cmbTextEngine.Name = "cmbTextEngine"
        Me.cmbTextEngine.Size = New System.Drawing.Size(154, 21)
        Me.cmbTextEngine.TabIndex = 3
        Me.cmbTextEngine.Text = "Default"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(12, 8)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(64, 13)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Text Engine"
        '
        'grpWord
        '
        Me.grpWord.BackColor = System.Drawing.Color.Transparent
        Me.grpWord.Controls.Add(Me.chkInsertPageBreaksWord)
        Me.grpWord.Location = New System.Drawing.Point(11, 44)
        Me.grpWord.Name = "grpWord"
        Me.grpWord.Size = New System.Drawing.Size(456, 63)
        Me.grpWord.TabIndex = 3
        Me.grpWord.Text = "MS Word - Editable"
        Me.grpWord.Visible = False
        '
        'chkInsertPageBreaksWord
        '
        Me.chkInsertPageBreaksWord.AutoSize = True
        '
        '
        '
        Me.chkInsertPageBreaksWord.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkInsertPageBreaksWord.Location = New System.Drawing.Point(8, 26)
        Me.chkInsertPageBreaksWord.Name = "chkInsertPageBreaksWord"
        Me.chkInsertPageBreaksWord.Size = New System.Drawing.Size(221, 16)
        Me.chkInsertPageBreaksWord.TabIndex = 0
        Me.chkInsertPageBreaksWord.Text = "Insert page break after each report page"
        '
        'grpXML
        '
        Me.grpXML.BackColor = System.Drawing.Color.Transparent
        Me.grpXML.Controls.Add(Me.txtXMLTableName)
        Me.grpXML.Controls.Add(Me.Label24)
        Me.grpXML.Controls.Add(Me.GroupBox13)
        Me.grpXML.Controls.Add(Me.chkXMLWriteHierachy)
        Me.grpXML.Location = New System.Drawing.Point(11, 44)
        Me.grpXML.Name = "grpXML"
        Me.grpXML.Size = New System.Drawing.Size(456, 207)
        Me.grpXML.TabIndex = 3
        Me.grpXML.Text = "XML Options"
        Me.grpXML.Visible = False
        '
        'txtXMLTableName
        '
        '
        '
        '
        Me.txtXMLTableName.Border.Class = "TextBoxBorder"
        Me.txtXMLTableName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtXMLTableName.Location = New System.Drawing.Point(17, 176)
        Me.txtXMLTableName.Name = "txtXMLTableName"
        Me.txtXMLTableName.Size = New System.Drawing.Size(341, 21)
        Me.txtXMLTableName.TabIndex = 3
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        '
        '
        '
        Me.Label24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label24.Location = New System.Drawing.Point(15, 161)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(139, 16)
        Me.Label24.TabIndex = 2
        Me.Label24.Text = "Data Table Name (Optional)"
        '
        'GroupBox13
        '
        Me.GroupBox13.Controls.Add(Me.optXMLDiffGram)
        Me.GroupBox13.Controls.Add(Me.optXMLIgnoreSchema)
        Me.GroupBox13.Controls.Add(Me.optXMLIncludeSchema)
        Me.GroupBox13.Location = New System.Drawing.Point(15, 54)
        Me.GroupBox13.Name = "GroupBox13"
        Me.GroupBox13.Size = New System.Drawing.Size(343, 100)
        Me.GroupBox13.TabIndex = 1
        Me.GroupBox13.TabStop = False
        Me.GroupBox13.Text = "XML Mode"
        '
        'optXMLDiffGram
        '
        Me.optXMLDiffGram.AutoSize = True
        Me.optXMLDiffGram.Location = New System.Drawing.Point(15, 66)
        Me.optXMLDiffGram.Name = "optXMLDiffGram"
        Me.optXMLDiffGram.Size = New System.Drawing.Size(88, 17)
        Me.optXMLDiffGram.TabIndex = 0
        Me.optXMLDiffGram.TabStop = True
        Me.optXMLDiffGram.Tag = "2"
        Me.optXMLDiffGram.Text = "Use DiffGram"
        Me.optXMLDiffGram.UseVisualStyleBackColor = True
        '
        'optXMLIgnoreSchema
        '
        Me.optXMLIgnoreSchema.AutoSize = True
        Me.optXMLIgnoreSchema.Location = New System.Drawing.Point(16, 43)
        Me.optXMLIgnoreSchema.Name = "optXMLIgnoreSchema"
        Me.optXMLIgnoreSchema.Size = New System.Drawing.Size(97, 17)
        Me.optXMLIgnoreSchema.TabIndex = 0
        Me.optXMLIgnoreSchema.TabStop = True
        Me.optXMLIgnoreSchema.Tag = "1"
        Me.optXMLIgnoreSchema.Text = "Ignore Schema"
        Me.optXMLIgnoreSchema.UseVisualStyleBackColor = True
        '
        'optXMLIncludeSchema
        '
        Me.optXMLIncludeSchema.AutoSize = True
        Me.optXMLIncludeSchema.Location = New System.Drawing.Point(16, 20)
        Me.optXMLIncludeSchema.Name = "optXMLIncludeSchema"
        Me.optXMLIncludeSchema.Size = New System.Drawing.Size(100, 17)
        Me.optXMLIncludeSchema.TabIndex = 0
        Me.optXMLIncludeSchema.TabStop = True
        Me.optXMLIncludeSchema.Tag = "0"
        Me.optXMLIncludeSchema.Text = "Include Schema"
        Me.optXMLIncludeSchema.UseVisualStyleBackColor = True
        '
        'chkXMLWriteHierachy
        '
        Me.chkXMLWriteHierachy.AutoSize = True
        '
        '
        '
        Me.chkXMLWriteHierachy.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkXMLWriteHierachy.Location = New System.Drawing.Point(15, 24)
        Me.chkXMLWriteHierachy.Name = "chkXMLWriteHierachy"
        Me.chkXMLWriteHierachy.Size = New System.Drawing.Size(94, 16)
        Me.chkXMLWriteHierachy.TabIndex = 0
        Me.chkXMLWriteHierachy.Text = "Write Hierachy"
        '
        'grpODBC
        '
        Me.grpODBC.BackColor = System.Drawing.Color.Transparent
        Me.grpODBC.Controls.Add(Me.txtTableName)
        Me.grpODBC.Controls.Add(Me.Label7)
        Me.grpODBC.Controls.Add(Me.cmdConnect)
        Me.grpODBC.Controls.Add(Me.UcDSN)
        Me.grpODBC.Location = New System.Drawing.Point(11, 44)
        Me.grpODBC.Name = "grpODBC"
        Me.grpODBC.Size = New System.Drawing.Size(456, 208)
        Me.grpODBC.TabIndex = 4
        Me.grpODBC.Text = "ODBC Options"
        Me.grpODBC.Visible = False
        '
        'txtTableName
        '
        '
        '
        '
        Me.txtTableName.Border.Class = "TextBoxBorder"
        Me.txtTableName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTableName.Enabled = False
        Me.txtTableName.Location = New System.Drawing.Point(80, 176)
        Me.txtTableName.Name = "txtTableName"
        Me.txtTableName.Size = New System.Drawing.Size(264, 21)
        Me.txtTableName.TabIndex = 0
        '
        'Label7
        '
        '
        '
        '
        Me.Label7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(8, 178)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(64, 16)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "Table Name"
        '
        'cmdConnect
        '
        Me.cmdConnect.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdConnect.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdConnect.Location = New System.Drawing.Point(144, 144)
        Me.cmdConnect.Name = "cmdConnect"
        Me.cmdConnect.Size = New System.Drawing.Size(75, 23)
        Me.cmdConnect.TabIndex = 1
        Me.cmdConnect.Text = "Connect..."
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.SystemColors.Control
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN.Location = New System.Drawing.Point(8, 24)
        Me.UcDSN.m_conString = "|||"
        Me.UcDSN.m_showConnectionType = False
        Me.UcDSN.Name = "UcDSN"
        Me.UcDSN.Size = New System.Drawing.Size(336, 112)
        Me.UcDSN.TabIndex = 0
        '
        'grpTiff
        '
        Me.grpTiff.BackColor = System.Drawing.Color.Transparent
        Me.grpTiff.Controls.Add(Me.chkNative)
        Me.grpTiff.Controls.Add(Me.GroupBox4)
        Me.grpTiff.Controls.Add(Me.GroupBox8)
        Me.grpTiff.Controls.Add(Me.GroupBox11)
        Me.grpTiff.Location = New System.Drawing.Point(11, 44)
        Me.grpTiff.Name = "grpTiff"
        Me.grpTiff.Size = New System.Drawing.Size(456, 320)
        Me.grpTiff.TabIndex = 8
        Me.grpTiff.Visible = False
        '
        'chkNative
        '
        Me.chkNative.AutoSize = True
        '
        '
        '
        Me.chkNative.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkNative.Checked = True
        Me.chkNative.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkNative.CheckValue = "Y"
        Me.chkNative.Location = New System.Drawing.Point(8, 13)
        Me.chkNative.Name = "chkNative"
        Me.chkNative.Size = New System.Drawing.Size(161, 16)
        Me.chkNative.TabIndex = 0
        Me.chkNative.Text = "Use SQL-RD's default format"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label20)
        Me.GroupBox4.Controls.Add(Me.lblFrom)
        Me.GroupBox4.Controls.Add(Me.numTo)
        Me.GroupBox4.Controls.Add(Me.numFrom)
        Me.GroupBox4.Controls.Add(Me.rbPageRange)
        Me.GroupBox4.Controls.Add(Me.rbAllPages)
        Me.GroupBox4.Location = New System.Drawing.Point(186, 196)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(175, 117)
        Me.GroupBox4.TabIndex = 3
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Page Range"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        '
        '
        '
        Me.Label20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label20.Enabled = False
        Me.Label20.Location = New System.Drawing.Point(93, 82)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(12, 16)
        Me.Label20.TabIndex = 5
        Me.Label20.Text = "to"
        '
        'lblFrom
        '
        Me.lblFrom.AutoSize = True
        '
        '
        '
        Me.lblFrom.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblFrom.Enabled = False
        Me.lblFrom.Location = New System.Drawing.Point(5, 82)
        Me.lblFrom.Name = "lblFrom"
        Me.lblFrom.Size = New System.Drawing.Size(27, 16)
        Me.lblFrom.TabIndex = 4
        Me.lblFrom.Text = "From"
        '
        'numTo
        '
        Me.numTo.Enabled = False
        Me.numTo.Location = New System.Drawing.Point(116, 78)
        Me.numTo.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.numTo.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.numTo.Name = "numTo"
        Me.numTo.Size = New System.Drawing.Size(53, 21)
        Me.numTo.TabIndex = 3
        Me.numTo.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'numFrom
        '
        Me.numFrom.Enabled = False
        Me.numFrom.Location = New System.Drawing.Point(37, 78)
        Me.numFrom.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.numFrom.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.numFrom.Name = "numFrom"
        Me.numFrom.Size = New System.Drawing.Size(51, 21)
        Me.numFrom.TabIndex = 2
        Me.numFrom.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'rbPageRange
        '
        Me.rbPageRange.AutoSize = True
        Me.rbPageRange.Location = New System.Drawing.Point(7, 53)
        Me.rbPageRange.Name = "rbPageRange"
        Me.rbPageRange.Size = New System.Drawing.Size(83, 17)
        Me.rbPageRange.TabIndex = 1
        Me.rbPageRange.Text = "Page Range"
        Me.rbPageRange.UseVisualStyleBackColor = True
        '
        'rbAllPages
        '
        Me.rbAllPages.AutoSize = True
        Me.rbAllPages.Checked = True
        Me.rbAllPages.Location = New System.Drawing.Point(7, 29)
        Me.rbAllPages.Name = "rbAllPages"
        Me.rbAllPages.Size = New System.Drawing.Size(68, 17)
        Me.rbAllPages.TabIndex = 0
        Me.rbAllPages.TabStop = True
        Me.rbAllPages.Text = "All Pages"
        Me.rbAllPages.UseVisualStyleBackColor = True
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.chkGrayScale)
        Me.GroupBox8.Controls.Add(Me.cmbColor)
        Me.GroupBox8.Location = New System.Drawing.Point(7, 196)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(174, 117)
        Me.GroupBox8.TabIndex = 2
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Color Depth"
        '
        'chkGrayScale
        '
        Me.chkGrayScale.AutoSize = True
        '
        '
        '
        Me.chkGrayScale.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkGrayScale.Enabled = False
        Me.chkGrayScale.Location = New System.Drawing.Point(14, 67)
        Me.chkGrayScale.Name = "chkGrayScale"
        Me.chkGrayScale.Size = New System.Drawing.Size(148, 16)
        Me.chkGrayScale.TabIndex = 1
        Me.chkGrayScale.Text = "Convert to 8-bit grayscale"
        '
        'cmbColor
        '
        Me.cmbColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbColor.FormattingEnabled = True
        Me.cmbColor.Items.AddRange(New Object() {"Black and White", "256 colors", "24-bit color"})
        Me.cmbColor.Location = New System.Drawing.Point(12, 31)
        Me.cmbColor.MaxDropDownItems = 3
        Me.cmbColor.Name = "cmbColor"
        Me.cmbColor.Size = New System.Drawing.Size(148, 21)
        Me.cmbColor.TabIndex = 0
        '
        'GroupBox11
        '
        Me.GroupBox11.Controls.Add(Me.rbCLASSF196)
        Me.GroupBox11.Controls.Add(Me.rbCLASSF)
        Me.GroupBox11.Controls.Add(Me.rbMAC)
        Me.GroupBox11.Controls.Add(Me.rbLZW)
        Me.GroupBox11.Controls.Add(Me.rbFAX4)
        Me.GroupBox11.Controls.Add(Me.rbFAX3)
        Me.GroupBox11.Controls.Add(Me.rbRLE)
        Me.GroupBox11.Controls.Add(Me.rbNone)
        Me.GroupBox11.Location = New System.Drawing.Point(6, 35)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(355, 158)
        Me.GroupBox11.TabIndex = 1
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "Compression"
        '
        'rbCLASSF196
        '
        Me.rbCLASSF196.AutoSize = True
        Me.rbCLASSF196.Location = New System.Drawing.Point(11, 119)
        Me.rbCLASSF196.Name = "rbCLASSF196"
        Me.rbCLASSF196.Size = New System.Drawing.Size(264, 17)
        Me.rbCLASSF196.TabIndex = 7
        Me.rbCLASSF196.Text = "204x196 ClassF TIFF file, Fax machine compatible"
        Me.rbCLASSF196.UseVisualStyleBackColor = True
        '
        'rbCLASSF
        '
        Me.rbCLASSF.AutoSize = True
        Me.rbCLASSF.Location = New System.Drawing.Point(11, 96)
        Me.rbCLASSF.Name = "rbCLASSF"
        Me.rbCLASSF.Size = New System.Drawing.Size(258, 17)
        Me.rbCLASSF.TabIndex = 6
        Me.rbCLASSF.Text = "204x98 ClassF TIFF file, Fax machine compatible"
        Me.rbCLASSF.UseVisualStyleBackColor = True
        '
        'rbMAC
        '
        Me.rbMAC.AutoSize = True
        Me.rbMAC.Location = New System.Drawing.Point(11, 72)
        Me.rbMAC.Name = "rbMAC"
        Me.rbMAC.Size = New System.Drawing.Size(94, 17)
        Me.rbMAC.TabIndex = 2
        Me.rbMAC.Text = "Macintosh RLE"
        Me.rbMAC.UseVisualStyleBackColor = True
        '
        'rbLZW
        '
        Me.rbLZW.AutoSize = True
        Me.rbLZW.Location = New System.Drawing.Point(11, 50)
        Me.rbLZW.Name = "rbLZW"
        Me.rbLZW.Size = New System.Drawing.Size(150, 17)
        Me.rbLZW.TabIndex = 1
        Me.rbLZW.Text = "Lempel-Ziv && Welch (LZW)"
        Me.rbLZW.UseVisualStyleBackColor = True
        '
        'rbFAX4
        '
        Me.rbFAX4.AutoSize = True
        Me.rbFAX4.Location = New System.Drawing.Point(180, 72)
        Me.rbFAX4.Name = "rbFAX4"
        Me.rbFAX4.Size = New System.Drawing.Size(161, 17)
        Me.rbFAX4.TabIndex = 5
        Me.rbFAX4.Text = "CCITT Group 4 fax encoding"
        Me.rbFAX4.UseVisualStyleBackColor = True
        '
        'rbFAX3
        '
        Me.rbFAX3.AutoSize = True
        Me.rbFAX3.Location = New System.Drawing.Point(180, 50)
        Me.rbFAX3.Name = "rbFAX3"
        Me.rbFAX3.Size = New System.Drawing.Size(161, 17)
        Me.rbFAX3.TabIndex = 4
        Me.rbFAX3.Text = "CCITT Group 3 fax encoding"
        Me.rbFAX3.UseVisualStyleBackColor = True
        '
        'rbRLE
        '
        Me.rbRLE.AutoSize = True
        Me.rbRLE.Location = New System.Drawing.Point(180, 25)
        Me.rbRLE.Name = "rbRLE"
        Me.rbRLE.Size = New System.Drawing.Size(163, 17)
        Me.rbRLE.TabIndex = 3
        Me.rbRLE.Text = "CCITT modified Huffman RLE"
        Me.rbRLE.UseVisualStyleBackColor = True
        '
        'rbNone
        '
        Me.rbNone.AutoSize = True
        Me.rbNone.Checked = True
        Me.rbNone.Location = New System.Drawing.Point(11, 25)
        Me.rbNone.Name = "rbNone"
        Me.rbNone.Size = New System.Drawing.Size(137, 17)
        Me.rbNone.TabIndex = 0
        Me.rbNone.TabStop = True
        Me.rbNone.Text = "Uncompressed TIFF file"
        Me.rbNone.UseVisualStyleBackColor = True
        '
        'grpAppendFile
        '
        Me.grpAppendFile.BackColor = System.Drawing.Color.Transparent
        Me.grpAppendFile.Controls.Add(Me.chkAppendFile)
        Me.grpAppendFile.Enabled = False
        Me.grpAppendFile.Location = New System.Drawing.Point(8, 129)
        Me.grpAppendFile.Name = "grpAppendFile"
        Me.grpAppendFile.Size = New System.Drawing.Size(367, 60)
        Me.grpAppendFile.TabIndex = 1
        Me.grpAppendFile.TabStop = False
        Me.grpAppendFile.Text = "Append to existing file"
        '
        'chkAppendFile
        '
        Me.chkAppendFile.AutoSize = True
        '
        '
        '
        Me.chkAppendFile.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAppendFile.Location = New System.Drawing.Point(11, 26)
        Me.chkAppendFile.Name = "chkAppendFile"
        Me.chkAppendFile.Size = New System.Drawing.Size(278, 16)
        Me.chkAppendFile.TabIndex = 1
        Me.chkAppendFile.Text = "Append to file if one already exists (disk destination)"
        '
        'grpPageRange
        '
        Me.grpPageRange.BackColor = System.Drawing.Color.Transparent
        Me.grpPageRange.Controls.Add(Me.txtPageFrom)
        Me.grpPageRange.Controls.Add(Me.Label4)
        Me.grpPageRange.Controls.Add(Me.optAllPages)
        Me.grpPageRange.Controls.Add(Me.optPageRange)
        Me.grpPageRange.Controls.Add(Me.Label5)
        Me.grpPageRange.Controls.Add(Me.txtPageTo)
        Me.grpPageRange.Location = New System.Drawing.Point(8, 3)
        Me.grpPageRange.Name = "grpPageRange"
        Me.grpPageRange.Size = New System.Drawing.Size(367, 120)
        Me.grpPageRange.TabIndex = 0
        Me.grpPageRange.TabStop = False
        Me.grpPageRange.Text = "Export Page Ranges"
        '
        'txtPageFrom
        '
        Me.txtPageFrom.Location = New System.Drawing.Point(56, 80)
        Me.txtPageFrom.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.txtPageFrom.Name = "txtPageFrom"
        Me.txtPageFrom.Size = New System.Drawing.Size(48, 21)
        Me.txtPageFrom.TabIndex = 2
        Me.txtPageFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        '
        '
        '
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 80)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 16)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "From"
        '
        'optAllPages
        '
        Me.optAllPages.Checked = True
        Me.optAllPages.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optAllPages.Location = New System.Drawing.Point(8, 16)
        Me.optAllPages.Name = "optAllPages"
        Me.optAllPages.Size = New System.Drawing.Size(216, 24)
        Me.optAllPages.TabIndex = 0
        Me.optAllPages.TabStop = True
        Me.optAllPages.Text = "All Pages"
        '
        'optPageRange
        '
        Me.optPageRange.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optPageRange.Location = New System.Drawing.Point(8, 48)
        Me.optPageRange.Name = "optPageRange"
        Me.optPageRange.Size = New System.Drawing.Size(216, 24)
        Me.optPageRange.TabIndex = 0
        Me.optPageRange.Text = "Page Range"
        '
        'Label5
        '
        '
        '
        '
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(120, 80)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(24, 16)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "To"
        '
        'txtPageTo
        '
        Me.txtPageTo.Location = New System.Drawing.Point(168, 80)
        Me.txtPageTo.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.txtPageTo.Name = "txtPageTo"
        Me.txtPageTo.Size = New System.Drawing.Size(48, 21)
        Me.txtPageTo.TabIndex = 2
        Me.txtPageTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.dtPDFExpiry)
        Me.GroupBox2.Controls.Add(Me.chkPDFExpiry)
        Me.GroupBox2.Controls.Add(Me.txtInfoCreated)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.txtInfoTitle)
        Me.GroupBox2.Controls.Add(Me.txtInfoAuthor)
        Me.GroupBox2.Controls.Add(Me.txtInfoSubject)
        Me.GroupBox2.Controls.Add(Me.txtInfoKeywords)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.txtInfoProducer)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 1)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(474, 346)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        '
        'dtPDFExpiry
        '
        Me.dtPDFExpiry.Enabled = False
        Me.dtPDFExpiry.Location = New System.Drawing.Point(133, 298)
        Me.dtPDFExpiry.Name = "dtPDFExpiry"
        Me.dtPDFExpiry.Size = New System.Drawing.Size(325, 21)
        Me.dtPDFExpiry.TabIndex = 29
        '
        'chkPDFExpiry
        '
        '
        '
        '
        Me.chkPDFExpiry.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkPDFExpiry.Enabled = False
        Me.chkPDFExpiry.Location = New System.Drawing.Point(6, 297)
        Me.chkPDFExpiry.Name = "chkPDFExpiry"
        Me.chkPDFExpiry.Size = New System.Drawing.Size(121, 23)
        Me.chkPDFExpiry.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.chkPDFExpiry.TabIndex = 28
        Me.chkPDFExpiry.Text = "PDF Expiry Date"
        '
        'txtInfoCreated
        '
        Me.txtInfoCreated.Enabled = False
        Me.txtInfoCreated.Location = New System.Drawing.Point(133, 146)
        Me.txtInfoCreated.Name = "txtInfoCreated"
        Me.txtInfoCreated.Size = New System.Drawing.Size(325, 21)
        Me.txtInfoCreated.TabIndex = 26
        '
        'Label11
        '
        '
        '
        '
        Me.Label11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(6, 20)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(100, 16)
        Me.Label11.TabIndex = 23
        Me.Label11.Text = "Title"
        '
        'txtInfoTitle
        '
        '
        '
        '
        Me.txtInfoTitle.Border.Class = "TextBoxBorder"
        Me.txtInfoTitle.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtInfoTitle.Location = New System.Drawing.Point(133, 18)
        Me.txtInfoTitle.Name = "txtInfoTitle"
        Me.txtInfoTitle.Size = New System.Drawing.Size(325, 21)
        Me.txtInfoTitle.TabIndex = 16
        '
        'txtInfoAuthor
        '
        '
        '
        '
        Me.txtInfoAuthor.Border.Class = "TextBoxBorder"
        Me.txtInfoAuthor.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtInfoAuthor.Location = New System.Drawing.Point(133, 50)
        Me.txtInfoAuthor.Name = "txtInfoAuthor"
        Me.txtInfoAuthor.Size = New System.Drawing.Size(325, 21)
        Me.txtInfoAuthor.TabIndex = 17
        '
        'txtInfoSubject
        '
        '
        '
        '
        Me.txtInfoSubject.Border.Class = "TextBoxBorder"
        Me.txtInfoSubject.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtInfoSubject.Location = New System.Drawing.Point(133, 114)
        Me.txtInfoSubject.Name = "txtInfoSubject"
        Me.txtInfoSubject.Size = New System.Drawing.Size(325, 21)
        Me.txtInfoSubject.TabIndex = 25
        '
        'txtInfoKeywords
        '
        '
        '
        '
        Me.txtInfoKeywords.Border.Class = "TextBoxBorder"
        Me.txtInfoKeywords.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtInfoKeywords.Location = New System.Drawing.Point(133, 178)
        Me.txtInfoKeywords.Multiline = True
        Me.txtInfoKeywords.Name = "txtInfoKeywords"
        Me.txtInfoKeywords.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtInfoKeywords.Size = New System.Drawing.Size(325, 107)
        Me.txtInfoKeywords.TabIndex = 27
        Me.txtInfoKeywords.Tag = "memo"
        '
        'Label12
        '
        '
        '
        '
        Me.Label12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(6, 52)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(100, 16)
        Me.Label12.TabIndex = 22
        Me.Label12.Text = "Author"
        '
        'Label13
        '
        '
        '
        '
        Me.Label13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(6, 84)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(100, 16)
        Me.Label13.TabIndex = 19
        Me.Label13.Text = "Producer"
        '
        'Label14
        '
        '
        '
        '
        Me.Label14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(6, 178)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(112, 16)
        Me.Label14.TabIndex = 18
        Me.Label14.Text = "Keywords/Comments"
        '
        'Label15
        '
        '
        '
        '
        Me.Label15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(6, 148)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(100, 16)
        Me.Label15.TabIndex = 21
        Me.Label15.Text = "Date Created"
        '
        'txtInfoProducer
        '
        '
        '
        '
        Me.txtInfoProducer.Border.Class = "TextBoxBorder"
        Me.txtInfoProducer.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtInfoProducer.ContextMenu = Me.mnuInserter
        Me.txtInfoProducer.Location = New System.Drawing.Point(133, 82)
        Me.txtInfoProducer.Name = "txtInfoProducer"
        Me.txtInfoProducer.Size = New System.Drawing.Size(325, 21)
        Me.txtInfoProducer.TabIndex = 20
        '
        'Label16
        '
        '
        '
        '
        Me.Label16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label16.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label16.Location = New System.Drawing.Point(6, 116)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(100, 16)
        Me.Label16.TabIndex = 24
        Me.Label16.Text = "Subject"
        '
        'Speller
        '
        Me.Speller.AllowAnyCase = False
        Me.Speller.AllowMixedCase = False
        Me.Speller.AlwaysShowDialog = False
        Me.Speller.BackColor = System.Drawing.Color.Empty
        Me.Speller.CheckCompoundWords = False
        Me.Speller.ConsiderationRange = 80
        Me.Speller.ControlBox = True
        Me.Speller.DictFilePath = Nothing
        Me.Speller.FindCapitalizedSuggestions = False
        Me.Speller.ForeColor = System.Drawing.Color.Empty
        Me.Speller.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.Speller.GUILanguage = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Icon = Nothing
        Me.Speller.IgnoreCapitalizedWords = False
        Me.Speller.IgnoreURLsAndEmailAddresses = True
        Me.Speller.IgnoreWordsWithDigits = True
        Me.Speller.IgnoreXML = False
        Me.Speller.IncludeUserDictionaryInSuggestions = False
        Me.Speller.LanguageParser = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Location = New System.Drawing.Point(300, 200)
        Me.Speller.LookIntoHyphenatedText = True
        Me.Speller.MdiParent = Nothing
        Me.Speller.MinimizeBox = True
        Me.Speller.Modal = False
        Me.Speller.ModalAutoDispose = True
        Me.Speller.ModalOwner = Nothing
        Me.Speller.QueryTextBoxMultiline = True
        Me.Speller.SeparateHyphenWords = False
        Me.Speller.ShowFinishedBoxAtEnd = True
        Me.Speller.ShowInTaskbar = False
        Me.Speller.SizeGripStyle = System.Windows.Forms.SizeGripStyle.[Auto]
        Me.Speller.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Speller.SuggestionsMethod = Keyoti.RapidSpell.SuggestionsMethodType.HashingSuggestions
        Me.Speller.SuggestSplitWords = True
        Me.Speller.TextBoxBaseToCheck = Nothing
        Me.Speller.Texts.AddButtonText = "Add"
        Me.Speller.Texts.CancelButtonText = "Cancel"
        Me.Speller.Texts.ChangeAllButtonText = "Change All"
        Me.Speller.Texts.ChangeButtonText = "Change"
        Me.Speller.Texts.CheckCompletePopUpText = "The spelling check is complete."
        Me.Speller.Texts.CheckCompletePopUpTitle = "Spelling"
        Me.Speller.Texts.CheckingSpellingText = "Checking Document..."
        Me.Speller.Texts.DialogTitleText = "Spelling"
        Me.Speller.Texts.FindingSuggestionsLabelText = "Finding Suggestions..."
        Me.Speller.Texts.FindSuggestionsLabelText = "Find Suggestions?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpText = "Finished checking selection, would you like to check the rest of the text?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpTitle = "Finished Checking Selection"
        Me.Speller.Texts.IgnoreAllButtonText = "Ignore All"
        Me.Speller.Texts.IgnoreButtonText = "Ignore"
        Me.Speller.Texts.InDictionaryLabelText = "In Dictionary:"
        Me.Speller.Texts.NoSuggestionsText = "No Suggestions."
        Me.Speller.Texts.NotInDictionaryLabelText = "Not In Dictionary:"
        Me.Speller.Texts.RemoveDuplicateWordText = "Remove duplicate word"
        Me.Speller.Texts.ResumeButtonText = "Resume"
        Me.Speller.Texts.SuggestionsLabelText = "Suggestions:"
        Me.Speller.ThirdPartyTextComponentToCheck = Nothing
        Me.Speller.TopLevel = True
        Me.Speller.TopMost = False
        Me.Speller.UserDictionaryFile = Nothing
        Me.Speller.V2Parser = True
        Me.Speller.WarnDuplicates = True
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.DefaultTooltipSettings = New DevComponents.DotNetBar.SuperTooltipInfo("", "", "", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray)
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'stabOptions
        '
        '
        '
        '
        '
        '
        '
        Me.stabOptions.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabOptions.ControlBox.MenuBox.Name = ""
        Me.stabOptions.ControlBox.Name = ""
        Me.stabOptions.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabOptions.ControlBox.MenuBox, Me.stabOptions.ControlBox.CloseBox})
        Me.stabOptions.Controls.Add(Me.formatOptionsPanel)
        Me.stabOptions.Controls.Add(Me.fileSummaryPanel)
        Me.stabOptions.Controls.Add(Me.pagerangeTabPanel)
        Me.stabOptions.Dock = System.Windows.Forms.DockStyle.Fill
        Me.stabOptions.Location = New System.Drawing.Point(0, 32)
        Me.stabOptions.Name = "stabOptions"
        Me.stabOptions.ReorderTabsEnabled = True
        Me.stabOptions.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabOptions.SelectedTabIndex = 0
        Me.stabOptions.Size = New System.Drawing.Size(480, 374)
        Me.stabOptions.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.stabOptions.TabIndex = 3
        Me.stabOptions.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem1, Me.SuperTabItem2, Me.stabFileSummary})
        Me.stabOptions.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.WinMediaPlayer12
        Me.stabOptions.Text = "SuperTabControl1"
        '
        'formatOptionsPanel
        '
        Me.formatOptionsPanel.Controls.Add(Me.grpExcel)
        Me.formatOptionsPanel.Controls.Add(Me.grpPDF)
        Me.formatOptionsPanel.Controls.Add(Me.grpText)
        Me.formatOptionsPanel.Controls.Add(Me.grpCustom)
        Me.formatOptionsPanel.Controls.Add(Me.grpHTML)
        Me.formatOptionsPanel.Controls.Add(Me.grpCSV)
        Me.formatOptionsPanel.Controls.Add(Me.grpCrystal)
        Me.formatOptionsPanel.Controls.Add(Me.grpTiff)
        Me.formatOptionsPanel.Controls.Add(Me.grpXML)
        Me.formatOptionsPanel.Controls.Add(Me.grpWord)
        Me.formatOptionsPanel.Controls.Add(Me.grpODBC)
        Me.formatOptionsPanel.Controls.Add(Me.grpRecordStyle)
        Me.formatOptionsPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.formatOptionsPanel.Location = New System.Drawing.Point(0, 24)
        Me.formatOptionsPanel.Name = "formatOptionsPanel"
        Me.formatOptionsPanel.Size = New System.Drawing.Size(480, 350)
        Me.formatOptionsPanel.TabIndex = 1
        Me.formatOptionsPanel.TabItem = Me.SuperTabItem1
        '
        'grpCustom
        '
        Me.grpCustom.Controls.Add(Me.Label17)
        Me.grpCustom.Controls.Add(Me.txtrenderextension)
        Me.grpCustom.Location = New System.Drawing.Point(11, 44)
        Me.grpCustom.Name = "grpCustom"
        Me.grpCustom.Size = New System.Drawing.Size(456, 75)
        Me.grpCustom.TabIndex = 10
        Me.grpCustom.Text = "Custom Format Options"
        Me.grpCustom.Visible = False
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(15, 26)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(253, 13)
        Me.Label17.TabIndex = 1
        Me.Label17.Text = "Custom Rendering Extension Name (e.g. PDF, TXT)"
        '
        'txtrenderextension
        '
        '
        '
        '
        Me.txtrenderextension.Border.Class = "TextBoxBorder"
        Me.txtrenderextension.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtrenderextension.Location = New System.Drawing.Point(15, 45)
        Me.txtrenderextension.Name = "txtrenderextension"
        Me.txtrenderextension.Size = New System.Drawing.Size(309, 21)
        Me.txtrenderextension.TabIndex = 0
        '
        'grpCrystal
        '
        Me.grpCrystal.Controls.Add(Me.chkMakeCrystalReadOnly)
        Me.grpCrystal.Location = New System.Drawing.Point(11, 44)
        Me.grpCrystal.Name = "grpCrystal"
        Me.grpCrystal.Size = New System.Drawing.Size(454, 314)
        Me.grpCrystal.TabIndex = 9
        Me.grpCrystal.Visible = False
        '
        'chkMakeCrystalReadOnly
        '
        '
        '
        '
        Me.chkMakeCrystalReadOnly.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkMakeCrystalReadOnly.Location = New System.Drawing.Point(11, 9)
        Me.chkMakeCrystalReadOnly.Name = "chkMakeCrystalReadOnly"
        Me.chkMakeCrystalReadOnly.Size = New System.Drawing.Size(434, 23)
        Me.chkMakeCrystalReadOnly.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.chkMakeCrystalReadOnly.TabIndex = 0
        Me.chkMakeCrystalReadOnly.Text = "Export to Crystal Reports Read-Only format (RPTR)"
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.formatOptionsPanel
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "Format Options"
        '
        'fileSummaryPanel
        '
        Me.fileSummaryPanel.Controls.Add(Me.GroupBox2)
        Me.fileSummaryPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.fileSummaryPanel.Location = New System.Drawing.Point(0, 0)
        Me.fileSummaryPanel.Name = "fileSummaryPanel"
        Me.fileSummaryPanel.Size = New System.Drawing.Size(480, 374)
        Me.fileSummaryPanel.TabIndex = 0
        Me.fileSummaryPanel.TabItem = Me.stabFileSummary
        '
        'stabFileSummary
        '
        Me.stabFileSummary.AttachedControl = Me.fileSummaryPanel
        Me.stabFileSummary.GlobalItem = False
        Me.stabFileSummary.Name = "stabFileSummary"
        Me.stabFileSummary.Text = "File Summary"
        '
        'pagerangeTabPanel
        '
        Me.pagerangeTabPanel.Controls.Add(Me.grpAppendFile)
        Me.pagerangeTabPanel.Controls.Add(Me.grpPageRange)
        Me.pagerangeTabPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pagerangeTabPanel.Location = New System.Drawing.Point(0, 0)
        Me.pagerangeTabPanel.Name = "pagerangeTabPanel"
        Me.pagerangeTabPanel.Size = New System.Drawing.Size(480, 374)
        Me.pagerangeTabPanel.TabIndex = 0
        Me.pagerangeTabPanel.TabItem = Me.SuperTabItem2
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.pagerangeTabPanel
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Options"
        '
        'cmbFormat
        '
        Me.cmbFormat.FormattingEnabled = True
        Me.cmbFormat.Location = New System.Drawing.Point(84, 3)
        Me.cmbFormat.Name = "cmbFormat"
        Me.cmbFormat.Size = New System.Drawing.Size(240, 21)
        Me.cmbFormat.TabIndex = 0
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.LabelX1)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmbFormat)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(480, 32)
        Me.FlowLayoutPanel1.TabIndex = 4
        '
        'LabelX1
        '
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(3, 3)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(75, 23)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "Format"
        '
        'frmRptOptions
        '
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me.stabOptions)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Name = "frmRptOptions"
        Me.Size = New System.Drawing.Size(480, 406)
        Me.grpPDF.ResumeLayout(False)
        CType(Me.SuperTabControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl2.ResumeLayout(False)
        Me.SuperTabControlPanel4.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.grpRecordStyle.ResumeLayout(False)
        Me.grpRecordStyle.PerformLayout()
        Me.grpHTML.ResumeLayout(False)
        Me.grpHTML.PerformLayout()
        Me.grpExcel.ResumeLayout(False)
        CType(Me.stabExcel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabExcel.ResumeLayout(False)
        Me.ExcelGeneralSuperTab.ResumeLayout(False)
        Me.ExcelGeneralSuperTab.PerformLayout()
        Me.GroupBox10.ResumeLayout(False)
        Me.grpCSV.ResumeLayout(False)
        Me.grpCSV.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.grpText.ResumeLayout(False)
        Me.grpText.PerformLayout()
        Me.tbMiraplacid.ResumeLayout(False)
        Me.tbMiraplacid.PerformLayout()
        CType(Me.txtdpi, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpMiraplacid.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        CType(Me.txtwhitespacesize, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTimePerPage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpWord.ResumeLayout(False)
        Me.grpWord.PerformLayout()
        Me.grpXML.ResumeLayout(False)
        Me.grpXML.PerformLayout()
        Me.GroupBox13.ResumeLayout(False)
        Me.GroupBox13.PerformLayout()
        Me.grpODBC.ResumeLayout(False)
        Me.grpTiff.ResumeLayout(False)
        Me.grpTiff.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.numTo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numFrom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.GroupBox11.ResumeLayout(False)
        Me.GroupBox11.PerformLayout()
        Me.grpAppendFile.ResumeLayout(False)
        Me.grpAppendFile.PerformLayout()
        Me.grpPageRange.ResumeLayout(False)
        CType(Me.txtPageFrom, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPageTo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.stabOptions, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabOptions.ResumeLayout(False)
        Me.formatOptionsPanel.ResumeLayout(False)
        Me.grpCustom.ResumeLayout(False)
        Me.grpCustom.PerformLayout()
        Me.grpCrystal.ResumeLayout(False)
        Me.fileSummaryPanel.ResumeLayout(False)
        Me.pagerangeTabPanel.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmRptOptions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Click

    End Sub

    'Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
    '    ofd.ShowDialog()

    '    If ofd.FileName.Length = 0 Then Return

    '    txtWatermark.Text = ofd.FileName
    'End Sub

    Private Sub _SetFormats(ByVal sType As String)
        Dim s() As String

        If sType = "SMS" Then
            s = New String() {"CSV (*.csv)", "Text (*.txt)"}

            cmbFormat.Items.Clear()
            cmbFormat.Items.AddRange(s)
            cmbFormat.Text = ""
        ElseIf is_Smart = True Then
            s = New String() {"MS Excel  (*.xls)", "XML (*.xml)", "Acrobat Format (*.pdf)", "HTML (*.html)", "JPEG (*.jpg)", "TIFF (*.tif)"}

            cmbFormat.Items.Clear()
            cmbFormat.Items.AddRange(s)
            cmbFormat.Text = ""
        ElseIf Me.m_IsQuery = True Then
            s = New String() {"MS Excel 97-2000 (*.xls)", "XML (*.xml)", "CSV (*.csv)"}

            cmbFormat.Items.Clear()
            cmbFormat.Items.AddRange(s)
            cmbFormat.Text = ""
        Else
            s = New String() {"Acrobat Format (*.pdf)", "CSV (*.csv)", _
            "Data Interchange Format (*.dif)", "dBase II (*.dbf)", "dBase III (*.dbf)", _
            "dBase IV (*.dbf)", "HTML (*.htm)", "Lotus 1-2-3 (*.wk1)", "Lotus 1-2-3 (*.wk3)", _
            "Lotus 1-2-3 (*.wk4)", "Lotus 1-2-3 (*.wks)", _
            "MS Excel 97-2000 (*.xls)", _
            "MS Excel 2007 (*.xlsx)", _
            "Web Archive (*.mhtml)", _
            "TIFF (*.tif)", "XML (*.xml)", "MS Word (*.doc)", "MS Word 2007 (*.docx)", "Rich Text Format (*.rtf)", "Text (*.txt)", "Custom (*.*)", "MS Word - Text (*.txt)"}

            cmbFormat.Items.Clear()
            cmbFormat.Items.AddRange(s)
            'cmbFormat.Text = ""
        End If
    End Sub

    Private Sub frmRptOptions_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        setupForDragAndDrop(txtWatermark)
        setupForDragAndDrop(txtOwnerPassword)
        setupForDragAndDrop(txtUserPassword)
        setupForDragAndDrop(txtWorksheetName)
        setupForDragAndDrop(txtExcelPass)
        setupForDragAndDrop(txtInfoTitle)
        setupForDragAndDrop(txtInfoAuthor)
        setupForDragAndDrop(txtInfoSubject)
        setupForDragAndDrop(txtInfoKeywords)
        setupForDragAndDrop(txtInfoProducer)

        _SetFormats(m_destinationType)
    End Sub


    Public Sub loadPackageOptions(ByVal sFormat As String, Optional ByVal nPackID As Integer = 99999)
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        SQL = "SELECT  * FROM PackageOptions WHERE PackID = " & nPackID

        oRs = clsMarsData.GetData(SQL)

        Try
            If oRs.EOF = False Then
                If Convert.ToBoolean(oRs("pdfsecurity").Value) = True Then
                    chkPDFSecurity.Checked = True
                    txtOwnerPassword.Text = IsNull(_DecryptDBValue(oRs("pdfpassword").Value))
                    txtUserPassword.Text = IsNull(_DecryptDBValue(oRs("userpassword").Value))
                    txtWatermark.Text = IsNull(oRs("pdfwatermark").Value)
                    chkPrint.Checked = Convert.ToBoolean(oRs("canprint").Value)
                    chkCopy.Checked = Convert.ToBoolean(oRs("cancopy").Value)
                    chkEdit.Checked = Convert.ToBoolean(oRs("canedit").Value)
                    chkNotes.Checked = Convert.ToBoolean(oRs("cannotes").Value)
                    chkFill.Checked = Convert.ToBoolean(oRs("canfill").Value)
                    chkAccess.Checked = Convert.ToBoolean(oRs("canaccess").Value)
                    chkAssemble.Checked = Convert.ToBoolean(oRs("canassemble").Value)
                    chkFullRes.Checked = Convert.ToBoolean(oRs("canprintfull").Value)
                End If

                txtInfoTitle.Text = IsNull(oRs("infotitle").Value)
                txtInfoAuthor.Text = IsNull(oRs("infoauthor").Value)
                txtInfoSubject.Text = IsNull(oRs("infosubject").Value)
                txtInfoKeywords.Text = IsNull(oRs("infokeywords").Value)
                txtInfoProducer.Text = IsNull(oRs("infoproducer").Value)


                Try
                    txtInfoCreated.Value = CType(oRs("infocreated").Value, Date)
                Catch ex As Exception
                    txtInfoCreated.Value = Now
                End Try


                If Convert.ToBoolean(Convert.ToInt16(IsNull(oRs("protectexcel").Value, 0))) = True Then
                    Me.chkExcelPass.Checked = True

                    Me.txtExcelPass.Text = _DecryptDBValue(IsNull(oRs("excelpassword").Value))
                End If

                Try
                    txtWorksheetName.Text = IsNull(oRs("worksheetname").Value)
                Catch :End Try
            End If

            oRs.Close()
        Catch : End Try

        If sFormat = "PDF" Then
            grpPDF.Visible = True
            grpPDF.BringToFront()

            pagerangeTabPanel.Enabled = False
            fileSummaryPanel.Enabled = False
        ElseIf sFormat = "XLS" Then
            grpExcel.Visible = True
            grpExcel.BringToFront()

            ExcelGeneralSuperTab.Enabled = False

            pagerangeTabPanel.Enabled = False
            fileSummaryPanel.Enabled = False
        End If
    End Sub
    Public Sub savePackageOptions(ByVal sFormat As String, Optional ByVal nPackID As Integer = 99999)
        Dim sCols As String
        Dim sVals As String

        clsMarsData.WriteData("DELETE FROM PackageOptions WHERE PackID =" & nPackID)

        sCols = "OptionID,PackID,PDFSecurity,PDFPassword,UserPassword,PDFWatermark,CanPrint,CanCopy,CanEdit," & _
        "CanNotes,CanFill,CanAccess,CanAssemble,CanPrintFull," & _
        "InfoTitle,InfoAuthor,InfoSubject,InfoKeyWords,InfoCreated,InfoProducer,ProtectExcel,ExcelPassword,WorkSheetName"


        sVals = clsMarsData.CreateDataID("packageoptions", "optionid") & "," & _
        nPackID & "," & _
        Convert.ToInt32(chkPDFSecurity.Checked) & "," & _
        "'" & SQLPrepare(_EncryptDBValue(txtOwnerPassword.Text)) & "'," & _
        "'" & SQLPrepare(_EncryptDBValue(txtUserPassword.Text)) & "'," & _
        "'" & SQLPrepare(txtWatermark.Text) & "'," & _
        Convert.ToInt32(chkPrint.Checked) & "," & _
        Convert.ToInt32(chkCopy.Checked) & "," & _
        Convert.ToInt32(chkEdit.Checked) & "," & _
        Convert.ToInt32(chkNotes.Checked) & "," & _
        Convert.ToInt32(chkFill.Checked) & "," & _
        Convert.ToInt32(chkAccess.Checked) & "," & _
        Convert.ToInt32(chkAssemble.Checked) & "," & _
        Convert.ToInt32(chkFullRes.Checked) & "," & _
        "'" & SQLPrepare(txtInfoTitle.Text) & "'," & _
        "'" & SQLPrepare(txtInfoAuthor.Text) & "'," & _
        "'" & SQLPrepare(txtInfoSubject.Text) & "'," & _
        "'" & SQLPrepare(txtInfoKeywords.Text) & "'," & _
        "'" & ConDateTime(txtInfoCreated.Value) & "'," & _
        "'" & SQLPrepare(txtInfoProducer.Text) & "'," & _
        Convert.ToInt32(chkExcelPass.Checked) & "," & _
        "'" & SQLPrepare(_EncryptDBValue(txtExcelPass.Text)) & "'," & _
        "'" & SQLPrepare(txtWorksheetName.Text) & "'"

        Dim SQL As String = "INSERT INTO PackageOptions (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)
    End Sub
    Public Sub PackageOptions(ByVal sFormat As String, Optional ByVal nPackID As Integer = 99999)
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        SQL = "SELECT  * FROM PackageOptions WHERE PackID = " & nPackID

        oRs = clsMarsData.GetData(SQL)

        Try
            If oRs.EOF = False Then
                If Convert.ToBoolean(oRs("pdfsecurity").Value) = True Then
                    chkPDFSecurity.Checked = True
                    txtOwnerPassword.Text = IsNull(_DecryptDBValue(oRs("pdfpassword").Value))
                    txtUserPassword.Text = IsNull(_DecryptDBValue(oRs("userpassword").Value))
                    txtWatermark.Text = IsNull(oRs("pdfwatermark").Value)
                    chkPrint.Checked = Convert.ToBoolean(oRs("canprint").Value)
                    chkCopy.Checked = Convert.ToBoolean(oRs("cancopy").Value)
                    chkEdit.Checked = Convert.ToBoolean(oRs("canedit").Value)
                    chkNotes.Checked = Convert.ToBoolean(oRs("cannotes").Value)
                    chkFill.Checked = Convert.ToBoolean(oRs("canfill").Value)
                    chkAccess.Checked = Convert.ToBoolean(oRs("canaccess").Value)
                    chkAssemble.Checked = Convert.ToBoolean(oRs("canassemble").Value)
                    chkFullRes.Checked = Convert.ToBoolean(oRs("canprintfull").Value)

                    txtInfoTitle.Text = IsNull(oRs("infotitle").Value)
                    txtInfoAuthor.Text = IsNull(oRs("infoauthor").Value)
                    txtInfoSubject.Text = IsNull(oRs("infosubject").Value)
                    txtInfoKeywords.Text = IsNull(oRs("infokeywords").Value)
                    txtInfoProducer.Text = IsNull(oRs("infoproducer").Value)


                    Try
                        txtInfoCreated.Value = CType(oRs("infocreated").Value, Date)
                    Catch ex As Exception
                        txtInfoCreated.Value = Now
                    End Try
                End If

                If Convert.ToBoolean(Convert.ToInt16(IsNull(oRs("protectexcel").Value, 0))) = True Then
                    Me.chkExcelPass.Checked = True

                    Me.txtExcelPass.Text = _DecryptDBValue(IsNull(oRs("excelpassword").Value))
                End If
            End If

            oRs.Close()
        Catch : End Try

        If sFormat = "PDF" Then
            grpPDF.Visible = True
            grpPDF.BringToFront()

            pagerangeTabPanel.Enabled = False
            fileSummaryPanel.Enabled = False
        ElseIf sFormat = "XLS" Then
            grpExcel.Visible = True
            grpExcel.BringToFront()

            pagerangeTabPanel.Enabled = False
            fileSummaryPanel.Enabled = False
        End If


        If UserCancel = True Then
            Return
        End If

        Dim sCols As String
        Dim sVals As String

        clsMarsData.WriteData("DELETE FROM PackageOptions WHERE PackID =" & nPackID)

        sCols = "OptionID,PackID,PDFSecurity,PDFPassword,UserPassword,PDFWatermark,CanPrint,CanCopy,CanEdit," & _
        "CanNotes,CanFill,CanAccess,CanAssemble,CanPrintFull," & _
        "InfoTitle,InfoAuthor,InfoSubject,InfoKeyWords,InfoCreated,InfoProducer,ProtectExcel,ExcelPassword"


        sVals = clsMarsData.CreateDataID("packageoptions", "optionid") & "," & _
        nPackID & "," & _
        Convert.ToInt32(chkPDFSecurity.Checked) & "," & _
        "'" & SQLPrepare(_EncryptDBValue(txtOwnerPassword.Text)) & "'," & _
        "'" & SQLPrepare(_EncryptDBValue(txtUserPassword.Text)) & "'," & _
        "'" & SQLPrepare(txtWatermark.Text) & "'," & _
        Convert.ToInt32(chkPrint.Checked) & "," & _
        Convert.ToInt32(chkCopy.Checked) & "," & _
        Convert.ToInt32(chkEdit.Checked) & "," & _
        Convert.ToInt32(chkNotes.Checked) & "," & _
        Convert.ToInt32(chkFill.Checked) & "," & _
        Convert.ToInt32(chkAccess.Checked) & "," & _
        Convert.ToInt32(chkAssemble.Checked) & "," & _
        Convert.ToInt32(chkFullRes.Checked) & "," & _
        "'" & SQLPrepare(txtInfoTitle.Text) & "'," & _
        "'" & SQLPrepare(txtInfoAuthor.Text) & "'," & _
        "'" & SQLPrepare(txtInfoSubject.Text) & "'," & _
        "'" & SQLPrepare(txtInfoKeywords.Text) & "'," & _
        "'" & ConDateTime(txtInfoCreated.Value) & "'," & _
        "'" & SQLPrepare(txtInfoProducer.Text) & "'," & _
        Convert.ToInt32(chkExcelPass.Checked) & "," & _
        "'" & SQLPrepare(_EncryptDBValue(txtExcelPass.Text)) & "'"

        SQL = "INSERT INTO PackageOptions (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)
    End Sub

    Public Sub taskOptions(ByVal format As String, ByVal destinationID As Integer)
        pagerangeTabPanel.Enabled = False
        fileSummaryPanel.Enabled = False

        Select Case format.ToLower
            Case "ms excel 97-2000"
                grpExcel.Visible = False
                grpExcel.BringToFront()
            Case "xml"
                grpXML.Visible = True
                grpXML.BringToFront()
            Case "csv"
                grpCSV.Visible = True
                grpCSV.BringToFront()
        End Select

        Dim SQL As String = "SELECT * FROM ReportOptions WHERE destinationID = " & destinationID
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            '//csv
            Me.txtDelimiter.Text = IsNull(oRs("sdelimiter").Value, "")
            Me.txtCharacter.Text = IsNull(oRs("scharacter").Value, "")
            '//excel
            Me.txtWorksheetName.Text = IsNull(oRs("worksheetname").Value, "")
            Me.txtExcelPass.Text = _DecryptDBValue(IsNull(oRs("excelpassword").Value, ""))
            '//XML
            Select Case IsNull(oRs("xmlmode").Value, 0)
                Case 0
                    Me.optXMLIncludeSchema.Checked = True
                Case 1
                    Me.optXMLIgnoreSchema.Checked = True
                Case 2
                    Me.optXMLDiffGram.Checked = True
            End Select

            Me.chkXMLWriteHierachy.Checked = IsNull(oRs("XMLWriteHierachy").Value, 0)
            Me.txtXMLTableName.Text = IsNull(oRs("xmltablename").Value, "")
        End If



        If UserCancel = True Then Return

        clsMarsData.WriteData("DELETE FROM ReportOptions WHERE destinationID =" & destinationID)

        Dim cols, vals As String
        Dim optionID As Integer = clsMarsData.CreateDataID("reportoptions", "optionid")
        Dim xmlWriteMode As Integer = 0

        If Me.optXMLIncludeSchema.Checked Then
            xmlWriteMode = 0
        ElseIf Me.optXMLIgnoreSchema.Checked Then
            xmlWriteMode = 1
        ElseIf Me.optXMLDiffGram.Checked Then
            xmlWriteMode = 2
        End If

        cols = "optionid,destinationid,sdelimiter,scharacter,worksheetname,excelpassword,xmlmode,xmlwritehierachy,xmltablename"

        vals = optionID & "," & _
        destinationID & "," & _
        "'" & SQLPrepare(Me.txtDelimiter.Text) & "'," & _
        "'" & SQLPrepare(Me.txtCharacter.Text) & "'," & _
        "'" & SQLPrepare(Me.txtWorksheetName.Text) & "'," & _
        "'" & SQLPrepare(_EncryptDBValue(Me.txtExcelPass.Text)) & "'," & _
        xmlWriteMode & "," & _
        Convert.ToInt32(Me.chkXMLWriteHierachy.Checked) & "," & _
        "'" & SQLPrepare(Me.txtXMLTableName.Text) & "'"

        clsMarsData.DataItem.InsertData("ReportOptions", cols, vals, True)

    End Sub
    Public Sub ReportOptions(ByVal sFormat As String, ByVal ReportType As String, _
    Optional ByVal nReportID As Integer = 99999, _
    Optional ByVal nDestinationID As Integer = 99999, Optional ByVal IsDynamic As Boolean = False, _
    Optional ByVal WorkSheetName As String = "", Optional ByVal IsBursting As Boolean = False)

        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sReportColumn As String

        xFormat = sFormat.ToLower.Trim

        Dynamic = IsDynamic

        If ReportType = "Single" Then
            SQL = "SELECT * FROM ReportOptions WHERE DestinationID = " & nDestinationID
            nReportID = 0
        Else
            SQL = "SELECT * FROM ReportOptions WHERE ReportID =" & nReportID
            nDestinationID = 0
        End If

        Me.m_ReportID = nReportID
        Me.m_DestinationID = nDestinationID
        ' m_Format = sFormat
        m_ReportType = ReportType


        oRs = clsMarsData.GetData(SQL)

        Try
            If oRs.EOF = False Then
                chkPDFSecurity.Checked = IsNull(oRs("pdfsecurity").Value)
                txtOwnerPassword.Text = _DecryptDBValue(IsNull(oRs("pdfpassword").Value))
                txtUserPassword.Text = _DecryptDBValue(IsNull(oRs("userpassword").Value))
                txtWatermark.Text = IsNull(oRs("pdfwatermark").Value)
                chkPrint.Checked = Convert.ToBoolean(oRs("canprint").Value)
                chkCopy.Checked = Convert.ToBoolean(oRs("cancopy").Value)
                chkEdit.Checked = Convert.ToBoolean(oRs("canedit").Value)
                chkNotes.Checked = Convert.ToBoolean(oRs("cannotes").Value)
                chkFill.Checked = Convert.ToBoolean(oRs("canfill").Value)
                chkAccess.Checked = Convert.ToBoolean(oRs("canaccess").Value)
                chkAssemble.Checked = Convert.ToBoolean(oRs("canassemble").Value)
                chkFullRes.Checked = Convert.ToBoolean(oRs("canprintfull").Value)

                txtPageFrom.Value = oRs("pagefrom").Value
                txtPageTo.Value = oRs("pageto").Value
                txtWorksheetName.Text = IsNull(oRs("worksheetname").Value)

                If txtPageFrom.Value > 0 Or txtPageFrom.Value > 0 Then
                    optPageRange.Checked = True
                End If

                If IsNull(oRs("scharacter").Value, "") = "tab" Then
                    optTab.Checked = True
                Else
                    optCharacter.Checked = True
                    txtCharacter.Text = IsNull(oRs("scharacter").Value, "")
                End If

                txtDelimiter.Text = oRs("sdelimiter").Value
                txtTableName.Text = oRs("tablename").Value


                Try
                    chkExcelPass.Checked = Convert.ToBoolean(oRs("protectexcel").Value)
                    txtExcelPass.Text = _DecryptDBValue(oRs("excelpassword").Value)
                Catch : End Try

                Try
                    '//the same field is used for Word - Editable
                    chkInsertPageBreaksWord.Checked = Convert.ToBoolean(oRs("pdfbookmarks").Value)
                Catch : End Try

                With UcDSN
                    .cmbDSN.Text = Convert.ToString(oRs("odbc").Value).Split("|")(0)
                    .txtUserID.Text = Convert.ToString(oRs("odbc").Value).Split("|")(1)
                    .txtPassword.Text = Convert.ToString(oRs("odbc").Value).Split("|")(2)
                End With


                'for pdf and excel properties
                txtInfoTitle.Text = IsNull(oRs("infotitle").Value)
                txtInfoAuthor.Text = IsNull(oRs("infoauthor").Value)
                txtInfoSubject.Text = IsNull(oRs("infosubject").Value)
                txtInfoKeywords.Text = IsNull(oRs("infokeywords").Value)
                txtInfoProducer.Text = IsNull(oRs("infoproducer").Value)

                Try
                    txtInfoCreated.Value = CType(oRs("infocreated").Value, Date)
                Catch ex As Exception
                    txtInfoCreated.Value = Now
                End Try

                Me.chkAppendFile.Checked = IsNull(oRs("appendtofile").Value, 0)

                'for tif properties
                If IsNull(oRs("tcompression").Value) = "0" Or IsNull(oRs("tcompression").Value) = "" Or IsNull(oRs("tcompression").Value) = "Native" Then
                    chkNative.Checked = True
                Else
                    chkNative.Checked = False
                End If
                If IsNull(oRs("tcompression").Value) <> "0" And IsNull(oRs("tcompression").Value) <> "" Then
                    If IsNull(oRs("tcompression").Value) = "None" Then rbNone.Checked = True
                    If IsNull(oRs("tcompression").Value) = "LZW" Then rbLZW.Checked = True
                    If IsNull(oRs("tcompression").Value) = "MAC" Then rbMAC.Checked = True
                    If IsNull(oRs("tcompression").Value) = "RLE" Then rbRLE.Checked = True
                    If IsNull(oRs("tcompression").Value) = "FAX3" Then rbFAX3.Checked = True
                    If IsNull(oRs("tcompression").Value) = "FAX4" Then rbFAX4.Checked = True
                    If IsNull(oRs("tcompression").Value) = "CLASSF" Then rbCLASSF.Checked = True
                    If IsNull(oRs("tcompression").Value) = "CLASSF196" Then rbCLASSF196.Checked = True
                    If IsNull(oRs("tcompression").Value) = "Native" Then chkNative.Checked = True

                    cmbColor.Text = IsNull(oRs("tcolour").Value)
                    If cmbColor.Text = "" Then cmbColor.Text = "24-bit color"

                    If cmbColor.Text = "Black and White" Then
                        chkGrayScale.Checked = Convert.ToBoolean(oRs("tgrey").Value)
                    Else
                        chkGrayScale.Checked = False
                    End If

                    rbPageRange.Checked = Convert.ToBoolean(oRs("tpage").Value)
                    If rbPageRange.Checked = False Then
                        numFrom.Value = 1
                        numTo.Value = 1
                    Else
                        numFrom.Value = oRs("pagefrom").Value
                        numTo.Value = oRs("pageto").Value
                        If numFrom.Value = -1 Then numFrom.Value = 1
                        If numTo.Value = -1 Then numTo.Value = 1
                    End If
                End If


            Else
                If WorkSheetName.Length > 25 Then
                    txtWorksheetName.Text = WorkSheetName.Substring(0, 25)
                    setError(txtWorksheetName, "The worksheet name has been truncated to 25 characters")
                Else
                    txtWorksheetName.Text = WorkSheetName
                End If
            End If

            oRs.Close()
        Catch : End Try

        Select Case sFormat.ToLower.Trim
            Case "acrobat format"
                grpPDF.Visible = True
                grpPDF.BringToFront()
                fileSummaryPanel.Enabled = True
                Label13.Text = "Producer"
                txtInfoCreated.Enabled = True
            Case "csv"
                grpCSV.Visible = True
                grpCSV.BringToFront()
                fileSummaryPanel.Enabled = False
                Me.grpAppendFile.Enabled = True
                Me.grpPageRange.Enabled = False

#If CRYSTAL_VER > 11.5 Then
                If Me.chkCSVType.Checked = True Then Me.cmbcsvExportMode.Enabled = True
#End If
            Case "html"
                grpHTML.Visible = True
                grpHTML.BringToFront()
                pagerangeTabPanel.Enabled = False
                fileSummaryPanel.Enabled = False
                Me.grpAppendFile.Enabled = False
                Me.grpPageRange.Enabled = False
            Case "ms excel - data only"
                grpExcel.Visible = True
                grpExcel.BringToFront()
                fileSummaryPanel.Enabled = True
                Label13.Text = "Company"
#If CRYSTAL_VER >= 11 Then
                Me.ExcelCR11LSuperTab.Enabled = True
                pnDataOnly.Visible = True

#If CRYSTAL_VER < 12 Then
                chkSimplifyPageHeaders.Enabled = False
                chkShowGrpOutlines.Enabled = False
#End If

#Else

#End If
            Case "ms excel 97-2000"
                grpExcel.Visible = True
                grpExcel.BringToFront()
                fileSummaryPanel.Enabled = True
                Label13.Text = "Company"
#If CRYSTAL_VER > 11.5 Then
                pnDataOnly.Visible = True
#End If
            Case "ms word"
                formatOptionsPanel.Enabled = False
                stabOptions.SelectedTab = SuperTabItem2
                fileSummaryPanel.Enabled = True
                Label13.Text = "Company"
#If CRYSTAL_VER >= 12 Then
            Case "ms word - editable"
                formatOptionsPanel.Enabled = True
                grpWord.Visible = True
                fileSummaryPanel.Enabled = True
                Label13.Text = "Company"
#End If
            Case "odbc"
                grpODBC.Visible = True
                grpODBC.BringToFront()
                pagerangeTabPanel.Enabled = False
                fileSummaryPanel.Enabled = False

            Case "rich text format"
                formatOptionsPanel.Enabled = False
                stabOptions.SelectedTab = SuperTabItem2
                fileSummaryPanel.Enabled = False
            Case "text"
                grpText.Visible = True
                grpText.BringToFront()
                pagerangeTabPanel.Enabled = True
                fileSummaryPanel.Enabled = False
                Me.grpAppendFile.Enabled = True
                Me.grpPageRange.Enabled = False
            Case "ms excel 7", "ms excel 8"
                pagerangeTabPanel.Enabled = False
                grpExcel.Visible = True
                grpExcel.BringToFront()
                Label13.Text = "Company"

            Case "tiff"
                formatOptionsPanel.Enabled = True
                stabOptions.SelectedTab = SuperTabItem1
                pagerangeTabPanel.Enabled = False
                fileSummaryPanel.Enabled = False
                grpTiff.Visible = True
                grpTiff.BringToFront()
                If cmbColor.Text = "" Then cmbColor.Text = "24-bit color"
        End Select

    End Sub
    Public Sub SaveReportOptions(reportType As String, destinationID As Integer, reportID As Integer)
        Dim SQL As String
        Dim sReportColumn As String

        If UserCancel = True Then
            Return
        End If

        m_ReportType = reportType
        m_DestinationID = destinationID
        m_ReportID = reportID
        xFormat = m_format


        If m_ReportType = "Single" Then
            SQL = "DELETE FROM ReportOptions WHERE DestinationID =" & m_DestinationID
        Else
            SQL = "DELETE FROM ReportOptions WHERE ReportID =" & m_ReportID
        End If

        clsMarsData.WriteData(SQL)

        Dim sCols As String
        Dim sVals As String


        Dim sCharacter As String

        If optCharacter.Checked Then
            sCharacter = txtCharacter.Text
        Else
            sCharacter = "tab"
        End If
        'Tiff Options
        Dim sCompression As String
        If rbNone.Checked = True Then
            sCompression = "None"
        ElseIf rbLZW.Checked = True Then
            sCompression = "LZW"
        ElseIf rbMAC.Checked = True Then
            sCompression = "MAC"
        ElseIf rbRLE.Checked = True Then
            sCompression = "RLE"
        ElseIf rbFAX3.Checked = True Then
            sCompression = "FAX3"
        ElseIf rbFAX4.Checked = True Then
            sCompression = "FAX4"
        ElseIf rbCLASSF.Checked = True Then
            sCompression = "CLASSF"
        ElseIf rbCLASSF196.Checked = True Then
            sCompression = "CLASSF196"
        Else : sCompression = "0"
        End If
        If chkNative.Checked = True Then
            sCompression = "Native"
        End If

        If sCompression = "0" Then cmbColor.Text = "0"
        Dim sFrom As Integer
        Dim sTo As Integer
        If rbPageRange.Checked = True Then
            sFrom = numFrom.Value
            sTo = numTo.Value
        Else
            sFrom = -1
            sTo = -1
        End If

        Dim pdfBookmarkFieldValue

        sCols = "OptionID, PDFWaterMark,PDFPassword,DestinationID,UserPassword,CanPrint," & _
        "CanCopy,CanEdit,CanNotes,CanFill,CanAccess,CanAssemble,CanPrintFull,ReportID,HTMLMerge," & _
        "ColWidth,ExportHF,PageBreak,ConvertDate,PageFrom,PageTo,sCharacter,sDelimiter,ODBC,TableName," & _
        "LinesPerPage,PDFSecurity,HTMLNavigator,WorkSheetName," & _
        "InfoTitle,InfoAuthor,InfoSubject,InfoKeyWords,InfoCreated," & _
        "InfoProducer,ExcelBursting,ExcelBurstColumn," & _
        "ProtectExcel,ExcelPassword,KeepNoFormats,KeepDateFormats,PDFBookmarks,CharPerInch," & _
        "ExcelShowGrid,ExcelPHPF,TCompression,TColour,TGrey,TPage,AppendtoFile,RenderName,CSVEngine,TextEngine," & _
        "textdpi,usetextdriverdefault,formattingstyle,characterset,eolstyle,whitespacesize,insertbreaks,csvignoreheaders,timeperpage"

        Dim nID As Integer = clsMarsData.CreateDataID("reportoptions", "reportid")

        sVals = nID & "," & _
        "'" & SQLPrepare(txtWatermark.Text) & "'," & _
        "'" & SQLPrepare(_EncryptDBValue(txtOwnerPassword.Text)) & "'," & _
        m_DestinationID & "," & _
        "'" & SQLPrepare(_EncryptDBValue(txtUserPassword.Text)) & "'," & _
        Convert.ToInt32(chkPrint.Checked) & "," & _
        Convert.ToInt32(chkCopy.Checked) & "," & _
        Convert.ToInt32(chkEdit.Checked) & "," & _
        Convert.ToInt32(chkNotes.Checked) & "," & _
        Convert.ToInt32(chkFill.Checked) & "," & _
        Convert.ToInt32(chkAccess.Checked) & "," & _
        Convert.ToInt32(chkAssemble.Checked) & "," & _
        Convert.ToInt32(chkFullRes.Checked) & "," & _
        m_ReportID & "," & _
        0 & "," & _
        "'" & 0 & "'," & _
        0 & "," & _
        0 & "," & _
        0 & "," & _
        sFrom & "," & _
        sTo & "," & _
        "'" & SQLPrepare(m_CSVChar) & "'," & _
        "'" & SQLPrepare(Me.txtDelimiter.Text) & "'," & _
        "''," & _
        "''," & _
        0 & "," & _
        Convert.ToInt32(Me.chkPDFSecurity.Checked) & "," & _
        0 & "," & _
        "'" & SQLPrepare(Microsoft.VisualBasic.Left(txtWorksheetName.Text, 50)) & "'," & _
        "'" & SQLPrepare(txtInfoTitle.Text) & "'," & _
        "'" & SQLPrepare(txtInfoAuthor.Text) & "'," & _
        "'" & SQLPrepare(txtInfoSubject.Text) & "'," & _
        "'" & SQLPrepare(txtInfoKeywords.Text) & "'," & _
        "'" & ConDateTime(txtInfoCreated.Value) & "'," & _
        "'" & SQLPrepare(txtInfoProducer.Text) & "'," & _
        0 & "," & _
        "'" & SQLPrepare(sReportColumn) & "'," & _
        Convert.ToInt32(chkExcelPass.Checked) & "," & _
        "'" & SQLPrepare(_EncryptDBValue(txtExcelPass.Text)) & "'," & _
        0 & "," & _
        0 & "," & _
        0 & "," & _
        0 & "," & _
        Convert.ToInt32(Me.m_preserveLinks) & "," & _
        "'0'" & "," & _
        "'" & sCompression & "'," & _
        "'" & SQLPrepare(cmbColor.Text) & "'," & _
        Convert.ToInt32(chkGrayScale.Checked) & "," & _
        Convert.ToInt32(rbPageRange.Checked) & "," & _
        Convert.ToInt32(Me.chkAppendFile.Checked) & "," & _
        "'" & SQLPrepare(Me.txtrenderextension.Text) & "'," & _
        "'" & SQLPrepare(Me.cmbCSVEngine.Text) & "'," & _
        "'" & cmbTextEngine.Text & "'," & _
        txtdpi.Value & "," & _
        Convert.ToInt16(chkUseDriverDefaults.Checked) & "," & _
        "'" & cmbformattingstyle.Text & "'," & _
        m_text_characterset & "," & _
        m_text_eolstyle & "," & _
        txtwhitespacesize.Value & "," & _
        Convert.ToInt32(chkInsertBreaks.Checked) & "," & _
        Convert.ToInt32(chkIgnoreHeaders.Checked) & "," & _
        txtTimePerPage.Value

        SQL = "INSERT INTO ReportOptions (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

    End Sub


    Public Sub loadReportOptions(isDynamic As Boolean, reportType As String, nDestinationID As Integer, nReportID As Integer, isBursting As Boolean)

        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sReportColumn As String

        xFormat = m_format

        Dynamic = isDynamic

        If reportType = "Single" Then
            SQL = "SELECT * FROM ReportOptions WHERE DestinationID = " & nDestinationID
            nReportID = 0
        Else
            SQL = "SELECT * FROM ReportOptions WHERE ReportID =" & nReportID
            nDestinationID = 0
        End If

        Me.m_ReportID = nReportID
        Me.m_DestinationID = nDestinationID

        oRs = clsMarsData.GetData(SQL)

        Try
            If oRs.EOF = False Then
                chkPDFSecurity.Checked = IsNull(oRs("pdfsecurity").Value)
                txtOwnerPassword.Text = _DecryptDBValue(IsNull(oRs("pdfpassword").Value))
                txtUserPassword.Text = _DecryptDBValue(IsNull(oRs("userpassword").Value))
                txtWatermark.Text = IsNull(oRs("pdfwatermark").Value)
                chkPrint.Checked = Convert.ToBoolean(oRs("canprint").Value)
                chkCopy.Checked = Convert.ToBoolean(oRs("cancopy").Value)
                chkEdit.Checked = Convert.ToBoolean(oRs("canedit").Value)
                chkNotes.Checked = Convert.ToBoolean(oRs("cannotes").Value)
                chkFill.Checked = Convert.ToBoolean(oRs("canfill").Value)
                chkAccess.Checked = Convert.ToBoolean(oRs("canaccess").Value)
                chkAssemble.Checked = Convert.ToBoolean(oRs("canassemble").Value)
                chkFullRes.Checked = Convert.ToBoolean(oRs("canprintfull").Value)

                Try
                    txtPageFrom.Value = oRs("pagefrom").Value
                    txtPageTo.Value = oRs("pageto").Value
                Catch : End Try

                txtWorksheetName.Text = IsNull(oRs("worksheetname").Value)

                Try
                    chkMakeCrystalReadOnly.Checked = IsNull(oRs("crystalreadonly").Value, 0)
                Catch : End Try

                If txtPageFrom.Value > 0 Or txtPageFrom.Value > 0 Then
                    optPageRange.Checked = True
                End If

                If IsNull(oRs("scharacter").Value, "") = "tab" Then
                    optTab.Checked = True
                Else
                    optCharacter.Checked = True
                    txtCharacter.Text = IsNull(oRs("scharacter").Value, "")
                End If

                txtDelimiter.Text = oRs("sdelimiter").Value
                txtTableName.Text = oRs("tablename").Value


                Try
                    chkExcelPass.Checked = Convert.ToBoolean(oRs("protectexcel").Value)
                    txtExcelPass.Text = _DecryptDBValue(oRs("excelpassword").Value)
                Catch : End Try

                Try
                    '//the same field is used for Word - Editable
                    chkInsertPageBreaksWord.Checked = Convert.ToBoolean(oRs("pdfbookmarks").Value)
                Catch : End Try

                Try
                    With UcDSN
                        .cmbDSN.Text = Convert.ToString(oRs("odbc").Value).Split("|")(0)
                        .txtUserID.Text = Convert.ToString(oRs("odbc").Value).Split("|")(1)
                        .txtPassword.Text = Convert.ToString(oRs("odbc").Value).Split("|")(2)
                    End With
                Catch : End Try

                'for pdf and excel properties
                txtInfoTitle.Text = IsNull(oRs("infotitle").Value)
                txtInfoAuthor.Text = IsNull(oRs("infoauthor").Value)
                txtInfoSubject.Text = IsNull(oRs("infosubject").Value)
                txtInfoKeywords.Text = IsNull(oRs("infokeywords").Value)
                txtInfoProducer.Text = IsNull(oRs("infoproducer").Value)

                Try
                    txtInfoCreated.Value = CType(oRs("infocreated").Value, Date)
                Catch ex As Exception
                    txtInfoCreated.Value = Now
                End Try

                Me.chkAppendFile.Checked = IsNull(oRs("appendtofile").Value, 0)

                'for tif properties
                If IsNull(oRs("tcompression").Value) = "0" Or IsNull(oRs("tcompression").Value) = "" Or IsNull(oRs("tcompression").Value) = "Native" Then
                    chkNative.Checked = True
                Else
                    chkNative.Checked = False
                End If
                If IsNull(oRs("tcompression").Value) <> "0" And IsNull(oRs("tcompression").Value) <> "" Then
                    If IsNull(oRs("tcompression").Value) = "None" Then rbNone.Checked = True
                    If IsNull(oRs("tcompression").Value) = "LZW" Then rbLZW.Checked = True
                    If IsNull(oRs("tcompression").Value) = "MAC" Then rbMAC.Checked = True
                    If IsNull(oRs("tcompression").Value) = "RLE" Then rbRLE.Checked = True
                    If IsNull(oRs("tcompression").Value) = "FAX3" Then rbFAX3.Checked = True
                    If IsNull(oRs("tcompression").Value) = "FAX4" Then rbFAX4.Checked = True
                    If IsNull(oRs("tcompression").Value) = "CLASSF" Then rbCLASSF.Checked = True
                    If IsNull(oRs("tcompression").Value) = "CLASSF196" Then rbCLASSF196.Checked = True
                    If IsNull(oRs("tcompression").Value) = "Native" Then chkNative.Checked = True

                    cmbColor.Text = IsNull(oRs("tcolour").Value)
                    If cmbColor.Text = "" Then cmbColor.Text = "24-bit color"

                    If cmbColor.Text = "Black and White" Then
                        chkGrayScale.Checked = Convert.ToBoolean(oRs("tgrey").Value)
                    Else
                        chkGrayScale.Checked = False
                    End If

                    rbPageRange.Checked = Convert.ToBoolean(oRs("tpage").Value)
                    If rbPageRange.Checked = False Then
                        numFrom.Value = 1
                        numTo.Value = 1
                    Else
                        numFrom.Value = oRs("pagefrom").Value
                        numTo.Value = oRs("pageto").Value
                        If numFrom.Value = -1 Then numFrom.Value = 1
                        If numTo.Value = -1 Then numTo.Value = 1
                    End If
                End If

                Try
                    chkPDFExpiry.Checked = Convert.ToInt32(IsNull(oRs("expirepdf").Value))
                    dtPDFExpiry.Value = IsNull(oRs("pdfexpirydate").Value, Date.Now)
                Catch : End Try

                '//csv options biznitches
                Try
                    cmbCSVEngine.Text = IsNull(oRs("csvengine").Value, "Standard")
                Catch
                    cmbCSVEngine.Text = "Standard"
                End Try

                Try
                    chkIgnoreHeaders.Checked = oRs("csvIgnoreHeaders").Value
                Catch ex As Exception
                    chkIgnoreHeaders.Checked = False
                End Try

                Try
                    txtrenderextension.Text = oRs("RenderName").Value
                Catch ex As Exception
                    txtrenderextension.Text = ""
                End Try
                '//text options
                Try
                    cmbTextEngine.Text = IsNull(oRs("textengine").Value, "default")
                Catch ex As Exception
                    cmbTextEngine.Text = "Default"
                End Try

                Try
                    If cmbTextEngine.Text.ToLower <> "default" Then
                        txtdpi.Value = IsNull(oRs("textdpi").Value, 96)

                        Dim usedefaults As Boolean = IsNull(oRs("usetextdriverdefault").Value, 1)

                        chkUseDriverDefaults.Checked = usedefaults

                        If usedefaults = False Then
                            m_text_formattingstyle = IsNull(oRs("formattingstyle").Value, "Formatted")
                            m_text_characterset = IsNull(oRs("characterset").Value, 1250)
                            m_text_eolstyle = IsNull(oRs("eolstyle").Value, 0)
                            txtwhitespacesize.Value = IsNull(oRs("whitespacesize").Value, 100)
                            chkInsertBreaks.Checked = IsNull(oRs("insertbreaks").Value, 0)
                        End If

                        Try
                            txtTimePerPage.Value = IsNull(oRs("timeperpage").Value, 1)
                        Catch ex As Exception
                            txtTimePerPage.Value = 1
                        End Try
                    End If
                Catch : End Try

            End If

            oRs.Close()
        Catch : End Try

        Select Case m_format.ToLower.Trim
            Case "acrobat format"
                grpPDF.Visible = True
                grpPDF.BringToFront()
                fileSummaryPanel.Enabled = True
                Label13.Text = "Producer"
                txtInfoCreated.Enabled = True
                dtPDFExpiry.Enabled = chkPDFExpiry.Checked
                chkPDFExpiry.Enabled = True
            Case "csv"
                grpCSV.Visible = True
                grpCSV.BringToFront()
                fileSummaryPanel.Enabled = False
                Me.grpAppendFile.Enabled = True
                Me.grpPageRange.Enabled = False

            Case "html"
                grpHTML.Visible = True
                grpHTML.BringToFront()
                pagerangeTabPanel.Enabled = False
                fileSummaryPanel.Enabled = False
                Me.grpAppendFile.Enabled = False
                Me.grpPageRange.Enabled = False
            Case "ms excel - data only", "ms excel workbook - data only"
                grpExcel.Visible = True
                grpExcel.BringToFront()
                fileSummaryPanel.Enabled = True
                Label13.Text = "Company"
            Case "ms excel 97-2000", "ms excel 2007"
                grpExcel.Visible = True
                grpExcel.BringToFront()
                fileSummaryPanel.Enabled = True
                Label13.Text = "Company"
#If CRYSTAL_VER > 11.5 Then
                pnDataOnly.Visible = True
#End If
            Case "ms word", "ms word 2007"
                formatOptionsPanel.Enabled = False
                stabOptions.SelectedTab = stabOptions.Tabs(1)
                fileSummaryPanel.Enabled = True
                Label13.Text = "Company"
#If CRYSTAL_VER >= 12 Then
            Case "ms word - editable"
                formatOptionsPanel.Enabled = True
                grpWord.Visible = True
                fileSummaryPanel.Enabled = True
                Label13.Text = "Company"
#End If
            Case "odbc"
                grpODBC.Visible = True
                grpODBC.BringToFront()
                pagerangeTabPanel.Enabled = False
                fileSummaryPanel.Enabled = False

                ' cmdOK.Enabled = False
            Case "rich text format"
                formatOptionsPanel.Enabled = False
                stabOptions.SelectedTab = stabOptions.Tabs(1)
                pagerangeTabPanel.Enabled = False
            Case "text"
                grpText.Visible = True
                grpText.BringToFront()
                formatOptionsPanel.Enabled = True
                fileSummaryPanel.Enabled = False
                Me.grpAppendFile.Enabled = True
                Me.grpPageRange.Enabled = False
            Case "ms excel 7", "ms excel 8"
                pagerangeTabPanel.Enabled = False
                grpExcel.Visible = True
                grpExcel.BringToFront()
                Label13.Text = "Company"

            Case "tiff"
                formatOptionsPanel.Enabled = True
                stabOptions.SelectedTab = stabOptions.Tabs(0)
                pagerangeTabPanel.Enabled = False
                fileSummaryPanel.Enabled = False
                grpTiff.Visible = True
                grpTiff.BringToFront()
                If cmbColor.Text = "" Then cmbColor.Text = "24-bit color"
            Case "custom"
                grpCustom.Visible = True
                grpCustom.BringToFront()

        End Select

        formLoaded = True

    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        UserCancel = True
    End Sub

    Public Function isDataValid() As Boolean
        'lets validate user input
        If cmbFormat.Text = "" Then
            setError(cmbFormat, "Please select an output format")
            cmbFormat.Focus()
            Return False
        End If


        Select Case m_format.ToLower
            Case "ms excel - data only", "ms excel 97-2000"
                If chkExcelPass.Checked And txtExcelPass.Text.Length = 0 Then
                    setError(txtExcelPass, "Please provide the workbook password")
                    txtExcelPass.Focus()
                    Return False
                End If
            Case "ms excel 7", "ms excel 8"
                If chkExcelPass.Checked And txtExcelPass.Text.Length = 0 Then
                    setError(txtExcelPass, "Please provide the workbook password")
                    txtExcelPass.Focus()
                    Return False
                End If
            Case "odbc"
                With UcDSN
                    If .cmbDSN.Text.Length = 0 Then
                        setError(.cmbDSN, "Please select the datasource name")
                        .cmbDSN.Focus()
                        Return False
                    ElseIf txtTableName.Text.Length = 0 Then
                        setError(txtTableName, "Please provide a table name")
                        txtTableName.Focus()
                        Return False
                    Else
                        Try
                            Dim oRs As New ADODB.Recordset
                            Dim oCon As New ADODB.Connection

                            oCon.Open(.cmbDSN.Text, .txtUserID.Text, .txtPassword.Text)

                            oRs.Open("SELECT TOP 1 * FROM " & txtTableName.Text)

                            oRs.Close()

                            setError(txtTableName, "A table with this name already exists in the selected datasource")
                            txtTableName.Focus()
                            Return False
                        Catch : End Try
                    End If
                End With

            Case "text"

        End Select

        If optPageRange.Checked = True Then
            Try
                Int32.Parse(txtPageFrom.Value)
            Catch ex As Exception
                setError(txtPageFrom, "Please enter a valid numeric value")
                txtPageFrom.Focus()
                Return False
            End Try

            Try
                Int32.Parse(txtPageTo.Value)
            Catch ex As Exception
                setError(txtPageTo, "Please enter a valid numeric value")
                txtPageTo.Focus()
                Return False
            End Try

            If txtPageFrom.Value > txtPageTo.Value Then
                setError(txtPageTo, "The starting page cannot be greater than the end page")
                txtPageTo.Focus()
                Return False
            End If
        End If

        Return True
    End Function

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPDFSecurity.CheckedChanged
        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.pd1_AdvancedPDFPack) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, chkPDFSecurity, "Advanced PDF Pack")
            chkPDFSecurity.Checked = False
            Return
        End If

        GroupBox1.Enabled = chkPDFSecurity.Checked

    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        oField.Undo()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        oField.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        oField.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        oField.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        oField.SelectedText = String.Empty
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        oField.SelectAll()
    End Sub

    Private Sub mnuSpell_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSpell.Click
        With Speller
            .TextBoxBaseToCheck = oField
            .Modal = True
            .ModalOwner = Me
            .Check()
        End With
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(eventID)
        oInsert.m_EventBased = Me.m_eventBased
        oInsert.m_EventID = Me.eventID
        oInsert.GetConstants(Nothing)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click

        On Error Resume Next

        'If Dynamic = False Then
        Dim oItem As New frmDataItems

        oField.SelectedText = oItem._GetDataItem(Me.eventID)
        'Else
        '    Dim oIntruder As New frmInserter(0)

        '    oIntruder.GetDatabaseField(gTables, gsCon, Me)
        'End If

    End Sub

    Private Sub txtOwnerPassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtOwnerPassword.TextChanged

    End Sub

    Private Sub txtOwnerPassword_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOwnerPassword.GotFocus
        oField = txtOwnerPassword
    End Sub

    Private Sub txtUserPassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtUserPassword.TextChanged

    End Sub

    Private Sub txtUserPassword_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUserPassword.GotFocus
        oField = txtUserPassword
    End Sub


    Private Sub optAllPages_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optAllPages.CheckedChanged
        If optAllPages.Checked = True Then
            txtPageFrom.Value = 0
            txtPageFrom.Enabled = False
            txtPageTo.Value = 0
            txtPageTo.Enabled = False
        End If
    End Sub

    Private Sub optPageRange_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optPageRange.CheckedChanged
        If optPageRange.Checked = True Then
            txtPageFrom.Enabled = True
            txtPageTo.Enabled = True
        End If
    End Sub



    Private Sub optCharacter_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optCharacter.CheckedChanged
        txtCharacter.Enabled = optCharacter.Checked
    End Sub

    Private Sub cmdConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdConnect.Click
        If UcDSN._Validate = True Then
            txtTableName.Enabled = True
        Else
            _ErrorHandle("Could not connect to the selected datasource", -214789045, "frmRptOptions.ucDSN_Validate()", 1188, _
            "Please check the DSN name and the provided credentials")
        End If
    End Sub

    Private Sub txtWatermark_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWatermark.GotFocus
        oField = txtWatermark
    End Sub

    Private Sub txtWorksheetName_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWorksheetName.GotFocus
        oField = txtWorksheetName
    End Sub

    Private Sub txtInfoTitle_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInfoTitle.GotFocus
        oField = sender
    End Sub

    Private Sub txtInfoAuthor_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInfoAuthor.GotFocus
        oField = sender
    End Sub


    Private Sub txtInfoProducer_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInfoProducer.GotFocus
        oField = sender
    End Sub

    Private Sub txtInfoSubject_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInfoSubject.GotFocus
        oField = sender
    End Sub

    Private Sub txtInfoKeywords_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInfoKeywords.GotFocus
        oField = sender
    End Sub

    Private Sub txtInfoTitle_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInfoTitle.TextChanged, txtInfoAuthor.TextChanged, txtInfoKeywords.TextChanged, txtInfoProducer.TextChanged, txtInfoSubject.TextChanged, txtInfoTitle.TextChanged
        If cmbFormat.Text.Contains("pdf") Then
            If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.pd1_AdvancedPDFPack) = False Then
                _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, sender, "Advanced PPDF Pack")
                sender.text = String.Empty
                Return
            End If
        ElseIf cmbFormat.Text.Contains("xls") Then
            If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.xl1_AdvancedXLPack) = False Then
                _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, sender, "Advanced Excel Pack")
                sender.text = String.Empty
                Return
            End If
        End If

    End Sub



    Private Sub cmbGroupColumns_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub chkExcelPass_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkExcelPass.CheckedChanged
        If chkExcelPass.Checked = True And IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.xl1_AdvancedXLPack) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, chkExcelPass, "Advanced Excel Pack")
            chkExcelPass.Checked = False
            Return
        End If

        txtExcelPass.Enabled = chkExcelPass.Checked
    End Sub


    Private Sub txtWorksheetName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtWorksheetName.KeyPress

        If (Char.IsLetterOrDigit(e.KeyChar) = False) And e.KeyChar <> Chr(8) And e.KeyChar <> Chr(32) And e.KeyChar <> Chr(3) And e.KeyChar <> Chr(24) And e.KeyChar <> Chr(22) And e.KeyChar <> Chr(1) Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtWorksheetName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtWorksheetName.TextChanged
        setError(sender, "")
    End Sub

    Private Sub btnPDF_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPDF.Click
        Dim pdfStamper As frmPDFStamp = New frmPDFStamp

        With pdfStamper
            .m_Dynamic = Me.Dynamic
            .m_eventBased = Me.eventBased

            If Me.m_ReportID <> 0 Then
                txtWatermark.Text = .AddStampDtls(m_ReportID, txtWatermark.Text)
            Else
                txtWatermark.Text = .AddStampDtls(m_DestinationID, txtWatermark.Text)
            End If
        End With

    End Sub

    Private Sub txtWatermark_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtWatermark.TextChanged
        If Me.txtWatermark.Text.Length > 0 And IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.pd1_AdvancedPDFPack) = False Then
            _NeedUpgrade(gEdition.ENTERPRISEPROPLUS, txtWatermark, "Advanced PDF Pack")
            Me.txtWatermark.Text = String.Empty

        End If
    End Sub

    Private Sub chkExportHF_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
#If CRYSTAL_VER = 11 Or crystal_ver = 11.5 Then
        cmbPageHeader.Visible = chkExportHF.Checked
#End If
    End Sub

    Private Sub rbPageRange_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbPageRange.CheckedChanged
        If rbPageRange.Checked = True Then
            lblFrom.Enabled = True
            Label20.Enabled = True
            numFrom.Enabled = True
            numTo.Enabled = True
            rbAllPages.Checked = False
        End If
        If rbPageRange.Checked = False Then
            lblFrom.Enabled = False
            Label20.Enabled = False
            numFrom.Enabled = False
            numTo.Enabled = False
            rbPageRange.Checked = False
        End If
    End Sub

    Private Sub numFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles numFrom.ValueChanged
        numTo.Minimum = numFrom.Value
    End Sub


    Private Sub chkNative_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNative.CheckedChanged
        If chkNative.Checked = True Then
            GroupBox11.Enabled = False
            GroupBox8.Enabled = False
            GroupBox4.Enabled = False
        Else
            GroupBox11.Enabled = True
            GroupBox8.Enabled = True
            GroupBox4.Enabled = True
        End If
    End Sub

    Private Sub rbCLASSF_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbCLASSF.CheckedChanged
        If rbCLASSF.Checked = True Then
            cmbColor.Text = "Black and White"
            chkGrayScale.Checked = True
            GroupBox8.Enabled = False
        End If
        If chkNative.Checked = True Then
            GroupBox11.Enabled = False
            GroupBox8.Enabled = False
            GroupBox4.Enabled = False
        End If
    End Sub

    Private Sub rbCLASSF196_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbCLASSF196.CheckedChanged
        If rbCLASSF196.Checked = True Then
            cmbColor.Text = "Black and White"
            chkGrayScale.Checked = True
            GroupBox8.Enabled = False
        End If
        If chkNative.Checked = True Then
            GroupBox11.Enabled = False
            GroupBox8.Enabled = False
            GroupBox4.Enabled = False
        End If
    End Sub

    Private Sub rbRLE_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbRLE.CheckedChanged
        If rbRLE.Checked = True Then
            cmbColor.Text = "Black and White"
            chkGrayScale.Checked = True
            GroupBox8.Enabled = False
        End If
        If chkNative.Checked = True Then
            GroupBox11.Enabled = False
            GroupBox8.Enabled = False
            GroupBox4.Enabled = False
        End If
    End Sub

    Private Sub rbFAX3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbFAX3.CheckedChanged
        If rbFAX3.Checked = True Then
            cmbColor.Text = "Black and White"
            chkGrayScale.Checked = True
            GroupBox8.Enabled = False
        End If
        If chkNative.Checked = True Then
            GroupBox11.Enabled = False
            GroupBox8.Enabled = False
            GroupBox4.Enabled = False
        End If
    End Sub

    Private Sub rbFAX4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbFAX4.CheckedChanged
        If rbFAX4.Checked = True Then
            cmbColor.Text = "Black and White"
            chkGrayScale.Checked = True
            GroupBox8.Enabled = False
        End If
        If chkNative.Checked = True Then
            GroupBox11.Enabled = False
            GroupBox8.Enabled = False
            GroupBox4.Enabled = False
        End If
    End Sub

    Private Sub rbNone_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbNone.CheckedChanged
        If rbNone.Checked = True Then
            GroupBox8.Enabled = True
            GroupBox4.Enabled = True
        End If
        If chkNative.Checked = True Then
            GroupBox11.Enabled = False
            GroupBox8.Enabled = False
            GroupBox4.Enabled = False
        End If
    End Sub

    Private Sub rbLZW_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbLZW.CheckedChanged
        If rbLZW.Checked = True Then
            GroupBox8.Enabled = True
            GroupBox4.Enabled = True
        End If
        If chkNative.Checked = True Then
            GroupBox11.Enabled = False
            GroupBox8.Enabled = False
            GroupBox4.Enabled = False
        End If
    End Sub

    Private Sub rbMAC_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbMAC.CheckedChanged
        If rbMAC.Checked = True Then
            GroupBox8.Enabled = True
            GroupBox4.Enabled = True
        End If
        If chkNative.Checked = True Then
            GroupBox11.Enabled = False
            GroupBox8.Enabled = False
            GroupBox4.Enabled = False
        End If
    End Sub

    Private Sub GroupBox8_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GroupBox8.EnabledChanged
        If rbRLE.Checked = True Or rbFAX3.Checked = True Or rbFAX4.Checked = True Or rbCLASSF.Checked = True Or Me.rbCLASSF196.Checked = True Then
            cmbColor.Text = "Black and White"
            chkGrayScale.Checked = True
            GroupBox4.Enabled = False
        End If
        If chkNative.Checked = True Then GroupBox8.Enabled = False
    End Sub

    Private Sub cmbColor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbColor.SelectedIndexChanged
        If cmbColor.Text <> "Black and White" Then
            chkGrayScale.Checked = False
        Else
            chkGrayScale.Checked = True
        End If
    End Sub


    Private Sub txtExcelPass_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtExcelPass.TextChanged

    End Sub

    Private Sub cmbcsvReportPageSections_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub cmbcsvReportPageSections_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Dim cmb As ComboBox = CType(sender, ComboBox)

        If cmb.Text.ToLower <> "donotexport" And cmb.Text.ToLower <> "export" And cmb.Text.ToLower <> "exportisolated" Then
            cmb.Text = "Export"
        End If
    End Sub

    Public Sub addFormat(ByVal format As String, ByVal selected As Boolean)
        Dim exists As Boolean = False

        For Each it As String In cmbFormat.Items
            If String.Compare(it, format, True) = 0 Then
                exists = True
                Return
            End If
        Next

        If exists = False Then


            cmbFormat.Items.Add(format)

            cmbFormat.Text = format
        End If
    End Sub
    Public Sub removeFormat(ByVal format As String)
        For Each it As String In cmbFormat.Items
            If String.Compare(it, format, True) = 0 Then
                cmbFormat.Items.Remove(it)
                Exit For
            End If
        Next
    End Sub
    Public Sub setFormats(ByVal formats() As String)
        cmbFormat.Items.Clear()

        For Each sformat As String In formats
            cmbFormat.Items.Add(sformat)
        Next

        cmbFormat.Sorted = True
    End Sub
    Public Sub setFormats(ByVal sType As String, ByVal isSmart As Boolean, ByVal isQuery As Boolean)
        Dim sformats() As String

        If sType = "SMS" Then
            sformats = New String() {"CSV (*.csv)", "Tab Seperated (*.txt)", "Text (*.txt)", "Record Style (*.rec)"}
        ElseIf isSmart = True Then
            sformats = New String() {"MS Excel (*.xls)", "XML (*.xml)", "Acrobat Format (*.pdf)", "HTML (*.htm)", "JPEG (*.jpg)", "TIFF (*.tif)"}
        ElseIf isQuery = True Then
            sformats = New String() {"MS Excel 97-2000 (*.xls)", "XML (*.xml)", "CSV (*.csv)"}
        Else
#If CRYSTAL_VER >= 12 Then
             sformats = New String() {"Acrobat Format (*.pdf)", "Crystal Reports (*.rpt)", "CSV (*.csv)", _
            "Data Interchange Format (*.dif)", "dBase II (*.dbf)", "dBase III (*.dbf)", _
            "dBase IV (*.dbf)", "HTML (*.htm)", "Lotus 1-2-3 (*.wk1)", "Lotus 1-2-3 (*.wk3)", _
            "Lotus 1-2-3 (*.wk4)", "Lotus 1-2-3 (*.wks)", "MS Excel - Data Only (*.xls)", _
            "MS Excel 7 (*.xls)", "MS Excel 8 (*.xls)", "MS Excel 97-2000 (*.xls)", _
            "MS Word (*.doc)", "Rich Text Format (*.rtf)", "Tab Separated (*.txt)", _
            "Text (*.txt)", "TIFF (*.tif)", "XML (*.xml)", "Record Style (*.rec)","MS Word - Editable (*.rtf)"}
#Else
            sformats = New String() {"Acrobat Format (*.pdf)", "Crystal Reports (*.rpt)", "CSV (*.csv)", _
           "Data Interchange Format (*.dif)", "dBase II (*.dbf)", "dBase III (*.dbf)", _
           "dBase IV (*.dbf)", "HTML (*.htm)", "Lotus 1-2-3 (*.wk1)", "Lotus 1-2-3 (*.wk3)", _
           "Lotus 1-2-3 (*.wk4)", "Lotus 1-2-3 (*.wks)", "MS Excel - Data Only (*.xls)", _
           "MS Excel 7 (*.xls)", "MS Excel 8 (*.xls)", "MS Excel 97-2000 (*.xls)", _
           "MS Word (*.doc)", "Rich Text Format (*.rtf)", "Tab Separated (*.txt)", _
           "Text (*.txt)", "TIFF (*.tif)", "XML (*.xml)", "Record Style (*.rec)"}
#End If
        End If

        If sformats IsNot Nothing Then
            cmbFormat.Items.Clear()

            For Each sformat As String In sformats
                cmbFormat.Items.Add(sformat)
            Next

#If CRYSTAL_VER > 12 Then
            cmbFormat.Items.Add("MS Excel Workbook - Data Only (*.xlsx)")
#End If

            cmbFormat.Sorted = True

        End If
    End Sub

    Private Sub tvFormats_AfterNodeSelect(ByVal sender As Object, ByVal e As DevComponents.AdvTree.AdvTreeNodeEventArgs)

    End Sub

    Dim tooltip As DevComponents.DotNetBar.SuperTooltip
    Dim info As DevComponents.DotNetBar.SuperTooltipInfo

    Private Sub cmbFormat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbFormat.SelectedIndexChanged
        setError(cmbFormat, "")

        Dim sformat As String = cmbFormat.Text

        Dim groupBoxName As String = ""

        sformat = sformat.Split("(")(0).Trim.ToLower
        pagerangeTabPanel.Enabled = True
        chkPDFExpiry.Enabled = False

        If tooltip Is Nothing Then tooltip = New DevComponents.DotNetBar.SuperTooltip

        '   = New DevComponents.DotNetBar.SuperTooltipInfo("Information", "", "The format to export the report to.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Yellow)
        tooltip.SetSuperTooltip(cmbFormat, Nothing)

        Select Case sformat.ToLower.Trim
            Case "acrobat format"
                groupBoxName = "grpPDF"
                fileSummaryPanel.Enabled = True
                Label13.Text = "Producer"
                txtInfoCreated.Enabled = True
                fileSummaryPanel.Enabled = True
                grpPageRange.Enabled = True
                grpAppendFile.Enabled = False
                chkPDFExpiry.Enabled = True
            Case "csv"
                groupBoxName = "grpCSV"

                fileSummaryPanel.Enabled = False
                Me.grpAppendFile.Enabled = True
                Me.grpPageRange.Enabled = False

            Case "html"
                groupBoxName = "grpHTML"

                grpHTML.Visible = True
                grpHTML.BringToFront()

                fileSummaryPanel.Enabled = False
                Me.grpAppendFile.Enabled = False
                Me.grpPageRange.Enabled = False
            Case "ms excel 97-2000", "ms excel 2007"
                groupBoxName = "grpExcel"
                grpExcel.Visible = True
                grpExcel.BringToFront()
                fileSummaryPanel.Enabled = True

                Me.grpAppendFile.Enabled = False
                Me.grpPageRange.Enabled = False

                Label13.Text = "Company"

                If sformat.ToLower.Trim = "ms excel 2007" Then
                    info = New DevComponents.DotNetBar.SuperTooltipInfo("Information", "", "This format is only supported in SSRS 2012 or later", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Yellow)
                    tooltip.SetSuperTooltip(cmbFormat, info)
                    tooltip.ShowTooltip(cmbFormat)
                End If
            Case "ms word", "ms word 2007"
                groupBoxName = ""
                fileSummaryPanel.Enabled = True
                Label13.Text = "Company"
                grpPageRange.Enabled = True
                grpAppendFile.Enabled = False

                If sformat.ToLower.Trim = "ms word 2007" Then
                    info = New DevComponents.DotNetBar.SuperTooltipInfo("Information", "", "This format is only supported in SSRS 2012 or later", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Yellow)
                    tooltip.SetSuperTooltip(cmbFormat, info)
                    tooltip.ShowTooltip(cmbFormat)
                End If
            Case "text"
                groupBoxName = "grpText"
                grpText.Visible = True
                grpText.BringToFront()

                fileSummaryPanel.Enabled = False
                Me.grpAppendFile.Enabled = True
                Me.grpPageRange.Enabled = False

            Case "tiff"
                groupBoxName = "grpTiff"
                formatOptionsPanel.Enabled = True
                stabOptions.SelectedTab = SuperTabItem1

                Me.grpAppendFile.Enabled = False
                Me.grpPageRange.Enabled = False

                fileSummaryPanel.Enabled = False
                grpTiff.Visible = True
                grpTiff.BringToFront()
                If cmbColor.Text = "" Then cmbColor.Text = "24-bit color"
            Case "custom"
                groupBoxName = "grpCustom"
                formatOptionsPanel.Enabled = True
                stabOptions.SelectedTab = SuperTabItem1
                Me.grpCustom.Visible = True
                grpCustom.BringToFront()
            Case Else
                groupBoxName = ""
        End Select

        For Each pnl As Control In formatOptionsPanel.Controls
            If TypeOf pnl Is Panel Then
                If pnl.Name.StartsWith("grp") Then
                    If String.Compare(pnl.Name, groupBoxName, True) = 0 Then
                        pnl.Visible = True
                        pnl.Dock = DockStyle.Fill
                        pnl.BringToFront()
                    Else
                        pnl.Dock = DockStyle.Right
                        pnl.Visible = False
                    End If
                End If
            End If
        Next


    End Sub

    Private Sub chkPDFExpiry_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkPDFExpiry.CheckedChanged
        dtPDFExpiry.Enabled = chkPDFExpiry.Checked
    End Sub

    Private Sub cmbTextEngine_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cmbTextEngine.SelectedIndexChanged
        If cmbTextEngine.Text = "Default" Then
            tbMiraplacid.Visible = False
        Else
            '//try creating the driver COM object

            Try

                Dim obj As Object = CreateObject("Miraplacid.TextDriver")

                tbMiraplacid.Visible = True
            Catch ex As Exception
                If formLoaded Then
                    Dim res As DialogResult = MessageBox.Show("The Miraplacid Print Driver cannot be accessed. Are you sure you want to use this engine?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
                    If res = DialogResult.No Then
                        cmbTextEngine.Text = "Default"
                    Else
                        tbMiraplacid.Visible = True
                    End If
                Else
                    tbMiraplacid.Visible = True
                End If
            End Try


        End If
    End Sub
    Private Sub chkUseDriverDefaults_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUseDriverDefaults.CheckedChanged
        grpMiraplacid.Enabled = Not (chkUseDriverDefaults.Checked)
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Try
            Process.Start("http://www.miraplacid.com/mtd/")
        Catch : End Try
    End Sub
End Class
