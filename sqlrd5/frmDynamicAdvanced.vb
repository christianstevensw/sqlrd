Friend Class frmDynamicAdvanced
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim oData As New clsMarsData
    Dim oDSN As String
    Dim oUser As String
    Dim oPassword As String
    Dim oTable1 As String
    Dim sOp As String = " AND "
    Dim sMode As String = "simple"
    Dim sKeyColumn As String
    Dim sKeyParameter As String
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Dim UserCancel As Boolean
    Public m_eventID As Integer = 99999

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbTable As System.Windows.Forms.ComboBox
    Friend WithEvents cmbColumn As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblTable1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblTable2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents grpSimple As System.Windows.Forms.GroupBox
    Friend WithEvents lsvWhere As System.Windows.Forms.ListView
    Friend WithEvents cmdAddWhere As DevComponents.DotNetBar.ButtonX
    Friend WithEvents optAnd As System.Windows.Forms.RadioButton
    Friend WithEvents optOr As System.Windows.Forms.RadioButton
    Friend WithEvents cmbWhere As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbOperator As System.Windows.Forms.ComboBox
    Friend WithEvents cmbValue As System.Windows.Forms.ComboBox
    Friend WithEvents cmdRemoveWhere As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdAdvanced As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmbColumn1 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbColumn2 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbSelTable As System.Windows.Forms.ComboBox
    Friend WithEvents grpAdvanced As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtQuery As System.Windows.Forms.TextBox
    Friend WithEvents cmdParse As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDynamicAdvanced))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmbTable = New System.Windows.Forms.ComboBox()
        Me.cmbColumn = New System.Windows.Forms.ComboBox()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.cmbColumn1 = New System.Windows.Forms.ComboBox()
        Me.cmbColumn2 = New System.Windows.Forms.ComboBox()
        Me.lblTable1 = New DevComponents.DotNetBar.LabelX()
        Me.lblTable2 = New DevComponents.DotNetBar.LabelX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.grpSimple = New System.Windows.Forms.GroupBox()
        Me.lsvWhere = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmdAddWhere = New DevComponents.DotNetBar.ButtonX()
        Me.optAnd = New System.Windows.Forms.RadioButton()
        Me.optOr = New System.Windows.Forms.RadioButton()
        Me.cmbWhere = New System.Windows.Forms.ComboBox()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.cmbOperator = New System.Windows.Forms.ComboBox()
        Me.cmbValue = New System.Windows.Forms.ComboBox()
        Me.cmdRemoveWhere = New DevComponents.DotNetBar.ButtonX()
        Me.cmbSelTable = New System.Windows.Forms.ComboBox()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmdAdvanced = New DevComponents.DotNetBar.ButtonX()
        Me.grpAdvanced = New System.Windows.Forms.GroupBox()
        Me.txtQuery = New System.Windows.Forms.TextBox()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.cmdParse = New DevComponents.DotNetBar.ButtonX()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.GroupBox1.SuspendLayout()
        Me.grpSimple.SuspendLayout()
        Me.grpAdvanced.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmbTable)
        Me.GroupBox1.Controls.Add(Me.cmbColumn)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.cmbColumn1)
        Me.GroupBox1.Controls.Add(Me.cmbColumn2)
        Me.GroupBox1.Controls.Add(Me.lblTable1)
        Me.GroupBox1.Controls.Add(Me.lblTable2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(448, 144)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'cmbTable
        '
        Me.cmbTable.ItemHeight = 13
        Me.cmbTable.Location = New System.Drawing.Point(8, 42)
        Me.cmbTable.Name = "cmbTable"
        Me.cmbTable.Size = New System.Drawing.Size(160, 21)
        Me.cmbTable.TabIndex = 0
        '
        'cmbColumn
        '
        Me.cmbColumn.ItemHeight = 13
        Me.cmbColumn.Location = New System.Drawing.Point(288, 42)
        Me.cmbColumn.Name = "cmbColumn"
        Me.cmbColumn.Size = New System.Drawing.Size(152, 21)
        Me.cmbColumn.TabIndex = 1
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(328, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Please select the table and column that holds the required value"
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(328, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Please specify how these tables link together"
        '
        'cmbColumn1
        '
        Me.cmbColumn1.ItemHeight = 13
        Me.cmbColumn1.Location = New System.Drawing.Point(8, 112)
        Me.cmbColumn1.Name = "cmbColumn1"
        Me.cmbColumn1.Size = New System.Drawing.Size(160, 21)
        Me.cmbColumn1.TabIndex = 2
        '
        'cmbColumn2
        '
        Me.cmbColumn2.ItemHeight = 13
        Me.cmbColumn2.Location = New System.Drawing.Point(288, 112)
        Me.cmbColumn2.Name = "cmbColumn2"
        Me.cmbColumn2.Size = New System.Drawing.Size(152, 21)
        Me.cmbColumn2.TabIndex = 3
        '
        'lblTable1
        '
        '
        '
        '
        Me.lblTable1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblTable1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblTable1.Location = New System.Drawing.Point(8, 96)
        Me.lblTable1.Name = "lblTable1"
        Me.lblTable1.Size = New System.Drawing.Size(152, 16)
        Me.lblTable1.TabIndex = 0
        '
        'lblTable2
        '
        '
        '
        '
        Me.lblTable2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblTable2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblTable2.Location = New System.Drawing.Point(288, 96)
        Me.lblTable2.Name = "lblTable2"
        Me.lblTable2.Size = New System.Drawing.Size(120, 16)
        Me.lblTable2.TabIndex = 0
        '
        'Label3
        '
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(216, 114)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(24, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "="
        '
        'grpSimple
        '
        Me.grpSimple.Controls.Add(Me.lsvWhere)
        Me.grpSimple.Controls.Add(Me.cmdAddWhere)
        Me.grpSimple.Controls.Add(Me.optAnd)
        Me.grpSimple.Controls.Add(Me.optOr)
        Me.grpSimple.Controls.Add(Me.cmbWhere)
        Me.grpSimple.Controls.Add(Me.Label6)
        Me.grpSimple.Controls.Add(Me.cmbOperator)
        Me.grpSimple.Controls.Add(Me.cmbValue)
        Me.grpSimple.Controls.Add(Me.cmdRemoveWhere)
        Me.grpSimple.Controls.Add(Me.cmbSelTable)
        Me.grpSimple.Location = New System.Drawing.Point(8, 160)
        Me.grpSimple.Name = "grpSimple"
        Me.grpSimple.Size = New System.Drawing.Size(448, 208)
        Me.grpSimple.TabIndex = 1
        Me.grpSimple.TabStop = False
        '
        'lsvWhere
        '
        Me.lsvWhere.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvWhere.Location = New System.Drawing.Point(8, 112)
        Me.lsvWhere.Name = "lsvWhere"
        Me.lsvWhere.Size = New System.Drawing.Size(432, 88)
        Me.lsvWhere.TabIndex = 7
        Me.lsvWhere.UseCompatibleStateImageBehavior = False
        Me.lsvWhere.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Where"
        Me.ColumnHeader1.Width = 428
        '
        'cmdAddWhere
        '
        Me.cmdAddWhere.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAddWhere.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAddWhere.Image = CType(resources.GetObject("cmdAddWhere.Image"), System.Drawing.Image)
        Me.cmdAddWhere.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddWhere.Location = New System.Drawing.Point(288, 80)
        Me.cmdAddWhere.Name = "cmdAddWhere"
        Me.cmdAddWhere.Size = New System.Drawing.Size(40, 24)
        Me.cmdAddWhere.TabIndex = 4
        '
        'optAnd
        '
        Me.optAnd.Checked = True
        Me.optAnd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optAnd.Location = New System.Drawing.Point(184, 80)
        Me.optAnd.Name = "optAnd"
        Me.optAnd.Size = New System.Drawing.Size(48, 24)
        Me.optAnd.TabIndex = 5
        Me.optAnd.TabStop = True
        '
        'optOr
        '
        Me.optOr.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optOr.Location = New System.Drawing.Point(232, 80)
        Me.optOr.Name = "optOr"
        Me.optOr.Size = New System.Drawing.Size(40, 24)
        Me.optOr.TabIndex = 6
        Me.optOr.Text = "Or"
        '
        'cmbWhere
        '
        Me.cmbWhere.ItemHeight = 13
        Me.cmbWhere.Location = New System.Drawing.Point(8, 56)
        Me.cmbWhere.Name = "cmbWhere"
        Me.cmbWhere.Size = New System.Drawing.Size(160, 21)
        Me.cmbWhere.TabIndex = 1
        '
        'Label6
        '
        '
        '
        '
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(8, 16)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(240, 16)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Only return records where (optional)"
        '
        'cmbOperator
        '
        Me.cmbOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOperator.ItemHeight = 13
        Me.cmbOperator.Items.AddRange(New Object() {"<", "<=", "<>", "=", ">", ">=", "BEGINS WITH", "CONTAINS", "DOES NOT BEGIN WITH", "DOES NOT CONTAIN", "DOES NOT END WITH", "ENDS WITH", "IS EMPTY", "IS NOT EMPTY"})
        Me.cmbOperator.Location = New System.Drawing.Point(184, 56)
        Me.cmbOperator.Name = "cmbOperator"
        Me.cmbOperator.Size = New System.Drawing.Size(88, 21)
        Me.cmbOperator.Sorted = True
        Me.cmbOperator.TabIndex = 2
        '
        'cmbValue
        '
        Me.cmbValue.ItemHeight = 13
        Me.cmbValue.Location = New System.Drawing.Point(288, 56)
        Me.cmbValue.Name = "cmbValue"
        Me.cmbValue.Size = New System.Drawing.Size(152, 21)
        Me.cmbValue.TabIndex = 3
        '
        'cmdRemoveWhere
        '
        Me.cmdRemoveWhere.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRemoveWhere.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRemoveWhere.Image = CType(resources.GetObject("cmdRemoveWhere.Image"), System.Drawing.Image)
        Me.cmdRemoveWhere.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveWhere.Location = New System.Drawing.Point(128, 80)
        Me.cmdRemoveWhere.Name = "cmdRemoveWhere"
        Me.cmdRemoveWhere.Size = New System.Drawing.Size(40, 24)
        Me.cmdRemoveWhere.TabIndex = 8
        '
        'cmbSelTable
        '
        Me.cmbSelTable.ItemHeight = 13
        Me.cmbSelTable.Location = New System.Drawing.Point(8, 32)
        Me.cmbSelTable.Name = "cmbSelTable"
        Me.cmbSelTable.Size = New System.Drawing.Size(160, 21)
        Me.cmbSelTable.TabIndex = 0
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.Enabled = False
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(464, 16)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 48
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(464, 48)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 49
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdAdvanced
        '
        Me.cmdAdvanced.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAdvanced.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAdvanced.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAdvanced.Location = New System.Drawing.Point(464, 80)
        Me.cmdAdvanced.Name = "cmdAdvanced"
        Me.cmdAdvanced.Size = New System.Drawing.Size(75, 23)
        Me.cmdAdvanced.TabIndex = 50
        Me.cmdAdvanced.Text = "&Advanced"
        '
        'grpAdvanced
        '
        Me.grpAdvanced.Controls.Add(Me.txtQuery)
        Me.grpAdvanced.Controls.Add(Me.Label4)
        Me.grpAdvanced.Location = New System.Drawing.Point(8, 160)
        Me.grpAdvanced.Name = "grpAdvanced"
        Me.grpAdvanced.Size = New System.Drawing.Size(448, 208)
        Me.grpAdvanced.TabIndex = 24
        Me.grpAdvanced.TabStop = False
        '
        'txtQuery
        '
        Me.txtQuery.Location = New System.Drawing.Point(8, 32)
        Me.txtQuery.Multiline = True
        Me.txtQuery.Name = "txtQuery"
        Me.txtQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtQuery.Size = New System.Drawing.Size(432, 168)
        Me.txtQuery.TabIndex = 0
        Me.txtQuery.Tag = "memo"
        '
        'Label4
        '
        '
        '
        '
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(100, 16)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "SQL Query"
        '
        'cmdParse
        '
        Me.cmdParse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdParse.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdParse.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdParse.Location = New System.Drawing.Point(381, 376)
        Me.cmdParse.Name = "cmdParse"
        Me.cmdParse.Size = New System.Drawing.Size(75, 23)
        Me.cmdParse.TabIndex = 2
        Me.cmdParse.Text = "Parse"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'frmDynamicAdvanced
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(546, 408)
        Me.ControlBox = False
        Me.Controls.Add(Me.grpSimple)
        Me.Controls.Add(Me.grpAdvanced)
        Me.Controls.Add(Me.cmdParse)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdAdvanced)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmDynamicAdvanced"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Advanced Query"
        Me.GroupBox1.ResumeLayout(False)
        Me.grpSimple.ResumeLayout(False)
        Me.grpAdvanced.ResumeLayout(False)
        Me.grpAdvanced.PerformLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmDynamicAdvanced_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
        txtQuery.ContextMenu = Me.mnuInserter
        cmbValue.ContextMenu = Me.mnuInserter
    End Sub
    Public Function _AdvancedDynamicEdit(ByVal nID As Integer, ByVal sDSN As String, _
    ByVal sUser As String, ByVal sPassword As String, ByVal sKey As String, _
    ByVal sParameter As String, Optional ByVal sType As String = "Report") As String

        Dim SQL As String

        oDSN = sDSN
        oUser = sUser
        oPassword = sPassword
        sKeyColumn = sKey
        sKeyParameter = sParameter

        If sType = "Report" Then
            SQL = "SELECT * FROM DynamicLink WHERE ReportID = " & nID
        Else
            SQL = "SELECT * FROM DynamicLink WHERE PackID = " & nID
        End If


        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then Return String.Empty

        If oRs.EOF = False Then
            Try
                cmbTable.Text = oRs("valuecolumn").Value.Split(".")(0)
            Catch
                cmbTable.Text = oRs("valuecolumn").Value
            End Try

            Try
                cmbColumn.Text = oRs("valuecolumn").Value.Split(".")(1)
            Catch
                cmbColumn.Text = oRs("valuecolumn").Value
            End Try


            lblTable1.Text = IsNull(oRs("table1").Value)
            lblTable2.Text = IsNull(oRs("table2").Value)
            cmbColumn1.Text = IsNull(oRs("joincolumn1").Value)
            cmbColumn2.Text = IsNull(oRs("joincolumn2").Value)
            oTable1 = lblTable1.Text
            txtQuery.Text = IsNull(oRs("linksql").Value)
            cmbSelTable.Items.Add(lblTable1.Text)
            cmbSelTable.Items.Add(lblTable2.Text)

            Dim sWhere() As String = Convert.ToString(IsNull(oRs("wherelist").Value)).Split("|")

            If Not sWhere Is Nothing Then
                For Each s As String In sWhere
                    If s.Length > 0 Then lsvWhere.Items.Add(s)
                Next
            End If

        End If

        grpAdvanced.BringToFront()
        grpAdvanced.Visible = True

        cmdAdvanced.Text = "&Simple"
        sMode = "advanced"

        ShowDialog()

        If UserCancel = True Then Return String.Empty

        Dim sVals As String

        Dim sWhereList As String = String.Empty

        For Each lsv As ListViewItem In lsvWhere.Items
            sWhereList &= lsv.Text & "|"
        Next

        Dim fieldInfo As String

        Dim start As Integer
        Dim ends As Integer

        If txtQuery.Text.ToLower.IndexOf("distinct") > -1 Then
            start = txtQuery.Text.ToLower.IndexOf("distinct") + "distinct".Length
        Else
            start = txtQuery.Text.ToLower.IndexOf("select") + "select".Length
        End If

        ends = txtQuery.Text.ToLower.IndexOf("from")

        fieldInfo = txtQuery.Text.Substring(start, ends - start).Trim

        If fieldInfo.IndexOf(".") > -1 Then
            cmbTable.Text = fieldInfo.Split(".")(0)
            cmbColumn.Text = fieldInfo.Split(".")(1)
        End If

        sVals = "LinkSQL = '" & SQLPrepare(txtQuery.Text) & "'," & _
        "ConString ='" & SQLPrepare(oDSN & "|" & oUser & "|" & oPassword) & "'," & _
        "KeyColumn = '" & SQLPrepare(sKeyColumn) & "'," & _
        "KeyParameter = '" & SQLPrepare(sKeyParameter) & "'," & _
        "ValueColumn = '" & SQLPrepare(cmbTable.Text & "." & cmbColumn.Text) & "'," & _
        "Table1 = '" & SQLPrepare(lblTable1.Text) & "'," & _
        "Table2 = '" & SQLPrepare(lblTable2.Text) & "'," & _
        "JoinColumn1 = '" & SQLPrepare(cmbColumn1.Text) & "'," & _
        "JoinColumn2 = '" & SQLPrepare(cmbColumn2.Text) & "'," & _
        "WhereList = '" & SQLPrepare(sWhereList) & "'"

        If sType = "Report" Then
            SQL = "UPDATE DynamicLink SET " & sVals & " WHERE ReportID = " & nID
        Else
            SQL = "UPDATE DynamicLink SET " & sVals & " WHERE PackID = " & nID
        End If

        clsMarsData.WriteData(SQL)



        Return txtQuery.Text

    End Function

    Public Function _AdvancedDynamicLinking(ByVal sDSN As String, ByVal sUser As String, ByVal sPassword _
    As String, ByVal sTable1 As String, ByVal sKey As String, ByVal sParameter As String) As String


        oDSN = sDSN
        oUser = sUser
        oPassword = sPassword
        oTable1 = sTable1
        sKeyColumn = sKey
        sKeyParameter = sParameter

        lblTable1.Text = oTable1

        oData.GetTables(cmbTable, sDSN, sUser, sPassword)

        oData.GetColumns(cmbColumn1, sDSN, sTable1, sUser, sPassword)

        Me.ShowDialog()

        If UserCancel = True Then Return String.Empty

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim sWhere As String

        For Each lsv As ListViewItem In lsvWhere.Items
            sWhere &= lsv.Text & "|"
        Next

        sCols = "LinkID,ReportID,LinkSQL,ConString,KeyColumn,KeyParameter, " & _
            "ValueColumn,Table1,Table2,JoinColumn1,JoinColumn2,WhereList,PackID"


        sVals = clsMarsData.CreateDataID & "," & _
        99999 & "," & _
        "'" & SQLPrepare(txtQuery.Text) & "'," & _
        "'" & SQLPrepare(oDSN & "|" & oUser & "|" & oPassword) & "'," & _
        "'" & SQLPrepare(sKeyColumn) & "'," & _
        "'" & SQLPrepare(sKeyParameter) & "'," & _
        "'" & SQLPrepare(cmbTable.Text & "." & cmbColumn.Text) & "'," & _
        "'" & lblTable1.Text & "'," & _
        "'" & lblTable2.Text & "'," & _
        "'" & lblTable1.Text & "." & cmbColumn1.Text & "'," & _
        "'" & lblTable2.Text & "." & cmbColumn2.Text & "'," & _
        "'" & SQLPrepare(sWhere) & "',99999"


        SQL = "INSERT INTO DynamicLink (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        Return txtQuery.Text

    End Function

    Private Sub cmbTable_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTable.SelectedIndexChanged
        oData.GetColumns(cmbColumn, oDSN, cmbTable.Text, oUser, oPassword)

        oData.GetColumns(cmbColumn2, oDSN, cmbTable.Text, oUser, oPassword)

        lblTable2.Text = cmbTable.Text

        gTables(1) = cmbTable.Text

        cmbSelTable.Items.Clear()

        cmbSelTable.Items.Add(oTable1)
        cmbSelTable.Items.Add(cmbTable.Text)
    End Sub

    Private Sub cmbSelTable_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSelTable.SelectedIndexChanged
        oData.GetColumns(cmbWhere, oDSN, cmbSelTable.Text, oUser, oPassword)
    End Sub

    Private Sub cmbWhere_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbWhere.SelectedIndexChanged
        oData.ReturnDistinctValues(cmbValue, oDSN, oUser, oPassword, cmbWhere.Text, cmbSelTable.Text)
    End Sub

    Private Sub cmdAddWhere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddWhere.Click
        Dim sKey As String = ""
        Dim oItem As clsMyList
        Dim nType As Integer

        If cmbWhere.Text = "" Then
            ep.SetError(cmbWhere, "Please select the column name")
            Return
        ElseIf cmbOperator.Text = "" Then
            ep.SetError(cmbOperator, "Please select the operator")
            Return
        End If

        If lsvWhere.Items.Count > 0 Then
            sKey = sOp
        End If

        Select Case cmbOperator.Text.ToLower
            Case "begins with"
                lsvWhere.Items.Add(sKey & _
                cmbSelTable.Text & "." & cmbWhere.Text & _
                " LIKE '" & cmbValue.Text & "%'")
            Case "ends with"
                lsvWhere.Items.Add(sKey & _
                cmbSelTable.Text & "." & cmbWhere.Text & _
                " LIKE '%" & cmbValue.Text & "'")
            Case "contains"
                lsvWhere.Items.Add(sKey & _
                cmbSelTable.Text & "." & cmbWhere.Text & _
                " LIKE '%" & cmbValue.Text & "%'")
            Case "does not contain"
                lsvWhere.Items.Add(sKey & _
                cmbSelTable.Text & "." & cmbWhere.Text & _
                " NOT LIKE '%" & cmbValue.Text & "%'")
            Case "does not begin with"
                lsvWhere.Items.Add(sKey & _
                cmbSelTable.Text & "." & cmbWhere.Text & _
                " NOT LIKE '" & cmbValue.Text & "%'")
            Case "does not end with"
                lsvWhere.Items.Add(sKey & _
                cmbSelTable.Text & "." & cmbWhere.Text & _
                " NOT LIKE '%" & cmbValue.Text & "'")
            Case "is empty"

                lsvWhere.Items.Add(sKey & _
                "(" & cmbSelTable.Text & "." & cmbWhere.Text & _
                " IS NULL OR " & _
                cmbSelTable.Text & "." & cmbWhere.Text & " ='')")
            Case "is not empty"
                cmbValue.Enabled = False
                lsvWhere.Items.Add(sKey & _
                "(" & cmbSelTable.Text & "." & cmbWhere.Text & _
                " IS NOT NULL AND " & _
                cmbSelTable.Text & "." & cmbWhere.Text & " <> '')")
            Case Else

                oItem = cmbWhere.Items(cmbWhere.SelectedIndex)

                nType = oItem.ItemData

                If nType = ADODB.DataTypeEnum.adBSTR Or nType = ADODB.DataTypeEnum.adChar Or nType = ADODB.DataTypeEnum.adLongVarChar _
                       Or nType = ADODB.DataTypeEnum.adLongVarWChar Or nType = ADODB.DataTypeEnum.adVarChar Or nType = ADODB.DataTypeEnum.adVarWChar _
                       Or nType = ADODB.DataTypeEnum.adWChar Then

                    lsvWhere.Items.Add(sKey & cmbSelTable.Text & "." & cmbWhere.Text & cmbOperator.Text & "'" & cmbValue.Text & "'")
                Else
                    lsvWhere.Items.Add(sKey & cmbSelTable.Text & "." & cmbWhere.Text & cmbOperator.Text & cmbValue.Text)

                End If

        End Select

        ep.SetError(lsvWhere, "")

        Dim lsv As ListViewItem = lsvWhere.Items(0)

        If lsv.Text.ToLower.StartsWith(" and ") Then
            lsv.Text = lsv.Text.Substring(5, lsv.Text.Length - 5)
        ElseIf lsv.Text.ToLower.StartsWith(" or ") Then
            lsv.Text = lsv.Text.Substring(4, lsv.Text.Length - 4)
        End If
    End Sub

    Private Sub cmdRemoveWhere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveWhere.Click
        If lsvWhere.SelectedItems.Count = 0 Then Return

        Dim lsv As ListViewItem = lsvWhere.SelectedItems(0)

        lsvWhere.Items.Remove(lsv)

        If lsvWhere.Items.Count = 0 Then Return

        lsv = lsvWhere.Items(0)

        If lsv.Text.ToLower.StartsWith(" and ") Then
            lsv.Text = lsv.Text.Substring(5, lsv.Text.Length - 5)
        ElseIf lsv.Text.ToLower.StartsWith(" or ") Then
            lsv.Text = lsv.Text.Substring(4, lsv.Text.Length - 4)
        End If
    End Sub

    Private Sub cmdParse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdParse.Click
        If sMode = "simple" Then
            txtQuery.Text = "SELECT DISTINCT " & cmbTable.Text & "." & _
            cmbColumn.Text & " FROM " & cmbTable.Text & " INNER JOIN " & _
            lblTable1.Text & " ON " & lblTable1.Text & "." & cmbColumn1.Text & " = " & _
            lblTable2.Text & "." & cmbColumn2.Text

            If lsvWhere.Items.Count > 0 Then

                txtQuery.Text &= " WHERE "

                For Each oitem As ListViewItem In lsvWhere.Items
                    txtQuery.Text &= oitem.Text
                Next
            End If
        End If

        Dim oRs As New ADODB.Recordset
        Dim oCon As New ADODB.Connection
        Dim oRes As DialogResult

        Try
            oCon.Open(oDSN, oUser, oPassword)
            oRs.Open(txtQuery.Text, oCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

            oRes = MessageBox.Show("SQL parsed successfully! View Results?", _
            Application.ProductName, MessageBoxButtons.YesNo, _
            MessageBoxIcon.Question)

            If oRes = DialogResult.Yes Then
                Dim oResults As New frmDBResults

                oResults._ShowResults(oRs)
            End If


            Dim fieldInfo As String

            Dim start As Integer
            Dim ends As Integer

            If txtQuery.Text.ToLower.IndexOf("distinct") > -1 Then
                start = txtQuery.Text.ToLower.IndexOf("distinct") + "distinct".Length
            Else
                start = txtQuery.Text.ToLower.IndexOf("select") + "select".Length
            End If

            ends = txtQuery.Text.ToLower.IndexOf("from")

            fieldInfo = txtQuery.Text.Substring(start, ends - start).Trim

            If fieldInfo.IndexOf(".") > -1 Then
                cmbTable.Text = fieldInfo.Split(".")(0)
                cmbColumn.Text = fieldInfo.Split(".")(1)
            End If

            cmdOK.Enabled = True
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            cmdOK.Enabled = False
        End Try

    End Sub

    Private Sub cmdAdvanced_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdvanced.Click
        If cmdAdvanced.Text.ToLower = "&advanced" Then

            grpAdvanced.BringToFront()

            cmdAdvanced.Text = "&Simple"

            txtQuery.Text = "SELECT DISTINCT " & cmbTable.Text & "." & _
            cmbColumn.Text & " FROM " & cmbTable.Text & " INNER JOIN " & _
            lblTable1.Text & " ON " & lblTable1.Text & "." & cmbColumn1.Text & " = " & _
            lblTable2.Text & "." & cmbColumn2.Text

            If lsvWhere.Items.Count > 0 Then

                txtQuery.Text &= " WHERE "

                For Each oitem As ListViewItem In lsvWhere.Items
                    txtQuery.Text &= oitem.Text
                Next
            End If

            sMode = "advanced"
        Else
            If MessageBox.Show("Contiuning will lose any customization " & _
                "you might have made to the SQL Query. Continue?", _
                Application.ProductName, MessageBoxButtons.YesNo, _
                MessageBoxIcon.Exclamation) = DialogResult.No Then Return

            grpSimple.BringToFront()
            cmdAdvanced.Text = "&Advanced"
            sMode = "simple"
        End If
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Me.Close()
    End Sub

    Private Sub cmbColumn1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbColumn1.SelectedIndexChanged

    End Sub

    Private Sub cmbOperator_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbOperator.SelectedIndexChanged
        Select Case cmbOperator.Text.ToLower
            Case "is empty", "is not empty"
                cmbValue.Text = ""
                cmbValue.Enabled = False
            Case Else
                cmbValue.Enabled = True
        End Select
    End Sub
    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Undo()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectedText = ""
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim oInsert As frmInserter = New frmInserter(Me.m_eventID)

        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oData As New frmDataItems

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectedText = oData._GetDataItem(Me.m_eventID)
    End Sub
End Class
