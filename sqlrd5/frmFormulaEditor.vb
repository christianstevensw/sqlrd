Imports UrielGuy.SyntaxHighlightingTextBox
Public Class frmFormulaEditor
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim UserCancel As Boolean = True
    Dim mode As e_mode
    Dim ep As ErrorProvider = New ErrorProvider
    Friend WithEvents txtOutput As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents chkEvaluate As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Dim recordID As Integer = 0
    Friend WithEvents cmbName As System.Windows.Forms.TextBox
    Dim txtDefinition As UrielGuy.SyntaxHighlightingTextBox.SyntaxHighlightingTextBox
    Dim m_eventID As Integer = 99999
    Dim inserter As frmInserter

    Enum e_mode
        ADD = 0
        EDIT = 1
    End Enum
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdSave As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents UndoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CopyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PasteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents InsertToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SelectAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConstantsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents DatabaseFieldToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnTest As DevComponents.DotNetBar.ButtonX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmbName = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.chkEvaluate = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtOutput = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.btnTest = New DevComponents.DotNetBar.ButtonX()
        Me.mnuInserter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.UndoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.CutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CopyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PasteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.InsertToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator()
        Me.SelectAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConstantsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripSeparator()
        Me.DatabaseFieldToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmdSave = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox1.SuspendLayout()
        Me.mnuInserter.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmbName)
        Me.GroupBox1.Controls.Add(Me.Panel1)
        Me.GroupBox1.Controls.Add(Me.chkEvaluate)
        Me.GroupBox1.Controls.Add(Me.txtOutput)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.btnTest)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(440, 391)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'cmbName
        '
        Me.cmbName.Location = New System.Drawing.Point(8, 37)
        Me.cmbName.Name = "cmbName"
        Me.cmbName.Size = New System.Drawing.Size(413, 21)
        Me.cmbName.TabIndex = 5
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(8, 86)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(413, 194)
        Me.Panel1.TabIndex = 4
        '
        'chkEvaluate
        '
        '
        '
        '
        Me.chkEvaluate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkEvaluate.Location = New System.Drawing.Point(8, 287)
        Me.chkEvaluate.Name = "chkEvaluate"
        Me.chkEvaluate.Size = New System.Drawing.Size(281, 23)
        Me.chkEvaluate.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.chkEvaluate.TabIndex = 3
        Me.chkEvaluate.Text = "Evaluate functions in definition"
        '
        'txtOutput
        '
        Me.txtOutput.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtOutput.Border.Class = "TextBoxBorder"
        Me.txtOutput.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtOutput.DisabledBackColor = System.Drawing.Color.White
        Me.txtOutput.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOutput.ForeColor = System.Drawing.Color.Black
        Me.txtOutput.Location = New System.Drawing.Point(8, 315)
        Me.txtOutput.Multiline = True
        Me.txtOutput.Name = "txtOutput"
        Me.txtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtOutput.Size = New System.Drawing.Size(413, 65)
        Me.txtOutput.TabIndex = 2
        Me.txtOutput.Tag = "memo"
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Formula Name"
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Definition"
        '
        'btnTest
        '
        Me.btnTest.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnTest.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnTest.Enabled = False
        Me.btnTest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnTest.Location = New System.Drawing.Point(346, 286)
        Me.btnTest.Name = "btnTest"
        Me.btnTest.Size = New System.Drawing.Size(75, 23)
        Me.btnTest.TabIndex = 1
        Me.btnTest.Text = "&Test"
        '
        'mnuInserter
        '
        Me.mnuInserter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UndoToolStripMenuItem, Me.ToolStripMenuItem1, Me.CutToolStripMenuItem, Me.CopyToolStripMenuItem, Me.PasteToolStripMenuItem, Me.DeleteToolStripMenuItem, Me.ToolStripMenuItem2, Me.InsertToolStripMenuItem, Me.ToolStripMenuItem3, Me.SelectAllToolStripMenuItem})
        Me.mnuInserter.Name = "mnuInserter"
        Me.mnuInserter.Size = New System.Drawing.Size(123, 176)
        '
        'UndoToolStripMenuItem
        '
        Me.UndoToolStripMenuItem.Name = "UndoToolStripMenuItem"
        Me.UndoToolStripMenuItem.Size = New System.Drawing.Size(122, 22)
        Me.UndoToolStripMenuItem.Text = "Undo"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(119, 6)
        '
        'CutToolStripMenuItem
        '
        Me.CutToolStripMenuItem.Name = "CutToolStripMenuItem"
        Me.CutToolStripMenuItem.Size = New System.Drawing.Size(122, 22)
        Me.CutToolStripMenuItem.Text = "Cut"
        '
        'CopyToolStripMenuItem
        '
        Me.CopyToolStripMenuItem.Name = "CopyToolStripMenuItem"
        Me.CopyToolStripMenuItem.Size = New System.Drawing.Size(122, 22)
        Me.CopyToolStripMenuItem.Text = "Copy"
        '
        'PasteToolStripMenuItem
        '
        Me.PasteToolStripMenuItem.Name = "PasteToolStripMenuItem"
        Me.PasteToolStripMenuItem.Size = New System.Drawing.Size(122, 22)
        Me.PasteToolStripMenuItem.Text = "Paste"
        '
        'DeleteToolStripMenuItem
        '
        Me.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem"
        Me.DeleteToolStripMenuItem.Size = New System.Drawing.Size(122, 22)
        Me.DeleteToolStripMenuItem.Text = "Delete"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(119, 6)
        '
        'InsertToolStripMenuItem
        '
        Me.InsertToolStripMenuItem.Name = "InsertToolStripMenuItem"
        Me.InsertToolStripMenuItem.Size = New System.Drawing.Size(122, 22)
        Me.InsertToolStripMenuItem.Text = "Select All"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(119, 6)
        '
        'SelectAllToolStripMenuItem
        '
        Me.SelectAllToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConstantsToolStripMenuItem, Me.ToolStripMenuItem4, Me.DatabaseFieldToolStripMenuItem})
        Me.SelectAllToolStripMenuItem.Name = "SelectAllToolStripMenuItem"
        Me.SelectAllToolStripMenuItem.Size = New System.Drawing.Size(122, 22)
        Me.SelectAllToolStripMenuItem.Text = "Insert"
        Me.SelectAllToolStripMenuItem.Visible = False
        '
        'ConstantsToolStripMenuItem
        '
        Me.ConstantsToolStripMenuItem.Name = "ConstantsToolStripMenuItem"
        Me.ConstantsToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ConstantsToolStripMenuItem.Text = "Constants"
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(149, 6)
        '
        'DatabaseFieldToolStripMenuItem
        '
        Me.DatabaseFieldToolStripMenuItem.Name = "DatabaseFieldToolStripMenuItem"
        Me.DatabaseFieldToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.DatabaseFieldToolStripMenuItem.Text = "Database Field"
        '
        'cmdSave
        '
        Me.cmdSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdSave.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdSave.Enabled = False
        Me.cmdSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSave.Location = New System.Drawing.Point(454, 7)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(75, 25)
        Me.cmdSave.TabIndex = 3
        Me.cmdSave.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(454, 39)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 25)
        Me.cmdCancel.TabIndex = 2
        Me.cmdCancel.Text = "&Cancel"
        '
        'frmFormulaEditor
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(541, 392)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cmdCancel)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmFormulaEditor"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Formula Editor"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.mnuInserter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmFormulaEditor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        txtDefinition.Focus()

        Application.DoEvents()


        inserter = showInserter(Me, m_eventID)

        setupForDragAndDrop(txtDefinition)

    End Sub



    Sub addSyntaxAwareTextBox()
        txtDefinition = New UrielGuy.SyntaxHighlightingTextBox.SyntaxHighlightingTextBox
        txtDefinition.Visible = True
        Panel1.Controls.Add(txtDefinition)

        txtDefinition.Dock = DockStyle.Fill
        txtDefinition.Seperators.Add(" ")
        txtDefinition.Seperators.Add("\r")
        txtDefinition.Seperators.Add("\n")
        txtDefinition.Seperators.Add(",")
        txtDefinition.Seperators.Add(".")
        txtDefinition.Seperators.Add("-")
        txtDefinition.Seperators.Add("+")
        txtDefinition.Seperators.Add("(")
        txtDefinition.Seperators.Add(")")

        txtDefinition.WordWrap = True
        txtDefinition.ScrollBars = RichTextBoxScrollBars.Both '// & RichTextBoxScrollBars.ForcedVertical

        txtDefinition.FilterAutoComplete = True
        '/*txtDefinition.HighlightDescriptors.Add(new HighlightDescriptor("<", Color.Gray, nothing, DescriptorType.Word, DescriptorRecognition.WholeWord, true))

        'txtDefinition.HighlightDescriptors.Add(new HighlightDescriptor("<<", ">>", Color.DarkGreen, nothing, DescriptorType.ToCloseToken, DescriptorRecognition.StartsWith, false))
        Try
            Dim keywords() As String = IO.File.ReadAllLines(sAppPath & "keywords.config")


            For Each s As String In keywords
                txtDefinition.HighlightDescriptors.Add(New UrielGuy.SyntaxHighlightingTextBox.HighlightDescriptor(s, Color.Magenta, Nothing, DescriptorType.Word, DescriptorRecognition.WholeWord, True))
            Next

            txtDefinition.HighlightDescriptors.Add(New UrielGuy.SyntaxHighlightingTextBox.HighlightDescriptor("<[", ">", Color.Blue, Nothing, DescriptorType.ToCloseToken, DescriptorRecognition.StartsWith, False))
        Catch ex As Exception
            Dim keywords() As String = New String() {"datediff", "Replace", "sin", "Now", "Today", "Rnd", "Random", "midA", "substring", "Len", "Trim", "ifn", "ifd", "Format", "UCase", "LCase", "WCase", "Date", "Int", "Round", "findnumericvalue"}

            For Each s As String In keywords
                txtDefinition.HighlightDescriptors.Add(New UrielGuy.SyntaxHighlightingTextBox.HighlightDescriptor(s, Color.Magenta, Nothing, DescriptorType.Word, DescriptorRecognition.WholeWord, True))
            Next
        End Try



        txtDefinition.BorderStyle = BorderStyle.FixedSingle

        txtDefinition.Font = New System.Drawing.Font("Tahoma", 10)

        txtDefinition.ContextMenuStrip = mnuInserter
    End Sub

    Public Function EditConstant(ByVal nID As Integer) As String
        addSyntaxAwareTextBox()

        mode = e_mode.EDIT
        recordID = nID

        Dim SQL As String = "SELECT * FROM FormulaAttr WHERE formulaid =" & nID
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Me.cmbName.Text = oRs("formulaname").Value
            Me.txtDefinition.Text = oRs("formuladef").Value

            Try
                chkEvaluate.Checked = oRs("EvaluateFunctions").Value
            Catch : End Try
        End If


        Me.ShowDialog()

        If UserCancel = False Then
            SQL = "UPDATE formulaattr SET formulaname = '" & SQLPrepare(cmbName.Text) & "', formuladef ='" & SQLPrepare(Me.txtDefinition.Text) & "', EvaluateFunctions = " & Convert.ToInt16(chkEvaluate.Checked) & " WHERE formulaid =" & nID

            clsMarsData.WriteData(SQL)

            Return cmbName.Text
        Else
            Return Nothing
        End If
    End Function

    Dim m_newDataItemID As Integer

    Public ReadOnly Property newDataItemID As Integer
        Get
            Return m_newDataItemID
        End Get
    End Property

    Public Function AddConstant(Optional eventID As Integer = 99999) As String
        addSyntaxAwareTextBox()

        mode = e_mode.ADD
        Dim formulaID As Integer = clsMarsData.CreateDataID("formulaattr", "formulaid")

        recordID = formulaID
        eventID = eventID
        Me.ShowDialog()

        If UserCancel = True Then Return Nothing

        m_newDataItemID = formulaID

        Dim SQL As String
        Dim sCols, sVals As String


        sCols = "formulaid,formulaname,formuladef,evaluatefunctions"

        sVals = formulaID & "," & _
            "'" & SQLPrepare(cmbName.Text) & "'," & _
            "'" & SQLPrepare(txtDefinition.Text) & "'," & _
            Convert.ToInt16(chkEvaluate.Checked)

        If clsMarsData.DataItem.InsertData("formulaattr", sCols, sVals) = True Then
            Return cmbName.Text
        Else
            Return Nothing
        End If
    End Function
    Public Function GetName() As String
        Me.ShowDialog()

        Return cmbName.Text
    End Function
    Private Sub cmbName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim SQL As String

        SQL = "SELECT * FROM FormulaAttr WHERE FormulaName ='" & _
        SQLPrepare(cmbName.Text) & "'"

        oRs = clsMarsData.GetData(SQL)

        Try
            If oRs.EOF = False Then
                txtDefinition.Text = oRs("formuladef").Value
            End If

            oRs.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub cmbName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbName.TextChanged
        If cmbName.Text.Length = 0 Then
            cmdSave.Enabled = False
            btnTest.Enabled = False
        Else
            cmdSave.Enabled = True
            btnTest.Enabled = True
        End If
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Dim SQL As String
        Dim oData As New clsMarsData
        Dim sMsg As String

        If mode = e_mode.ADD Then
            If cmbName.Text = "" Then
                setError(cmbName, "Please enter a name for the Constant")
                cmbName.Focus()
                Return
            ElseIf clsMarsData.IsDuplicate("FormulaAttr", "FormulaName", cmbName.Text, False, recordID, "FormulaID") = True Then
                setError(cmbName, "A User Constant with this name already exists. Please use a different name")
                cmbName.Focus()
                Return
            ElseIf Me.txtDefinition.Text = "" Then
                setError(Me.txtDefinition, "Please enter the definition for this Constant")
                Me.txtDefinition.Focus()
                Return
            End If
        Else
            If cmbName.Text = "" Then
                setError(cmbName, "Please enter a name for the Constant")
                cmbName.Focus()
                Return
            ElseIf txtDefinition.Text = "" Then
                setError(Me.txtDefinition, "Please enter the definition for this Constant")
                Me.txtDefinition.Focus()
                Return
            End If
        End If

        UserCancel = False

            Close()

            'If clsMarsData.IsDuplicate("FormulaAttr", "FormulaName" _
            ', cmbName.Text, False) = True Then

            '    SQL = "UPDATE FormulaAttr SET FormulaDef = '" & _
            '    SQLPrepare(txtDefinition.Text) & "' WHERE " & _
            '    "FormulaName = '" & SQLPrepare(cmbName.Text) & "'"

            '    sMsg = "Constant updated successfully"
            'Else

            '    SQL = "INSERT INTO FormulaAttr " & _
            '    "(FormulaID,FormulaName,FormulaDef) " & _
            '    "VALUES (" & _
            '    clsMarsData.CreateDataID("formulaattr", "formulaid") & "," & _
            '    "'" & SQLPrepare(cmbName.Text) & "'," & _
            '    "'" & SQLPrepare(txtDefinition.Text) & "')"

            '    sMsg = "Constant saved successfully"
            'End If

            'If clsMarsData.WriteData(SQL) = True Then
            '    cmbName.Items.Add(cmbName.Text)

            '    MessageBox.Show(sMsg, Application.ProductName, MessageBoxButtons.OK, _
            '        MessageBoxIcon.Information)

            '    Close()
            'End If
    End Sub

    Private Sub txtDefinition_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If txtDefinition.Text.Length = 0 Then
            cmdSave.Enabled = False
        Else
            cmdSave.Enabled = True
        End If
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Close()
    End Sub




    Private Sub UndoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UndoToolStripMenuItem.Click
        txtDefinition.Undo()
    End Sub

    Private Sub CutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CutToolStripMenuItem.Click
        txtDefinition.Cut()
    End Sub

    Private Sub CopyToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CopyToolStripMenuItem.Click
        txtDefinition.Copy()
    End Sub

    Private Sub PasteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PasteToolStripMenuItem.Click
        txtDefinition.Paste()
    End Sub

    Private Sub DeleteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteToolStripMenuItem.Click
        txtDefinition.SelectedText = ""
    End Sub

    Private Sub InsertToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InsertToolStripMenuItem.Click
        txtDefinition.SelectAll()
    End Sub

    Private Sub ConstantsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConstantsToolStripMenuItem.Click
        Dim inserter As frmInserter = New frmInserter(0)
        inserter.m_HideUserConstants = True
        inserter.GetConstants(Me)
    End Sub

    Private Sub DatabaseFieldToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DatabaseFieldToolStripMenuItem.Click
        Dim oItem As New frmDataItems

        txtDefinition.SelectedText = oItem._GetDataItem(0)
    End Sub

    Private Sub btnTest_Click(sender As Object, e As EventArgs) Handles btnTest.Click
        Dim evaluator As ChristianSteven.Evaluator = New ChristianSteven.Evaluator

        Try
            Dim tmp As String = evaluator.Eval(clsMarsParser.Parser.ParseString(txtDefinition.Text))

            txtOutput.ForeColor = Color.Blue
            txtOutput.Text = "==> " & tmp
        Catch ex As Exception
            txtOutput.ForeColor = Color.Red
            txtOutput.Text = "==> Error: " & ex.ToString
        End Try
    End Sub

    Private Sub chkEvaluate_CheckedChanged(sender As Object, e As EventArgs) Handles chkEvaluate.CheckedChanged
        btnTest.Enabled = chkEvaluate.Checked
    End Sub


    Private Sub rtf_DragEnter(sender As Object, e As DragEventArgs)
        e.Effect = DragDropEffects.Copy

    End Sub
End Class
