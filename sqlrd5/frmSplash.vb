Imports Microsoft.Win32
Public Class frmSplash
    Inherits DevComponents.DotNetBar.Office2007Form
    Public IsValidated As Boolean = False
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Dim AutoLogin As Boolean
#Region " Windows Form Designer generated code "


    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblUserName As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblEdition As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblVersion As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblCompany As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblCustomerNo As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtUserID As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lblReg As DevComponents.DotNetBar.LabelX
    Friend WithEvents tmAutoClose As System.Timers.Timer
    Friend WithEvents chkRem As DevComponents.DotNetBar.Controls.CheckBoxX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSplash))
        Me.lblUserName = New DevComponents.DotNetBar.LabelX()
        Me.lblCompany = New DevComponents.DotNetBar.LabelX()
        Me.lblCustomerNo = New DevComponents.DotNetBar.LabelX()
        Me.lblReg = New DevComponents.DotNetBar.LabelX()
        Me.chkRem = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.txtUserID = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.txtPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.lblEdition = New DevComponents.DotNetBar.LabelX()
        Me.lblVersion = New DevComponents.DotNetBar.LabelX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.tmAutoClose = New System.Timers.Timer()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        CType(Me.tmAutoClose, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblUserName
        '
        '
        '
        '
        Me.lblUserName.BackgroundStyle.Class = ""
        Me.lblUserName.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblUserName.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblUserName.Location = New System.Drawing.Point(3, 3)
        Me.lblUserName.Name = "lblUserName"
        Me.lblUserName.Size = New System.Drawing.Size(224, 16)
        Me.lblUserName.TabIndex = 0
        Me.lblUserName.Text = "Beta Tester"
        '
        'lblCompany
        '
        '
        '
        '
        Me.lblCompany.BackgroundStyle.Class = ""
        Me.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblCompany.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblCompany.Location = New System.Drawing.Point(3, 25)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(224, 16)
        Me.lblCompany.TabIndex = 0
        Me.lblCompany.Text = "Beta Tester"
        '
        'lblCustomerNo
        '
        '
        '
        '
        Me.lblCustomerNo.BackgroundStyle.Class = ""
        Me.lblCustomerNo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblCustomerNo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblCustomerNo.Location = New System.Drawing.Point(233, 3)
        Me.lblCustomerNo.Name = "lblCustomerNo"
        Me.lblCustomerNo.Size = New System.Drawing.Size(224, 16)
        Me.lblCustomerNo.TabIndex = 0
        Me.lblCustomerNo.Text = "Customer #:"
        '
        'lblReg
        '
        '
        '
        '
        Me.lblReg.BackgroundStyle.Class = ""
        Me.lblReg.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblReg.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblReg.Location = New System.Drawing.Point(3, 47)
        Me.lblReg.Name = "lblReg"
        Me.lblReg.Size = New System.Drawing.Size(224, 16)
        Me.lblReg.TabIndex = 0
        Me.lblReg.Text = "Beta Tester"
        '
        'chkRem
        '
        Me.chkRem.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.chkRem.BackgroundStyle.Class = ""
        Me.chkRem.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkRem.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkRem.Location = New System.Drawing.Point(63, 57)
        Me.chkRem.Name = "chkRem"
        Me.chkRem.Size = New System.Drawing.Size(119, 17)
        Me.chkRem.TabIndex = 2
        Me.chkRem.Text = "Remember login"
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label5.BackgroundStyle.Class = ""
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(3, 30)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(54, 21)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Password"
        '
        'txtUserID
        '
        '
        '
        '
        Me.txtUserID.Border.Class = "TextBoxBorder"
        Me.txtUserID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUserID.Location = New System.Drawing.Point(63, 3)
        Me.txtUserID.Name = "txtUserID"
        Me.txtUserID.Size = New System.Drawing.Size(119, 21)
        Me.txtUserID.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label4.BackgroundStyle.Class = ""
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(3, 3)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 21)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "UserID"
        '
        'txtPassword
        '
        '
        '
        '
        Me.txtPassword.Border.Class = "TextBoxBorder"
        Me.txtPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtPassword.Location = New System.Drawing.Point(63, 30)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtPassword.Size = New System.Drawing.Size(119, 21)
        Me.txtPassword.TabIndex = 1
        '
        'lblEdition
        '
        Me.lblEdition.AutoSize = True
        Me.lblEdition.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lblEdition.BackgroundStyle.Class = ""
        Me.lblEdition.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblEdition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblEdition.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblEdition.Location = New System.Drawing.Point(2, 215)
        Me.lblEdition.Name = "lblEdition"
        Me.lblEdition.Size = New System.Drawing.Size(103, 16)
        Me.lblEdition.TabIndex = 6
        Me.lblEdition.Text = "Evaluation Edition"
        '
        'lblVersion
        '
        Me.lblVersion.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lblVersion.BackgroundStyle.Class = ""
        Me.lblVersion.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblVersion.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblVersion.Location = New System.Drawing.Point(209, 217)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(226, 12)
        Me.lblVersion.TabIndex = 6
        Me.lblVersion.Text = "Label2"
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 6.0!)
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(3, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(316, 16)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "All rights reserved. This product is protected by US and international copyright " & _
    "laws"
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label3.BackgroundStyle.Class = ""
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 6.0!)
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(504, 3)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(132, 16)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Copyright ChristianSteven Software � 2011"
        '
        'tmAutoClose
        '
        Me.tmAutoClose.Interval = 5000.0R
        Me.tmAutoClose.SynchronizingObject = Me
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.BackColor = System.Drawing.Color.White
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.32692!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70.67308!))
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel2, 1, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.Label4, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.chkRem, 1, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.txtUserID, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txtPassword, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Label5, 0, 1)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(305, 10)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 4
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(208, 116)
        Me.TableLayoutPanel2.TabIndex = 4
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(63, 80)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(131, 31)
        Me.FlowLayoutPanel2.TabIndex = 19
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(3, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(56, 23)
        Me.cmdOK.TabIndex = 3
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(65, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(57, 23)
        Me.cmdCancel.TabIndex = 4
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel1.Controls.Add(Me.lblUserName)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblCompany)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblReg)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblCustomerNo)
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(519, 12)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(175, 69)
        Me.FlowLayoutPanel1.TabIndex = 13
        Me.FlowLayoutPanel1.Visible = False
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.BackColor = System.Drawing.Color.White
        Me.TableLayoutPanel4.ColumnCount = 2
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.Label3, 1, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.Label2, 0, 1)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(0, 252)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 2
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(639, 16)
        Me.TableLayoutPanel4.TabIndex = 17
        '
        'frmSplash
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(639, 268)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.lblEdition)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.TableLayoutPanel4)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmSplash"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.tmAutoClose, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Structure logoSettings
        Dim Name As String
        Dim labelTop As Integer
        Dim formHeight As Integer
        Dim picName As String
    End Structure

    Dim helenSettings As logoSettings = New logoSettings


    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW As Object = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property
    Private Sub LoadPic()
        On Error Resume Next
        Dim LastPic As String
        Dim oUI As clsMarsUI = New clsMarsUI

        LastPic = oUI.ReadRegistry("LastPic", "0", , , True)

        Select Case LastPic
            Case "0", "10"
                lblVersion.Top = 217
                lblEdition.Top = 217
                Me.BackgroundImage = Image.FromFile(getAssetLocation("sqlrd7sarah.png"))
                oUI.SaveRegistry("LastPic", "1", , , True)
            Case "1"
                lblVersion.Top = 227
                lblEdition.Top = 227
                Me.BackgroundImage = Image.FromFile(getAssetLocation("sqlrd7helen.png"))
                oUI.SaveRegistry("LastPic", "0", , , True)
        End Select
    End Sub
    Private Sub frmSplash_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Me.Height = NicePanel1.Height
        'Me.Width = NicePanel1.Width

        LoadPic()

        Dim oFile As FileVersionInfo = FileVersionInfo.GetVersionInfo(sAppPath & "sqlrd.exe")

        lblVersion.Text = oFile.FileMajorPart & "." & oFile.FileMinorPart & " " & oFile.Comments

        lblEdition.Text = gsEdition & " Edition"

        'FormatForWinXP(Me)

        Dim oUI As New clsMarsUI

        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim UnifiedLogin As Boolean

        Try
            UnifiedLogin = oUI.ReadRegistry("UnifiedLogin", 0)
        Catch ex As Exception
            UnifiedLogin = False
        End Try


        oRs = clsMarsData.GetData("SELECT * FROM CRDUsers WHERE AutoLogin = 1")

        Try
            If Not oRs Is Nothing Then
                If oRs.EOF = False Then
                    txtUserID.Text = oRs("userid").Value

                    If IsNull(oRs("bytekey").Value, 0) = 0 Then
                        txtPassword.Text = Decrypt(oRs("password").Value, "qwer][po")
                    Else
                        txtPassword.Text = _DecryptDBValue(oRs("password").Value)
                    End If

                    chkRem.Checked = True
                    tmAutoClose.Enabled = True
                Else
                    txtUserID.Text = oUI.ReadRegistry("LastUserID", String.Empty)
                End If

                oRs.Close()
            Else
                txtUserID.Text = oUI.ReadRegistry("LastUserID", String.Empty)
            End If
        Catch : End Try

        Dim Custno As String
        Dim RegNo As String
        Dim firstName, lastName As String
        Dim UserCo As String
        Dim oSec As New clsMarsSecurity

        With oUI
            Custno = .ReadRegistry("CustNo", "", , , True)
            firstName = .ReadRegistry("RegFirstName", "", , , True)
            lastName = .ReadRegistry("RegLastName", "", , , True)
            UserCo = .ReadRegistry("RegCo", "", , , True)
            RegNo = .ReadRegistry("RegNo", "", , , True)
        End With

        If clsLicenser.m_licenser.CheckLicense(RegNo, firstName, lastName, UserCo, Custno) = False Then RegNo = "Not Licensed"

        lblCustomerNo.Text = "Customer #: " & Custno
        lblUserName.Text = firstName & " " & lastName
        lblCompany.Text = UserCo
        lblReg.Text = RegNo

        If (Custno.StartsWith("10") = False Or Custno.Length < 7) And _
       (gnEdition = 0 Or gnEdition = 10) Then
            Dim oRes As DialogResult = MessageBox.Show("You have not yet registered your " & _
            "evaluation edition.  Register now and get Free technical and " & _
            "pre-sales support", Application.ProductName, MessageBoxButtons.OKCancel, _
             MessageBoxIcon.Information)

            If oRes = Windows.Forms.DialogResult.OK Then
                clsWebProcess.Start("http://www.christiansteven.com/sqlrd/purchase/preregister.htm")
            End If
        End If

        If gsEdition = "Evaluation" Then
            lblEdition.Text = lblEdition.Text & " (" & nTimeLeft & " Days Left)"
        End If

        If UnifiedLogin = True Then
            txtUserID.Text = Environment.UserDomainName & "\" & Environment.UserName
            txtUserID.Enabled = False
            txtPassword.Enabled = False
            txtPassword.Text = "gibrishish"
            chkRem.Checked = False
            chkRem.Enabled = False
            '   tmAutoClose.Enabled = True
        End If
    End Sub


    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        IsValidated = False
        Close()
    End Sub
    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Dim oSec As New clsMarsSecurity
        Dim UnifiedLogin As Boolean
        Dim oUI As New clsMarsUI

        Try
            UnifiedLogin = oUI.ReadRegistry("UnifiedLogin", 0)
        Catch ex As Exception
            UnifiedLogin = False
        End Try

        tmAutoClose.Enabled = False

        If UnifiedLogin = False Then
            If oSec._ValidatePassword(txtUserID.Text, txtPassword.Text) = True Then
                gUser = txtUserID.Text
                CloseInStyle()
                SavePrevUser()
                IsValidated = True
                '   oMain.tmCheckUpdate.Enabled = True

                Dim oData As New clsMarsData
                Dim SQL As String

                SQL = "UPDATE CRDUsers SET [AutoLogin] =" & Convert.ToInt32(chkRem.Checked) & " WHERE [UserID] = '" & SQLPrepare(Me.txtUserID.Text) & "'"

                clsMarsData.WriteData(SQL, False)

                If chkRem.Checked = True Then
                    SQL = "UPDATE CRDUsers SET [AutoLogin] =" & 0 & " WHERE [UserID] <> '" & SQLPrepare(Me.txtUserID.Text) & "'"

                    clsMarsData.WriteData(SQL, False)
                End If
            Else
                ep.SetError(txtPassword, "Invalid login credentials provided. Please check your username and password.")
                txtPassword.Focus()
                txtPassword.SelectAll()
                IsValidated = False
            End If
        Else
            Dim sUser As String
            Dim oData As New clsMarsData
            Dim oRs As ADODB.Recordset
            Dim switchBack As DialogResult
            Dim sMsg As String = "The current NT user has not been authorized to access SQL-RD. Switch back to SQL-RD aunthentication?"

            sUser = Environment.UserDomainName & "\" & Environment.UserName


            oRs = clsMarsData.GetData("SELECT * FROM DomainAttr WHERE DomainName ='" & SQLPrepare(sUser) & "'")

            If oRs Is Nothing Then

                IsValidated = False
            ElseIf oRs.EOF = True Then
                IsValidated = False
            Else
                IsValidated = True
                gRole = oRs("userrole").Value
            End If

            If IsValidated = False Then '//check the groups added
                Dim rsTest As ADODB.Recordset = New ADODB.Recordset
                Dim userTest As clsMarsUsers = New clsMarsUsers

                rsTest = clsMarsData.GetData("SELECT * FROM domainattr WHERE domainname LIKE '%.*%'")

                If rsTest IsNot Nothing Then
                    Do While rsTest.EOF = False
                        Dim gname, dname, uname As String

                        gname = rsTest("domainname").Value
                        gname = gname.Remove(gname.Length - 2, 2) '//remove the .*
                        gname = gname.Split("\")(1) '//get the name without the domain

                        If txtUserID.Text.Contains("\") Then
                            dname = txtUserID.Text.Split("\")(0)
                            uname = txtUserID.Text.Split("\")(1)
                        Else
                            dname = ""
                            uname = txtUserID.Text
                        End If

                        If userTest.Check_If_Member_Of_AD_Group(Environment.UserName, gname, Environment.UserDomainName, "", "") = True Then
                            gRole = rsTest("userrole").Value
                            IsValidated = True
                            Exit Do
                        End If

                        rsTest.MoveNext()
                    Loop

                    rsTest.Close()
                End If
            End If

            If IsValidated = False Then
                switchBack = MessageBox.Show(sMsg, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)

                If switchBack = Windows.Forms.DialogResult.Yes Then
                    Dim oAuth As frmRemoteLogin = New frmRemoteLogin

                    If oAuth.AdminCheck = True Then
                        oUI.SaveRegistry("UnifiedLogin", 0, , , True)
                        MessageBox.Show("Please restart SQL-RD in order to log in using SQL-RD aunthentication.", _
                        Application.ProductName, MessageBoxButtons.OK)
                    End If
                End If

                CloseInStyle()
                IsValidated = False
            Else
                gUser = txtUserID.Text
                IsValidated = True
                CloseInStyle()
                '   oMain.tmCheckUpdate.Enabled = True
            End If
        End If

    End Sub
    'Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
    '    Dim oSec As New clsMarsSecurity
    '    Dim UnifiedLogin As Boolean
    '    Dim oUI As New clsMarsUI

    '    Try
    '        UnifiedLogin = oUI.ReadRegistry("UnifiedLogin", 0)
    '    Catch ex As Exception
    '        UnifiedLogin = False
    '    End Try

    '    tmAutoClose.Enabled = False

    '    If UnifiedLogin = False Then
    '        If oSec._ValidatePassword(txtUserID.Text, txtPassword.Text) = True Then
    '            gUser = txtUserID.Text
    '            CloseInStyle()
    '            SavePrevUser()
    '            IsValidated = True

    '            Dim oData As New clsMarsData
    '            Dim SQL As String

    '            SQL = "UPDATE CRDUsers SET [AutoLogin] =" & Convert.ToInt32(chkRem.Checked) & " WHERE [UserID] = '" & SQLPrepare(Me.txtUserID.Text) & "'"

    '            clsMarsData.WriteData(SQL, False)

    '            If chkRem.Checked = True Then
    '                SQL = "UPDATE CRDUsers SET [AutoLogin] =" & 0 & " WHERE [UserID] <> '" & SQLPrepare(Me.txtUserID.Text) & "'"

    '                clsMarsData.WriteData(SQL, False)
    '            End If
    '        Else
    '            SetError(txtPassword, "Invalid login credentials provided. Please check your username and password.")
    '            txtPassword.Focus()
    '            txtPassword.SelectAll()
    '            IsValidated = False
    '        End If
    '    Else
    '        Dim sUser As String
    '        Dim oData As New clsMarsData
    '        Dim oRs As ADODB.Recordset
    '        Dim switchBack As DialogResult
    '        Dim sMsg As String = "The current NT user has not been authorized to access SQL-RD. Switch back to SQL-RD aunthentication?"

    '        sUser = txtUserID.Text

    '        oRs = clsMarsData.GetData("SELECT * FROM DomainAttr WHERE DomainName ='" & SQLPrepare(sUser) & "'")

    '        If oRs Is Nothing Then
    '            switchBack = MessageBox.Show(sMsg, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)

    '            If switchBack = Windows.Forms.DialogResult.Yes Then
    '                Dim oAuth As frmRemoteLogin = New frmRemoteLogin

    '                If oAuth.AdminCheck = True Then
    '                    oUI.SaveRegistry("UnifiedLogin", 0, , , True)
    '                    MessageBox.Show("Please restart SQL-RD in order to log in using SQL-RD aunthentication.", _
    '                    Application.ProductName, MessageBoxButtons.OK)
    '                End If
    '            End If

    '            CloseInStyle()
    '            IsValidated = False
    '        ElseIf oRs.EOF = True Then
    '            switchBack = MessageBox.Show(sMsg, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)

    '            If switchBack = Windows.Forms.DialogResult.Yes Then
    '                Dim oAuth As frmRemoteLogin = New frmRemoteLogin

    '                If oAuth.AdminCheck = True Then
    '                    oUI.SaveRegistry("UnifiedLogin", 0, , , True)
    '                    MessageBox.Show("Please restart SQL-RD in order to log in using SQL-RD aunthentication.", _
    '                    Application.ProductName, MessageBoxButtons.OK)
    '                End If
    '            End If

    '            CloseInStyle()
    '            IsValidated = False

    '        Else
    '            gUser = txtUserID.Text
    '            gRole = oRs("userrole").Value
    '            IsValidated = True
    '            CloseInStyle()
    '        End If
    '    End If

    'End Sub

    Public Sub CloseInStyle()
        'Dim I As Double = 1
        'Dim nDelay As Double = 0.0625


        'While (Me.Opacity > 0)
        '    Me.Opacity -= nDelay
        '    System.Threading.Thread.Sleep(94)
        'End While


        Me.Close()
    End Sub
    Private Sub SavePrevUser()
        Dim oUI As New clsMarsUI

        oUI.SaveRegistry("LastUserID", txtUserID.Text)
    End Sub

    Private Sub lblEval_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs)
        clsWebProcess.Start("http://www.christiansteven.com/purchase_mars.htm")
    End Sub

    Private Sub tmAutoClose_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles tmAutoClose.Elapsed
        cmdOK_Click(sender, e)
        tmAutoClose.Enabled = False
    End Sub

    Private Sub txtUserID_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) _
    Handles txtUserID.GotFocus, txtPassword.GotFocus
        Dim txtBox As TextBox = CType(sender, TextBox)

        txtBox.BackColor = Color.Yellow
    End Sub

    Private Sub txtUserID_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) _
    Handles txtUserID.LostFocus, txtPassword.LostFocus
        Dim txtBox As TextBox = CType(sender, TextBox)

        txtBox.BackColor = Color.White
    End Sub

    Private Sub txtUserID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtUserID.TextChanged
        tmAutoClose.Enabled = False
    End Sub

    Private Sub txtPassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPassword.TextChanged
        tmAutoClose.Enabled = False
        SetError(txtPassword, "")
    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim sLink As String

        Try
            sLink = ReadTextFromFile(sAppPath & "link.xml")

            If sLink.Length > 0 Then
                Process.Start(sLink)
            Else
                clsWebProcess.Start("http://www.christiansteven.com/main.htm")
            End If
        Catch
            clsWebProcess.Start("http://www.christiansteven.com/main.htm")
        End Try
    End Sub


    Private Sub chkRem_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRem.CheckedChanged
        tmAutoClose.Enabled = False
    End Sub



End Class
