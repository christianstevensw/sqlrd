Imports Microsoft.Win32
Imports System.Collections.Generic
#If CRYSTAL_VER = 8 Then
imports My.Crystal85
#ElseIf CRYSTAL_VER = 9 Then
imports My.Crystal9 
#ElseIf CRYSTAL_VER = 10 Then
imports My.Crystal10 
#ElseIf CRYSTAL_VER = 11 Then
Imports My.Crystal11
#End If

Friend Class frmSingleProp
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim ReportRestore() As Boolean
    Friend WithEvents parameterController As sqlrd.ucParameterController
    Dim ep As New ErrorProvider

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdDbLoc As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtDBLoc As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdLoc As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtFolder As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdApply As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtID As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents fbg As System.Windows.Forms.FolderBrowserDialog

    Friend WithEvents txtFormula As DevComponents.DotNetBar.Controls.TextBoxX
    ' Friend WithEvents imgTools As System.Windows.Forms.ImageList
    Friend WithEvents oTask As sqlrd.ucTasks
    Friend WithEvents Label8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbTable As System.Windows.Forms.ComboBox
    Friend WithEvents cmdValidate As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmbColumn As System.Windows.Forms.ComboBox
    Friend WithEvents cmbValue As System.Windows.Forms.ComboBox
    Friend WithEvents Label21 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbKey As System.Windows.Forms.ComboBox
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents UcDest As sqlrd.ucDestination
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDesc As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKeyWord As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents optStatic As System.Windows.Forms.RadioButton
    Friend WithEvents optDynamic As System.Windows.Forms.RadioButton
    ' Friend WithEvents imgSingle As System.Windows.Forms.ImageList
    Friend WithEvents Label11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtSnapshots As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkSnapshots As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdView As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdDelete As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdExecute As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lsvSnapshots As DevComponents.DotNetBar.Controls.ListViewEx
    ' Friend WithEvents imgSnap As System.Windows.Forms.ImageList
    Friend WithEvents cmdRequery As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cmdTest As DevComponents.DotNetBar.ButtonX
    'Friend WithEvents UcBurst As sqlrd.ucBursting
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents chkStaticDest As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents btnView As DevComponents.DotNetBar.ButtonX
    Friend WithEvents tbSchedule As DevComponents.DotNetBar.TabItem
    Friend WithEvents tbGeneral As DevComponents.DotNetBar.TabItem
    Friend WithEvents tbReport As DevComponents.DotNetBar.TabItem
    Friend WithEvents tbOptions As DevComponents.DotNetBar.TabItem
    Friend WithEvents tbOutput As DevComponents.DotNetBar.TabItem
    Friend WithEvents tbHistory As DevComponents.DotNetBar.TabItem
    Friend WithEvents tbExceptions As DevComponents.DotNetBar.TabItem
    Friend WithEvents tbTasks As DevComponents.DotNetBar.TabItem
    Friend WithEvents tbLinking As DevComponents.DotNetBar.TabItem
    Friend WithEvents tbSnapshots As DevComponents.DotNetBar.TabItem
    Friend WithEvents grpDynamicTasks As System.Windows.Forms.GroupBox
    Friend WithEvents optOnce As System.Windows.Forms.RadioButton
    Friend WithEvents optAll As System.Windows.Forms.RadioButton
    Friend WithEvents txtUrl As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lsvDatasources As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents mnuDatasources As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ClearToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnRefreshDS As DevComponents.DotNetBar.ButtonX
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    '  Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents cmdTest2 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents bgWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents tbDataDriven As DevComponents.DotNetBar.TabItem
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents btnConnect As DevComponents.DotNetBar.ButtonX
    Friend WithEvents UcDSNDD As sqlrd.ucDSN
    Friend WithEvents chkGroupReports As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents grpQuery As System.Windows.Forms.GroupBox
    Friend WithEvents btnBuild As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtQuery As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents UcError As sqlrd.ucErrorHandler
    Friend WithEvents grpAutoResume As System.Windows.Forms.GroupBox
    Friend WithEvents chkAutoResume As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtCacheExpiry As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents btnClear As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmbDDKey As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents stabProperties As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabGeneral As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel7 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabDynamic As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabDataDriver As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabSnapshots As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel6 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabTasks As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel11 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents UcHistory As sqlrd.ucScheduleHistory
    Friend WithEvents tabHistory As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents UcBlankReportX1 As sqlrd.ucBlankReportX
    Friend WithEvents tabException As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel8 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabDestination As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel9 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabDatasources As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel5 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabReport As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel10 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents UcSchedule1 As sqlrd.ucSchedule
    Friend WithEvents tabSchedule As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdTestLink As DevComponents.DotNetBar.ButtonX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSingleProp))
        Me.txtDesc = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label7 = New DevComponents.DotNetBar.LabelX()
        Me.Label9 = New DevComponents.DotNetBar.LabelX()
        Me.txtKeyWord = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdDbLoc = New DevComponents.DotNetBar.ButtonX()
        Me.txtDBLoc = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdLoc = New DevComponents.DotNetBar.ButtonX()
        Me.txtFolder = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.cmdRequery = New DevComponents.DotNetBar.ButtonX()
        Me.txtFormula = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdApply = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.txtID = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.ofd = New System.Windows.Forms.OpenFileDialog()
        Me.fbg = New System.Windows.Forms.FolderBrowserDialog()
        Me.btnView = New DevComponents.DotNetBar.ButtonX()
        Me.chkStaticDest = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.UcDSN = New sqlrd.ucDSN()
        Me.cmdValidate = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnClear = New DevComponents.DotNetBar.ButtonX()
        Me.Label21 = New DevComponents.DotNetBar.LabelX()
        Me.cmbValue = New System.Windows.Forms.ComboBox()
        Me.cmbColumn = New System.Windows.Forms.ComboBox()
        Me.cmbTable = New System.Windows.Forms.ComboBox()
        Me.Label8 = New DevComponents.DotNetBar.LabelX()
        Me.cmdTestLink = New DevComponents.DotNetBar.ButtonX()
        Me.optStatic = New System.Windows.Forms.RadioButton()
        Me.cmbKey = New System.Windows.Forms.ComboBox()
        Me.optDynamic = New System.Windows.Forms.RadioButton()
        Me.cmdView = New DevComponents.DotNetBar.ButtonX()
        Me.lsvSnapshots = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label11 = New DevComponents.DotNetBar.LabelX()
        Me.txtSnapshots = New System.Windows.Forms.NumericUpDown()
        Me.chkSnapshots = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.cmdDelete = New DevComponents.DotNetBar.ButtonX()
        Me.cmdExecute = New DevComponents.DotNetBar.ButtonX()
        Me.cmdTest = New DevComponents.DotNetBar.ButtonX()
        Me.btnRefreshDS = New DevComponents.DotNetBar.ButtonX()
        Me.txtUrl = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.chkGroupReports = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.grpQuery = New System.Windows.Forms.GroupBox()
        Me.cmbDDKey = New System.Windows.Forms.ComboBox()
        Me.Label12 = New DevComponents.DotNetBar.LabelX()
        Me.btnBuild = New DevComponents.DotNetBar.ButtonX()
        Me.txtQuery = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btnConnect = New DevComponents.DotNetBar.ButtonX()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.grpAutoResume = New System.Windows.Forms.GroupBox()
        Me.chkAutoResume = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtCacheExpiry = New System.Windows.Forms.NumericUpDown()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.Label10 = New DevComponents.DotNetBar.LabelX()
        Me.cmdTest2 = New DevComponents.DotNetBar.ButtonX()
        Me.grpDynamicTasks = New System.Windows.Forms.GroupBox()
        Me.optOnce = New System.Windows.Forms.RadioButton()
        Me.optAll = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lsvDatasources = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ClearToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider()
        Me.UcDest = New sqlrd.ucDestination()
        Me.oTask = New sqlrd.ucTasks()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.bgWorker = New System.ComponentModel.BackgroundWorker()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.stabProperties = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel5 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.parameterController = New sqlrd.ucParameterController()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.tabReport = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabGeneral = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel8 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabDestination = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.UcBlankReportX1 = New sqlrd.ucBlankReportX()
        Me.UcError = New sqlrd.ucErrorHandler()
        Me.tabException = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.UcDSNDD = New sqlrd.ucDSN()
        Me.tabDataDriver = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel7 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.tabDynamic = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel10 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.UcSchedule1 = New sqlrd.ucSchedule()
        Me.tabSchedule = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel11 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.UcHistory = New sqlrd.ucScheduleHistory()
        Me.tabHistory = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel9 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabDatasources = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabSnapshots = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel6 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabTasks = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.txtSnapshots, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpQuery.SuspendLayout()
        Me.grpAutoResume.SuspendLayout()
        CType(Me.txtCacheExpiry, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpDynamicTasks.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.stabProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabProperties.SuspendLayout()
        Me.SuperTabControlPanel5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel8.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuperTabControlPanel4.SuspendLayout()
        Me.SuperTabControlPanel7.SuspendLayout()
        Me.SuperTabControlPanel10.SuspendLayout()
        Me.SuperTabControlPanel11.SuspendLayout()
        Me.SuperTabControlPanel9.SuspendLayout()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.SuperTabControlPanel6.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtDesc
        '
        Me.txtDesc.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDesc.Border.Class = "TextBoxBorder"
        Me.txtDesc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDesc.ForeColor = System.Drawing.Color.Black
        Me.HelpProvider1.SetHelpKeyword(Me.txtDesc, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtDesc, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtDesc.Location = New System.Drawing.Point(7, 200)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.HelpProvider1.SetShowHelp(Me.txtDesc, True)
        Me.txtDesc.Size = New System.Drawing.Size(500, 156)
        Me.txtDesc.TabIndex = 6
        Me.txtDesc.Tag = "memo"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label7, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label7, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(7, 184)
        Me.Label7.Name = "Label7"
        Me.HelpProvider1.SetShowHelp(Me.Label7, True)
        Me.Label7.Size = New System.Drawing.Size(208, 16)
        Me.Label7.TabIndex = 30
        Me.Label7.Text = "Description (optional)"
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label9, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label9, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(7, 361)
        Me.Label9.Name = "Label9"
        Me.HelpProvider1.SetShowHelp(Me.Label9, True)
        Me.Label9.Size = New System.Drawing.Size(208, 16)
        Me.Label9.TabIndex = 29
        Me.Label9.Text = "Keyword (optional)"
        '
        'txtKeyWord
        '
        Me.txtKeyWord.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtKeyWord.Border.Class = "TextBoxBorder"
        Me.txtKeyWord.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKeyWord.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtKeyWord, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtKeyWord, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtKeyWord.Location = New System.Drawing.Point(7, 377)
        Me.txtKeyWord.Name = "txtKeyWord"
        Me.HelpProvider1.SetShowHelp(Me.txtKeyWord, True)
        Me.txtKeyWord.Size = New System.Drawing.Size(500, 21)
        Me.txtKeyWord.TabIndex = 7
        Me.txtKeyWord.Tag = "memo"
        '
        'cmdDbLoc
        '
        Me.cmdDbLoc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdDbLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDbLoc.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.HelpProvider1.SetHelpKeyword(Me.cmdDbLoc, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdDbLoc, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdDbLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDbLoc.Location = New System.Drawing.Point(449, 154)
        Me.cmdDbLoc.Name = "cmdDbLoc"
        Me.HelpProvider1.SetShowHelp(Me.cmdDbLoc, True)
        Me.cmdDbLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdDbLoc.TabIndex = 5
        Me.cmdDbLoc.Text = "..."
        '
        'txtDBLoc
        '
        Me.txtDBLoc.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDBLoc.Border.Class = "TextBoxBorder"
        Me.txtDBLoc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDBLoc.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtDBLoc, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtDBLoc, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtDBLoc.Location = New System.Drawing.Point(7, 154)
        Me.txtDBLoc.Name = "txtDBLoc"
        Me.txtDBLoc.ReadOnly = True
        Me.HelpProvider1.SetShowHelp(Me.txtDBLoc, True)
        Me.txtDBLoc.Size = New System.Drawing.Size(429, 21)
        Me.txtDBLoc.TabIndex = 4
        '
        'cmdLoc
        '
        Me.cmdLoc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdLoc.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.HelpProvider1.SetHelpKeyword(Me.cmdLoc, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdLoc, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoc.Location = New System.Drawing.Point(451, 63)
        Me.cmdLoc.Name = "cmdLoc"
        Me.HelpProvider1.SetShowHelp(Me.cmdLoc, True)
        Me.cmdLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdLoc.TabIndex = 2
        Me.cmdLoc.Text = "..."
        '
        'txtFolder
        '
        Me.txtFolder.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtFolder.Border.Class = "TextBoxBorder"
        Me.txtFolder.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFolder.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtFolder, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtFolder, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtFolder.Location = New System.Drawing.Point(7, 63)
        Me.txtFolder.Name = "txtFolder"
        Me.txtFolder.ReadOnly = True
        Me.HelpProvider1.SetShowHelp(Me.txtFolder, True)
        Me.txtFolder.Size = New System.Drawing.Size(429, 21)
        Me.txtFolder.TabIndex = 1
        Me.txtFolder.Tag = "1"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label4, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label4, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(7, 47)
        Me.Label4.Name = "Label4"
        Me.HelpProvider1.SetShowHelp(Me.Label4, True)
        Me.Label4.Size = New System.Drawing.Size(208, 16)
        Me.Label4.TabIndex = 18
        Me.Label4.Text = "Create In"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label3, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label3, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(7, 138)
        Me.Label3.Name = "Label3"
        Me.HelpProvider1.SetShowHelp(Me.Label3, True)
        Me.Label3.Size = New System.Drawing.Size(208, 16)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Report Location"
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtName, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtName, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtName.Location = New System.Drawing.Point(7, 23)
        Me.txtName.Name = "txtName"
        Me.HelpProvider1.SetShowHelp(Me.txtName, True)
        Me.txtName.Size = New System.Drawing.Size(500, 21)
        Me.txtName.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label2, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label2, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(7, 7)
        Me.Label2.Name = "Label2"
        Me.HelpProvider1.SetShowHelp(Me.Label2, True)
        Me.Label2.Size = New System.Drawing.Size(208, 16)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Schedule Name"
        '
        'cmdRequery
        '
        Me.cmdRequery.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRequery.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.HelpProvider1.SetHelpKeyword(Me.cmdRequery, "Report.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdRequery, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdRequery.Image = CType(resources.GetObject("cmdRequery.Image"), System.Drawing.Image)
        Me.cmdRequery.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRequery.Location = New System.Drawing.Point(612, 14)
        Me.cmdRequery.Name = "cmdRequery"
        Me.HelpProvider1.SetShowHelp(Me.cmdRequery, True)
        Me.cmdRequery.Size = New System.Drawing.Size(72, 23)
        Me.cmdRequery.TabIndex = 3
        '
        'txtFormula
        '
        Me.txtFormula.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtFormula.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFormula.ForeColor = System.Drawing.Color.Black
        Me.txtFormula.Location = New System.Drawing.Point(80, 360)
        Me.txtFormula.Name = "txtFormula"
        Me.txtFormula.Size = New System.Drawing.Size(24, 15)
        Me.txtFormula.TabIndex = 25
        Me.txtFormula.Visible = False
        '
        'cmdApply
        '
        Me.cmdApply.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdApply.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdApply.Enabled = False
        Me.cmdApply.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdApply.Location = New System.Drawing.Point(748, 3)
        Me.cmdApply.Name = "cmdApply"
        Me.cmdApply.Size = New System.Drawing.Size(75, 23)
        Me.cmdApply.TabIndex = 49
        Me.cmdApply.Text = "&Apply"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(667, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 48
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(586, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        '
        'txtID
        '
        Me.txtID.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtID.ForeColor = System.Drawing.Color.Black
        Me.txtID.Location = New System.Drawing.Point(48, 360)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(24, 15)
        Me.txtID.TabIndex = 10
        Me.txtID.Visible = False
        '
        'btnView
        '
        Me.btnView.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnView.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.HelpProvider1.SetHelpKeyword(Me.btnView, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.btnView, System.Windows.Forms.HelpNavigator.Topic)
        Me.btnView.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnView.Location = New System.Drawing.Point(385, 45)
        Me.btnView.Name = "btnView"
        Me.HelpProvider1.SetShowHelp(Me.btnView, True)
        Me.btnView.Size = New System.Drawing.Size(37, 16)
        Me.btnView.TabIndex = 4
        Me.btnView.Text = "..."
        '
        'chkStaticDest
        '
        Me.chkStaticDest.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkStaticDest.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.chkStaticDest, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.chkStaticDest, System.Windows.Forms.HelpNavigator.Topic)
        Me.chkStaticDest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkStaticDest.Location = New System.Drawing.Point(13, 80)
        Me.chkStaticDest.Name = "chkStaticDest"
        Me.HelpProvider1.SetShowHelp(Me.chkStaticDest, True)
        Me.chkStaticDest.Size = New System.Drawing.Size(376, 24)
        Me.chkStaticDest.TabIndex = 5
        Me.chkStaticDest.Text = "Use a static destination for this dynamic schedule"
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox6.Controls.Add(Me.UcDSN)
        Me.GroupBox6.Controls.Add(Me.cmdValidate)
        Me.GroupBox6.Controls.Add(Me.GroupBox2)
        Me.GroupBox6.Location = New System.Drawing.Point(13, 98)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(416, 288)
        Me.GroupBox6.TabIndex = 17
        Me.GroupBox6.TabStop = False
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.Color.Transparent
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.HelpProvider1.SetHelpKeyword(Me.UcDSN, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.UcDSN, System.Windows.Forms.HelpNavigator.Topic)
        Me.UcDSN.Location = New System.Drawing.Point(8, 16)
        Me.UcDSN.m_conString = "|||"
        Me.UcDSN.m_showConnectionType = False
        Me.UcDSN.Name = "UcDSN"
        Me.HelpProvider1.SetShowHelp(Me.UcDSN, True)
        Me.UcDSN.Size = New System.Drawing.Size(384, 112)
        Me.UcDSN.TabIndex = 6
        '
        'cmdValidate
        '
        Me.cmdValidate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdValidate.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.HelpProvider1.SetHelpKeyword(Me.cmdValidate, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdValidate, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdValidate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdValidate.Location = New System.Drawing.Point(168, 136)
        Me.cmdValidate.Name = "cmdValidate"
        Me.HelpProvider1.SetShowHelp(Me.cmdValidate, True)
        Me.cmdValidate.Size = New System.Drawing.Size(75, 23)
        Me.cmdValidate.TabIndex = 7
        Me.cmdValidate.Text = "Connect..."
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnClear)
        Me.GroupBox2.Controls.Add(Me.Label21)
        Me.GroupBox2.Controls.Add(Me.cmbValue)
        Me.GroupBox2.Controls.Add(Me.cmbColumn)
        Me.GroupBox2.Controls.Add(Me.cmbTable)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.cmdTestLink)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 160)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(400, 112)
        Me.GroupBox2.TabIndex = 15
        Me.GroupBox2.TabStop = False
        '
        'btnClear
        '
        Me.btnClear.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnClear.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnClear.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnClear.Location = New System.Drawing.Point(277, 81)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(75, 21)
        Me.btnClear.TabIndex = 12
        Me.btnClear.Text = "Clear"
        '
        'Label21
        '
        '
        '
        '
        Me.Label21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label21, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label21, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label21.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label21.Location = New System.Drawing.Point(8, 64)
        Me.Label21.Name = "Label21"
        Me.HelpProvider1.SetShowHelp(Me.Label21, True)
        Me.Label21.Size = New System.Drawing.Size(352, 16)
        Me.Label21.TabIndex = 9
        Me.Label21.Text = "Please select the column that holds the required xxx"
        '
        'cmbValue
        '
        Me.cmbValue.Enabled = False
        Me.HelpProvider1.SetHelpKeyword(Me.cmbValue, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmbValue, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmbValue.ItemHeight = 13
        Me.cmbValue.Location = New System.Drawing.Point(8, 80)
        Me.cmbValue.Name = "cmbValue"
        Me.HelpProvider1.SetShowHelp(Me.cmbValue, True)
        Me.cmbValue.Size = New System.Drawing.Size(168, 21)
        Me.cmbValue.TabIndex = 10
        '
        'cmbColumn
        '
        Me.cmbColumn.Enabled = False
        Me.HelpProvider1.SetHelpKeyword(Me.cmbColumn, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmbColumn, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmbColumn.ItemHeight = 13
        Me.cmbColumn.Location = New System.Drawing.Point(184, 32)
        Me.cmbColumn.Name = "cmbColumn"
        Me.HelpProvider1.SetShowHelp(Me.cmbColumn, True)
        Me.cmbColumn.Size = New System.Drawing.Size(168, 21)
        Me.cmbColumn.TabIndex = 9
        '
        'cmbTable
        '
        Me.cmbTable.Enabled = False
        Me.HelpProvider1.SetHelpKeyword(Me.cmbTable, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmbTable, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmbTable.ItemHeight = 13
        Me.cmbTable.Location = New System.Drawing.Point(8, 32)
        Me.cmbTable.Name = "cmbTable"
        Me.HelpProvider1.SetShowHelp(Me.cmbTable, True)
        Me.cmbTable.Size = New System.Drawing.Size(168, 21)
        Me.cmbTable.TabIndex = 8
        '
        'Label8
        '
        '
        '
        '
        Me.Label8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label8, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label8, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(8, 16)
        Me.Label8.Name = "Label8"
        Me.HelpProvider1.SetShowHelp(Me.Label8, True)
        Me.Label8.Size = New System.Drawing.Size(384, 16)
        Me.Label8.TabIndex = 10
        Me.Label8.Text = "Please select the table and column that must equal the provided parameter"
        '
        'cmdTestLink
        '
        Me.cmdTestLink.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdTestLink.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.HelpProvider1.SetHelpKeyword(Me.cmdTestLink, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdTestLink, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdTestLink.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTestLink.Location = New System.Drawing.Point(184, 80)
        Me.cmdTestLink.Name = "cmdTestLink"
        Me.HelpProvider1.SetShowHelp(Me.cmdTestLink, True)
        Me.cmdTestLink.Size = New System.Drawing.Size(75, 21)
        Me.cmdTestLink.TabIndex = 11
        Me.cmdTestLink.Text = "Test"
        '
        'optStatic
        '
        Me.optStatic.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.optStatic, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.optStatic, System.Windows.Forms.HelpNavigator.Topic)
        Me.optStatic.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optStatic.Location = New System.Drawing.Point(13, 36)
        Me.optStatic.Name = "optStatic"
        Me.HelpProvider1.SetShowHelp(Me.optStatic, True)
        Me.optStatic.Size = New System.Drawing.Size(147, 29)
        Me.optStatic.TabIndex = 2
        Me.optStatic.TabStop = True
        Me.optStatic.Text = "Populate from static data"
        Me.optStatic.UseVisualStyleBackColor = False
        '
        'cmbKey
        '
        Me.cmbKey.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.HelpProvider1.SetHelpKeyword(Me.cmbKey, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmbKey, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmbKey.ItemHeight = 13
        Me.cmbKey.Location = New System.Drawing.Point(94, 8)
        Me.cmbKey.Name = "cmbKey"
        Me.HelpProvider1.SetShowHelp(Me.cmbKey, True)
        Me.cmbKey.Size = New System.Drawing.Size(191, 21)
        Me.cmbKey.TabIndex = 1
        '
        'optDynamic
        '
        Me.optDynamic.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.optDynamic, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.optDynamic, System.Windows.Forms.HelpNavigator.Topic)
        Me.optDynamic.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDynamic.Location = New System.Drawing.Point(179, 42)
        Me.optDynamic.Name = "optDynamic"
        Me.HelpProvider1.SetShowHelp(Me.optDynamic, True)
        Me.optDynamic.Size = New System.Drawing.Size(194, 23)
        Me.optDynamic.TabIndex = 3
        Me.optDynamic.TabStop = True
        Me.optDynamic.Text = "Populate with data from database"
        Me.optDynamic.UseVisualStyleBackColor = False
        '
        'cmdView
        '
        Me.cmdView.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdView.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.HelpProvider1.SetHelpKeyword(Me.cmdView, "Snapshots.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdView, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdView.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdView.Location = New System.Drawing.Point(6, 467)
        Me.cmdView.Name = "cmdView"
        Me.HelpProvider1.SetShowHelp(Me.cmdView, True)
        Me.cmdView.Size = New System.Drawing.Size(75, 23)
        Me.cmdView.TabIndex = 4
        Me.cmdView.Text = "View"
        '
        'lsvSnapshots
        '
        Me.lsvSnapshots.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvSnapshots.Border.Class = "ListViewBorder"
        Me.lsvSnapshots.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvSnapshots.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3, Me.ColumnHeader4})
        Me.lsvSnapshots.DisabledBackColor = System.Drawing.Color.Empty
        Me.lsvSnapshots.ForeColor = System.Drawing.Color.Black
        Me.lsvSnapshots.FullRowSelect = True
        Me.lsvSnapshots.GridLines = True
        Me.lsvSnapshots.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.HelpProvider1.SetHelpKeyword(Me.lsvSnapshots, "Snapshots.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.lsvSnapshots, System.Windows.Forms.HelpNavigator.Topic)
        Me.lsvSnapshots.Location = New System.Drawing.Point(6, 34)
        Me.lsvSnapshots.Name = "lsvSnapshots"
        Me.HelpProvider1.SetShowHelp(Me.lsvSnapshots, True)
        Me.lsvSnapshots.Size = New System.Drawing.Size(672, 427)
        Me.lsvSnapshots.TabIndex = 3
        Me.lsvSnapshots.UseCompatibleStateImageBehavior = False
        Me.lsvSnapshots.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Entry Date"
        Me.ColumnHeader3.Width = 86
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Path"
        Me.ColumnHeader4.Width = 322
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label11, "Snapshots.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label11, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(318, 12)
        Me.Label11.Name = "Label11"
        Me.HelpProvider1.SetShowHelp(Me.Label11, True)
        Me.Label11.Size = New System.Drawing.Size(80, 16)
        Me.Label11.TabIndex = 5
        Me.Label11.Text = "Days"
        '
        'txtSnapshots
        '
        Me.txtSnapshots.Enabled = False
        Me.HelpProvider1.SetHelpKeyword(Me.txtSnapshots, "Snapshots.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtSnapshots, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtSnapshots.Location = New System.Drawing.Point(254, 10)
        Me.txtSnapshots.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.txtSnapshots.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtSnapshots.Name = "txtSnapshots"
        Me.HelpProvider1.SetShowHelp(Me.txtSnapshots, True)
        Me.txtSnapshots.Size = New System.Drawing.Size(56, 21)
        Me.txtSnapshots.TabIndex = 2
        Me.txtSnapshots.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSnapshots.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'chkSnapshots
        '
        Me.chkSnapshots.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkSnapshots.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.chkSnapshots, "Snapshots.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.chkSnapshots, System.Windows.Forms.HelpNavigator.Topic)
        Me.chkSnapshots.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkSnapshots.Location = New System.Drawing.Point(6, 8)
        Me.chkSnapshots.Name = "chkSnapshots"
        Me.HelpProvider1.SetShowHelp(Me.chkSnapshots, True)
        Me.chkSnapshots.Size = New System.Drawing.Size(240, 24)
        Me.chkSnapshots.TabIndex = 1
        Me.chkSnapshots.Text = "Enable snapshots and keep snapshots for"
        '
        'cmdDelete
        '
        Me.cmdDelete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdDelete.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.HelpProvider1.SetHelpKeyword(Me.cmdDelete, "Snapshots.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdDelete, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDelete.Location = New System.Drawing.Point(182, 467)
        Me.cmdDelete.Name = "cmdDelete"
        Me.HelpProvider1.SetShowHelp(Me.cmdDelete, True)
        Me.cmdDelete.Size = New System.Drawing.Size(75, 23)
        Me.cmdDelete.TabIndex = 6
        Me.cmdDelete.Text = "Delete"
        '
        'cmdExecute
        '
        Me.cmdExecute.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdExecute.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.HelpProvider1.SetHelpKeyword(Me.cmdExecute, "Snapshots.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdExecute, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdExecute.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdExecute.Location = New System.Drawing.Point(94, 467)
        Me.cmdExecute.Name = "cmdExecute"
        Me.HelpProvider1.SetShowHelp(Me.cmdExecute, True)
        Me.cmdExecute.Size = New System.Drawing.Size(75, 23)
        Me.cmdExecute.TabIndex = 5
        Me.cmdExecute.Text = "Execute"
        '
        'cmdTest
        '
        Me.cmdTest.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdTest.BackColor = System.Drawing.SystemColors.Control
        Me.cmdTest.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdTest.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.HelpProvider1.SetHelpKeyword(Me.cmdTest, "Report_Options.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdTest, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdTest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTest.Location = New System.Drawing.Point(611, 462)
        Me.cmdTest.Name = "cmdTest"
        Me.HelpProvider1.SetShowHelp(Me.cmdTest, True)
        Me.cmdTest.Size = New System.Drawing.Size(72, 23)
        Me.cmdTest.TabIndex = 2
        Me.cmdTest.Text = "Preview"
        '
        'btnRefreshDS
        '
        Me.btnRefreshDS.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnRefreshDS.BackColor = System.Drawing.SystemColors.Control
        Me.btnRefreshDS.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnRefreshDS.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.HelpProvider1.SetHelpKeyword(Me.btnRefreshDS, "Report_Options.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.btnRefreshDS, System.Windows.Forms.HelpNavigator.Topic)
        Me.btnRefreshDS.Image = CType(resources.GetObject("btnRefreshDS.Image"), System.Drawing.Image)
        Me.btnRefreshDS.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnRefreshDS.Location = New System.Drawing.Point(574, 462)
        Me.btnRefreshDS.Name = "btnRefreshDS"
        Me.HelpProvider1.SetShowHelp(Me.btnRefreshDS, True)
        Me.btnRefreshDS.Size = New System.Drawing.Size(32, 23)
        Me.btnRefreshDS.TabIndex = 3
        '
        'txtUrl
        '
        Me.txtUrl.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtUrl.Border.Class = "TextBoxBorder"
        Me.txtUrl.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUrl.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtUrl, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtUrl, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtUrl.Location = New System.Drawing.Point(7, 110)
        Me.txtUrl.Name = "txtUrl"
        Me.HelpProvider1.SetShowHelp(Me.txtUrl, True)
        Me.txtUrl.Size = New System.Drawing.Size(500, 21)
        Me.txtUrl.TabIndex = 3
        Me.txtUrl.Tag = "memo"
        Me.txtUrl.Text = "http://myReportServer/ReportServer/"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label6, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label6, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(7, 93)
        Me.Label6.Name = "Label6"
        Me.HelpProvider1.SetShowHelp(Me.Label6, True)
        Me.Label6.Size = New System.Drawing.Size(112, 16)
        Me.Label6.TabIndex = 32
        Me.Label6.Text = "Report  Server URL"
        '
        'chkGroupReports
        '
        Me.chkGroupReports.AutoSize = True
        Me.chkGroupReports.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkGroupReports.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkGroupReports.Location = New System.Drawing.Point(12, 385)
        Me.chkGroupReports.Name = "chkGroupReports"
        Me.chkGroupReports.Size = New System.Drawing.Size(337, 16)
        Me.chkGroupReports.TabIndex = 7
        Me.chkGroupReports.Text = "Group reports together by email address (email destination only)"
        '
        'grpQuery
        '
        Me.grpQuery.BackColor = System.Drawing.Color.Transparent
        Me.grpQuery.Controls.Add(Me.cmbDDKey)
        Me.grpQuery.Controls.Add(Me.Label12)
        Me.grpQuery.Controls.Add(Me.btnBuild)
        Me.grpQuery.Controls.Add(Me.txtQuery)
        Me.grpQuery.Enabled = False
        Me.grpQuery.Location = New System.Drawing.Point(12, 185)
        Me.grpQuery.Name = "grpQuery"
        Me.grpQuery.Size = New System.Drawing.Size(576, 196)
        Me.grpQuery.TabIndex = 6
        Me.grpQuery.TabStop = False
        Me.grpQuery.Text = "Data selection criteria"
        '
        'cmbDDKey
        '
        Me.cmbDDKey.FormattingEnabled = True
        Me.cmbDDKey.Location = New System.Drawing.Point(86, 164)
        Me.cmbDDKey.Name = "cmbDDKey"
        Me.cmbDDKey.Size = New System.Drawing.Size(240, 21)
        Me.cmbDDKey.TabIndex = 7
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        '
        '
        '
        Me.Label12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label12.Location = New System.Drawing.Point(14, 168)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(60, 16)
        Me.Label12.TabIndex = 6
        Me.Label12.Text = "Key Column"
        '
        'btnBuild
        '
        Me.btnBuild.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnBuild.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnBuild.Location = New System.Drawing.Point(251, 135)
        Me.btnBuild.Name = "btnBuild"
        Me.btnBuild.Size = New System.Drawing.Size(75, 23)
        Me.btnBuild.TabIndex = 1
        Me.btnBuild.Text = "&Build"
        '
        'txtQuery
        '
        Me.txtQuery.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtQuery.Border.Class = "TextBoxBorder"
        Me.txtQuery.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtQuery.ForeColor = System.Drawing.Color.Black
        Me.txtQuery.Location = New System.Drawing.Point(9, 19)
        Me.txtQuery.Multiline = True
        Me.txtQuery.Name = "txtQuery"
        Me.txtQuery.ReadOnly = True
        Me.txtQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtQuery.Size = New System.Drawing.Size(558, 110)
        Me.txtQuery.TabIndex = 0
        '
        'btnConnect
        '
        Me.btnConnect.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnConnect.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnConnect.Location = New System.Drawing.Point(263, 160)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(75, 23)
        Me.btnConnect.TabIndex = 4
        Me.btnConnect.Text = "&Connect"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.Location = New System.Drawing.Point(8, 7)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(136, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Database Connection Setup"
        '
        'grpAutoResume
        '
        Me.grpAutoResume.BackColor = System.Drawing.Color.Transparent
        Me.grpAutoResume.Controls.Add(Me.chkAutoResume)
        Me.grpAutoResume.Controls.Add(Me.txtCacheExpiry)
        Me.grpAutoResume.Controls.Add(Me.Label1)
        Me.grpAutoResume.Controls.Add(Me.Label10)
        Me.grpAutoResume.Location = New System.Drawing.Point(12, 8)
        Me.grpAutoResume.Name = "grpAutoResume"
        Me.grpAutoResume.Size = New System.Drawing.Size(513, 67)
        Me.grpAutoResume.TabIndex = 31
        Me.grpAutoResume.TabStop = False
        '
        'chkAutoResume
        '
        Me.chkAutoResume.AutoSize = True
        Me.chkAutoResume.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkAutoResume.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAutoResume.Location = New System.Drawing.Point(2, 13)
        Me.chkAutoResume.Name = "chkAutoResume"
        Me.chkAutoResume.Size = New System.Drawing.Size(267, 16)
        Me.chkAutoResume.TabIndex = 26
        Me.chkAutoResume.Text = "Resume failed/errored schedules with cached data"
        '
        'txtCacheExpiry
        '
        Me.txtCacheExpiry.Enabled = False
        Me.txtCacheExpiry.Location = New System.Drawing.Point(168, 37)
        Me.txtCacheExpiry.Maximum = New Decimal(New Integer() {10080, 0, 0, 0})
        Me.txtCacheExpiry.Name = "txtCacheExpiry"
        Me.txtCacheExpiry.Size = New System.Drawing.Size(38, 21)
        Me.txtCacheExpiry.TabIndex = 29
        Me.txtCacheExpiry.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.Location = New System.Drawing.Point(-1, 41)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(164, 16)
        Me.Label1.TabIndex = 27
        Me.Label1.Text = "Expire dynamic cached data after"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label10.Location = New System.Drawing.Point(208, 41)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(41, 16)
        Me.Label10.TabIndex = 28
        Me.Label10.Text = "minutes"
        '
        'cmdTest2
        '
        Me.cmdTest2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdTest2.BackColor = System.Drawing.SystemColors.Control
        Me.cmdTest2.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdTest2.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.HelpProvider1.SetHelpKeyword(Me.cmdTest2, "Report_Options.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdTest2, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdTest2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTest2.Location = New System.Drawing.Point(534, 14)
        Me.cmdTest2.Name = "cmdTest2"
        Me.HelpProvider1.SetShowHelp(Me.cmdTest2, True)
        Me.cmdTest2.Size = New System.Drawing.Size(72, 23)
        Me.cmdTest2.TabIndex = 26
        Me.cmdTest2.Text = "Preview"
        '
        'grpDynamicTasks
        '
        Me.grpDynamicTasks.BackColor = System.Drawing.Color.Transparent
        Me.grpDynamicTasks.Controls.Add(Me.optOnce)
        Me.grpDynamicTasks.Controls.Add(Me.optAll)
        Me.grpDynamicTasks.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.grpDynamicTasks.Enabled = False
        Me.grpDynamicTasks.Location = New System.Drawing.Point(0, 422)
        Me.grpDynamicTasks.Name = "grpDynamicTasks"
        Me.grpDynamicTasks.Size = New System.Drawing.Size(690, 74)
        Me.grpDynamicTasks.TabIndex = 17
        Me.grpDynamicTasks.TabStop = False
        Me.grpDynamicTasks.Text = "Run custom tasks..."
        '
        'optOnce
        '
        Me.optOnce.AutoSize = True
        Me.HelpProvider1.SetHelpKeyword(Me.optOnce, "Tasks.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.optOnce, System.Windows.Forms.HelpNavigator.Topic)
        Me.optOnce.Location = New System.Drawing.Point(6, 46)
        Me.optOnce.Name = "optOnce"
        Me.HelpProvider1.SetShowHelp(Me.optOnce, True)
        Me.optOnce.Size = New System.Drawing.Size(162, 17)
        Me.optOnce.TabIndex = 2
        Me.optOnce.TabStop = True
        Me.optOnce.Tag = "1"
        Me.optOnce.Text = "Once for the entire schedule"
        Me.optOnce.UseVisualStyleBackColor = True
        '
        'optAll
        '
        Me.optAll.AutoSize = True
        Me.optAll.Checked = True
        Me.HelpProvider1.SetHelpKeyword(Me.optAll, "Tasks.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.optAll, System.Windows.Forms.HelpNavigator.Topic)
        Me.optAll.Location = New System.Drawing.Point(6, 20)
        Me.optAll.Name = "optAll"
        Me.HelpProvider1.SetShowHelp(Me.optAll, True)
        Me.optAll.Size = New System.Drawing.Size(179, 17)
        Me.optAll.TabIndex = 1
        Me.optAll.TabStop = True
        Me.optAll.Tag = "0"
        Me.optAll.Text = "Once for each generated report"
        Me.optAll.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.lsvDatasources)
        Me.GroupBox1.Controls.Add(Me.btnRefreshDS)
        Me.GroupBox1.Controls.Add(Me.cmdTest)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(690, 496)
        Me.GroupBox1.TabIndex = 25
        Me.GroupBox1.TabStop = False
        '
        'lsvDatasources
        '
        Me.lsvDatasources.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvDatasources.Border.Class = "ListViewBorder"
        Me.lsvDatasources.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvDatasources.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader5, Me.ColumnHeader6})
        Me.lsvDatasources.DisabledBackColor = System.Drawing.Color.Empty
        Me.lsvDatasources.ForeColor = System.Drawing.Color.Black
        Me.HelpProvider1.SetHelpKeyword(Me.lsvDatasources, "Report_Options.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.lsvDatasources, System.Windows.Forms.HelpNavigator.Topic)
        Me.lsvDatasources.Location = New System.Drawing.Point(8, 18)
        Me.lsvDatasources.Name = "lsvDatasources"
        Me.HelpProvider1.SetShowHelp(Me.lsvDatasources, True)
        Me.lsvDatasources.Size = New System.Drawing.Size(675, 438)
        Me.lsvDatasources.TabIndex = 1
        Me.lsvDatasources.UseCompatibleStateImageBehavior = False
        Me.lsvDatasources.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Datasource Name"
        Me.ColumnHeader5.Width = 274
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Name"
        Me.ColumnHeader6.Width = 116
        '
        'ClearToolStripMenuItem
        '
        Me.ClearToolStripMenuItem.Name = "ClearToolStripMenuItem"
        Me.ClearToolStripMenuItem.Size = New System.Drawing.Size(99, 22)
        Me.ClearToolStripMenuItem.Text = "&Clear"
        '
        'UcDest
        '
        Me.UcDest.BackColor = System.Drawing.Color.Transparent
        Me.UcDest.destinationsPicker = False
        Me.UcDest.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcDest.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.HelpProvider1.SetHelpKeyword(Me.UcDest, "Output.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.UcDest, System.Windows.Forms.HelpNavigator.Topic)
        Me.UcDest.Location = New System.Drawing.Point(0, 0)
        Me.UcDest.m_CanDisable = True
        Me.UcDest.m_DelayDelete = True
        Me.UcDest.m_eventBased = False
        Me.UcDest.m_ExcelBurst = False
        Me.UcDest.m_IsDynamic = False
        Me.UcDest.m_isPackage = False
        Me.UcDest.m_IsQuery = False
        Me.UcDest.m_StaticDest = False
        Me.UcDest.Name = "UcDest"
        Me.HelpProvider1.SetShowHelp(Me.UcDest, True)
        Me.UcDest.Size = New System.Drawing.Size(690, 496)
        Me.UcDest.TabIndex = 0
        '
        'oTask
        '
        Me.oTask.BackColor = System.Drawing.Color.Transparent
        Me.oTask.Dock = System.Windows.Forms.DockStyle.Fill
        Me.oTask.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.oTask.ForeColor = System.Drawing.Color.Navy
        Me.HelpProvider1.SetHelpKeyword(Me.oTask, "Tasks.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.oTask, System.Windows.Forms.HelpNavigator.Topic)
        Me.oTask.Location = New System.Drawing.Point(0, 0)
        Me.oTask.m_defaultTaks = False
        Me.oTask.m_eventBased = False
        Me.oTask.m_eventID = 99999
        Me.oTask.m_forExceptionHandling = False
        Me.oTask.m_showAfterType = True
        Me.oTask.m_showExpanded = True
        Me.oTask.Name = "oTask"
        Me.HelpProvider1.SetShowHelp(Me.oTask, True)
        Me.oTask.Size = New System.Drawing.Size(690, 422)
        Me.oTask.TabIndex = 0
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.DefaultTooltipSettings = New DevComponents.DotNetBar.SuperTooltipInfo("", "", "", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray)
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'bgWorker
        '
        Me.bgWorker.WorkerSupportsCancellation = True
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'stabProperties
        '
        Me.stabProperties.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.stabProperties.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabProperties.ControlBox.MenuBox.Name = ""
        Me.stabProperties.ControlBox.Name = ""
        Me.stabProperties.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabProperties.ControlBox.MenuBox, Me.stabProperties.ControlBox.CloseBox})
        Me.stabProperties.Controls.Add(Me.SuperTabControlPanel5)
        Me.stabProperties.Controls.Add(Me.SuperTabControlPanel1)
        Me.stabProperties.Controls.Add(Me.SuperTabControlPanel8)
        Me.stabProperties.Controls.Add(Me.SuperTabControlPanel2)
        Me.stabProperties.Controls.Add(Me.SuperTabControlPanel4)
        Me.stabProperties.Controls.Add(Me.SuperTabControlPanel7)
        Me.stabProperties.Controls.Add(Me.SuperTabControlPanel10)
        Me.stabProperties.Controls.Add(Me.SuperTabControlPanel11)
        Me.stabProperties.Controls.Add(Me.SuperTabControlPanel9)
        Me.stabProperties.Controls.Add(Me.SuperTabControlPanel3)
        Me.stabProperties.Controls.Add(Me.SuperTabControlPanel6)
        Me.stabProperties.Dock = System.Windows.Forms.DockStyle.Fill
        Me.stabProperties.ForeColor = System.Drawing.Color.Black
        Me.stabProperties.Location = New System.Drawing.Point(0, 0)
        Me.stabProperties.Name = "stabProperties"
        Me.stabProperties.ReorderTabsEnabled = True
        Me.stabProperties.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabProperties.SelectedTabIndex = 0
        Me.stabProperties.Size = New System.Drawing.Size(826, 496)
        Me.stabProperties.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.stabProperties.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.stabProperties.TabIndex = 51
        Me.stabProperties.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabGeneral, Me.tabSchedule, Me.tabDatasources, Me.tabReport, Me.tabDestination, Me.tabException, Me.tabTasks, Me.tabSnapshots, Me.tabHistory, Me.tabDataDriver, Me.tabDynamic})
        Me.stabProperties.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        '
        'SuperTabControlPanel5
        '
        Me.SuperTabControlPanel5.Controls.Add(Me.parameterController)
        Me.SuperTabControlPanel5.Controls.Add(Me.Panel1)
        Me.SuperTabControlPanel5.Controls.Add(Me.txtFormula)
        Me.SuperTabControlPanel5.Controls.Add(Me.txtID)
        Me.SuperTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel5.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel5.Name = "SuperTabControlPanel5"
        Me.SuperTabControlPanel5.Size = New System.Drawing.Size(690, 496)
        Me.SuperTabControlPanel5.TabIndex = 0
        Me.SuperTabControlPanel5.TabItem = Me.tabReport
        '
        'parameterController
        '
        Me.parameterController.Dock = System.Windows.Forms.DockStyle.Fill
        Me.parameterController.formsAuth = False
        Me.parameterController.Location = New System.Drawing.Point(0, 0)
        Me.parameterController.Name = "parameterController"
        Me.parameterController.reportid = 0
        Me.parameterController.serverPassword = Nothing
        Me.parameterController.serverUser = Nothing
        Me.parameterController.Size = New System.Drawing.Size(690, 407)
        Me.parameterController.ssrsConnected = False
        Me.parameterController.TabIndex = 34
        Me.parameterController.url = Nothing
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.cmdTest2)
        Me.Panel1.Controls.Add(Me.grpAutoResume)
        Me.Panel1.Controls.Add(Me.cmdRequery)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 407)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(690, 89)
        Me.Panel1.TabIndex = 33
        '
        'tabReport
        '
        Me.tabReport.AttachedControl = Me.SuperTabControlPanel5
        Me.tabReport.GlobalItem = False
        Me.tabReport.Image = CType(resources.GetObject("tabReport.Image"), System.Drawing.Image)
        Me.tabReport.Name = "tabReport"
        Me.tabReport.Text = "Report"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.txtUrl)
        Me.SuperTabControlPanel1.Controls.Add(Me.Label2)
        Me.SuperTabControlPanel1.Controls.Add(Me.Label6)
        Me.SuperTabControlPanel1.Controls.Add(Me.cmdLoc)
        Me.SuperTabControlPanel1.Controls.Add(Me.txtDesc)
        Me.SuperTabControlPanel1.Controls.Add(Me.txtDBLoc)
        Me.SuperTabControlPanel1.Controls.Add(Me.txtFolder)
        Me.SuperTabControlPanel1.Controls.Add(Me.Label7)
        Me.SuperTabControlPanel1.Controls.Add(Me.cmdDbLoc)
        Me.SuperTabControlPanel1.Controls.Add(Me.txtName)
        Me.SuperTabControlPanel1.Controls.Add(Me.Label4)
        Me.SuperTabControlPanel1.Controls.Add(Me.Label9)
        Me.SuperTabControlPanel1.Controls.Add(Me.txtKeyWord)
        Me.SuperTabControlPanel1.Controls.Add(Me.Label3)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(690, 496)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabGeneral
        '
        'tabGeneral
        '
        Me.tabGeneral.AttachedControl = Me.SuperTabControlPanel1
        Me.tabGeneral.GlobalItem = False
        Me.tabGeneral.Image = CType(resources.GetObject("tabGeneral.Image"), System.Drawing.Image)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Text = "General"
        '
        'SuperTabControlPanel8
        '
        Me.SuperTabControlPanel8.Controls.Add(Me.UcDest)
        Me.SuperTabControlPanel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel8.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel8.Name = "SuperTabControlPanel8"
        Me.SuperTabControlPanel8.Size = New System.Drawing.Size(690, 496)
        Me.SuperTabControlPanel8.TabIndex = 0
        Me.SuperTabControlPanel8.TabItem = Me.tabDestination
        '
        'tabDestination
        '
        Me.tabDestination.AttachedControl = Me.SuperTabControlPanel8
        Me.tabDestination.GlobalItem = False
        Me.tabDestination.Image = CType(resources.GetObject("tabDestination.Image"), System.Drawing.Image)
        Me.tabDestination.Name = "tabDestination"
        Me.tabDestination.Text = "Destinations"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.UcBlankReportX1)
        Me.SuperTabControlPanel2.Controls.Add(Me.UcError)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(690, 496)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.tabException
        '
        'UcBlankReportX1
        '
        Me.UcBlankReportX1.BackColor = System.Drawing.Color.Transparent
        Me.UcBlankReportX1.blankID = 0
        Me.UcBlankReportX1.blankType = "AlertandReport"
        Me.UcBlankReportX1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcBlankReportX1.Location = New System.Drawing.Point(0, 121)
        Me.UcBlankReportX1.m_customDSN = ""
        Me.UcBlankReportX1.m_customPassword = ""
        Me.UcBlankReportX1.m_customUserID = ""
        Me.UcBlankReportX1.m_showAllReportsBlankForTasks = False
        Me.UcBlankReportX1.Name = "UcBlankReportX1"
        Me.UcBlankReportX1.Size = New System.Drawing.Size(690, 375)
        Me.UcBlankReportX1.TabIndex = 6
        '
        'UcError
        '
        Me.UcError.BackColor = System.Drawing.Color.Transparent
        Me.UcError.Dock = System.Windows.Forms.DockStyle.Top
        Me.UcError.Location = New System.Drawing.Point(0, 0)
        Me.UcError.m_showFailOnOne = False
        Me.UcError.Name = "UcError"
        Me.UcError.Size = New System.Drawing.Size(690, 121)
        Me.UcError.TabIndex = 5
        '
        'tabException
        '
        Me.tabException.AttachedControl = Me.SuperTabControlPanel2
        Me.tabException.GlobalItem = False
        Me.tabException.Image = CType(resources.GetObject("tabException.Image"), System.Drawing.Image)
        Me.tabException.Name = "tabException"
        Me.tabException.Text = "Exception Handling"
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.chkGroupReports)
        Me.SuperTabControlPanel4.Controls.Add(Me.Label5)
        Me.SuperTabControlPanel4.Controls.Add(Me.grpQuery)
        Me.SuperTabControlPanel4.Controls.Add(Me.UcDSNDD)
        Me.SuperTabControlPanel4.Controls.Add(Me.btnConnect)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(690, 496)
        Me.SuperTabControlPanel4.TabIndex = 0
        Me.SuperTabControlPanel4.TabItem = Me.tabDataDriver
        '
        'UcDSNDD
        '
        Me.UcDSNDD.BackColor = System.Drawing.Color.White
        Me.UcDSNDD.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSNDD.ForeColor = System.Drawing.Color.Navy
        Me.UcDSNDD.Location = New System.Drawing.Point(11, 26)
        Me.UcDSNDD.m_conString = "|||"
        Me.UcDSNDD.m_showConnectionType = False
        Me.UcDSNDD.Name = "UcDSNDD"
        Me.UcDSNDD.Size = New System.Drawing.Size(577, 128)
        Me.UcDSNDD.TabIndex = 3
        '
        'tabDataDriver
        '
        Me.tabDataDriver.AttachedControl = Me.SuperTabControlPanel4
        Me.tabDataDriver.GlobalItem = False
        Me.tabDataDriver.Image = CType(resources.GetObject("tabDataDriver.Image"), System.Drawing.Image)
        Me.tabDataDriver.Name = "tabDataDriver"
        Me.tabDataDriver.Text = "Data Driver "
        Me.tabDataDriver.Visible = False
        '
        'SuperTabControlPanel7
        '
        Me.SuperTabControlPanel7.Controls.Add(Me.LabelX1)
        Me.SuperTabControlPanel7.Controls.Add(Me.btnView)
        Me.SuperTabControlPanel7.Controls.Add(Me.GroupBox6)
        Me.SuperTabControlPanel7.Controls.Add(Me.chkStaticDest)
        Me.SuperTabControlPanel7.Controls.Add(Me.optDynamic)
        Me.SuperTabControlPanel7.Controls.Add(Me.cmbKey)
        Me.SuperTabControlPanel7.Controls.Add(Me.optStatic)
        Me.SuperTabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel7.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel7.Name = "SuperTabControlPanel7"
        Me.SuperTabControlPanel7.Size = New System.Drawing.Size(690, 496)
        Me.SuperTabControlPanel7.TabIndex = 0
        Me.SuperTabControlPanel7.TabItem = Me.tabDynamic
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(13, 5)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(75, 23)
        Me.LabelX1.TabIndex = 18
        Me.LabelX1.Text = "Key Parameter"
        '
        'tabDynamic
        '
        Me.tabDynamic.AttachedControl = Me.SuperTabControlPanel7
        Me.tabDynamic.GlobalItem = False
        Me.tabDynamic.Image = CType(resources.GetObject("tabDynamic.Image"), System.Drawing.Image)
        Me.tabDynamic.Name = "tabDynamic"
        Me.tabDynamic.Text = "Linking"
        Me.tabDynamic.Visible = False
        '
        'SuperTabControlPanel10
        '
        Me.SuperTabControlPanel10.Controls.Add(Me.UcSchedule1)
        Me.SuperTabControlPanel10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel10.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel10.Name = "SuperTabControlPanel10"
        Me.SuperTabControlPanel10.Size = New System.Drawing.Size(690, 496)
        Me.SuperTabControlPanel10.TabIndex = 0
        Me.SuperTabControlPanel10.TabItem = Me.tabSchedule
        '
        'UcSchedule1
        '
        Me.UcSchedule1.BackColor = System.Drawing.Color.Transparent
        Me.UcSchedule1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcSchedule1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UcSchedule1.Location = New System.Drawing.Point(0, 0)
        Me.UcSchedule1.m_collaborationServerID = 0
        Me.UcSchedule1.m_nextRun = "2012-02-28 10:37:49"
        Me.UcSchedule1.m_RepeatUnit = ""
        Me.UcSchedule1.mode = sqlrd.ucSchedule.modeEnum.EDIT
        Me.UcSchedule1.Name = "UcSchedule1"
        Me.UcSchedule1.scheduleID = 0
        Me.UcSchedule1.scheduleStatus = True
        Me.UcSchedule1.sFrequency = "Daily"
        Me.UcSchedule1.Size = New System.Drawing.Size(690, 496)
        Me.UcSchedule1.TabIndex = 0
        '
        'tabSchedule
        '
        Me.tabSchedule.AttachedControl = Me.SuperTabControlPanel10
        Me.tabSchedule.GlobalItem = False
        Me.tabSchedule.Image = CType(resources.GetObject("tabSchedule.Image"), System.Drawing.Image)
        Me.tabSchedule.Name = "tabSchedule"
        Me.tabSchedule.Text = "Schedule"
        '
        'SuperTabControlPanel11
        '
        Me.SuperTabControlPanel11.Controls.Add(Me.UcHistory)
        Me.SuperTabControlPanel11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel11.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel11.Name = "SuperTabControlPanel11"
        Me.SuperTabControlPanel11.Size = New System.Drawing.Size(690, 496)
        Me.SuperTabControlPanel11.TabIndex = 0
        Me.SuperTabControlPanel11.TabItem = Me.tabHistory
        '
        'UcHistory
        '
        Me.UcHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcHistory.Location = New System.Drawing.Point(0, 0)
        Me.UcHistory.m_filter = False
        Me.UcHistory.m_historyLimit = 0
        Me.UcHistory.m_objectID = 0
        Me.UcHistory.m_scheduleType = sqlrd.clsMarsScheduler.enScheduleType.REPORT
        Me.UcHistory.m_showControls = True
        Me.UcHistory.m_showSchedulesList = False
        Me.UcHistory.m_sortOrder = " ASC "
        Me.UcHistory.Name = "UcHistory"
        Me.UcHistory.Size = New System.Drawing.Size(690, 496)
        Me.UcHistory.TabIndex = 0
        '
        'tabHistory
        '
        Me.tabHistory.AttachedControl = Me.SuperTabControlPanel11
        Me.tabHistory.GlobalItem = False
        Me.tabHistory.Image = CType(resources.GetObject("tabHistory.Image"), System.Drawing.Image)
        Me.tabHistory.Name = "tabHistory"
        Me.tabHistory.Text = "History"
        '
        'SuperTabControlPanel9
        '
        Me.SuperTabControlPanel9.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel9.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel9.Name = "SuperTabControlPanel9"
        Me.SuperTabControlPanel9.Size = New System.Drawing.Size(690, 496)
        Me.SuperTabControlPanel9.TabIndex = 0
        Me.SuperTabControlPanel9.TabItem = Me.tabDatasources
        '
        'tabDatasources
        '
        Me.tabDatasources.AttachedControl = Me.SuperTabControlPanel9
        Me.tabDatasources.GlobalItem = False
        Me.tabDatasources.Image = CType(resources.GetObject("tabDatasources.Image"), System.Drawing.Image)
        Me.tabDatasources.Name = "tabDatasources"
        Me.tabDatasources.Text = "Report Options"
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.cmdView)
        Me.SuperTabControlPanel3.Controls.Add(Me.chkSnapshots)
        Me.SuperTabControlPanel3.Controls.Add(Me.txtSnapshots)
        Me.SuperTabControlPanel3.Controls.Add(Me.lsvSnapshots)
        Me.SuperTabControlPanel3.Controls.Add(Me.cmdDelete)
        Me.SuperTabControlPanel3.Controls.Add(Me.cmdExecute)
        Me.SuperTabControlPanel3.Controls.Add(Me.Label11)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(690, 496)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.tabSnapshots
        '
        'tabSnapshots
        '
        Me.tabSnapshots.AttachedControl = Me.SuperTabControlPanel3
        Me.tabSnapshots.GlobalItem = False
        Me.tabSnapshots.Image = CType(resources.GetObject("tabSnapshots.Image"), System.Drawing.Image)
        Me.tabSnapshots.Name = "tabSnapshots"
        Me.tabSnapshots.Text = "Snapshots"
        '
        'SuperTabControlPanel6
        '
        Me.SuperTabControlPanel6.Controls.Add(Me.oTask)
        Me.SuperTabControlPanel6.Controls.Add(Me.grpDynamicTasks)
        Me.SuperTabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel6.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel6.Name = "SuperTabControlPanel6"
        Me.SuperTabControlPanel6.Size = New System.Drawing.Size(690, 496)
        Me.SuperTabControlPanel6.TabIndex = 0
        Me.SuperTabControlPanel6.TabItem = Me.tabTasks
        '
        'tabTasks
        '
        Me.tabTasks.AttachedControl = Me.SuperTabControlPanel6
        Me.tabTasks.GlobalItem = False
        Me.tabTasks.Image = CType(resources.GetObject("tabTasks.Image"), System.Drawing.Image)
        Me.tabTasks.Name = "tabTasks"
        Me.tabTasks.Text = "Tasks"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdApply)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 496)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(826, 31)
        Me.FlowLayoutPanel1.TabIndex = 52
        '
        'frmSingleProp
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(826, 527)
        Me.Controls.Add(Me.stabProperties)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.HelpButton = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSingleProp"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Single Schedule Properties"
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.txtSnapshots, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpQuery.ResumeLayout(False)
        Me.grpQuery.PerformLayout()
        Me.grpAutoResume.ResumeLayout(False)
        Me.grpAutoResume.PerformLayout()
        CType(Me.txtCacheExpiry, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpDynamicTasks.ResumeLayout(False)
        Me.grpDynamicTasks.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.stabProperties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabProperties.ResumeLayout(False)
        Me.SuperTabControlPanel5.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel8.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel4.ResumeLayout(False)
        Me.SuperTabControlPanel4.PerformLayout()
        Me.SuperTabControlPanel7.ResumeLayout(False)
        Me.SuperTabControlPanel10.ResumeLayout(False)
        Me.SuperTabControlPanel11.ResumeLayout(False)
        Me.SuperTabControlPanel9.ResumeLayout(False)
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.SuperTabControlPanel6.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim oMsg As clsMarsMessaging = New clsMarsMessaging
    Dim oData As clsMarsData = New clsMarsData
    Dim oUI As clsMarsUI = New clsMarsUI
    Dim OkayToClose As Boolean
    Dim IsDynamic As Boolean = False
    Dim IsBursting As Boolean = False
    Dim sLink As String = String.Empty
    Public oRpt As Object 'ReportServer.ReportingService
    Dim sCache As String
    Dim ScheduleID As Integer
    Dim parDefaults As Hashtable
    Dim serverUser As String
    Dim serverPassword As String
    Dim dtParameters As DataTable
    Dim showTooltip As Boolean = True
    Dim owner As String = ""
    Dim IsDataDriven As Boolean = False
    Dim m_serverParametersTable As DataTable
    Dim formsAuth As Boolean


    Private ReadOnly Property m_autoCalcValue() As Decimal
        Get
            Dim val As Decimal = clsMarsScheduler.globalItem.getScheduleDuration(Me.txtID.Text, 0, 0, 0, 0, clsMarsScheduler.enCalcType.MEDIAN, 3)

            Return val

        End Get
    End Property

    Private Sub frmSingleProp_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        g_parameterList = Nothing
    End Sub

    Private Sub frmSingleProp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        HelpProvider1.HelpNamespace = gHelpPath
        FormatForWinXP(Me)

        bgWorker.RunWorkerAsync()

    End Sub

    Private ReadOnly Property m_ParametersList() As ArrayList
        Get
            Dim arr As ArrayList = New ArrayList

            For Each it As ListViewItem In parameterController.lsvParameters.Items
                arr.Add(it.Text)
            Next

            Return arr
        End Get
    End Property
    Public Sub EditSchedule(ByVal nReportID As Int32)

        Me.showTooltip = False

        Dim SQL As String
        Dim Rs As ADODB.Recordset
        Dim rsP As ADODB.Recordset
        Dim sWhere As String
        Dim I As Int32
        Dim sDestination As String = ""
        Dim autoCalc As Boolean

        Try
            sWhere = " WHERE  ReportID = " & nReportID

            SQL = "SELECT * FROM ReportAttr " & sWhere

            Rs = clsMarsData.GetData(SQL)

            If Rs Is Nothing Then Return

            If Rs.EOF = False Then
                txtID.Text = Rs("reportid").Value
                txtDBLoc.Text = Rs("cachepath").Value
                txtUrl.Text = Rs("databasepath").Value
                owner = Rs("owner").Value
                Me.serverUser = IsNull(Rs("rptuserid").Value)
                Me.serverPassword = _DecryptDBValue(IsNull(Rs("rptpassword").Value))
                UcDest.nPackID = 0
                UcDest.nSmartID = 0
                UcDest.nReportID = txtID.Text
                UcDest.LoadAll()
                autoCalc = IsNull(Rs("autocalc").Value, 0)

                Try
                    formsAuth = Convert.ToInt32(IsNull(Rs("rptFormsAuth").Value, 0))
                Catch ex As Exception
                    formsAuth = False
                End Try

                txtName.Text = Rs("reporttitle").Value

                UcDest.sReportTitle = txtName.Text

                rsP = clsMarsData.GetData("SELECT FolderName FROM Folders WHERE FolderID = " & Rs("Parent").Value)

                txtFolder.Text = rsP.Fields(0).Value

                txtFolder.Tag = Rs("Parent").Value

                rsP.Close()

                rsP = clsMarsData.GetData("SELECT DestinationType FROM DestinationAttr WHERE " & _
                "ReportID = " & nReportID)

                Try
                    If rsP.EOF = False Then
                        sDestination = rsP.Fields(0).Value
                    End If
                    rsP.Close()

                    Select Case sDestination.ToLower
                        Case "email"
                            Label21.Text = Label21.Text.Replace("xxx", "email address")
                        Case "disk"
                            Label21.Text = Label21.Text.Replace("xxx", "directory")
                        Case "printer"
                            Label21.Text = Label21.Text.Replace("xxx", "printer name")
                        Case "ftp"
                            Label21.Text = Label21.Text.Replace("xxx", "FTP Server")
                    End Select
                Catch : End Try

                Try
                    UcError.cmbRetry.Text = Rs("retry").Value
                    UcError.m_autoFailAfter = IIf(Rs("assumefail").Value = 0, 30, Rs("assumefail").Value)
                    txtFormula.Text = IsNull(Rs("selectionformula").Value)
                    serverUser = Rs("RptUserID").Value
                    serverPassword = _DecryptDBValue(Rs("RptPassword").Value)
                Catch : End Try

                Try
                    chkStaticDest.Checked = Convert.ToBoolean(Rs("staticdest").Value)
                    UcDest.StaticDest = chkStaticDest.Checked
                Catch ex As Exception
                    chkStaticDest.Checked = False
                End Try

                If IsNull(Rs("dynamic").Value, "0") = "-1" Or IsNull(Rs("dynamic").Value, "0") = "1" Then
                    IsDynamic = True
                    Me.Text = "Dynamic Schedule Properties - " & txtName.Text
                    Me.grpAutoResume.Enabled = True
                Else
                    IsDynamic = False
                    Me.Text = "Single Report Schedule Properties - " & txtName.Text
                    Me.grpAutoResume.Enabled = False
                End If

                If IsNull(Rs("dynamictasks").Value, 0) = 0 Then
                    Me.optAll.Checked = True
                Else
                    Me.optOnce.Checked = True
                End If

                If IsNull(Rs("isdatadriven").Value, "0") = "1" Then
                    IsDataDriven = True
                Else
                    IsDataDriven = False
                End If

                UcDest.isDynamic = IsDynamic

                If UcError.cmbAssumeFail.Text > 0 Then
                    UcError.chkAssumeFail.Checked = True
                Else
                    UcError.chkAssumeFail.Checked = True
                    UcError.m_autoFailAfter = 30
                End If

                UcError.chkAutoCalc.Checked = autoCalc

                If autoCalc = True Then
                    UcError.chkAutoCalc.Checked = True
                    UcError.chkAssumeFail.Checked = True
                    UcError.cmbAssumeFail.Enabled = False

                    Dim temp As Decimal = Me.m_autoCalcValue

                    If temp < 1 Then temp = 1

                    Try
                        UcError.m_autoFailAfter = temp
                    Catch : End Try
                End If

                Try
                    UcBlankReportX1.chkCheckBlankReport.Checked = Convert.ToBoolean(Rs("checkblank").Value)
                    UcBlankReportX1.getInfo(txtID.Text, clsMarsScheduler.enScheduleType.REPORT)
                Catch : End Try

                UcError.txtRetryInterval.Value = IsNull(Rs("retryinterval").Value, 0)

            End If

            Rs.Close()

            UcSchedule1.mode = ucSchedule.modeEnum.EDIT
            UcSchedule1.loadScheduleInfo(nReportID, clsMarsScheduler.enScheduleType.REPORT, txtDesc, txtKeyWord, oTask)

            parameterController.loadParameters(txtID.Text, txtUrl.Text, txtDBLoc.Text, Nothing, serverUser, serverPassword, formsAuth)

            Rs = clsMarsData.GetData("SELECT * FROM ReportDatasource WHERE ReportID =" & nReportID)

            If Rs IsNot Nothing Then
                Do While Rs.EOF = False
                    Dim item As ListViewItem = lsvDatasources.Items.Add(Rs("datasourcename").Value)

                    item.SubItems.Add(Rs("rptuserid").Value)

                    item.ImageIndex = 0
                    item.Tag = Rs("datasourceid").Value

                    Rs.MoveNext()
                Loop

                Rs.Close()
            End If

            'add dynamic linking is necessary

            If IsDynamic = True Then

                grpDynamicTasks.Enabled = True

                tabDynamic.Visible = True

                'cmdParameters.Enabled = True 
                'cmdRequery.Enabled = False

                SQL = "SELECT * FROM DynamicLink WHERE ReportID =" & nReportID

                Rs = clsMarsData.GetData(SQL)

                If Not Rs Is Nothing Then
                    If Rs.EOF = False Then
                        With UcDSN
                            .cmbDSN.Text = Convert.ToString(Rs("constring").Value).Split("|")(0)
                            .txtUserID.Text = Convert.ToString(Rs("constring").Value).Split("|")(1)
                            .txtPassword.Text = Convert.ToString(Rs("constring").Value).Split("|")(2)
                            gsCon = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & .txtPassword.Text
                        End With


                        cmbTable.Text = Convert.ToString(Rs("keycolumn").Value).Split(".")(0)
                        cmbColumn.Text = Convert.ToString(Rs("keycolumn").Value).Split(".")(1)

                        Dim valueColumn As String = IsNull(Rs("valuecolumn").Value)
                        Dim valueTable As String = ""

                        Try
                            valueTable = valueColumn.Split(".")(0)
                        Catch ex As Exception
                            valueTable = cmbTable.Text
                        End Try

                        If valueTable = cmbTable.Text Then
                            Try
                                cmbValue.Text = Convert.ToString(Rs("valuecolumn").Value).Split(".")(1)
                            Catch
                                cmbValue.Text = Convert.ToString(Rs("valuecolumn").Value)
                            End Try
                        Else
                            cmbValue.Text = "<Advanced>"
                        End If

                        gTables(0) = cmbTable.Text

                        If IsNull(Rs("table2").Value).Length > 0 Then _
                            gTables(1) = Rs("table2").Value

                    End If
                    Rs.Close()

                    Rs = clsMarsData.GetData("SELECT KeyParameter,AutoResume,ExpireCacheAfter FROM DynamicAttr WHERE " & _
                    "ReportID = " & nReportID)


                    For Each oItem As ListViewItem In parameterController.lsvParameters.Items
                        cmbKey.Items.Add(oItem.Text)
                    Next

                    If Not Rs Is Nothing Then
                        If Rs.EOF = False Then
                            cmbKey.Text = Rs("keyparameter").Value
                            Me.chkAutoResume.Checked = Convert.ToBoolean(Convert.ToInt32(IsNull(Rs("autoresume").Value, 0)))
                            Me.txtCacheExpiry.Value = IsNull(Rs("expirecacheafter").Value, 0)
                        End If
                    End If

                    '   g_parameterList = Nothing
                    '   g_parameterList = New ArrayList


                    'lets not remove the key parameter from the parameter list and also set its type
                    For Each oitem As ListViewItem In parameterController.lsvParameters.Items
                        If oitem.Text = cmbKey.Text Then
                            Dim kvp As KeyValuePair(Of String, String) = oitem.Tag


                            Try
                                If kvp.Value.StartsWith("[") And kvp.Value.EndsWith("]") Then
                                    optDynamic.Checked = True
                                Else
                                    optStatic.Checked = True
                                End If
                            Catch
                                optStatic.Checked = True
                            End Try
                        
                        End If
                    Next
                End If

            End If

            'add data-driven schedule data
            Try
                If IsDataDriven = True Then
                    UcError.m_showFailOnOne = True
                    grpDynamicTasks.Enabled = True
                    Me.UcDest.isDataDriven = True
                    tabDataDriver.Visible = True
                    '    Me.txtUrl.ContextMenu = Me.mnuInserter

                    Text = "Data-Driven Schedule Properties"

                    Rs = clsMarsData.GetData("SELECT * FROM DataDrivenAttr WHERE ReportID =" & nReportID)

                    If Rs IsNot Nothing Then
                        If Rs.EOF = False Then

                            tabDataDriver.Visible = True

                            Dim sQuery As String
                            Dim conString As String

                            sQuery = Rs("sqlquery").Value
                            conString = Rs("constring").Value

                            txtQuery.Text = sQuery

                            Try
                                UcError.chkFailonOne.Checked = IsNull(Rs("failonone").Value, 0)
                            Catch ex As Exception
                                UcError.chkFailonOne.Checked = False
                            End Try

                            clsMarsReport.populateDataDrivenCache(sQuery, conString, True)

                            Dim DSN, sUser, sPassword As String

                            DSN = conString.Split("|")(0)
                            sUser = conString.Split("|")(1)
                            sPassword = _DecryptDBValue(conString.Split("|")(2))

                            With UcDSNDD
                                .cmbDSN.Text = DSN
                                .txtUserID.Text = sUser
                                .txtPassword.Text = sPassword
                            End With

                            Try
                                Me.chkGroupReports.Checked = IsNull(Rs("groupreports").Value, 1)
                            Catch
                                Me.chkGroupReports.Checked = True
                            End Try

                            Dim rsDD As ADODB.Recordset = New ADODB.Recordset
                            Dim ddCon As ADODB.Connection = New ADODB.Connection

                            ddCon.Open(DSN, sUser, sPassword)

                            rsDD.Open(sQuery, ddCon)

                            Me.cmbDDKey.Items.Clear()

                            Dim z As Integer = 0

                            frmRDScheduleWizard.m_DataFields = New Hashtable

                            For Each fld As ADODB.Field In rsDD.Fields
                                frmRDScheduleWizard.m_DataFields.Add(z, fld.Name)
                                Me.cmbDDKey.Items.Add(fld.Name)
                                z += 1
                            Next

                            Try
                                Me.cmbDDKey.Text = IsNull(Rs("keycolumn").Value)
                            Catch : End Try

                            rsDD.Close()

                            ddCon.Close()
                            ddCon = Nothing
                        End If

                        Rs.Close()
                    End If
                End If
            Catch : End Try

            Rs = clsMarsData.GetData("SELECT * FROM ReportSnapshots WHERE ReportID =" & nReportID)

            Try
                If Rs.EOF = False Then
                    chkSnapshots.Checked = True
                    txtSnapshots.Value = Rs("keepsnap").Value
                End If

                Rs.Close()
            Catch : End Try

            UcDest.m_parameterList = Me.m_ParametersList

            Me.showTooltip = True
            cmdOK.Enabled = True


            Try
                'set up auto-complete data
                With Me.txtUrl
                    .AutoCompleteMode = AutoCompleteMode.SuggestAppend
                    .AutoCompleteSource = AutoCompleteSource.CustomSource
                    .AutoCompleteCustomSource = getUrlHistory()
                End With

                Me.ShowDialog()
            Catch ex As Exception
                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            End Try
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub GetParameterAvailableValues()
        Try
            oRpt = New rsClients.rsClient(clsMarsParser.Parser.ParseString(txtUrl.Text))

            oUI.BusyProgress(10, "Connecting to server...")

            oRpt.Url = clsMarsParser.Parser.ParseString(txtUrl.Text)

            If serverUser = "" Then
                oRpt.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials
            Else
                Dim userDomain As String = ""
                Dim tmpUser As String = serverUser

                getDomainAndUserFromString(tmpUser, userDomain)

                Dim oCred As System.Net.NetworkCredential = New System.Net.NetworkCredential(tmpUser, serverPassword, userDomain)

                oRpt.Credentials = oCred
            End If

            Dim sRDLPath As String = clsMarsReport.m_rdltempPath & txtName.Text & ".rdl"
            Dim repDef() As Byte

            oUI.BusyProgress(40, "Getting server definition...")

            If IO.File.Exists(sRDLPath) = True Then
                IO.File.Delete(sRDLPath)
            End If

            Dim fsReport As New System.IO.FileStream(sRDLPath, IO.FileMode.Create)

            repDef = oRpt.GetReportDefinition(txtDBLoc.Text)

            fsReport.Write(repDef, 0, repDef.Length)

            fsReport.Close()

            oUI.BusyProgress(70, "Loading values...")

            clsMarsReport._GetParameterValues(sRDLPath)

            Dim sPars() As String
            Dim sParDefaults As String()
            Dim I As Integer = 0

            sPars = clsMarsReport._GetParameterFields(sRDLPath)
            sParDefaults = clsMarsReport._GetParameterValues(sRDLPath)

            parDefaults = New Hashtable

            oUI.BusyProgress(90, "Populating parameters...")

            If sPars IsNot Nothing Then
                For Each s As String In sPars

                    parDefaults.Add(s.Split("|")(0), sParDefaults(I))

                    I += 1
                Next
            End If

            UcDest.m_parameterList = Me.m_ParametersList

            oUI.BusyProgress(, , True)
        Catch
            oUI.BusyProgress(, , True)
        End Try

    End Sub
    Private Sub cmdLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoc.Click
        Dim oFolders As frmFolders = New frmFolders
        Dim sFolder(1) As String

        sFolder = oFolders.GetFolder

        If sFolder(0) <> "" And sFolder(0) <> "Desktop" Then
            txtFolder.Text = sFolder(0)
            txtFolder.Tag = sFolder(1)
        End If
        oUI.ResetError(sender, ep)
    End Sub

    Private Sub cmdDbLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDbLoc.Click
        Try
            Dim srsVersion As String = clsMarsReport.m_serverVersion(txtUrl.Text, serverUser, serverPassword, formsAuth)

            'If srsVersion >= "2009" Then
            '    oRpt = New rsClients.rsClient2010(txtUrl.Text)
            If srsVersion >= "2007" Then
                oRpt = New rsClients.rsClient2008(txtUrl.Text)
            Else
                oRpt = New rsClients.rsClient(txtUrl.Text)
            End If

            If srsVersion >= "2007" Then txtUrl.Text = clsMarsParser.Parser.fixASMXfor2008(txtUrl.Text)

            'we check to see if we should put reportserver2005.asmx in the URL
            If txtUrl.Text.Contains("2006.asmx") Then '//its sharepoint mode
                oRpt = New ReportServer_2006.ReportingService2006
            ElseIf txtUrl.Text.Contains("2010.asmx") Then
                oRpt = New rsClients.rsClient2010(txtUrl.Text)
            End If

            oRpt.Url = clsMarsParser.Parser.ParseString(txtUrl.Text)

            Dim sPath As String

            Dim oServer As frmReportServer = New frmReportServer

            oServer.m_ReadOnly = False

            sPath = oServer.newGetReports(oRpt, clsMarsParser.Parser.ParseString(txtUrl.Text), serverUser, serverPassword, txtDBLoc.Text, formsAuth)

            If sPath IsNot Nothing Then
                txtDBLoc.Text = sPath
                txtName.Text = sPath.Split("/")(sPath.Split("/").GetUpperBound(0))
                cmdOK.Enabled = True

                If txtDBLoc.Text.StartsWith("/") = False And Not (txtDBLoc.Text.ToLower.StartsWith("http:")) Then
                    txtDBLoc.Text = "/" & txtDBLoc.Text
                End If
            Else
                Return
            End If

            cmdRequery_Click(sender, e) '//requery parameters
            btnRefreshDS_Click(sender, e) '//requery datasources

            txtName.Focus()
            txtName.SelectAll()

            cmdOK.Enabled = True

            'save the Url as it has been validated
            logUrlHistory(Me.txtUrl.Text)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), _
            "Please check your report server's web service URL and try again")

            If ex.Message.ToLower.Contains("cannot be found") Then
                txtName.Text = ""
                txtName.Text = ""
                'txtDesc.Text = ""
            End If
        End Try
    End Sub


    Private Sub cmdApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdApply.Click
        Try
            Dim SQL As String
            Dim sWhere As String
            Dim sPrinters As String = ""
            Dim nRepeat As Double

            'validate user entries
            If txtName.Text = "" Then
                stabProperties.SelectedTab = tabGeneral
                ep.SetError(txtName, "Please enter the name of the schedule")
                OkayToClose = False
                txtName.Focus()
                Exit Sub
            ElseIf clsMarsUI.candoRename(txtName.Text, Me.txtFolder.Tag, clsMarsScheduler.enScheduleType.REPORT, txtID.Text, IsDynamic, IsDataDriven) = False Then
                stabProperties.SelectedTab = tabGeneral
                ep.SetError(txtName, "A single schedule with this name already exists in this folder. Please use a different name.")
                OkayToClose = False
                txtName.Focus()
                Return
            ElseIf txtFolder.Text = "" Then
                stabProperties.SelectedTab = tabGeneral
                ep.SetError(txtFolder, "Please select the containing folder")
                txtFolder.Focus()
                OkayToClose = False
                Exit Sub
            ElseIf txtDBLoc.Text = "" Then
                stabProperties.SelectedTab = tabGeneral
                ep.SetError(txtDBLoc, "Please select the MS Access database")
                txtDBLoc.Focus()
                OkayToClose = False
                Exit Sub

            ElseIf UcError.chkAssumeFail.Checked = True And UcError.cmbAssumeFail.Text = "" Then
                stabProperties.SelectedTab = tabException
                ep.SetError(UcError.cmbAssumeFail, "Please select the amount before a schedule is assumed as failed")
                UcError.cmbAssumeFail.Focus()
                OkayToClose = False
                Exit Sub
            ElseIf UcDest.lsvDestination.Nodes.Count = 0 Then
                setError(UcDest.lsvDestination, "Please add at least one destination")
                stabProperties.SelectedTab = tabDestination
                OkayToClose = False
                Return
            ElseIf UcError.chkAssumeFail.Checked = True And UcError.cmbAssumeFail.Value = 0 Then
                ep.SetError(UcError.cmbAssumeFail, "Please provide a value greater than zero")
                stabProperties.SelectedTab = tabException
                UcError.cmbAssumeFail.Focus()
                OkayToClose = False
                Return
            ElseIf UcSchedule1.isAllDataValid = False Then
                stabProperties.SelectedTab = tabSchedule
                OkayToClose = False
                Return
            ElseIf IsDynamic = True Then
                If Me.chkStaticDest.Checked = False Then
                    If cmbTable.Text.Length = 0 Then
                        ep.SetError(cmbTable, "Please select the table that holds the value that must match the key parameter")
                        stabProperties.SelectedTab = tabDynamic
                        cmbTable.Focus()
                        Return
                    ElseIf cmbColumn.Text.Length = 0 Then
                        ep.SetError(cmbColumn, "Please select the column that holds the value that must match the key parameter")
                        stabProperties.SelectedTab = Me.tabDynamic
                        cmbColumn.Focus()
                        Return
                    ElseIf cmbValue.Text.Length = 0 Then
                        ep.SetError(cmbValue, "Please select the column that will provide the report destination")
                        stabProperties.SelectedTab = Me.tabDynamic
                        cmbValue.Focus()
                        Return
                    ElseIf cmbValue.Text = "<Advanced>" And sLink = String.Empty Then
                        ep.SetError(cmbValue, "Please set up the query for the value")
                        stabProperties.SelectedTab = Me.tabDynamic
                        cmbValue.Focus()
                        Return
                    ElseIf UcDSN._Validate = False Then
                        stabProperties.SelectedTab = Me.tabDynamic
                        UcDSN.cmbDSN.Focus()
                    End If
                End If
            End If

            Dim enabledCount As Integer = 0

            For Each nn As DevComponents.AdvTree.Node In UcDest.lsvDestination.Nodes
                If nn.Checked = True Then
                    enabledCount += 1
                End If
            Next

            If enabledCount = 0 Then
                ep.SetError(UcDest.lsvDestination, "Please enable at least one destination before saving")
                stabProperties.SelectedTab = tabDestination
                OkayToClose = False
                Return
            End If

            sWhere = " WHERE (PackID = 0 OR PackID IS NULL) and ReportID = " & txtID.Text

            Dim dynamicTasks As Integer

            If optAll.Checked Then '//run once for each
                dynamicTasks = 0
            Else
                dynamicTasks = 1 '//run once for all
            End If


            SQL = "UPDATE ReportAttr SET " & _
                "DatabasePath = '" & clsMarsParser.Parser.ParseString(txtUrl.Text).Replace("'", "''") & "', " & _
                "ReportName = '" & txtName.Text.Replace("'", "''") & "', " & _
                "CachePath = '" & SQLPrepare(txtDBLoc.Text) & "'," & _
                "Parent = " & txtFolder.Tag & ", " & _
                "ReportTitle = '" & txtName.Text.Replace("'", "''") & "', " & _
                "retry = " & UcError.cmbRetry.Value & "," & _
                "AssumeFail = " & UcError.m_autoFailAfter & "," & _
                "CheckBlank = " & Convert.ToInt32(UcBlankReportX1.chkCheckBlankReport.Checked) & "," & _
                "SelectionFormula = '" & txtFormula.Text.Replace("'", "''") & "'," & _
                "RptUserID = '" & SQLPrepare(Me.serverUser) & "'," & _
                "RptPassword = '" & SQLPrepare(_EncryptDBValue(Me.serverPassword)) & "'," & _
                "RptServer = ''," & _
                "RptDatabase = ''," & _
                "UseLogin = " & 0 & "," & _
                "UseSavedData = " & 0 & ", " & _
                "AutoRefresh = " & 0 & "," & _
                "StaticDest = " & Convert.ToInt32(Me.chkStaticDest.Checked) & "," & _
                "DynamicTasks = " & dynamicTasks & "," & _
                "RptDatabaseType ='', " & _
                "AutoCalc =" & Convert.ToInt32(UcError.chkAutoCalc.Checked) & "," & _
                "RetryInterval =" & UcError.txtRetryInterval.Value & ", " & _
                "rptFormsAuth =" & Convert.ToInt32(formsAuth) & " "

            SQL &= sWhere

            clsMarsData.WriteData(SQL)

            UcSchedule1.updateSchedule(txtID.Text, clsMarsScheduler.enScheduleType.REPORT, txtDesc.Text, txtKeyWord.Text)

            UcBlankReportX1.saveInfo(txtID.Text, clsMarsScheduler.enScheduleType.REPORT)

            If parameterController.lsvParameters.Items.Count > 0 Then parameterController.saveParameters(txtID.Text)

            If chkSnapshots.Checked = True Then
                clsMarsData.WriteData("DELETE FROM ReportSnapshots WHERE ReportID =" & txtID.Text)

                SQL = "INSERT INTO ReportSnapshots (SnapID,ReportID,KeepSnap) "

                SQL &= "VALUES (" & clsMarsData.CreateDataID("reportsnapshots", "snapid") & "," & _
                txtID.Text & "," & _
                txtSnapshots.Value & ")"

                clsMarsData.WriteData(SQL)
            Else
                clsMarsData.WriteData("DELETE FROM ReportSnapshots WHERE ReportID =" & txtID.Text)

                Dim rsd As ADODB.Recordset

                rsd = clsMarsData.GetData("SELECT SnapPath FROM SnapshotsAttr WHERE ReportID =" & txtID.Text)

                Try
                    Do While rsd.EOF = False
                        Try
                            System.IO.File.Delete(rsd("snappath").Value)
                        Catch : End Try

                        rsd.MoveNext()
                    Loop

                    rsd.Close()
                Catch : End Try

                clsMarsData.WriteData("DELETE FROM SnapshotsAttr WHERE ReportID =" & txtID.Text)
            End If

            'for dynamic data
            Dim ValueCol As String
            Dim LinkSQL As String

            If cmbValue.Text.ToLower = "<advanced>" Then
                ValueCol = "<Advanced>"
            Else
                ValueCol = cmbTable.Text & "." & cmbValue.Text
            End If

            SQL = "UPDATE DynamicAttr SET " & _
            "KeyParameter ='" & SQLPrepare(cmbKey.Text) & "', " & _
            "Autoresume = " & Convert.ToInt32(Me.chkAutoResume.Checked) & "," & _
            "ExpireCacheAfter = " & Me.txtCacheExpiry.Value & _
            " WHERE ReportID =" & txtID.Text

            clsMarsData.WriteData(SQL)

            If optStatic.Checked Then
                For Each it As ListViewItem In parameterController.lsvParameters.Items
                    If String.Compare(it.Text, cmbKey.Text, True) = 0 Then
                        Dim kvp As KeyValuePair(Of String, String) = it.Tag

                        SQL = String.Format("UPDATE dynamicattr SET keycolumn = '{0}' WHERE reportid = '{1}'", SQLPrepare(kvp.Value), txtID.Text)

                        clsMarsData.WriteData(SQL)
                    End If
                Next
            End If

            If ValueCol <> "<Advanced>" Then
                LinkSQL = "SELECT DISTINCT " & cmbTable.Text & "." & cmbValue.Text & " FROM " & cmbTable.Text

                SQL = "UPDATE DynamicLink SET " & _
                "KeyColumn ='" & SQLPrepare(cmbTable.Text & "." & cmbColumn.Text) & "'," & _
                "KeyParameter ='" & SQLPrepare(cmbKey.Text) & "'," & _
                "LinkSQL ='" & SQLPrepare(LinkSQL) & "'," & _
                "ConString ='" & SQLPrepare(UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & UcDSN.txtPassword.Text & "|") & "'," & _
                "ValueColumn ='" & SQLPrepare(ValueCol) & "' WHERE " & _
                "ReportID =" & txtID.Text

                clsMarsData.WriteData(SQL)
            End If

            For Each item As ListViewItem In lsvDatasources.Items
                SQL = "SELECT * FROM ReportDatasource WHERE ReportID = " & txtID.Text & " AND " & _
                "DatasourceName = '" & SQLPrepare(item.Text) & "'"

                Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

                If oRs IsNot Nothing Then
                    If oRs.EOF = True Then
                        Dim sCols As String = "DatasourceID,ReportID,DatasourceName,RptUserID,RptPassword"
                        Dim sVals As String = clsMarsData.CreateDataID("reportdatasource", "datasourceid") & "," & _
                        txtID.Text & "," & _
                        "'" & SQLPrepare(item.Text) & "'," & _
                        "'default'," & _
                        "'<default>'"

                        SQL = "INSERT INTO ReportDatasource (" & sCols & ") VALUES (" & sVals & ")"

                        clsMarsData.WriteData(SQL)
                    End If

                    oRs.Close()
                End If
            Next

            With UcDSN
                gsCon = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & .txtPassword.Text
            End With

            If IsDataDriven = True Then
                If Me.cmbDDKey.Text = "" Then
                    ep.SetError(Me.cmbDDKey, "Please select the key column")
                    Me.cmbDDKey.Focus()
                    Me.stabProperties.SelectedTab = Me.tabDataDriver
                    Return
                End If

                'check if the selected key column contains unique values only. Going to do this by reading from the data cache
                Try
                    For Each row As DataRow In clsMarsReport.m_dataDrivenCache.Rows
                        Dim dupKey As String = IsNull(row(cmbDDKey.Text), "")
                        Dim rows() As DataRow

                        'use select to get all the rows where the key column matches the dupKey
                        rows = clsMarsReport.m_dataDrivenCache.Select(Me.cmbDDKey.Text & " = '" & dupKey & "'")

                        If rows IsNot Nothing Then
                            If rows.Length > 1 Then
                                ep.SetError(cmbDDKey, "The selected key column contains duplicate values of '" & dupKey & "'. Please select a column that contains UNIQUE values only.")
                                cmbDDKey.Focus()
                                Me.stabProperties.SelectedTab = Me.tabDataDriver
                                Return
                            End If
                        End If
                    Next
                Catch : End Try

                SQL = "UPDATE DataDrivenAttr SET " & _
                "SQLQuery = '" & SQLPrepare(txtQuery.Text) & "'," & _
                "ConString = '" & SQLPrepare(Me.UcDSNDD.m_conString) & "', " & _
                "FailOnOne = " & Convert.ToInt32(UcError.chkFailonOne.Checked) & ", " & _
                "GroupReports = " & Convert.ToInt32(Me.chkGroupReports.Checked) & ", " & _
                "KeyColumn = '" & SQLPrepare(Me.cmbDDKey.Text) & "' " & _
                "WHERE ReportID =" & txtID.Text

                clsMarsData.WriteData(SQL)
            End If


            If noMsg = False Then setAndShowMessageOnControl(cmdApply, "Saved!", "", "Your schedule has been updated successfully")

            OkayToClose = True

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, _
            "frmSingleProp.cmdApply_Click", _GetLineNumber(ex.StackTrace))
            OkayToClose = False
        End Try
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        cmdApply.Enabled = True
        oUI.ResetError(sender, ep)
    End Sub

    Private Sub txtFolder_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFolder.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtDBLoc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) 'Handles txtDBLoc.TextChanged
        If txtDBLoc.Text.Length > 0 And showTooltip = True Then
            Dim info As New DevComponents.DotNetBar.SuperTooltipInfo
            Dim msg As String

            msg = "Please be aware that by entering the report path manually, SQL-RD will not " & _
            "connect to your report server to list your reports. " & vbCrLf & _
            "If you would like SQL-RD to " & _
            "connect to the report server and list your reports, remove all text from this field."

            With info
                .Color = DevComponents.DotNetBar.eTooltipColor.Lemon
                .CustomSize = New System.Drawing.Size(400, 100)
                .BodyText = msg
                .HeaderText = "For your information:"
                .HeaderVisible = True
                .FooterVisible = False
            End With

            Me.SuperTooltip1.SetSuperTooltip(Me.cmdDbLoc, info)

            SuperTooltip1.ShowTooltip(Me.cmdDbLoc)

            '  Me.cmdDbLoc.Image = Me.ImageList1.Images(1)
            '   Me.cmdDbLoc.ImageAlign = ContentAlignment.MiddleCenter

            showTooltip = False
        ElseIf Me.txtDBLoc.Text.Length = 0 Then
            SuperTooltip1.SetSuperTooltip(Me.cmdDbLoc, Nothing)

            '  Me.cmdDbLoc.Image = ImageList1.Images(2)
            '  Me.cmdDbLoc.ImageAlign = ContentAlignment.MiddleCenter

            showTooltip = True
        End If

        cmdApply.Enabled = False
        cmdOK.Enabled = False
    End Sub



    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Dim SQL As String
        Dim lsv As ListViewItem

        cmdApply_Click(sender, e)

        If OkayToClose = False Then Exit Sub

        Try
            If bgWorker.IsBusy = True Then bgWorker.CancelAsync()
        Catch : End Try

        Dim oUI As New clsMarsUI

        If IsDynamic = True Then
            clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.DYNAMIC, clsMarsAudit.AuditAction.EDIT)
        ElseIf IsBursting = True Then
            clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.BURST, clsMarsAudit.AuditAction.EDIT)
        Else
            clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.SINGLES, clsMarsAudit.AuditAction.EDIT)
        End If

        UcDest.doDeletions()

        Me.Close()
    End Sub

    Private Sub cmdValidate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdValidate.Click
        If UcDSN._Validate = True Then
            cmbTable.Enabled = True
            cmbColumn.Enabled = True
            cmbValue.Enabled = True

            With UcDSN
                gsCon = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & .txtPassword.Text
            End With

            oData.GetTables(cmbTable, UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, _
                UcDSN.txtPassword.Text)

            oData.GetColumns(cmbColumn, UcDSN.cmbDSN.Text, cmbTable.Text, _
                UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

            oData.GetColumns(cmbValue, UcDSN.cmbDSN.Text, cmbTable.Text, _
                UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

            cmbValue.Items.Add("<Advanced>")
            cmbValue.Sorted = True
        Else
            _ErrorHandle("Could not connect to the selected data source", -2147636225, "ucDSN._Validate(frmDynamicSchedule.cmdValidate_Click)", 1912)
            cmbTable.Enabled = False
            cmbColumn.Enabled = False
            cmbValue.Enabled = False
        End If
    End Sub


    Private Sub cmbTable_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTable.SelectedIndexChanged
        oData.GetColumns(cmbColumn, UcDSN.cmbDSN.Text, cmbTable.Text, _
        UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)


        oData.GetColumns(cmbValue, UcDSN.cmbDSN.Text, cmbTable.Text, _
        UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

        cmbValue.Items.Add("<Advanced>")

        cmbValue.Sorted = True
    End Sub

    Private Sub cmbValue_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbValue.SelectedIndexChanged
        If cmbValue.Text.ToLower = "<advanced>" Then
            Dim oForm As New frmDynamicAdvanced

            oForm._AdvancedDynamicEdit(txtID.Text, UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, _
            UcDSN.txtPassword.Text, cmbTable.Text & "." & cmbColumn.Text, cmbKey.Text)
        End If
    End Sub

    Dim closeFlag As Boolean

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        If closeFlag = False Then
            Dim sp As DevComponents.DotNetBar.SuperTooltip = New DevComponents.DotNetBar.SuperTooltip
            Dim spi As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo("Really Cancel?", "",
                                                                                                               "Are you sure you would like to close the wizard? All your progress will be lost. Click Cancel again to confirm.",
                                                                                                           Nothing,
                                                                                                               Nothing,
                                                                                                               DevComponents.DotNetBar.eTooltipColor.Office2003)
            sp.SetSuperTooltip(sender, spi)
            sp.ShowTooltip(sender)
            closeFlag = True
        Else
            clsMarsEvent.currentEventID = 99999
            Dim oCleaner As clsMarsData = New clsMarsData
            oCleaner.CleanDB()
            Me.Close()
        End If
    End Sub

    Private Sub optStatic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles optStatic.Click
        Dim sVal As String = ""
        Try
10:         If cmbKey.Text.Length = 0 Then
20:             ep.SetError(cmbKey, "Please select the key parameter")
30:             cmbKey.Focus()
40:             Return
            End If

50:         Dim oGet As New frmKeyParameter

60:         For Each oItem As ListViewItem In parameterController.lsvParameters.Items
70:             If oItem.Text = cmbKey.Text Then
                    Dim kvp As KeyValuePair(Of String, String) = oItem.Tag

80:                 Try
90:                     sVal = kvp.Value
                    Catch : End Try

100:                Exit For
                End If
110:        Next

120:        Try
130:            clsMarsReport.GetReportParameters(clsMarsParser.Parser.ParseString(txtUrl.Text), serverUser, serverPassword, txtDBLoc.Text, txtName.Text, False, parDefaults, , formsAuth)
            Catch : End Try

            Dim label As String = ""

140:        sVal = oGet._GetParameterValue(cmbKey.Text, sVal, parDefaults(cmbKey.Text), Me.m_serverParametersTable, label)

150:        If sVal.Length = 0 Then Return

160:        For Each oItem As ListViewItem In parameterController.lsvParameters.Items
170:            If oItem.Text = cmbKey.Text Then

180:                clsMarsData.WriteData("UPDATE ReportParameter SET ParValue = '" & SQLPrepare(sVal) & "', parvaluelabel ='" & SQLPrepare(label) & "' WHERE " & _
                    "ReportID = " & txtID.Text & " AND ParName LIKE  '" & SQLPrepare(cmbKey.Text) & "'")

                    Dim kvp As KeyValuePair(Of String, String) = New KeyValuePair(Of String, String)(label, sVal)
                    oItem.Tag = kvp
210:                Exit For
                End If
250:        Next

260:        clsMarsData.WriteData("DELETE FROM DynamicAttr WHERE ReportID=" & txtID.Text)

            Dim sCols As String
            Dim sVals As String
            Dim SQL As String

270:        sCols = "DynamicID,KeyParameter,KeyColumn,ConString," & _
          "Query,ReportID"

280:        sVals = clsMarsData.CreateDataID("dynamicattr", "dynamicid") & "," & _
          "'" & SQLPrepare(cmbKey.Text) & "'," & _
          "'" & SQLPrepare(sVal) & "'," & _
          "'_'," & _
          "'_'," & _
          txtID.Text

290:        SQL = "INSERT INTO DynamicAttr(" & sCols & ") VALUES (" & sVals & ")"

300:        clsMarsData.WriteData(SQL)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try
    End Sub


    Private Sub optDynamic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles optDynamic.Click
        Dim oPar As New frmDynamicParameter
        Dim sParameter As String

        If cmbKey.Text.Length = 0 Then
            ep.SetError(cmbKey, "Please select the key parameter")
            cmbKey.Focus()
            Return
        End If

        sParameter = oPar.AddParameterDynamicQuery(cmbKey.Text, "report", txtID.Text)

        If sParameter.Length = 0 Then Return

        For Each oItem As ListViewItem In parameterController.lsvParameters.Items
            If oItem.Text = cmbKey.Text Then
                Dim kvp As KeyValuePair(Of String, String) = New KeyValuePair(Of String, String)(sParameter, sParameter)

                oItem.Tag = kvp

                clsMarsData.WriteData("UPDATE ReportParameter SET ParValue = '" & SQLPrepare(sParameter) & "', parvaluelabel='" & SQLPrepare(sParameter) & "' WHERE " & _
                "ReportID = " & txtID.Text & " AND ParName LIKE  '" & SQLPrepare(cmbKey.Text) & "'")

                Exit For
            End If
        Next
    End Sub

    


    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        For Each oItem As ListViewItem In lsvSnapshots.SelectedItems
            Try
                Dim nID As Integer

                nID = oItem.Tag

                If clsMarsData.WriteData _
                ("DELETE FROM SnapshotsAttr WHERE AttrID =" & nID) = True Then

                    System.IO.File.Delete(oItem.SubItems(1).Text)

                    oItem.Remove()
                End If
            Catch ex As Exception
                _ErrorHandle(ex.Message, Err.Number, _
                "frmSingleProp.cmdDelete_Click", _GetLineNumber(ex.StackTrace))
            End Try
        Next
    End Sub

    Private Sub cmdView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdView.Click
        Dim oProc As Process

        For Each oItem As ListViewItem In lsvSnapshots.SelectedItems
            Try
                oProc = New Process

                oProc.StartInfo.FileName = oItem.SubItems(1).Text

                oProc.Start()
            Catch ex As Exception
                _ErrorHandle(ex.Message, Err.Number, _
                "frmSingleProp.cmdView_Click", _GetLineNumber(ex.StackTrace))
            End Try
        Next
    End Sub

    Private Sub ShowSnapshots()

        Dim oRs As ADODB.Recordset
        Dim SQL As String

        If lsvSnapshots.Items.Count > 0 Then Return

        If chkSnapshots.Checked = True Then
            SQL = "SELECT * FROM SnapshotsAttr WHERE ReportID = " & txtID.Text

            oRs = clsMarsData.GetData(SQL)

            Try
                Do While oRs.EOF = False

                    Dim oItem As New ListViewItem

                    oItem.Text = oRs("datecreated").Value
                    oItem.SubItems.Add(oRs("snappath").Value)

                    oItem.Tag = oRs("attrid").Value
                    oItem.ImageIndex = 0

                    lsvSnapshots.Items.Add(oItem)

                    oRs.MoveNext()
                Loop

                oRs.Close()
            Catch ex As Exception
                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
                _GetLineNumber(ex.StackTrace))
            End Try
        End If
    End Sub

    Private Sub chkSnapshots_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSnapshots.CheckedChanged
        If chkSnapshots.Checked = True Then
            If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.sa8_Snapshots) Then
                _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, sender, getFeatDesc(featureCodes.sa8_Snapshots))
                chkSnapshots.Checked = False
                Return
            End If
        End If

        txtSnapshots.Enabled = chkSnapshots.Checked
    End Sub

    Private Sub cmdExecute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdExecute.Click
        Dim oSnap As New clsMarsSnapshots

        For Each oItem As ListViewItem In lsvSnapshots.SelectedItems
            oSnap.ExecuteSnapshot(txtID.Text, oItem.SubItems(1).Text)
        Next

    End Sub



    Private Sub cmdTestRpt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles cmdTest.Click
        noMsg = True
        cmdApply_Click(Nothing, Nothing)
        cmdApply.Enabled = True

        clsMarsReport.oReport.ViewReport(txtID.Text, True, serverUser, serverPassword, True)
        noMsg = False
    End Sub



    Private Sub cmdRequery_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRequery.Click
        If MessageBox.Show("Requery the report for parameters? This will remove the current parameters and respective values", _
        Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim logFile As String = "parameter_requery.log"
            '//store the parameters and their values
            Dim dtCache As Hashtable = New Hashtable

            clsMarsDebug.writeToDebug(logFile, "Saving current parameters", False)

            For Each it As ListViewItem In parameterController.lsvParameters.Items
                If dtCache.ContainsKey(it.Text) = False Then
                    dtCache.Add(it.Text, it.Tag)
                End If
            Next

            clsMarsDebug.writeToDebug(logFile, "Deleting current values", True)

            clsMarsData.WriteData("DELETE FROM ReportParameter WHERE ReportID =" & txtID.Text)

            clsMarsDebug.writeToDebug(logFile, "Getting server version", True)

            parameterController.loadParameters(txtUrl.Text, txtDBLoc.Text, Nothing, serverUser, serverPassword, formsAuth)

            ' UcBlank.m_ParametersList = New ArrayList

            '//reset their values
            clsMarsDebug.writeToDebug(logFile, "Resetting values", True)

            For Each item As ListViewItem In parameterController.lsvParameters.Items
                Dim name As String = item.Text


                If dtCache.ContainsKey(name) Then
                    Dim obj = dtCache.Item(name)

                    If item.SubItems(2).Text.ToLower = "true" Then '//is it multivalue??
                        If TypeOf obj Is List(Of KeyValuePair(Of String, String)) Then
                            item.Tag = obj
                        Else
                            item.Tag = Nothing
                        End If
                    Else
                        If TypeOf obj Is KeyValuePair(Of String, String) Then
                            item.Tag = obj
                        Else
                            item.Tag = Nothing
                        End If
                    End If
                End If
            Next
        End If

    End Sub


    Private Sub chkStaticDest_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkStaticDest.CheckedChanged
        GroupBox6.Enabled = Not chkStaticDest.Checked

        UcDest.StaticDest = chkStaticDest.Checked

    End Sub

    Private Sub btnView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnView.Click
        If optStatic.Checked = True Then
            optStatic_Click(sender, e)
        ElseIf optDynamic.Checked = True Then
            optDynamic_Click(sender, e)
        End If
    End Sub



    Private Sub cmdTestLink_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTestLink.Click

        Dim SQL As String
        Dim ParValue As String
        Dim LinkSQL As String
        Dim LinkCon As String
        Dim oRs As ADODB.Recordset
        Dim sDSN As String
        Dim sUser As String
        Dim sPassword As String

        ParValue = InputBox("Please provide a sample value for the parameter", Application.ProductName)

        If ParValue.Length = 0 Then Return

        Try
            Dim oLinkCon As New ADODB.Connection

            If cmbValue.Text = "<Advanced>" Then
                SQL = "SELECT * FROM DynamicLink WHERE ReportID= " & txtID.Text

                oRs = clsMarsData.GetData(SQL)

                If oRs.EOF = False Then
                    LinkSQL = oRs("linksql").Value
                    LinkCon = oRs("constring").Value
                End If

                sDSN = LinkCon.Split("|")(0)
                sUser = LinkCon.Split("|")(1)
                sPassword = LinkCon.Split("|")(2)

                oLinkCon.Open(sDSN, sUser, sPassword)

                ParValue = clsMarsData.ConvertToFieldType(LinkSQL, oLinkCon, oRs("keycolumn").Value, ParValue)

                If LinkSQL.ToLower.IndexOf("where") > -1 Then
                    LinkSQL &= " AND " & oRs("keycolumn").Value & " = " & ParValue
                Else
                    LinkSQL &= " WHERE " & oRs("keycolumn").Value & " = " & ParValue
                End If

                oRs.Close()
            Else
                sDSN = UcDSN.cmbDSN.Text
                sUser = UcDSN.txtUserID.Text
                sPassword = UcDSN.txtPassword.Text

                oLinkCon.Open(sDSN, sUser, sPassword)

                LinkSQL = "SELECT " & cmbValue.Text & " FROM " & cmbTable.Text


                ParValue = clsMarsData.ConvertToFieldType(LinkSQL, oLinkCon, cmbColumn.Text, ParValue)

                LinkSQL &= " WHERE " & cmbColumn.Text & " = " & ParValue

            End If

            oRs = New ADODB.Recordset

            oRs.Open(LinkSQL, oLinkCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

            Dim oResults As New frmDBResults

            oResults._ShowResults(oRs)

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace), LinkSQL)
        End Try

    End Sub


    Private Sub tabProperties_SelectedTabChanged(ByVal sender As Object, ByVal e As DevComponents.DotNetBar.TabStripTabChangedEventArgs)
        Select Case e.NewTab.Text.ToLower
            Case "history"
                UcHistory.DrawScheduleHistory(clsMarsScheduler.enScheduleType.REPORT, txtID.Text)
            Case "snapshots"
                ShowSnapshots()
            Case "datasources"
                'add datasources from the report that have default credentials in case user wants to set them up
                'clsMarsReport.GetReportDatasources(txtUrl.Text, serverUser, serverPassword, txtDBLoc.Text, txtName.Text, True, Me.lsvDatasources)
        End Select
    End Sub

    Private Sub lsvDatasources_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvDatasources.DoubleClick
        Dim oSet As New frmSetTableLogin
        Dim oItem As ListViewItem
        Dim sValues() As String

        If lsvDatasources.SelectedItems.Count = 0 Then Return

        oItem = lsvDatasources.SelectedItems(0)

        sValues = oSet.SaveLoginInfo(txtID.Text, oItem.Text, oItem.Tag)

        If Not sValues Is Nothing Then
            oItem.SubItems(1).Text = sValues(0)
            oItem.Tag = sValues(1)
        End If
    End Sub


    Private Sub ClearToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClearToolStripMenuItem.Click
        If lsvDatasources.SelectedItems.Count = 0 Then Return
        Dim oData As New clsMarsData

        Dim oItem As ListViewItem = lsvDatasources.SelectedItems(0)

        Dim nDataID As Integer = oItem.Tag

        If nDataID > 0 Then
            If clsMarsData.WriteData("DELETE FROM ReportDatasource WHERE DatasourceID =" & nDataID) = True Then
                oItem.SubItems(1).Text = "default"
            End If
        End If
    End Sub
    Private Sub lsvDatasources_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lsvDatasources.KeyUp
        If e.KeyCode = Keys.Enter Then
            Me.lsvDatasources_DoubleClick(sender, e)
        End If
    End Sub


    Private Sub btnRefreshDS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshDS.Click
        lsvDatasources.Items.Clear()

        clsMarsReport.GetReportDatasources(clsMarsParser.Parser.ParseString(txtUrl.Text), serverUser, serverPassword, txtDBLoc.Text, txtName.Text, True, Me.lsvDatasources, formsAuth)

        clsMarsData.WriteData("DELETE FROM ReportDatasource WHERE ReportID=" & txtID.Text)
    End Sub

    Private Sub tvHistory_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs)

    End Sub

    Dim noMsg As Boolean = False

    Private Sub cmdTest2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTest2.Click
        noMsg = True
        cmdApply_Click(Nothing, Nothing)
        cmdTestRpt_Click(Nothing, Nothing)
        noMsg = False
    End Sub

    Private Sub bgWorker_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgWorker.DoWork
        Try
            m_serverParametersTable = clsMarsReport.oReport.getserverReportParameters(Me.txtUrl.Text, Me.txtDBLoc.Text, serverUser, serverPassword, "", formsAuth)

            Me.GetParameterAvailableValues()
        Catch
            oUI.BusyProgress(, , True)
        End Try
    End Sub

    Private Sub bgWorker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgWorker.RunWorkerCompleted

    End Sub

    Private Sub btnConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnect.Click
        grpQuery.Enabled = UcDSNDD._Validate
    End Sub

    Private Sub btnBuild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuild.Click
        Dim queryBuilder As frmDynamicParameter = New frmDynamicParameter

        Dim sVals(1) As String
        Dim sCon As String = ""
        Dim temp As String = txtQuery.Text

        If UcDSNDD.cmbDSN.Text.Length > 0 Then
            sCon = UcDSNDD.cmbDSN.Text & "|" & UcDSNDD.txtUserID.Text & "|" & UcDSNDD.txtPassword.Text & "|"
        End If

        queryBuilder.m_showStar = True
        queryBuilder.Label1.Text = "Please select the table that holds the required data"

        sVals = queryBuilder.ReturnSQLQuery(sCon, txtQuery.Text)

        If sVals Is Nothing Then Return

        sCon = sVals(0)
        txtQuery.Text = sVals(1)

        UcDSNDD.cmbDSN.Text = sCon.Split("|")(0)
        UcDSNDD.txtUserID.Text = sCon.Split("|")(1)
        UcDSNDD.txtPassword.Text = sCon.Split("|")(2)

        Dim oRs As ADODB.Recordset = New ADODB.Recordset
        Dim oCon As ADODB.Connection = New ADODB.Connection

        oCon.Open(UcDSNDD.cmbDSN.Text, UcDSNDD.txtUserID.Text, UcDSNDD.txtPassword.Text)

        oRs.Open(clsMarsParser.Parser.ParseString(txtQuery.Text), oCon, ADODB.CursorTypeEnum.adOpenStatic)

        Me.cmbDDKey.Items.Clear()

        Dim I As Integer = 0

        frmRDScheduleWizard.m_DataFields = New Hashtable

        For Each fld As ADODB.Field In oRs.Fields
            frmRDScheduleWizard.m_DataFields.Add(I, fld.Name)
            Me.cmbDDKey.Items.Add(fld.Name)
            I += 1
        Next

        Dim tmpCon As String = ""

        With UcDSNDD
            tmpCon = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & _EncryptDBValue(.txtPassword.Text)
        End With

        If txtQuery.Text.ToLower <> temp.ToLower Then
            clsMarsReport.populateDataDrivenCache(txtQuery.Text, tmpCon, True)
        End If

        oRs.Close()
        oCon.Close()

        oRs = Nothing
        oCon = Nothing
    End Sub

    Private Sub chkAutoResume_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAutoResume.CheckedChanged
        txtCacheExpiry.Enabled = Me.chkAutoResume.Checked
    End Sub
    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.Undo()
        Catch : End Try
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.Cut()
        Catch : End Try
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.Copy()
        Catch : End Try
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.Paste()
        Catch : End Try
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.SelectedText = ""
        Catch : End Try
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.SelectAll()
        Catch : End Try
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim oInsert As New frmInserter(99999)
        oInsert.m_EventBased = False
        oInsert.GetConstants(Me)
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Dim res As DialogResult = MessageBox.Show("Are you sure you would like to clear all the Dynamic linking information for this schedule?", _
        Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If res = Windows.Forms.DialogResult.Yes Then
            clsMarsData.WriteData("DELETE FROM DynamicLink WHERE ReportID=" & Me.txtID.Text)

            With UcDSN
                .cmbDSN.Text = ""
                .txtPassword.Text = ""
                .txtUserID.Text = ""
            End With

            Me.cmbTable.Text = ""
            cmbValue.Text = ""
            cmbColumn.Text = ""
        End If
    End Sub

    Private Sub chkGroupReports_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkGroupReports.CheckedChanged
        Me.UcDest.disableEmbed = chkGroupReports.Checked
    End Sub

    Private Sub lsvParameters_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub cmbDDKey_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDDKey.SelectedIndexChanged
        ep.SetError(Me.cmbDDKey, "")
        cmdApply.Enabled = True
    End Sub

    Dim m_inserter As frmInserter = New frmInserter(99999)
    Private Sub stabProperties_SelectedTabChanged(sender As Object, e As DevComponents.DotNetBar.SuperTabStripSelectedTabChangedEventArgs) Handles stabProperties.SelectedTabChanged
        Try

            If e.NewValue.Text = "History" Then
                With UcHistory
                    .m_objectID = txtID.Text
                    .m_scheduleType = clsMarsScheduler.enScheduleType.REPORT
                    .m_showControls = True
                    .DrawScheduleHistory()
                End With
            ElseIf e.NewValue.Text = "Snapshots" Then
                ShowSnapshots()
            ElseIf e.NewValue.Text = "Exception Handling" Then
                UcBlankReportX1.UcBlankReportTasks.ShowAfterType = False
                UcBlankReportX1.UcBlankReportTasks.ScheduleID = txtID.Text
                UcBlankReportX1.UcBlankReportTasks.LoadTasks()
            End If

            Select Case e.NewValue.Text
                Case "Report"
                    If parameterController.lsvParameters.Items.Count = 0 Then
                        parameterController.loadParameters(txtUrl.Text, txtDBLoc.Text, Nothing, serverUser, serverPassword, formsAuth)
                    End If

                    If m_inserter.IsDisposed Then
                        m_inserter = New frmInserter(0)
                    End If

                    m_inserter.GetConstants(Me)
                    ' UcParList.addHandlers()
                Case Else
                    m_inserter.Hide()
            End Select
        Catch : End Try
    End Sub

    Private Sub cmbKey_DropDown(sender As Object, e As System.EventArgs) Handles cmbKey.DropDown
        If cmbKey.Items.Count = 0 Then
            For Each par As ListViewItem In parameterController.lsvParameters.Items
                cmbKey.Items.Add(par.Text)
            Next
        End If
    End Sub
End Class
