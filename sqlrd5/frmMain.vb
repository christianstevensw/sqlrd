Imports Microsoft.Win32
Imports DevComponents.DotNetBar
Imports System.ServiceProcess
Imports System.Management

Friend Class frmMain
    Inherits System.Windows.Forms.Form
    'Public StatusBar As New ProgressStatus
    Friend WithEvents BubbleButton14 As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents BubbleBar As DevComponents.DotNetBar.BubbleBar
    Friend WithEvents BubbleBarTab2 As DevComponents.DotNetBar.BubbleBarTab
    Friend WithEvents btnOptions As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnSystemMonitor As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnProperties As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnDesign As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnCalendar As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnAddressBook As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents BubbleButton1 As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnSingle As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnPackage As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnDynamic As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnDynamicPackage As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnAutomation As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnEvent As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnBursting As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents BubbleButton2 As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnHelp As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnForums As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnKbase As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnHowTo As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents TabItem1 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabItem2 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabItem3 As DevComponents.DotNetBar.TabItem
    Friend WithEvents Super As DevComponents.DotNetBar.SuperTooltip
    Dim oUI As clsMarsUI = New clsMarsUI
    Public dock As frmDock
    Shared UserCancel As Boolean = False
    Public m_formSize As Hashtable = Nothing
    Public dtOOP As DataTable
    Friend WithEvents tmCheckUpdate As System.Timers.Timer
    Public pWatcher As frmOOPwatcher = Nothing
    Public Shared Property m_UserCancel() As Boolean
        Get
            Dim returnValue As Boolean = UserCancel

            If UserCancel = True Then
                UserCancel = False
            End If

            Return returnValue
        End Get
        Set(ByVal value As Boolean)
            UserCancel = value
        End Set
    End Property
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        Dim c As Control
        Dim mdic As MdiClient

        For Each c In Me.Controls
            Try
                mdic = CType(c, Control)
            Catch
                mdic = Nothing
            End Try

            If Not mdic Is Nothing Then

                mdic.BackColor = System.Drawing.Color.FromArgb(128, 128, 128)

            End If

        Next

        Me.CheckForIllegalCrossThreadCalls = False
        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ofd As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents barLeftDockSite As DevComponents.DotNetBar.DockSite
    Friend WithEvents barRightDockSite As DevComponents.DotNetBar.DockSite
    Friend WithEvents barTopDockSite As DevComponents.DotNetBar.DockSite
    Friend WithEvents barBottomDockSite As DevComponents.DotNetBar.DockSite
    Friend WithEvents DotNetBarManager1 As DevComponents.DotNetBar.DotNetBarManager
    Friend WithEvents tmDate As System.Timers.Timer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.ofd = New System.Windows.Forms.FolderBrowserDialog
        Me.DotNetBarManager1 = New DevComponents.DotNetBar.DotNetBarManager(Me.components)
        Me.barBottomDockSite = New DevComponents.DotNetBar.DockSite
        Me.barLeftDockSite = New DevComponents.DotNetBar.DockSite
        Me.barRightDockSite = New DevComponents.DotNetBar.DockSite
        Me.barTopDockSite = New DevComponents.DotNetBar.DockSite
        Me.tmDate = New System.Timers.Timer
        Me.BubbleBar = New DevComponents.DotNetBar.BubbleBar
        Me.BubbleBarTab2 = New DevComponents.DotNetBar.BubbleBarTab(Me.components)
        Me.btnOptions = New DevComponents.DotNetBar.BubbleButton
        Me.btnSystemMonitor = New DevComponents.DotNetBar.BubbleButton
        Me.btnProperties = New DevComponents.DotNetBar.BubbleButton
        Me.btnDesign = New DevComponents.DotNetBar.BubbleButton
        Me.btnCalendar = New DevComponents.DotNetBar.BubbleButton
        Me.btnAddressBook = New DevComponents.DotNetBar.BubbleButton
        Me.BubbleButton1 = New DevComponents.DotNetBar.BubbleButton
        Me.btnSingle = New DevComponents.DotNetBar.BubbleButton
        Me.btnPackage = New DevComponents.DotNetBar.BubbleButton
        Me.btnDynamic = New DevComponents.DotNetBar.BubbleButton
        Me.btnDynamicPackage = New DevComponents.DotNetBar.BubbleButton
        Me.btnAutomation = New DevComponents.DotNetBar.BubbleButton
        Me.btnEvent = New DevComponents.DotNetBar.BubbleButton
        Me.btnBursting = New DevComponents.DotNetBar.BubbleButton
        Me.BubbleButton2 = New DevComponents.DotNetBar.BubbleButton
        Me.btnHelp = New DevComponents.DotNetBar.BubbleButton
        Me.btnForums = New DevComponents.DotNetBar.BubbleButton
        Me.btnKbase = New DevComponents.DotNetBar.BubbleButton
        Me.btnHowTo = New DevComponents.DotNetBar.BubbleButton
        Me.TabItem1 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabItem2 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabItem3 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.Super = New DevComponents.DotNetBar.SuperTooltip
        Me.BubbleButton14 = New DevComponents.DotNetBar.BubbleButton
        Me.tmCheckUpdate = New System.Timers.Timer
        CType(Me.tmDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BubbleBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tmCheckUpdate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DotNetBarManager1
        '
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins)
        Me.DotNetBarManager1.BottomDockSite = Me.barBottomDockSite
        Me.DotNetBarManager1.DefinitionName = "frmMain.DotNetBarManager1.xml"
        Me.DotNetBarManager1.LeftDockSite = Me.barLeftDockSite
        Me.DotNetBarManager1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.DotNetBarManager1.ParentForm = Me
        Me.DotNetBarManager1.RightDockSite = Me.barRightDockSite
        Me.DotNetBarManager1.ShowCustomizeContextMenu = False
        Me.DotNetBarManager1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003
        Me.DotNetBarManager1.ThemeAware = False
        Me.DotNetBarManager1.TopDockSite = Me.barTopDockSite
        '
        'barBottomDockSite
        '
        Me.barBottomDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.barBottomDockSite.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barBottomDockSite.Location = New System.Drawing.Point(0, 605)
        Me.barBottomDockSite.Name = "barBottomDockSite"
        Me.barBottomDockSite.NeedsLayout = False
        Me.barBottomDockSite.Size = New System.Drawing.Size(775, 25)
        Me.barBottomDockSite.TabIndex = 4
        Me.barBottomDockSite.TabStop = False
        '
        'barLeftDockSite
        '
        Me.barLeftDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.barLeftDockSite.Dock = System.Windows.Forms.DockStyle.Left
        Me.barLeftDockSite.Location = New System.Drawing.Point(0, 25)
        Me.barLeftDockSite.Name = "barLeftDockSite"
        Me.barLeftDockSite.NeedsLayout = False
        Me.barLeftDockSite.Size = New System.Drawing.Size(0, 580)
        Me.barLeftDockSite.TabIndex = 1
        Me.barLeftDockSite.TabStop = False
        '
        'barRightDockSite
        '
        Me.barRightDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.barRightDockSite.Dock = System.Windows.Forms.DockStyle.Right
        Me.barRightDockSite.Location = New System.Drawing.Point(775, 25)
        Me.barRightDockSite.Name = "barRightDockSite"
        Me.barRightDockSite.NeedsLayout = False
        Me.barRightDockSite.Size = New System.Drawing.Size(0, 580)
        Me.barRightDockSite.TabIndex = 2
        Me.barRightDockSite.TabStop = False
        '
        'barTopDockSite
        '
        Me.barTopDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.barTopDockSite.Dock = System.Windows.Forms.DockStyle.Top
        Me.barTopDockSite.Location = New System.Drawing.Point(0, 0)
        Me.barTopDockSite.Name = "barTopDockSite"
        Me.barTopDockSite.NeedsLayout = False
        Me.barTopDockSite.Size = New System.Drawing.Size(775, 25)
        Me.barTopDockSite.TabIndex = 3
        Me.barTopDockSite.TabStop = False
        '
        'tmDate
        '
        Me.tmDate.Enabled = True
        Me.tmDate.Interval = 1000
        Me.tmDate.SynchronizingObject = Me
        '
        'BubbleBar
        '
        Me.BubbleBar.Alignment = DevComponents.DotNetBar.eBubbleButtonAlignment.Bottom
        Me.BubbleBar.AntiAlias = True
        Me.BubbleBar.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.BubbleBar.ButtonBackAreaStyle.BackColor = System.Drawing.Color.Gray
        Me.BubbleBar.ButtonBackAreaStyle.BackColorGradientAngle = 90
        Me.BubbleBar.ButtonBackAreaStyle.BorderColor = System.Drawing.Color.Gray
        Me.BubbleBar.ButtonBackAreaStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.BubbleBar.ButtonBackAreaStyle.BorderLeftWidth = 1
        Me.BubbleBar.ButtonBackAreaStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.BubbleBar.ButtonBackAreaStyle.BorderRightWidth = 1
        Me.BubbleBar.ButtonBackAreaStyle.BorderTopWidth = 1
        Me.BubbleBar.ButtonBackAreaStyle.PaddingBottom = 3
        Me.BubbleBar.ButtonBackAreaStyle.PaddingLeft = 3
        Me.BubbleBar.ButtonBackAreaStyle.PaddingRight = 3
        Me.BubbleBar.ButtonBackAreaStyle.PaddingTop = 3
        Me.BubbleBar.ButtonSpacing = 1
        Me.BubbleBar.ImageSizeNormal = New System.Drawing.Size(24, 24)
        Me.BubbleBar.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.BubbleBar.Location = New System.Drawing.Point(38, 442)
        Me.BubbleBar.MouseOverTabColors.BorderColor = System.Drawing.SystemColors.Highlight
        Me.BubbleBar.Name = "BubbleBar"
        Me.BubbleBar.SelectedTab = Me.BubbleBarTab2
        Me.BubbleBar.SelectedTabColors.BorderColor = System.Drawing.Color.Black
        Me.BubbleBar.Size = New System.Drawing.Size(613, 48)
        Me.BubbleBar.TabIndex = 25
        Me.BubbleBar.Tabs.Add(Me.BubbleBarTab2)
        Me.BubbleBar.Text = "Create"
        Me.BubbleBar.Visible = False
        '
        'BubbleBarTab2
        '
        Me.BubbleBarTab2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(244, Byte), Integer), CType(CType(213, Byte), Integer))
        Me.BubbleBarTab2.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BubbleBarTab2.Buttons.AddRange(New DevComponents.DotNetBar.BubbleButton() {Me.btnOptions, Me.btnSystemMonitor, Me.btnProperties, Me.btnDesign, Me.btnCalendar, Me.btnAddressBook, Me.BubbleButton1, Me.btnSingle, Me.btnPackage, Me.btnDynamic, Me.btnDynamicPackage, Me.btnAutomation, Me.btnEvent, Me.btnBursting, Me.BubbleButton2, Me.btnHelp, Me.btnForums, Me.btnKbase, Me.btnHowTo})
        Me.BubbleBarTab2.DarkBorderColor = System.Drawing.Color.FromArgb(CType(CType(190, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BubbleBarTab2.LightBorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.BubbleBarTab2.Name = "BubbleBarTab2"
        Me.BubbleBarTab2.PredefinedColor = DevComponents.DotNetBar.eTabItemColor.Yellow
        Me.BubbleBarTab2.Text = "Task Dock"
        Me.BubbleBarTab2.TextColor = System.Drawing.Color.Black
        '
        'btnOptions
        '
        Me.btnOptions.Image = CType(resources.GetObject("btnOptions.Image"), System.Drawing.Image)
        Me.btnOptions.ImageLarge = CType(resources.GetObject("btnOptions.ImageLarge"), System.Drawing.Image)
        Me.btnOptions.Name = "btnOptions"
        Me.btnOptions.TooltipText = "System Options"
        '
        'btnSystemMonitor
        '
        Me.btnSystemMonitor.Image = CType(resources.GetObject("btnSystemMonitor.Image"), System.Drawing.Image)
        Me.btnSystemMonitor.ImageLarge = CType(resources.GetObject("btnSystemMonitor.ImageLarge"), System.Drawing.Image)
        Me.btnSystemMonitor.Name = "btnSystemMonitor"
        Me.btnSystemMonitor.TooltipText = "System Monitor"
        '
        'btnProperties
        '
        Me.btnProperties.Enabled = False
        Me.btnProperties.Image = CType(resources.GetObject("btnProperties.Image"), System.Drawing.Image)
        Me.btnProperties.ImageLarge = CType(resources.GetObject("btnProperties.ImageLarge"), System.Drawing.Image)
        Me.btnProperties.Name = "btnProperties"
        Me.btnProperties.TooltipText = "Properties"
        '
        'btnDesign
        '
        Me.btnDesign.Enabled = False
        Me.btnDesign.Image = CType(resources.GetObject("btnDesign.Image"), System.Drawing.Image)
        Me.btnDesign.ImageLarge = CType(resources.GetObject("btnDesign.ImageLarge"), System.Drawing.Image)
        Me.btnDesign.Name = "btnDesign"
        Me.btnDesign.TooltipText = "Report Design"
        '
        'btnCalendar
        '
        Me.btnCalendar.Image = CType(resources.GetObject("btnCalendar.Image"), System.Drawing.Image)
        Me.btnCalendar.ImageLarge = CType(resources.GetObject("btnCalendar.ImageLarge"), System.Drawing.Image)
        Me.btnCalendar.Name = "btnCalendar"
        Me.btnCalendar.TooltipText = "Custom Calendar"
        '
        'btnAddressBook
        '
        Me.btnAddressBook.Image = CType(resources.GetObject("btnAddressBook.Image"), System.Drawing.Image)
        Me.btnAddressBook.ImageLarge = CType(resources.GetObject("btnAddressBook.ImageLarge"), System.Drawing.Image)
        Me.btnAddressBook.Name = "btnAddressBook"
        Me.btnAddressBook.TooltipText = "Address Book"
        '
        'BubbleButton1
        '
        Me.BubbleButton1.Enabled = False
        Me.BubbleButton1.Image = CType(resources.GetObject("BubbleButton1.Image"), System.Drawing.Image)
        Me.BubbleButton1.ImageLarge = CType(resources.GetObject("BubbleButton1.ImageLarge"), System.Drawing.Image)
        Me.BubbleButton1.Name = "BubbleButton1"
        '
        'btnSingle
        '
        Me.btnSingle.Image = CType(resources.GetObject("btnSingle.Image"), System.Drawing.Image)
        Me.btnSingle.ImageLarge = CType(resources.GetObject("btnSingle.ImageLarge"), System.Drawing.Image)
        Me.btnSingle.Name = "btnSingle"
        Me.btnSingle.TooltipText = "Single Schedule"
        '
        'btnPackage
        '
        Me.btnPackage.Image = CType(resources.GetObject("btnPackage.Image"), System.Drawing.Image)
        Me.btnPackage.ImageLarge = CType(resources.GetObject("btnPackage.ImageLarge"), System.Drawing.Image)
        Me.btnPackage.Name = "btnPackage"
        Me.btnPackage.TooltipText = "Packaged Reports Schedule"
        '
        'btnDynamic
        '
        Me.btnDynamic.Image = CType(resources.GetObject("btnDynamic.Image"), System.Drawing.Image)
        Me.btnDynamic.ImageLarge = CType(resources.GetObject("btnDynamic.ImageLarge"), System.Drawing.Image)
        Me.btnDynamic.Name = "btnDynamic"
        Me.btnDynamic.TooltipText = "Dynamic Schedule"
        '
        'btnDynamicPackage
        '
        Me.btnDynamicPackage.Image = CType(resources.GetObject("btnDynamicPackage.Image"), System.Drawing.Image)
        Me.btnDynamicPackage.ImageLarge = CType(resources.GetObject("btnDynamicPackage.ImageLarge"), System.Drawing.Image)
        Me.btnDynamicPackage.Name = "btnDynamicPackage"
        Me.btnDynamicPackage.TooltipText = "Dynamic Package Schedule"
        '
        'btnAutomation
        '
        Me.btnAutomation.Image = CType(resources.GetObject("btnAutomation.Image"), System.Drawing.Image)
        Me.btnAutomation.ImageLarge = CType(resources.GetObject("btnAutomation.ImageLarge"), System.Drawing.Image)
        Me.btnAutomation.Name = "btnAutomation"
        Me.btnAutomation.TooltipText = "Automation Schedule"
        '
        'btnEvent
        '
        Me.btnEvent.Image = CType(resources.GetObject("btnEvent.Image"), System.Drawing.Image)
        Me.btnEvent.ImageLarge = CType(resources.GetObject("btnEvent.ImageLarge"), System.Drawing.Image)
        Me.btnEvent.Name = "btnEvent"
        Me.btnEvent.TooltipText = "Event-Based Schedule"
        '
        'btnBursting
        '
        Me.btnBursting.Image = CType(resources.GetObject("btnBursting.Image"), System.Drawing.Image)
        Me.btnBursting.ImageLarge = CType(resources.GetObject("btnBursting.ImageLarge"), System.Drawing.Image)
        Me.btnBursting.Name = "btnBursting"
        Me.btnBursting.TooltipText = "Bursting Schedule"
        '
        'BubbleButton2
        '
        Me.BubbleButton2.Enabled = False
        Me.BubbleButton2.Image = CType(resources.GetObject("BubbleButton2.Image"), System.Drawing.Image)
        Me.BubbleButton2.ImageLarge = CType(resources.GetObject("BubbleButton2.ImageLarge"), System.Drawing.Image)
        Me.BubbleButton2.Name = "BubbleButton2"
        '
        'btnHelp
        '
        Me.btnHelp.Image = CType(resources.GetObject("btnHelp.Image"), System.Drawing.Image)
        Me.btnHelp.ImageLarge = CType(resources.GetObject("btnHelp.ImageLarge"), System.Drawing.Image)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.TooltipText = "Help"
        '
        'btnForums
        '
        Me.btnForums.Image = CType(resources.GetObject("btnForums.Image"), System.Drawing.Image)
        Me.btnForums.ImageLarge = CType(resources.GetObject("btnForums.ImageLarge"), System.Drawing.Image)
        Me.btnForums.Name = "btnForums"
        Me.btnForums.TooltipText = "User Forums"
        '
        'btnKbase
        '
        Me.btnKbase.Image = CType(resources.GetObject("btnKbase.Image"), System.Drawing.Image)
        Me.btnKbase.ImageLarge = CType(resources.GetObject("btnKbase.ImageLarge"), System.Drawing.Image)
        Me.btnKbase.Name = "btnKbase"
        Me.btnKbase.TooltipText = "Knowledge Base"
        '
        'btnHowTo
        '
        Me.btnHowTo.Image = CType(resources.GetObject("btnHowTo.Image"), System.Drawing.Image)
        Me.btnHowTo.ImageLarge = CType(resources.GetObject("btnHowTo.ImageLarge"), System.Drawing.Image)
        Me.btnHowTo.Name = "btnHowTo"
        Me.btnHowTo.TooltipText = "How To...Demos"
        '
        'TabItem1
        '
        Me.TabItem1.Name = "TabItem1"
        Me.TabItem1.Text = "TabItem1"
        '
        'TabItem2
        '
        Me.TabItem2.Name = "TabItem2"
        Me.TabItem2.Text = "TabItem2"
        '
        'TabItem3
        '
        Me.TabItem3.Name = "TabItem3"
        Me.TabItem3.Text = "TabItem3"
        '
        'Super
        '
        Me.Super.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Super.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.Super.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'BubbleButton14
        '
        Me.BubbleButton14.Enabled = False
        Me.BubbleButton14.Image = CType(resources.GetObject("BubbleButton14.Image"), System.Drawing.Image)
        Me.BubbleButton14.ImageLarge = CType(resources.GetObject("BubbleButton14.ImageLarge"), System.Drawing.Image)
        Me.BubbleButton14.Name = "BubbleButton14"
        Me.BubbleButton14.TooltipText = "Report Design"
        '
        'tmCheckUpdate
        '
        Me.tmCheckUpdate.Interval = 5000
        Me.tmCheckUpdate.SynchronizingObject = Me
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(775, 630)
        Me.Controls.Add(Me.BubbleBar)
        Me.Controls.Add(Me.barLeftDockSite)
        Me.Controls.Add(Me.barRightDockSite)
        Me.Controls.Add(Me.barTopDockSite)
        Me.Controls.Add(Me.barBottomDockSite)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.MinimumSize = New System.Drawing.Size(600, 400)
        Me.Name = "frmMain"
        Me.Text = "SQL-RD"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.tmDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BubbleBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tmCheckUpdate, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    'Private Sub InitializeStatusBar()
    '    Dim info = New System.Windows.Forms.StatusBarPanel
    '    Dim progress = New System.Windows.Forms.StatusBarPanel
    '    Dim DateTimePanel As System.Windows.Forms.StatusBarPanel = New System.Windows.Forms.StatusBarPanel

    '    info.Text = "Ready"
    '    info.Width = 200
    '    DateTimePanel.Text = Date.Now.Date
    '    DateTimePanel.Alignment = HorizontalAlignment.Right
    '    progress.width = 700

    '    DateTimePanel.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring

    '    'progress.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring

    '    With StatusBar
    '        .Panels.Add(info)
    '        .Panels.Add(progress)
    '        .Panels.Add(DateTimePanel)
    '        .ShowPanels = True
    '        .setProgressBar = 1
    '        .progressBar.Minimum = 0
    '        .progressBar.Maximum = 100

    '    End With

    '    Me.Controls.Add(StatusBar)

    '    StatusBar.progressBar.Visible = False
    'End Sub

    Private Sub _CheckODBC()
        Dim ShowMsg As Boolean = False
        Dim sType As String
        Dim sMsg As String

        If Not (IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.sa6_StoreInODBC)) And gConType = "ODBC" Then

            sType = "an ODBC/SQL Server"

            ShowMsg = True
        End If

        If ShowMsg = True Then
            sMsg = "Your current SQL-RD edition does not allows the use of " & sType & " datasource. " & _
            "Press OK to migrate your SQL-RD to file system"

            MessageBox.Show(sMsg, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            clsMigration.MigrateSystem(clsMigration.m_datConString, "", "")

            'Dim oSQL As New frmSQLAdmin

            'oSQL._ReturntoDat(True)

            oUI.SaveRegistry("ConType", "DAT")
        End If

    End Sub

    Private Sub MoveBubbleBar()
        Try
            BubbleBar.Top = Me.Height - (BubbleBar.Height + 57)
            BubbleBar.Left = (Me.Width / 2) - (BubbleBar.Width / 2)

            'userInfo.Owner = Me
            'userInfo.Top = Me.Height - (userInfo.Height + 57)
            'userInfo.Left = Me.Width - (userInfo.Width + 10)
        Catch : End Try
    End Sub

    Protected Sub ProcessTaskPane()
        Dim oBtn As ButtonItem
        Dim oMenu As ButtonItem

        oBtn = DotNetBarManager1.GetItem("item_109", True)

        oMenu = DotNetBarManager1.GetItem("btnTaskPane", True)

        If oUI.ReadRegistry("ShowTaskPane", "1") = "1" Then
            oMenu.Checked = True

            If Not oBtn Is Nothing Then
                Dim oTaskPane As Bar

                oTaskPane = DotNetBarManager1.GetItemBar(oBtn)
                oTaskPane.Visible = True
            End If
        Else
            oMenu.Checked = False

            If Not oBtn Is Nothing Then
                Dim oTaskPane As Bar

                oTaskPane = DotNetBarManager1.GetItemBar(oBtn)
                oTaskPane.Visible = False
            End If
        End If

    End Sub
    Private Sub DotNetBarManager1_ContainerLoadControl(ByVal sender As Object, ByVal e As System.EventArgs) Handles DotNetBarManager1.ContainerLoadControl
        ' Always cast to the BaseItem first since there could be different types coming in
        Dim item As BaseItem = CType(sender, BaseItem)
        Dim dockitem As DockContainerItem

        If item.Name = "dDock" Then
            dockitem = CType(item, DockContainerItem)
            dock = New frmDock
            dock.TopLevel = False
            dockitem.Control = dock
            item.Displayed = True
        End If
    End Sub

    Private Sub frmMain_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            tmDate.Stop()
        Catch : End Try
    End Sub

    Private Sub frmMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try


            Dim size As String = Me.m_formSize("Width") & "," & Me.m_formSize("Height")

            oUI.SaveRegistry("frmWindow", size)

            'clsSettingsManager.Appconfig.savetoConfigFile(gConfigFile)

            clsSystemTools.BackConfigFile(clsSystemTools.configSaveTime.onClose)


        Catch ex As Exception

        End Try
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim oSplash As frmSplash = New frmSplash

        oSplash.ShowDialog()

        If oSplash.IsValidated = False Then
            End
        End If

        oUI.BusyProgress(10, "Loading...")

        _FirstRun()

        _CheckODBC()

        'InitializeStatusBar()

        oUI.BusyProgress(30, "Loading...")

        Dim oDate As LabelItem = DotNetBarManager1.GetItem("item_925")

        oDate.Text = Now.Date

        '        nWindowCount = 0
        ReDim Preserve oWindow(nWindowCount)
        oWindow(nWindowCount) = New frmWindow
        oWindow(nWindowCount).MdiParent = Me
        nWindowCurrent = nWindowCount
        oWindow(nWindowCount).Tag = nWindowCount
        oWindow(nWindowCount).Text = "Desktop"

        oWindow(nWindowCount).Show()

        oUI.BusyProgress(50, "Loading...")

        Dim sView As String = oUI.ReadRegistry("WindowViewStyle", "LargeIcon")

        Dim oBtnView(3) As ButtonItem
        Dim oUser As LabelItem
        Dim oSys As ButtonItem

        oBtnView(0) = DotNetBarManager1.GetItem("item_568")
        oBtnView(1) = DotNetBarManager1.GetItem("item_575")
        oBtnView(2) = DotNetBarManager1.GetItem("item_582")
        oBtnView(3) = DotNetBarManager1.GetItem("ButtonItem2")

        oUser = DotNetBarManager1.GetItem("taskUser", True)

        oUser.Text = "User: " & gRole & "\" & gUser

        oUser = DotNetBarManager1.GetItem("taskCon", True)
        oSys = DotNetBarManager1.GetItem("item_153")

        oUI.BusyProgress(60, "Loading...")

        If gConType = "DAT" Then
            oUser.Text = "Connection: File"
            oSys.Text = "Migrate to SQL Server/ODBC"
        ElseIf gConType = "LOCAL" Then
            oUser.Text = "Connection: Local"
            oSys.Text = "Migrate to ODBC"
        Else
            oUser.Text = "Connection : ODBC"
            oSys.Text = "Migrate to Local System"
            oSys.Visible = False
        End If


        oSys = DotNetBarManager1.GetItem("item_166")

        If gConType = "DAT" Then
            oSys.Enabled = True
        ElseIf gConType = "LOCAL" Then
            oSys.Enabled = False
        Else
            oSys.Enabled = True
        End If

        oSys = DotNetBarManager1.GetItem("item_247")

        If gConType = "ODBC" Then
            oSys.Text = "To Local SQL Server"
            oSys.Enabled = True
        Else
            oSys.Enabled = False
        End If

        oSys = DotNetBarManager1.GetItem("item_258")

        If gConType = "LOCAL" Or gConType = "DAT" Then
            oSys.Enabled = True
            oSys.Text = "To ODBC"
        Else
            oSys.Enabled = False
        End If

        oSys = DotNetBarManager1.GetItem("item_163")

        If gConType = "LOCAL" Or gConType = "DAT" Then
            oSys.Enabled = True
        Else
            oSys.Enabled = True
        End If

        oSys = DotNetBarManager1.GetItem("item_487")

        If gConType = "ODBC" Or gConType = "LOCAL" Then
            oSys.Enabled = False
        Else
            oSys.Enabled = True
        End If

        oSys = DotNetBarManager1.GetItem("item_246")

        If oSys IsNot Nothing Then
            If clsMarsSecurity._HasGroupAccess("Activation & Registration", , , True) Then
                oSys.Enabled = True
            Else
                oSys.Enabled = False
            End If
        End If

        oSys = DotNetBarManager1.GetItem("ButtonItem11")

        If oSys IsNot Nothing Then
            If clsMarsSecurity._HasGroupAccess("Feature Upgrades", , , True) Then
                oSys.Enabled = True
            Else
                oSys.Enabled = False
            End If
        End If

        Select Case sView.ToLower
            Case "largeicon"

                oBtnView(0).Checked = True
                oBtnView(1).Checked = False
                oBtnView(2).Checked = False
                oBtnView(3).Checked = False
            Case "list"

                oBtnView(0).Checked = False
                oBtnView(1).Checked = True
                oBtnView(2).Checked = False
                oBtnView(3).Checked = False
            Case "details"

                oBtnView(0).Checked = False
                oBtnView(1).Checked = False
                oBtnView(2).Checked = True
                oBtnView(3).Checked = False
            Case "tile"
                oBtnView(0).Checked = False
                oBtnView(1).Checked = False
                oBtnView(2).Checked = False
                oBtnView(3).Checked = True
        End Select

        oUI.BusyProgress(70, "Loading...")

        Me.Text = "SQL-RD - " & gsEdition & " Edition"

        If gnEdition = MarsGlobal.gEdition.EVALUATION Or (gnEdition = gEdition.SILVER And nTimeLeft <> 0) Then
            Dim sCustNo As String

            sCustNo = oUI.ReadRegistry("CustNo", "")

            Me.Text &= " [" & nTimeLeft & " days left]"

            oSys = DotNetBarManager1.GetItem("item_314")

            oSys.Visible = False

            oSys = DotNetBarManager1.GetItem("item_475")

            If sCustNo <> "" Then
                oSys.Visible = False
            End If
        Else
            oSys = DotNetBarManager1.GetItem("item_475")

            oSys.Visible = False
        End If

        oUI.BusyProgress(80, "Loading...")

        Dim FirstRun As Boolean

        Try
            FirstRun = oUI.ReadRegistry("FirstRun", 1)
        Catch ex As Exception
            FirstRun = True
        End Try

        If FirstRun = True Then
            oUI.SaveRegistry("FirstRun", 0)

            Dim oConfig As New frmOptions

            oConfig.ShowDialog()
        End If

        Me.Show()

        Application.DoEvents()

        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData

        SQL = "SELECT * FROM RemoteSysAttr WHERE AutoConnect =1"

        oRs = clsMarsData.GetData(SQL)

        oUI.BusyProgress(90, "Loading...")

        If oRs.EOF = False Then
            Dim oRemote As New frmRemoteAdmin
            Dim sPath As String

            gPCName = oRemote.ConnectRemote(oRs("livepath").Value)

            If gPCName.Length > 0 Then
                'close all windows
                For Each oForm As frmWindow In oWindow
                    oForm.Close()
                    oForm.Dispose()
                Next

                sPath = oRs("livepath").Value

                nWindowCount = 0

                'close data connect
                oData.CloseMainDataConnection()

                sCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sPath & "\sqlrdlive.dat;Persist Security Info=False"

                oData.OpenMainDataConnection()

                oData.UpgradeCRDDb()

                sAppPath = sPath & "\"

                Try
                    oWindow = Nothing

                    'nWindowCount += 1
                    ReDim oWindow(nWindowCount)
                    oWindow(nWindowCount) = New frmWindow
                    oWindow(nWindowCount).MdiParent = oMain
                    oWindow(nWindowCount).Show()
                    oWindow(nWindowCount).Tag = nWindowCount
                    oWindow(nWindowCount).Text = "Desktop"
                    nWindowCurrent = nWindowCount


                    oMain.Text = "SQL-RD " & gsEdition & " Edition - on Machine:\\" & gPCName
                Catch ex As Exception
                    ''console.writeLine(ex.ToString)
                End Try

                Try
                    Dim oBtnConnect As ButtonItem
                    Dim oBtnDisconnect As ButtonItem

                    oBtnConnect = oMain.DotNetBarManager1.GetItem("item_169")
                    oBtnDisconnect = oMain.DotNetBarManager1.GetItem("item_201")

                    oBtnConnect.Enabled = False
                    oBtnDisconnect.Enabled = True

                Catch : End Try
            End If

        End If

        Try
            oBtnView(0) = DotNetBarManager1.GetItem("item_152")

            If MailType = MarsGlobal.gMailType.SMTP Then
                oBtnView(0).Enabled = True
            Else
                oBtnView(0).Enabled = False
            End If

            '  oUI.SetAppTheme()

            Dim oBtn As ButtonItem

            oBtnView(0) = DotNetBarManager1.GetItem("item_290", True)
            oBtn = DotNetBarManager1.GetItem("item_109", True)

            Dim dockItem As DockContainerItem = DotNetBarManager1.GetItem("dDock", True)
            Dim dock As Bar = DotNetBarManager1.GetItemBar(dockItem)

            If oUI.ReadRegistry("ShowDock", "1") = "1" Then
                oBtnView(0).Checked = True
                dock.Visible = True
            Else
                oBtnView(0).Checked = False
                dock.Visible = False
            End If
        Catch : End Try

        If hideKhov Then
            Try
                Dim oBtn As ButtonItem
                oBtn = DotNetBarManager1.GetItem("btnCollaboration", True)
                If oBtn IsNot Nothing Then
                    oBtn.Visible = False
                End If
            Catch : End Try
        End If

        'userInfo = New frmUserInfo
        'MoveBubbleBar()

        Me.ProcessTaskPane()

        Dim size As String = oUI.ReadRegistry("frmWindow", "824,638")

        Me.m_formSize = New Hashtable

        Me.m_formSize.Add("Width", size.Split(",")(0))
        Me.m_formSize.Add("Height", size.Split(",")(1))

        Dim DontshowWelcome As Boolean = Convert.ToBoolean(Convert.ToInt16(oUI.ReadRegistry("DontShowWelcomeScreen", 0)))

        If DontshowWelcome = False Then
            Dim oStarter As frmStarter = New frmStarter

            oStarter.ShowDialog(Me)
        End If

        clsSystemTools.BackConfigFile(clsSystemTools.configSaveTime.onOpen)

        Me.dtOOP = New DataTable("Processes")

        With Me.dtOOP.Columns
            .Add("Name")
            .Add("Type")
            .Add("PID")
            .Add("ScheduleID")
            .Add("Status")
            .Add("StatusValue")
            .Add("Argument")
            .Add("EntryDate")
        End With

        oUI.BusyProgress(100, "Loading...", True)
    End Sub

    Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        On Error Resume Next

        Application.Exit()
    End Sub

    Private Sub mnuCascade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.LayoutMdi(MdiLayout.Cascade)

        oUI.SaveRegistry("frmWindow", oWindow(nWindowCurrent).Width & "," & oWindow(nWindowCurrent).Height)
    End Sub

    Private Sub mnuTileHorizontal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.LayoutMdi(MdiLayout.TileHorizontal)

        oUI.SaveRegistry("frmWindow", oWindow(nWindowCurrent).Width & "," & oWindow(nWindowCurrent).Height)
    End Sub

    Private Sub mnuTileVertical_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.LayoutMdi(MdiLayout.TileVertical)

        oUI.SaveRegistry("frmWindow", oWindow(nWindowCurrent).Width & "," & oWindow(nWindowCurrent).Height)
    End Sub

    Private Sub mnuArrange_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.LayoutMdi(MdiLayout.ArrangeIcons)
    End Sub


   

    Private Sub mnuShowDesktop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        nWindowCount += 1
        ReDim Preserve oWindow(nWindowCount)
        oWindow(nWindowCount) = New frmWindow
        oWindow(nWindowCount).MdiParent = Me
        oWindow(nWindowCount).Show()
        oWindow(nWindowCount).Tag = nWindowCount
        oWindow(nWindowCount).Text = "Desktop"
        nWindowCurrent = nWindowCount
    End Sub

    Private Sub mnuSingleWizard_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim oSingle As frmSingleScheduleWizard = New frmSingleScheduleWizard

        oSingle.ShowDialog(Me)
    End Sub

    Private Sub mnuPackWizard_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim oPack As frmPackWizard = New frmPackWizard

        oPack.ShowDialog(Me)
    End Sub

    Private Sub mnuOptions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If clsMarsSecurity._HasGroupAccess("Options") = True Then
            Dim oConfig As frmOptions = New frmOptions

            oConfig.ShowDialog(Me)
        End If
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim sLoc As String = ""

        With ofd
            .Description = "Please select the folder to backup to"
            .ShowNewFolderButton = True
            .ShowDialog()
        End With

        sLoc = ofd.SelectedPath

        If sLoc = "" Then Exit Sub

        If sLoc.Substring(sLoc.Length - 1, 1) <> "\" Then
            sLoc += "\"
        End If

        System.IO.File.Copy(sAppPath & "marslive.mdb", sLoc & "marslive.mdb")
    End Sub


    Private Sub mnuCompactSys_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim oDB As DAO.DBEngine = New DAO.DBEngine

        If System.IO.File.Exists(sAppPath & "marslive.bak") Then
            System.IO.File.Delete(sAppPath & "marslive.bak")
        End If

        'first create a copy of the current database
        System.IO.File.Copy(sAppPath & "marslive.mdb", sAppPath & "marslive.bak")

        'create a copy of marslive.mdb for compacting
        System.IO.File.Copy(sAppPath & "marslive.mdb", sAppPath & "temp.tmp")

        'compact the file to temp1.mdb
        oDB.CompactDatabase(sAppPath & "temp.tmp", sAppPath & "temp1.tmp")

        'delete the older file
        System.IO.File.Delete(sAppPath & "temp.tmp")

        'delete the old marslive
        System.IO.File.Delete(sAppPath & "marslive.mdb")

        'create a new marslive.mdb
        System.IO.File.Move(sAppPath & "temp1.tmp", sAppPath & "marslive.mdb")

    End Sub

   
    Private Sub mnuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim oSplash As frmSplash = New frmSplash

        oSplash.ShowDialog()
    End Sub

    Private Sub mnuSysMon_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If clsMarsSecurity._HasGroupAccess("System Monitor") = True Then
            Dim oSys As frmSystemMonitor = New frmSystemMonitor

            oSys.Show()
        End If
    End Sub

    Private Sub DotNetBarManager1_ItemAdded(ByVal sender As Object, ByVal e As System.EventArgs) Handles DotNetBarManager1.ItemAdded

    End Sub

    Private Sub DotNetBarManager1_ItemClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DotNetBarManager1.ItemClick
        Dim oItem As BaseItem
        Dim oBtnView(3) As ButtonItem

        Try
            oBtnView(0) = DotNetBarManager1.GetItem("item_568")
            oBtnView(1) = DotNetBarManager1.GetItem("item_575")
            oBtnView(2) = DotNetBarManager1.GetItem("item_582")
            oBtnView(3) = DotNetBarManager1.GetItem("ButtonItem2")

            oItem = CType(sender, BaseItem)

            Select Case oItem.Text.ToLower.Replace("&", "")
                Case "collaboration"
                   
                Case "validate features"
                 
                Case "feature upgrade wizard"
                    Dim wizard As frmFunctionUpgrade = New frmFunctionUpgrade

                    wizard.ShowDialog()
                Case "operational hours"
                    If clsMarsSecurity._HasGroupAccess("Operational Hours") Then
                        Dim frmOp As frmOperationsExplorer = New frmOperationsExplorer

                        frmOp.MdiParent = oMain
                        frmOp.ShowInTaskbar = False

                        frmOp.Show()
                    End If
                Case "getting started"
                    Dim oStarter As New frmStarter

                    oStarter.Show()
                Case "task pane"
                    Try
                        Dim oBtn As ButtonItem
                        Dim oVMenu As ButtonItem
                        oBtn = DotNetBarManager1.GetItem("item_109", True)

                        oVMenu = CType(sender, ButtonItem)

                        If Not oBtn Is Nothing Then
                            Dim oTaskPane As Bar

                            oTaskPane = DotNetBarManager1.GetItemBar(oBtn)

                            If oTaskPane.Visible = True Then
                                oTaskPane.Visible = False
                                oVMenu.Checked = False

                                oUI.SaveRegistry("ShowTaskPane", 0)
                            Else
                                oTaskPane.Visible = True
                                oVMenu.Checked = True
                                oUI.SaveRegistry("ShowTaskPane", 1)
                            End If
                        End If
                    Catch : End Try
                Case "task dock"
                    Try

                        Dim oVMenu As ButtonItem
                        Dim oForm As frmWindow = oWindow(nWindowCurrent)

                        oVMenu = CType(sender, ButtonItem)

                        Dim dockItem As DockContainerItem = DotNetBarManager1.GetItem("dDock", True)
                        Dim dock As Bar = DotNetBarManager1.GetItemBar(dockItem)

                        If dock.Visible = True Then
                            dock.Visible = False
                            oVMenu.Checked = False
                            oUI.SaveRegistry("ShowDock", 0)
                        Else
                            dock.Visible = True
                            oVMenu.Checked = True
                            oUI.SaveRegistry("ShowDock", 1)
                        End If

                    Catch : End Try
                Case "update odbc login info"
                    If clsMarsSecurity._HasGroupAccess("System Migration") = True Then
                        Dim oLogin As New frmODBCLogin

                        oLogin.EditLogins()
                    End If
                Case "to file system"
                    oUI.SaveRegistry("ConType", "DAT")

                    MessageBox.Show("SQL-RD will now exit so that you can log into the new system", Application.ProductName, _
                       MessageBoxButtons.OK, MessageBoxIcon.Information)

                    Process.Start(sAppPath & assemblyName)

                    Me.Close()
                Case "to local sql server"
                    Dim editLogin As frmLocalSQLCon = New frmLocalSQLCon

                    If editLogin.editSQLInfo = True Then
                        oUI.SaveRegistry("ConType", "LOCAL", , , True)

                        MessageBox.Show("SQL-RD will now exit so that you can log into the new system", Application.ProductName, _
                        MessageBoxButtons.OK, MessageBoxIcon.Information)

                        Process.Start(sAppPath & assemblyName)

                        Me.Close()
                    End If
                Case "to odbc"
                    Dim s As String

                    s = oUI.ReadRegistry("ConString", "", True)

                    If s = "" Then
                        If clsMarsSecurity._HasGroupAccess("System Migration") = True Then
                            Dim oLogin As New frmODBCLogin

                            If oLogin.EditLogins() = True Then

                                Dim dataCopy As frmContentSelector = New frmContentSelector

                                dataCopy.ShowDialog()

                                oUI.SaveRegistry("ConType", "ODBC")

                            End If
                        End If
                    Else

                        If clsMarsSecurity._HasGroupAccess("System Migration") = False Then Return

                        Dim Con As String
                        Dim DSN As String
                        Dim userID As String
                        Dim password As String

                        Con = oUI.ReadRegistry("ConString", "", True)

                        If Con.Length = 0 Then _
                        Con = "Provider=MSDASQL.1;Password=;Persist Security Info=True;User ID=;Data Source=SQL-RD"

                        Dim conItem As ADODB.Connection = New ADODB.Connection
                        Dim recordItem As ADODB.Recordset = New ADODB.Recordset

                        Try
                            Try
                                conItem.Open(Con)
                            Catch ex As Exception
                                'try the alternative con string
                                Dim tempCon As String = clsMarsUI.MainUI.ReadRegistry("ConString2", "")

                                If tempCon = "" Then Throw ex

                                DSN = tempCon.Split("|")(0)
                                userID = tempCon.Split("|")(1)
                                password = _DecryptDBValue(tempCon.Split("|")(2))

                                conItem = New ADODB.Connection
                                conItem.Mode = ADODB.ConnectModeEnum.adModeShareDenyNone
                                conItem.Open(DSN, userID, password)
                            End Try

                            recordItem.Open("SELECT TOP 1 * FROM scheduleattr", conItem, ADODB.CursorTypeEnum.adOpenStatic)

                            Dim contentSelector As frmContentSelector = New frmContentSelector

                            contentSelector.selectContent(conItem)
                        Catch ex As Exception
                            Dim suggest As String

                            suggest = "If you are certain that a SQL-RD database does exist on the given server then check the following on the server:" & vbCrLf & _
                            "- The user provided has full permissions at table level to the SQL-RD database tables" & vbCrLf & _
                            "- Log into this PC as the database owner, log back into SQL-RD and perform the switch again" & vbCrLf & _
                            "- If your database tables are not owned by 'dbo' then ask your administrator to run a script to change the ownerships to 'dbo'." & _
                            "This is required in order to provide the permissions you will require to switch to this database and to perform the functions you require."

                            _ErrorHandle("Could not validate SQL-RD tables on the selected datasource. Please check that the database is a valid SQL-RD database.", -214690210, _
                            "_ValidateData", 0, suggest)
                            Return
                        Finally
                            Try : conItem.Close() : Catch : End Try

                            conItem = Nothing
                            recordItem = Nothing
                        End Try


                        oUI.SaveRegistry("ConType", "ODBC")

                    End If

                    MessageBox.Show("SQL-RD will now exit so that you can log into the new system", Application.ProductName, _
                       MessageBoxButtons.OK, MessageBoxIcon.Information)

                    Process.Start(sAppPath & assemblyName)

                    Me.Close()
                Case "migrate to odbc"
                    Dim s As String

                    s = "" '//oUI.ReadRegistry("ConString", "", True)

                    If s.Length = 0 Then
                        If clsMarsSecurity._HasGroupAccess("System Migration") = True Then
                          

                            'If clsMarsSecurity._HasGroupAccess("System Migration") = True Then
                            '    Dim oLogin As New frmODBCLogin

                            '    If oLogin.EditLogins() = True Then

                            '        oUI.SaveRegistry("ConType", "ODBC")

                            '    End If
                            'End If

                            Dim sMsg As String

                            sMsg = "To Migrate to SQL Server (or some other ODBC compliant database):" & vbCrLf & vbCrLf & _
                            "1.  Create a new empty database instance in your ODBC Compliant database." & vbCrLf & vbCrLf & _
                            "2.  On the SQL-RD PC, create a SYSTEM ODBC DSN. Windows integrated authentication is " & _
                            "preferred.  Ensure you select the new database as the 'default database'." & vbCrLf & vbCrLf & _
                            "3.  In SQL-RD, go to Tools - System Tools - Migrate to SQL Server/ODBC" & vbCrLf & vbCrLf & _
                            "4.  Select your new DSN from the dropdown list and enter your credentials (if you " & _
                            "are not using Windows Authentication)" & vbCrLf & vbCrLf & _
                            "5.  Click 'Go'." & vbCrLf & vbCrLf & _
                            "Your local file system will be migrated to the new database, and SQL-RD will  use this " & _
                            "new database as its default database." & vbCrLf & vbCrLf & _
                            "Important:" & vbCrLf & vbCrLf & _
                            "ChristianSteven Software does not provide support or instructions on how to use SQL Server " & _
                            "or other databases.  If you do not have any experience with databases, please contact " & _
                            "your database administrator and arrange for the migration to be performed by an expert."

                            Dim oMsg As New frmCustomMsg

                            oMsg.ShowMsg(sMsg, "Migrate to SQL Server/ODBC", True)

                            MessageBox.Show("SQL-RD needs to perform a system backup before proceeding", Application.ProductName, _
                            MessageBoxButtons.OK, MessageBoxIcon.Information)

                            Dim oTools As New clsSystemTools

                            oTools._BackupSystem()

                            Dim oSQL As New frmSQLAdmin

                            oSQL.Text = "Migrate to ODBC"

                            oSQL.ShowDialog()
                        End If
                    End If

                    MessageBox.Show("SQL-RD will now exit so that you can log into the new system", Application.ProductName, _
                       MessageBoxButtons.OK, MessageBoxIcon.Information)

                    Process.Start(sAppPath & assemblyName)

                    Me.Close()
                Case "activate sql-rd"
                    Dim oSys As New clsSystemTools

                    oSys._ActivateSystem(True, True)
                Case "deactivate sql-rd"
                    Dim oSys As New clsSystemTools

                    oSys._DeactivateSystem(True)
                Case "check for updates"
                    'Process.Start(sAppPath & "updatecheck.exe")
                    checkforUpdate(True)
                Case "create support files"
                    Dim oSupport As frmSupportFiles

                    oSupport = New frmSupportFiles

                    oSupport._CreateSupportFiles()
                Case "migrate to local sql server"

                    If clsMarsSecurity._HasGroupAccess("System Migration") = True Then
                        MessageBox.Show("SQL-RD needs to perform a system backup before proceeding", Application.ProductName, _
                        MessageBoxButtons.OK, MessageBoxIcon.Information)

                        Dim oTools As New clsSystemTools

                        oTools._BackupSystem()

                        Dim SQLExists As Boolean = False

                        Dim svcs As ServiceController() = ServiceController.GetServices

                        For Each svc As ServiceController In svcs
                            If svc.DisplayName = clsMigration.m_DatabaseService Then
                                SQLExists = True
                            End If
                        Next


                        If SQLExists = False Then
                            Dim sMsg As String

                            sMsg = "You do not have a local installation of SQL Server on this machine." & _
                            "Please follow the instructions provided in the document located at:" & vbCrLf & _
                            "http://www.christiansteven.com/_support/sqlinstall.pdf " & vbCrLf & _
                            "and then try migrating again."

                            Dim oMsg As New frmCustomMsg

                            oMsg.ShowMsg(sMsg, "Migrate to local SQL Server", True)

                        Else
                            Try
                                Dim svc As ServiceController = New ServiceController(clsMigration.m_DatabaseService)

                                If svc.Status <> ServiceControllerStatus.Running Then
                                    svc.Start()
                                End If
                            Catch ex As Exception
                                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 1363)
                            End Try

                            Dim sMsg As String

                            sMsg = "WARNING: If a database called 'SQL-RD' currently exists in your local SQL Server, then it will be dropped and recreated. Proceed?"

                            If MessageBox.Show(sMsg, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.No Then
                                Return
                            End If

                            Dim oSQL As New frmSQLAdmin


                            Dim sConn As String = clsMigration.m_localConString
                            Dim dataLocation As String = sAppPath & "DATA\"

                            If IO.Directory.Exists(dataLocation) = False Then IO.Directory.CreateDirectory(dataLocation)

                            If oSQL._CreateDatabase(Environment.MachineName & "\SQLRD", dataLocation) = True Then

                                If clsMigration.MigrateSystem(sConn, "", "") = True Then

                                    oUI.SaveRegistry("ConType", "LOCAL", , , True)
                                    oUI.SaveRegistry("ConString", sConn, True, , True)

                                    MessageBox.Show("SQL-RD will now exit so that you may log into the new system", _
                                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

                                    Process.Start(sAppPath & assemblyName)

                                    Me.Close()
                                End If

                            End If

                        End If
                    End If
                Case "migrate to file system"
                    If clsMarsSecurity._HasGroupAccess("System Migration") = True Then

                        Dim res As Windows.Forms.DialogResult = MessageBox.Show("Migrate to file system?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

                        If res = Windows.Forms.DialogResult.Yes Then

                            clsServiceController.itemGlobal.StopScheduler()

                            If IO.File.Exists(sAppPath & "sqlrdlive.dat") = False Then
                                IO.File.Copy(sAppPath & "sqlrd.dat", sAppPath & "sqlrdlive.dat")
                            End If

                            If clsMigration.MigrateSystem(clsMigration.m_datConString, "", "") = True Then

                                oUI.SaveRegistry("ConType", "DAT", , , True)

                                MessageBox.Show("Migration to file system completed. SQL-RD will now exit to apply the changes", _
                                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.None)

                                Process.Start(Application.ExecutablePath)

                                clsServiceController.itemGlobal.StartScheduler()
                                Me.Close()
                            End If
                        End If
                    End If
                Case "migrate to sql server/odbc"
                    If clsMarsSecurity._HasGroupAccess("System Migration") = True Then
                       

                        Dim sMsg As String

                        sMsg = "To Migrate to SQL Server (or some other ODBC compliant database):" & vbCrLf & vbCrLf & _
                        "1.  Create a new empty database instance in your ODBC Compliant database." & vbCrLf & vbCrLf & _
                        "2.  On the SQL-RD PC, create a SYSTEM ODBC DSN. Windows integrated authentication is " & _
                        "preferred.  Ensure you select the new database as the 'default database'." & vbCrLf & vbCrLf & _
                        "3.  In SQL-RD, go to Tools - System Tools - Migrate to SQL Server/ODBC" & vbCrLf & vbCrLf & _
                        "4.  Select your new DSN from the dropdown list and enter your credentials (if you " & _
                        "are not using Windows Authentication)" & vbCrLf & vbCrLf & _
                        "5.  Click 'Go'." & vbCrLf & vbCrLf & _
                        "Your local file system will be migrated to the new database, and SQL-RD will  use this " & _
                        "new database as its default database." & vbCrLf & vbCrLf & _
                        "Important:" & vbCrLf & vbCrLf & _
                        "ChristianSteven Software does not provide support or instructions on how to use SQL Server " & _
                        "or other databases.  If you do not have any experience with databases, please contact " & _
                        "your database administrator and arrange for the migration to be performed by an expert."

                        Dim oMsg As New frmCustomMsg

                        oMsg.ShowMsg(sMsg, "Migrate to SQL Server/ODBC", True, "http://www.christiansteven.com/using_odbc.pdf")

                        MessageBox.Show("SQL-RD needs to perform a system backup before proceeding", Application.ProductName, _
                        MessageBoxButtons.OK, MessageBoxIcon.Information)

                        Dim oTools As New clsSystemTools

                        oTools._BackupSystem()

                        Dim oSQL As New frmSQLAdmin

                        oSQL.ShowDialog()
                    End If
                Case "smtp servers manager"
                    

                    If clsMarsSecurity._HasGroupAccess("SMTP Servers Manager") Then
                        Dim oSMTP As New frmSMTPServers

                        oSMTP.IsDialog = False

                        oSMTP.MdiParent = Me
                        oSMTP.Show()
                    End If
                Case "choose details"
                    Dim oView As New frmViewOptons

                    oView.ViewOptions()
                Case "themes"
                    Dim oView As New frmViewOptons

                    oView.ViewOptions(True)
                Case "register for support"
                    clsWebProcess.Start("http://www.christiansteven.com/sql-rd/purchase/register.htm") '.
                Case "folder"
                   

                Case "smart folder"
                    If clsMarsSecurity._HasGroupAccess("Smart Folder Mngmnt") = True Then
                        Dim oForm As New frmSmartFolders

                        oForm.ShowDialog()
                    End If
                Case "show desktop"
                    nWindowCount += 1
                    ReDim Preserve oWindow(nWindowCount)
                    oWindow(nWindowCount) = New frmWindow
                    oWindow(nWindowCount).MdiParent = Me
                    oWindow(nWindowCount).Show()
                    oWindow(nWindowCount).Tag = nWindowCount
                    oWindow(nWindowCount).Text = "Desktop"
                    nWindowCurrent = nWindowCount
                Case "exit"
                    Try




                        Me.Close()
                    Catch ex As Exception
                        Me.Close()
                    End Try
                Case "logout"
                    If MessageBox.Show("Logout of SQL-RD?", Application.ProductName, _
                     MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                        Dim oProc As New Process

                        oProc.StartInfo.FileName = sAppPath & assemblyName

                        oProc.Start()

                        Try


                            Me.Close()
                        Catch ex As Exception

                            Me.Close() 'Application.Exit()
                        End Try
                    End If
                Case "single report schedule"
                    If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
                        Dim oSingle As frmSingleScheduleWizard = New frmSingleScheduleWizard

                        oSingle.ShowDialog(Me)

                        oUI.SaveRegistry("LastScheduleType", "Single")
                    End If
                Case "dynamic schedule"
                    If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
                        Dim oDynamic As New frmDynamicSchedule

                        oDynamic.ShowDialog(Me)

                        oUI.SaveRegistry("LastScheduleType", "Dynamic")
                    End If
                Case "packaged reports schedule"
                    If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
                        Dim oPack As frmPackWizard = New frmPackWizard

                        oPack.ShowDialog(Me)

                        oUI.SaveRegistry("LastScheduleType", "Package")
                    End If
                Case "automation schedule"
                    If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
                        Dim oAuto As New frmAutoScheduleWizard

                        oAuto.ShowDialog(Me)

                        oUI.SaveRegistry("LastScheduleType", "Automation")
                    End If
                Case "event-based schedule"
                    If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
                        Dim oEvent As New frmEventWizard6

                        oEvent.ShowDialog(Me)

                        oUI.SaveRegistry("LastScheduleType", "Event-Based")
                    End If

                Case "event-based package"
                    If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
                        Dim oPackage As New frmEventPackage

                        oPackage.ShowDialog(Me)

                        oUI.SaveRegistry("LastScheduleType", "Event-Based Package")
                    End If
                Case "data-driven schedule"
                    If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
                      

                        Dim oSchedule As frmRDScheduleWizard = New frmRDScheduleWizard

                        oSchedule.ShowDialog(Me)

                        oUI.SaveRegistry("LastScheduleType", "Data-Driven Schedule")

                    End If
                Case "data-driven package"
                    If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
                  

                        Dim oSchedule As frmDataDrivenPackage = New frmDataDrivenPackage

                        oSchedule.ShowDialog(Me)

                        oUI.SaveRegistry("LastScheduleType", "Data-Driven Package")

                    End If
                Case "options"
                    If clsMarsSecurity._HasGroupAccess("Options") = True Then
                        Dim oConfig As frmOptions = New frmOptions

                        oConfig.ShowDialog(Me)
                    End If
                Case "system monitor"
                    If clsMarsSecurity._HasGroupAccess("System Monitor") = True Then
                        If gSysmonInstance Is Nothing Then
                            Dim oSys As frmSystemMonitor = New frmSystemMonitor

                            gSysmonInstance = oSys

                            oSys.Show()
                        Else
                            gSysmonInstance.Focus()
                        End If
                    End If
                Case "properties"
                    Dim oForm As frmWindow
                    Dim olsv As ListView
                    Dim sType As String
                    Dim nID As Integer



                    oForm = oWindow(nWindowCurrent)

                    If olsv.SelectedItems.Count = 0 Then Return

                    sType = Convert.ToString(olsv.SelectedItems(0).Tag).Split(":")(0)

                    nID = Convert.ToString(olsv.SelectedItems(0).Tag).Split(":")(1)

                    Select Case sType.ToLower
                        Case "package"
                            If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules", nID, clsMarsScheduler.enScheduleType.PACKAGE) = False Then
                                Return
                            End If

                            Dim oPack As New frmPackageProp

                            oPack.EditPackage(nID)
                        Case "report"
                            If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules", nID, clsMarsScheduler.enScheduleType.REPORT) = False Then
                                Return
                            End If

                            Dim oReport As New frmSingleProp

                            oReport.EditSchedule(nID)
                        Case "automation"
                            If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules", nID, clsMarsScheduler.enScheduleType.AUTOMATION) = False Then
                                Return
                            End If

                            Dim oAuto As New frmAutoProp

                            oAuto.EditSchedule(nID)
                        Case "event"
                            If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules", nID, clsMarsScheduler.enScheduleType.EVENTBASED) = False Then
                                Return
                            End If
                            Dim oEvent As New frmEventProp

                            oEvent.EditSchedule(nID)
                        Case "packed report"
                            If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules", nID, clsMarsScheduler.enScheduleType.REPORT) = False Then
                                Return
                            End If

                            Dim oReport As New frmPackedReport

                            oReport.EditReport(nID)
                    End Select
                Case "address book"

                    Dim oAddress As New frmAddressBook

                    oAddress.MdiParent = Me

                    oAddress.Show()
                Case "custom calendar"
                    If clsMarsSecurity._HasGroupAccess("Custom Calendar") = True Then
                        Dim oCalendar As New frmCalendarExplorer
                        oCalendar.MdiParent = Me

                        oCalendar.Show()
                    End If

                Case "backup system"
                    Dim oSys As New clsSystemTools

                    oSys._BackupSystem()
                Case "restore system"
                    Dim oSys As New clsSystemTools

                    oSys._RestoreSystem()
                Case "compact system"
                    Dim oSys As New clsSystemTools

                    oSys._CompactSystem()
               
                Case "cascade"
                    Me.LayoutMdi(MdiLayout.Cascade)
                    Me.m_formSize.Item("Width") = oWindow(nWindowCurrent).Width
                    Me.m_formSize.Item("Height") = oWindow(nWindowCurrent).Height

                Case "tile horizontal"
                    Me.LayoutMdi(MdiLayout.TileHorizontal)
                    Me.m_formSize.Item("Width") = oWindow(nWindowCurrent).Width
                    Me.m_formSize.Item("Height") = oWindow(nWindowCurrent).Height

                Case "tile vertical"
                    Me.LayoutMdi(MdiLayout.TileVertical)
                    Me.m_formSize.Item("Width") = oWindow(nWindowCurrent).Width
                    Me.m_formSize.Item("Height") = oWindow(nWindowCurrent).Height

                Case "arrange icons"
                    Me.LayoutMdi(MdiLayout.ArrangeIcons)
                Case "about sql-rd"
                    Dim oSplash As New frmAbout

                    oSplash.ShowDialog(Me)
                Case "report design"

                Case "sql-rd help", "sql-rd &help"
                    Try
                        Process.Start(sAppPath & "sql-rd.chm")
                    Catch : End Try
                Case "sql-rd"
                    Try
                        clsWebProcess.Start("http://www.christiansteven.com/main_sql-rd.htm") '.
                    Catch : End Try
                Case "technical support"
                    Try
                        RequestHelp("http://www.christiansteven.com/sup_req_sql-rd.htm") '.
                    Catch : End Try
                Case "search knowledge base"
                    Try
                        clsWebProcess.Start("http://www.christiansteven.com/support_sql-rd.htm")
                    Catch : End Try
                Case "browse user forum"
                    Try
                        clsWebProcess.Start("http://www.christiansteven.com/userforum_SQL-RD.htm")
                    Catch : End Try
                Case "how to...demos"
                    Try
                        clsWebProcess.Start("http://www.christiansteven.com/sql-rd/demos/sql-rd_demos.htm") '.
                    Catch : End Try
                Case "cancel"
                    Me.m_UserCancel = True
                    gsID(0) = ""
                    gsID(1) = ""

                    'Dim oReport As New clsMarsReport

                    'If oWindow(nWindowCount).mainWorker.IsBusy = True Then
                    '    Try
                    '        oWindow(nWindowCount).mainWorker.CancelAsync()

                    '        If Not gsID Is Nothing Then
                    '            Dim oSchedule As New clsMarsScheduler

                    '            Select Case gsID(0).ToLower
                    '                Case "report"
                    '                    oSchedule.SetScheduleHistory(False, "User Cancelled", gsID(1))
                    '                Case "automation"
                    '                    oSchedule.SetScheduleHistory(False, "User Cancelled", , , gsID(1))
                    '                Case "package"
                    '                    oSchedule.SetScheduleHistory(False, "User Cancelled", , gsID(1))
                    '                Case "eventbased"
                    '                    Dim oEvent As New clsMarsEvent
                    '                    oEvent.SaveEventHistory(gsID(1), False, "User Cancelled")
                    '            End Select

                    '            _ErrorHandle("User Cancelled", -1, "none", 652)
                    '        End If

                    '        gsID(0) = ""
                    '        gsID(1) = ""
                    '    Catch ex As Exception
                    '        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
                    '    Finally
                    '        oUI.BusyProgress(100, , True)
                    '    End Try
                    'End If

                Case "user manager"

                    If clsMarsSecurity._HasGroupAccess("User Manager") = True Then
                        Dim oForm As New frmUserManager

                        oForm.MdiParent = Me
                        oForm.Show()
                    End If
                Case "license agreement"
                    Try
                        Dim oProc As New Process

                        oProc.Start(sAppPath & "END USER LICENSE AGREEMENT.rtf")
                    Catch : End Try
                Case "connect"
                    If clsMarsSecurity._HasGroupAccess("Remote Administration") = True Then
                        Dim oRemote As New frmRemoteAdmin
                        oRemote.MdiParent = Me
                        oRemote.Show()
                    End If
                Case "disconnect"

                    Dim oRemote As New frmRemoteAdmin

                    oRemote.Disconnect()
                Case "export schedules"
                    If clsMarsSecurity._HasGroupAccess("Export Schedules") Then
                        Dim oExport As New frmExportWizard

                        oExport.ShowDialog(Me)
                    End If
                Case "dynamic package schedule"
                    Dim oDynamic As New frmDynamicPackage

                    oDynamic.ShowDialog()

                    oUI.SaveRegistry("LastScheduleType", "Dynamic-Package")
                Case "general"
                    
                Case "advanced"
                    
                Case "status bar"
                    
                    'dumps
                Case "single schedule data"
                    Dim SQL As String = "SELECT reportattr.*, destinationattr.*, scheduleattr.* " & _
                    "FROM (destinationattr INNER JOIN reportattr ON destinationattr.reportid = reportattr.reportid) " & _
                    "INNER JOIN scheduleattr ON reportattr.reportid = scheduleattr.reportid"

                    Dim oDump As frmHistoryReport = New frmHistoryReport

                    oDump.systemDumpReport(SQL, "Single Schedules")

                Case "package schedule data"
                    Dim SQL As String = "SELECT packageattr.*, scheduleattr.*, reportattr.*, destinationattr.* " & _
                    "FROM ((packageattr INNER JOIN scheduleattr ON packageattr.packid = scheduleattr.packid) INNER JOIN " & _
                    "destinationattr ON packageattr.packid = destinationattr.packid) LEFT JOIN " & _
                    "reportattr ON packageattr.packid = reportattr.packid"

                    Dim oDump As frmHistoryReport = New frmHistoryReport

                    oDump.systemDumpReport(SQL, "Package Schedules")
                Case "event-based schedule data"
                    Dim SQL As String = "SELECT EventAttr6.*, EventConditions.* " & _
                    "FROM (EventAttr6 INNER JOIN EventConditions ON EventAttr6.EventID = EventConditions.EventID) " & _
                    "WHERE (PackID=0 OR PackID IS NULL)"

                    Dim oDump As frmHistoryReport = New frmHistoryReport

                    oDump.systemDumpReport(SQL, "Event-Based Schedules")
                Case "event-based package data"
                    Dim SQL As String = "SELECT EventPackageAttr.*, EventAttr6.*, EventConditions.*, ScheduleAttr.* " & _
                    "FROM (EventPackageAttr INNER JOIN ScheduleAttr ON EventPackageAttr.EventPackID = ScheduleAttr.EventPackID) " & _
                    "INNER JOIN (EventAttr6 INNER JOIN EventConditions ON EventAttr6.EventID = EventConditions.EventID) ON " & _
                    "EventPackageAttr.EventPackID = EventAttr6.PackID"

                    Dim oDump As frmHistoryReport = New frmHistoryReport

                    oDump.systemDumpReport(SQL, "Event-Based Package Schedules")

            End Select
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub
    Private Sub RequestHelp(ByVal sUrl As String)
        On Error Resume Next
        Dim Obj As Object
        Dim sCompany As String
        Dim sName As String
        Dim sCustID As String
        Dim I As Integer
        Dim Sel As Object
        Dim cur As String
        Dim X As Integer
        Dim ScheduleType As String
        Dim MailType As String
        Dim Y As Double
        Dim oFile As FileVersionInfo = FileVersionInfo.GetVersionInfo(sAppPath & assemblyName)

        sCompany = oUI.ReadRegistry("RegCo", "")
        sName = oUI.ReadRegistry("RegFirstName", "") & " " & oUI.ReadRegistry("RegLastName", "")
        sCustID = oUI.ReadRegistry("CustNo", "")
        ScheduleType = oUI.ReadRegistry("SQL-RDService", "")
        MailType = oUI.ReadRegistry("MailType", "")

        Dim wb As New SHDocVw.InternetExplorer

        wb.Visible = False

        wb.Navigate(sUrl)

        Do While wb.Busy = True
            If I < 100 Then
                I = I + 5
            Else
                I = 0
            End If

            oUI.BusyProgress(I, "Connecting to ChristianSteven Support Server...")

            Y = I / 3

            If Y = CInt(I / 3) Then _Delay(1)
        Loop

        oUI.BusyProgress(90, "Loading system configuration", False)

        _Delay(1)

        AppStatus(False)

        For Each Obj In wb.Document.All.tags("input")
            If Obj.name = "Customer_Number" Then
                Obj.Value = sCustID
            ElseIf Obj.name = "Company_Name" Then
                Obj.Value = sCompany
            ElseIf Obj.name = "Customer_Name" Then
                Obj.Value = sName
            ElseIf Obj.name = "Product_Version" Then
                Obj.Value = oFile.FileVersion
            ElseIf Obj.name = "Build_Number" Then
                ''console.writeLine(oFile.Comments.ToLower)

                Obj.Value = oFile.Comments.ToLower.Replace("build", String.Empty).Trim
            ElseIf Obj.name = "Operating_System" Then
                Obj.Value = Environment.OSVersion
            End If
        Next

        For Each Obj In wb.Document.All.tags("select")

            If Obj.name = "Schduler_Type" Then
                Select Case ScheduleType
                    Case "WindowsNT"
                        Obj.selectedIndex = 2
                    Case "WindowsApp"
                        Obj.selectedIndex = 1
                    Case "NONE"
                        Obj.selectedIndex = 0
                    Case Else
                        Obj.selectedIndex = 0
                End Select
            ElseIf Obj.name = "Email_Type" Then
                Select Case MailType.ToLower
                    Case "mapi"
                        If oUI.ReadRegistry("MAPIType", "").ToLower = "single" Then
                            Obj.selectedIndex = 1
                        Else
                            Obj.selectedIndex = 2
                        End If
                    Case "smtp"
                        Obj.selectedIndex = 0
                    Case "groupwise"
                        Obj.selectedIndex = 3
                    Case "none"
                        Obj.selectedIndex = 4
                    Case Else
                        Obj.selectedIndex = 4
                End Select
            End If
        Next

        wb.Visible = True

        Obj = Nothing

        oUI.BusyProgress(100, "", True)
    End Sub



    <DebuggerStepThrough()> Private Sub tmDate_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles tmDate.Elapsed
        Try
            Dim oDate As LabelItem = DotNetBarManager1.GetItem("item_925")

            oDate.Text = Now
        Catch : End Try
    End Sub

    Private Sub btnOptions_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnOptions.Click

        If clsMarsSecurity._HasGroupAccess("Options") = True Then
            Dim oConfig As frmOptions = New frmOptions

            oConfig.ShowDialog(Me)
        End If

    End Sub

    Private Sub btnSystemMonitor_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnSystemMonitor.Click

        If clsMarsSecurity._HasGroupAccess("System Monitor") = True Then
            Dim oSys As frmSystemMonitor = New frmSystemMonitor

            oSys.Show()
        End If
    End Sub

    Private Sub btnProperties_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnProperties.Click

        Dim oForm As frmWindow
        Dim olsv As ListView
        Dim sType As String
        Dim nID As Integer

        oForm = oWindow(nWindowCurrent)


        If olsv.SelectedItems.Count = 0 Then Return

        sType = Convert.ToString(olsv.SelectedItems(0).Tag).Split(":")(0)

        nID = Convert.ToString(olsv.SelectedItems(0).Tag).Split(":")(1)

        Select Case sType.ToLower
            Case "package"
                If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules", nID, clsMarsScheduler.enScheduleType.PACKAGE) = False Then
                    Return
                End If

                Dim oPack As New frmPackageProp

                oPack.EditPackage(nID)
            Case "report"
                If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules", nID, clsMarsScheduler.enScheduleType.REPORT) = False Then
                    Return
                End If

                Dim oReport As New frmSingleProp

                oReport.EditSchedule(nID)
            Case "automation"
                If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules", nID, clsMarsScheduler.enScheduleType.AUTOMATION) = False Then
                    Return
                End If

                Dim oAuto As New frmAutoProp

                oAuto.EditSchedule(nID)
            Case "event"
                If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules", nID, clsMarsScheduler.enScheduleType.EVENTBASED) = False Then
                    Return
                End If

                Dim oEvent As New frmEventProp

                oEvent.EditSchedule(nID)
            Case "packed report"
                If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules", nID, clsMarsScheduler.enScheduleType.REPORT) = False Then
                    Return
                End If

                Dim oReport As New frmPackedReport

                oReport.EditReport(nID)
        End Select
    End Sub

    Private Sub btnDesign_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnDesign.Click
        
    End Sub

    Private Sub btnCalendar_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnCalendar.Click
        If clsMarsSecurity._HasGroupAccess("Custom Calendar") = True Then
            Dim oCalendar As New frmCalendarExplorer
            oCalendar.MdiParent = Me

            oCalendar.Show()
        End If
    End Sub

    Private Sub btnAddressBook_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnAddressBook.Click



        Dim oAddress As New frmAddressBook

        oAddress.MdiParent = Me

        oAddress.Show()
    End Sub

    Private Sub btnSingle_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnSingle.Click

        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
            Dim oSingle As frmSingleScheduleWizard = New frmSingleScheduleWizard

            oSingle.ShowDialog(Me)
        End If
    End Sub

    Private Sub btnPackage_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnPackage.Click

        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
            Dim oPack As frmPackWizard = New frmPackWizard

            oPack.ShowDialog(Me)
        End If
    End Sub

    Private Sub btnDynamic_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnDynamic.Click

        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
            Dim oDynamic As New frmDynamicSchedule

            oDynamic.ShowDialog(Me)
        End If

    End Sub

    Private Sub btnDynamicPackage_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnDynamicPackage.Click
        Dim oDynamic As New frmDynamicPackage

        oDynamic.ShowDialog()
    End Sub

    Private Sub btnAutomation_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnAutomation.Click

        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
            Dim oAuto As New frmAutoScheduleWizard

            oAuto.ShowDialog(Me)
        End If

    End Sub

    Private Sub btnEvent_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnEvent.Click

        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
            Dim oEvent As New frmEventWizard6

            oEvent.ShowDialog(Me)
        End If

    End Sub



    Private Sub btnHelp_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnHelp.Click

        Try
            Process.Start(sAppPath & "sqlrd.chm")
        Catch : End Try

    End Sub

    Private Sub btnForums_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnForums.Click

        clsWebProcess.Start("http://www.christiansteven.com/support_sql-rd.htm")

    End Sub

    Private Sub btnKbase_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnKbase.Click

        clsWebProcess.Start("http://www.christiansteven.com/support_sql-rd.htm")

    End Sub

    Private Sub btnHowTo_Click(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ClickEventArgs) Handles btnHowTo.Click

        clsWebProcess.Start("http://www.christiansteven.com/sql-rd/demos/sql-rd_demos.htm")
    End Sub


    Private Sub frmMain_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        MoveBubbleBar()
        Me.LayoutMdi(MdiLayout.Cascade)
    End Sub
    Public Shared Function isExpressInstalled() As Boolean

        Const edition As String = "Express Edition"

        Const instance As String = "MSSQL$SQLEXPRESS"

        Const spLevel As Integer = 1

        Dim fCheckEdition As Boolean = False

        Dim fCheckSpLevel As Boolean = False

        Try

            Dim getSqlExpress As ManagementObjectSearcher = New ManagementObjectSearcher("root\Microsoft\SqlServer\ComputerManagement", "select * from SqlServiceAdvancedProperty where SQLServiceType = 1 and ServiceName = '" + instance + "' and (PropertyName = 'SKUNAME' or PropertyName = 'SPLEVEL')")

            If getSqlExpress.Get.Count = 0 Then

                Return False

            End If

            For Each sqlEngine As ManagementObject In getSqlExpress.Get

                If sqlEngine("ServiceName").ToString.Equals(instance) Then

                    Select Case sqlEngine("PropertyName").ToString

                        Case "SKUNAME"

                            fCheckEdition = sqlEngine("PropertyStrValue").ToString.Contains(edition)

                        Case "SPLEVEL"

                            fCheckSpLevel = Integer.Parse(sqlEngine("PropertyNumValue").ToString) >= spLevel

                    End Select

                End If

            Next

            If fCheckEdition And fCheckSpLevel Then

                Return True

            End If

            Return False

        Catch e As Exception

            Return False

        End Try

    End Function

    Private Sub tmCheckUpdate_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles tmCheckUpdate.Elapsed
        tmCheckUpdate.Stop()

        Me.checkforUpdate()
    End Sub

    Private Sub checkforUpdate(Optional ByVal Manual As Boolean = False)
        Dim oCheck As clsSystemTools = New clsSystemTools

        oCheck.checkforUpdates(gDownloadUrl, Manual)
    End Sub
End Class
