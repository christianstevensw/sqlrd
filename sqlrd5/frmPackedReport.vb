Imports DevComponents.DotNetBar
Imports Microsoft.Reporting.WinForms
Imports System.Collections.Generic

#If CRYSTAL_VER = 8 Then
imports My.Crystal85
#ElseIf CRYSTAL_VER = 9 Then
imports My.Crystal9 
#ElseIf CRYSTAL_VER = 10 Then
imports My.Crystal10 
#ElseIf CRYSTAL_VER = 11 Then
Imports My.Crystal11
#End If


Friend Class frmPackedReport

    Inherits DevComponents.DotNetBar.Office2007Form
    Dim UserCancel As Boolean
    Dim oErr As clsMarsUI = New clsMarsUI
    Dim oData As clsMarsData = New clsMarsData
    Public sDestination As String
    Dim ReportID As Integer = 99999
    Dim PackID As Integer = 99999
    Dim oField As TextBox
    Dim oRpt As Object 'ReportServer.ReportingService
    Dim ServerUser As String = ""
    Dim ServerPassword As String = ""
    Dim formsAuth As Boolean
    Dim ParDefaults As Hashtable
    Dim dtParameters As DataTable
    Dim parCollection As Hashtable
    Friend WithEvents txtAdjustStamp As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Dim ReportRestore() As Boolean
    Dim eventReport As Boolean = False
    Dim showTooltip As Boolean = True
    Dim m_serverParametersTable As DataTable
    Dim sRDLPath As String
    Dim previousReportLocation, previousReportServer As String

    Friend WithEvents txtUrl As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lsvDatasources As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents mnuDatasources As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ClearToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnRefreshDS As DevComponents.DotNetBar.ButtonX

    Friend WithEvents cmdLoginTest2 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents bgWorker As System.ComponentModel.BackgroundWorker
    Dim m_Mode As String = ""
    Public IsDataDriven As Boolean
    Friend WithEvents tbPack As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel5 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem5 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem4 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem3 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabReport As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents ucReportOptions As sqlrd.frmRptOptions
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents UcBlankReportX1 As sqlrd.ucBlankReportX
    Friend WithEvents SuperTabControlPanel6 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabDestination As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents UcDestPack As sqlrd.ucDestination
    Friend WithEvents parameterController As sqlrd.ucParameterController
    Public m_serverUrl As String = ""
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtReportName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDBLocation As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdDBLoc As DevComponents.DotNetBar.ButtonX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbDateTime As System.Windows.Forms.ComboBox
    Friend WithEvents chkCustomExt As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtCustomName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents optNaming As System.Windows.Forms.RadioButton
    Friend WithEvents optCustomName As System.Windows.Forms.RadioButton
    Friend WithEvents txtCustomExt As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents chkAppendDateTime As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ErrProv As System.Windows.Forms.ErrorProvider
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents chkEmbed As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtFormula As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents txtDefault As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdLoginTest As DevComponents.DotNetBar.ButtonX
    Friend WithEvents chkStatus As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cmdRequery As DevComponents.DotNetBar.ButtonX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPackedReport))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ucReportOptions = New sqlrd.frmRptOptions()
        Me.txtUrl = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.chkStatus = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkEmbed = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.cmdDBLoc = New DevComponents.DotNetBar.ButtonX()
        Me.txtReportName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtDBLocation = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdLoginTest2 = New DevComponents.DotNetBar.ButtonX()
        Me.cmdRequery = New DevComponents.DotNetBar.ButtonX()
        Me.btnRefreshDS = New DevComponents.DotNetBar.ButtonX()
        Me.lsvDatasources = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.mnuDatasources = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ClearToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmdLoginTest = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtAdjustStamp = New System.Windows.Forms.NumericUpDown()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.cmbDateTime = New System.Windows.Forms.ComboBox()
        Me.chkCustomExt = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtCustomName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.optNaming = New System.Windows.Forms.RadioButton()
        Me.optCustomName = New System.Windows.Forms.RadioButton()
        Me.txtCustomExt = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.chkAppendDateTime = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtDefault = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.ErrProv = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.txtFormula = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.ofd = New System.Windows.Forms.OpenFileDialog()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.bgWorker = New System.ComponentModel.BackgroundWorker()
        Me.tbPack = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.parameterController = New sqlrd.ucParameterController()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabReport = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.SuperTabItem3 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem4 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel6 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.UcDestPack = New sqlrd.ucDestination()
        Me.tabDestination = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel5 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.UcBlankReportX1 = New sqlrd.ucBlankReportX()
        Me.SuperTabItem5 = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GroupBox1.SuspendLayout()
        Me.mnuDatasources.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.txtAdjustStamp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrProv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbPack, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbPack.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.SuperTabControlPanel4.SuspendLayout()
        Me.SuperTabControlPanel6.SuspendLayout()
        Me.SuperTabControlPanel5.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.ucReportOptions)
        Me.GroupBox1.Controls.Add(Me.txtUrl)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.chkStatus)
        Me.GroupBox1.Controls.Add(Me.chkEmbed)
        Me.GroupBox1.Controls.Add(Me.cmdDBLoc)
        Me.GroupBox1.Controls.Add(Me.txtReportName)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtDBLocation)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(728, 557)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'ucReportOptions
        '
        Me.ucReportOptions.BackColor = System.Drawing.Color.Transparent
        Me.ucReportOptions.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ucReportOptions.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ucReportOptions.Location = New System.Drawing.Point(3, 107)
        Me.ucReportOptions.m_destinationType = Nothing
        Me.ucReportOptions.m_eventBased = False
        Me.ucReportOptions.m_isQuery = False
        Me.ucReportOptions.Name = "ucReportOptions"
        Me.ucReportOptions.selectedFormat = ""
        Me.ucReportOptions.Size = New System.Drawing.Size(722, 423)
        Me.ucReportOptions.TabIndex = 27
        '
        'txtUrl
        '
        Me.txtUrl.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtUrl.Border.Class = "TextBoxBorder"
        Me.txtUrl.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUrl.DisabledBackColor = System.Drawing.Color.White
        Me.txtUrl.ForeColor = System.Drawing.Color.Blue
        Me.txtUrl.Location = New System.Drawing.Point(115, 15)
        Me.txtUrl.Name = "txtUrl"
        Me.txtUrl.Size = New System.Drawing.Size(569, 21)
        Me.txtUrl.TabIndex = 0
        Me.txtUrl.Tag = "memo"
        Me.txtUrl.Text = "http://myReportServer/ReportServer/"
        '
        'Label6
        '
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(6, 17)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(229, 16)
        Me.Label6.TabIndex = 26
        Me.Label6.Text = "Report  Service URL"
        '
        'chkStatus
        '
        '
        '
        '
        Me.chkStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkStatus.Checked = True
        Me.chkStatus.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkStatus.CheckValue = "Y"
        Me.chkStatus.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.chkStatus.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkStatus.Location = New System.Drawing.Point(3, 530)
        Me.chkStatus.Name = "chkStatus"
        Me.chkStatus.Size = New System.Drawing.Size(722, 24)
        Me.chkStatus.TabIndex = 5
        Me.chkStatus.Text = "Enabled"
        '
        'chkEmbed
        '
        Me.chkEmbed.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.chkEmbed.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkEmbed.Enabled = False
        Me.chkEmbed.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkEmbed.ForeColor = System.Drawing.Color.Navy
        Me.chkEmbed.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkEmbed.Location = New System.Drawing.Point(159, 285)
        Me.chkEmbed.Name = "chkEmbed"
        Me.chkEmbed.Size = New System.Drawing.Size(112, 16)
        Me.chkEmbed.TabIndex = 6
        Me.chkEmbed.Text = "Embed into email"
        Me.chkEmbed.Visible = False
        '
        'cmdDBLoc
        '
        Me.cmdDBLoc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdDBLoc.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdDBLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDBLoc.Location = New System.Drawing.Point(628, 42)
        Me.cmdDBLoc.Name = "cmdDBLoc"
        Me.cmdDBLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdDBLoc.TabIndex = 2
        Me.cmdDBLoc.Text = "..."
        '
        'txtReportName
        '
        Me.txtReportName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtReportName.Border.Class = "TextBoxBorder"
        Me.txtReportName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtReportName.DisabledBackColor = System.Drawing.Color.White
        Me.txtReportName.ForeColor = System.Drawing.Color.Blue
        Me.txtReportName.Location = New System.Drawing.Point(115, 69)
        Me.txtReportName.Name = "txtReportName"
        Me.txtReportName.Size = New System.Drawing.Size(569, 21)
        Me.txtReportName.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(6, 71)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(103, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Report Name"
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(6, 44)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(104, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Report Location"
        '
        'txtDBLocation
        '
        Me.txtDBLocation.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.txtDBLocation.Border.Class = "TextBoxBorder"
        Me.txtDBLocation.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDBLocation.DisabledBackColor = System.Drawing.Color.White
        Me.txtDBLocation.ForeColor = System.Drawing.Color.Blue
        Me.txtDBLocation.Location = New System.Drawing.Point(115, 42)
        Me.txtDBLocation.Name = "txtDBLocation"
        Me.txtDBLocation.ReadOnly = True
        Me.txtDBLocation.Size = New System.Drawing.Size(507, 21)
        Me.txtDBLocation.TabIndex = 1
        Me.txtDBLocation.Tag = "memo"
        '
        'cmdLoginTest2
        '
        Me.cmdLoginTest2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdLoginTest2.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdLoginTest2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoginTest2.Location = New System.Drawing.Point(575, 3)
        Me.cmdLoginTest2.Name = "cmdLoginTest2"
        Me.cmdLoginTest2.Size = New System.Drawing.Size(75, 23)
        Me.cmdLoginTest2.TabIndex = 3
        Me.cmdLoginTest2.Text = "Preview"
        '
        'cmdRequery
        '
        Me.cmdRequery.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRequery.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRequery.Enabled = False
        Me.cmdRequery.Image = CType(resources.GetObject("cmdRequery.Image"), System.Drawing.Image)
        Me.cmdRequery.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRequery.Location = New System.Drawing.Point(656, 3)
        Me.cmdRequery.Name = "cmdRequery"
        Me.cmdRequery.Size = New System.Drawing.Size(75, 23)
        Me.cmdRequery.TabIndex = 2
        Me.ToolTip1.SetToolTip(Me.cmdRequery, "Requery the report file for parameters")
        '
        'btnRefreshDS
        '
        Me.btnRefreshDS.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnRefreshDS.BackColor = System.Drawing.SystemColors.Control
        Me.btnRefreshDS.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnRefreshDS.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.btnRefreshDS.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnRefreshDS.Location = New System.Drawing.Point(575, 3)
        Me.btnRefreshDS.Name = "btnRefreshDS"
        Me.btnRefreshDS.Size = New System.Drawing.Size(75, 23)
        Me.btnRefreshDS.TabIndex = 3
        Me.btnRefreshDS.Text = "Refresh"
        Me.ToolTip1.SetToolTip(Me.btnRefreshDS, "Refresh report datasources")
        '
        'lsvDatasources
        '
        Me.lsvDatasources.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvDatasources.Border.Class = "ListViewBorder"
        Me.lsvDatasources.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvDatasources.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader4, Me.ColumnHeader5})
        Me.lsvDatasources.ContextMenuStrip = Me.mnuDatasources
        Me.lsvDatasources.DisabledBackColor = System.Drawing.Color.Empty
        Me.lsvDatasources.ForeColor = System.Drawing.Color.Black
        Me.lsvDatasources.FullRowSelect = True
        Me.lsvDatasources.HideSelection = False
        Me.lsvDatasources.Location = New System.Drawing.Point(3, 3)
        Me.lsvDatasources.Name = "lsvDatasources"
        Me.lsvDatasources.Size = New System.Drawing.Size(728, 468)
        Me.SuperTooltip1.SetSuperTooltip(Me.lsvDatasources, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Double-click on a datasource to set its login credentials. Leaving a datasource's" & _
            " credentials not set will result in errors if they are required.", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon))
        Me.lsvDatasources.TabIndex = 0
        Me.lsvDatasources.UseCompatibleStateImageBehavior = False
        Me.lsvDatasources.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Datasource Name"
        Me.ColumnHeader4.Width = 274
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Name"
        Me.ColumnHeader5.Width = 116
        '
        'mnuDatasources
        '
        Me.mnuDatasources.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClearToolStripMenuItem})
        Me.mnuDatasources.Name = "mnuDatasources"
        Me.mnuDatasources.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuDatasources.Size = New System.Drawing.Size(102, 26)
        '
        'ClearToolStripMenuItem
        '
        Me.ClearToolStripMenuItem.Name = "ClearToolStripMenuItem"
        Me.ClearToolStripMenuItem.Size = New System.Drawing.Size(101, 22)
        Me.ClearToolStripMenuItem.Text = "&Clear"
        '
        'cmdLoginTest
        '
        Me.cmdLoginTest.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdLoginTest.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdLoginTest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoginTest.Location = New System.Drawing.Point(656, 3)
        Me.cmdLoginTest.Name = "cmdLoginTest"
        Me.cmdLoginTest.Size = New System.Drawing.Size(75, 23)
        Me.cmdLoginTest.TabIndex = 2
        Me.cmdLoginTest.Text = "Preview"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.txtAdjustStamp)
        Me.GroupBox2.Controls.Add(Me.Label26)
        Me.GroupBox2.Controls.Add(Me.cmbDateTime)
        Me.GroupBox2.Controls.Add(Me.chkCustomExt)
        Me.GroupBox2.Controls.Add(Me.txtCustomName)
        Me.GroupBox2.Controls.Add(Me.optNaming)
        Me.GroupBox2.Controls.Add(Me.optCustomName)
        Me.GroupBox2.Controls.Add(Me.txtCustomExt)
        Me.GroupBox2.Controls.Add(Me.chkAppendDateTime)
        Me.GroupBox2.Controls.Add(Me.txtDefault)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(717, 496)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        '
        'txtAdjustStamp
        '
        Me.txtAdjustStamp.Enabled = False
        Me.txtAdjustStamp.Location = New System.Drawing.Point(253, 220)
        Me.txtAdjustStamp.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.txtAdjustStamp.Minimum = New Decimal(New Integer() {365, 0, 0, -2147483648})
        Me.txtAdjustStamp.Name = "txtAdjustStamp"
        Me.txtAdjustStamp.Size = New System.Drawing.Size(49, 21)
        Me.txtAdjustStamp.TabIndex = 9
        Me.txtAdjustStamp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(8, 224)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(168, 13)
        Me.Label26.TabIndex = 19
        Me.Label26.Text = "Adjust date/time stamp by (days)"
        '
        'cmbDateTime
        '
        Me.cmbDateTime.Enabled = False
        Me.cmbDateTime.ForeColor = System.Drawing.Color.Blue
        Me.cmbDateTime.ItemHeight = 13
        Me.cmbDateTime.Items.AddRange(New Object() {"ddHH", "ddMM", "ddMMyy", "ddMMyyHHmm", "ddMMyyHHmms", "ddMMyyyy", "ddMMyyyyHHmm", "ddMMyyyyHHmmss", "HHmm", "HHmmss", "MMddyy", "MMddyyHHmm", "MMddyyHHmmss", "MMddyyyy", "MMddyyyyHHmm", "MMddyyyyHHmmss", "yyMMdd", "yyMMddHHmm", "yyMMddHHmmss", "yyyyMMdd", "yyyyMMddHHmm", "yyyyMMddHHmmss", "MMMMM"})
        Me.cmbDateTime.Location = New System.Drawing.Point(253, 185)
        Me.cmbDateTime.Name = "cmbDateTime"
        Me.cmbDateTime.Size = New System.Drawing.Size(383, 21)
        Me.cmbDateTime.TabIndex = 17
        '
        'chkCustomExt
        '
        '
        '
        '
        Me.chkCustomExt.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkCustomExt.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkCustomExt.Location = New System.Drawing.Point(8, 130)
        Me.chkCustomExt.Name = "chkCustomExt"
        Me.chkCustomExt.Size = New System.Drawing.Size(184, 24)
        Me.chkCustomExt.TabIndex = 5
        Me.chkCustomExt.Text = "Customize output extension"
        '
        'txtCustomName
        '
        Me.txtCustomName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtCustomName.Border.Class = "TextBoxBorder"
        Me.txtCustomName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCustomName.ContextMenu = Me.mnuInserter
        Me.txtCustomName.DisabledBackColor = System.Drawing.Color.White
        Me.txtCustomName.Enabled = False
        Me.txtCustomName.ForeColor = System.Drawing.Color.Blue
        Me.txtCustomName.Location = New System.Drawing.Point(253, 79)
        Me.txtCustomName.Name = "txtCustomName"
        Me.txtCustomName.Size = New System.Drawing.Size(383, 21)
        Me.txtCustomName.TabIndex = 4
        Me.txtCustomName.Tag = "memo"
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'optNaming
        '
        Me.optNaming.Checked = True
        Me.optNaming.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optNaming.Location = New System.Drawing.Point(8, 24)
        Me.optNaming.Name = "optNaming"
        Me.optNaming.Size = New System.Drawing.Size(200, 24)
        Me.optNaming.TabIndex = 0
        Me.optNaming.TabStop = True
        Me.optNaming.Text = "Use the default naming convention"
        '
        'optCustomName
        '
        Me.optCustomName.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optCustomName.Location = New System.Drawing.Point(8, 77)
        Me.optCustomName.Name = "optCustomName"
        Me.optCustomName.Size = New System.Drawing.Size(184, 24)
        Me.optCustomName.TabIndex = 3
        Me.optCustomName.Text = "Customize the output file name"
        '
        'txtCustomExt
        '
        Me.txtCustomExt.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtCustomExt.Border.Class = "TextBoxBorder"
        Me.txtCustomExt.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCustomExt.ContextMenu = Me.mnuInserter
        Me.txtCustomExt.DisabledBackColor = System.Drawing.Color.White
        Me.txtCustomExt.Enabled = False
        Me.txtCustomExt.ForeColor = System.Drawing.Color.Blue
        Me.txtCustomExt.Location = New System.Drawing.Point(253, 132)
        Me.txtCustomExt.Name = "txtCustomExt"
        Me.txtCustomExt.Size = New System.Drawing.Size(383, 21)
        Me.txtCustomExt.TabIndex = 6
        '
        'chkAppendDateTime
        '
        '
        '
        '
        Me.chkAppendDateTime.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAppendDateTime.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAppendDateTime.Location = New System.Drawing.Point(8, 183)
        Me.chkAppendDateTime.Name = "chkAppendDateTime"
        Me.chkAppendDateTime.Size = New System.Drawing.Size(208, 24)
        Me.chkAppendDateTime.TabIndex = 7
        Me.chkAppendDateTime.Text = "Append date/time to report output"
        '
        'txtDefault
        '
        Me.txtDefault.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDefault.Border.Class = "TextBoxBorder"
        Me.txtDefault.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDefault.ContextMenu = Me.mnuInserter
        Me.txtDefault.DisabledBackColor = System.Drawing.Color.White
        Me.txtDefault.ForeColor = System.Drawing.Color.Blue
        Me.txtDefault.Location = New System.Drawing.Point(253, 26)
        Me.txtDefault.Name = "txtDefault"
        Me.txtDefault.ReadOnly = True
        Me.txtDefault.Size = New System.Drawing.Size(383, 21)
        Me.txtDefault.TabIndex = 1
        Me.txtDefault.TabStop = False
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.Enabled = False
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(711, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 24)
        Me.cmdOK.TabIndex = 49
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(792, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 24)
        Me.cmdCancel.TabIndex = 50
        Me.cmdCancel.Text = "&Cancel"
        '
        'ErrProv
        '
        Me.ErrProv.ContainerControl = Me
        '
        'txtFormula
        '
        Me.txtFormula.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtFormula.Border.Class = "TextBoxBorder"
        Me.txtFormula.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFormula.DisabledBackColor = System.Drawing.Color.White
        Me.txtFormula.ForeColor = System.Drawing.Color.Black
        Me.txtFormula.Location = New System.Drawing.Point(605, 3)
        Me.txtFormula.Name = "txtFormula"
        Me.txtFormula.Size = New System.Drawing.Size(100, 21)
        Me.txtFormula.TabIndex = 12
        Me.txtFormula.Visible = False
        '
        'ofd
        '
        Me.ofd.DefaultExt = "*.mdb"
        Me.ofd.Filter = "Crystal Reports|*.rpt|All Files|*.*"
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.DefaultTooltipSettings = New DevComponents.DotNetBar.SuperTooltipInfo("", "", "", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray)
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'bgWorker
        '
        Me.bgWorker.WorkerSupportsCancellation = True
        '
        'tbPack
        '
        Me.tbPack.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.tbPack.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.tbPack.ControlBox.MenuBox.Name = ""
        Me.tbPack.ControlBox.Name = ""
        Me.tbPack.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tbPack.ControlBox.MenuBox, Me.tbPack.ControlBox.CloseBox})
        Me.tbPack.Controls.Add(Me.SuperTabControlPanel3)
        Me.tbPack.Controls.Add(Me.SuperTabControlPanel2)
        Me.tbPack.Controls.Add(Me.SuperTabControlPanel1)
        Me.tbPack.Controls.Add(Me.SuperTabControlPanel4)
        Me.tbPack.Controls.Add(Me.SuperTabControlPanel6)
        Me.tbPack.Controls.Add(Me.SuperTabControlPanel5)
        Me.tbPack.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbPack.ForeColor = System.Drawing.Color.Black
        Me.tbPack.Location = New System.Drawing.Point(0, 0)
        Me.tbPack.Name = "tbPack"
        Me.tbPack.ReorderTabsEnabled = True
        Me.tbPack.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.tbPack.SelectedTabIndex = 4
        Me.tbPack.Size = New System.Drawing.Size(870, 560)
        Me.tbPack.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.tbPack.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.tbPack.TabIndex = 51
        Me.tbPack.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabReport, Me.SuperTabItem3, Me.SuperTabItem2, Me.SuperTabItem4, Me.SuperTabItem5, Me.tabDestination})
        Me.tbPack.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        Me.tbPack.Text = "Destination"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.parameterController)
        Me.SuperTabControlPanel2.Controls.Add(Me.FlowLayoutPanel2)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(734, 560)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'parameterController
        '
        Me.parameterController.formsAuth = False
        Me.parameterController.Location = New System.Drawing.Point(11, 12)
        Me.parameterController.Name = "parameterController"
        Me.parameterController.serverPassword = Nothing
        Me.parameterController.serverUser = Nothing
        Me.parameterController.Size = New System.Drawing.Size(709, 511)
        Me.parameterController.ssrsConnected = False
        Me.parameterController.TabIndex = 5
        Me.parameterController.url = Nothing
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdRequery)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdLoginTest2)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 529)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(734, 31)
        Me.FlowLayoutPanel2.TabIndex = 4
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Image = CType(resources.GetObject("SuperTabItem2.Image"), System.Drawing.Image)
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Parameters"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(734, 560)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabReport
        '
        'tabReport
        '
        Me.tabReport.AttachedControl = Me.SuperTabControlPanel1
        Me.tabReport.GlobalItem = False
        Me.tabReport.Image = CType(resources.GetObject("tabReport.Image"), System.Drawing.Image)
        Me.tabReport.Name = "tabReport"
        Me.tabReport.Text = "Report"
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.FlowLayoutPanel3)
        Me.SuperTabControlPanel3.Controls.Add(Me.lsvDatasources)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(734, 560)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.SuperTabItem3
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel3.Controls.Add(Me.cmdLoginTest)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnRefreshDS)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(0, 529)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(734, 31)
        Me.FlowLayoutPanel3.TabIndex = 4
        '
        'SuperTabItem3
        '
        Me.SuperTabItem3.AttachedControl = Me.SuperTabControlPanel3
        Me.SuperTabItem3.GlobalItem = False
        Me.SuperTabItem3.Image = CType(resources.GetObject("SuperTabItem3.Image"), System.Drawing.Image)
        Me.SuperTabItem3.Name = "SuperTabItem3"
        Me.SuperTabItem3.Text = "Datasources"
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.GroupBox2)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(734, 560)
        Me.SuperTabControlPanel4.TabIndex = 0
        Me.SuperTabControlPanel4.TabItem = Me.SuperTabItem4
        '
        'SuperTabItem4
        '
        Me.SuperTabItem4.AttachedControl = Me.SuperTabControlPanel4
        Me.SuperTabItem4.GlobalItem = False
        Me.SuperTabItem4.Image = CType(resources.GetObject("SuperTabItem4.Image"), System.Drawing.Image)
        Me.SuperTabItem4.Name = "SuperTabItem4"
        Me.SuperTabItem4.Text = "Naming (Optional)"
        '
        'SuperTabControlPanel6
        '
        Me.SuperTabControlPanel6.Controls.Add(Me.UcDestPack)
        Me.SuperTabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel6.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel6.Name = "SuperTabControlPanel6"
        Me.SuperTabControlPanel6.Size = New System.Drawing.Size(734, 560)
        Me.SuperTabControlPanel6.TabIndex = 0
        Me.SuperTabControlPanel6.TabItem = Me.tabDestination
        '
        'UcDestPack
        '
        Me.UcDestPack.BackColor = System.Drawing.Color.Transparent
        Me.UcDestPack.destinationsPicker = False
        Me.UcDestPack.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcDestPack.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDestPack.Location = New System.Drawing.Point(0, 0)
        Me.UcDestPack.m_CanDisable = True
        Me.UcDestPack.m_DelayDelete = False
        Me.UcDestPack.m_eventBased = False
        Me.UcDestPack.m_ExcelBurst = False
        Me.UcDestPack.m_IsDynamic = False
        Me.UcDestPack.m_isPackage = False
        Me.UcDestPack.m_IsQuery = False
        Me.UcDestPack.m_StaticDest = False
        Me.UcDestPack.Name = "UcDestPack"
        Me.UcDestPack.Size = New System.Drawing.Size(734, 560)
        Me.UcDestPack.TabIndex = 0
        '
        'tabDestination
        '
        Me.tabDestination.AttachedControl = Me.SuperTabControlPanel6
        Me.tabDestination.GlobalItem = False
        Me.tabDestination.Image = CType(resources.GetObject("tabDestination.Image"), System.Drawing.Image)
        Me.tabDestination.Name = "tabDestination"
        Me.tabDestination.Text = "Destination"
        Me.tabDestination.Visible = False
        '
        'SuperTabControlPanel5
        '
        Me.SuperTabControlPanel5.Controls.Add(Me.UcBlankReportX1)
        Me.SuperTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel5.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel5.Name = "SuperTabControlPanel5"
        Me.SuperTabControlPanel5.Size = New System.Drawing.Size(734, 560)
        Me.SuperTabControlPanel5.TabIndex = 0
        Me.SuperTabControlPanel5.TabItem = Me.SuperTabItem5
        '
        'UcBlankReportX1
        '
        Me.UcBlankReportX1.BackColor = System.Drawing.Color.Transparent
        Me.UcBlankReportX1.blankID = 0
        Me.UcBlankReportX1.blankType = "AlertandReport"
        Me.UcBlankReportX1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcBlankReportX1.Location = New System.Drawing.Point(0, 0)
        Me.UcBlankReportX1.m_customDSN = ""
        Me.UcBlankReportX1.m_customPassword = ""
        Me.UcBlankReportX1.m_customUserID = ""
        Me.UcBlankReportX1.m_showAllReportsBlankForTasks = False
        Me.UcBlankReportX1.Name = "UcBlankReportX1"
        Me.UcBlankReportX1.Size = New System.Drawing.Size(734, 560)
        Me.UcBlankReportX1.TabIndex = 0
        '
        'SuperTabItem5
        '
        Me.SuperTabItem5.AttachedControl = Me.SuperTabControlPanel5
        Me.SuperTabItem5.GlobalItem = False
        Me.SuperTabItem5.Image = CType(resources.GetObject("SuperTabItem5.Image"), System.Drawing.Image)
        Me.SuperTabItem5.Name = "SuperTabItem5"
        Me.SuperTabItem5.Text = "Exception Handling"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtFormula)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 560)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(870, 32)
        Me.FlowLayoutPanel1.TabIndex = 52
        '
        'frmPackedReport
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(870, 592)
        Me.ControlBox = False
        Me.Controls.Add(Me.tbPack)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmPackedReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Packaged Report Properties"
        Me.GroupBox1.ResumeLayout(False)
        Me.mnuDatasources.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.txtAdjustStamp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrProv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbPack, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbPack.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.SuperTabControlPanel4.ResumeLayout(False)
        Me.SuperTabControlPanel6.ResumeLayout(False)
        Me.SuperTabControlPanel5.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region


    Public Property m_EventReport() As Boolean
        Get
            Return eventReport
        End Get
        Set(ByVal value As Boolean)
            If value = True Then
                AddDestinationPage()
            End If

            eventReport = value

            If value = True Then
                chkStatus.Visible = False
                ucReportOptions.Visible = False
            End If

        End Set
    End Property

    Public Property m_eventID() As Integer
        Get
            Return PackID
        End Get
        Set(ByVal value As Integer)
            PackID = value
        End Set
    End Property
    Private Sub AddDestinationPage()

        tabDestination.Visible = True
        UcDestPack.m_eventBased = True
        UcDestPack.eventID = Me.m_eventID
        UcDestPack.nReportID = Me.ReportID

        ucReportOptions.Visible = False
    End Sub

    Private Sub GetParameterAvailableValues()
        Try
            oRpt = New rsClients.rsClient(txtUrl.Text)

            clsMarsUI.MainUI.BusyProgress(10, "Connecting to server...")

            oRpt.Url = txtUrl.Text

            If ServerUser = "" Then
                oRpt.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials
            Else
                Dim userDomain As String = ""
                Dim tmpUser As String = ServerUser

                getDomainAndUserFromString(tmpUser, userDomain)

                Dim oCred As System.Net.NetworkCredential = New System.Net.NetworkCredential(tmpUser, ServerPassword, userDomain)

                oRpt.Credentials = oCred
            End If

            Dim sRDLPath As String = clsMarsReport.m_rdltempPath & Me.txtReportName.Text & ".rdl"
            Dim repDef() As Byte

            clsMarsUI.MainUI.BusyProgress(40, "Getting server definition...")

            If IO.File.Exists(sRDLPath) = True Then
                IO.File.Delete(sRDLPath)
            End If

            Dim fsReport As New System.IO.FileStream(sRDLPath, IO.FileMode.Create)

            repDef = oRpt.GetReportDefinition(txtDBLocation.Text)

            fsReport.Write(repDef, 0, repDef.Length)

            fsReport.Close()

            clsMarsUI.MainUI.BusyProgress(70, "Loading values...")

            clsMarsReport._GetParameterValues(sRDLPath)

            Dim sPars() As String
            Dim sParDefaults As String()
            Dim I As Integer = 0

            sPars = clsMarsReport._GetParameterFields(sRDLPath)
            sParDefaults = clsMarsReport._GetParameterValues(sRDLPath)

            ParDefaults = New Hashtable

            clsMarsUI.MainUI.BusyProgress(90, "Populating parameters...")

            If sPars IsNot Nothing Then
                For Each s As String In sPars

                    ParDefaults.Add(s.Split("|")(0), sParDefaults(I))

                    I += 1
                Next
            End If

            clsMarsUI.MainUI.BusyProgress(, , True)
        Catch
            clsMarsUI.MainUI.BusyProgress(, , True)
        End Try

    End Sub
    Public Function EditReport(ByVal nReportID As Integer, Optional ByRef serverParameters As DataTable = Nothing) As String()
        AddHandler ucReportOptions.cmbFormat.SelectedValueChanged, AddressOf changeNamingName
        AddHandler ucReportOptions.cmbFormat.TextChanged, AddressOf changeNamingName

        Dim SQL As String
        Dim sWhere As String = " WHERE ReportID = " & nReportID
        Dim rs As ADODB.Recordset
        Dim nIdent As Integer
        Dim sVals(1) As String
        Dim CachePath As String

        m_Mode = "Edit"

        Me.showTooltip = False

        SQL = "SELECT * FROM ReportAttr" & sWhere

        rs = clsMarsData.GetData(SQL)

        ReportID = nReportID

        cmdRequery.Enabled = True

        Try
            If rs.EOF = False Then
                Try
                    If Me.m_EventReport = False Then
                        nIdent = GetDelimitedWord(rs("reporttitle").Value, 1, ":")
                    Else
                        nIdent = 0
                    End If
                Catch
                    nIdent = 0
                End Try

                txtReportName.Text = IIf(Me.m_EventReport = False, GetDelimitedWord(rs("reporttitle").Value, 2, ":"), rs("reporttitle").Value)
                txtDBLocation.Text = rs("cachepath").Value
                txtUrl.Text = rs("databasepath").Value
                txtFormula.Text = IsNull(rs("selectionformula").Value)
                CachePath = rs("cachepath").Value
                PackID = rs("packID").Value
                Me.ServerUser = IsNull(rs("rptuserid").Value)
                Me.ServerPassword = _DecryptDBValue(IsNull(rs("rptpassword").Value))


                Me.previousReportLocation = rs("cachepath").Value
                Me.previousReportServer = rs("databasepath").Value

                Try
                    formsAuth = Convert.ToInt32(IsNull(rs("rptFormsAuth").Value, 0))
                Catch ex As Exception
                    formsAuth = False
                End Try

                If UcDestPack IsNot Nothing Then
                    With UcDestPack
                        .nReportID = nReportID
                        .nPackID = 0
                        .nSmartID = 0
                        UcDestPack.m_isPackage = False
                        UcDestPack.eventID = PackID
                        .LoadAll()
                    End With
                End If

                Try
                    UcBlankReportX1.chkCheckBlankReport.Checked = Convert.ToBoolean(rs("checkblank").Value)
                    Me.UcBlankReportX1.UcBlankReportTasks.ScheduleID = nReportID
                    UcBlankReportX1.getInfo(nReportID, clsMarsScheduler.enScheduleType.REPORT)
                Catch : End Try

            End If
        Catch : End Try

        rs.Close()

        SQL = "SELECT * FROM PackagedReportAttr " & sWhere

        rs = clsMarsData.GetData(SQL)

        Try
            If rs.EOF = False Then
                ucReportOptions.selectedFormat = rs("outputformat").Value
                txtAdjustStamp.Value = IsNull(rs("adjuststamp").Value, 0)

                If rs("customext").Value.Length > 0 Then
                    txtCustomExt.Text = rs("customext").Value
                    chkCustomExt.Checked = True
                End If

                chkAppendDateTime.Checked = Convert.ToBoolean(Convert.ToInt32(rs("appenddatetime").Value))

                If chkAppendDateTime.Checked = True Then
                    cmbDateTime.Text = rs("datetimeformat").Value
                End If

                If rs("customname").Value.Length > 0 Then
                    txtCustomName.Text = rs("customname").Value
                    optCustomName.Checked = True
                Else
                    optNaming.Checked = True
                End If

                Try
                    chkStatus.Checked = Convert.ToBoolean(rs("status").Value)
                Catch
                    chkStatus.Checked = True
                End Try
            End If

            rs.Close()
        Catch : End Try

        Dim ssrsReport As New clsMarsReport

        'UcParsList.m_serverUrl = txtUrl.Text
        'UcParsList.m_reportPath =
        'UcParsList.m_serverUser = ServerUser
        'UcParsList.m_serverPassword = ServerPassword

        'UcParsList.m_serverparametersCollection = ssrsReport.getserverReportParametersCollection(txtUrl.Text, txtDBLocation.Text, ServerUser, ServerPassword, "", formsAuth, , ReportID)

        parameterController.loadParameters(nReportID, txtUrl.Text, txtDBLocation.Text, Nothing, ServerUser, ServerPassword, formsAuth)

        'custom sections
        SQL = "SELECT * FROM ReportDatasource WHERE ReportID =" & nReportID

        rs = clsMarsData.GetData(SQL)

        Try
            Dim oItem As ListViewItem

            If rs.EOF = False Then
                lsvDatasources.Items.Clear()
            End If

            Do While rs.EOF = False
                oItem = lsvDatasources.Items.Add(rs("datasourcename").Value)
                oItem.SubItems.Add(rs("rptuserid").Value)
                oItem.Tag = rs("datasourceid").Value
                rs.MoveNext()
            Loop

            rs.Close()
        Catch : End Try

        'blank reports
        If UcBlankReportX1.chkCheckBlankReport.Checked = True Then
            UcBlankReportX1.getInfo(nReportID, clsMarsScheduler.enScheduleType.REPORT)
        End If

        cmdOK.Enabled = True

        Me.showTooltip = True

        If Me.IsDataDriven = True Then
            ucReportOptions.cmbFormat.DropDownStyle = ComboBoxStyle.DropDown
        End If

        Try
            bgWorker.RunWorkerAsync()
        Catch : End Try

        ucReportOptions.loadReportOptions(False, "packaged", 0, nReportID, False)

        Me.ShowDialog()

        If UserCancel = True Then Return Nothing

        SQL = "UPDATE ReportAttr SET " & _
            "DatabasePath = '" & SQLPrepare(txtUrl.Text) & "'," & _
            "ReportName = '" & SQLPrepare(txtReportName.Text) & "'," & _
            "ReportTitle = '" & IIf(Me.m_EventReport = False, nIdent & ":" & SQLPrepare(txtReportName.Text), SQLPrepare(txtReportName.Text)) & "'," & _
            "SelectionFormula = '" & SQLPrepare(txtFormula.Text) & "', " & _
            "CachePath = '" & SQLPrepare(txtDBLocation.Text) & "'," & _
            "UseSavedData = " & 0 & "," & _
            "RptUserID = '" & SQLPrepare(Me.ServerUser) & "'," & _
            "RptPassword = '" & SQLPrepare(_EncryptDBValue(Me.ServerPassword)) & "'," & _
            "RptServer = ''," & _
            "RptDatabase = ''," & _
            "UseLogin = " & 0 & "," & _
            "AutoRefresh =" & 0 & "," & _
            "RptDatabaseType =''," & _
            "CheckBlank =" & Convert.ToInt32(UcBlankReportX1.chkCheckBlankReport.Checked) & " "

        SQL &= sWhere

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE PackagedReportAttr SET " & _
        "OutputFormat ='" & SQLPrepare(ucReportOptions.selectedFormat) & "'," & _
        "CustomExt = '" & SQLPrepare(txtCustomExt.Text) & "'," & _
        "AppendDateTime =" & Convert.ToInt32(chkAppendDateTime.Checked) & "," & _
        "DateTimeFormat ='" & SQLPrepare(cmbDateTime.Text) & "'," & _
        "CustomName ='" & SQLPrepare(txtCustomName.Text) & "'," & _
        "Status = " & Convert.ToInt32(chkStatus.Checked) & "," & _
        "AdjustStamp = " & txtAdjustStamp.Value

        SQL &= sWhere

        clsMarsData.WriteData(SQL)

        For Each item As ListViewItem In lsvDatasources.Items
            SQL = "SELECT * FROM ReportDatasource WHERE ReportID = " & nReportID & " AND " & _
            "DatasourceName = '" & SQLPrepare(item.Text) & "'"

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing Then
                If oRs.EOF = True Then
                    Dim sCols As String = "DatasourceID,ReportID,DatasourceName,RptUserID,RptPassword"
                    Dim sVal As String = clsMarsData.CreateDataID("reportdatasource", "datasourceid") & "," & _
                    nReportID & "," & _
                    "'" & SQLPrepare(item.Text) & "'," & _
                    "'default'," & _
                    "'<default>'"

                    SQL = "INSERT INTO ReportDatasource (" & sCols & ") VALUES (" & sVal & ")"

                    clsMarsData.WriteData(SQL)
                End If

                oRs.Close()
            End If
        Next

        If tabDestination.Visible = False Then ucReportOptions.SaveReportOptions("packaged", 0, nReportID)


        If parameterController.numberOfParameters > 0 Then parameterController.saveParameters(nReportID)

        'save the blank report attributes
        UcBlankReportX1.saveInfo(nReportID, clsMarsScheduler.enScheduleType.REPORT)

        If Me.m_EventReport = False Then
            sVals(0) = nIdent & ":" & txtReportName.Text
        Else
            sVals(0) = txtReportName.Text
        End If

        sVals(1) = ucReportOptions.selectedFormat

        parameterController.generateParametersDataTable(serverParameters)


        clsMarsAudit._LogAudit(txtReportName.Text, clsMarsAudit.ScheduleType.PACKED, clsMarsAudit.AuditAction.EDIT)

        Return sVals

    End Function

    Private Property totalReportsInPackage As Integer

    Public Function AddReport(ByVal nCount As Integer, Optional ByVal nPackID As Integer = 99999, _
    Optional ByRef reportPars As Hashtable = Nothing, Optional ByRef serverParameters As DataTable = Nothing, Optional ByVal sUrl As String = "") As String()

        AddHandler ucReportOptions.cmbFormat.SelectedValueChanged, AddressOf changeNamingName
        AddHandler ucReportOptions.cmbFormat.TextChanged, AddressOf changeNamingName

        Dim SQL As String
        Dim sVal(2) As String
        Dim nTempID As Int32
        Dim nReportID As Integer
        Dim sCols As String
        Dim sVals As String
        Dim oReport As New clsMarsReport

        m_Mode = "Add"

        totalReportsInPackage = nCount

        PackID = nPackID

        If reportPars IsNot Nothing Then
            Me.parCollection = reportPars
        Else
            parCollection = New Hashtable
        End If

        If _CheckScheduleCount() = False Then Return sVal

        ReportID = clsMarsData.CreateDataID("reportattr", "reportid")

        nReportID = ReportID

        Try

            If UcDestPack IsNot Nothing Then
                UcDestPack.nReportID = nReportID
                UcDestPack.nPackID = 0
                UcDestPack.nSmartID = 0
                UcDestPack.m_isPackage = False
            End If

            cmdOK.Enabled = True

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try

        If Me.m_serverUrl = "" Or Me.m_serverUrl = "http://myReportServer/ReportServer/" Then
            txtUrl.Text = clsMarsUI.MainUI.ReadRegistry("ReportsLoc", "")
        Else
            txtUrl.Text = Me.m_serverUrl
        End If

        'set up auto-complete data
        With Me.txtUrl
            .AutoCompleteMode = AutoCompleteMode.SuggestAppend
            .AutoCompleteSource = AutoCompleteSource.CustomSource
            .AutoCompleteCustomSource = getUrlHistory()
        End With

        If Me.IsDataDriven = True Then
            ucReportOptions.cmbFormat.DropDownStyle = ComboBoxStyle.DropDown
        End If

        Me.UcBlankReportX1.UcBlankReportTasks.ScheduleID = nReportID

        parameterController.reportid = nReportID

        Me.ShowDialog()

        If UserCancel = True Then
            UserCancel = False
            Return sVal
            Exit Function
        End If

        If parCollection IsNot Nothing Then
            reportPars = parCollection
        End If

        ''nTempid is used as a temporary place holder for a packaged report
        nTempID = nPackID

        nReportID = ReportID

        ''save the report
        sCols = "ReportID,PackID,DatabasePath,ReportName," & _
            "Parent,ReportTitle," & _
            "Retry," & _
            "SelectionFormula,Owner,RptUserID,RptPassword,RptServer,RptDatabase,UseLogin," & _
            "UseSavedData,CachePath,AutoRefresh,PackOrderID,RptDatabaseType,CheckBlank, rptFormsAuth"


        sVals = nReportID & "," & _
        nPackID & "," & _
        "'" & SQLPrepare(txtUrl.Text) & "'," & _
        "'" & SQLPrepare(txtReportName.Text) & "'," & _
        0 & "," & _
        "'" & IIf(Me.m_EventReport = False, nCount & ":" & SQLPrepare(txtReportName.Text), SQLPrepare(txtReportName.Text)) & "'," & _
        0 & "," & _
        "'" & SQLPrepare(txtFormula.Text) & "'," & _
        "'" & gUser & "'," & _
        "'" & SQLPrepare(ServerUser) & "'," & _
        "'" & SQLPrepare(_EncryptDBValue(ServerPassword)) & "'," & _
        "''," & _
        "''," & _
        0 & "," & _
        0 & "," & _
        "'" & SQLPrepare(txtDBLocation.Text) & "'," & _
        0 & "," & _
        nCount & "," & _
        "''," & _
        Convert.ToInt32(UcBlankReportX1.chkCheckBlankReport.Checked) & "," & _
        Convert.ToInt32(formsAuth)

        SQL = "INSERT INTO ReportAttr(" & sCols & ") VALUES (" & sVals & ")"

        'lets save the other report properties
        If clsMarsData.WriteData(SQL) = True Then
            sCols = "AttrID,PackID,ReportID,OutputFormat,CustomExt," & _
            "AppendDateTime,DateTimeFormat,CustomName,Status,DisabledDate,AdjustStamp"

            sVals = clsMarsData.CreateDataID("packagedreportattr", "attrid") & "," & _
            nPackID & "," & _
            nReportID & "," & _
            "'" & SQLPrepare(ucReportOptions.selectedFormat) & "'," & _
            "'" & SQLPrepare(txtCustomExt.Text) & "'," & _
            Convert.ToInt32(chkAppendDateTime.Checked) & "," & _
            "'" & SQLPrepare(cmbDateTime.Text) & "'," & _
            "'" & SQLPrepare(txtCustomName.Text) & "'," & _
            Convert.ToInt32(chkStatus.Checked) & "," & _
            "'" & ConDateTime(Now) & "'," & _
            txtAdjustStamp.Value

            SQL = "INSERT INTO PackagedReportAttr(" & sCols & ") " & _
            "VALUES (" & sVals & ")"

            clsMarsData.WriteData(SQL)

        End If

        parameterController.saveParameters(nReportID)

        SQL = "UPDATE ReportOptions SET " & _
        "ReportID = " & nReportID & "," & _
        "DestinationID = 0 WHERE ReportID = 99999 OR DestinationID = 99999"

        clsMarsData.WriteData(SQL, False)

        SQL = "UPDATE SubReportParameters SET ReportID = " & nReportID & " WHERE ReportID = 99999"

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE SubReportLogin SET ReportID = " & nReportID & " WHERE ReportID = 99999"

        clsMarsData.WriteData(SQL)

        clsMarsData.WriteData("UPDATE StampAttr SET ForeignID =" & nReportID & " WHERE ForeignID=99999")

        SQL = "UPDATE ReportDatasource SET ReportID = " & nReportID & " WHERE ReportID = 99999"

        clsMarsData.WriteData(SQL)

        For Each item As ListViewItem In lsvDatasources.Items
            SQL = "SELECT * FROM ReportDatasource WHERE ReportID = " & nReportID & " AND " & _
            "DatasourceName = '" & SQLPrepare(item.Text) & "'"

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing Then
                If oRs.EOF = True Then
                    sCols = "DatasourceID,ReportID,DatasourceName,RptUserID,RptPassword"
                    sVals = clsMarsData.CreateDataID("reportdatasource", "datasourceid") & "," & _
                    nReportID & "," & _
                    "'" & SQLPrepare(item.Text) & "'," & _
                    "'default'," & _
                    "'<default>'"

                    SQL = "INSERT INTO ReportDatasource (" & sCols & ") VALUES (" & sVals & ")"

                    clsMarsData.WriteData(SQL)
                End If

                oRs.Close()
            End If
        Next

        If tabDestination.Visible = False Then ucReportOptions.SaveReportOptions("packaged", 0, nReportID)

        'save blank report stuff
        If UcBlankReportX1.chkCheckBlankReport.Checked = True Then
            UcBlankReportX1.saveInfo(nReportID, clsMarsScheduler.enScheduleType.REPORT)
        End If

        sVal(0) = nCount & ":" & txtReportName.Text
        sVal(1) = nReportID
        sVal(2) = ucReportOptions.selectedFormat

        clsMarsAudit._LogAudit(txtReportName.Text, clsMarsAudit.ScheduleType.PACKED, clsMarsAudit.AuditAction.CREATE)

        parameterController.generateParametersDataTable(serverParameters)

        Return sVal
    End Function


    
    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        'validate user entry
        If txtReportName.Text = "" Then
            ErrProv.SetError(txtReportName, "Please provide a name for this report")
            txtReportName.Focus()
            Exit Sub
        ElseIf txtDBLocation.Text = "" Then
            ErrProv.SetError(txtDBLocation, "Please select the Access database containing the report")
            txtDBLocation.Focus()
            Exit Sub
        ElseIf ucReportOptions.selectedFormat = "" And Me.m_EventReport = False Then
            tbPack.SelectedTab = tabReport
            ErrProv.SetError(ucReportOptions.cmbFormat, "Please select the output format")
            ucReportOptions.cmbFormat.Focus()
            Exit Sub
        ElseIf optCustomName.Checked = True And txtCustomName.Text = "" Then
            ErrProv.SetError(txtCustomName, "Please specify the custom output name")
            txtCustomName.Focus()
            Exit Sub
        ElseIf chkCustomExt.Checked = True And txtCustomExt.Text = "" Then
            ErrProv.SetError(txtCustomExt, "Please specify the custom extenstion")
            txtCustomExt.Focus()
            Exit Sub
        ElseIf chkAppendDateTime.Checked = True And cmbDateTime.Text = "" Then
            ErrProv.SetError(cmbDateTime, "Please select a date/time format")
            cmbDateTime.Focus()
            Exit Sub
        ElseIf Me.m_EventReport = True Then
            If UcDestPack IsNot Nothing Then
                If UcDestPack.lsvDestination.Nodes.Count = 0 Then
                    ErrProv.SetError(UcDestPack.lsvDestination, "Please add a destination for this report")
                    Me.tbPack.SelectedTab = Me.tbPack.Tabs(4)
                    Return
                End If
            End If
        End If

        Dim SQL As String = "SELECT * FROM ReportParameter WHERE ReportID = " & ReportID
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim pars As New ArrayList

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                pars.Add(IsNull(oRs("parname").Value).ToString.ToLower)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        Dim blankFound As Boolean = parameterController.hasUnsetParameters


        If blankFound Then
            Dim oRes As DialogResult = _
                MessageBox.Show("One or more parameters has been " & _
                "left blank. Edit the parameters? Click 'Yes' to add a parameter value or 'No' to leave empty.", _
                Application.ProductName, MessageBoxButtons.YesNo, _
                MessageBoxIcon.Question)

            If oRes = Windows.Forms.DialogResult.Yes Then
                Return
            End If
        End If

        Me.Close()
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True

        If m_Mode = "Edit" Then
            clsMarsData.WriteData(String.Format("UPDATE reportattr SET databaselocation = '{0}', cachepath ='{1}' WHERE reportid = {2}", SQLPrepare(previousReportServer), SQLPrepare(previousReportLocation), ReportID), False)
        End If

        Me.Close()
    End Sub

    Private Sub txtReportName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtReportName.TextChanged
        Try
            oErr.ResetError(sender, ErrProv)

            If optNaming.Checked = True Then
                If ucReportOptions.selectedFormat.Length > 0 Then
                    txtDefault.Text = txtReportName.Text & ucReportOptions.selectedFormat.Split("*")(1).Replace(")", String.Empty).Trim
                End If
            Else
                txtDefault.Text = txtReportName.Text
            End If

            If UcDestPack IsNot Nothing Then
                UcDestPack.sReportTitle = txtReportName.Text
            End If
        Catch : End Try
    End Sub


    Private Sub txtCustomName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCustomName.TextChanged
        oErr.ResetError(sender, ErrProv)
    End Sub

    Private Sub txtCustomExt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCustomExt.TextChanged
        oErr.ResetError(sender, ErrProv)
    End Sub

    Private Sub cmbDateTime_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDateTime.SelectedIndexChanged
        oErr.ResetError(sender, ErrProv)
    End Sub

    Private Sub cmbDateTime_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbDateTime.Click
        ' oErr.ResetError(sender, ErrProv)
    End Sub

    Private Sub cmdDBLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDBLoc.Click
        Try
            Dim txtDesc, txtKeyword As TextBox
            txtDesc = New TextBox
            txtKeyword = New TextBox

            browseAndPickReportFromServer(oRpt, ServerUser, ServerPassword, txtUrl, txtDBLocation, txtReportName, txtDesc, txtKeyword, cmdOK, Nothing, lsvDatasources, formsAuth)

            parameterController.loadParameters(txtUrl.Text, txtDBLocation.Text, Nothing, ServerUser, ServerPassword, formsAuth)

            Return

            Dim srsVersion As String = clsMarsReport.m_serverVersion(txtUrl.Text, ServerUser, ServerPassword, formsAuth)

            'If srsVersion >= "2009" Then
            'oRpt = New rsClients.rsClient2010(txtUrl.Text)
            If srsVersion >= "2007" Then
                oRpt = New rsClients.rsClient2008(txtUrl.Text)
            Else
                oRpt = New rsClients.rsClient(txtUrl.Text)
            End If

            'we check to see if we should put reportserver2005.asmx in the URL
            If srsVersion >= "2007" Then txtUrl.Text = clsMarsParser.Parser.fixASMXfor2008(txtUrl.Text)

            If txtUrl.Text.Contains("2006.asmx") Then '//its sharepoint mode
                oRpt = New ReportServer_2006.ReportingService2006
            ElseIf txtUrl.Text.Contains("2010.asmx") Then
                oRpt = New rsClients.rsClient2010(txtUrl.Text)
            End If

            oRpt.Url = txtUrl.Text

            Dim sPath As String

            Dim oServer As frmReportServer = New frmReportServer

            'If Me.m_Mode = "Edit" Then oServer.m_ReadOnly = True

            sPath = oServer.newGetReports(oRpt, txtUrl.Text, ServerUser, ServerPassword, txtDBLocation.Text)

            If sPath IsNot Nothing Then
                txtDBLocation.Text = sPath
                txtReportName.Text = sPath.Split("/")(sPath.Split("/").GetUpperBound(0))
                cmdOK.Enabled = True

                If txtDBLocation.Text.StartsWith("/") = False Then
                    txtDBLocation.Text = "/" & txtDBLocation.Text
                End If
            Else
                Return
            End If

            If Me.m_Mode = "Edit" Then
                cmdRequery_Click(sender, e)
                btnRefreshDS_Click(sender, e)
            Else
                sRDLPath = clsMarsReport.m_rdltempPath & txtReportName.Text & ".rdl"
                Dim repDef() As Byte

                If IO.File.Exists(sRDLPath) = True Then
                    IO.File.Delete(sRDLPath)
                End If

                Dim fsReport As New System.IO.FileStream(sRDLPath, IO.FileMode.Create)

                If TypeOf oRpt Is rsClients.rsClient2010 Then
                    repDef = oRpt.GetItemDefinition(txtDBLocation.Text)
                Else
                    repDef = oRpt.GetReportDefinition(txtDBLocation.Text)
                End If

                fsReport.Write(repDef, 0, repDef.Length)

                fsReport.Close()

                'get the parameters
                'UcParsList.m_serverUrl = txtUrl.Text
                'UcParsList.m_reportPath = txtDBLocation.Text
                'UcParsList.m_serverUser = ServerUser
                'UcParsList.m_serverPassword = ServerPassword
                'UcParsList.m_rdlPath = sRDLPath
                'UcParsList.loadParameters()

                lsvDatasources.Items.Clear()

                Dim sData As ArrayList = New ArrayList

                sData = clsMarsReport._GetDatasources(sRDLPath)

                If sData IsNot Nothing Then
                    lsvDatasources.Items.Clear()

                    For Each s As Object In sData
                        Dim oItem As ListViewItem = lsvDatasources.Items.Add(s)

                        oItem.SubItems.Add("default")

                        oItem.ImageIndex = 0
                        oItem.Tag = 0
                    Next
                End If

                'txtDesc.Text = clsMarsReport._GetReportDescription(sRDLPath)

                IO.File.Delete(sRDLPath)
            End If

            txtReportName.Focus()
            txtReportName.SelectAll()

            cmdOK.Enabled = True
            'save the Url as it has been validated
            logUrlHistory(Me.txtUrl.Text)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), _
            "Please check your report server's web service URL and try again")

            If ex.Message.ToLower.Contains("cannot be found") Then
                txtDBLocation.Text = ""
                txtReportName.Text = ""
                'txtDesc.Text = ""
            End If
        End Try
    End Sub



    Private Sub optCustomName_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optCustomName.CheckedChanged
        If optCustomName.Checked = True Then
            txtCustomName.Enabled = True
            txtDefault.Text = String.Empty
        Else
            txtCustomName.Enabled = False
            txtCustomName.Text = ""
        End If
    End Sub

    Private Sub chkCustomExt_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCustomExt.CheckedChanged
        If chkCustomExt.Checked = True Then
            txtCustomExt.Enabled = True
        Else
            txtCustomExt.Enabled = False
            txtCustomExt.Text = ""
        End If
    End Sub

    Private Sub chkAppendDateTime_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAppendDateTime.CheckedChanged
        If chkAppendDateTime.Checked = True Then
            cmbDateTime.Enabled = True

            Dim oUI As New clsMarsUI
            cmbDateTime.Text = oUI.ReadRegistry("DefDateTimeStamp", "")
            txtAdjustStamp.Enabled = True
        Else
            cmbDateTime.Enabled = False
            cmbDateTime.Text = ""
            txtAdjustStamp.Enabled = False
            txtAdjustStamp.Value = 0
        End If
    End Sub

    Private Sub frmPackedReport_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        g_parameterList = Nothing
    End Sub



    Private Sub frmPackedReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        txtReportName.Focus()

        If txtUrl.Text.Length = 0 Then
            txtUrl.Text = "http://myReportServer/ReportServer/Reportservice.asmx"
            txtUrl.ForeColor = Color.Gray
        End If
        tbPack.SelectedTab = tbPack.Tabs(0)

        setupForDragAndDrop(txtCustomExt)
        setupForDragAndDrop(txtCustomName)

        UcBlankReportX1.UcBlankReportTasks.ShowAfterType = False
    End Sub

    Private Sub txtCustomName_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCustomName.GotFocus
        oField = txtCustomName
    End Sub

    Private Sub txtCustomExt_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCustomExt.GotFocus
        oField = txtCustomExt
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        On Error Resume Next

        oField.Undo()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next

        oField.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next

        oField.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next

        oField.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next

        oField.SelectedText = String.Empty
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next

        oField.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(Me.PackID)
        oInsert.m_EventBased = Me.m_EventReport
        oInsert.m_EventID = PackID
        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oItem As New frmDataItems

        oField.SelectedText = oItem._GetDataItem(Me.m_eventID)

    End Sub

    Private Sub cmbFormat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            If optNaming.Checked = True And ucReportOptions.selectedFormat.Length > 0 Then
                txtDefault.Text = txtReportName.Text & ucReportOptions.selectedFormat.Split("*")(1).Replace(")", String.Empty).Trim
            End If
        Catch
            txtDefault.Text = String.Empty
        End Try

        Dim sWorksheet As String

        If txtCustomName.Text.Length = 0 Then
            sWorksheet = txtReportName.Text
        Else
            sWorksheet = txtCustomName.Text
        End If

        ErrProv.SetError(sender, String.Empty)

        If Me.Visible = True Then
            Dim opt As New frmRptOptions

            Dim format As String = ""

            If ucReportOptions.selectedFormat.Contains("(") Then
                format = ucReportOptions.selectedFormat.Split("(")(0).Trim()
            Else
                format = ucReportOptions.selectedFormat
            End If

            Select Case ucReportOptions.selectedFormat
                Case "Acrobat Format (*.pdf)"
                    opt.ReportOptions(format, "Packed", ReportID)
                Case "MS Excel 97-2000 (*.xls)"
                    opt.ReportOptions(format, "Packed", ReportID, , , sWorksheet)
                Case "MS Word (*.doc)"
                    opt.ReportOptions(format, "Packed", ReportID, )
                Case "CSV (*.csv)"
                    opt.ReportOptions(ucReportOptions.selectedFormat.Split("(")(0).Trim, "Package", ReportID)
                Case "HTML (*.htm)"
                    opt.ReportOptions(ucReportOptions.selectedFormat.Split("(")(0).Trim, "Package", ReportID)
                Case "TIFF (*.tif)"
                    opt.ReportOptions(format, "Packed", ReportID)
                Case "Text (*.txt)"
                    opt.ReportOptions(format, "Packed", ReportID)
                Case "Custom (*.*)"
                    Me.chkCustomExt.Checked = True
                    opt.ReportOptions(format, "Package", ReportID)
            End Select
        End If
    End Sub

    Private Sub optNaming_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optNaming.CheckedChanged

        If optNaming.Checked = True And ucReportOptions.selectedFormat.Length > 0 Then
            txtDefault.Text = txtReportName.Text & ucReportOptions.selectedFormat.Split("*")(1).Replace(")", String.Empty).Trim
        End If

    End Sub

    Private Sub lsvDatasources_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvDatasources.DoubleClick
        Dim oSet As New frmSetTableLogin
        Dim oItem As ListViewItem
        Dim sValues() As String

        If lsvDatasources.SelectedItems.Count = 0 Then Return

        oItem = lsvDatasources.SelectedItems(0)

        sValues = oSet.SaveLoginInfo(ReportID, oItem.Text, oItem.Tag)

        If Not sValues Is Nothing Then
            oItem.SubItems(1).Text = sValues(0)
            oItem.Tag = sValues(1)
        End If
    End Sub

    Private Sub cmdLoginTestregress_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) 'Handles cmdLoginTest.Click
        If m_Mode = "Add" Then
            Dim res As System.Windows.Forms.DialogResult = MessageBox.Show("This will save the report set up. Would you like to proceed?", Application.ProductName, MessageBoxButtons.YesNo)

            If res = Windows.Forms.DialogResult.Yes Then
                '      Me.SaveNewReportInformation(parCollection, PackID, ReportID, totalReportsInPackage, m_serverParametersTable)
            End If
            clsMarsReport.oReport.ViewReport(ReportID, True, ServerUser, ServerPassword, True)
        Else
            Dim res As System.Windows.Forms.DialogResult = MessageBox.Show("This will save the selected  parameters. Would you like to proceed?", Application.ProductName, MessageBoxButtons.YesNo)

            If res = Windows.Forms.DialogResult.Yes Then
                If parameterController.numberOfParameters > 0 Then parameterController.saveParameters(ReportID)
                clsMarsReport.oReport.ViewReport(ReportID, True, ServerUser, ServerPassword, True)
            End If
        End If

    End Sub


    Private Sub cmdLoginTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoginTest.Click

        If m_Mode <> "Add" Then
            If parameterController.numberOfParameters > 0 Then parameterController.saveParameters(ReportID)

            clsMarsData.WriteData("Update reportattr set cachepath ='" & SQLPrepare(txtDBLocation.Text) & "' WHERE reportid = " & ReportID)

            clsMarsReport.oReport.ViewReport(ReportID, True, ServerUser, ServerPassword, True)

            Return
        End If

        Dim rv As Microsoft.Reporting.WinForms.ReportViewer
        Dim actualUrl As String = ""

        Try
            AppStatus(True)

            oRpt = New rsClients.rsClient(txtUrl.Text)

            rv = New Microsoft.Reporting.WinForms.ReportViewer

            With oRpt
                .Url = txtUrl.Text

                For x As Integer = 0 To txtUrl.Text.Split("/").GetUpperBound(0) - 1
                    actualUrl &= txtUrl.Text.Split("/")(x) & "/"
                Next

                rv.ProcessingMode = ProcessingMode.Remote
                rv.ServerReport.ReportServerUrl = New Uri(actualUrl)

                If ServerPassword.Length = 0 And ServerUser.Length = 0 Then
                    .Credentials = System.Net.CredentialCache.DefaultCredentials
                    rv.ServerReport.ReportServerCredentials.NetworkCredentials = System.Net.CredentialCache.DefaultCredentials

                Else
                    Dim userDomain As String = ""
                    Dim tmpUser As String = ServerUser

                    getDomainAndUserFromString(tmpUser, userDomain)

                    Dim oCred As System.Net.NetworkCredential = New System.Net.NetworkCredential(tmpUser, ServerPassword, userDomain)

                    .Credentials = oCred

                    rv.ServerReport.ReportServerCredentials.NetworkCredentials = oCred

                    Try
                        If formsAuth Then rv.ServerReport.ReportServerCredentials.SetFormsCredentials(Nothing, tmpUser, ServerPassword, userDomain)
                    Catch : End Try
                End If
            End With

            Dim serverVersion As String = "2000"

            Try
                serverVersion = rv.ServerReport.GetServerVersion
                rv.ServerReport.ReportPath = Me.txtDBLocation.Text
            Catch ex As Exception
                serverVersion = "2000"
                rv = Nothing
            End Try

            Dim oPar() As ReportServer.ParameterValue = Nothing
            Dim reportPars() As Microsoft.Reporting.WinForms.ReportParameter = Nothing
            Dim oDataLogins() As ReportServer.DataSourceCredentials = Nothing
            Dim reportDS() As Microsoft.Reporting.WinForms.DataSourceCredentials
            Dim I As Integer = 0
            Dim oData As New clsMarsData

            'set the parameters
            Dim oParse As New clsMarsParser
            Dim rpt As clsMarsReport = New clsMarsReport

            For Each oItem As ListViewItem In parameterController.lsvParameters.Items
                If oItem.Tag Is Nothing Then Continue For

                Dim obj = oItem.Tag

                If TypeOf obj Is System.Collections.Generic.KeyValuePair(Of String, String) Then
                    Dim setvalue As String = CType(obj, KeyValuePair(Of String, String)).Value

                    If setvalue.ToLower = "[sql-rddefault]" Then
                        Continue For
                    End If

                    ReDim Preserve oPar(I)
                    ReDim Preserve reportPars(I)

                    oPar(I) = New ReportServer.ParameterValue
                    oPar(I).Name = oItem.Text

                    reportPars(I) = New Microsoft.Reporting.WinForms.ReportParameter
                    reportPars(I).Name = oItem.Text

                    If setvalue.ToLower = "[sql-rdnull]" Then
                        oPar(I) = Nothing
                    Else
                        reportPars(I).Values.Add(clsMarsParser.Parser.ParseString(setvalue))
                        oPar(I).Value = setvalue
                    End If
                Else

                    For Each kvp As KeyValuePair(Of String, String) In obj
                        Dim setvalue As String = kvp.Value

                        '//remove the blank index
                        If setvalue.ToLower = "[sql-rddefault]" Then
                            Continue For
                        End If

                        If reportPars Is Nothing OrElse reportPars.GetUpperBound(0) < I Then '//initialize this parameter if its blank
                            ReDim Preserve oPar(I)
                            ReDim Preserve reportPars(I)

                            oPar(I) = New ReportServer.ParameterValue
                            oPar(I).Name = oItem.Text

                            reportPars(I) = New Microsoft.Reporting.WinForms.ReportParameter
                            reportPars(I).Name = oItem.Text
                        End If

                        If setvalue.ToLower = "[sql-rdnull]" Then
                            oPar(I) = Nothing
                        ElseIf setvalue.ToLower = "[selectall]" Then
                            For Each p As ReportServer.ReportParameter In parameterController.parameters
                                If String.Compare(p.Name, oItem.Text, True) = 0 Then
                                    If p.ValidValues IsNot Nothing Then

                                        For Each v As ReportServer.ValidValue In p.ValidValues
                                            reportPars(I).Values.Add(v.Value)
                                        Next
                                    End If

                                    Exit For
                                End If
                            Next
                        Else
                            reportPars(I).Values.Add(clsMarsParser.Parser.ParseString(setvalue))
                            oPar(I).Value = setvalue
                        End If
                    Next

                End If

                I += 1
            Next

            If rv IsNot Nothing And reportPars IsNot Nothing Then rv.ServerReport.SetParameters(reportPars)

            I = 0

            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM ReportDatasource WHERE ReportID = " & 99999)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    ReDim Preserve oDataLogins(I)
                    ReDim Preserve reportDS(I)

                    oDataLogins(I) = New ReportServer.DataSourceCredentials
                    reportDS(I) = New Microsoft.Reporting.WinForms.DataSourceCredentials

                    With oDataLogins(I)
                        .DataSourceName = oRs("datasourcename").Value
                        .UserName = oRs("rptuserid").Value
                        .Password = _DecryptDBValue(IsNull(oRs("rptpassword").Value))
                    End With

                    With reportDS(I)
                        .Name = oRs("datasourcename").Value
                        .UserId = oRs("rptuserid").Value
                        .Password = _DecryptDBValue(IsNull(oRs("rptpassword").Value))
                    End With

                    I += 1
                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            If rv IsNot Nothing And reportDS IsNot Nothing Then rv.ServerReport.SetDataSourceCredentials(reportDS)

            If serverVersion = "2000" Then
                Dim warnings As ReportServer.Warning() = Nothing
                Dim streamIDs As String() = Nothing

                Dim oResult As Byte()

                oResult = oRpt.Render(Me.txtDBLocation.Text, "PDF", Nothing, "", _
                    oPar, oDataLogins, Nothing, "", "", Nothing, _
                    warnings, streamIDs)

                Dim sFileName As String = sAppPath & clsMarsData.CreateDataID & ".pdf"

                'write the bytes to disk
                With System.IO.File.Create(sFileName, oResult.Length)
                    .Write(oResult, 0, oResult.Length)
                    .Close()
                End With

                'view the exported file
                Dim oView As New frmPreview

                AppStatus(False)

                oView.PreviewReport(sFileName, txtDBLocation.Text, True)

                Try : IO.File.Delete(sFileName) : Catch : End Try
            Else
                Dim oView As New frmPreview

                oView.PreviewReport(rv, txtDBLocation.Text, True)
            End If
        Catch ex As Exception
            AppStatus(False)
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace), "Please check your datasource credentials and try again")
        End Try
    End Sub

    'Private Sub cmdRequery_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRequery.Click
    '    If (MessageBox.Show("Requery the report for parameters? This will remove the current parameters and respective values", _
    '    Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes) Then

    '        Dim logFile As String = "parameter_requery.log"
    '        '//store the parameters and their values
    '        Dim dtCache As DataTable = New DataTable
    '        dtCache.Columns.Add("parname")
    '        dtCache.Columns.Add("parvalue")
    '        dtCache.Columns.Add("multivalue")

    '        clsMarsDebug.writeToDebug(logFile, "Saving current parameters", False)

    '        For Each it As ucParameters In UcParsList.m_ParameterControlsCollection
    '            Dim r As DataRow = dtCache.Rows.Add
    '            r("parname") = it.m_parameterName
    '            r("parvalue") = it.m_parameterValue
    '            r("multivalue") = it.m_allowMultipleValues
    '        Next

    '        clsMarsDebug.writeToDebug(logFile, "Getting server version", True)

    '        clsMarsData.WriteData("DELETE FROM ReportParameter WHERE ReportID =" & Me.ReportID)

    '        If clsMarsReport.m_serverVersion(txtUrl.Text, ServerUser, ServerPassword, formsAuth) = "2000" Then
    '            clsMarsReport.GetReportParameters(txtUrl.Text, ServerUser, ServerPassword, txtDBLocation.Text, txtReportName.Text, True, ParDefaults, Nothing, formsAuth)
    '        Else
    '            clsMarsReport.GetReportParameters(txtUrl.Text, ServerUser, ServerPassword, txtDBLocation.Text, txtReportName.Text, True, , formsAuth)
    '        End If

    '        With UcParsList
    '            .m_rdlPath = txtDBLocation.Text
    '            .m_serverUrl = txtUrl.Text
    '            .m_serverUser = ServerUser
    '            .m_serverPassword = ServerPassword
    '            .m_formsAuth = formsAuth
    '            .loadParameters(formsAuth)
    '        End With

    '        clsMarsDebug.writeToDebug(logFile, "Resetting values", True)

    '        For Each ctrl As ucParameters In UcParsList.Controls
    '            Dim name As String = ctrl.m_parameterName
    '            Dim rows() As DataRow = dtCache.Select("parname = '" & SQLPrepare(Name) & "'")

    '            Dim hsT As Hashtable = New Hashtable

    '            If rows IsNot Nothing AndAlso rows.Length > 0 Then
    '                Dim row As DataRow = rows(0)

    '                ctrl.m_parameterValue = row("parvalue")
    '            End If
    '        Next
    '    End If
    'End Sub

    Private Sub cmdRequery_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRequery.Click
         If MessageBox.Show("Requery the report for parameters? This will remove the current parameters and respective values", _
       Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim logFile As String = "parameter_requery.log"
            '//store the parameters and their values
            Dim dtCache As Hashtable = New Hashtable

            clsMarsDebug.writeToDebug(logFile, "Saving current parameters", False)

            For Each it As ListViewItem In parameterController.lsvParameters.Items
                If dtCache.ContainsKey(it.Text) = False Then
                    dtCache.Add(it.Text, it.Tag)
                End If
            Next

            clsMarsDebug.writeToDebug(logFile, "Deleting current values", True)

            clsMarsData.WriteData("DELETE FROM ReportParameter WHERE ReportID =" & ReportID)

            clsMarsDebug.writeToDebug(logFile, "Getting server version", True)

            parameterController.loadParameters(txtUrl.Text, txtDBLocation.Text, Nothing, ServerUser, ServerPassword, formsAuth)

            ' UcBlank.m_ParametersList = New ArrayList

            '//reset their values
            clsMarsDebug.writeToDebug(logFile, "Resetting values", True)

            For Each item As ListViewItem In parameterController.lsvParameters.Items
                Dim name As String = item.Text


                If dtCache.ContainsKey(name) Then
                    Dim obj = dtCache.Item(name)

                    If item.SubItems(2).Text.ToLower = "true" Then '//is it multivalue??
                        If TypeOf obj Is List(Of KeyValuePair(Of String, String)) Then
                            item.Tag = obj
                        Else
                            item.Tag = Nothing
                        End If
                    Else
                        If TypeOf obj Is KeyValuePair(Of String, String) Then
                            item.Tag = obj
                        Else
                            item.Tag = Nothing
                        End If
                    End If
                End If
            Next
        End If


    End Sub
    Private Sub tbPack_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case tbPack.SelectedTabIndex
            Case 3
                'add datasources from the report that have default credentials in case user wants to set them up
                If txtDBLocation.Text.Length > 0 And txtUrl.Text.Length > 0 And m_Mode = "Add" Then
                    clsMarsReport.GetReportDatasources(txtUrl.Text, ServerUser, ServerPassword, txtDBLocation.Text, txtReportName.Text, True, Me.lsvDatasources, formsAuth)
                End If
        End Select
    End Sub

    Private Sub txtUrl_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUrl.GotFocus
        If txtUrl.ForeColor = Color.Gray Then
            txtUrl.Text = ""
            txtUrl.ForeColor = Color.Blue
        End If
    End Sub

    Private Sub lsvParameters_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub



    Private Sub ClearToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClearToolStripMenuItem.Click
        If lsvDatasources.SelectedItems.Count = 0 Then Return
        Dim oData As New clsMarsData

        Dim oItem As ListViewItem = lsvDatasources.SelectedItems(0)

        Dim nDataID As Integer = oItem.Tag

        If nDataID > 0 Then
            If clsMarsData.WriteData("DELETE FROM ReportDatasource WHERE DatasourceID =" & nDataID) = True Then
                oItem.SubItems(1).Text = "default"
            End If
        End If
    End Sub

    Private Sub lsvDatasources_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lsvDatasources.KeyUp
        If e.KeyCode = Keys.Enter Then
            Me.lsvDatasources_DoubleClick(sender, e)
        End If
    End Sub

    Private Sub btnRefreshDS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshDS.Click
        lsvDatasources.Items.Clear()

        clsMarsReport.GetReportDatasources(txtUrl.Text, ServerUser, ServerPassword, txtDBLocation.Text, txtReportName.Text, True, Me.lsvDatasources, formsAuth)

        clsMarsData.WriteData("DELETE FROM ReportDatasource WHERE reportid =" & ReportID)
    End Sub


    Private Sub chkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkStatus.CheckedChanged

    End Sub

    Private Sub txtDBLocation_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) 'Handles txtDBLocation.TextChanged
        If txtDBLocation.Text.Length > 0 And showTooltip = True Then
            Dim info As New DevComponents.DotNetBar.SuperTooltipInfo
            Dim msg As String

            msg = "Please be aware that by entering the report path manually, SQL-RD will not " & _
            "connect to your report server to list your reports. " & vbCrLf & _
            "If you would like SQL-RD to " & _
            "connect to the report server and list your reports, remove all text from this field."

            With info
                .Color = DevComponents.DotNetBar.eTooltipColor.Lemon
                .CustomSize = New System.Drawing.Size(400, 100)
                .BodyText = msg
                .HeaderText = "For your information:"
                .HeaderVisible = True
                .FooterVisible = False
            End With

            Me.SuperTooltip1.SetSuperTooltip(Me.cmdDBLoc, info)

            SuperTooltip1.ShowTooltip(Me.cmdDBLoc)

            'Me.cmdDBLoc.Image = Me.ImageList1.Images(1)
            '  Me.cmdDBLoc.ImageAlign = ContentAlignment.MiddleCenter

            showTooltip = False
        ElseIf Me.txtDBLocation.Text.Length = 0 Then
            SuperTooltip1.SetSuperTooltip(Me.cmdDBLoc, Nothing)

            'Me.cmdDBLoc.Image = ImageList1.Images(2)
            '  Me.cmdDBLoc.ImageAlign = ContentAlignment.MiddleCenter

            showTooltip = True
        End If

        cmdOK.Enabled = False
    End Sub

    Private Sub cmdLoginTest2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoginTest2.Click
        cmdLoginTest_Click(Nothing, Nothing)
    End Sub

    Private Sub bgWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgWorker.DoWork
        m_serverParametersTable = clsMarsReport.oReport.getserverReportParameters(Me.txtUrl.Text, Me.txtDBLocation.Text, _
        ServerUser, ServerPassword, "", formsAuth)
        Me.GetParameterAvailableValues()
    End Sub


    Private Sub txtUrl_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUrl.TextChanged
        m_serverUrl = txtUrl.Text
    End Sub

    Private Sub changeNamingName(sender As System.Object, e As System.EventArgs)
        Dim sTitle As String = txtReportName.Text

        If optNaming.Checked = True And sTitle.Length > 0 And ucReportOptions.cmbFormat.Text.IndexOf("*") > -1 Then
            txtDefault.Text = sTitle & ucReportOptions.cmbFormat.Text.Split("*")(1).Replace(")", String.Empty).Trim
        End If
    End Sub

    Dim m_inserter As frmInserter

    Private Sub tbPack_SelectedTabChanged(sender As Object, e As DevComponents.DotNetBar.SuperTabStripSelectedTabChangedEventArgs) Handles tbPack.SelectedTabChanged
        If m_EventReport = False Then
            Select Case e.NewValue.Text.ToLower
                Case "parameters", "naming", "report", "naming (optional)"
                    If m_inserter IsNot Nothing AndAlso m_inserter.IsDisposed = False Then
                        m_inserter.GetConstants(Me)
                    Else
                        m_inserter = New frmInserter(m_eventID)

                        m_inserter.GetConstants(Me)
                    End If

                    If e.NewValue.Text.ToLower = "parameters" Then
                        If parameterController.numberOfParameters = 0 Then
                            If m_Mode.ToUpper = "ADD" Then
                                parameterController.loadParameters(txtUrl.Text, txtDBLocation.Text, Nothing, ServerUser, ServerPassword, formsAuth)
                            Else
                                parameterController.loadParameters(ReportID, txtUrl.Text, txtDBLocation.Text, Nothing, ServerUser, ServerPassword, formsAuth)
                            End If
                        End If
                    End If
                Case Else
                    If m_inserter IsNot Nothing Then m_inserter.Hide()
            End Select
        Else
            Select Case e.NewValue.Text.ToLower
                Case "parameters", "naming (optional)"
                    If m_inserter IsNot Nothing AndAlso m_inserter.IsDisposed = False Then
                        m_inserter.GetConstants(Me)
                    Else
                        m_inserter = New frmInserter(m_eventID)

                        m_inserter.GetConstants(Me)
                    End If

                    If e.NewValue.Text.ToLower = "parameters" Then
                        If parameterController.numberOfParameters = 0 Then
                            If m_Mode.ToUpper = "ADD" Then
                                parameterController.loadParameters(txtUrl.Text, txtDBLocation.Text, Nothing, ServerUser, ServerPassword, formsAuth)
                            Else
                                parameterController.loadParameters(ReportID, txtUrl.Text, txtDBLocation.Text, Nothing, ServerUser, ServerPassword, formsAuth)
                            End If
                        End If
                    End If
                Case Else
                    If m_inserter IsNot Nothing Then m_inserter.Hide()
            End Select
        End If
    End Sub

    Private Sub lsvDatasources_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lsvDatasources.SelectedIndexChanged

    End Sub
End Class
