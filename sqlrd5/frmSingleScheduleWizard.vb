Imports System.Drawing.Printing
Imports Microsoft.Win32
Imports Microsoft.Reporting.WinForms
Imports System.Collections.Generic
#If CRYSTAL_VER = 8 Then
imports My.Crystal85
#ElseIf CRYSTAL_VER = 9 Then
imports My.Crystal9 
#ElseIf CRYSTAL_VER = 10 Then
imports My.Crystal10 
#ElseIf CRYSTAL_VER = 11 Then
Imports My.Crystal11
#End If
Friend Class frmSingleScheduleWizard
    Inherits DevComponents.DotNetBar.Office2007Form
    '#Const CRYSTAL_VER = 9
    Dim nStep As Integer
    Dim sFrequency As String
    Dim oErr As clsMarsUI = New clsMarsUI
    Dim oMsg As clsMarsMessaging = New clsMarsMessaging
    Dim oRpt As Object 'ReportServer.ReportingService
    Dim ReportRestore() As Boolean
    Dim oUI As New clsMarsUI
    Dim ServerUser As String = ""
    Dim ServerPassword As String = ""
    Dim formsAuth As Boolean
    Dim ParDefaults As Hashtable
    Dim dtParameters As DataTable
    Dim showTooltip As Boolean = True
    Dim m_serverParametersTable As DataTable
    Dim m_inserter As frmInserter = New frmInserter(0)
    Const S1 As String = "Step 1: Report Setup"
    Const S2 As String = "Step 2: Schedule Setup"
    Const S3 As String = "Step 3: Destination Setup"
    Const S4 As String = "Step 4: Report Parameters"
    Const S5 As String = "Step 5: Report Options"
    Const S6 As String = "Step 6: Exception Handling"
    Const S7 As String = "Step 7: Custom Tasks"

    Dim HasCancelled As Boolean = False
    Friend WithEvents txtUrl As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lsvDatasources As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    'Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtSnapshots As System.Windows.Forms.NumericUpDown
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents mnuDatasources As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ClearToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents UcError As sqlrd.ucErrorHandler
    Friend WithEvents historyStrip As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents stabMain As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel7 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabControlPanel6 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabException As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel5 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabDestinations As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabReportOptions As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabReport As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabSchedule As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabGeneral As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents Step4 As System.Windows.Forms.Panel
    Friend WithEvents txtFormula As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents SuperTabControlPanel8 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabCustomTasks As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents ReflectionImage1 As DevComponents.DotNetBar.Controls.ReflectionImage
    Friend WithEvents ucSet As sqlrd.ucSchedule
    Friend WithEvents UcBlankReportX1 As sqlrd.ucBlankReportX
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnTest2 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents parameterController As sqlrd.ucParameterController
    Friend WithEvents chkSnapshots As System.Windows.Forms.CheckBox


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Step1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtFolder As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdLoc As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtDBLoc As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdDbLoc As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents cmdNext As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Step2 As System.Windows.Forms.Panel
    Friend WithEvents fbg As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents Step3 As System.Windows.Forms.Panel
    Friend WithEvents ErrProv As System.Windows.Forms.ErrorProvider
    Friend WithEvents cmdFinish As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents oTask As sqlrd.ucTasks
    Friend WithEvents mnuContacts As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuMAPI As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMARS As System.Windows.Forms.MenuItem
    Friend WithEvents txtDesc As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtKeyWord As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents UcDest As sqlrd.ucDestination
    Friend WithEvents UcTasks As sqlrd.ucTasks
    Friend WithEvents Step6 As System.Windows.Forms.Panel
    Friend WithEvents Step7 As System.Windows.Forms.Panel
    Friend WithEvents Step5 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdLoginTest As DevComponents.DotNetBar.ButtonX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSingleScheduleWizard))
        Me.Step1 = New System.Windows.Forms.Panel()
        Me.ReflectionImage1 = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.txtUrl = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtDesc = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdDbLoc = New DevComponents.DotNetBar.ButtonX()
        Me.txtDBLoc = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdLoc = New DevComponents.DotNetBar.ButtonX()
        Me.txtFolder = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtKeyWord = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.ofd = New System.Windows.Forms.OpenFileDialog()
        Me.cmdNext = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.Step2 = New System.Windows.Forms.Panel()
        Me.ucSet = New sqlrd.ucSchedule()
        Me.Step3 = New System.Windows.Forms.Panel()
        Me.UcDest = New sqlrd.ucDestination()
        Me.fbg = New System.Windows.Forms.FolderBrowserDialog()
        Me.Step6 = New System.Windows.Forms.Panel()
        Me.UcBlankReportX1 = New sqlrd.ucBlankReportX()
        Me.UcError = New sqlrd.ucErrorHandler()
        Me.ErrProv = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cmdFinish = New DevComponents.DotNetBar.ButtonX()
        Me.Step7 = New System.Windows.Forms.Panel()
        Me.UcTasks = New sqlrd.ucTasks()
        Me.Step5 = New System.Windows.Forms.Panel()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtSnapshots = New System.Windows.Forms.NumericUpDown()
        Me.chkSnapshots = New System.Windows.Forms.CheckBox()
        Me.lsvDatasources = New System.Windows.Forms.ListView()
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.mnuDatasources = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ClearToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmdLoginTest = New DevComponents.DotNetBar.ButtonX()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.mnuContacts = New System.Windows.Forms.ContextMenu()
        Me.mnuMAPI = New System.Windows.Forms.MenuItem()
        Me.mnuMARS = New System.Windows.Forms.MenuItem()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider()
        Me.historyStrip = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.stabMain = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.Step4 = New System.Windows.Forms.Panel()
        Me.parameterController = New sqlrd.ucParameterController()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnTest2 = New DevComponents.DotNetBar.ButtonX()
        Me.txtFormula = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.tabReport = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabGeneral = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabReportOptions = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel6 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabException = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel5 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabDestinations = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabSchedule = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel8 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabCustomTasks = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel7 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Step1.SuspendLayout()
        Me.Step2.SuspendLayout()
        Me.Step3.SuspendLayout()
        Me.Step6.SuspendLayout()
        CType(Me.ErrProv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step7.SuspendLayout()
        Me.Step5.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.txtSnapshots, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mnuDatasources.SuspendLayout()
        CType(Me.stabMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabMain.SuspendLayout()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.Step4.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel4.SuspendLayout()
        Me.SuperTabControlPanel6.SuspendLayout()
        Me.SuperTabControlPanel5.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuperTabControlPanel8.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Step1
        '
        Me.Step1.BackColor = System.Drawing.Color.White
        Me.Step1.Controls.Add(Me.ReflectionImage1)
        Me.Step1.Controls.Add(Me.txtUrl)
        Me.Step1.Controls.Add(Me.Label6)
        Me.Step1.Controls.Add(Me.txtDesc)
        Me.Step1.Controls.Add(Me.cmdDbLoc)
        Me.Step1.Controls.Add(Me.txtDBLoc)
        Me.Step1.Controls.Add(Me.cmdLoc)
        Me.Step1.Controls.Add(Me.txtFolder)
        Me.Step1.Controls.Add(Me.Label4)
        Me.Step1.Controls.Add(Me.Label3)
        Me.Step1.Controls.Add(Me.txtName)
        Me.Step1.Controls.Add(Me.Label2)
        Me.Step1.Controls.Add(Me.Label1)
        Me.Step1.Controls.Add(Me.Label7)
        Me.Step1.Controls.Add(Me.txtKeyWord)
        Me.Step1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.HelpProvider1.SetHelpKeyword(Me.Step1, "Single_Report_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.Step1, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step1.Location = New System.Drawing.Point(0, 0)
        Me.Step1.Name = "Step1"
        Me.HelpProvider1.SetShowHelp(Me.Step1, True)
        Me.Step1.Size = New System.Drawing.Size(615, 450)
        Me.Step1.TabIndex = 0
        '
        'ReflectionImage1
        '
        Me.ReflectionImage1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.ReflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ReflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.ReflectionImage1.Image = CType(resources.GetObject("ReflectionImage1.Image"), System.Drawing.Image)
        Me.ReflectionImage1.Location = New System.Drawing.Point(483, 132)
        Me.ReflectionImage1.Name = "ReflectionImage1"
        Me.ReflectionImage1.Size = New System.Drawing.Size(128, 196)
        Me.ReflectionImage1.TabIndex = 25
        '
        'txtUrl
        '
        Me.txtUrl.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtUrl.Border.Class = "TextBoxBorder"
        Me.txtUrl.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUrl.DisabledBackColor = System.Drawing.Color.White
        Me.txtUrl.ForeColor = System.Drawing.Color.Blue
        Me.txtUrl.Location = New System.Drawing.Point(117, 51)
        Me.txtUrl.Name = "txtUrl"
        Me.txtUrl.Size = New System.Drawing.Size(357, 21)
        Me.SuperTooltip1.SetSuperTooltip(Me.txtUrl, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Enter the address for the report server's web service e.g. http://myreportserver/" & _
            "reportserver/reportservice.asmx", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, True, True, New System.Drawing.Size(320, 85)))
        Me.txtUrl.TabIndex = 2
        Me.txtUrl.Tag = "memo"
        Me.txtUrl.Text = "http://myReportServer/ReportServer/"
        '
        'Label6
        '
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(7, 53)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(108, 25)
        Me.Label6.TabIndex = 24
        Me.Label6.Text = "Report  Service URL"
        '
        'txtDesc
        '
        Me.txtDesc.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDesc.Border.Class = "TextBoxBorder"
        Me.txtDesc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDesc.DisabledBackColor = System.Drawing.Color.White
        Me.txtDesc.ForeColor = System.Drawing.Color.Black
        Me.txtDesc.Location = New System.Drawing.Point(117, 132)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDesc.Size = New System.Drawing.Size(358, 128)
        Me.txtDesc.TabIndex = 6
        Me.txtDesc.Tag = "memo"
        '
        'cmdDbLoc
        '
        Me.cmdDbLoc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdDbLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDbLoc.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdDbLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDbLoc.Location = New System.Drawing.Point(480, 78)
        Me.cmdDbLoc.Name = "cmdDbLoc"
        Me.cmdDbLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdDbLoc.TabIndex = 4
        Me.cmdDbLoc.Text = "..."
        '
        'txtDBLoc
        '
        Me.txtDBLoc.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDBLoc.Border.Class = "TextBoxBorder"
        Me.txtDBLoc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDBLoc.DisabledBackColor = System.Drawing.Color.White
        Me.txtDBLoc.Enabled = False
        Me.txtDBLoc.ForeColor = System.Drawing.Color.Blue
        Me.txtDBLoc.Location = New System.Drawing.Point(117, 78)
        Me.txtDBLoc.Name = "txtDBLoc"
        Me.txtDBLoc.ReadOnly = True
        Me.txtDBLoc.Size = New System.Drawing.Size(357, 21)
        Me.txtDBLoc.TabIndex = 3
        '
        'cmdLoc
        '
        Me.cmdLoc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdLoc.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoc.Location = New System.Drawing.Point(480, 24)
        Me.cmdLoc.Name = "cmdLoc"
        Me.cmdLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdLoc.TabIndex = 1
        Me.cmdLoc.Text = "...."
        Me.ToolTip.SetToolTip(Me.cmdLoc, "Browse")
        '
        'txtFolder
        '
        Me.txtFolder.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtFolder.Border.Class = "TextBoxBorder"
        Me.txtFolder.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFolder.DisabledBackColor = System.Drawing.Color.White
        Me.txtFolder.ForeColor = System.Drawing.Color.Blue
        Me.txtFolder.Location = New System.Drawing.Point(117, 24)
        Me.txtFolder.Name = "txtFolder"
        Me.txtFolder.ReadOnly = True
        Me.txtFolder.Size = New System.Drawing.Size(357, 21)
        Me.txtFolder.TabIndex = 0
        Me.txtFolder.Tag = "1"
        '
        'Label4
        '
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(7, 26)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(108, 25)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Create In"
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(7, 78)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(108, 25)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Report  Location"
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.DisabledBackColor = System.Drawing.Color.White
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(117, 105)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(358, 21)
        Me.txtName.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(7, 105)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(108, 25)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Schedule Name"
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(7, 130)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(108, 25)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Description (optional)"
        '
        'Label7
        '
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(7, 266)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(107, 21)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Keyword (optional)"
        '
        'txtKeyWord
        '
        Me.txtKeyWord.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtKeyWord.Border.Class = "TextBoxBorder"
        Me.txtKeyWord.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKeyWord.DisabledBackColor = System.Drawing.Color.White
        Me.txtKeyWord.ForeColor = System.Drawing.Color.Blue
        Me.txtKeyWord.Location = New System.Drawing.Point(117, 266)
        Me.txtKeyWord.Name = "txtKeyWord"
        Me.txtKeyWord.Size = New System.Drawing.Size(360, 21)
        Me.txtKeyWord.TabIndex = 7
        Me.txtKeyWord.Tag = "memo"
        '
        'ofd
        '
        Me.ofd.DefaultExt = "*.mdb"
        Me.ofd.Filter = "Report Definition File|*.rdl|All Files|*.*"
        '
        'cmdNext
        '
        Me.cmdNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdNext.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdNext.Enabled = False
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(589, 3)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(75, 25)
        Me.cmdNext.TabIndex = 49
        Me.cmdNext.Text = "&Next"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(508, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 25)
        Me.cmdCancel.TabIndex = 48
        Me.cmdCancel.Text = "&Cancel"
        '
        'Step2
        '
        Me.Step2.BackColor = System.Drawing.Color.White
        Me.Step2.Controls.Add(Me.ucSet)
        Me.Step2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.HelpProvider1.SetHelpKeyword(Me.Step2, "Single_Report_Schedule.htm#Step2")
        Me.HelpProvider1.SetHelpNavigator(Me.Step2, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step2.Location = New System.Drawing.Point(0, 0)
        Me.Step2.Name = "Step2"
        Me.HelpProvider1.SetShowHelp(Me.Step2, True)
        Me.Step2.Size = New System.Drawing.Size(615, 450)
        Me.Step2.TabIndex = 0
        '
        'ucSet
        '
        Me.ucSet.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ucSet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ucSet.Location = New System.Drawing.Point(0, 0)
        Me.ucSet.m_collaborationServerID = 0
        Me.ucSet.m_nextRun = "2014-07-28 14:42:06"
        Me.ucSet.m_RepeatUnit = ""
        Me.ucSet.mode = sqlrd.ucSchedule.modeEnum.SETUP
        Me.ucSet.Name = "ucSet"
        Me.ucSet.scheduleID = 0
        Me.ucSet.scheduleStatus = True
        Me.ucSet.sFrequency = "Daily"
        Me.ucSet.Size = New System.Drawing.Size(615, 450)
        Me.ucSet.TabIndex = 0
        '
        'Step3
        '
        Me.Step3.BackColor = System.Drawing.Color.White
        Me.Step3.Controls.Add(Me.UcDest)
        Me.Step3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Step3.Location = New System.Drawing.Point(0, 0)
        Me.Step3.Name = "Step3"
        Me.Step3.Size = New System.Drawing.Size(615, 450)
        Me.Step3.TabIndex = 8
        '
        'UcDest
        '
        Me.UcDest.BackColor = System.Drawing.Color.Transparent
        Me.UcDest.destinationsPicker = False
        Me.UcDest.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcDest.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.HelpProvider1.SetHelpKeyword(Me.UcDest, "Single_Report_Schedule.htm#Step3")
        Me.HelpProvider1.SetHelpNavigator(Me.UcDest, System.Windows.Forms.HelpNavigator.Topic)
        Me.UcDest.Location = New System.Drawing.Point(0, 0)
        Me.UcDest.m_CanDisable = True
        Me.UcDest.m_DelayDelete = False
        Me.UcDest.m_eventBased = False
        Me.UcDest.m_ExcelBurst = False
        Me.UcDest.m_IsDynamic = False
        Me.UcDest.m_isPackage = False
        Me.UcDest.m_IsQuery = False
        Me.UcDest.m_StaticDest = False
        Me.UcDest.Name = "UcDest"
        Me.HelpProvider1.SetShowHelp(Me.UcDest, True)
        Me.UcDest.Size = New System.Drawing.Size(615, 450)
        Me.UcDest.TabIndex = 0
        '
        'Step6
        '
        Me.Step6.BackColor = System.Drawing.Color.White
        Me.Step6.Controls.Add(Me.UcBlankReportX1)
        Me.Step6.Controls.Add(Me.UcError)
        Me.Step6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Step6.Location = New System.Drawing.Point(0, 0)
        Me.Step6.Name = "Step6"
        Me.Step6.Size = New System.Drawing.Size(615, 450)
        Me.Step6.TabIndex = 10
        '
        'UcBlankReportX1
        '
        Me.UcBlankReportX1.BackColor = System.Drawing.Color.Transparent
        Me.UcBlankReportX1.blankID = 0
        Me.UcBlankReportX1.blankType = "AlertandReport"
        Me.UcBlankReportX1.Location = New System.Drawing.Point(3, 78)
        Me.UcBlankReportX1.m_customDSN = ""
        Me.UcBlankReportX1.m_customPassword = ""
        Me.UcBlankReportX1.m_customUserID = ""
        Me.UcBlankReportX1.m_showAllReportsBlankForTasks = False
        Me.UcBlankReportX1.Name = "UcBlankReportX1"
        Me.UcBlankReportX1.Size = New System.Drawing.Size(591, 366)
        Me.UcBlankReportX1.TabIndex = 5
        '
        'UcError
        '
        Me.UcError.BackColor = System.Drawing.Color.Transparent
        Me.UcError.Location = New System.Drawing.Point(7, 7)
        Me.UcError.m_showFailOnOne = False
        Me.UcError.Name = "UcError"
        Me.UcError.Size = New System.Drawing.Size(430, 65)
        Me.UcError.TabIndex = 4
        '
        'ErrProv
        '
        Me.ErrProv.ContainerControl = Me
        '
        'cmdFinish
        '
        Me.cmdFinish.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdFinish.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdFinish.Enabled = False
        Me.cmdFinish.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdFinish.Location = New System.Drawing.Point(670, 3)
        Me.cmdFinish.Name = "cmdFinish"
        Me.cmdFinish.Size = New System.Drawing.Size(75, 25)
        Me.cmdFinish.TabIndex = 50
        Me.cmdFinish.Text = "&Finish"
        '
        'Step7
        '
        Me.Step7.BackColor = System.Drawing.Color.White
        Me.Step7.Controls.Add(Me.UcTasks)
        Me.Step7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Step7.Location = New System.Drawing.Point(0, 0)
        Me.Step7.Name = "Step7"
        Me.Step7.Size = New System.Drawing.Size(615, 450)
        Me.Step7.TabIndex = 0
        '
        'UcTasks
        '
        Me.UcTasks.BackColor = System.Drawing.Color.White
        Me.UcTasks.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcTasks.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcTasks.ForeColor = System.Drawing.Color.Navy
        Me.HelpProvider1.SetHelpKeyword(Me.UcTasks, "Single_Report_Schedule.htm#Step7")
        Me.HelpProvider1.SetHelpNavigator(Me.UcTasks, System.Windows.Forms.HelpNavigator.Topic)
        Me.UcTasks.Location = New System.Drawing.Point(0, 0)
        Me.UcTasks.m_defaultTaks = False
        Me.UcTasks.m_eventBased = False
        Me.UcTasks.m_eventID = 99999
        Me.UcTasks.m_forExceptionHandling = False
        Me.UcTasks.m_showAfterType = True
        Me.UcTasks.m_showExpanded = True
        Me.UcTasks.Name = "UcTasks"
        Me.HelpProvider1.SetShowHelp(Me.UcTasks, True)
        Me.UcTasks.Size = New System.Drawing.Size(615, 450)
        Me.UcTasks.TabIndex = 0
        '
        'Step5
        '
        Me.Step5.BackColor = System.Drawing.Color.White
        Me.Step5.Controls.Add(Me.GroupBox5)
        Me.Step5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Step5.Location = New System.Drawing.Point(0, 0)
        Me.Step5.Name = "Step5"
        Me.Step5.Size = New System.Drawing.Size(615, 450)
        Me.Step5.TabIndex = 16
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label9)
        Me.GroupBox5.Controls.Add(Me.txtSnapshots)
        Me.GroupBox5.Controls.Add(Me.chkSnapshots)
        Me.GroupBox5.Controls.Add(Me.lsvDatasources)
        Me.GroupBox5.Controls.Add(Me.cmdLoginTest)
        Me.GroupBox5.Location = New System.Drawing.Point(5, 7)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(604, 437)
        Me.GroupBox5.TabIndex = 0
        Me.GroupBox5.TabStop = False
        '
        'Label9
        '
        Me.HelpProvider1.SetHelpKeyword(Me.Label9, "Single_Report_Schedule.htm#Step5")
        Me.HelpProvider1.SetHelpNavigator(Me.Label9, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(320, 409)
        Me.Label9.Name = "Label9"
        Me.HelpProvider1.SetShowHelp(Me.Label9, True)
        Me.Label9.Size = New System.Drawing.Size(80, 16)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "Days"
        '
        'txtSnapshots
        '
        Me.txtSnapshots.Enabled = False
        Me.HelpProvider1.SetHelpKeyword(Me.txtSnapshots, "Single_Report_Schedule.htm#Step5")
        Me.HelpProvider1.SetHelpNavigator(Me.txtSnapshots, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtSnapshots.Location = New System.Drawing.Point(256, 407)
        Me.txtSnapshots.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.txtSnapshots.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtSnapshots.Name = "txtSnapshots"
        Me.HelpProvider1.SetShowHelp(Me.txtSnapshots, True)
        Me.txtSnapshots.Size = New System.Drawing.Size(56, 21)
        Me.txtSnapshots.TabIndex = 1
        Me.txtSnapshots.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSnapshots.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'chkSnapshots
        '
        Me.HelpProvider1.SetHelpKeyword(Me.chkSnapshots, "Single_Report_Schedule.htm#Step5")
        Me.HelpProvider1.SetHelpNavigator(Me.chkSnapshots, System.Windows.Forms.HelpNavigator.Topic)
        Me.chkSnapshots.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkSnapshots.Location = New System.Drawing.Point(8, 407)
        Me.chkSnapshots.Name = "chkSnapshots"
        Me.HelpProvider1.SetShowHelp(Me.chkSnapshots, True)
        Me.chkSnapshots.Size = New System.Drawing.Size(240, 24)
        Me.chkSnapshots.TabIndex = 0
        Me.chkSnapshots.Text = "Enable snapshots and keep snapshots for"
        '
        'lsvDatasources
        '
        Me.lsvDatasources.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3, Me.ColumnHeader4})
        Me.lsvDatasources.ContextMenuStrip = Me.mnuDatasources
        Me.lsvDatasources.FullRowSelect = True
        Me.HelpProvider1.SetHelpKeyword(Me.lsvDatasources, "Single_Report_Schedule.htm#Step5")
        Me.HelpProvider1.SetHelpNavigator(Me.lsvDatasources, System.Windows.Forms.HelpNavigator.Topic)
        Me.lsvDatasources.HideSelection = False
        Me.lsvDatasources.Location = New System.Drawing.Point(6, 16)
        Me.lsvDatasources.Name = "lsvDatasources"
        Me.HelpProvider1.SetShowHelp(Me.lsvDatasources, True)
        Me.lsvDatasources.Size = New System.Drawing.Size(590, 382)
        Me.SuperTooltip1.SetSuperTooltip(Me.lsvDatasources, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Double-click on a datasource to set its login credentials. Leaving a datasource's" & _
            " credentials not set will result in errors if they are required.", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon))
        Me.lsvDatasources.TabIndex = 3
        Me.lsvDatasources.UseCompatibleStateImageBehavior = False
        Me.lsvDatasources.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Datasource Name"
        Me.ColumnHeader3.Width = 274
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Name"
        Me.ColumnHeader4.Width = 116
        '
        'mnuDatasources
        '
        Me.mnuDatasources.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClearToolStripMenuItem})
        Me.mnuDatasources.Name = "mnuDatasources"
        Me.mnuDatasources.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuDatasources.Size = New System.Drawing.Size(102, 26)
        '
        'ClearToolStripMenuItem
        '
        Me.ClearToolStripMenuItem.Name = "ClearToolStripMenuItem"
        Me.ClearToolStripMenuItem.Size = New System.Drawing.Size(101, 22)
        Me.ClearToolStripMenuItem.Text = "&Clear"
        '
        'cmdLoginTest
        '
        Me.cmdLoginTest.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdLoginTest.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.HelpProvider1.SetHelpKeyword(Me.cmdLoginTest, "Single_Report_Schedule.htm#Step5")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdLoginTest, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdLoginTest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoginTest.Location = New System.Drawing.Point(521, 404)
        Me.cmdLoginTest.Name = "cmdLoginTest"
        Me.HelpProvider1.SetShowHelp(Me.cmdLoginTest, True)
        Me.cmdLoginTest.Size = New System.Drawing.Size(75, 24)
        Me.cmdLoginTest.TabIndex = 2
        Me.cmdLoginTest.Text = "Preview"
        '
        'mnuContacts
        '
        Me.mnuContacts.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuMAPI, Me.mnuMARS})
        '
        'mnuMAPI
        '
        Me.mnuMAPI.Index = 0
        Me.mnuMAPI.Text = "MAPI Address Book"
        '
        'mnuMARS
        '
        Me.mnuMARS.Index = 1
        Me.mnuMARS.Text = "SQL-RD Address Book"
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.DefaultTooltipSettings = New DevComponents.DotNetBar.SuperTooltipInfo("", "", "", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray)
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'historyStrip
        '
        Me.historyStrip.Name = "historyStrip"
        Me.historyStrip.Size = New System.Drawing.Size(61, 4)
        '
        'stabMain
        '
        Me.stabMain.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.stabMain.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabMain.ControlBox.MenuBox.Name = ""
        Me.stabMain.ControlBox.Name = ""
        Me.stabMain.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabMain.ControlBox.MenuBox, Me.stabMain.ControlBox.CloseBox})
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel3)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel1)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel4)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel6)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel5)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel2)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel8)
        Me.stabMain.ForeColor = System.Drawing.Color.Black
        Me.stabMain.Location = New System.Drawing.Point(-1, 1)
        Me.stabMain.Name = "stabMain"
        Me.stabMain.ReorderTabsEnabled = True
        Me.stabMain.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabMain.SelectedTabIndex = 5
        Me.stabMain.Size = New System.Drawing.Size(751, 450)
        Me.stabMain.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.stabMain.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.stabMain.TabIndex = 51
        Me.stabMain.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabGeneral, Me.tabSchedule, Me.tabReportOptions, Me.tabReport, Me.tabDestinations, Me.tabException, Me.tabCustomTasks})
        Me.stabMain.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        Me.stabMain.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.Step4)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(615, 450)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.tabReport
        '
        'Step4
        '
        Me.Step4.BackColor = System.Drawing.Color.White
        Me.Step4.Controls.Add(Me.parameterController)
        Me.Step4.Controls.Add(Me.FlowLayoutPanel2)
        Me.Step4.Controls.Add(Me.txtFormula)
        Me.Step4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Step4.Location = New System.Drawing.Point(0, 0)
        Me.Step4.Name = "Step4"
        Me.Step4.Size = New System.Drawing.Size(615, 450)
        Me.Step4.TabIndex = 9
        '
        'parameterController
        '
        Me.parameterController.Dock = System.Windows.Forms.DockStyle.Top
        Me.parameterController.formsAuth = False
        Me.parameterController.Location = New System.Drawing.Point(0, 0)
        Me.parameterController.Name = "parameterController"
        Me.parameterController.serverPassword = Nothing
        Me.parameterController.serverUser = Nothing
        Me.parameterController.Size = New System.Drawing.Size(615, 416)
        Me.parameterController.TabIndex = 14
        Me.parameterController.url = Nothing
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.btnTest2)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 419)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(615, 31)
        Me.FlowLayoutPanel2.TabIndex = 13
        '
        'btnTest2
        '
        Me.btnTest2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnTest2.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnTest2.Location = New System.Drawing.Point(537, 3)
        Me.btnTest2.Name = "btnTest2"
        Me.btnTest2.Size = New System.Drawing.Size(75, 23)
        Me.btnTest2.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.btnTest2.TabIndex = 0
        Me.btnTest2.Text = "Preview"
        '
        'txtFormula
        '
        Me.txtFormula.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtFormula.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFormula.DisabledBackColor = System.Drawing.Color.White
        Me.txtFormula.ForeColor = System.Drawing.Color.Black
        Me.txtFormula.Location = New System.Drawing.Point(308, 182)
        Me.txtFormula.Name = "txtFormula"
        Me.txtFormula.Size = New System.Drawing.Size(100, 15)
        Me.txtFormula.TabIndex = 11
        Me.txtFormula.Visible = False
        '
        'tabReport
        '
        Me.tabReport.AttachedControl = Me.SuperTabControlPanel3
        Me.tabReport.GlobalItem = False
        Me.tabReport.Image = CType(resources.GetObject("tabReport.Image"), System.Drawing.Image)
        Me.tabReport.Name = "tabReport"
        Me.tabReport.Text = "Report"
        Me.tabReport.Visible = False
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.Step1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(615, 450)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabGeneral
        '
        'tabGeneral
        '
        Me.tabGeneral.AttachedControl = Me.SuperTabControlPanel1
        Me.tabGeneral.GlobalItem = False
        Me.tabGeneral.Image = CType(resources.GetObject("tabGeneral.Image"), System.Drawing.Image)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Text = "General"
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.Step5)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(615, 450)
        Me.SuperTabControlPanel4.TabIndex = 0
        Me.SuperTabControlPanel4.TabItem = Me.tabReportOptions
        '
        'tabReportOptions
        '
        Me.tabReportOptions.AttachedControl = Me.SuperTabControlPanel4
        Me.tabReportOptions.GlobalItem = False
        Me.tabReportOptions.Image = CType(resources.GetObject("tabReportOptions.Image"), System.Drawing.Image)
        Me.tabReportOptions.Name = "tabReportOptions"
        Me.tabReportOptions.Text = "Datasources"
        Me.tabReportOptions.Visible = False
        '
        'SuperTabControlPanel6
        '
        Me.SuperTabControlPanel6.Controls.Add(Me.Step6)
        Me.SuperTabControlPanel6.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.SuperTabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel6.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel6.Name = "SuperTabControlPanel6"
        Me.SuperTabControlPanel6.Size = New System.Drawing.Size(615, 450)
        Me.SuperTabControlPanel6.TabIndex = 0
        Me.SuperTabControlPanel6.TabItem = Me.tabException
        '
        'tabException
        '
        Me.tabException.AttachedControl = Me.SuperTabControlPanel6
        Me.tabException.GlobalItem = False
        Me.tabException.Image = CType(resources.GetObject("tabException.Image"), System.Drawing.Image)
        Me.tabException.Name = "tabException"
        Me.tabException.Text = "Exception Handling"
        Me.tabException.Visible = False
        '
        'SuperTabControlPanel5
        '
        Me.SuperTabControlPanel5.Controls.Add(Me.Step3)
        Me.SuperTabControlPanel5.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.SuperTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel5.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel5.Name = "SuperTabControlPanel5"
        Me.SuperTabControlPanel5.Size = New System.Drawing.Size(615, 450)
        Me.SuperTabControlPanel5.TabIndex = 0
        Me.SuperTabControlPanel5.TabItem = Me.tabDestinations
        '
        'tabDestinations
        '
        Me.tabDestinations.AttachedControl = Me.SuperTabControlPanel5
        Me.tabDestinations.GlobalItem = False
        Me.tabDestinations.Image = CType(resources.GetObject("tabDestinations.Image"), System.Drawing.Image)
        Me.tabDestinations.Name = "tabDestinations"
        Me.tabDestinations.Text = "Destinations"
        Me.tabDestinations.Visible = False
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.Step2)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(615, 450)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.tabSchedule
        '
        'tabSchedule
        '
        Me.tabSchedule.AttachedControl = Me.SuperTabControlPanel2
        Me.tabSchedule.GlobalItem = False
        Me.tabSchedule.Image = CType(resources.GetObject("tabSchedule.Image"), System.Drawing.Image)
        Me.tabSchedule.Name = "tabSchedule"
        Me.tabSchedule.Text = "Schedule"
        Me.tabSchedule.Visible = False
        '
        'SuperTabControlPanel8
        '
        Me.SuperTabControlPanel8.Controls.Add(Me.Step7)
        Me.SuperTabControlPanel8.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.SuperTabControlPanel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel8.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel8.Name = "SuperTabControlPanel8"
        Me.SuperTabControlPanel8.Size = New System.Drawing.Size(615, 450)
        Me.SuperTabControlPanel8.TabIndex = 2
        Me.SuperTabControlPanel8.TabItem = Me.tabCustomTasks
        '
        'tabCustomTasks
        '
        Me.tabCustomTasks.AttachedControl = Me.SuperTabControlPanel8
        Me.tabCustomTasks.GlobalItem = False
        Me.tabCustomTasks.Image = CType(resources.GetObject("tabCustomTasks.Image"), System.Drawing.Image)
        Me.tabCustomTasks.Name = "tabCustomTasks"
        Me.tabCustomTasks.Text = "Custom Tasks"
        Me.tabCustomTasks.Visible = False
        '
        'SuperTabControlPanel7
        '
        Me.SuperTabControlPanel7.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.SuperTabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel7.Location = New System.Drawing.Point(109, 0)
        Me.SuperTabControlPanel7.Name = "SuperTabControlPanel7"
        Me.SuperTabControlPanel7.Size = New System.Drawing.Size(642, 450)
        Me.SuperTabControlPanel7.TabIndex = 0
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdFinish)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdNext)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 454)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(748, 31)
        Me.FlowLayoutPanel1.TabIndex = 52
        '
        'frmSingleScheduleWizard
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(748, 485)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.stabMain)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.HelpButton = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSingleScheduleWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Single Report Schedule Wizard"
        Me.Step1.ResumeLayout(False)
        Me.Step2.ResumeLayout(False)
        Me.Step3.ResumeLayout(False)
        Me.Step6.ResumeLayout(False)
        CType(Me.ErrProv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step7.ResumeLayout(False)
        Me.Step5.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        CType(Me.txtSnapshots, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mnuDatasources.ResumeLayout(False)
        CType(Me.stabMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabMain.ResumeLayout(False)
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.Step4.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel4.ResumeLayout(False)
        Me.SuperTabControlPanel6.ResumeLayout(False)
        Me.SuperTabControlPanel5.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel8.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW As Object = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property



    Private Sub cmdDbLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDbLoc.Click
        Try
            browseAndPickReportFromServer(oRpt, ServerUser, ServerPassword, txtUrl, txtDBLoc, txtName, txtDesc, txtKeyWord, cmdNext, Me.parameterController, lsvDatasources, formsAuth)

            Return

            Dim srsVersion As String = clsMarsReport.m_serverVersion(txtUrl.Text, ServerUser, ServerPassword, formsAuth)

            ' If srsVersion >= "2009" Then
            ' oRpt = New rsClients.rsClient2010(txtUrl.Text)
            If srsVersion >= "2007" Then
                oRpt = New rsClients.rsClient2008(txtUrl.Text)
            Else
                oRpt = New rsClients.rsClient(txtUrl.Text)
            End If

            'Dim oServerLogin As frmRemoteLogin = New frmRemoteLogin
            'Dim sReturn As String() = oServerLogin.ServerLogin(ServerUser, ServerPassword)
            'Dim cred As System.Net.NetworkCredential

            'we check to see if we should put reportserver2005.asmx in the URL
            If srsVersion >= "2007" Then txtUrl.Text = clsMarsParser.Parser.fixASMXfor2008(txtUrl.Text)

            If txtUrl.Text.Contains("2006.asmx") Then '//its sharepoint mode
                oRpt = New ReportServer_2006.ReportingService2006
            ElseIf txtUrl.Text.Contains("2010.asmx") Then
                oRpt = New rsClients.rsClient2010(txtUrl.Text)
            End If

            oRpt.Url = txtUrl.Text

            Dim sPath As String

            Dim oServer As frmReportServer = New frmReportServer

            sPath = oServer.newGetReports(oRpt, txtUrl.Text, ServerUser, ServerPassword, txtDBLoc.Text)

            If sPath IsNot Nothing Then
                txtDBLoc.Text = sPath
                txtName.Text = sPath.Split("/")(sPath.Split("/").GetUpperBound(0))
                cmdNext.Enabled = True

                If txtDBLoc.Text.StartsWith("/") = False And (TypeOf oRpt Is ReportServer.ReportingService Or TypeOf oRpt Is ReportServer_2005.ReportingService2005 Or _
                                                              TypeOf oRpt Is ReportServer_2010.ReportingService2010) Then
                    txtDBLoc.Text = "/" & txtDBLoc.Text
                End If
            Else
                Return
            End If

            Dim sRDLPath As String = clsMarsReport.m_rdltempPath & txtName.Text & ".rdl"

            Dim repDef() As Byte

            If IO.File.Exists(sRDLPath) = True Then
                IO.File.Delete(sRDLPath)
            End If

            Dim fsReport As New System.IO.FileStream(sRDLPath, IO.FileMode.Create)

            If TypeOf oRpt Is rsClients.rsClient2010 Then
                If txtDBLoc.Text.ToLower.StartsWith("/http") Then
                    txtDBLoc.Text = txtDBLoc.Text.Remove(0, 1)
                End If

                repDef = oRpt.GetItemDefinition(txtDBLoc.Text)
            Else
                repDef = oRpt.GetReportDefinition(txtDBLoc.Text)
            End If


            fsReport.Write(repDef, 0, repDef.LongLength)

            fsReport.Close()

            'get the parameters


            'UcDest.m_parameterList = ucparList.m_ParametersCollection
            '    Me.UcBlank.m_ParametersList = Me.m_ParametersList

            lsvDatasources.Items.Clear()

            Dim sData As ArrayList = New ArrayList

            sData = clsMarsReport._GetDatasources(sRDLPath)

            If sData IsNot Nothing Then
                lsvDatasources.Items.Clear()

                For Each s As Object In sData
                    Dim oItem As ListViewItem = lsvDatasources.Items.Add(s)

                    oItem.SubItems.Add("default")

                    oItem.ImageIndex = 0
                    oItem.Tag = 0
                Next
            End If

            txtDesc.Text = clsMarsReport._GetReportDescription(sRDLPath)

            IO.File.Delete(sRDLPath)

            txtName.Focus()
            txtName.SelectAll()

            cmdNext.Enabled = True

            'save the Url as it has been validated
            logUrlHistory(Me.txtUrl.Text)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), _
            "Please check your report server's web service URL and try again")

            If ex.Message.ToLower.Contains("cannot be found") Then
                txtDBLoc.Text = ""
                txtName.Text = ""
                txtDesc.Text = ""
            End If
        End Try
    End Sub
    Dim closeFlag As Boolean

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Try
            If closeFlag = False Then
                Dim sp As DevComponents.DotNetBar.SuperTooltip = New DevComponents.DotNetBar.SuperTooltip
                Dim spi As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo("Really Cancel?", "",
                                                                                                                   "Are you sure you would like to close the wizard? All your progress will be lost. Click Cancel again to confirm.",
                                                                                                               Nothing,
                                                                                                                   Nothing,
                                                                                                                   DevComponents.DotNetBar.eTooltipColor.Office2003)
                sp.SetSuperTooltip(sender, spi)
                sp.ShowTooltip(sender)
                closeFlag = True
            Else
                Dim oCleaner As clsMarsData = New clsMarsData
                HasCancelled = True
                oCleaner.CleanDB()
                Me.Close()
            End If
        Catch : End Try

    End Sub

    Private Sub stabMain_SelectedTabChanged(ByVal sender As Object, ByVal e As DevComponents.DotNetBar.SuperTabStripSelectedTabChangedEventArgs) Handles stabMain.SelectedTabChanged
        Select Case stabMain.SelectedTab.Text
            Case "Report"
                If m_inserter.IsDisposed Then
                    m_inserter = New frmInserter(0)
                End If

                m_inserter.GetConstants(Me)

                ' Dim ssrsReport As clsMarsReport = New clsMarsReport

                ' ucparList.m_serverparametersCollection = ssrsReport.getserverReportParametersCollection(txtUrl.Text, txtDBLoc.Text, ServerUser, ServerPassword, "", formsAuth, , 99999)

                ' ucparList.addHandlers()

                If parameterController.lsvParameters.Items.Count = 0 Then
                    parameterController.loadParameters(txtUrl.Text, txtDBLoc.Text, Nothing, ServerUser, ServerPassword, formsAuth)
                End If
            Case Else
                m_inserter.Hide()
        End Select
    End Sub


    Private Sub frmSingleScheduleWizard_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If HasCancelled = False Then
            cmdCancel_Click(Nothing, Nothing)
        End If
    End Sub



    Private Sub frmSingleScheduleWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        stabMain.SelectedTab = tabGeneral

        HelpProvider1.HelpNamespace = gHelpPath
        Dim oData As New clsMarsData

        oData.CleanDB()

        Dim I As Int32 = 0

        If _CheckScheduleCount() = False Then
            Close()
            Return
        End If

        FormatForWinXP(Me)

        txtFolder.Focus()

        If gParentID > 0 And gParent.Length > 0 Then
            txtFolder.Text = gParent
            txtFolder.Tag = gParentID
            If gParentID > 0 And gParent.Length > 0 Then
                Dim fld As folder = New folder(gParentID)

                txtFolder.Text = fld.getFolderPath
                txtFolder.Tag = gParentID
            End If
        End If

        txtUrl.Text = clsMarsUI.MainUI.ReadRegistry("ReportsLoc", "")

        If txtUrl.Text.Length = 0 Then
            txtUrl.Text = "http://myReportServer/ReportServer/Reportservice.asmx"
            txtUrl.ForeColor = Color.Gray
        End If

        'set up auto-complete data
        With Me.txtUrl
            .AutoCompleteMode = AutoCompleteMode.SuggestAppend
            .AutoCompleteSource = AutoCompleteSource.CustomSource
            .AutoCompleteCustomSource = getUrlHistory()
        End With

    End Sub

    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click

        Dim oData As New clsMarsData

        Select Case stabMain.SelectedTab.Text
            Case "General"
                If txtName.Text = "" Then
                    ErrProv.SetError(txtName, "Please provide a name for this schedule")
                    txtName.Focus()
                    Exit Sub
                ElseIf txtFolder.Text = "" Then
                    ErrProv.SetError(cmdLoc, "Please select the destination folder")
                    cmdLoc.Focus()
                    Exit Sub
                ElseIf txtDBLoc.Text = "" Then
                    ErrProv.SetError(cmdDbLoc, "Please select the access database for the report")
                    cmdDbLoc.Focus()
                    Exit Sub
                ElseIf clsMarsUI.candoRename(txtName.Text, Me.txtFolder.Tag, clsMarsScheduler.enScheduleType.REPORT) = False Then
                    ErrProv.SetError(txtName, "A single report schedule " & _
                    "already exist with this name")
                    txtName.Focus()
                    Return
                End If

                tabSchedule.Visible = True
                stabMain.SelectedTab = tabSchedule
            Case "Schedule"
                If ucSet.isAllDataValid = False Then
                    Return
                End If

                tabReportOptions.Visible = True
                stabMain.SelectedTab = tabReportOptions

            Case "Datasources"
                tabReport.Visible = True
                stabMain.SelectedTab = tabReport
            Case "Report"
                Dim blankFound As Boolean = False

                If parameterController.hasUnsetParameters Then
                    Dim oRes As DialogResult = _
                                                MessageBox.Show("One or more parameters has been " & _
                                                "left blank. Edit the parameters?", _
                                                Application.ProductName, MessageBoxButtons.YesNo, _
                                                MessageBoxIcon.Question)
                    If oRes = DialogResult.Yes Then
                        Return
                    End If
                End If

                tabDestinations.Visible = True
                stabMain.SelectedTab = tabDestinations

            Case "Destinations"
                If UcDest.lsvDestination.Nodes.Count = 0 Then
                    ErrProv.SetError(UcDest.lsvDestination, "Please set up a destination")
                    Return
                End If

                tabException.Visible = True
                stabMain.SelectedTab = tabException
                UcError.cmbRetry.Focus()
            Case "Exception Handling"

                '  If UcBlank.ValidateEntries = False Then Return

                tabCustomTasks.Visible = True
                stabMain.SelectedTab = tabCustomTasks

                cmdNext.Enabled = False
                cmdFinish.Enabled = True
                Me.AcceptButton = cmdFinish
            Case Else
                Exit Sub
        End Select

    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        oErr.ResetError(sender, ErrProv)
        UcDest.sReportTitle = txtName.Text
    End Sub

    Private Sub cmdLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoc.Click
        Dim oFolders As frmFolders = New frmFolders
        Dim sFolder(1) As String

        sFolder = oFolders.GetFolder

        If sFolder(0) <> "" And sFolder(0) <> "Desktop" Then
            txtFolder.Text = sFolder(0)
            txtFolder.Tag = sFolder(1)
        End If
    End Sub



    Private Sub txtFolder_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFolder.TextChanged
        oErr.ResetError(sender, ErrProv)
    End Sub

    Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Select Case nStep
            Case 7
                Step7.Visible = False
                Step6.Visible = True
                Step6.BringToFront()
                cmdNext.Visible = True
                cmdFinish.Visible = False
                Me.AcceptButton = cmdNext
            Case 6
                Step6.Visible = False
                Step5.Visible = True
                Step5.BringToFront()
            Case 5
                Step5.Visible = False
                Step4.Visible = True
                Step4.BringToFront()

            Case 4
                Step4.Visible = False
                Step3.Visible = True
                Step3.BringToFront()
            Case 3
                Step3.Visible = False
                Step2.Visible = True
                Step2.BringToFront()
            Case 2
                Step2.Visible = False
                Step1.Visible = True
                Step1.BringToFront()
                txtFolder.Focus()
            Case Else
                Exit Sub
        End Select

        nStep -= 1
    End Sub

    Private Sub cmdFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFinish.Click
        On Error GoTo Trap
        Dim nReportID As Int32
        Dim sPrinters As String = ""
        Dim lsv As ListViewItem
        Dim WriteSuccess As Boolean
        Dim nInclude As Integer
        Dim sCols As String
        Dim sVals As String
        Dim oReport As New clsMarsReport

        cmdFinish.Enabled = False

        oErr.BusyProgress(10, "Validating user data...")

        'if everything is in order then lets save the report

        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData


        oErr.BusyProgress(30, "Saving report data...")

        nReportID = clsMarsData.CreateDataID("reportattr", "reportid")

        sCols = "ReportID,PackID,DatabasePath,ReportName," & _
            "Parent,ReportTitle," & _
            "Retry,AssumeFail,CheckBlank," & _
            "SelectionFormula,Owner,RptUserID,RptPassword,RptServer,RptDatabase,UseLogin," & _
            "UseSavedData,CachePath,LastRefreshed,AutoRefresh,Bursting,Dynamic,RptDatabaseType,AutoCalc,RetryInterval, rptFormsAuth"

todo:   sVals = nReportID & ",0," & _
            "'" & SQLPrepare(txtUrl.Text) & "'," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            txtFolder.Tag & "," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            UcError.cmbRetry.Value & "," & _
            UcError.m_autoFailAfter & "," & _
          Convert.ToInt32(UcBlankReportX1.chkCheckBlankReport.Checked) & "," & _
            "'" & SQLPrepare(txtFormula.Text) & "'," & _
            "'" & gUser & "'," & _
            "'" & SQLPrepare(ServerUser) & "'," & _
            "'" & SQLPrepare(_EncryptDBValue(ServerPassword)) & "'," & _
            "''," & _
            "''," & _
            0 & "," & _
            0 & "," & _
            "'" & SQLPrepare(txtDBLoc.Text) & "', '" & ConDateTime(Date.Now) & "'," & _
            0 & "," & _
            "0,0," & _
            "'', " & _
            Convert.ToInt32(UcError.chkAutoCalc.Checked) & "," & _
            UcError.txtRetryInterval.Value & "," & _
            Convert.ToInt32(formsAuth)

        SQL = "INSERT INTO ReportAttr(" & sCols & ") VALUES (" & sVals & ")"

        WriteSuccess = clsMarsData.WriteData(SQL)

        If WriteSuccess = True Then
            Dim ScheduleID As Integer = ucSet.saveSchedule(nReportID, clsMarsScheduler.enScheduleType.REPORT, txtDesc.Text, txtKeyWord.Text)

            If WriteSuccess = False Then
                clsMarsData.WriteData("DELETE FROM ReportAttr WHERE ReportID = " & nReportID & " AND PackID = 0")
                Exit Sub
            End If


            oErr.BusyProgress(75, "Saving destination data...")

            SQL = "UPDATE DestinationAttr SET ReportID =" & nReportID & "," & _
            "PackID = 0, SmartID = 0 WHERE ReportID = 99999"

            WriteSuccess = clsMarsData.WriteData(SQL)

            If WriteSuccess = False Then
                clsMarsData.WriteData("DELETE FROM ScheduleAttr WHERE ReportID = " & nReportID & " AND PackID = 0")
                clsMarsData.WriteData("DELETE FROM ReportAttr WHERE ReportID = " & nReportID & " AND PackID = 0")
                Exit Sub
            End If

            oErr.BusyProgress(90, "Saving report parameters...")

            parameterController.saveParameters(nReportID)


            SQL = "UPDATE Tasks SET ScheduleID = " & ScheduleID & " WHERE ScheduleID = 99999"
            clsMarsData.WriteData(SQL)

            'update subreport parameters
            SQL = "UPDATE SubReportParameters SET ReportID =" & nReportID & " WHERE ReportID = 99999"

            clsMarsData.WriteData(SQL)

            SQL = "UPDATE SubReportLogin SET ReportID = " & nReportID & " WHERE ReportID = 99999"

            clsMarsData.WriteData(SQL)

            'save report tables
            SQL = "UPDATE ReportTable SET ReportID = " & nReportID & " WHERE ReportID = 99999"

            clsMarsData.WriteData(SQL)

            SQL = "UPDATE SubReportTable SET ReportID = " & nReportID & " WHERE ReportID = 99999"

            clsMarsData.WriteData(SQL)

            SQL = "UPDATE ReportDatasource SET ReportID = " & nReportID & " WHERE ReportID = 99999"

            clsMarsData.WriteData(SQL)

            For Each item As ListViewItem In lsvDatasources.Items
                SQL = "SELECT * FROM ReportDatasource WHERE ReportID = " & nReportID & " AND " & _
                "DatasourceName = '" & SQLPrepare(item.Text) & "'"

                Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

                If oRs IsNot Nothing Then
                    If oRs.EOF = True Then
                        sCols = "DatasourceID,ReportID,DatasourceName,RptUserID,RptPassword"
                        sVals = clsMarsData.CreateDataID("reportdatasource", "datasourceid") & "," & _
                        nReportID & "," & _
                        "'" & SQLPrepare(item.Text) & "'," & _
                        "'default'," & _
                        "'<default>'"

                        SQL = "INSERT INTO ReportDatasource (" & sCols & ") VALUES (" & sVals & ")"

                        clsMarsData.WriteData(SQL)
                    End If

                    oRs.Close()
                End If
            Next

            'save blank report alert information if available
            If UcBlankReportX1.chkCheckBlankReport.Checked = True Then
                UcBlankReportX1.saveInfo(nReportID, clsMarsScheduler.enScheduleType.REPORT)
            End If

            'save any snapshots details if any
            If chkSnapshots.Checked = True Then
                sCols = "SnapID,ReportID,KeepSnap"

                sVals = clsMarsData.CreateDataID("reportsnapshots", "snapid") & "," & _
                nReportID & "," & _
                txtSnapshots.Value

                SQL = "INSERT INTO ReportSnapshots (" & sCols & ") VALUES " & _
                "(" & sVals & ")"

                clsMarsData.WriteData(SQL)
            End If
        End If

        clsMarsData.WriteData("UPDATE ReportOptions SET ReportID =0 WHERE ReportID =99999")

        If gRole.ToLower <> "administrator" Then
            Dim oUser As New clsMarsUsers

            oUser.AssignView(nReportID, gUser, clsMarsUsers.enViewType.ViewSingle)
        End If

        frmMainWin.itemToSelect = New genericExplorerObject
        frmMainWin.itemToSelect.folderID = txtFolder.Tag
        frmMainWin.itemToSelect.itemName = txtName.Text



        clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.SINGLES, clsMarsAudit.AuditAction.CREATE)

        Close()
Trap:
        cmdFinish.Enabled = True

        clsMarsUI.MainUI.BusyProgress(, , True)


    End Sub


    Private Sub chkSnapshots_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSnapshots.CheckedChanged
        If chkSnapshots.Checked = True Then
            If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.sa8_Snapshots) Then
                _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, sender, getFeatDesc(featureCodes.sa8_Snapshots))
                chkSnapshots.Checked = False
                Return
            End If
        End If

        txtSnapshots.Enabled = chkSnapshots.Checked
    End Sub


    Private Sub cmdLoginTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoginTest.Click, btnTest2.Click
        Dim rv As Microsoft.Reporting.WinForms.ReportViewer
        Dim actualUrl As String = ""

        Try
            AppStatus(True)

            oRpt = New rsClients.rsClient(txtUrl.Text)

            rv = New Microsoft.Reporting.WinForms.ReportViewer

            With oRpt
                .Url = txtUrl.Text

                For x As Integer = 0 To txtUrl.Text.Split("/").GetUpperBound(0) - 1
                    actualUrl &= txtUrl.Text.Split("/")(x) & "/"
                Next

                rv.ProcessingMode = ProcessingMode.Remote
                rv.ServerReport.ReportServerUrl = New Uri(actualUrl)

                If ServerPassword.Length = 0 And ServerUser.Length = 0 Then
                    .Credentials = System.Net.CredentialCache.DefaultCredentials
                    rv.ServerReport.ReportServerCredentials.NetworkCredentials = System.Net.CredentialCache.DefaultCredentials

                Else
                    Dim userDomain As String = ""
                    Dim tmpUser As String = ServerUser

                    getDomainAndUserFromString(tmpUser, userDomain)

                    Dim oCred As System.Net.NetworkCredential = New System.Net.NetworkCredential(tmpUser, ServerPassword, userDomain)

                    .Credentials = oCred

                    rv.ServerReport.ReportServerCredentials.NetworkCredentials = oCred

                    Try
                        If formsAuth Then rv.ServerReport.ReportServerCredentials.SetFormsCredentials(Nothing, tmpUser, ServerPassword, userDomain)
                    Catch : End Try
                End If
            End With

            Dim serverVersion As String = "2000"

            Try
                serverVersion = rv.ServerReport.GetServerVersion
                rv.ServerReport.ReportPath = Me.txtDBLoc.Text
            Catch ex As Exception
                serverVersion = "2000"
                rv = Nothing
            End Try

            Dim oPar() As ReportServer.ParameterValue = Nothing
            Dim reportPars() As Microsoft.Reporting.WinForms.ReportParameter = Nothing
            Dim oDataLogins() As ReportServer.DataSourceCredentials = Nothing
            Dim reportDS() As Microsoft.Reporting.WinForms.DataSourceCredentials
            Dim I As Integer = 0
            Dim oData As New clsMarsData

            'set the parameters
            Dim oParse As New clsMarsParser
            Dim rpt As clsMarsReport = New clsMarsReport

            For Each oItem As ListViewItem In parameterController.lsvParameters.Items

                If oItem.Tag Is Nothing Then Continue For

                Dim obj = oItem.Tag

                If TypeOf obj Is System.Collections.Generic.KeyValuePair(Of String, String) Then
                    Dim setvalue As String = CType(obj, KeyValuePair(Of String, String)).Value

                    If setvalue.ToLower = "[sql-rddefault]" Then Continue For

                    ReDim Preserve oPar(I)
                    ReDim Preserve reportPars(I)

                    oPar(I) = New ReportServer.ParameterValue
                    oPar(I).Name = oItem.Text

                    reportPars(I) = New Microsoft.Reporting.WinForms.ReportParameter
                    reportPars(I).Name = oItem.Text

                    If setvalue.ToLower = "[sql-rdnull]" Then
                        oPar(I) = Nothing
                    Else
                        reportPars(I).Values.Add(clsMarsParser.Parser.ParseString(setvalue))
                        oPar(I).Value = setvalue
                    End If
                Else
                    Dim doSkip As Boolean

                    For Each kvp As KeyValuePair(Of String, String) In obj
                        If kvp.Value.ToLower = "[sqlrd-default]" Then
                            doSkip = True
                            Exit For
                        End If
                    Next

                    If doSkip Then Continue For

                    ReDim Preserve oPar(I)
                    ReDim Preserve reportPars(I)

                    oPar(I) = New ReportServer.ParameterValue
                    oPar(I).Name = oItem.Text

                    reportPars(I) = New Microsoft.Reporting.WinForms.ReportParameter
                    reportPars(I).Name = oItem.Text

                    For Each kvp As KeyValuePair(Of String, String) In obj
                        Dim setvalue As String = kvp.Value

                        If setvalue.ToLower = "[sql-rddefault]" Then Continue For

                        If setvalue.ToLower = "[sql-rdnull]" Then
                            oPar(I) = Nothing
                        ElseIf setvalue.ToLower = "[selectall]" Then
                            For Each p As ReportServer.ReportParameter In parameterController.parameters
                                If String.Compare(p.Name, oItem.Text, True) = 0 Then
                                    If p.ValidValues IsNot Nothing Then

                                        For Each v As ReportServer.ValidValue In p.ValidValues
                                            reportPars(I).Values.Add(v.Value)
                                        Next
                                    End If

                                    Exit For
                                End If
                            Next
                        Else
                            reportPars(I).Values.Add(clsMarsParser.Parser.ParseString(setvalue))
                            oPar(I).Value = setvalue
                        End If
                    Next

                End If

                I += 1
            Next

            If rv IsNot Nothing And reportPars IsNot Nothing Then rv.ServerReport.SetParameters(reportPars)

            I = 0

            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM ReportDatasource WHERE ReportID = " & 99999)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    ReDim Preserve oDataLogins(I)
                    ReDim Preserve reportDS(I)

                    oDataLogins(I) = New ReportServer.DataSourceCredentials
                    reportDS(I) = New Microsoft.Reporting.WinForms.DataSourceCredentials

                    With oDataLogins(I)
                        .DataSourceName = oRs("datasourcename").Value
                        .UserName = oRs("rptuserid").Value
                        .Password = _DecryptDBValue(IsNull(oRs("rptpassword").Value))
                    End With

                    With reportDS(I)
                        .Name = oRs("datasourcename").Value
                        .UserId = oRs("rptuserid").Value
                        .Password = _DecryptDBValue(IsNull(oRs("rptpassword").Value))
                    End With

                    I += 1
                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            If rv IsNot Nothing And reportDS IsNot Nothing Then rv.ServerReport.SetDataSourceCredentials(reportDS)

            If serverVersion = "2000" Then
                Dim warnings As ReportServer.Warning() = Nothing
                Dim streamIDs As String() = Nothing

                Dim oResult As Byte()

                oResult = oRpt.Render(Me.txtDBLoc.Text, "PDF", Nothing, "", _
                    oPar, oDataLogins, Nothing, "", "", Nothing, _
                    warnings, streamIDs)

                Dim sFileName As String = sAppPath & clsMarsData.CreateDataID & ".pdf"

                'write the bytes to disk
                With System.IO.File.Create(sFileName, oResult.Length)
                    .Write(oResult, 0, oResult.Length)
                    .Close()
                End With

                'view the exported file
                Dim oView As New frmPreview

                AppStatus(False)

                oView.PreviewReport(sFileName, txtDBLoc.Text, True)

                Try : IO.File.Delete(sFileName) : Catch : End Try
            Else
                Dim oView As New frmPreview

                oView.PreviewReport(rv, txtDBLoc.Text, True)
            End If
        Catch ex As Exception
            AppStatus(False)
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace), "Please check your datasource credentials and try again")
        End Try
    End Sub

    Private Sub cmbGroupColumns_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ErrProv.SetError(sender, "")
    End Sub

    Private Sub lsvDatasources_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvDatasources.DoubleClick
        Dim oSet As New frmSetTableLogin
        Dim oItem As ListViewItem
        Dim sValues() As String

        If lsvDatasources.SelectedItems.Count = 0 Then Return

        oItem = lsvDatasources.SelectedItems(0)

        sValues = oSet.SaveLoginInfo(99999, oItem.Text, oItem.Tag)

        If Not sValues Is Nothing Then
            oItem.SubItems(1).Text = sValues(0)
            oItem.Tag = sValues(1)
        End If
    End Sub



    Private Sub txtUrl_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUrl.GotFocus
        If txtUrl.ForeColor = Color.Gray Then
            txtUrl.Text = ""
            txtUrl.ForeColor = Color.Blue
        End If
    End Sub


    Private Sub ClearToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClearToolStripMenuItem.Click

        If lsvDatasources.SelectedItems.Count = 0 Then Return
        Dim oData As New clsMarsData

        Dim oItem As ListViewItem = lsvDatasources.SelectedItems(0)

        Dim nDataID As Integer = oItem.Tag

        If nDataID > 0 Then
            If clsMarsData.WriteData("DELETE FROM ReportDatasource WHERE DatasourceID =" & nDataID) = True Then
                oItem.SubItems(1).Text = "default"
            End If
        End If


    End Sub

    Private Sub lsvDatasources_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lsvDatasources.KeyUp
        If e.KeyCode = Keys.Enter Then
            Me.lsvDatasources_DoubleClick(sender, e)
        End If
    End Sub


    Private Sub txtDBLoc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If txtDBLoc.Text.Length > 0 And showTooltip = True Then
            Dim info As New DevComponents.DotNetBar.SuperTooltipInfo
            Dim msg As String

            msg = "Please be aware that by entering the report path manually, SQL-RD will not " & _
            "connect to your report server to list your reports. " & vbCrLf & _
            "If you would like SQL-RD to " & _
            "connect to the report server and list your reports, remove all text from this field."

            With info
                .Color = DevComponents.DotNetBar.eTooltipColor.Lemon
                .CustomSize = New System.Drawing.Size(400, 100)
                .BodyText = msg
                .HeaderText = "For your information:"
                .HeaderVisible = True
                .FooterVisible = False
            End With

            Me.SuperTooltip1.SetSuperTooltip(Me.cmdDbLoc, info)

            SuperTooltip1.ShowTooltip(Me.cmdDbLoc)

            'Me.cmdDbLoc.Image = Me.ImageList1.Images(1)
            ' = ContentAlignment.MiddleCenter

            showTooltip = False
        ElseIf Me.txtDBLoc.Text.Length = 0 Then
            SuperTooltip1.SetSuperTooltip(Me.cmdDbLoc, Nothing)

            'Me.cmdDbLoc.Image = ImageList1.Images(2)
            ' Me.cmdDbLoc.ImageAlign = ContentAlignment.MiddleCenter

            showTooltip = True
        End If

        cmdNext.Enabled = False
    End Sub



    Private Sub lsvDatasources_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lsvDatasources.SelectedIndexChanged

    End Sub
End Class
