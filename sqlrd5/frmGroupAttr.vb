Imports sqlrd.clsMarsData
Imports DevComponents
Imports System.Collections.Generic
Friend Class frmGroupAttr
    Inherits DevComponents.DotNetBar.Office2007Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtGroupName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtGroupDesc As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents chkAll As System.Windows.Forms.CheckBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents optAllowDefaultDestinations As System.Windows.Forms.RadioButton
    Friend WithEvents optAllowDestinationTypes As System.Windows.Forms.RadioButton
    Friend WithEvents optAllowAllDestinations As System.Windows.Forms.RadioButton
    Friend WithEvents btnAllowedDefaultDestinations As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lsvAllowedTypes As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents btnRemoveBlackout As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnAddBlackout As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmbBlackOuts As System.Windows.Forms.ComboBox
    Friend WithEvents lsvBlackouts As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents optRestrictTasksByType As System.Windows.Forms.RadioButton
    Friend WithEvents optAllowAllTasks As System.Windows.Forms.RadioButton
    Friend WithEvents lsvAllowedTasks As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lsvPermissions As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel5 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem5 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem4 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem3 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents ReflectionImage1 As DevComponents.DotNetBar.Controls.ReflectionImage
    Friend WithEvents SuperTabControlPanel6 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem6 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents tvFolders As DevComponents.AdvTree.AdvTree
    Friend WithEvents NodeConnector1 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle1 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents chkRestrictFolderAccess As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim ListViewItem1 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Activation & Deactivation")
        Dim ListViewItem2 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Backup & Restore")
        Dim ListViewItem3 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Compact System")
        Dim ListViewItem4 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Create/Edit Schedules")
        Dim ListViewItem5 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Custom Calendar")
        Dim ListViewItem6 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Customize Sender Information (SMTP)")
        Dim ListViewItem7 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Data Items")
        Dim ListViewItem8 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Delete Schedules")
        Dim ListViewItem9 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Export Schedules")
        Dim ListViewItem10 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Feature Upgrades")
        Dim ListViewItem11 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Folder Management")
        Dim ListViewItem12 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Full Access to other group members' schedules")
        Dim ListViewItem13 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Operational Hours")
        Dim ListViewItem14 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Options")
        Dim ListViewItem15 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Over-ride Default Parameters")
        Dim ListViewItem16 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Remote Administration")
        Dim ListViewItem17 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Smart Folder Mngmnt")
        Dim ListViewItem18 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("SMTP Servers Manager")
        Dim ListViewItem19 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("System Migration")
        Dim ListViewItem20 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("System Monitor")
        Dim ListViewItem21 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("User Manager")
        Dim ListViewItem22 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Disk")
        Dim ListViewItem23 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Email")
        Dim ListViewItem24 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Printer")
        Dim ListViewItem25 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("FTP")
        Dim ListViewItem26 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("SMS")
        Dim ListViewItem27 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("ODBC")
        Dim ListViewItem28 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("SharePoint")
        Dim ListViewItem29 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Fax")
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGroupAttr))
        Dim ListViewItem30 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("General")
        Dim ListViewItem31 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Files and Folders")
        Dim ListViewItem32 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Internet")
        Dim ListViewItem33 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Database")
        Dim ListViewItem34 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Registry")
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.txtGroupName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtGroupDesc = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.lsvPermissions = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.chkAll = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lsvAllowedTypes = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnAllowedDefaultDestinations = New DevComponents.DotNetBar.ButtonX()
        Me.optAllowDefaultDestinations = New System.Windows.Forms.RadioButton()
        Me.optAllowDestinationTypes = New System.Windows.Forms.RadioButton()
        Me.optAllowAllDestinations = New System.Windows.Forms.RadioButton()
        Me.btnRemoveBlackout = New DevComponents.DotNetBar.ButtonX()
        Me.btnAddBlackout = New DevComponents.DotNetBar.ButtonX()
        Me.cmbBlackOuts = New System.Windows.Forms.ComboBox()
        Me.lsvBlackouts = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.lsvAllowedTasks = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.optRestrictTasksByType = New System.Windows.Forms.RadioButton()
        Me.optAllowAllTasks = New System.Windows.Forms.RadioButton()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel6 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tvFolders = New DevComponents.AdvTree.AdvTree()
        Me.NodeConnector1 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle1 = New DevComponents.DotNetBar.ElementStyle()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.chkRestrictFolderAccess = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.SuperTabItem6 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel5 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem5 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem4 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem3 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.ReflectionImage1 = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel6.SuspendLayout()
        CType(Me.tvFolders, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuperTabControlPanel5.SuspendLayout()
        Me.SuperTabControlPanel4.SuspendLayout()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.Location = New System.Drawing.Point(8, 51)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Description"
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.Location = New System.Drawing.Point(8, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 23)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Group Name"
        '
        'txtGroupName
        '
        Me.txtGroupName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtGroupName.Border.Class = "TextBoxBorder"
        Me.txtGroupName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtGroupName.DisabledBackColor = System.Drawing.Color.White
        Me.txtGroupName.ForeColor = System.Drawing.Color.Black
        Me.txtGroupName.Location = New System.Drawing.Point(8, 24)
        Me.txtGroupName.Name = "txtGroupName"
        Me.txtGroupName.Size = New System.Drawing.Size(648, 21)
        Me.txtGroupName.TabIndex = 0
        '
        'txtGroupDesc
        '
        Me.txtGroupDesc.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtGroupDesc.Border.Class = "TextBoxBorder"
        Me.txtGroupDesc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtGroupDesc.DisabledBackColor = System.Drawing.Color.White
        Me.txtGroupDesc.ForeColor = System.Drawing.Color.Black
        Me.txtGroupDesc.Location = New System.Drawing.Point(8, 67)
        Me.txtGroupDesc.Multiline = True
        Me.txtGroupDesc.Name = "txtGroupDesc"
        Me.txtGroupDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtGroupDesc.Size = New System.Drawing.Size(648, 261)
        Me.txtGroupDesc.TabIndex = 1
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(677, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(758, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 59
        Me.cmdCancel.Text = "&Cancel"
        '
        'lsvPermissions
        '
        Me.lsvPermissions.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvPermissions.Border.Class = "ListViewBorder"
        Me.lsvPermissions.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvPermissions.CheckBoxes = True
        Me.lsvPermissions.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader4})
        Me.lsvPermissions.DisabledBackColor = System.Drawing.Color.Empty
        Me.lsvPermissions.ForeColor = System.Drawing.Color.Black
        Me.lsvPermissions.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        ListViewItem1.StateImageIndex = 0
        ListViewItem2.StateImageIndex = 0
        ListViewItem3.StateImageIndex = 0
        ListViewItem4.StateImageIndex = 0
        ListViewItem5.StateImageIndex = 0
        ListViewItem6.StateImageIndex = 0
        ListViewItem7.StateImageIndex = 0
        ListViewItem8.StateImageIndex = 0
        ListViewItem9.StateImageIndex = 0
        ListViewItem10.StateImageIndex = 0
        ListViewItem11.StateImageIndex = 0
        ListViewItem12.StateImageIndex = 0
        ListViewItem13.StateImageIndex = 0
        ListViewItem14.StateImageIndex = 0
        ListViewItem15.StateImageIndex = 0
        ListViewItem16.StateImageIndex = 0
        ListViewItem17.StateImageIndex = 0
        ListViewItem18.StateImageIndex = 0
        ListViewItem19.StateImageIndex = 0
        ListViewItem20.StateImageIndex = 0
        ListViewItem21.StateImageIndex = 0
        Me.lsvPermissions.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem1, ListViewItem2, ListViewItem3, ListViewItem4, ListViewItem5, ListViewItem6, ListViewItem7, ListViewItem8, ListViewItem9, ListViewItem10, ListViewItem11, ListViewItem12, ListViewItem13, ListViewItem14, ListViewItem15, ListViewItem16, ListViewItem17, ListViewItem18, ListViewItem19, ListViewItem20, ListViewItem21})
        Me.lsvPermissions.Location = New System.Drawing.Point(8, 33)
        Me.lsvPermissions.Name = "lsvPermissions"
        Me.lsvPermissions.Size = New System.Drawing.Size(661, 309)
        Me.lsvPermissions.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lsvPermissions.TabIndex = 14
        Me.lsvPermissions.UseCompatibleStateImageBehavior = False
        Me.lsvPermissions.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Width = 420
        '
        'chkAll
        '
        Me.chkAll.BackColor = System.Drawing.Color.Transparent
        Me.chkAll.Location = New System.Drawing.Point(13, 3)
        Me.chkAll.Name = "chkAll"
        Me.chkAll.Size = New System.Drawing.Size(104, 24)
        Me.chkAll.TabIndex = 0
        Me.chkAll.Text = "All"
        Me.chkAll.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.lsvAllowedTypes)
        Me.GroupBox1.Controls.Add(Me.btnAllowedDefaultDestinations)
        Me.GroupBox1.Controls.Add(Me.optAllowDefaultDestinations)
        Me.GroupBox1.Controls.Add(Me.optAllowDestinationTypes)
        Me.GroupBox1.Controls.Add(Me.optAllowAllDestinations)
        Me.GroupBox1.Location = New System.Drawing.Point(4, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(300, 339)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Destination Restrictions"
        '
        'lsvAllowedTypes
        '
        Me.lsvAllowedTypes.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvAllowedTypes.Border.Class = "ListViewBorder"
        Me.lsvAllowedTypes.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvAllowedTypes.CheckBoxes = True
        Me.lsvAllowedTypes.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvAllowedTypes.DisabledBackColor = System.Drawing.Color.Empty
        Me.lsvAllowedTypes.Enabled = False
        Me.lsvAllowedTypes.ForeColor = System.Drawing.Color.Black
        Me.lsvAllowedTypes.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        ListViewItem22.StateImageIndex = 0
        ListViewItem23.StateImageIndex = 0
        ListViewItem24.StateImageIndex = 0
        ListViewItem25.StateImageIndex = 0
        ListViewItem26.StateImageIndex = 0
        ListViewItem27.StateImageIndex = 0
        ListViewItem28.StateImageIndex = 0
        ListViewItem29.StateImageIndex = 0
        Me.lsvAllowedTypes.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem22, ListViewItem23, ListViewItem24, ListViewItem25, ListViewItem26, ListViewItem27, ListViewItem28, ListViewItem29})
        Me.lsvAllowedTypes.Location = New System.Drawing.Point(38, 67)
        Me.lsvAllowedTypes.Name = "lsvAllowedTypes"
        Me.lsvAllowedTypes.Size = New System.Drawing.Size(256, 173)
        Me.lsvAllowedTypes.TabIndex = 16
        Me.lsvAllowedTypes.UseCompatibleStateImageBehavior = False
        Me.lsvAllowedTypes.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Name"
        Me.ColumnHeader1.Width = 218
        '
        'btnAllowedDefaultDestinations
        '
        Me.btnAllowedDefaultDestinations.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnAllowedDefaultDestinations.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnAllowedDefaultDestinations.Location = New System.Drawing.Point(265, 246)
        Me.btnAllowedDefaultDestinations.Name = "btnAllowedDefaultDestinations"
        Me.btnAllowedDefaultDestinations.Size = New System.Drawing.Size(29, 17)
        Me.btnAllowedDefaultDestinations.TabIndex = 15
        Me.btnAllowedDefaultDestinations.Text = "..."
        '
        'optAllowDefaultDestinations
        '
        Me.optAllowDefaultDestinations.AutoSize = True
        Me.optAllowDefaultDestinations.Location = New System.Drawing.Point(16, 246)
        Me.optAllowDefaultDestinations.Name = "optAllowDefaultDestinations"
        Me.optAllowDefaultDestinations.Size = New System.Drawing.Size(217, 17)
        Me.optAllowDefaultDestinations.TabIndex = 0
        Me.optAllowDefaultDestinations.Tag = "DefaultOnly"
        Me.optAllowDefaultDestinations.Text = "Only allow assigned default destinations"
        Me.optAllowDefaultDestinations.UseVisualStyleBackColor = True
        '
        'optAllowDestinationTypes
        '
        Me.optAllowDestinationTypes.AutoSize = True
        Me.optAllowDestinationTypes.Location = New System.Drawing.Point(17, 44)
        Me.optAllowDestinationTypes.Name = "optAllowDestinationTypes"
        Me.optAllowDestinationTypes.Size = New System.Drawing.Size(196, 17)
        Me.optAllowDestinationTypes.TabIndex = 0
        Me.optAllowDestinationTypes.Tag = "TypesOnly"
        Me.optAllowDestinationTypes.Text = "Only allow certain destination types"
        Me.optAllowDestinationTypes.UseVisualStyleBackColor = True
        '
        'optAllowAllDestinations
        '
        Me.optAllowAllDestinations.AutoSize = True
        Me.optAllowAllDestinations.Checked = True
        Me.optAllowAllDestinations.Location = New System.Drawing.Point(17, 21)
        Me.optAllowAllDestinations.Name = "optAllowAllDestinations"
        Me.optAllowAllDestinations.Size = New System.Drawing.Size(124, 17)
        Me.optAllowAllDestinations.TabIndex = 0
        Me.optAllowAllDestinations.TabStop = True
        Me.optAllowAllDestinations.Tag = "All"
        Me.optAllowAllDestinations.Text = "Allow all destinations"
        Me.optAllowAllDestinations.UseVisualStyleBackColor = True
        '
        'btnRemoveBlackout
        '
        Me.btnRemoveBlackout.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnRemoveBlackout.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnRemoveBlackout.Image = CType(resources.GetObject("btnRemoveBlackout.Image"), System.Drawing.Image)
        Me.btnRemoveBlackout.Location = New System.Drawing.Point(415, 70)
        Me.btnRemoveBlackout.Name = "btnRemoveBlackout"
        Me.btnRemoveBlackout.Size = New System.Drawing.Size(46, 23)
        Me.btnRemoveBlackout.TabIndex = 3
        '
        'btnAddBlackout
        '
        Me.btnAddBlackout.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnAddBlackout.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnAddBlackout.Image = CType(resources.GetObject("btnAddBlackout.Image"), System.Drawing.Image)
        Me.btnAddBlackout.Location = New System.Drawing.Point(415, 28)
        Me.btnAddBlackout.Name = "btnAddBlackout"
        Me.btnAddBlackout.Size = New System.Drawing.Size(46, 23)
        Me.btnAddBlackout.TabIndex = 3
        '
        'cmbBlackOuts
        '
        Me.cmbBlackOuts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBlackOuts.FormattingEnabled = True
        Me.cmbBlackOuts.Location = New System.Drawing.Point(7, 31)
        Me.cmbBlackOuts.Name = "cmbBlackOuts"
        Me.cmbBlackOuts.Size = New System.Drawing.Size(401, 21)
        Me.cmbBlackOuts.TabIndex = 2
        '
        'lsvBlackouts
        '
        Me.lsvBlackouts.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvBlackouts.Border.Class = "ListViewBorder"
        Me.lsvBlackouts.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvBlackouts.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader2})
        Me.lsvBlackouts.DisabledBackColor = System.Drawing.Color.Empty
        Me.lsvBlackouts.ForeColor = System.Drawing.Color.Black
        Me.lsvBlackouts.FullRowSelect = True
        Me.lsvBlackouts.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lsvBlackouts.HideSelection = False
        Me.lsvBlackouts.Location = New System.Drawing.Point(7, 70)
        Me.lsvBlackouts.Name = "lsvBlackouts"
        Me.lsvBlackouts.Size = New System.Drawing.Size(401, 272)
        Me.lsvBlackouts.TabIndex = 1
        Me.lsvBlackouts.UseCompatibleStateImageBehavior = False
        Me.lsvBlackouts.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Width = 384
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.Location = New System.Drawing.Point(4, 3)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(363, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "You may specify when this Group is NOT allowed to schedule their reports"
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox4.Controls.Add(Me.lsvAllowedTasks)
        Me.GroupBox4.Controls.Add(Me.optRestrictTasksByType)
        Me.GroupBox4.Controls.Add(Me.optAllowAllTasks)
        Me.GroupBox4.Location = New System.Drawing.Point(4, 3)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(316, 339)
        Me.GroupBox4.TabIndex = 3
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Custom Task Restrictions"
        '
        'lsvAllowedTasks
        '
        Me.lsvAllowedTasks.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvAllowedTasks.Border.Class = "ListViewBorder"
        Me.lsvAllowedTasks.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvAllowedTasks.CheckBoxes = True
        Me.lsvAllowedTasks.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3})
        Me.lsvAllowedTasks.DisabledBackColor = System.Drawing.Color.Empty
        Me.lsvAllowedTasks.Enabled = False
        Me.lsvAllowedTasks.ForeColor = System.Drawing.Color.Black
        Me.lsvAllowedTasks.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        ListViewItem30.StateImageIndex = 0
        ListViewItem31.StateImageIndex = 0
        ListViewItem32.StateImageIndex = 0
        ListViewItem33.StateImageIndex = 0
        ListViewItem34.StateImageIndex = 0
        Me.lsvAllowedTasks.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem30, ListViewItem31, ListViewItem32, ListViewItem33, ListViewItem34})
        Me.lsvAllowedTasks.Location = New System.Drawing.Point(78, 67)
        Me.lsvAllowedTasks.Name = "lsvAllowedTasks"
        Me.lsvAllowedTasks.Size = New System.Drawing.Size(232, 266)
        Me.lsvAllowedTasks.TabIndex = 1
        Me.lsvAllowedTasks.UseCompatibleStateImageBehavior = False
        Me.lsvAllowedTasks.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Width = 210
        '
        'optRestrictTasksByType
        '
        Me.optRestrictTasksByType.AutoSize = True
        Me.optRestrictTasksByType.Location = New System.Drawing.Point(7, 44)
        Me.optRestrictTasksByType.Name = "optRestrictTasksByType"
        Me.optRestrictTasksByType.Size = New System.Drawing.Size(178, 17)
        Me.optRestrictTasksByType.TabIndex = 0
        Me.optRestrictTasksByType.Tag = "typesonly"
        Me.optRestrictTasksByType.Text = "Allow only certain Custom Tasks"
        Me.optRestrictTasksByType.UseVisualStyleBackColor = True
        '
        'optAllowAllTasks
        '
        Me.optAllowAllTasks.AutoSize = True
        Me.optAllowAllTasks.Checked = True
        Me.optAllowAllTasks.Location = New System.Drawing.Point(7, 21)
        Me.optAllowAllTasks.Name = "optAllowAllTasks"
        Me.optAllowAllTasks.Size = New System.Drawing.Size(132, 17)
        Me.optAllowAllTasks.TabIndex = 0
        Me.optAllowAllTasks.TabStop = True
        Me.optAllowAllTasks.Tag = "all"
        Me.optAllowAllTasks.Text = "Allow all Custom Tasks"
        Me.optAllowAllTasks.UseVisualStyleBackColor = True
        '
        'SuperTabControl1
        '
        Me.SuperTabControl1.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel6)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel5)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel4)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel3)
        Me.SuperTabControl1.ForeColor = System.Drawing.Color.Black
        Me.SuperTabControl1.Location = New System.Drawing.Point(3, 5)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 3
        Me.SuperTabControl1.Size = New System.Drawing.Size(672, 371)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperTabControl1.TabIndex = 60
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem1, Me.SuperTabItem2, Me.SuperTabItem3, Me.SuperTabItem4, Me.SuperTabItem5, Me.SuperTabItem6})
        Me.SuperTabControl1.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel6
        '
        Me.SuperTabControlPanel6.Controls.Add(Me.tvFolders)
        Me.SuperTabControlPanel6.Controls.Add(Me.FlowLayoutPanel2)
        Me.SuperTabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel6.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel6.Name = "SuperTabControlPanel6"
        Me.SuperTabControlPanel6.Size = New System.Drawing.Size(672, 371)
        Me.SuperTabControlPanel6.TabIndex = 0
        Me.SuperTabControlPanel6.TabItem = Me.SuperTabItem6
        '
        'tvFolders
        '
        Me.tvFolders.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.tvFolders.AllowDrop = True
        Me.tvFolders.BackColor = System.Drawing.Color.WhiteSmoke
        '
        '
        '
        Me.tvFolders.BackgroundStyle.Class = "TreeBorderKey"
        Me.tvFolders.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.tvFolders.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvFolders.Enabled = False
        Me.tvFolders.ExpandButtonType = DevComponents.AdvTree.eExpandButtonType.Triangle
        Me.tvFolders.ExpandLineColor = System.Drawing.Color.Transparent
        Me.tvFolders.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.tvFolders.Location = New System.Drawing.Point(0, 32)
        Me.tvFolders.Name = "tvFolders"
        Me.tvFolders.NodesConnector = Me.NodeConnector1
        Me.tvFolders.NodeStyle = Me.ElementStyle1
        Me.tvFolders.PathSeparator = ";"
        Me.tvFolders.Size = New System.Drawing.Size(672, 339)
        Me.tvFolders.Styles.Add(Me.ElementStyle1)
        Me.tvFolders.TabIndex = 0
        Me.tvFolders.Text = "AdvTree1"
        '
        'NodeConnector1
        '
        Me.NodeConnector1.LineColor = System.Drawing.Color.Transparent
        '
        'ElementStyle1
        '
        Me.ElementStyle1.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle1.Name = "ElementStyle1"
        Me.ElementStyle1.TextColor = System.Drawing.SystemColors.ControlText
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel2.Controls.Add(Me.chkRestrictFolderAccess)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(672, 32)
        Me.FlowLayoutPanel2.TabIndex = 1
        '
        'chkRestrictFolderAccess
        '
        '
        '
        '
        Me.chkRestrictFolderAccess.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkRestrictFolderAccess.Location = New System.Drawing.Point(3, 3)
        Me.chkRestrictFolderAccess.Name = "chkRestrictFolderAccess"
        Me.chkRestrictFolderAccess.Size = New System.Drawing.Size(342, 23)
        Me.chkRestrictFolderAccess.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.chkRestrictFolderAccess.TabIndex = 0
        Me.chkRestrictFolderAccess.Text = "Only allow access to the following folders"
        '
        'SuperTabItem6
        '
        Me.SuperTabItem6.AttachedControl = Me.SuperTabControlPanel6
        Me.SuperTabItem6.GlobalItem = False
        Me.SuperTabItem6.Name = "SuperTabItem6"
        Me.SuperTabItem6.Text = "Folders"
        '
        'SuperTabControlPanel5
        '
        Me.SuperTabControlPanel5.Controls.Add(Me.GroupBox4)
        Me.SuperTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel5.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel5.Name = "SuperTabControlPanel5"
        Me.SuperTabControlPanel5.Size = New System.Drawing.Size(672, 371)
        Me.SuperTabControlPanel5.TabIndex = 0
        Me.SuperTabControlPanel5.TabItem = Me.SuperTabItem5
        '
        'SuperTabItem5
        '
        Me.SuperTabItem5.AttachedControl = Me.SuperTabControlPanel5
        Me.SuperTabItem5.GlobalItem = False
        Me.SuperTabItem5.Name = "SuperTabItem5"
        Me.SuperTabItem5.Text = "Custom Tasks"
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.btnRemoveBlackout)
        Me.SuperTabControlPanel4.Controls.Add(Me.Label3)
        Me.SuperTabControlPanel4.Controls.Add(Me.btnAddBlackout)
        Me.SuperTabControlPanel4.Controls.Add(Me.lsvBlackouts)
        Me.SuperTabControlPanel4.Controls.Add(Me.cmbBlackOuts)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(672, 371)
        Me.SuperTabControlPanel4.TabIndex = 0
        Me.SuperTabControlPanel4.TabItem = Me.SuperTabItem4
        '
        'SuperTabItem4
        '
        Me.SuperTabItem4.AttachedControl = Me.SuperTabControlPanel4
        Me.SuperTabItem4.GlobalItem = False
        Me.SuperTabItem4.Name = "SuperTabItem4"
        Me.SuperTabItem4.Text = "Blackout Times"
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(672, 371)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.SuperTabItem3
        '
        'SuperTabItem3
        '
        Me.SuperTabItem3.AttachedControl = Me.SuperTabControlPanel3
        Me.SuperTabItem3.GlobalItem = False
        Me.SuperTabItem3.Name = "SuperTabItem3"
        Me.SuperTabItem3.Text = "Destination Restrictions"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.lsvPermissions)
        Me.SuperTabControlPanel2.Controls.Add(Me.chkAll)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(672, 345)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Permissions"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.txtGroupName)
        Me.SuperTabControlPanel1.Controls.Add(Me.Label2)
        Me.SuperTabControlPanel1.Controls.Add(Me.txtGroupDesc)
        Me.SuperTabControlPanel1.Controls.Add(Me.Label1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(672, 345)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem1
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "General"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 380)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(836, 30)
        Me.FlowLayoutPanel1.TabIndex = 61
        '
        'ReflectionImage1
        '
        '
        '
        '
        Me.ReflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ReflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.ReflectionImage1.Image = CType(resources.GetObject("ReflectionImage1.Image"), System.Drawing.Image)
        Me.ReflectionImage1.Location = New System.Drawing.Point(696, 29)
        Me.ReflectionImage1.Name = "ReflectionImage1"
        Me.ReflectionImage1.Size = New System.Drawing.Size(128, 209)
        Me.ReflectionImage1.TabIndex = 62
        '
        'frmGroupAttr
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(836, 410)
        Me.ControlBox = False
        Me.Controls.Add(Me.ReflectionImage1)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.SuperTabControl1)
        Me.DoubleBuffered = True
        Me.EnableGlass = False
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmGroupAttr"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Group Editor"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel6.ResumeLayout(False)
        CType(Me.tvFolders, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel5.ResumeLayout(False)
        Me.SuperTabControlPanel4.ResumeLayout(False)
        Me.SuperTabControlPanel4.PerformLayout()
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim UserCancel As Boolean
    Dim sMode As String
    Dim m_groupID As Integer
    Dim _ignoreBit As Boolean = True
    Dim folderImage As Image = resizeImage(My.Resources.Folder_256x2561, New Size(16, 16))
    Dim allowedFolders As List(Of Integer)
    Dim grp As usergroup
    Dim UIloaded As Boolean = False
    Dim m_groupAddedByDestinationRestr As Boolean

    Sub loadFolders()

        If grp IsNot Nothing Then
            allowedFolders = grp.userAllowedFolders
        Else
            allowedFolders = New List(Of Integer)
        End If

        tvFolders.BeginUpdate()
        tvFolders.Nodes.Clear()

        Dim rootNode As AdvTree.Node = New AdvTree.Node("Home")
        rootNode.Tag = "folder"
        rootNode.DataKey = 0
        rootNode.Image = Image.FromFile(getAssetLocation("node.png"))
        tvFolders.Nodes.Add(rootNode)
        rootNode.Expand()

        Dim topLevel As folder = New folder(0)


        For Each fld As folder In topLevel.getSubFolders
            Dim folderName As String = fld.folderName
            Dim folderID As Integer = fld.folderID

            Using node As DevComponents.AdvTree.Node = New DevComponents.AdvTree.Node(folderName)
                node.Tag = "folder"
                node.Image = folderImage
                node.DataKey = folderID
                node.CheckBoxVisible = True

                If allowedFolders.Contains(folderID) Then
                    node.Checked = True
                Else
                    node.Checked = False
                End If

                rootNode.Nodes.Add(node)

                populatefolderChildren(folderID, node)
            End Using
        Next

        tvFolders.EndUpdate()
    End Sub

    Sub populatefolderChildren(ByVal folderID As Integer, ByVal parentnode As DevComponents.AdvTree.Node)
        Dim parentFolder As folder = New folder(folderID)
        ' Dim folderImage As Image = resizeImage(My.Resources.Folder_256x2561, 16, 16)

        For Each fld As folder In parentFolder.getSubFolders
            Dim folderName As String = fld.folderName

            Using node As DevComponents.AdvTree.Node = New DevComponents.AdvTree.Node(folderName)
                node.Tag = "folder"
                node.Image = folderImage
                node.DataKey = fld.folderID
                parentnode.Nodes.Add(node)

                node.CheckBoxVisible = True

                If allowedFolders.Contains(fld.folderID) Then
                    node.Checked = True

                    Dim pn As AdvTree.Node = node.Parent

                    Do While pn IsNot Nothing
                        pn.Expand()

                        pn = pn.Parent
                    Loop
                Else
                    node.Checked = False
                End If

                populatefolderChildren(fld.folderID, node)
            End Using
        Next
    End Sub
    Public Sub EditGroup(ByVal nID As Integer)
        m_groupID = nID

        grp = New usergroup(m_groupID)

        sMode = "Edit"

        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oldGroupName As String = ""

        SQL = "SELECT * FROM GroupAttr WHERE GroupID =" & nID

        oRs = GetData(SQL)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                txtGroupName.Text = oRs("groupname").Value
                txtGroupDesc.Text = oRs("groupdesc").Value
                destinationAccessType = IsNull(oRs("destinationAccessType").Value, "")
                customTaskAccessType = IsNull(oRs("customtaskaccesstype").Value, "all")
                folderAccessType = Convert.ToInt16(IsNull(oRs("folderaccesstype").Value, 0))
            End If

            oRs.Close()
        End If

        Me._GetGroupPermissions(nID)

        '//allowed destinations
        If optAllowDestinationTypes.Checked Then
            oRs = clsMarsData.GetData("SELECT * FROM groupdestinationTypesAttr WHERE groupID =" & nID)

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    Dim itemName As String = oRs("destinationtype").Value

                    Dim listItem As ListViewItem = lsvAllowedTypes.FindItemWithText(itemName)

                    If listItem IsNot Nothing Then
                        listItem.Checked = True
                    End If

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If
        End If

        '//allowed tasks
        If optRestrictTasksByType.Checked Then
            oRs = clsMarsData.GetData("SELECT * FROM groupTaskTypesAttr WHERE groupID =" & nID)

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    Dim itemName As String = oRs("tasktype").Value

                    Dim listItem As ListViewItem = lsvAllowedTasks.FindItemWithText(itemName)

                    If listItem IsNot Nothing Then
                        listItem.Checked = True
                    End If

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If
        End If



        oldGroupName = txtGroupName.Text

        _ignoreBit = False

        loadGroupBlackoutPeriods(nID)

        loadFolders()

        UIloaded = True

        Me.ShowDialog()

        If UserCancel = True Then
            Return
        End If

        SQL = "UPDATE GroupAttr SET " & _
        "GroupName = '" & txtGroupName.Text & "'," & _
        "GroupDesc = '" & txtGroupDesc.Text & "', " & _
        "destinationAccessType = '" & destinationAccessType & "', " & _
        "customtaskaccesstype = '" & customTaskAccessType & "', " & _
        "folderaccesstype = " & folderAccessType & " " & _
        " WHERE GroupID =" & nID

        WriteData(SQL)

        Me._SetGroupPermissions(nID)

        If optAllowDestinationTypes.Checked Then
            Dim arr As ArrayList = New ArrayList

            For Each it As ListViewItem In lsvAllowedTypes.CheckedItems
                arr.Add(it.Text)
            Next

            Dim usergrp As New usergroup(nID)
            usergrp.allowedDestinatoionTypes = arr
        End If


        If optRestrictTasksByType.Checked Then
            Dim arr As ArrayList = New ArrayList

            For Each it As ListViewItem In lsvAllowedTasks.CheckedItems
                arr.Add(it.Text)
            Next

            Dim usergrp As New usergroup(nID)
            usergrp.allowedTaskTypes = arr
        End If

        SQL = "UPDATE CRDUsers SET UserRole ='" & SQLPrepare(txtGroupName.Text) & "' WHERE UserRole = '" & SQLPrepare(oldGroupName) & "'"

        WriteData(SQL)

        Me.setGroupBlackoutPeriods(nID)

        allowedFolders = New List(Of Integer)

        getAllowedFolders()

        grp.userAllowedFolders = allowedFolders
    End Sub


    Sub doAddGroup(ngroupID As Integer)
        grp = New usergroup()
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String

        sCols = "GroupID,GroupName,GroupDesc,destinationAccessType,customtaskaccesstype,folderaccesstype"

        sVals = nGroupID & "," & _
        "'" & SQLPrepare(txtGroupName.Text) & "'," & _
        "'" & SQLPrepare(txtGroupDesc.Text) & "'," & _
        "'" & Me.destinationAccessType & "'," & _
        "'" & Me.customTaskAccessType & "'," & _
        folderAccessType

        SQL = "INSERT INTO GroupAttr (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        Me._SetGroupPermissions(nGroupID)

        If optAllowDestinationTypes.Checked Then
            clsMarsData.WriteData("DELETE FROM groupdestinationTypesAttr WHERE groupID =" & nGroupID)

            sCols = "attrid,groupid,destinationType"

            For Each item As ListViewItem In lsvAllowedTypes.CheckedItems
                sVals = clsMarsData.CreateDataID("groupdestinationTypesAttr", "attrid") & "," & _
                nGroupID & "," & _
                "'" & item.Text & "'"

                clsMarsData.DataItem.InsertData("groupdestinationTypesAttr", sCols, sVals)
            Next
        End If

        Me.setGroupBlackoutPeriods(nGroupID)

        allowedFolders = New List(Of Integer)

        getAllowedFolders()

        grp.userAllowedFolders = allowedFolders
    End Sub

    Public Sub AddGroup()
        sMode = "Add"

        Dim nGroupID As Integer = clsMarsData.CreateDataID("groupattr", "groupid")

        m_groupID = nGroupID

        loadFolders()

        UIloaded = True

        Me.ShowDialog()

        If UserCancel = True Then
            Return
        End If

        If m_groupAddedByDestinationRestr = False Then doAddGroup(nGroupID)
    End Sub
    Dim operationHoursT As Hashtable = New Hashtable

    Private Sub frmGroupAttr_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        ep = New ErrorProvider

        Dim SQL As String
        Dim oRs As ADODB.Recordset

        Me.cmbBlackOuts.Items.Clear()

        SQL = "SELECT * FROM OperationAttr"

        oRs = clsMarsData.GetData(SQL)

        Me.cmbBlackOuts.Items.Add("<New...>")

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                cmbBlackOuts.Items.Add(oRs("operationname").Value)
                operationHoursT.Add(oRs("operationname").Value, oRs("operationid").Value)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        SuperTabControl1.SelectedTab = SuperTabControl1.Tabs(0)
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtGroupName.Text.Length = 0 Then
            ep.SetError(txtGroupName, "Please provide a unique group name")
            txtGroupName.Focus()
            Return
        ElseIf txtGroupName.Text.ToLower = "administrator" Or txtGroupName.Text.ToLower = "user" Then
            ep.SetError(txtGroupName, "The provided group name is an internal SQL-RD group name, please pick a different name and try again")
            txtGroupName.Focus()
            Return
        ElseIf clsMarsData.IsDuplicate("GroupAttr", "GroupName", txtGroupName.Text, False) = True _
        And sMode = "Add" And m_groupAddedByDestinationRestr = False Then
            ep.SetError(txtGroupName, "A group with this name already exists")
            txtGroupName.Focus()
            Return
        End If

        Close()
    End Sub

    Private Sub chkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAll.CheckedChanged


        For Each it As ListViewItem In lsvPermissions.Items
            it.Checked = chkAll.Checked
        Next
    End Sub

    Private Sub _GetGroupPermissions(ByVal nGroupID As Integer)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oCheck As CheckBox
        Dim sText As String
        Dim nValue As Integer

        SQL = "SELECT * FROM GroupPermissions WHERE GroupID =" & nGroupID

        oRs = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then
            Return
        Else
            Do While oRs.EOF = False
                sText = _DecryptDBValue(oRs("permdesc").Value)
                nValue = oRs("permvalue").Value

                For Each it As ListViewItem In lsvPermissions.Items
                    If it.Text = sText Then
                        If nValue = 1458 Then
                            it.Checked = True
                        Else
                            it.Checked = False
                        End If
                    End If
                Next

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

    End Sub
    Private Sub _SetGroupPermissions(ByVal nGroupID As Integer)
        Dim oCheck As CheckBox
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim nPerm As Integer

        sCols = "PermID,GroupID,PermDesc,PermValue"

        clsMarsData.WriteData("DELETE FROM GroupPermissions WHERE GroupID =" & nGroupID)

        For Each it As ListViewItem In lsvPermissions.Items
            If it.Checked = True Then
                nPerm = 1458
            Else
                nPerm = 54532
            End If

            sVals = clsMarsData.CreateDataID("grouppermissions", "permid") & "," & _
               nGroupID & "," & _
               "'" & SQLPrepare(_EncryptDBValue(it.Text)) & "'," & _
               nPerm

            SQL = "INSERT INTO GroupPermissions (" & sCols & ") VALUES (" & sVals & ")"

            If clsMarsData.WriteData(SQL) = False Then
                Return
            End If
        Next
    End Sub

    Private Sub loadGroupBlackoutPeriods(ByVal groupID As Integer)
        Dim SQL As String = "SELECT * FROM groupBlackoutAttr WHERE groupid = " & groupID
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then

            Do While oRs.EOF = False
                Dim opID As Integer = oRs("operationhrsid").Value
                Dim op As clsOperationalHours = New clsOperationalHours(opID)
                Dim it As ListViewItem

                If op.operationHoursName IsNot Nothing Then
                    it = New ListViewItem(op.operationHoursName)
                    it.Tag = opID

                    lsvBlackouts.Items.Add(it)
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub
    Private Sub setGroupBlackoutPeriods(ByVal groupID As Integer)
        clsMarsData.WriteData("DELETE FROM groupBlackoutAttr WHERE groupid =" & groupID)

        Dim cols, vals As String

        cols = "blackoutid,groupid,operationhrsid"

        For Each it As ListViewItem In lsvBlackouts.Items
            vals = clsMarsData.CreateDataID("groupBlackoutAttr", "blackoutid") & "," & _
            groupID & "," & _
            it.Tag

            clsMarsData.DataItem.InsertData("groupBlackoutAttr", cols, vals)
        Next

    End Sub


    Private Sub txtGroupName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtGroupName.KeyPress
        If (Char.IsLetterOrDigit(e.KeyChar) = False) And e.KeyChar <> Chr(8) And _
        e.KeyChar <> " "c Then
            e.Handled = True
        End If
    End Sub


    Private Sub chkDeleteSchedules_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub optAllowDestinationTypes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optAllowDestinationTypes.CheckedChanged
        lsvAllowedTypes.Enabled = optAllowDestinationTypes.Checked
    End Sub
    Private Property customTaskAccessType() As String
        Get
            If optAllowAllTasks.Checked Then
                Return optAllowAllTasks.Tag
            Else
                Return optRestrictTasksByType.Tag
            End If
        End Get
        Set(ByVal value As String)
            Select Case value.ToLower
                Case "all"
                    optAllowAllTasks.Checked = True
                Case Else
                    optRestrictTasksByType.Checked = True
            End Select
        End Set
    End Property

    Private Property destinationAccessType() As String
        Get
            If optAllowAllDestinations.Checked Then
                Return optAllowAllDestinations.Tag
            ElseIf optAllowDefaultDestinations.Checked Then
                Return optAllowDefaultDestinations.Tag
            Else
                Return optAllowDestinationTypes.Tag
            End If
        End Get
        Set(ByVal value As String)
            Select Case value.ToLower
                Case "all"
                    optAllowAllDestinations.Checked = True
                Case "typesonly"
                    optAllowDestinationTypes.Checked = True
                Case "defaultonly"
                    optAllowDefaultDestinations.Checked = True
            End Select
        End Set
    End Property

    Public Property folderAccessType As Integer
        Get
            If chkRestrictFolderAccess.Checked Then
                Return 1
            Else
                Return 0
            End If
        End Get
        Set(value As Integer)
            chkRestrictFolderAccess.Checked = value
        End Set
    End Property

    Private Sub btnAllowedDefaultDestinations_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAllowedDefaultDestinations.Click
        If sMode = "Add" Then
            Dim res As DialogResult = MessageBox.Show("SQL-RD will save the group before proceeding. Click Cancel if you do not want to save the group right now.", Application.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Information)

            If res = Windows.Forms.DialogResult.Cancel Then Return

            doAddGroup(m_groupID)

            m_groupAddedByDestinationRestr = True
        End If

        Dim assigner As frmDefaultDestinationAssigner = New frmDefaultDestinationAssigner

        assigner.assignDestinationToGroup(m_groupID)
    End Sub

    Private Sub optAllowAllDestinations_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optAllowAllDestinations.CheckedChanged, optAllowDefaultDestinations.CheckedChanged, optAllowDestinationTypes.CheckedChanged
        Dim opt As RadioButton = CType(sender, RadioButton)

        clsMarsData.WriteData("UPDATE groupattr SET destinationAccessType ='" & opt.Tag & "' WHERE groupid = " & m_groupID)

        If opt.Name = optAllowDefaultDestinations.Name And optAllowDefaultDestinations.Checked And _ignoreBit = False Then
            btnAllowedDefaultDestinations_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub cmbBlackOuts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbBlackOuts.SelectedIndexChanged
        If cmbBlackOuts.Text = "" Then Return

        If cmbBlackOuts.Text = "<New...>" Then
            Dim opAdd As frmOperationalHours = New frmOperationalHours
            opAdd.Text = "Configure Blackout Hours"

            Dim newID As Integer = 0
            Dim sNew As String = opAdd.AddOperationalHours(newID)

            If sNew <> "" Then
                operationHoursT.Add(sNew, newID)
                Me.cmbBlackOuts.Items.Add(sNew)
                Me.cmbBlackOuts.Text = sNew
            End If
        Else
            For Each itx As ListViewItem In lsvBlackouts.Items
                If String.Compare(itx.Text, cmbBlackOuts.Text, True) = 0 Then
                    Return
                End If
            Next

            Dim it As ListViewItem = New ListViewItem(cmbBlackOuts.Text)
            it.Tag = operationHoursT.Item(cmbBlackOuts.Text)
            lsvBlackouts.Items.Add(it)
        End If
    End Sub

    Private Sub btnAddBlackout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddBlackout.Click
        If cmbBlackOuts.Text = "" Then Return

        cmbBlackOuts_SelectedIndexChanged(sender, e)
    End Sub

    Private Sub btnRemoveBlackout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveBlackout.Click
        Dim arr As New ArrayList

        For Each it As ListViewItem In lsvBlackouts.SelectedItems
            arr.Add(it)
        Next

        For Each it As ListViewItem In arr
            it.Remove()
        Next
    End Sub

    Private Sub lsvBlackouts_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvBlackouts.DoubleClick
        If lsvBlackouts.SelectedItems.Count = 0 Then Return

        Dim opID As Integer = lsvBlackouts.SelectedItems(0).Tag

        Dim opEditor As frmOperationalHours = New frmOperationalHours

        If opEditor.EditOperationalHours(opID) = True Then
            Dim op As clsOperationalHours = New clsOperationalHours(opID)
            lsvBlackouts.SelectedItems(0).Text = op.operationHoursName
        End If
    End Sub

    Private Sub lsvBlackouts_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lsvBlackouts.KeyUp
        If e.KeyCode = Keys.Delete Then
            btnRemoveBlackout_Click(sender, e)
        End If
    End Sub

    Private Sub optRestrictTasksByType_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optRestrictTasksByType.CheckedChanged
        lsvAllowedTasks.Enabled = optRestrictTasksByType.Checked
    End Sub


    Private Sub getAllowedFolders(Optional searchNode As AdvTree.Node = Nothing)

        If chkRestrictFolderAccess.Checked = False Then Return

        Dim topNode As AdvTree.Node = tvFolders.Nodes(0) '//the very first node

        If searchNode Is Nothing Then searchNode = topNode

        For Each nn As AdvTree.Node In searchNode.Nodes
            If nn.Checked Then
                allowedFolders.Add(nn.DataKey)
            End If

            If nn.Nodes.Count > 0 Then
                getAllowedFolders(nn)
            End If
        Next

    End Sub

    Private Sub chkRestrictFolderAccess_CheckedChanged(sender As Object, e As EventArgs) Handles chkRestrictFolderAccess.CheckedChanged
        tvFolders.Enabled = chkRestrictFolderAccess.Checked

        If chkRestrictFolderAccess.Checked = True Then
            tvFolders.BackColor = Color.White
        Else
            tvFolders.BackColor = Color.WhiteSmoke
        End If
    End Sub

    Private Sub tvFolders_AfterCheck(sender As Object, e As AdvTree.AdvTreeCellEventArgs) Handles tvFolders.AfterCheck
        If tvFolders.SelectedNode Is Nothing Then Return

        If UIloaded Then
            For Each nn As AdvTree.Node In tvFolders.SelectedNode.Nodes
                nn.Checked = e.Cell.Checked
            Next
        End If
    End Sub

   
End Class
