Imports System.IO
Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Form1))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Button3)
        Me.GroupBox1.Controls.Add(Me.Button4)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(360, 64)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Make..."
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Button1.Location = New System.Drawing.Point(8, 32)
        Me.Button1.Name = "Button1"
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "For 8.5"
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Button2.Location = New System.Drawing.Point(96, 32)
        Me.Button2.Name = "Button2"
        Me.Button2.TabIndex = 0
        Me.Button2.Text = "For 9"
        '
        'Button3
        '
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Button3.Location = New System.Drawing.Point(184, 32)
        Me.Button3.Name = "Button3"
        Me.Button3.TabIndex = 0
        Me.Button3.Text = "For 10"
        '
        'Button4
        '
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Button4.Location = New System.Drawing.Point(272, 32)
        Me.Button4.Name = "Button4"
        Me.Button4.TabIndex = 0
        Me.Button4.Text = "For 11"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(376, 81)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.Text = "Maker"
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        CreateFiles(8)
    End Sub

    Private Sub CreateFiles(ByVal sVersion As Integer)
        Dim sOriginalEXE As String
        Dim sOriginalInterop As String
        Dim sNewEXE As String
        Dim sNewInterop As String

        sOriginalEXE = "C:\Visual Studio Projects\crd5\bin\crd5.exe"
        sOriginalInterop = "C:\Visual Studio Projects\crd5\bin\Interop.CRAXDRT.dll"

        'protect it first
        Dim o As New Process

        o.StartInfo.FileName = "C:\Program Files\PCGNETC5\PCGNET.EXE"
        o.StartInfo.Arguments = "-process -silent G:\ROOT\CHRISTIANSTEVEN\PCGNET\crd5.prj"

        o.Start()
        o.WaitForExit()

        Select Case sVersion
            Case 8
                sNewEXE = "Interop85.dll"
                sNewInterop = "Interop.CRAXDRT85.dll"
            Case 9
                sNewEXE = "Interop90.dll"
                sNewInterop = "Interop.CRAXDRT90.dll"
            Case 10
                sNewEXE = "Interop10.dll"
                sNewInterop = "Interop.CRAXDRT10.dll"
            Case 11
                sNewEXE = "Interop11.dll"
                sNewInterop = "Interop.CRAXDRT11.dll"
        End Select

        Dim sBuildsFolder As String = "C:\Visual Studio Projects\crd5\Builds\"

        System.IO.File.Copy(sOriginalEXE, sBuildsFolder & sNewEXE)
        'System.IO.File.Copy(sOriginalInterop, sBuildsFolder & sNewInterop)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        CreateFiles(9)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        CreateFiles(10)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        CreateFiles(11)
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim oRes As DialogResult
            Dim sFile As String

            oRes = MessageBox.Show("Delete previous builds?", "CRD5Maker", MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If oRes = DialogResult.Yes Then
                For Each s As String In IO.Directory.GetFiles(Application.StartupPath)
                    sFile = IO.Path.GetFileName(s)

                    If sFile.ToLower.StartsWith("interop") Then
                        IO.File.Delete(s)
                    End If
                Next
            End If
        Catch : End Try

        Me.TopMost = True
    End Sub
End Class
