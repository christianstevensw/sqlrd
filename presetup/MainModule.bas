Attribute VB_Name = "MainModule"
Private Declare Function GetSystemDirectory Lib "kernel32" Alias _
"GetSystemDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long) As Long
Private Const WAIT_INFINITE = -1&
Private Const SYNCHRONIZE = &H100000

Private Declare Function OpenProcess Lib "kernel32" _
  (ByVal dwDesiredAccess As Long, _
   ByVal bInheritHandle As Long, _
   ByVal dwProcessId As Long) As Long
   
Private Declare Function WaitForSingleObject Lib "kernel32" _
  (ByVal hHandle As Long, _
   ByVal dwMilliseconds As Long) As Long
   
Private Declare Function CloseHandle Lib "kernel32" _
  (ByVal hObject As Long) As Long

Const WM_CLOSE = &H10
Const INFINITE = &HFFFFFFFF

Dim hWindow As Long
Dim lngResult As Long
Dim lngReturnValue As Long

Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal _
lpClassName As String, ByVal lpWindowName As String) As Long

Private Declare Function PostMessage Lib "user32" Alias "PostMessageA" _
(ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam _
As Long) As Long

Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" _
  (ByVal hwnd As Long, ByVal lpOperation As String, _
  ByVal lpFile As String, ByVal lpParameters As String, _
  ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

Private Declare Function ShellExecuteForExplore Lib "shell32.dll" Alias "ShellExecuteA" _
  (ByVal hwnd As Long, ByVal lpOperation As String, _
  ByVal lpFile As String, lpParameters As Any, _
  lpDirectory As Any, ByVal nShowCmd As Long) As Long
  
  
Public Enum EShellShowConstants
    essSW_HIDE = 0
    essSW_MAXIMIZE = 3
    essSW_MINIMIZE = 6
    essSW_SHOWMAXIMIZED = 3
    essSW_SHOWMINIMIZED = 2
    essSW_SHOWNORMAL = 1
    essSW_SHOWNOACTIVATE = 4
    essSW_SHOWNA = 8
    essSW_SHOWMINNOACTIVE = 7
    essSW_SHOWDEFAULT = 10
    essSW_RESTORE = 9
    essSW_SHOW = 5
End Enum

Private Const ERROR_FILE_NOT_FOUND = 2&
Private Const ERROR_PATH_NOT_FOUND = 3&
Private Const ERROR_BAD_FORMAT = 11&
Private Const SE_ERR_ACCESSDENIED = 5        ' access denied
Private Const SE_ERR_ASSOCINCOMPLETE = 27
Private Const SE_ERR_DDEBUSY = 30
Private Const SE_ERR_DDEFAIL = 29
Private Const SE_ERR_DDETIMEOUT = 28
Private Const SE_ERR_DLLNOTFOUND = 32
Private Const SE_ERR_FNF = 2                ' file not found
Private Const SE_ERR_NOASSOC = 31
Private Const SE_ERR_PNF = 3                ' path not found
Private Const SE_ERR_OOM = 8                ' out of memory
Private Const SE_ERR_SHARE = 26
'for the registry
Private Const HKEY_CLASSES_ROOT = &H80000000
Private Const HKEY_CURRENT_USER = &H80000001
Private Const HKEY_LOCAL_MACHINE = &H80000002
Private Const HKEY_USERS = &H80000003
Private Const HKEY_CURRENT_CONFIG = &H80000005
Private Const HKEY_DYN_DATA = &H80000006
Private Const REG_SZ = 1 'Unicode nul terminated string
Private Const REG_BINARY = 3 'Free form binary
Private Const REG_DWORD = 4 '32-bit number
Private Const ERROR_SUCCESS = 0&
Dim ShellErr As String

Public Sub Main()
    Dim sPath As String
    Dim Response As String
    
    sPath = App.Path
    
    If Right(sPath, 1) <> "\" Then sPath = sPath & "\"
    
    If CheckDOTNET = False Then

        'check Windows Installer version
        ProcessWinInstaller
        
        SuperShell sPath & "dotnetfx.exe"
        
        'ShellEx sPath & "setup.exe", essSW_SHOWNORMAL
        SuperShell sPath & "setup.exe"
    Else
        'ShellEx sPath & "setup.exe", essSW_SHOWNORMAL
        SuperShell sPath & "setup.exe"
    End If
     
    End
End Sub

Private Function GetVersion(sFile As String) As Double
    On Error Resume Next
    
    Dim ver As New cVersion
    
    Dim vs As VS_FIXEDFILEINFO, s$
    
    ver.FileName = sFile
    
    ver.Version vs, s, Comments
    
    Dim sResult As String
    
    sResult = HiWord(vs.dwFileVersionMS)  '  e.g. 0x00030075 = "3.75"
    sResult = sResult & "." & LoWord(vs.dwFileVersionMS)
    
    GetVersion = CDbl(sResult)
    
End Function
Private Sub ProcessWinInstaller()
    On Error Resume Next
    Dim SysPath As String
    Dim FileVer As Double
    Dim sPath As String
    
    SysPath = GetSysDir
    
    If Right(SysPath, 1) <> "\" Then SysPath = SysPath & "\"
    
    SysPath = SysPath & "msiexec.exe"
    
    FileVer = GetVersion(SysPath)
    
    If FileVer < 3.1 Then
        sPath = App.Path
    
        If Right(sPath, 1) <> "\" Then sPath = sPath & "\"
    
        SuperShell sPath & "windowsinstaller.exe"
    End If
End Sub
Private Function CheckDOTNET() As Boolean
    'Dim fso As New FileSystemObject
    Dim SysPath As String
    Dim v2Path As String
    
    SysPath = GetSysDir
    
    SysPath = LCase(SysPath)
    
    SysPath = Replace(SysPath, "system32", "")
    
    v2Path = SysPath & "Microsoft.NET\Framework\v2.0.50727\mscorlib.dll"
    
    If Dir(v2Path) = "" Then
        CheckDOTNET = False
    Else
        CheckDOTNET = True
    End If
    
End Function

Public Function GetSysDir() As String
    Dim sSave As String, Ret As Long
    'Create a buffer
    sSave = Space(255)
    'Get the system directory
    Ret = GetSystemDirectory(sSave, 255)
    'Remove all unnecessary chr$(0)'s
    sSave = Left$(sSave, Ret)
    'Show the windows directory
    GetSysDir = sSave
End Function
Public Function SuperShell(sCmd As String)
    On Error Resume Next
    Dim hProcess As Long
    Dim TaskID As Long
    Dim cmdline As String
    
    Screen.MousePointer = vbHourglass
    
    TaskID = Shell(sCmd, vbNormalFocus)
   
    hProcess = OpenProcess(SYNCHRONIZE, True, TaskID)
    Call WaitForSingleObject(hProcess, WAIT_INFINITE)
    CloseHandle hProcess
    
    Screen.MousePointer = vbDefault
   
End Function
Public Function ShellEx( _
        ByVal sFile As String, _
        Optional ByVal eShowCmd As EShellShowConstants = essSW_SHOWDEFAULT, _
        Optional ByVal sParameters As String = "", _
        Optional ByVal sDefaultDir As String = "", _
        Optional sOperation As String = "open", _
        Optional Owner As Long = 0 _
    ) As Boolean
Dim lR As Long
Dim lErr As Long, sErr As Long
    If (InStr(UCase$(sFile), ".EXE") <> 0) Then
        eShowCmd = 0
    End If
    On Error Resume Next
    If (sParameters = "") And (sDefaultDir = "") Then
        lR = ShellExecuteForExplore(Owner, sOperation, sFile, 0, 0, essSW_SHOWNORMAL)
    Else
        lR = ShellExecute(Owner, sOperation, sFile, sParameters, sDefaultDir, eShowCmd)
    End If
    If (lR < 0) Or (lR > 32) Then
        ShellEx = True
    Else
        ' raise an appropriate error:
        lErr = vbObjectError + 1048 + lR
        Select Case lR
        Case 0
            lErr = 7: sErr = "Out of memory"
            ShellErr = "Out of memory"
        Case ERROR_FILE_NOT_FOUND
            lErr = 53: sErr = "File not found"
            ShellErr = "File not found: make sure the setup.exe is in the same folder as Crystal Reports Distributor.msi"
            
        Case ERROR_PATH_NOT_FOUND
            lErr = 76: sErr = "Path not found"
            ShellErr = "Path not found"
        Case ERROR_BAD_FORMAT
            sErr = "The executable file is invalid or corrupt"
            ShellErr = "The executable is invalid or corrupt"
        Case SE_ERR_ACCESSDENIED
            lErr = 75: sErr = "Path/file access error"
            ShellErr = "Path/file access error"
        Case SE_ERR_ASSOCINCOMPLETE
            sErr = "This file type does not have a valid file association."
            ShellErr = "This file does not have a valid association"
        Case SE_ERR_DDEBUSY
            lErr = 285: sErr = "The file could not be opened because the target application is busy. Please try again in a moment."
            ShellErr = "The file could not be opened because the target application is busy. Please try again in a moment."
        Case SE_ERR_DDEFAIL
            lErr = 285: sErr = "The file could not be opened because the DDE transaction failed. Please try again in a moment."
            ShellErr = "The file could not be opened because the DDE transaction failed. Please try again in a moment."
        Case SE_ERR_DDETIMEOUT
            lErr = 286: sErr = "The file could not be opened due to time out. Please try again in a moment."
            ShellErr = "The file could not be opened due to a time out. Please try again in a moment"
        Case SE_ERR_DLLNOTFOUND
            lErr = 48: sErr = "The specified dynamic-link library was not found."
            ShellErr = "The specified dynamic-link library was not found."
        Case SE_ERR_FNF
            lErr = 53: sErr = "File not found"
            ShellErr = "File not found"
        Case SE_ERR_NOASSOC
            sErr = "No application is associated with this file type."
            ShellErr = "No application is associated with this file type."
        Case SE_ERR_OOM
            lErr = 7: sErr = "Out of memory"
            ShellErr = "Out of memory"
        Case SE_ERR_PNF
            lErr = 76: sErr = "Path not found"
            ShellErr = "Path not found"
        Case SE_ERR_SHARE
            lErr = 75: sErr = "A sharing violation occurred."
            ShellErr = "A sharing violation occured"
        Case Else
            sErr = "An error occurred occurred whilst trying to open or print the selected file."
            ShellErr = "An error occurred occurred whilst trying to open or print the selected file."
        End Select
           
        'Err.Raise lErr, , App.Title & ".GShell", sErr
        
        ShellEx = False
    End If

End Function

