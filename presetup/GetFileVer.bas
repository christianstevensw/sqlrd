Attribute VB_Name = "GetFileVer"
Private Declare Function GetLocaleInfoA Lib "kernel32.dll" (ByVal lLCID As Long, ByVal lLCTYPE As Long, ByVal strLCData As String, ByVal lDataLen As Long) As Long

Private Declare Sub lstrcpyn Lib "kernel32.dll" (ByVal strDest As String, ByVal strSrc As Any, ByVal lBytes As Long)

Private Declare Function GetFileVersionInfoSize Lib "version.dll" Alias "GetFileVersionInfoSizeA" (ByVal sFile As String, lpLen As Long) As Long

Private Declare Function GetFileVersionInfo Lib "version.dll" Alias "GetFileVersionInfoA" (ByVal sFile As String, ByVal lpIgnored As Long, ByVal lpSize As Long, ByVal lpBuf As Long) As Long

Private Declare Function VerQueryValue Lib "version.dll" Alias "VerQueryValueA" (ByVal lpBuf As Long, ByVal szReceive As String, lpBufPtr As Long, lLen As Long) As Long

Private Declare Sub CopyMemory Lib "kernel32.dll" Alias "RtlMoveMemory" (pDest As Any, pSource As Any, ByVal ByteLen As Long)

Private Declare Function GetUserDefaultLCID Lib "kernel32.dll" () As Long
 
Public Function VersionInformation(ByVal file_name As _
    String) As String
Dim dummy_handle As Long
Dim buffer() As Byte
Dim info_size As Long
Dim info_address As Long
Dim fixed_file_info 'As VS_FIXEDFILEINFO
Dim fixed_file_info_size As Long
Dim result As String

    ' Get the version information buffer size.
    info_size = GetFileVersionInfoSize(file_name, _
        dummy_handle)
    If info_size = 0 Then
        MsgBox "No version information available"
        Exit Function
    End If

    ' Load the fixed file information into a buffer.
    ReDim buffer(1 To info_size)
    If GetFileVersionInfo(file_name, 0&, info_size, _
        buffer(1)) = 0 Then
        MsgBox "Error getting version information"
        Exit Function
    End If
    If VerQueryValue(buffer(1), "\", info_address, _
        fixed_file_info_size) = 0 Then
        MsgBox "Error getting fixed file version " & _
            "information"
        Exit Function
    End If

    ' Copy the information from the buffer into a
    ' usable structure.
    MoveMemory fixed_file_info, info_address, _
        Len(fixed_file_info)

    ' Get the version information.
    With fixed_file_info
        ' Structure version.
        result.StructureVersion = _
            Format$(.dwStrucVersionh) & "." & _
            Format$(.dwStrucVersionl)

        ' File version number.
        result.FileVersion = _
            Format$(.dwFileVersionMSh) & "." & _
            Format$(.dwFileVersionMSl) & "." & _
            Format$(.dwFileVersionLSh) & "." & _
            Format$(.dwFileVersionLSl)

        ' Product version number.
        result.ProductVersion = _
            Format$(.dwProductVersionMSh) & "." & _
            Format$(.dwProductVersionMSl) & "." & _
            Format$(.dwProductVersionLSh) & "." & _
            Format$(.dwProductVersionLSl)

        ' File attributes.
        result.FileFlags = ""
        If .dwFileFlags And VS_FF_DEBUG Then _
            result.FileFlags = result.FileFlags & " Debug"
        If .dwFileFlags And VS_FF_PRERELEASE Then _
            result.FileFlags = result.FileFlags & " PreRel"
        If .dwFileFlags And VS_FF_PATCHED Then _
            result.FileFlags = result.FileFlags & " Patched"
        If .dwFileFlags And VS_FF_PRIVATEBUILD Then _
            result.FileFlags = result.FileFlags & " Private"
        If .dwFileFlags And VS_FF_INFOINFERRED Then _
            result.FileFlags = result.FileFlags & " Info"
        If .dwFileFlags And VS_FF_SPECIALBUILD Then _
            result.FileFlags = result.FileFlags & " Special"
        If .dwFileFlags And VFT2_UNKNOWN Then _
            result.FileFlags = result.FileFlags + " Unknown"
        If Len(result.FileFlags) > 0 Then result.FileFlags _
            = Mid$(result.FileFlags, 2)

        ' Target operating system.
        Select Case .dwFileOS
            Case VOS_DOS_WINDOWS16
                result.TargetOperatingSystem = "DOS-Win16"
            Case VOS_DOS_WINDOWS32
                result.TargetOperatingSystem = "DOS-Win32"
            Case VOS_OS216_PM16
                result.TargetOperatingSystem = "OS/2-16 " & _
                    "PM-16"
            Case VOS_OS232_PM32
                result.TargetOperatingSystem = "OS/2-16 " & _
                    "PM-32"
            Case VOS_NT_WINDOWS32
                result.TargetOperatingSystem = "NT-Win32"
            Case Else
                result.TargetOperatingSystem = "Unknown"
        End Select

        ' File type.
        Select Case .dwFileType
           Case VFT_APP
                result.FileType = "App"
           Case VFT_DLL
                result.FileType = "DLL"
           Case VFT_DRV
                result.FileType = "Driver"
                Select Case fixed_file_info.dwFileSubtype
                    Case VFT2_DRV_PRINTER
                        result.FileSubtype = "Printer drv"
                    Case VFT2_DRV_KEYBOARD
                        result.FileSubtype = "Keyboard drv"
                    Case VFT2_DRV_LANGUAGE
                        result.FileSubtype = "Language drv"
                    Case VFT2_DRV_DISPLAY
                        result.FileSubtype = "Display drv"
                    Case VFT2_DRV_MOUSE
                        result.FileSubtype = "Mouse drv"
                    Case VFT2_DRV_NETWORK
                        result.FileSubtype = "Network drv"
                    Case VFT2_DRV_SYSTEM
                        result.FileSubtype = "System drv"
                    Case VFT2_DRV_INSTALLABLE
                        result.FileSubtype = "Installable"
                    Case VFT2_DRV_SOUND
                        result.FileSubtype = "Sound drv"
                    Case VFT2_DRV_COMM
                        result.FileSubtype = "Comm drv"
                    Case VFT2_UNKNOWN
                        result.FileSubtype = "Unknown"
                End Select
           Case VFT_FONT
                result.FileType = "Font"
           Case VFT_VXD
                result.FileType = "VxD"
           Case VFT_STATIC_LIB
                result.FileType = "Lib"
           Case Else
                result.FileType = "Unknown"
        End Select
    End With

    VersionInformation = result
End Function
