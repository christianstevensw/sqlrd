Attribute VB_Name = "modVersion"
Option Explicit

' module by Wpsjr1@syix.com (works with cVersion.cls)
' http://www.syix.com/wpsjr1/index.html

'dwFileOS flags
Public Enum FILEOS
   VOS_UNKNOWN = 0&
   VOS_DOS = 10000&
   VOS_OS216 = 20000&
   VOS_OS232 = 30000&
   VOS_NT = 40000

   VOS_BASE = 0&
   VOS__WINDOWS16 = 1&
   VOS_PM16 = 2&
   VOS_PM32 = 3&
   VOS_WINDOWS32 = 4&

   VOS_DOS_WINDOWS16 = 10001&
   VOS_DOS_WINDOWS32 = 10004&
   VOS_OS216_PM16 = 20002&
   VOS_OS232_PM32 = 30003&
   VOS_NT_WINDOWS32 = 40004
End Enum

' dwFileFlags
Public Enum FILEFLAGS
   VS_FF_DEBUG = 1&
   VS_FF_PRERELEASE = 2&
   VS_FF_PATCHED = 4&
   VS_FF_PRIVATEBUILD = 8&
   VS_FF_INFOINFERRED = 16&
   VS_FF_SPECIALBUILD = 20&
End Enum

' dwFileType
Public Enum FILETYPE
   VFT_UNKNOWN = 0&
   VFT_APP = 1&
   VFT_DLL = 2&
   VFT_DRV = 3&
   VFT_FONT = 4&
   VFT_VXD = 5&
   VFT_STATIC_LIB = 7&
End Enum

Public Type VS_FIXEDFILEINFO
  dwSignature        As Long
  dwStrucVersion     As Long     '  e.g. 0x00000042 = "0.42"
  dwFileVersionMS    As Long     '  e.g. 0x00030075 = "3.75"
  dwFileVersionLS    As Long     '  e.g. 0x00000031 = "0.31"
  dwProductVersionMS As Long     '  e.g. 0x00030010 = "3.10"
  dwProductVersionLS As Long     '  e.g. 0x00000031 = "0.31"
  dwFileFlagsMask    As Long     '  = 0x3F for version "0.42"
  dwFileFlags        As Long     '  e.g. VFF_DEBUG Or VFF_PRERELEASE
  dwFileOS           As Long     '  e.g. VOS_DOS_WINDOWS16
  dwFileType         As Long     '  e.g. VFT_DRIVER
  dwFileSubtype      As Long     '  e.g. VFT2_DRV_KEYBOARD
  dwFileDateMS       As Long     '  e.g. 0
  dwFileDateLS       As Long     '  e.g. 0
End Type

Public Function HiWord(l As Long) As Integer
  HiWord = (l \ 65536) And &HFFFF&
End Function

Public Function LoWord(l As Long) As Integer
  LoWord = l And &HFFFF&
End Function

Public Function GetFileType(ft As FILETYPE) As String
  Dim s$
  
  Select Case ft
    Case VFT_APP
      s = "Application"
    Case VFT_DLL
      s = "Dynamic Link Library"
    Case VFT_DRV
      s = "Device Driver"
    Case VFT_FONT
      s = "Font"
    Case VFT_VXD
      s = "Virtual Device"
    Case VFT_STATIC_LIB
      s = "Static Link Library"
    Case Else
      s = "Unknown Type"
  End Select
  GetFileType = s
End Function

Public Function GetOSType(fo As FILEOS) As String
  Dim s$
  
  Select Case fo
    Case VOS_WINDOWS32
      s = "Win32"
    Case VOS_NT
      s = "NT"
    Case VOS_DOS_WINDOWS32
      s = "Win32 DOS"
    Case VOS_NT_WINDOWS32
      s = "Win32 NT"
    Case Else
      s = "Unknown"
  End Select
  GetOSType = s
End Function
