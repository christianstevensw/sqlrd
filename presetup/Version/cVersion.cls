VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "cVersion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' class module by Wpsjr1@syix.com
' http://www.syix.com/wpsjr1/index.html
' get all version info about an external file

Private Declare Function GetFileVersionInfo Lib "version.dll" Alias "GetFileVersionInfoA" (ByVal lptstrFilename As String, ByVal dwHandle As Long, ByVal dwLen As Long, lpData As Any) As Long
Private Declare Function GetFileVersionInfoSize Lib "version.dll" Alias "GetFileVersionInfoSizeA" (ByVal lptstrFilename As String, lpdwHandle As Long) As Long
Private Declare Sub RtlMoveMemory Lib "kernel32" (dest As Any, src As Any, ByVal cb&)
Private Declare Function VerQueryValue Lib "version.dll" Alias "VerQueryValueA" (pBlock As Any, ByVal lpSubBlock As String, ByVal lplpBuffer As Long, puLen As Long) As Long

Private m_sFileName As String
Private m_bData() As Byte

Private Const LANG_ENGLISH = &H9
Private Const SUBLANG_ENGLISH_CAN = &H4

Public Enum VERSIONPROPS
  Comments
  Company_Name
  File_Description
  File_Version
  Internal_Name
  Legal_Copyright
  Legal_Trademarks
  Original_FileName
  Product_Name
  Product_Version
End Enum

Public Property Get FileName() As String
  FileName = m_sFileName
End Property

Public Property Let FileName(ByVal sFileName As String)
  m_sFileName = sFileName
End Property

' by using a Friend Sub you can pass a udt ByRef in VB5
Friend Sub Version(udtVS As VS_FIXEDFILEINFO, sVersion As String, eVP As VERSIONPROPS)
  If Len(m_sFileName) = 0 Then Exit Sub
  
  Dim pp&, pointer&, length&, l&, lHandle&, lRet&, lData&, s$, iLoWord%
  
  lRet = GetFileVersionInfoSize(m_sFileName, lHandle)
  If lRet > 0 Then
    ReDim m_bData(lRet)
      
    If GetFileVersionInfo(m_sFileName, 0, lRet, m_bData(0)) Then
      pp = VarPtr(pointer)
        
      If VerQueryValue(m_bData(0), "\", pp, length) Then
        RtlMoveMemory udtVS, ByVal pointer, Len(udtVS)
          
        If VerQueryValue(m_bData(0), "\VarFileInfo\Translation", pp, length) Then
          For l = 1 To length \ 4
            RtlMoveMemory lData, ByVal pointer, 4
            iLoWord = LoWord(lData)
            s = Right$("0000" & Hex(iLoWord), 4) & Right$("0000" & Hex(HiWord(lData)), 4)
            If iLoWord = (SUBLANG_ENGLISH_CAN * 256) + LANG_ENGLISH Then Exit For  ' English (Canadian?!)
          Next l
          If s = "00000000" Then s = "04090000"
            
          If VerQueryValue(m_bData(0), "\StringFileInfo\" & s & "\" & TranslateVersionProps(eVP), pp, length) Then
            length = length - 1 ' don't want the null
            sVersion = String$(length, 0)
            RtlMoveMemory ByVal sVersion, ByVal pointer, length
          End If
        End If
      End If
    End If
  End If
End Sub

Private Function TranslateVersionProps(vp As VERSIONPROPS) As String
  Dim s$
  
  Select Case vp
    Case Comments
      s = "Comments"
    Case Company_Name
      s = "CompanyName"
    Case File_Description
      s = "FileDescription"
    Case File_Version
      s = "FileVersion"
    Case Internal_Name
      s = "InternalName"
    Case Legal_Copyright
      s = "LegalCopyright"
    Case Legal_Trademarks
      s = "LegalTrademarks"
    Case Original_FileName
      s = "OriginalFileName"
    Case Product_Name
      s = "ProductName"
    Case Product_Version
      s = "ProductVersion"
    Case Else
      s = ""
  End Select
  TranslateVersionProps = s
End Function

