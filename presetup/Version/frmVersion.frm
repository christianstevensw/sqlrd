VERSION 5.00
Begin VB.Form frmVersion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Version Sample"
   ClientHeight    =   2508
   ClientLeft      =   36
   ClientTop       =   312
   ClientWidth     =   3744
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   2508
   ScaleWidth      =   3744
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdVersion 
      Caption         =   "Get Version Info"
      Height          =   372
      Left            =   1080
      TabIndex        =   0
      Top             =   1080
      Width           =   1452
   End
End
Attribute VB_Name = "frmVersion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' example by Wpsjr1@syix.com
' http://www.syix.com/wpsjr1/index.html
' Get all version info on an executable file.
' you must have a compiled EXE in the App.Path to see its version information.

Dim ver As cVersion

Private Sub Form_Load()
  Set ver = New cVersion
End Sub

Private Sub cmdVersion_Click()
  Dim vs As VS_FIXEDFILEINFO, s$
  
  ver.FileName = App.Path & "\pjtVersion.exe"
  ver.Version vs, s, Comments
  
  'Debug.Print Hex(vs.dwSignature)                  '  FEEF04BD
  'Debug.Print vs.dwStrucVersion                    '  e.g. 0x00000042 = "0.42"
  Debug.Print HiWord(vs.dwFileVersionMS) & " Major" '  e.g. 0x00030075 = "3.75"
  Debug.Print LoWord(vs.dwFileVersionMS) & " Minor"
  Debug.Print vs.dwFileVersionLS & " Revision"      '  e.g. 0x00000031 = "0.31"
  'Debug.Print vs.dwProductVersionMS                '  e.g. 0x00030010 = "3.10"
  'Debug.Print vs.dwProductVersionLS                '  e.g. 0x00000031 = "0.31"
  'Debug.Print vs.dwFileFlagsMask                   '  = 0x3F for version "0.42"
  'Debug.Print vs.dwFileFlags                       '  e.g. VFF_DEBUG Or VFF_PRERELEASE
  Debug.Print GetOSType(vs.dwFileOS)                '  e.g. VOS_DOS_WINDOWS16
  Debug.Print GetFileType(vs.dwFileType)            '  e.g. VFT_DRIVER
  'Debug.Print vs.dwFileSubtype                     '  e.g. VFT2_DRV_KEYBOARD
  'Debug.Print vs.dwFileDateMS                      '  e.g. 0
  'Debug.Print vs.dwFileDateLS                      '  e.g. 0
  Debug.Print s
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Set ver = Nothing
End Sub
