<?xml version="1.0"?>
<doc>
    <assembly>
        <name>Gnostice.PDFOne.PDFPrinter</name>
    </assembly>
    <members>
        <member name="T:Gnostice.PDFOne.PDFPrinter.PrinterErrors">
            <summary>
            Specifies the errors that may occur during the usage of PDFPrinter component.
            </summary>
        </member>
        <member name="T:Gnostice.PDFOne.PDFPrinter.PrintSubRange">
            <summary>
            Specifies the standard sub-ranges for printing PDF pages using PDFPrinter component.
            </summary>
        </member>
        <member name="F:Gnostice.PDFOne.PDFPrinter.PrintSubRange.All">
            <summary>
            Prints all available pages in the PDF.
            </summary>
        </member>
        <member name="F:Gnostice.PDFOne.PDFPrinter.PrintSubRange.OddPages">
            <summary>
            Prints only odd numbered pages in the PDF.
            </summary>
        </member>
        <member name="F:Gnostice.PDFOne.PDFPrinter.PrintSubRange.EvenPages">
            <summary>
            Prints only even numbered pages in the PDF.
            </summary>
        </member>
        <member name="T:Gnostice.PDFOne.PDFPrinter.PrintScaleType">
            <summary>
            Specifies the standard scale types for printing PDF files using PDFPrinter component.
            </summary>
        </member>
        <member name="F:Gnostice.PDFOne.PDFPrinter.PrintScaleType.None">
            <summary>
            No scaling is done, any larger page area is clipped off during printing.
            </summary>
        </member>
        <member name="F:Gnostice.PDFOne.PDFPrinter.PrintScaleType.FitToPrintableArea">
            <summary>
            Scales the PDF page to the printable area of the printer.
            </summary>
        </member>
        <member name="F:Gnostice.PDFOne.PDFPrinter.PrintScaleType.ReduceToPrintableArea">
            <summary>
            Scales the PDF page to the printable area of the printer only if the PDF page is larger.
            </summary>
        </member>
        <member name="T:Gnostice.PDFOne.PDFPrinter.PrintPageDelegate">
            <summary>
            Represents the method that will handle the <see cref="!:BeforePrintPage"/> and <see cref="!:AfterPrintPage"/> events
            of the PDFPrinter component.
            </summary>
            <param name="sender">The source of the event, i.e. an object of PDFPrinter class.</param>
            <param name="pageNumber">The page number of the currently printing page.</param>
            <param name="e">A PrintPageEventArgs that contains the event data.</param>
        </member>
        <member name="T:Gnostice.PDFOne.PDFPrinter.ChangePaperSourceDelegate">
            <summary>
            Represents the method that will handle the <see cref="!:ChangePaperSource"/> event of the 
            PDFPrinter component.
            </summary>
            <param name="sender">The source of the event, i.e. an object of PDFPrinter class.</param>
            <param name="pageNumber">The page number of the currently printing page.</param>
            <param name="paperSource">The paper source for printing this page.</param>
        </member>
        <member name="T:Gnostice.PDFOne.PDFPrinter.PrintErrorDelegate">
            <summary>
            Represents the method that will handle the <see cref="!:PrintError"/> event of the 
            PDFPrinter component.
            </summary>
            <param name="sender">The source of the event, i.e. an object of PDFPrinter class.</param>
            <param name="pageNumber">The page number of the currently printing page where error occurred.</param>
            <param name="e">The exception object with description of the error.</param>
        </member>
        <member name="T:Gnostice.PDFOne.PDFPrinter.PasswordRequiredHandler">
            <summary>
            Represents the method that will handle <see cref="!:PasswordRequired"/> event of the
            PDFPrinter component.
            </summary>
            <param name="sender">The source of the event, i.e. an object of PDFPrinter class.</param>
            <param name="password">The password for loading the current PDF document.</param>
        </member>
        <member name="T:Gnostice.PDFOne.PDFPrinter.PDFPrinter">
            <summary>
            Represents a control to print PDF files to printer.
            </summary>
        </member>
        <member name="M:Gnostice.PDFOne.PDFPrinter.PDFPrinter.#ctor">
            <summary>
            Initializes a new instance of PDFPrinter control.
            </summary>
        </member>
        <member name="M:Gnostice.PDFOne.PDFPrinter.PDFPrinter.LoadDocument(System.IO.Stream,System.String)">
            <summary>
            Loads the PDFPrinter component with the PDF file provided as stream.
            </summary>
            <param name="stream">An IO stream containing the PDF file to be loaded in the PDFPrinter.</param>
            <param name="password">The password required to load the document.</param>
            <returns>Returns <see cref="T:Gnostice.PDFOne.PDFPrinter.PrinterErrors"/> indicating the success or failure of the load operation.</returns>
            <remarks>
            If <code>password</code> parameter is <code>null</code>, then <see cref="E:Gnostice.PDFOne.PDFPrinter.PDFPrinter.PasswordRequired"/>
            event is raised. After a maximum of 3 unsuccessfull attempts, the loading 
            operation is aborted.
            
            The possible error messages that this method can return are:
            <list type="table">
            <listheader>
            <term>Error</term>
            <description>Meaning</description>
            </listheader>
            <item>
            <term>PrinterErrors.ActiveDocumentNotClosed</term>
            <description>A previously loaded PDF document was not closed properly</description>
            </item>
            <item>
            <term>PrinterErrors.InvalidPassword</term>
            <description>The password provided for loading the document, is wrong</description>
            </item>
            <item>
            <term>PrinterErrors.LoadFromStreamFailed</term>
            <description>The component was unable to load the PDF stream</description>
            </item>
            </list>
            </remarks>
        </member>
        <member name="M:Gnostice.PDFOne.PDFPrinter.PDFPrinter.LoadDocument(System.IO.Stream)">
            <summary>
            Loads the PDFPrinter component with the PDF file provided as stream.
            </summary>
            <param name="stream">An IO stream containing the PDF file to be loaded in the PDFPrinter.</param>
            <returns>Returns <see cref="T:Gnostice.PDFOne.PDFPrinter.PrinterErrors"/> indicating the success or failure of the load operation.</returns>
            <remarks>
            If the PDF document is password protected, then <see cref="E:Gnostice.PDFOne.PDFPrinter.PDFPrinter.PasswordRequired"/> event is raised. After a 
            maximum of 3 unsuccessfull attempts, the loading operation is aborted.
            
            The possible error messages that this method can return are:
            <list type="table">
            <listheader>
            <term>Error</term>
            <description>Meaning</description>
            </listheader>
            <item>
            <term>PrinterErrors.ActiveDocumentNotClosed</term>
            <description>A previously loaded PDF document was not closed properly</description>
            </item>
            <item>
            <item>
            <term>PrinterErrors.InvalidPassword</term>
            <description>The password provided for loading the document, is wrong</description>
            </item>
            <term>PrinterErrors.LoadFromStreamFailed</term>
            <description>The component was unable to load the PDF stream</description>
            </item>
            </list>
            </remarks>
        </member>
        <member name="M:Gnostice.PDFOne.PDFPrinter.PDFPrinter.LoadDocument(System.String,System.String)">
            <summary>
            Loads the PDFPrinter component with the PDF file specified.
            </summary>
            <param name="fileName">The file name of the PDF file to be loaded.</param>
            <param name="password">The password required to load the document.</param>
            <returns>Returns <see cref="T:Gnostice.PDFOne.PDFPrinter.PrinterErrors"/> indicating the success or failure of the load operation.</returns>
            <remarks>
            If the PDF document is password protected, then <see cref="E:Gnostice.PDFOne.PDFPrinter.PDFPrinter.PasswordRequired"/> event is raised. After a 
            maximum of 3 unsuccessfull attempts, the loading operation is aborted.
            
            The possible error messages that this method can return are:
            <list type="table">
            <listheader>
            <term>Error</term>
            <description>Meaning</description>
            </listheader>
            <item>
            <term>PrinterErrors.ActiveDocumentNotClosed</term>
            <description>A previously loaded PDF document was not closed properly</description>
            </item>
            <item>
            <item>
            <term>PrinterErrors.InvalidPassword</term>
            <description>The password provided for loading the document, is wrong</description>
            </item>
            <term>PrinterErrors.LoadFromFileFailed</term>
            <description>The component was unable to load the PDF file</description>
            </item>
            </list>
            </remarks>
        </member>
        <member name="M:Gnostice.PDFOne.PDFPrinter.PDFPrinter.LoadDocument(System.String)">
            <summary>
            Loads the PDFPrinter component with the PDF file specified.
            </summary>
            <param name="fileName">The file name of the PDF file to be loaded.</param>
            <returns>Returns <see cref="T:Gnostice.PDFOne.PDFPrinter.PrinterErrors"/> indicating the success or failure of the load operation.</returns>
            <remarks>
            If the PDF document is password protected, then <see cref="E:Gnostice.PDFOne.PDFPrinter.PDFPrinter.PasswordRequired"/> event is raised. After a 
            maximum of 3 unsuccessfull attempts, the loading operation is aborted.
            
            The possible error messages that this method can return are:
            <list type="table">
            <listheader>
            <term>Error</term>
            <description>Meaning</description>
            </listheader>
            <item>
            <term>PrinterErrors.ActiveDocumentNotClosed</term>
            <description>A previously loaded PDF document was not closed properly</description>
            </item>
            <item>
            <item>
            <term>PrinterErrors.InvalidPassword</term>
            <description>The password provided for loading the document, is wrong</description>
            </item>
            <term>PrinterErrors.LoadFromFileFailed</term>
            <description>The component was unable to load the PDF file</description>
            </item>
            </list>
            </remarks>
        </member>
        <member name="M:Gnostice.PDFOne.PDFPrinter.PDFPrinter.LoadDocument(Gnostice.PDFOne.PDFDocument,System.String)">
            <summary>
            Loads the PDFPrinter component with the specified <see cref="T:Gnostice.PDFOne.PDFDocument"/> object.
            </summary>
            <param name="document">The PDFDocument object to be loaded.</param>
            <param name="password">The password required to load the document.</param>
            <returns>Returns <see cref="T:Gnostice.PDFOne.PDFPrinter.PrinterErrors"/> indicating the success or failure of the load operation.</returns>
            <remarks>
            If <code>password</code> parameter is <code>null</code>, then <see cref="E:Gnostice.PDFOne.PDFPrinter.PDFPrinter.PasswordRequired"/>
            event is raised. After a maximum of 3 unsuccessfull attempts, the loading 
            operation is aborted.
            
            The possible error messages that this method can return are:
            <list type="table">
            <listheader>
            <term>Error</term>
            <description>Meaning</description>
            </listheader>
            <item>
            <term>PrinterErrors.ActiveDocumentNotClosed</term>
            <description>A previously loaded PDF document was not closed properly</description>
            </item>
            <item>
            <term>PrinterErrors.InvalidPassword</term>
            <description>The password provided for loading the document, is wrong</description>
            </item>
            <item>
            <term>PrinterErrors.LoadFromStreamFailed</term>
            <description>The component was unable to load the PDFDocument</description>
            </item>
            </list>
            </remarks>
        </member>
        <member name="M:Gnostice.PDFOne.PDFPrinter.PDFPrinter.LoadDocument(Gnostice.PDFOne.PDFDocument)">
            <summary>
            Loads the PDFPrinter component with the specified <see cref="T:Gnostice.PDFOne.PDFDocument"/> object.
            </summary>
            <param name="document">The PDFDocument object to be loaded.</param>
            <returns>Returns <see cref="T:Gnostice.PDFOne.PDFPrinter.PrinterErrors"/> indicating the success or failure of the load operation.</returns>
            <remarks>
            If the PDF document is password protected, then <see cref="E:Gnostice.PDFOne.PDFPrinter.PDFPrinter.PasswordRequired"/> event is raised. After a 
            maximum of 3 unsuccessfull attempts, the loading operation is aborted.
            
            The possible error messages that this method can return are:
            <list type="table">
            <listheader>
            <term>Error</term>
            <description>Meaning</description>
            </listheader>
            <item>
            <term>PrinterErrors.ActiveDocumentNotClosed</term>
            <description>A previously loaded PDF document was not closed properly</description>
            </item>
            <item>
            <item>
            <term>PrinterErrors.InvalidPassword</term>
            <description>The password provided for loading the document, is wrong</description>
            </item>
            <term>PrinterErrors.LoadFromStreamFailed</term>
            <description>The component was unable to load the PDFDocument object</description>
            </item>
            </list>
            </remarks>
        </member>
        <member name="M:Gnostice.PDFOne.PDFPrinter.PDFPrinter.LoadDocument(Gnostice.PDFOne.PDFDocUtil.PDFDocUtils)">
            <summary>
            Loads the PDFPrinter component with the given document handle. The document which 
            has been already loaded with other components (like PDFViewer) can be directly loaded to the 
            PDFPrinter component using this overloaded method.
            </summary>
            <param name="docHandle">An object of PDFDocUtils class returned by GetDocumentHandle() 
            method of PDFViewer or other PDFOne components.</param>
            <returns>Returns <see cref="T:Gnostice.PDFOne.PDFPrinter.PrinterErrors"/> indicating the success or failure of the load operation.</returns>
            <remarks>
            It is recommended NOT to use docHandle object directly as it may have undesired effects 
            on the other components.
            
            The possible error messages that this method can return are:
            <list type="table">
            <listheader>
            <term>Error</term>
            <description>Meaning</description>
            </listheader>
            <item>
            <term>PrinterErrors.ActiveDocumentNotClosed</term>
            <description>A previously loaded PDF document was not closed properly</description>
            </item>
            <item>
            <term>PrinterErrors.InvalidInputDocument</term>
            <description>The document handle provided is either not valid or not currently loaded with a PDF document</description>
            </item>
            </list>
            </remarks>
        </member>
        <member name="M:Gnostice.PDFOne.PDFPrinter.PDFPrinter.GetDocumentHandle">
            <summary>
            Gets the PDFDocUtils object for use with other components like PDFViewer.
            This represents the PDF document that is currently loaded in the PDFPrinter.
            </summary>
            <returns>An object of PDFDocUtils class.</returns>
            <example>
            // p is the PDFPrinter object
            PDFViewer v = new PDFViewer(p.GetDocumentHandle())
            </example>
            <remarks>
            It is recommended NOT to use this object directly as it may have undesired effects 
            on the Printer component.
            </remarks>
        </member>
        <member name="M:Gnostice.PDFOne.PDFPrinter.PDFPrinter.CloseDocument">
            <summary>
            Closes the currently loaded PDF document in the PDFPrinter component.
            </summary>
            <returns>Returns <see cref="T:Gnostice.PDFOne.PDFPrinter.PrinterErrors"/> indicating the success or failure of the close operation.</returns>
            <remarks>
            The possible error messages that this method can return are:
            <list type="table">
            <listheader>
            <term>Error</term>
            <description>Meaning</description>
            </listheader>
            <item>
            <term>PrinterErrors.ActiveDocumentNotClosed</term>
            <description>A previously loaded PDF document was not closed properly</description>
            </item>
            </list>
            </remarks>
        </member>
        <member name="M:Gnostice.PDFOne.PDFPrinter.PDFPrinter.Print(System.String)">
            <summary>
            Prints the currently loaded PDF document to the selected printer.
            </summary>
            <param name="documentName">Document name to display (for example, in a print status dialog box or printer queue) while printing the document.</param>
            <returns>Returns <see cref="T:Gnostice.PDFOne.PDFPrinter.PrinterErrors"/> indicating success or failure of the load operation.</returns>
            <remarks>
            The possible errors that this method can return are:
            <list type="table">
            <listheader>
            <term>Error</term>
            <description>Meaning</description>
            </listheader>
            <item>
            <term>PrinterErrors.PrintingFailed</term>
            <description>The component was unable to print the PDF file</description>
            </item>
            <item>
            <term>PrinterErrors.InvalidPageNumbers</term>
            <description>The page numbers to print are invalid for the current document.</description>
            </item>
            </list>
            </remarks>
        </member>
        <member name="M:Gnostice.PDFOne.PDFPrinter.PDFPrinter.Print">
            <summary>
            Prints the currently loaded PDF document to the selected printer.
            </summary>
            <returns>Returns <see cref="T:Gnostice.PDFOne.PDFPrinter.PrinterErrors"/> indicating success or failure of the load operation.</returns>
            <remarks>
            The possible errors that this method can return are:
            <list type="table">
            <listheader>
            <term>Error</term>
            <description>Meaning</description>
            </listheader>
            <item>
            <term>PrinterErrors.PrintingFailed</term>
            <description>The component was unable to print the PDF file</description>
            </item>
            <item>
            <term>PrinterErrors.InvalidPageNumbers</term>
            <description>The page numbers to print are invalid for the current document.</description>
            </item>
            </list>
            </remarks>
        </member>
        <member name="P:Gnostice.PDFOne.PDFPrinter.PDFPrinter.PrintOptions">
            <summary>
            Gets or sets settings related to current printer.
            </summary>
        </member>
        <member name="P:Gnostice.PDFOne.PDFPrinter.PDFPrinter.ShowPrintStatus">
            <summary>
            Gets or sets a value that indicates if the PDFPrinter 
            will show print status dialog during printing.
            </summary>
        </member>
        <member name="P:Gnostice.PDFOne.PDFPrinter.PDFPrinter.PDFLoaded">
            <summary>
            Gets a value that indicates whether the PDFPrinter is currently 
            loaded with a PDF file.
            </summary>
        </member>
        <member name="P:Gnostice.PDFOne.PDFPrinter.PDFPrinter.InputFileName">
            <summary>
            Gets the input PDF file name that is currently loaded by 
            the PDFPrinter control.
            </summary>
            <remarks>
            This value is an empty string if the PDF document was loaded using either
            stream or <see cref="T:Gnostice.PDFOne.PDFDocument"/> object.
            </remarks>
        </member>
        <member name="P:Gnostice.PDFOne.PDFPrinter.PDFPrinter.AutoRepair">
            <summary>
            Gets or sets a value that indicates if the PDF file needs to automatically 
            repaired in case it has errors.
            </summary>
        </member>
        <member name="P:Gnostice.PDFOne.PDFPrinter.PDFPrinter.ReversePageOrder">
            <summary>
            Gets or sets a value that indicates if the page order needs to be reversed during printing.
            </summary>
        </member>
        <member name="P:Gnostice.PDFOne.PDFPrinter.PDFPrinter.TotalPageCount">
            <summary>
            Gets the total number of pages in the current PDF file.
            </summary>
        </member>
        <member name="P:Gnostice.PDFOne.PDFPrinter.PDFPrinter.SelectedPages">
            <summary>
            Gets or sets a string representing custom selected pages that need to be 
            printed.
            </summary>
        </member>
        <member name="P:Gnostice.PDFOne.PDFPrinter.PDFPrinter.PageScaleType">
            <summary>
            Gets or sets the current <see cref="T:Gnostice.PDFOne.PDFPrinter.PrintScaleType"/> for the PDFPrinter component.
            </summary>
        </member>
        <member name="P:Gnostice.PDFOne.PDFPrinter.PDFPrinter.PageSubRange">
            <summary>
            Gets or sets the current <see cref="T:Gnostice.PDFOne.PDFPrinter.PrintSubRange"/> for the PDFPrinter component.
            </summary>
        </member>
        <member name="P:Gnostice.PDFOne.PDFPrinter.PDFPrinter.CurrentPageNumber">
            <summary>
            Gets or sets the current page number for the PDFPrinter component.
            </summary>
        </member>
        <member name="P:Gnostice.PDFOne.PDFPrinter.PDFPrinter.OffsetHardMargins">
            <summary>
            Gets or sets a value that indicates if the page contents need to be offset to the 
            left and top to avoid the hard margins. If set to true, this may cause clipping of 
            the page contents at the extreme left and top.
            </summary>
        </member>
        <member name="P:Gnostice.PDFOne.PDFPrinter.PDFPrinter.AutoRotate">
            <summary>
            Gets or sets a value that indicates if the page contents need to be automatically
            rotated before printing. If set to true, the page is rotated to adjust the contents
            to the paper orientation - portrait or landscape.
            </summary>
        </member>
        <member name="P:Gnostice.PDFOne.PDFPrinter.PDFPrinter.AutoCenter">
            <summary>
            Gets or sets a value that indicates if the page contents need to be automatically
            centered before printing. If set to true, the page content is centered to the paper 
            size.
            </summary>
        </member>
        <member name="E:Gnostice.PDFOne.PDFPrinter.PDFPrinter.BeforePrintPage">
            <summary>
            Occurs just before printing a page using the PDFPrinter component.
            </summary>
        </member>
        <member name="E:Gnostice.PDFOne.PDFPrinter.PDFPrinter.PrintError">
            <summary>
            Occurs when there is an error during the printing process.
            </summary>
        </member>
        <member name="E:Gnostice.PDFOne.PDFPrinter.PDFPrinter.AfterPrintPage">
            <summary>
            Occurs just after printing a page using the PDFPrinter component.
            </summary>
        </member>
        <member name="E:Gnostice.PDFOne.PDFPrinter.PDFPrinter.ChangePaperSource">
            <summary>
            Occurs just before starting to print the page.
            </summary>
            <remarks>
            This event can be used to print some pages of the PDF document to different printer trays 
            by setting the appropriate paper source in the handler.
            </remarks>
            <example>
            // Handle new event
            pdfPrinter1.ChangePaperSource += new ChangePaperSourceDelegate(pdfPrinter1_ChangePaperSource);
            
            // Set other settings and then print
            pdfPrinter1.Print();
            
            // change the paper source (or trays) as required
            void pdfPrinter1_ChangePaperSource(object sender, int pageNumber, ref PaperSource paperSource)
            {
              if (pageNumber &lt; 3)
                paperSource = pdfPrinter1.PrintOptions.PaperSources[1];
              else
                paperSource = pdfPrinter1.PrintOptions.PaperSources[0];
            }
            </example>
        </member>
        <member name="E:Gnostice.PDFOne.PDFPrinter.PDFPrinter.PasswordRequired">
            <summary>
            Occurs when the user loads a PDF document which requires a password to open.
            </summary>
        </member>
    </members>
</doc>
