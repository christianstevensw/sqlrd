﻿Imports System

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports sqlrd



'''<summary>
'''This is a test class for clsMarsReportTest and is intended
'''to contain all clsMarsReportTest Unit Tests
'''</summary>
<TestClass()> _
Public Class clsMarsReportTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for processCSVFile
    '''</summary>
    <TestMethod()> _
    Public Sub processCSVFileTest()
        Dim target As clsMarsReport = New clsMarsReport ' TODO: Initialize to an appropriate value
        Dim csvFile As String = "c:\users\steven\desktop\csv.csv"
        Dim seperator As String = "," ' TODO: Initialize to an appropriate value
        Dim delimiter As String = Chr(34) ' TODO: Initialize to an appropriate value
        Dim outputFile As String = "c:\users\steven\desktop\csv.txt" ' TODO: Initialize to an appropriate value
        Dim errInfo As Exception = Nothing ' TODO: Initialize to an appropriate value
        Dim errInfoExpected As Exception = Nothing ' TODO: Initialize to an appropriate value
        Dim ignoreHeader As Boolean = False ' TODO: Initialize to an appropriate value
        Dim expected As Boolean = False ' TODO: Initialize to an appropriate value
        Dim actual As Boolean

        actual = target.processCSVFile(csvFile, seperator, delimiter, outputFile, errInfo, ignoreHeader)
        Assert.AreEqual(errInfoExpected, errInfo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Verify the correctness of this test method.")
    End Sub

    '''<summary>
    '''A test for prepareTextFiles
    '''</summary>
    <TestMethod()> _
    Public Sub prepareTextFilesTest()
        Dim target As clsMarsReport = New clsMarsReport ' TODO: Initialize to an appropriate value
        Dim sFileName As String = String.Empty ' TODO: Initialize to an appropriate value
        Dim IsDataDriven As Boolean = False ' TODO: Initialize to an appropriate value
        Dim sortByDate As Boolean = False ' TODO: Initialize to an appropriate value
        Dim expected As Boolean = False ' TODO: Initialize to an appropriate value
        Dim actual As Boolean
        actual = target.prepareTextFiles(sFileName, IsDataDriven, sortByDate)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Verify the correctness of this test method.")
    End Sub
End Class
